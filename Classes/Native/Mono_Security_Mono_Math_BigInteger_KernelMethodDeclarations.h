﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.BigInteger
struct BigInteger_t451;
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t563;
// System.UInt32[]
struct UInt32U5BU5D_t436;

#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"

// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::AddSameSign(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t451 * Kernel_AddSameSign_m1818 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi1, BigInteger_t451 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::Subtract(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t451 * Kernel_Subtract_m1819 (Object_t * __this /* static, unused */, BigInteger_t451 * ___big, BigInteger_t451 * ___small, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MinusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_MinusEq_m1820 (Object_t * __this /* static, unused */, BigInteger_t451 * ___big, BigInteger_t451 * ___small, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::PlusEq(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" void Kernel_PlusEq_m1821 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi1, BigInteger_t451 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger/Sign Mono.Math.BigInteger/Kernel::Compare(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" int32_t Kernel_Compare_m1822 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi1, BigInteger_t451 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::SingleByteDivideInPlace(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_SingleByteDivideInPlace_m1823 (Object_t * __this /* static, unused */, BigInteger_t451 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::DwordMod(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_DwordMod_m1824 (Object_t * __this /* static, unused */, BigInteger_t451 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::DwordDivMod(Mono.Math.BigInteger,System.UInt32)
extern "C" BigIntegerU5BU5D_t563* Kernel_DwordDivMod_m1825 (Object_t * __this /* static, unused */, BigInteger_t451 * ___n, uint32_t ___d, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger[] Mono.Math.BigInteger/Kernel::multiByteDivide(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigIntegerU5BU5D_t563* Kernel_multiByteDivide_m1826 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi1, BigInteger_t451 * ___bi2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::LeftShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t451 * Kernel_LeftShift_m1827 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::RightShift(Mono.Math.BigInteger,System.Int32)
extern "C" BigInteger_t451 * Kernel_RightShift_m1828 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi, int32_t ___n, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::Multiply(System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32,System.UInt32,System.UInt32[],System.UInt32)
extern "C" void Kernel_Multiply_m1829 (Object_t * __this /* static, unused */, UInt32U5BU5D_t436* ___x, uint32_t ___xOffset, uint32_t ___xLen, UInt32U5BU5D_t436* ___y, uint32_t ___yOffset, uint32_t ___yLen, UInt32U5BU5D_t436* ___d, uint32_t ___dOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/Kernel::MultiplyMod2p32pmod(System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32,System.UInt32[],System.Int32,System.Int32)
extern "C" void Kernel_MultiplyMod2p32pmod_m1830 (Object_t * __this /* static, unused */, UInt32U5BU5D_t436* ___x, int32_t ___xOffset, int32_t ___xLen, UInt32U5BU5D_t436* ___y, int32_t ___yOffest, int32_t ___yLen, UInt32U5BU5D_t436* ___d, int32_t ___dOffset, int32_t ___mod, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,System.UInt32)
extern "C" uint32_t Kernel_modInverse_m1831 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi, uint32_t ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/Kernel::modInverse(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t451 * Kernel_modInverse_m1832 (Object_t * __this /* static, unused */, BigInteger_t451 * ___bi, BigInteger_t451 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
