﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Binder
struct Binder_t184;

#include "mscorlib_System_Object.h"

// System.Reflection.Binder
struct  Binder_t184  : public Object_t
{
};
struct Binder_t184_StaticFields{
	// System.Reflection.Binder System.Reflection.Binder::default_binder
	Binder_t184 * ___default_binder_0;
};
