﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9021_gshared (InternalEnumerator_1_t1452 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9021(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1452 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9021_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9022_gshared (InternalEnumerator_1_t1452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9022(__this, method) (( void (*) (InternalEnumerator_1_t1452 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9022_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9023_gshared (InternalEnumerator_1_t1452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9023(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1452 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9023_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9024_gshared (InternalEnumerator_1_t1452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9024(__this, method) (( void (*) (InternalEnumerator_1_t1452 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9024_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9025_gshared (InternalEnumerator_1_t1452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9025(__this, method) (( bool (*) (InternalEnumerator_1_t1452 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9025_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern "C" Slot_t704  InternalEnumerator_1_get_Current_m9026_gshared (InternalEnumerator_1_t1452 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9026(__this, method) (( Slot_t704  (*) (InternalEnumerator_1_t1452 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9026_gshared)(__this, method)
