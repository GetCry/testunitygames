﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Globalization.Unicode.Contraction
struct Contraction_t631;
// System.Char[]
struct CharU5BU5D_t191;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t264;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Globalization.Unicode.Contraction::.ctor(System.Char[],System.String,System.Byte[])
extern "C" void Contraction__ctor_m3613 (Contraction_t631 * __this, CharU5BU5D_t191* ___source, String_t* ___replacement, ByteU5BU5D_t264* ___sortkey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
