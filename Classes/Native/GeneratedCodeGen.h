﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Exception
struct Exception_t134;
// System.Text.StringBuilder
struct StringBuilder_t190;
// System.MulticastDelegate
struct MulticastDelegate_t40;
// System.Reflection.MethodBase
struct MethodBase_t193;

#include "mscorlib_System_Array.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_System_RuntimeArgumentHandle.h"
#include "mscorlib_System_RuntimeMethodHandle.h"

#pragma once
typedef Object_t Il2CppCodeGenObject;
typedef Array_t Il2CppCodeGenArray;
typedef String_t Il2CppCodeGenString;
typedef Type_t Il2CppCodeGenType;
typedef Exception_t134 Il2CppCodeGenException;
typedef Exception_t134 Il2CppCodeGenException;
typedef RuntimeTypeHandle_t597 Il2CppCodeGenRuntimeTypeHandle;
typedef RuntimeFieldHandle_t599 Il2CppCodeGenRuntimeFieldHandle;
typedef RuntimeArgumentHandle_t616 Il2CppCodeGenRuntimeArgumentHandle;
typedef RuntimeMethodHandle_t1208 Il2CppCodeGenRuntimeMethodHandle;
typedef StringBuilder_t190 Il2CppCodeGenStringBuilder;
typedef MulticastDelegate_t40 Il2CppCodeGenMulticastDelegate;
typedef MethodBase_t193 Il2CppCodeGenMethodBase;
