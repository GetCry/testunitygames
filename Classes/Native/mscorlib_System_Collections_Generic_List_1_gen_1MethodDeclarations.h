﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m659(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1__ctor_m7942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor(System.Int32)
#define List_1__ctor_m8206(__this, ___capacity, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1__ctor_m7944_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.cctor()
#define List_1__cctor_m8207(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m7946_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8208(__this, method) (( Object_t* (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m8209(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7950_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m8210(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m8211(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m8212(__this, ___item, method) (( bool (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7956_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m8213(__this, ___item, method) (( int32_t (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7958_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m8214(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m8215(__this, ___item, method) (( void (*) (List_1_t142 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7962_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8216(__this, method) (( bool (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m8217(__this, method) (( Object_t * (*) (List_1_t142 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m8218(__this, ___index, method) (( Object_t * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7968_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m8219(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Add(T)
#define List_1_Add_m8220(__this, ___item, method) (( void (*) (List_1_t142 *, PersistentCall_t140 *, const MethodInfo*))List_1_Add_m7972_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m8221(__this, ___newCount, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m7974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Clear()
#define List_1_Clear_m8222(__this, method) (( void (*) (List_1_t142 *, const MethodInfo*))List_1_Clear_m7976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Contains(T)
#define List_1_Contains_m8223(__this, ___item, method) (( bool (*) (List_1_t142 *, PersistentCall_t140 *, const MethodInfo*))List_1_Contains_m7978_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m8224(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t142 *, PersistentCallU5BU5D_t1358*, int32_t, const MethodInfo*))List_1_CopyTo_m7980_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m8225(__this, method) (( Enumerator_t1359  (*) (List_1_t142 *, const MethodInfo*))List_1_GetEnumerator_m7981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::IndexOf(T)
#define List_1_IndexOf_m8226(__this, ___item, method) (( int32_t (*) (List_1_t142 *, PersistentCall_t140 *, const MethodInfo*))List_1_IndexOf_m7983_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m8227(__this, ___start, ___delta, method) (( void (*) (List_1_t142 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m7985_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m8228(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_CheckIndex_m7987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Insert(System.Int32,T)
#define List_1_Insert_m8229(__this, ___index, ___item, method) (( void (*) (List_1_t142 *, int32_t, PersistentCall_t140 *, const MethodInfo*))List_1_Insert_m7989_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::Remove(T)
#define List_1_Remove_m8230(__this, ___item, method) (( bool (*) (List_1_t142 *, PersistentCall_t140 *, const MethodInfo*))List_1_Remove_m7991_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m8231(__this, ___index, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7993_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::ToArray()
#define List_1_ToArray_m8232(__this, method) (( PersistentCallU5BU5D_t1358* (*) (List_1_t142 *, const MethodInfo*))List_1_ToArray_m7995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Capacity()
#define List_1_get_Capacity_m8233(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Capacity_m7997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m8234(__this, ___value, method) (( void (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_set_Capacity_m7999_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Count()
#define List_1_get_Count_m8235(__this, method) (( int32_t (*) (List_1_t142 *, const MethodInfo*))List_1_get_Count_m8001_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::get_Item(System.Int32)
#define List_1_get_Item_m8236(__this, ___index, method) (( PersistentCall_t140 * (*) (List_1_t142 *, int32_t, const MethodInfo*))List_1_get_Item_m8003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m8237(__this, ___index, ___value, method) (( void (*) (List_1_t142 *, int32_t, PersistentCall_t140 *, const MethodInfo*))List_1_set_Item_m8005_gshared)(__this, ___index, ___value, method)
