﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8768_gshared (InternalEnumerator_1_t1420 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8768(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1420 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8768_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8769_gshared (InternalEnumerator_1_t1420 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8769(__this, method) (( void (*) (InternalEnumerator_1_t1420 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8769_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8770_gshared (InternalEnumerator_1_t1420 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8770(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1420 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8770_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8771_gshared (InternalEnumerator_1_t1420 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8771(__this, method) (( void (*) (InternalEnumerator_1_t1420 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8771_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8772_gshared (InternalEnumerator_1_t1420 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8772(__this, method) (( bool (*) (InternalEnumerator_1_t1420 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8772_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern "C" int8_t InternalEnumerator_1_get_Current_m8773_gshared (InternalEnumerator_1_t1420 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8773(__this, method) (( int8_t (*) (InternalEnumerator_1_t1420 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8773_gshared)(__this, method)
