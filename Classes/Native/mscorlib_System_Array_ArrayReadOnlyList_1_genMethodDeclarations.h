﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t1430;
// System.Object[]
struct ObjectU5BU5D_t147;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1516;
// System.Exception
struct Exception_t134;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m8862_gshared (ArrayReadOnlyList_1_t1430 * __this, ObjectU5BU5D_t147* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m8862(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, ObjectU5BU5D_t147*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m8862_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m8863_gshared (ArrayReadOnlyList_1_t1430 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m8863(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1430 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m8863_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m8864_gshared (ArrayReadOnlyList_1_t1430 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m8864(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1430 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m8864_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m8865_gshared (ArrayReadOnlyList_1_t1430 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m8865(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m8865_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m8866_gshared (ArrayReadOnlyList_1_t1430 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m8866(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1430 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m8866_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m8867_gshared (ArrayReadOnlyList_1_t1430 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m8867(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1430 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m8867_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m8868_gshared (ArrayReadOnlyList_1_t1430 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m8868(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m8868_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m8869_gshared (ArrayReadOnlyList_1_t1430 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m8869(__this, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m8869_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m8870_gshared (ArrayReadOnlyList_1_t1430 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m8870(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1430 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m8870_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m8871_gshared (ArrayReadOnlyList_1_t1430 * __this, ObjectU5BU5D_t147* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m8871(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, ObjectU5BU5D_t147*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m8871_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m8872_gshared (ArrayReadOnlyList_1_t1430 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m8872(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1430 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m8872_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m8873_gshared (ArrayReadOnlyList_1_t1430 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m8873(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1430 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m8873_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m8874_gshared (ArrayReadOnlyList_1_t1430 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m8874(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m8874_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m8875_gshared (ArrayReadOnlyList_1_t1430 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m8875(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1430 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m8875_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m8876_gshared (ArrayReadOnlyList_1_t1430 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m8876(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1430 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m8876_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t134 * ArrayReadOnlyList_1_ReadOnlyError_m8877_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m8877(__this /* static, unused */, method) (( Exception_t134 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m8877_gshared)(__this /* static, unused */, method)
