﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"

// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m7876_gshared (InternalEnumerator_1_t1321 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m7876(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1321 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m7876_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878_gshared (InternalEnumerator_1_t1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878(__this, method) (( void (*) (InternalEnumerator_1_t1321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880_gshared (InternalEnumerator_1_t1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1321 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m7882_gshared (InternalEnumerator_1_t1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m7882(__this, method) (( void (*) (InternalEnumerator_1_t1321 *, const MethodInfo*))InternalEnumerator_1_Dispose_m7882_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m7884_gshared (InternalEnumerator_1_t1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m7884(__this, method) (( bool (*) (InternalEnumerator_1_t1321 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m7884_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern "C" Object_t * InternalEnumerator_1_get_Current_m7886_gshared (InternalEnumerator_1_t1321 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m7886(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1321 *, const MethodInfo*))InternalEnumerator_1_get_Current_m7886_gshared)(__this, method)
