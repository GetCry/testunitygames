﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t1311;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m7863_gshared (GenericEqualityComparer_1_t1311 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m7863(__this, method) (( void (*) (GenericEqualityComparer_1_t1311 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m7863_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m9503_gshared (GenericEqualityComparer_1_t1311 * __this, DateTime_t118  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m9503(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1311 *, DateTime_t118 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m9503_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m9504_gshared (GenericEqualityComparer_1_t1311 * __this, DateTime_t118  ___x, DateTime_t118  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m9504(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1311 *, DateTime_t118 , DateTime_t118 , const MethodInfo*))GenericEqualityComparer_1_Equals_m9504_gshared)(__this, ___x, ___y, method)
