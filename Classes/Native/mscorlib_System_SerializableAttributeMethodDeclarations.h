﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.SerializableAttribute
struct SerializableAttribute_t585;

#include "codegen/il2cpp-codegen.h"

// System.Void System.SerializableAttribute::.ctor()
extern "C" void SerializableAttribute__ctor_m2775 (SerializableAttribute_t585 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
