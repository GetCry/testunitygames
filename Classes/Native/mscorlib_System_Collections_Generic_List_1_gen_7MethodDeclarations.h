﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t1478;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1526;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void List_1__ctor_m9314_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1__ctor_m9314(__this, method) (( void (*) (List_1_t1478 *, const MethodInfo*))List_1__ctor_m9314_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m9315_gshared (List_1_t1478 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m9315(__this, ___capacity, method) (( void (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1__ctor_m9315_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::.cctor()
extern "C" void List_1__cctor_m9316_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m9316(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m9316_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9317_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9317(__this, method) (( Object_t* (*) (List_1_t1478 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m9318_gshared (List_1_t1478 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m9318(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1478 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m9318_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m9319_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9319(__this, method) (( Object_t * (*) (List_1_t1478 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m9319_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m9320_gshared (List_1_t1478 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m9320(__this, ___item, method) (( int32_t (*) (List_1_t1478 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m9320_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m9321_gshared (List_1_t1478 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m9321(__this, ___item, method) (( bool (*) (List_1_t1478 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m9321_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m9322_gshared (List_1_t1478 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m9322(__this, ___item, method) (( int32_t (*) (List_1_t1478 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m9322_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m9323_gshared (List_1_t1478 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m9323(__this, ___index, ___item, method) (( void (*) (List_1_t1478 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m9323_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m9324_gshared (List_1_t1478 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m9324(__this, ___item, method) (( void (*) (List_1_t1478 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m9324_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9325_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9325(__this, method) (( bool (*) (List_1_t1478 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9325_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m9326_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m9326(__this, method) (( Object_t * (*) (List_1_t1478 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m9326_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m9327_gshared (List_1_t1478 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m9327(__this, ___index, method) (( Object_t * (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m9327_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m9328_gshared (List_1_t1478 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m9328(__this, ___index, ___value, method) (( void (*) (List_1_t1478 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m9328_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void List_1_Add_m9329_gshared (List_1_t1478 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define List_1_Add_m9329(__this, ___item, method) (( void (*) (List_1_t1478 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_Add_m9329_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m9330_gshared (List_1_t1478 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m9330(__this, ___newCount, method) (( void (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m9330_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void List_1_Clear_m9331_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_Clear_m9331(__this, method) (( void (*) (List_1_t1478 *, const MethodInfo*))List_1_Clear_m9331_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool List_1_Contains_m9332_gshared (List_1_t1478 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define List_1_Contains_m9332(__this, ___item, method) (( bool (*) (List_1_t1478 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_Contains_m9332_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m9333_gshared (List_1_t1478 * __this, CustomAttributeNamedArgumentU5BU5D_t1279* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m9333(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1478 *, CustomAttributeNamedArgumentU5BU5D_t1279*, int32_t, const MethodInfo*))List_1_CopyTo_m9333_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Enumerator_t1479  List_1_GetEnumerator_m9334_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m9334(__this, method) (( Enumerator_t1479  (*) (List_1_t1478 *, const MethodInfo*))List_1_GetEnumerator_m9334_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m9335_gshared (List_1_t1478 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define List_1_IndexOf_m9335(__this, ___item, method) (( int32_t (*) (List_1_t1478 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_IndexOf_m9335_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m9336_gshared (List_1_t1478 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m9336(__this, ___start, ___delta, method) (( void (*) (List_1_t1478 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m9336_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m9337_gshared (List_1_t1478 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m9337(__this, ___index, method) (( void (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_CheckIndex_m9337_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m9338_gshared (List_1_t1478 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define List_1_Insert_m9338(__this, ___index, ___item, method) (( void (*) (List_1_t1478 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_Insert_m9338_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool List_1_Remove_m9339_gshared (List_1_t1478 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define List_1_Remove_m9339(__this, ___item, method) (( bool (*) (List_1_t1478 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_Remove_m9339_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m9340_gshared (List_1_t1478 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m9340(__this, ___index, method) (( void (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_RemoveAt_m9340_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::ToArray()
extern "C" CustomAttributeNamedArgumentU5BU5D_t1279* List_1_ToArray_m9341_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_ToArray_m9341(__this, method) (( CustomAttributeNamedArgumentU5BU5D_t1279* (*) (List_1_t1478 *, const MethodInfo*))List_1_ToArray_m9341_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m9342_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m9342(__this, method) (( int32_t (*) (List_1_t1478 *, const MethodInfo*))List_1_get_Capacity_m9342_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m9343_gshared (List_1_t1478 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m9343(__this, ___value, method) (( void (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_set_Capacity_m9343_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m9344_gshared (List_1_t1478 * __this, const MethodInfo* method);
#define List_1_get_Count_m9344(__this, method) (( int32_t (*) (List_1_t1478 *, const MethodInfo*))List_1_get_Count_m9344_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t837  List_1_get_Item_m9345_gshared (List_1_t1478 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m9345(__this, ___index, method) (( CustomAttributeNamedArgument_t837  (*) (List_1_t1478 *, int32_t, const MethodInfo*))List_1_get_Item_m9345_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m9346_gshared (List_1_t1478 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___value, const MethodInfo* method);
#define List_1_set_Item_m9346(__this, ___index, ___value, method) (( void (*) (List_1_t1478 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))List_1_set_Item_m9346_gshared)(__this, ___index, ___value, method)
