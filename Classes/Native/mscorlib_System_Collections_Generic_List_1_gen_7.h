﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct  List_1_t1478  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::_items
	CustomAttributeNamedArgumentU5BU5D_t1279* ____items_0;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::_version
	int32_t ____version_2;
};
struct List_1_t1478_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>::EmptyArray
	CustomAttributeNamedArgumentU5BU5D_t1279* ___EmptyArray_3;
};
