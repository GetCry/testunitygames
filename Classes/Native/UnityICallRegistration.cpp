//Generated on : 11/07/2015 00:51:52
void RegisterAllStrippedInternalCalls()
{
	//Start Registrations for type : UnityEngine.Advertisements.UnityAdsInternal

		//System.Boolean UnityEngine.Advertisements.UnityAdsInternal::CanShowAds(System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_CanShowAds();
		Register_UnityEngine_Advertisements_UnityAdsInternal_CanShowAds();

		//System.Boolean UnityEngine.Advertisements.UnityAdsInternal::Show(System.String,System.String,System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_Show();
		Register_UnityEngine_Advertisements_UnityAdsInternal_Show();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::Init(System.String,System.Boolean,System.Boolean,System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_Init();
		Register_UnityEngine_Advertisements_UnityAdsInternal_Init();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::RegisterNative()
		void Register_UnityEngine_Advertisements_UnityAdsInternal_RegisterNative();
		Register_UnityEngine_Advertisements_UnityAdsInternal_RegisterNative();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::SetCampaignDataURL(System.String)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_SetCampaignDataURL();
		Register_UnityEngine_Advertisements_UnityAdsInternal_SetCampaignDataURL();

		//System.Void UnityEngine.Advertisements.UnityAdsInternal::SetLogLevel(System.Int32)
		void Register_UnityEngine_Advertisements_UnityAdsInternal_SetLogLevel();
		Register_UnityEngine_Advertisements_UnityAdsInternal_SetLogLevel();

	//End Registrations for type : UnityEngine.Advertisements.UnityAdsInternal

	//Start Registrations for type : UnityEngine.AnimationCurve

		//System.Void UnityEngine.AnimationCurve::Cleanup()
		void Register_UnityEngine_AnimationCurve_Cleanup();
		Register_UnityEngine_AnimationCurve_Cleanup();

		//System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
		void Register_UnityEngine_AnimationCurve_Init();
		Register_UnityEngine_AnimationCurve_Init();

	//End Registrations for type : UnityEngine.AnimationCurve

	//Start Registrations for type : UnityEngine.AssetBundle

		//UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAsset_Internal();
		Register_UnityEngine_AssetBundle_LoadAsset_Internal();

		//UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
		void Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();
		Register_UnityEngine_AssetBundle_LoadAssetWithSubAssets_Internal();

	//End Registrations for type : UnityEngine.AssetBundle

	//Start Registrations for type : UnityEngine.AssetBundleCreateRequest

		//System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
		void Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();
		Register_UnityEngine_AssetBundleCreateRequest_DisableCompatibilityChecks();

		//UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
		void Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();
		Register_UnityEngine_AssetBundleCreateRequest_get_assetBundle();

	//End Registrations for type : UnityEngine.AssetBundleCreateRequest

	//Start Registrations for type : UnityEngine.AsyncOperation

		//System.Void UnityEngine.AsyncOperation::InternalDestroy()
		void Register_UnityEngine_AsyncOperation_InternalDestroy();
		Register_UnityEngine_AsyncOperation_InternalDestroy();

	//End Registrations for type : UnityEngine.AsyncOperation

	//Start Registrations for type : UnityEngine.Bounds

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_Contains(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_Contains();

		//System.Boolean UnityEngine.Bounds::INTERNAL_CALL_Internal_IntersectRay(UnityEngine.Ray&,UnityEngine.Bounds&,System.Single&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_IntersectRay();

		//System.Single UnityEngine.Bounds::INTERNAL_CALL_Internal_SqrDistance(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_SqrDistance();

		//UnityEngine.Vector3 UnityEngine.Bounds::INTERNAL_CALL_Internal_GetClosestPoint(UnityEngine.Bounds&,UnityEngine.Vector3&)
		void Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();
		Register_UnityEngine_Bounds_INTERNAL_CALL_Internal_GetClosestPoint();

	//End Registrations for type : UnityEngine.Bounds

	//Start Registrations for type : UnityEngine.Camera

		//System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
		void Register_UnityEngine_Camera_GetAllCameras();
		Register_UnityEngine_Camera_GetAllCameras();

		//System.Int32 UnityEngine.Camera::get_allCamerasCount()
		void Register_UnityEngine_Camera_get_allCamerasCount();
		Register_UnityEngine_Camera_get_allCamerasCount();

		//System.Int32 UnityEngine.Camera::get_cullingMask()
		void Register_UnityEngine_Camera_get_cullingMask();
		Register_UnityEngine_Camera_get_cullingMask();

		//System.Int32 UnityEngine.Camera::get_eventMask()
		void Register_UnityEngine_Camera_get_eventMask();
		Register_UnityEngine_Camera_get_eventMask();

		//System.Single UnityEngine.Camera::get_farClipPlane()
		void Register_UnityEngine_Camera_get_farClipPlane();
		Register_UnityEngine_Camera_get_farClipPlane();

		//System.Single UnityEngine.Camera::get_nearClipPlane()
		void Register_UnityEngine_Camera_get_nearClipPlane();
		Register_UnityEngine_Camera_get_nearClipPlane();

		//System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
		void Register_UnityEngine_Camera_INTERNAL_get_pixelRect();
		Register_UnityEngine_Camera_INTERNAL_get_pixelRect();

		//UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
		void Register_UnityEngine_Camera_get_clearFlags();
		Register_UnityEngine_Camera_get_clearFlags();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry();

		//UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
		void Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();
		Register_UnityEngine_Camera_INTERNAL_CALL_RaycastTry2D();

		//UnityEngine.Ray UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&)
		void Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();
		Register_UnityEngine_Camera_INTERNAL_CALL_ScreenPointToRay();

		//UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
		void Register_UnityEngine_Camera_get_targetTexture();
		Register_UnityEngine_Camera_get_targetTexture();

	//End Registrations for type : UnityEngine.Camera

	//Start Registrations for type : UnityEngine.Component

		//System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
		void Register_UnityEngine_Component_GetComponentFastPath();
		Register_UnityEngine_Component_GetComponentFastPath();

		//UnityEngine.GameObject UnityEngine.Component::get_gameObject()
		void Register_UnityEngine_Component_get_gameObject();
		Register_UnityEngine_Component_get_gameObject();

	//End Registrations for type : UnityEngine.Component

	//Start Registrations for type : UnityEngine.Coroutine

		//System.Void UnityEngine.Coroutine::ReleaseCoroutine()
		void Register_UnityEngine_Coroutine_ReleaseCoroutine();
		Register_UnityEngine_Coroutine_ReleaseCoroutine();

	//End Registrations for type : UnityEngine.Coroutine

	//Start Registrations for type : UnityEngine.CullingGroup

		//System.Void UnityEngine.CullingGroup::Dispose()
		void Register_UnityEngine_CullingGroup_Dispose();
		Register_UnityEngine_CullingGroup_Dispose();

		//System.Void UnityEngine.CullingGroup::FinalizerFailure()
		void Register_UnityEngine_CullingGroup_FinalizerFailure();
		Register_UnityEngine_CullingGroup_FinalizerFailure();

	//End Registrations for type : UnityEngine.CullingGroup

	//Start Registrations for type : UnityEngine.Debug

		//System.Void UnityEngine.Debug::Internal_Log(System.Int32,System.String,UnityEngine.Object)
		void Register_UnityEngine_Debug_Internal_Log();
		Register_UnityEngine_Debug_Internal_Log();

	//End Registrations for type : UnityEngine.Debug

	//Start Registrations for type : UnityEngine.Display

		//System.Boolean UnityEngine.Display::MultiDisplayLicenseImpl()
		void Register_UnityEngine_Display_MultiDisplayLicenseImpl();
		Register_UnityEngine_Display_MultiDisplayLicenseImpl();

		//System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_RelativeMouseAtImpl();
		Register_UnityEngine_Display_RelativeMouseAtImpl();

		//System.Void UnityEngine.Display::ActivateDisplayImpl(System.IntPtr,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_ActivateDisplayImpl();
		Register_UnityEngine_Display_ActivateDisplayImpl();

		//System.Void UnityEngine.Display::GetRenderingBuffersImpl(System.IntPtr,UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&)
		void Register_UnityEngine_Display_GetRenderingBuffersImpl();
		Register_UnityEngine_Display_GetRenderingBuffersImpl();

		//System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetRenderingExtImpl();
		Register_UnityEngine_Display_GetRenderingExtImpl();

		//System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
		void Register_UnityEngine_Display_GetSystemExtImpl();
		Register_UnityEngine_Display_GetSystemExtImpl();

		//System.Void UnityEngine.Display::SetParamsImpl(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetParamsImpl();
		Register_UnityEngine_Display_SetParamsImpl();

		//System.Void UnityEngine.Display::SetRenderingResolutionImpl(System.IntPtr,System.Int32,System.Int32)
		void Register_UnityEngine_Display_SetRenderingResolutionImpl();
		Register_UnityEngine_Display_SetRenderingResolutionImpl();

	//End Registrations for type : UnityEngine.Display

	//Start Registrations for type : UnityEngine.GameObject

		//System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
		void Register_UnityEngine_GameObject_SendMessage();
		Register_UnityEngine_GameObject_SendMessage();

	//End Registrations for type : UnityEngine.GameObject

	//Start Registrations for type : UnityEngine.Gradient

		//System.Void UnityEngine.Gradient::Cleanup()
		void Register_UnityEngine_Gradient_Cleanup();
		Register_UnityEngine_Gradient_Cleanup();

		//System.Void UnityEngine.Gradient::Init()
		void Register_UnityEngine_Gradient_Init();
		Register_UnityEngine_Gradient_Init();

	//End Registrations for type : UnityEngine.Gradient

	//Start Registrations for type : UnityEngine.GUILayer

		//UnityEngine.GUIElement UnityEngine.GUILayer::INTERNAL_CALL_HitTest(UnityEngine.GUILayer,UnityEngine.Vector3&)
		void Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();
		Register_UnityEngine_GUILayer_INTERNAL_CALL_HitTest();

	//End Registrations for type : UnityEngine.GUILayer

	//Start Registrations for type : UnityEngine.Input

		//System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
		void Register_UnityEngine_Input_GetMouseButton();
		Register_UnityEngine_Input_GetMouseButton();

		//System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
		void Register_UnityEngine_Input_GetMouseButtonDown();
		Register_UnityEngine_Input_GetMouseButtonDown();

		//System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
		void Register_UnityEngine_Input_INTERNAL_get_mousePosition();
		Register_UnityEngine_Input_INTERNAL_get_mousePosition();

	//End Registrations for type : UnityEngine.Input

	//Start Registrations for type : UnityEngine.LayerMask

		//System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
		void Register_UnityEngine_LayerMask_NameToLayer();
		Register_UnityEngine_LayerMask_NameToLayer();

		//System.String UnityEngine.LayerMask::LayerToName(System.Int32)
		void Register_UnityEngine_LayerMask_LayerToName();
		Register_UnityEngine_LayerMask_LayerToName();

	//End Registrations for type : UnityEngine.LayerMask

	//Start Registrations for type : UnityEngine.Matrix4x4

		//System.Boolean UnityEngine.Matrix4x4::INTERNAL_CALL_Invert(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Invert();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Invert();

		//System.Boolean UnityEngine.Matrix4x4::get_isIdentity()
		void Register_UnityEngine_Matrix4x4_get_isIdentity();
		Register_UnityEngine_Matrix4x4_get_isIdentity();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Inverse(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Inverse();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_TRS(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_TRS();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::INTERNAL_CALL_Transpose(UnityEngine.Matrix4x4&)
		void Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();
		Register_UnityEngine_Matrix4x4_INTERNAL_CALL_Transpose();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Matrix4x4_Ortho();
		Register_UnityEngine_Matrix4x4_Ortho();

		//UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Perspective(System.Single,System.Single,System.Single,System.Single)
		void Register_UnityEngine_Matrix4x4_Perspective();
		Register_UnityEngine_Matrix4x4_Perspective();

	//End Registrations for type : UnityEngine.Matrix4x4

	//Start Registrations for type : UnityEngine.Object

		//System.String UnityEngine.Object::ToString()
		void Register_UnityEngine_Object_ToString();
		Register_UnityEngine_Object_ToString();

	//End Registrations for type : UnityEngine.Object

	//Start Registrations for type : UnityEngine.Rendering.SphericalHarmonicsL2

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddAmbientLightInternal(UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal();

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_AddDirectionalLightInternal(UnityEngine.Vector3&,UnityEngine.Color&,UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal();

		//System.Void UnityEngine.Rendering.SphericalHarmonicsL2::INTERNAL_CALL_ClearInternal(UnityEngine.Rendering.SphericalHarmonicsL2&)
		void Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal();
		Register_UnityEngine_Rendering_SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal();

	//End Registrations for type : UnityEngine.Rendering.SphericalHarmonicsL2

	//Start Registrations for type : UnityEngine.Resources

		//UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
		void Register_UnityEngine_Resources_Load();
		Register_UnityEngine_Resources_Load();

	//End Registrations for type : UnityEngine.Resources

	//Start Registrations for type : UnityEngine.ScriptableObject

		//System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
		void Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();
		Register_UnityEngine_ScriptableObject_Internal_CreateScriptableObject();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
		void Register_UnityEngine_ScriptableObject_CreateInstance();
		Register_UnityEngine_ScriptableObject_CreateInstance();

		//UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
		void Register_UnityEngine_ScriptableObject_CreateInstanceFromType();
		Register_UnityEngine_ScriptableObject_CreateInstanceFromType();

	//End Registrations for type : UnityEngine.ScriptableObject

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticated()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticated();

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Underage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Underage();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserID()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserID();

		//System.String UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserName()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserName();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_Authenticate()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_Authenticate();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievementDescriptions()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievementDescriptions();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadFriends()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadFriends();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadScores(System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_LoadUsers(System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_LoadUsers();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportProgress(System.String,System.Double)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportProgress();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ReportScore(System.Int64,System.String)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ReportScore();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ResetAllAchievements()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ResetAllAchievements();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowAchievementsUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowAchievementsUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowDefaultAchievementBanner(System.Boolean)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowDefaultAchievementBanner();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowLeaderboardUI()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowLeaderboardUI();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_ShowSpecificLeaderboardUI(System.String,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_ShowSpecificLeaderboardUI();

		//UnityEngine.Texture2D UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform::Internal_UserImage()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();
		Register_UnityEngine_SocialPlatforms_GameCenter_GameCenterPlatform_Internal_UserImage();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform

	//Start Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

		//System.Boolean UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Loading()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Loading();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Dispose()
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Dispose();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScores(System.String,System.Int32,System.Int32,System.Int32,System.Int32)
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScores();

		//System.Void UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard::Internal_LoadScoresWithUsers(System.String,System.Int32,System.String[])
		void Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();
		Register_UnityEngine_SocialPlatforms_GameCenter_GcLeaderboard_Internal_LoadScoresWithUsers();

	//End Registrations for type : UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard

	//Start Registrations for type : UnityEngine.Texture2D

		//System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
		void Register_UnityEngine_Texture2D_Internal_Create();
		Register_UnityEngine_Texture2D_Internal_Create();

	//End Registrations for type : UnityEngine.Texture2D

	//Start Registrations for type : UnityEngine.Transform

		//System.Int32 UnityEngine.Transform::get_childCount()
		void Register_UnityEngine_Transform_get_childCount();
		Register_UnityEngine_Transform_get_childCount();

		//UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
		void Register_UnityEngine_Transform_GetChild();
		Register_UnityEngine_Transform_GetChild();

	//End Registrations for type : UnityEngine.Transform

	//Start Registrations for type : UnityEngine.UnhandledExceptionHandler

		//System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler()
		void Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();
		Register_UnityEngine_UnhandledExceptionHandler_NativeUnhandledExceptionHandler();

	//End Registrations for type : UnityEngine.UnhandledExceptionHandler

}
