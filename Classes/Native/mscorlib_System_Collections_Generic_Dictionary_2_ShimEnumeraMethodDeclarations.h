﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>
struct ShimEnumerator_t1382;
// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m8435_gshared (ShimEnumerator_t1382 * __this, Dictionary_2_t1371 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m8435(__this, ___host, method) (( void (*) (ShimEnumerator_t1382 *, Dictionary_2_t1371 *, const MethodInfo*))ShimEnumerator__ctor_m8435_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m8436_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m8436(__this, method) (( bool (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_MoveNext_m8436_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Entry()
extern "C" DictionaryEntry_t376  ShimEnumerator_get_Entry_m8437_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m8437(__this, method) (( DictionaryEntry_t376  (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_get_Entry_m8437_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m8438_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m8438(__this, method) (( Object_t * (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_get_Key_m8438_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m8439_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m8439(__this, method) (( Object_t * (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_get_Value_m8439_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m8440_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m8440(__this, method) (( Object_t * (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_get_Current_m8440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Boolean>::Reset()
extern "C" void ShimEnumerator_Reset_m8441_gshared (ShimEnumerator_t1382 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m8441(__this, method) (( void (*) (ShimEnumerator_t1382 *, const MethodInfo*))ShimEnumerator_Reset_m8441_gshared)(__this, method)
