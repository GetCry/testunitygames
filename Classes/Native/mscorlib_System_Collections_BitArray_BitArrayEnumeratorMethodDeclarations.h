﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.BitArray/BitArrayEnumerator
struct BitArrayEnumerator_t701;
// System.Collections.BitArray
struct BitArray_t358;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.BitArray/BitArrayEnumerator::.ctor(System.Collections.BitArray)
extern "C" void BitArrayEnumerator__ctor_m4231 (BitArrayEnumerator_t701 * __this, BitArray_t358 * ___ba, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray/BitArrayEnumerator::Clone()
extern "C" Object_t * BitArrayEnumerator_Clone_m4232 (BitArrayEnumerator_t701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray/BitArrayEnumerator::get_Current()
extern "C" Object_t * BitArrayEnumerator_get_Current_m4233 (BitArrayEnumerator_t701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray/BitArrayEnumerator::MoveNext()
extern "C" bool BitArrayEnumerator_MoveNext_m4234 (BitArrayEnumerator_t701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray/BitArrayEnumerator::Reset()
extern "C" void BitArrayEnumerator_Reset_m4235 (BitArrayEnumerator_t701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray/BitArrayEnumerator::checkVersion()
extern "C" void BitArrayEnumerator_checkVersion_m4236 (BitArrayEnumerator_t701 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
