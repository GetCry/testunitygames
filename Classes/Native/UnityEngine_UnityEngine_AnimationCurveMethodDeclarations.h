﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationCurve
struct AnimationCurve_t94;
struct AnimationCurve_t94_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t162;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m451 (AnimationCurve_t94 * __this, KeyframeU5BU5D_t162* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m452 (AnimationCurve_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m453 (AnimationCurve_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m454 (AnimationCurve_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m455 (AnimationCurve_t94 * __this, KeyframeU5BU5D_t162* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t94_marshal(const AnimationCurve_t94& unmarshaled, AnimationCurve_t94_marshaled& marshaled);
extern "C" void AnimationCurve_t94_marshal_back(const AnimationCurve_t94_marshaled& marshaled, AnimationCurve_t94& unmarshaled);
extern "C" void AnimationCurve_t94_marshal_cleanup(AnimationCurve_t94_marshaled& marshaled);
