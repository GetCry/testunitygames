﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable/KeyMarker
struct KeyMarker_t705;

#include "mscorlib_System_Object.h"

// System.Collections.Hashtable/KeyMarker
struct  KeyMarker_t705  : public Object_t
{
};
struct KeyMarker_t705_StaticFields{
	// System.Collections.Hashtable/KeyMarker System.Collections.Hashtable/KeyMarker::Removed
	KeyMarker_t705 * ___Removed_0;
};
