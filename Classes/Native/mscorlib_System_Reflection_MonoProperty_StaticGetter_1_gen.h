﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t41;
// System.AsyncCallback
struct AsyncCallback_t42;

#include "mscorlib_System_MulticastDelegate.h"

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct  StaticGetter_1_t1485  : public MulticastDelegate_t40
{
};
