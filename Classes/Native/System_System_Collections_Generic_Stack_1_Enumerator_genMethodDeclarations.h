﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t1350;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.Stack`1<T>)
extern "C" void Enumerator__ctor_m8132_gshared (Enumerator_t1351 * __this, Stack_1_t1350 * ___t, const MethodInfo* method);
#define Enumerator__ctor_m8132(__this, ___t, method) (( void (*) (Enumerator_t1351 *, Stack_1_t1350 *, const MethodInfo*))Enumerator__ctor_m8132_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m8133_gshared (Enumerator_t1351 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m8133(__this, method) (( void (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8133_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m8134_gshared (Enumerator_t1351 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m8134(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8134_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m8135_gshared (Enumerator_t1351 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m8135(__this, method) (( void (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_Dispose_m8135_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m8136_gshared (Enumerator_t1351 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m8136(__this, method) (( bool (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_MoveNext_m8136_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m8137_gshared (Enumerator_t1351 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m8137(__this, method) (( Object_t * (*) (Enumerator_t1351 *, const MethodInfo*))Enumerator_get_Current_m8137_gshared)(__this, method)
