﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m8138(__this, ___t, method) (( void (*) (Enumerator_t1352 *, Stack_1_t182 *, const MethodInfo*))Enumerator__ctor_m8132_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8139(__this, method) (( void (*) (Enumerator_t1352 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8133_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8140(__this, method) (( Object_t * (*) (Enumerator_t1352 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8134_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m8141(__this, method) (( void (*) (Enumerator_t1352 *, const MethodInfo*))Enumerator_Dispose_m8135_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m8142(__this, method) (( bool (*) (Enumerator_t1352 *, const MethodInfo*))Enumerator_MoveNext_m8136_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m8143(__this, method) (( Type_t * (*) (Enumerator_t1352 *, const MethodInfo*))Enumerator_get_Current_m8137_gshared)(__this, method)
