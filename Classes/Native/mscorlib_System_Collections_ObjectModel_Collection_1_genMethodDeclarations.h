﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t1429;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t147;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1516;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1428;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m8831_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1__ctor_m8831(__this, method) (( void (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1__ctor_m8831_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8832_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8832(__this, method) (( bool (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8832_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m8833_gshared (Collection_1_t1429 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m8833(__this, ___array, ___index, method) (( void (*) (Collection_1_t1429 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m8833_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m8834_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m8834(__this, method) (( Object_t * (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m8834_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m8835_gshared (Collection_1_t1429 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m8835(__this, ___value, method) (( int32_t (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m8835_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m8836_gshared (Collection_1_t1429 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m8836(__this, ___value, method) (( bool (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m8836_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m8837_gshared (Collection_1_t1429 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m8837(__this, ___value, method) (( int32_t (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m8837_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m8838_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m8838(__this, ___index, ___value, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m8838_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m8839_gshared (Collection_1_t1429 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m8839(__this, ___value, method) (( void (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m8839_gshared)(__this, ___value, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m8840_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m8840(__this, method) (( Object_t * (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m8840_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m8841_gshared (Collection_1_t1429 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m8841(__this, ___index, method) (( Object_t * (*) (Collection_1_t1429 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m8841_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m8842_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m8842(__this, ___index, ___value, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m8842_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m8843_gshared (Collection_1_t1429 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m8843(__this, ___item, method) (( void (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_Add_m8843_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m8844_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_Clear_m8844(__this, method) (( void (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_Clear_m8844_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m8845_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m8845(__this, method) (( void (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_ClearItems_m8845_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m8846_gshared (Collection_1_t1429 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m8846(__this, ___item, method) (( bool (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_Contains_m8846_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m8847_gshared (Collection_1_t1429 * __this, ObjectU5BU5D_t147* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m8847(__this, ___array, ___index, method) (( void (*) (Collection_1_t1429 *, ObjectU5BU5D_t147*, int32_t, const MethodInfo*))Collection_1_CopyTo_m8847_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m8848_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m8848(__this, method) (( Object_t* (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_GetEnumerator_m8848_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m8849_gshared (Collection_1_t1429 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m8849(__this, ___item, method) (( int32_t (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m8849_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m8850_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m8850(__this, ___index, ___item, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m8850_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m8851_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m8851(__this, ___index, ___item, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m8851_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m8852_gshared (Collection_1_t1429 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m8852(__this, ___item, method) (( bool (*) (Collection_1_t1429 *, Object_t *, const MethodInfo*))Collection_1_Remove_m8852_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m8853_gshared (Collection_1_t1429 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m8853(__this, ___index, method) (( void (*) (Collection_1_t1429 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m8853_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m8854_gshared (Collection_1_t1429 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m8854(__this, ___index, method) (( void (*) (Collection_1_t1429 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m8854_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m8855_gshared (Collection_1_t1429 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m8855(__this, method) (( int32_t (*) (Collection_1_t1429 *, const MethodInfo*))Collection_1_get_Count_m8855_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m8856_gshared (Collection_1_t1429 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m8856(__this, ___index, method) (( Object_t * (*) (Collection_1_t1429 *, int32_t, const MethodInfo*))Collection_1_get_Item_m8856_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m8857_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m8857(__this, ___index, ___value, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m8857_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m8858_gshared (Collection_1_t1429 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m8858(__this, ___index, ___item, method) (( void (*) (Collection_1_t1429 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m8858_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m8859_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m8859(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m8859_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m8860_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m8860(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m8860_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m8861_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m8861(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m8861_gshared)(__this /* static, unused */, ___list, method)
