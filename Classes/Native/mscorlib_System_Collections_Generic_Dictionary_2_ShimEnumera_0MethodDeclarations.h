﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>
struct ShimEnumerator_t1395;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1389;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m8590_gshared (ShimEnumerator_t1395 * __this, Dictionary_2_t1389 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m8590(__this, ___host, method) (( void (*) (ShimEnumerator_t1395 *, Dictionary_2_t1389 *, const MethodInfo*))ShimEnumerator__ctor_m8590_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m8591_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m8591(__this, method) (( bool (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_MoveNext_m8591_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t376  ShimEnumerator_get_Entry_m8592_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m8592(__this, method) (( DictionaryEntry_t376  (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_get_Entry_m8592_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m8593_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m8593(__this, method) (( Object_t * (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_get_Key_m8593_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m8594_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m8594(__this, method) (( Object_t * (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_get_Value_m8594_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m8595_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m8595(__this, method) (( Object_t * (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_get_Current_m8595_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int32>::Reset()
extern "C" void ShimEnumerator_Reset_m8596_gshared (ShimEnumerator_t1395 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m8596(__this, method) (( void (*) (ShimEnumerator_t1395 *, const MethodInfo*))ShimEnumerator_Reset_m8596_gshared)(__this, method)
