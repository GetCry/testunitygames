﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_StateChanged_t39 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t58 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t69 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t72 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t75 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t86 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t88 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t90 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t82 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t294 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t336 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t228 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t371 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t473 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t552 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t530 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t531 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t514 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t515 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t595 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t42 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t659 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t667 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t755 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t756 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t840 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t855 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1034 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1221 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t598 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t847 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1222 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1223 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1225 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1144 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1226 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1153 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1150 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t778 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1151 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t170 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[41] = 
{
	pinvoke_delegate_wrapper_StateChanged_t39,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t58,
	pinvoke_delegate_wrapper_LogCallback_t69,
	pinvoke_delegate_wrapper_CameraCallback_t72,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t75,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t86,
	pinvoke_delegate_wrapper_PCMReaderCallback_t88,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t90,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t82,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t294,
	pinvoke_delegate_wrapper_CostDelegate_t336,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t228,
	pinvoke_delegate_wrapper_MatchEvaluator_t371,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t473,
	pinvoke_delegate_wrapper_PrimalityTest_t552,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t530,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t531,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t514,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t515,
	pinvoke_delegate_wrapper_Swapper_t595,
	pinvoke_delegate_wrapper_AsyncCallback_t42,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t659,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t667,
	pinvoke_delegate_wrapper_ReadDelegate_t755,
	pinvoke_delegate_wrapper_WriteDelegate_t756,
	pinvoke_delegate_wrapper_AddEventAdapter_t840,
	pinvoke_delegate_wrapper_GetterAdapter_t855,
	pinvoke_delegate_wrapper_CallbackHandler_t1034,
	pinvoke_delegate_wrapper_PrimalityTest_t1221,
	pinvoke_delegate_wrapper_MemberFilter_t598,
	pinvoke_delegate_wrapper_TypeFilter_t847,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1222,
	pinvoke_delegate_wrapper_HeaderHandler_t1223,
	pinvoke_delegate_wrapper_ThreadStart_t1225,
	pinvoke_delegate_wrapper_TimerCallback_t1144,
	pinvoke_delegate_wrapper_WaitCallback_t1226,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1153,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1150,
	pinvoke_delegate_wrapper_EventHandler_t778,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1151,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t170,
};
