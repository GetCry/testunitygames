﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_83.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9427_gshared (InternalEnumerator_1_t1492 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9427(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1492 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9427_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9428_gshared (InternalEnumerator_1_t1492 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9428(__this, method) (( void (*) (InternalEnumerator_1_t1492 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9428_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9429_gshared (InternalEnumerator_1_t1492 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9429(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1492 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9429_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9430_gshared (InternalEnumerator_1_t1492 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9430(__this, method) (( void (*) (InternalEnumerator_1_t1492 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9430_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9431_gshared (InternalEnumerator_1_t1492 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9431(__this, method) (( bool (*) (InternalEnumerator_1_t1492 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9431_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t118  InternalEnumerator_1_get_Current_m9432_gshared (InternalEnumerator_1_t1492 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9432(__this, method) (( DateTime_t118  (*) (InternalEnumerator_1_t1492 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9432_gshared)(__this, method)
