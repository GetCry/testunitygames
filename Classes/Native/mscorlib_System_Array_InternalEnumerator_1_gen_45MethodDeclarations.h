﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_45.h"

// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8756_gshared (InternalEnumerator_1_t1418 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8756(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1418 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8756_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8757_gshared (InternalEnumerator_1_t1418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8757(__this, method) (( void (*) (InternalEnumerator_1_t1418 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8757_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8758_gshared (InternalEnumerator_1_t1418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8758(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1418 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8758_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8759_gshared (InternalEnumerator_1_t1418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8759(__this, method) (( void (*) (InternalEnumerator_1_t1418 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8759_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8760_gshared (InternalEnumerator_1_t1418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8760(__this, method) (( bool (*) (InternalEnumerator_1_t1418 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8760_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern "C" uint64_t InternalEnumerator_1_get_Current_m8761_gshared (InternalEnumerator_1_t1418 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8761(__this, method) (( uint64_t (*) (InternalEnumerator_1_t1418 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8761_gshared)(__this, method)
