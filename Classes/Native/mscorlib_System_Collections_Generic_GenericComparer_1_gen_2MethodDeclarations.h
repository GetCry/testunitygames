﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t1317;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m7872_gshared (GenericComparer_1_t1317 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m7872(__this, method) (( void (*) (GenericComparer_1_t1317 *, const MethodInfo*))GenericComparer_1__ctor_m7872_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m9584_gshared (GenericComparer_1_t1317 * __this, TimeSpan_t278  ___x, TimeSpan_t278  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m9584(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1317 *, TimeSpan_t278 , TimeSpan_t278 , const MethodInfo*))GenericComparer_1_Compare_m9584_gshared)(__this, ___x, ___y, method)
