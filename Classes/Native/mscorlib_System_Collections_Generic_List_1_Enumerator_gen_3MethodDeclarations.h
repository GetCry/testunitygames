﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m8277(__this, ___l, method) (( void (*) (Enumerator_t1361 *, List_1_t144 *, const MethodInfo*))Enumerator__ctor_m8006_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8278(__this, method) (( void (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8007_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8279(__this, method) (( Object_t * (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8008_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::Dispose()
#define Enumerator_Dispose_m8280(__this, method) (( void (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_Dispose_m8009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::VerifyState()
#define Enumerator_VerifyState_m8281(__this, method) (( void (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_VerifyState_m8010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::MoveNext()
#define Enumerator_MoveNext_m8282(__this, method) (( bool (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_MoveNext_m8011_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::get_Current()
#define Enumerator_get_Current_m8283(__this, method) (( BaseInvokableCall_t138 * (*) (Enumerator_t1361 *, const MethodInfo*))Enumerator_get_Current_m8012_gshared)(__this, method)
