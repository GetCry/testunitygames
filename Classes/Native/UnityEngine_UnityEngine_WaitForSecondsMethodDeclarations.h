﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t11;
struct WaitForSeconds_t11_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m13 (WaitForSeconds_t11 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void WaitForSeconds_t11_marshal(const WaitForSeconds_t11& unmarshaled, WaitForSeconds_t11_marshaled& marshaled);
extern "C" void WaitForSeconds_t11_marshal_back(const WaitForSeconds_t11_marshaled& marshaled, WaitForSeconds_t11& unmarshaled);
extern "C" void WaitForSeconds_t11_marshal_cleanup(WaitForSeconds_t11_marshaled& marshaled);
