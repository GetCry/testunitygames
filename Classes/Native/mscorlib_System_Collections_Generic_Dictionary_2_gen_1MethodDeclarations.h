﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1371;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1373;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t165;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1518;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t1519;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t375;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m8299_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m8299(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2__ctor_m8299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m8300_gshared (Dictionary_2_t1371 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m8300(__this, ___comparer, method) (( void (*) (Dictionary_2_t1371 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m8300_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m8302_gshared (Dictionary_2_t1371 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m8302(__this, ___capacity, method) (( void (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m8302_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m8304_gshared (Dictionary_2_t1371 * __this, SerializationInfo_t165 * ___info, StreamingContext_t166  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m8304(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1371 *, SerializationInfo_t165 *, StreamingContext_t166 , const MethodInfo*))Dictionary_2__ctor_m8304_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m8306_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m8306(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m8306_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m8308_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m8308(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m8308_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m8310_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m8310(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m8310_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m8312_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m8312(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m8312_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m8314_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m8314(__this, ___key, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m8314_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8316_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8316(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8316_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8318_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8318(__this, method) (( bool (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8318_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8320_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t1374  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8320(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2_t1374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8320_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8322_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t1374  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8322(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t1374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8322_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8324_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2U5BU5D_t1518* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8324(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2U5BU5D_t1518*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8324_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8326_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t1374  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8326(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t1374 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8326_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m8328_gshared (Dictionary_2_t1371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m8328(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m8328_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8330_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8330(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8330_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8332_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8332(__this, method) (( Object_t* (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8332_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8334_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8334(__this, method) (( Object_t * (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8334_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m8336_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m8336(__this, method) (( int32_t (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_get_Count_m8336_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m8338_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m8338(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m8338_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m8340_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m8340(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m8340_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m8342_gshared (Dictionary_2_t1371 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m8342(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1371 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m8342_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m8344_gshared (Dictionary_2_t1371 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m8344(__this, ___size, method) (( void (*) (Dictionary_2_t1371 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m8344_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m8346_gshared (Dictionary_2_t1371 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m8346(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m8346_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1374  Dictionary_2_make_pair_m8348_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m8348(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1374  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m8348_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m8350_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2U5BU5D_t1518* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m8350(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1371 *, KeyValuePair_2U5BU5D_t1518*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m8350_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m8352_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m8352(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_Resize_m8352_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m8354_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m8354(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m8354_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m8356_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m8356(__this, method) (( void (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_Clear_m8356_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m8358_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m8358(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m8358_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m8360_gshared (Dictionary_2_t1371 * __this, SerializationInfo_t165 * ___info, StreamingContext_t166  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m8360(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1371 *, SerializationInfo_t165 *, StreamingContext_t166 , const MethodInfo*))Dictionary_2_GetObjectData_m8360_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m8362_gshared (Dictionary_2_t1371 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m8362(__this, ___sender, method) (( void (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m8362_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m8364_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m8364(__this, ___key, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m8364_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m8366_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m8366(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m8366_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m8368_gshared (Dictionary_2_t1371 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m8368(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m8368_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m8370_gshared (Dictionary_2_t1371 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m8370(__this, ___value, method) (( bool (*) (Dictionary_2_t1371 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m8370_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m8372_gshared (Dictionary_2_t1371 * __this, KeyValuePair_2_t1374  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m8372(__this, ___pair, method) (( bool (*) (Dictionary_2_t1371 *, KeyValuePair_2_t1374 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m8372_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t1381  Dictionary_2_GetEnumerator_m8374_gshared (Dictionary_2_t1371 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m8374(__this, method) (( Enumerator_t1381  (*) (Dictionary_2_t1371 *, const MethodInfo*))Dictionary_2_GetEnumerator_m8374_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t376  Dictionary_2_U3CCopyToU3Em__0_m8376_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m8376(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t376  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m8376_gshared)(__this /* static, unused */, ___key, ___value, method)
