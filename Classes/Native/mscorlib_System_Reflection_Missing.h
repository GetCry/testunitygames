﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Missing
struct Missing_t846;

#include "mscorlib_System_Object.h"

// System.Reflection.Missing
struct  Missing_t846  : public Object_t
{
};
struct Missing_t846_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t846 * ___Value_0;
};
