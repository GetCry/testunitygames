﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t1500;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::.ctor()
extern "C" void DefaultComparer__ctor_m9501_gshared (DefaultComparer_t1500 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m9501(__this, method) (( void (*) (DefaultComparer_t1500 *, const MethodInfo*))DefaultComparer__ctor_m9501_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTime>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m9502_gshared (DefaultComparer_t1500 * __this, DateTime_t118  ___x, DateTime_t118  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m9502(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1500 *, DateTime_t118 , DateTime_t118 , const MethodInfo*))DefaultComparer_Compare_m9502_gshared)(__this, ___x, ___y, method)
