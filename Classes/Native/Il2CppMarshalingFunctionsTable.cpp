﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void WaitForSeconds_t11_marshal ();
extern "C" void WaitForSeconds_t11_marshal_back ();
extern "C" void WaitForSeconds_t11_marshal_cleanup ();
extern "C" void Coroutine_t15_marshal ();
extern "C" void Coroutine_t15_marshal_back ();
extern "C" void Coroutine_t15_marshal_cleanup ();
extern "C" void ScriptableObject_t16_marshal ();
extern "C" void ScriptableObject_t16_marshal_back ();
extern "C" void ScriptableObject_t16_marshal_cleanup ();
extern "C" void Gradient_t47_marshal ();
extern "C" void Gradient_t47_marshal_back ();
extern "C" void Gradient_t47_marshal_cleanup ();
extern "C" void CacheIndex_t67_marshal ();
extern "C" void CacheIndex_t67_marshal_back ();
extern "C" void CacheIndex_t67_marshal_cleanup ();
extern "C" void AsyncOperation_t5_marshal ();
extern "C" void AsyncOperation_t5_marshal_back ();
extern "C" void AsyncOperation_t5_marshal_cleanup ();
extern "C" void Object_t8_marshal ();
extern "C" void Object_t8_marshal_back ();
extern "C" void Object_t8_marshal_cleanup ();
extern "C" void YieldInstruction_t12_marshal ();
extern "C" void YieldInstruction_t12_marshal_back ();
extern "C" void YieldInstruction_t12_marshal_cleanup ();
extern "C" void WebCamDevice_t92_marshal ();
extern "C" void WebCamDevice_t92_marshal_back ();
extern "C" void WebCamDevice_t92_marshal_cleanup ();
extern "C" void AnimationCurve_t94_marshal ();
extern "C" void AnimationCurve_t94_marshal_back ();
extern "C" void AnimationCurve_t94_marshal_cleanup ();
extern "C" void GcAchievementData_t108_marshal ();
extern "C" void GcAchievementData_t108_marshal_back ();
extern "C" void GcAchievementData_t108_marshal_cleanup ();
extern "C" void GcScoreData_t109_marshal ();
extern "C" void GcScoreData_t109_marshal_back ();
extern "C" void GcScoreData_t109_marshal_cleanup ();
extern "C" void X509ChainStatus_t275_marshal ();
extern "C" void X509ChainStatus_t275_marshal_back ();
extern "C" void X509ChainStatus_t275_marshal_cleanup ();
extern "C" void IntStack_t327_marshal ();
extern "C" void IntStack_t327_marshal_back ();
extern "C" void IntStack_t327_marshal_cleanup ();
extern "C" void Interval_t333_marshal ();
extern "C" void Interval_t333_marshal_back ();
extern "C" void Interval_t333_marshal_cleanup ();
extern "C" void UriScheme_t363_marshal ();
extern "C" void UriScheme_t363_marshal_back ();
extern "C" void UriScheme_t363_marshal_cleanup ();
extern "C" void Context_t638_marshal ();
extern "C" void Context_t638_marshal_back ();
extern "C" void Context_t638_marshal_cleanup ();
extern "C" void PreviousInfo_t639_marshal ();
extern "C" void PreviousInfo_t639_marshal_back ();
extern "C" void PreviousInfo_t639_marshal_cleanup ();
extern "C" void Escape_t640_marshal ();
extern "C" void Escape_t640_marshal_back ();
extern "C" void Escape_t640_marshal_cleanup ();
extern "C" void MonoIOStat_t758_marshal ();
extern "C" void MonoIOStat_t758_marshal_back ();
extern "C" void MonoIOStat_t758_marshal_cleanup ();
extern "C" void ParameterModifier_t857_marshal ();
extern "C" void ParameterModifier_t857_marshal_back ();
extern "C" void ParameterModifier_t857_marshal_cleanup ();
extern "C" void ResourceInfo_t868_marshal ();
extern "C" void ResourceInfo_t868_marshal_back ();
extern "C" void ResourceInfo_t868_marshal_cleanup ();
extern "C" void DSAParameters_t407_marshal ();
extern "C" void DSAParameters_t407_marshal_back ();
extern "C" void DSAParameters_t407_marshal_cleanup ();
extern "C" void RSAParameters_t405_marshal ();
extern "C" void RSAParameters_t405_marshal_back ();
extern "C" void RSAParameters_t405_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[25] = 
{
	{ WaitForSeconds_t11_marshal, WaitForSeconds_t11_marshal_back, WaitForSeconds_t11_marshal_cleanup },
	{ Coroutine_t15_marshal, Coroutine_t15_marshal_back, Coroutine_t15_marshal_cleanup },
	{ ScriptableObject_t16_marshal, ScriptableObject_t16_marshal_back, ScriptableObject_t16_marshal_cleanup },
	{ Gradient_t47_marshal, Gradient_t47_marshal_back, Gradient_t47_marshal_cleanup },
	{ CacheIndex_t67_marshal, CacheIndex_t67_marshal_back, CacheIndex_t67_marshal_cleanup },
	{ AsyncOperation_t5_marshal, AsyncOperation_t5_marshal_back, AsyncOperation_t5_marshal_cleanup },
	{ Object_t8_marshal, Object_t8_marshal_back, Object_t8_marshal_cleanup },
	{ YieldInstruction_t12_marshal, YieldInstruction_t12_marshal_back, YieldInstruction_t12_marshal_cleanup },
	{ WebCamDevice_t92_marshal, WebCamDevice_t92_marshal_back, WebCamDevice_t92_marshal_cleanup },
	{ AnimationCurve_t94_marshal, AnimationCurve_t94_marshal_back, AnimationCurve_t94_marshal_cleanup },
	{ GcAchievementData_t108_marshal, GcAchievementData_t108_marshal_back, GcAchievementData_t108_marshal_cleanup },
	{ GcScoreData_t109_marshal, GcScoreData_t109_marshal_back, GcScoreData_t109_marshal_cleanup },
	{ X509ChainStatus_t275_marshal, X509ChainStatus_t275_marshal_back, X509ChainStatus_t275_marshal_cleanup },
	{ IntStack_t327_marshal, IntStack_t327_marshal_back, IntStack_t327_marshal_cleanup },
	{ Interval_t333_marshal, Interval_t333_marshal_back, Interval_t333_marshal_cleanup },
	{ UriScheme_t363_marshal, UriScheme_t363_marshal_back, UriScheme_t363_marshal_cleanup },
	{ Context_t638_marshal, Context_t638_marshal_back, Context_t638_marshal_cleanup },
	{ PreviousInfo_t639_marshal, PreviousInfo_t639_marshal_back, PreviousInfo_t639_marshal_cleanup },
	{ Escape_t640_marshal, Escape_t640_marshal_back, Escape_t640_marshal_cleanup },
	{ MonoIOStat_t758_marshal, MonoIOStat_t758_marshal_back, MonoIOStat_t758_marshal_cleanup },
	{ ParameterModifier_t857_marshal, ParameterModifier_t857_marshal_back, ParameterModifier_t857_marshal_cleanup },
	{ ResourceInfo_t868_marshal, ResourceInfo_t868_marshal_back, ResourceInfo_t868_marshal_cleanup },
	{ DSAParameters_t407_marshal, DSAParameters_t407_marshal_back, DSAParameters_t407_marshal_cleanup },
	{ RSAParameters_t405_marshal, RSAParameters_t405_marshal_back, RSAParameters_t405_marshal_cleanup },
	NULL,
};
