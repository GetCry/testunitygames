﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IO.TextReader
struct TextReader_t690;

#include "mscorlib_System_Object.h"

// System.IO.TextReader
struct  TextReader_t690  : public Object_t
{
};
struct TextReader_t690_StaticFields{
	// System.IO.TextReader System.IO.TextReader::Null
	TextReader_t690 * ___Null_0;
};
