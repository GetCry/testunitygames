﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// System.Object[]
// System.Object[]
struct ObjectU5BU5D_t147  : public Array_t { };
// System.Type[]
// System.Type[]
struct TypeU5BU5D_t163  : public Array_t { };
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct IReflectU5BU5D_t1529  : public Array_t { };
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct _TypeU5BU5D_t1530  : public Array_t { };
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct MemberInfoU5BU5D_t1013  : public Array_t { };
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct ICustomAttributeProviderU5BU5D_t1531  : public Array_t { };
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct _MemberInfoU5BU5D_t1532  : public Array_t { };
// System.String[]
// System.String[]
struct StringU5BU5D_t122  : public Array_t { };
// System.IConvertible[]
// System.IConvertible[]
struct IConvertibleU5BU5D_t1533  : public Array_t { };
// System.IComparable[]
// System.IComparable[]
struct IComparableU5BU5D_t1534  : public Array_t { };
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct IEnumerableU5BU5D_t1535  : public Array_t { };
// System.ICloneable[]
// System.ICloneable[]
struct ICloneableU5BU5D_t1536  : public Array_t { };
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct IComparable_1U5BU5D_t1537  : public Array_t { };
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct IEquatable_1U5BU5D_t1538  : public Array_t { };
// System.ValueType[]
// System.ValueType[]
struct ValueTypeU5BU5D_t1539  : public Array_t { };
// System.IntPtr[]
// System.IntPtr[]
struct IntPtrU5BU5D_t160  : public Array_t { };
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct ISerializableU5BU5D_t1540  : public Array_t { };
// System.Single[]
// System.Single[]
struct SingleU5BU5D_t89  : public Array_t { };
// System.IFormattable[]
// System.IFormattable[]
struct IFormattableU5BU5D_t1541  : public Array_t { };
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct IComparable_1U5BU5D_t1542  : public Array_t { };
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct IEquatable_1U5BU5D_t1543  : public Array_t { };
// System.Attribute[]
// System.Attribute[]
struct AttributeU5BU5D_t1544  : public Array_t { };
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct _AttributeU5BU5D_t1545  : public Array_t { };
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t185  : public Array_t { };
// System.Char[]
// System.Char[]
struct CharU5BU5D_t191  : public Array_t { };
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct IComparable_1U5BU5D_t1546  : public Array_t { };
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct IEquatable_1U5BU5D_t1547  : public Array_t { };
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t194  : public Array_t { };
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct _ParameterInfoU5BU5D_t1548  : public Array_t { };
// System.UInt16[]
// System.UInt16[]
struct UInt16U5BU5D_t237  : public Array_t { };
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct IComparable_1U5BU5D_t1549  : public Array_t { };
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct IEquatable_1U5BU5D_t1550  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1518  : public Array_t { };
// System.Int32[]
// System.Int32[]
struct Int32U5BU5D_t306  : public Array_t { };
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct IComparable_1U5BU5D_t1551  : public Array_t { };
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct IEquatable_1U5BU5D_t1552  : public Array_t { };
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t1368  : public Array_t { };
// System.Boolean[]
// System.Boolean[]
struct BooleanU5BU5D_t244  : public Array_t { };
// System.IComparable`1<System.Boolean>[]
// System.IComparable`1<System.Boolean>[]
struct IComparable_1U5BU5D_t1553  : public Array_t { };
// System.IEquatable`1<System.Boolean>[]
// System.IEquatable`1<System.Boolean>[]
struct IEquatable_1U5BU5D_t1554  : public Array_t { };
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct DictionaryEntryU5BU5D_t1555  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t1517  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1521  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1520  : public Array_t { };
// System.Byte[]
// System.Byte[]
struct ByteU5BU5D_t264  : public Array_t { };
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct IComparable_1U5BU5D_t1556  : public Array_t { };
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct IEquatable_1U5BU5D_t1557  : public Array_t { };
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct X509CertificateU5BU5D_t380  : public Array_t { };
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct IDeserializationCallbackU5BU5D_t1558  : public Array_t { };
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct KeySizesU5BU5D_t445  : public Array_t { };
// System.UInt32[]
// System.UInt32[]
struct UInt32U5BU5D_t436  : public Array_t { };
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct IComparable_1U5BU5D_t1559  : public Array_t { };
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct IEquatable_1U5BU5D_t1560  : public Array_t { };
// System.Byte[][]
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t575  : public Array_t { };
// System.Array[]
// System.Array[]
struct ArrayU5BU5D_t1561  : public Array_t { };
// System.Collections.ICollection[]
// System.Collections.ICollection[]
struct ICollectionU5BU5D_t1562  : public Array_t { };
// System.Collections.IList[]
// System.Collections.IList[]
struct IListU5BU5D_t1563  : public Array_t { };
// System.Enum[]
// System.Enum[]
struct EnumU5BU5D_t1564  : public Array_t { };
// System.Single[,]
// System.Single[,]
struct SingleU5BU2CU5D_t1565  : public Array_t { };
// System.Delegate[]
// System.Delegate[]
struct DelegateU5BU5D_t1251  : public Array_t { };
// System.UInt64[]
// System.UInt64[]
struct UInt64U5BU5D_t1070  : public Array_t { };
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct IComparable_1U5BU5D_t1566  : public Array_t { };
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct IEquatable_1U5BU5D_t1567  : public Array_t { };
// System.Int16[]
// System.Int16[]
struct Int16U5BU5D_t1274  : public Array_t { };
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct IComparable_1U5BU5D_t1568  : public Array_t { };
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct IEquatable_1U5BU5D_t1569  : public Array_t { };
// System.SByte[]
// System.SByte[]
struct SByteU5BU5D_t1125  : public Array_t { };
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct IComparable_1U5BU5D_t1570  : public Array_t { };
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct IEquatable_1U5BU5D_t1571  : public Array_t { };
// System.Int64[]
// System.Int64[]
struct Int64U5BU5D_t1252  : public Array_t { };
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct IComparable_1U5BU5D_t1572  : public Array_t { };
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct IEquatable_1U5BU5D_t1573  : public Array_t { };
// System.Double[]
// System.Double[]
struct DoubleU5BU5D_t1253  : public Array_t { };
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct IComparable_1U5BU5D_t1574  : public Array_t { };
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct IEquatable_1U5BU5D_t1575  : public Array_t { };
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t1257  : public Array_t { };
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct _FieldInfoU5BU5D_t1576  : public Array_t { };
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t849  : public Array_t { };
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct _MethodInfoU5BU5D_t1577  : public Array_t { };
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct MethodBaseU5BU5D_t1258  : public Array_t { };
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct _MethodBaseU5BU5D_t1578  : public Array_t { };
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct ConstructorInfoU5BU5D_t1256  : public Array_t { };
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct _ConstructorInfoU5BU5D_t1579  : public Array_t { };
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct TableRangeU5BU5D_t629  : public Array_t { };
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct TailoringInfoU5BU5D_t636  : public Array_t { };
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t644  : public Array_t { };
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t645  : public Array_t { };
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1254  : public Array_t { };
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t1523  : public Array_t { };
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct SlotU5BU5D_t711  : public Array_t { };
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct SlotU5BU5D_t715  : public Array_t { };
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t724  : public Array_t { };
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t732  : public Array_t { };
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct ModuleBuilderU5BU5D_t780  : public Array_t { };
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct _ModuleBuilderU5BU5D_t1580  : public Array_t { };
// System.Reflection.Module[]
// System.Reflection.Module[]
struct ModuleU5BU5D_t781  : public Array_t { };
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct _ModuleU5BU5D_t1581  : public Array_t { };
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct ParameterBuilderU5BU5D_t786  : public Array_t { };
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct _ParameterBuilderU5BU5D_t1582  : public Array_t { };
// System.Type[][]
// System.Type[][]
struct TypeU5BU5DU5BU5D_t787  : public Array_t { };
// System.Reflection.Emit.ILTokenInfo[]
// System.Reflection.Emit.ILTokenInfo[]
struct ILTokenInfoU5BU5D_t796  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelData[]
// System.Reflection.Emit.ILGenerator/LabelData[]
struct LabelDataU5BU5D_t797  : public Array_t { };
// System.Reflection.Emit.ILGenerator/LabelFixup[]
// System.Reflection.Emit.ILGenerator/LabelFixup[]
struct LabelFixupU5BU5D_t798  : public Array_t { };
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct GenericTypeParameterBuilderU5BU5D_t801  : public Array_t { };
// System.Reflection.Emit.TypeBuilder[]
// System.Reflection.Emit.TypeBuilder[]
struct TypeBuilderU5BU5D_t804  : public Array_t { };
// System.Runtime.InteropServices._TypeBuilder[]
// System.Runtime.InteropServices._TypeBuilder[]
struct _TypeBuilderU5BU5D_t1583  : public Array_t { };
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct MethodBuilderU5BU5D_t811  : public Array_t { };
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct _MethodBuilderU5BU5D_t1584  : public Array_t { };
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct ConstructorBuilderU5BU5D_t812  : public Array_t { };
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct _ConstructorBuilderU5BU5D_t1585  : public Array_t { };
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct FieldBuilderU5BU5D_t813  : public Array_t { };
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct _FieldBuilderU5BU5D_t1586  : public Array_t { };
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t1259  : public Array_t { };
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct _PropertyInfoU5BU5D_t1587  : public Array_t { };
// System.Reflection.CustomAttributeTypedArgument[]
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278  : public Array_t { };
// System.Reflection.CustomAttributeNamedArgument[]
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279  : public Array_t { };
// System.Resources.ResourceReader/ResourceInfo[]
// System.Resources.ResourceReader/ResourceInfo[]
struct ResourceInfoU5BU5D_t872  : public Array_t { };
// System.Resources.ResourceReader/ResourceCacheItem[]
// System.Resources.ResourceReader/ResourceCacheItem[]
struct ResourceCacheItemU5BU5D_t873  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextProperty[]
// System.Runtime.Remoting.Contexts.IContextProperty[]
struct IContextPropertyU5BU5D_t1264  : public Array_t { };
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1224  : public Array_t { };
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct ITrackingHandlerU5BU5D_t1299  : public Array_t { };
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct IContextAttributeU5BU5D_t1268  : public Array_t { };
// System.DateTime[]
// System.DateTime[]
struct DateTimeU5BU5D_t1301  : public Array_t { };
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct IComparable_1U5BU5D_t1588  : public Array_t { };
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct IEquatable_1U5BU5D_t1589  : public Array_t { };
// System.Decimal[]
// System.Decimal[]
struct DecimalU5BU5D_t1302  : public Array_t { };
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct IComparable_1U5BU5D_t1590  : public Array_t { };
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct IEquatable_1U5BU5D_t1591  : public Array_t { };
// System.TimeSpan[]
// System.TimeSpan[]
struct TimeSpanU5BU5D_t1303  : public Array_t { };
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct IComparable_1U5BU5D_t1592  : public Array_t { };
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct IEquatable_1U5BU5D_t1593  : public Array_t { };
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct TypeTagU5BU5D_t1304  : public Array_t { };
// System.MonoType[]
// System.MonoType[]
struct MonoTypeU5BU5D_t1307  : public Array_t { };
// System.Byte[,]
// System.Byte[,]
struct ByteU5BU2CU5D_t1044  : public Array_t { };
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct StrongNameU5BU5D_t1497  : public Array_t { };
// System.Reflection.CustomAttributeData[]
// System.Reflection.CustomAttributeData[]
struct CustomAttributeDataU5BU5D_t1270  : public Array_t { };
