﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ObsoleteAttribute
struct ObsoleteAttribute_t602;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.ObsoleteAttribute::.ctor()
extern "C" void ObsoleteAttribute__ctor_m3572 (ObsoleteAttribute_t602 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String)
extern "C" void ObsoleteAttribute__ctor_m3573 (ObsoleteAttribute_t602 * __this, String_t* ___message, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
extern "C" void ObsoleteAttribute__ctor_m3574 (ObsoleteAttribute_t602 * __this, String_t* ___message, bool ___error, const MethodInfo* method) IL2CPP_METHOD_ATTR;
