﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t25;
// System.String
struct String_t;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t203;
// System.Net.IPAddress
struct IPAddress_t236;
// System.Net.IPv6Address
struct IPv6Address_t238;
// System.UriFormatException
struct UriFormatException_t365;
// System.Byte[]
struct ByteU5BU5D_t264;
// System.Object[]
struct ObjectU5BU5D_t147;
// System.Exception
struct Exception_t134;
// System.MulticastDelegate
struct MulticastDelegate_t40;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t644;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t645;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t628;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t631;
// System.Reflection.MethodBase
struct MethodBase_t193;
// System.Reflection.Module
struct Module_t799;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t961;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1224;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t165;
// System.Text.StringBuilder
struct StringBuilder_t190;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1115;
// System.Char[]
struct CharU5BU5D_t191;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1106;
// System.Int64[]
struct Int64U5BU5D_t1252;
// System.String[]
struct StringU5BU5D_t122;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Int16.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "mscorlib_System_UInt16.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "mscorlib_System_Byte.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "mscorlib_System_Char.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_UInt32.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "mscorlib_System_UInt64.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_6.h"

void* RuntimeInvoker_Void_t596 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_GcAchievementDescriptionData_t107_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t107  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t107 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_GcUserProfileData_t106_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t106  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t106 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Double_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_UserProfileU5BU5DU26_t1713_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t25** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t25**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_UserProfileU5BU5DU26_t1713_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t25** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t25**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_GcScoreData_t109 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t109  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t109 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_BoneWeight_t30_BoneWeight_t30 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t30  p1, BoneWeight_t30  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t30 *)args[0]), *((BoneWeight_t30 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t49 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t49 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_IntPtr_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_CullingGroupEvent_t38 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t38  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t38 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_CullingGroupEvent_t38_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t38  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t38 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Color_t45_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t45  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t45 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_LayerMask_t48 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t48  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t48 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t48_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t48  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t48  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Single_t177_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t177_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t49_Vector3_t49_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t49  (*Func)(void* obj, Vector3_t49  p1, Vector3_t49  p2, const MethodInfo* method);
	Vector3_t49  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Vector3_t49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t49_Vector3_t49_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t49  (*Func)(void* obj, Vector3_t49  p1, float p2, const MethodInfo* method);
	Vector3_t49  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Vector3_t49_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t49  p1, Vector3_t49  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Vector3_t49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Single_t177_Single_t177_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t45_Color_t45_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t45  (*Func)(void* obj, Color_t45  p1, float p2, const MethodInfo* method);
	Color_t45  ret = ((Func)method->method)(obj, *((Color_t45 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t54_Color_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t54  (*Func)(void* obj, Color_t45  p1, const MethodInfo* method);
	Vector4_t54  ret = ((Func)method->method)(obj, *((Color_t45 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t177_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t52_Matrix4x4_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Matrix4x4_t52  p1, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((Matrix4x4_t52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Matrix4x4U26_t1715 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Matrix4x4_t52 * p1, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, (Matrix4x4_t52 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Matrix4x4_t52_Matrix4x4U26_t1715 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t52  p1, Matrix4x4_t52 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t52 *)args[0]), (Matrix4x4_t52 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Matrix4x4U26_t1715_Matrix4x4U26_t1715 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t52 * p1, Matrix4x4_t52 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t52 *)args[0], (Matrix4x4_t52 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t54_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t54  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t54  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t54  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t54 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t49_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t49  (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	Vector3_t49  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Vector3_t49_Quaternion_t50_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49  p1, Quaternion_t50  p2, Vector3_t49  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Quaternion_t50 *)args[1]), *((Vector3_t49 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t52_Vector3_t49_Quaternion_t50_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Vector3_t49  p1, Quaternion_t50  p2, Vector3_t49  p3, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Quaternion_t50 *)args[1]), *((Vector3_t49 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Vector3U26_t1714_QuaternionU26_t1716_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Vector3_t49 * p1, Quaternion_t50 * p2, Vector3_t49 * p3, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, (Vector3_t49 *)args[0], (Quaternion_t50 *)args[1], (Vector3_t49 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Single_t177_Single_t177_Single_t177_Single_t177_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Single_t177_Single_t177_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t52_Matrix4x4_t52_Matrix4x4_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t52  (*Func)(void* obj, Matrix4x4_t52  p1, Matrix4x4_t52  p2, const MethodInfo* method);
	Matrix4x4_t52  ret = ((Func)method->method)(obj, *((Matrix4x4_t52 *)args[0]), *((Matrix4x4_t52 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t54_Matrix4x4_t52_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t54  (*Func)(void* obj, Matrix4x4_t52  p1, Vector4_t54  p2, const MethodInfo* method);
	Vector4_t54  ret = ((Func)method->method)(obj, *((Matrix4x4_t52 *)args[0]), *((Vector4_t54 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Matrix4x4_t52_Matrix4x4_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t52  p1, Matrix4x4_t52  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t52 *)args[0]), *((Matrix4x4_t52 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Vector3_t49_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49  p1, Vector3_t49  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Vector3_t49 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t49  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t49  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Bounds_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t53  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t53 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Bounds_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t53  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Bounds_t53_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t53  p1, Vector3_t49  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t53 *)args[0]), *((Vector3_t49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_BoundsU26_t1717_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t53 * p1, Vector3_t49 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t53 *)args[0], (Vector3_t49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Bounds_t53_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t53  p1, Vector3_t49  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t53 *)args[0]), *((Vector3_t49 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_BoundsU26_t1717_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t53 * p1, Vector3_t49 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t53 *)args[0], (Vector3_t49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_RayU26_t1718_BoundsU26_t1717_SingleU26_t1719 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t55 * p1, Bounds_t53 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t55 *)args[0], (Bounds_t53 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Ray_t55 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t55  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t55 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Ray_t55_SingleU26_t1719 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t55  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t55 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t49_BoundsU26_t1717_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t49  (*Func)(void* obj, Bounds_t53 * p1, Vector3_t49 * p2, const MethodInfo* method);
	Vector3_t49  ret = ((Func)method->method)(obj, (Bounds_t53 *)args[0], (Vector3_t49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Bounds_t53_Bounds_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t53  p1, Bounds_t53  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t53 *)args[0]), *((Bounds_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Vector4_t54_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t54  p1, Vector4_t54  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t54 *)args[0]), *((Vector4_t54 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t54  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t54 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t54_Vector4_t54_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t54  (*Func)(void* obj, Vector4_t54  p1, Vector4_t54  p2, const MethodInfo* method);
	Vector4_t54  ret = ((Func)method->method)(obj, *((Vector4_t54 *)args[0]), *((Vector4_t54 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Vector4_t54_Vector4_t54 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t54  p1, Vector4_t54  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t54 *)args[0]), *((Vector4_t54 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Single_t177_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_SphericalHarmonicsL2U26_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t66 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t66 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Color_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t45  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t45 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Color_t45_SphericalHarmonicsL2U26_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t45  p1, SphericalHarmonicsL2_t66 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t45 *)args[0]), (SphericalHarmonicsL2_t66 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_ColorU26_t1721_SphericalHarmonicsL2U26_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t45 * p1, SphericalHarmonicsL2_t66 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t45 *)args[0], (SphericalHarmonicsL2_t66 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Vector3_t49_Color_t45_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49  p1, Color_t45  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Color_t45 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Vector3_t49_Color_t45_SphericalHarmonicsL2U26_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49  p1, Color_t45  p2, SphericalHarmonicsL2_t66 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t49 *)args[0]), *((Color_t45 *)args[1]), (SphericalHarmonicsL2_t66 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Vector3U26_t1714_ColorU26_t1721_SphericalHarmonicsL2U26_t1720 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49 * p1, Color_t45 * p2, SphericalHarmonicsL2_t66 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t49 *)args[0], (Color_t45 *)args[1], (SphericalHarmonicsL2_t66 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t66  (*Func)(void* obj, SphericalHarmonicsL2_t66  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t66  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t66 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t66_Single_t177_SphericalHarmonicsL2_t66 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t66  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t66  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t66  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t66 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t66  (*Func)(void* obj, SphericalHarmonicsL2_t66  p1, SphericalHarmonicsL2_t66  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t66  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t66 *)args[0]), *((SphericalHarmonicsL2_t66 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t66  p1, SphericalHarmonicsL2_t66  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t66 *)args[0]), *((SphericalHarmonicsL2_t66 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Rect_t51 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t51  (*Func)(void* obj, const MethodInfo* method);
	Rect_t51  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_RectU26_t1722 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t51 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t51 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_CameraClearFlags_t112 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t55_Vector3_t49 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t55  (*Func)(void* obj, Vector3_t49  p1, const MethodInfo* method);
	Ray_t55  ret = ((Func)method->method)(obj, *((Vector3_t49 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t55_Object_t_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t55  (*Func)(void* obj, Object_t * p1, Vector3_t49 * p2, const MethodInfo* method);
	Ray_t55  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t49 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t55_Single_t177_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t55  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t55 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t1718_Single_t177_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t55 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t55 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t1718_Single_t177_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t55 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t55 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_RenderBuffer_t111 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t111  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t111  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_RenderBufferU26_t1724_RenderBufferU26_t1724 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t111 * p2, RenderBuffer_t111 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t111 *)args[1], (RenderBuffer_t111 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Vector3U26_t1714 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t49 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t49 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t45  (*Func)(void* obj, const MethodInfo* method);
	Color_t45  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t590_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t129 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Double_t187_SByte_t590_SByte_t590_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t118  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t118 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_DateTime_t118_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t118  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t118 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t130 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t123 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t123  (*Func)(void* obj, const MethodInfo* method);
	Range_t123  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Range_t123 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t123  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t123 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t131 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t125  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t125 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_HitInfo_t125_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t125  p1, HitInfo_t125  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t125 *)args[0]), *((HitInfo_t125 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t125  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t125 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_StringU26_t1725_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t166  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t166 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t1726 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t203 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t203 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t376 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_IPAddressU26_t1727 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t236 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t236 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t218 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_IPv6AddressU26_t1728 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t238 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t238 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t292_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t279_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t279_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t279_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t279 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t291 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_X509KeyUsageFlags_t284 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t284_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t419_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Int16_t591_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RegexOptions_t310 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Category_t317_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_UInt16_t398_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int16_t591_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_UInt16_t398_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int16_t591_Int16_t591_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int16_t591_Object_t_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_UInt16_t398 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t398_UInt16_t398_UInt16_t398 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t312_SByte_t590_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_UInt16_t398_UInt16_t398 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int32U26_t1723_Int32U26_t1723_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_UInt16_t398_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int32_t181_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t333  (*Func)(void* obj, const MethodInfo* method);
	Interval_t333  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Interval_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t333  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Interval_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t333  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t333 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t333_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t333  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t333  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t187_Interval_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t333  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t333_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t333  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t333 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t1729 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_RegexOptionsU26_t1729_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Int32U26_t1723_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int16_t591_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_UInt16_t398_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int16_t591_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_UInt16_t398 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t313 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t367_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t395_Object_t_Int32U26_t1723_CharU26_t1730 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_UriFormatExceptionU26_t1731 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t365 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t365 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t189_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t449_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t453 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_ByteU26_t1732_Int32U26_t1723_ByteU5BU5DU26_t1733 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t264** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t264**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t118_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t407 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t407  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t407 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t405_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t405  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t405  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_RSAParameters_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t405  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t405 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t407_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t407  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t407  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t180_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t478 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Byte_t419_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t491 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t492 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int16_t591_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_SByte_t590_SByte_t590_Int16_t591_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t494 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t512 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t442 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_ByteU5BU5DU26_t1733_ByteU5BU5DU26_t1733 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t264** p2, ByteU5BU5D_t264** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t264**)args[1], (ByteU5BU5D_t264**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t591_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_SByte_t590_SByte_t590_Int16_t591_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t526 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t525 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t540 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t511 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t526_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int64_t188_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Byte_t419_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t405 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t405  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t405  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Byte_t419_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_ObjectU5BU5DU26_t1735 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t147** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t147**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_ObjectU5BU5DU26_t1735 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t147** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t147**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_SByte_t590_Object_t_Int32_t181_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int32U26_t1723_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t134 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t134 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_SByte_t590_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t134 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t134 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32U26_t1723_Object_t_Object_t_BooleanU26_t1734_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32U26_t1723_Object_t_Object_t_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_Int32U26_t1723_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t134 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t134 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int16_t591_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int32U26_t1723_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t134 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int64U26_t1737_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int64U26_t1737_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t134 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int64U26_t1737 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int64U26_t1737 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_UInt32U26_t1738_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_UInt32U26_t1738_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t134 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_UInt32U26_t1738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_UInt32U26_t1738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_UInt64U26_t1739_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t134 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_UInt64U26_t1739 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_ByteU26_t1732 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_ByteU26_t1732 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_SByteU26_t1740_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByteU26_t1740 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int16U26_t1741_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t134 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t134 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int16U26_t1741 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_UInt16U26_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_UInt16U26_t1742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_ByteU2AU26_t1743_ByteU2AU26_t1743_DoubleU2AU26_t1744_UInt16U2AU26_t1745_UInt16U2AU26_t1745_UInt16U2AU26_t1745_UInt16U2AU26_t1745 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t740_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Int16_t591_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t395_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t591_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32U26_t1723_Int32U26_t1723_BooleanU26_t1734_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_DoubleU26_t1746_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t134 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t134 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_DoubleU26_t1746 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t592_Decimal_t592_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, Decimal_t592  p1, Decimal_t592  p2, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), *((Decimal_t592 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Decimal_t592_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t592  p1, Decimal_t592  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), *((Decimal_t592 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Decimal_t592_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592  p1, Decimal_t592  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), *((Decimal_t592 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Int32U26_t1723_BooleanU26_t1734_BooleanU26_t1734_Int32U26_t1723_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t592_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_DecimalU26_t1747_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t592 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t592 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_UInt64U26_t1739 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_Int64U26_t1737 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_DecimalU26_t1747 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, Decimal_t592 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], (Decimal_t592 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_DecimalU26_t1747 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t592 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_DecimalU26_t1747_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t592 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t592 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_DecimalU26_t1747_DecimalU26_t1747_DecimalU26_t1747 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t592 * p1, Decimal_t592 * p2, Decimal_t592 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t592 *)args[0], (Decimal_t592 *)args[1], (Decimal_t592 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t189 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t1748 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t40 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t40 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1216 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t188_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t864 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t843 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t597  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t597  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1216_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t597  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t597 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t597_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t597  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t597  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_RuntimeFieldHandle_t599 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t599  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t599 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_ContractionU5BU5DU26_t1749_Level2MapU5BU5DU26_t1750 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t644** p3, Level2MapU5BU5D_t645** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t644**)args[2], (Level2MapU5BU5D_t645**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_CodePointIndexerU26_t1751_ByteU2AU26_t1743_ByteU2AU26_t1743_CodePointIndexerU26_t1751_ByteU2AU26_t1743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t628 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t628 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t628 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t628 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t419_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t641_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734_BooleanU26_t1734_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734_BooleanU26_t1734_SByte_t590_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t638 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t638 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t638 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t638 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int16_t591_Int32_t181_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t638 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t638 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t638 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t638 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t638 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t638 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Object_t_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t638 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t638 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32_t181_ContractionU26_t1753_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t631 ** p8, Context_t638 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t631 **)args[7], (Context_t638 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181_Object_t_SByte_t590_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t638 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t638 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32_t181_ContractionU26_t1753_ContextU26_t1752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t631 ** p9, Context_t638 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t631 **)args[8], (Context_t638 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_ByteU5BU5DU26_t1733_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t264** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t264**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t652_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t407_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t407  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t407  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_DSAParameters_t407 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t407  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t407 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t591_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t591_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Single_t177_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Single_t177_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Single_t177_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590_MethodBaseU26_t1754_Int32U26_t1723_Int32U26_t1723_StringU26_t1725_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t193 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t193 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1165_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int32U26_t1723_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1165_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723_Int32U26_t1723_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_DateTime_t118_DateTime_t118_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t118  p1, DateTime_t118  p2, TimeSpan_t278  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((DateTime_t118 *)args[1]), *((TimeSpan_t278 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t592  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t592  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_SByte_t590_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_IntPtr_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Object_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t750_Object_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t759_IntPtr_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_MonoIOStatU26_t1756_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t758 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t758 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_IntPtr_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_IntPtr_t_Object_t_Int32_t181_Int32_t181_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_IntPtr_t_Int64_t188_Int32_t181_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_IntPtr_t_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_IntPtr_t_Int64_t188_MonoIOErrorU26_t1755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t1208 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1208  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1208  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t844 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t802 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t802  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t802  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t841 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t599 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t599  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t599  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_OpCode_t806 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t806  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t806 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_OpCode_t806_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t806  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t806 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t810 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t1723_ModuleU26_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t799 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t799 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_ObjectU5BU5DU26_t1735_Object_t_Object_t_Object_t_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t147** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t147**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t147** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t147**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_ObjectU5BU5DU26_t1735_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t147** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t147**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t839 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t599 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t599  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t599 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t166  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t166 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1208 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1208  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1208 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_MonoEventInfoU26_t1759 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t848 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t848 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t848_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t848  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t848  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_IntPtr_t_MonoMethodInfoU26_t1760 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t852 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t852 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t852_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t852  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t852  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t844_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t833_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t134 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t134 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_MonoPropertyInfoU26_t1761_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t853 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t853 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t860 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t856 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int64_t188_ResourceInfoU26_t1762 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t868 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t868 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t897_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t897  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t897  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Object_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t419_IntPtr_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_SByte_t590_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t278  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t166_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t166  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t166 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t166_ISurrogateSelectorU26_t1763 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t166  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t166 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t278_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t180_Object_t_StringU26_t1725_StringU26_t1725 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1003 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t166  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t166  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t_SByte_t590_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t419_Object_t_SByte_t590_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_ObjectU26_t1758_HeaderU5BU5DU26_t1764 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1224** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1224**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Byte_t419_Object_t_SByte_t590_ObjectU26_t1758_HeaderU5BU5DU26_t1764 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1224** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1224**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Byte_t419_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Byte_t419_Object_t_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t165 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t165 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t165 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t165 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t165 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t165 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188_ObjectU26_t1758_SerializationInfoU26_t1765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t165 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t165 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Object_t_Object_t_Int64_t188_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64U26_t1737_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int64U26_t1737_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Int64_t188_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t188_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Int32_t181_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Object_t_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_Int64_t188_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_SByte_t590_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t166  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t166 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t166  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t166 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_StreamingContext_t166 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t166  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t166 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t166_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t166  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t166 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Object_t_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t118  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t118 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1036 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1036  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1036  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1039 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1043 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t189_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_UInt32U26_t1738_Int32_t181_UInt32U26_t1738_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t589_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Int64_t188_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t444 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_StringBuilderU26_t1766_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t190 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t190 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_EncoderFallbackBufferU26_t1767_CharU5BU5DU26_t1768 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1115 ** p6, CharU5BU5D_t191** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1115 **)args[5], (CharU5BU5D_t191**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_DecoderFallbackBufferU26_t1769 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1106 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1106 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int16_t591_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int16_t591_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t181_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_SByte_t590_Int32_t181_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590_Int32U26_t1723_BooleanU26_t1734_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_CharU26_t1730_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1106 ** p7, ByteU5BU5D_t264** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1106 **)args[6], (ByteU5BU5D_t264**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1106 ** p6, ByteU5BU5D_t264** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1106 **)args[5], (ByteU5BU5D_t264**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_Object_t_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1106 ** p2, ByteU5BU5D_t264** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1106 **)args[1], (ByteU5BU5D_t264**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_Object_t_Int64_t188_Int32_t181_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1106 ** p2, ByteU5BU5D_t264** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1106 **)args[1], (ByteU5BU5D_t264**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_UInt32U26_t1738_UInt32U26_t1738_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1106 ** p9, ByteU5BU5D_t264** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1106 **)args[8], (ByteU5BU5D_t264**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_UInt32U26_t1738_UInt32U26_t1738_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1106 ** p8, ByteU5BU5D_t264** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1106 **)args[7], (ByteU5BU5D_t264**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t590_Object_t_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t590_SByte_t590_Object_t_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_TimeSpan_t278_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t278  p1, TimeSpan_t278  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int64_t188_Int64_t188_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_IntPtr_t_Int32_t181_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t188_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t419_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t419_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t395_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t187_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t591_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t590_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t177_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t398_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t189_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Single_t177 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t589_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_SByte_t590_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t278  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t1165 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t1162 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, TimeSpan_t278  p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DateTime_t118_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t118  p1, DateTime_t118  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((DateTime_t118 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_DateTime_t118_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, DateTime_t118  p1, int32_t p2, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_DateTimeU26_t1770_DateTimeOffsetU26_t1771_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t118 * p4, DateTimeOffset_t1163 * p5, int8_t p6, Exception_t134 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t118 *)args[3], (DateTimeOffset_t1163 *)args[4], *((int8_t*)args[5]), (Exception_t134 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t134 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t134 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Object_t_SByte_t590_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_SByte_t590_DateTimeU26_t1770_DateTimeOffsetU26_t1771_Object_t_Int32_t181_SByte_t590_BooleanU26_t1734_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t118 * p5, DateTimeOffset_t1163 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t118 *)args[4], (DateTimeOffset_t1163 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_Object_t_Object_t_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Int32_t181_DateTimeU26_t1770_SByte_t590_BooleanU26_t1734_SByte_t590_ExceptionU26_t1736 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t118 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t134 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t118 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t134 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_DateTime_t118_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, DateTime_t118  p1, TimeSpan_t278  p2, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_DateTime_t118_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t118  p1, DateTime_t118  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((DateTime_t118 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_DateTime_t118_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t118  p1, TimeSpan_t278  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int64_t188_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t278  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_DateTimeOffset_t1163 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1163  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1163 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_DateTimeOffset_t1163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1163  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1163 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t591_Object_t_BooleanU26_t1734_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t591_Object_t_BooleanU26_t1734_BooleanU26_t1734_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t118_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t118  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t118_Nullable_1_t1269_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t118  p1, Nullable_1_t1269  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((Nullable_1_t1269 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_MonoEnumInfo_t1176 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1176  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1176 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_MonoEnumInfoU26_t1772 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1176 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1176 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Int16_t591_Int16_t591 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Int64_t188_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int16_t591_Int16_t591_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Guid_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1185  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1185 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Guid_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1185  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1185 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t1185  (*Func)(void* obj, const MethodInfo* method);
	Guid_t1185  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t590_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t187_Double_t187_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t864_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t596_UInt64U2AU26_t1773_Int32U2AU26_t1774_CharU2AU26_t1775_CharU2AU26_t1775_Int64U2AU26_t1776_Int32U2AU26_t1774 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Double_t187_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t592  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t592 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t591_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t188_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t177_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t592_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t592  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t592 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t177_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t187_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Object_t_BooleanU26_t1734_SByte_t590_Int32U26_t1723_Int32U26_t1723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t188_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, TimeSpan_t278  p1, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_TimeSpan_t278_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t278  p1, TimeSpan_t278  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t278  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t278  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_Double_t187_Int64_t188 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_TimeSpan_t278_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, TimeSpan_t278  p1, TimeSpan_t278  p2, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((TimeSpan_t278 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_DateTime_t118_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t118  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t118_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t118  (*Func)(void* obj, DateTime_t118  p1, const MethodInfo* method);
	DateTime_t118  ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t278_DateTime_t118_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, DateTime_t118  p1, TimeSpan_t278  p2, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((DateTime_t118 *)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Int32_t181_Int64U5BU5DU26_t1777_StringU5BU5DU26_t1778 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1252** p2, StringU5BU5D_t122** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1252**)args[1], (StringU5BU5D_t122**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1351 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1351  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1351  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t147** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t147**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t147** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t147**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_KeyValuePair_2_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1445  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1445 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1445  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1445_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1445  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1445  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_ObjectU26_t1758 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1448 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1448  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1448  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t376_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1445  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1445  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1331  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1331  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t108_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t108  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t108  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_GcAchievementData_t108 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t108  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t108 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_GcAchievementData_t108 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t108  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t108 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_GcAchievementData_t108 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t108  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t108 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_GcAchievementData_t108 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t108  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t108 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t109_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t109  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t109  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_GcScoreData_t109 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t109  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t109 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_GcScoreData_t109 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t109  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t109 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_GcScoreData_t109 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t109  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t109 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t93_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t93  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t93  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Keyframe_t93 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t93  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t93 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Keyframe_t93 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t93  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t93 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Keyframe_t93 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t93  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t93 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Keyframe_t93 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t93  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t93 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterModifier_t857_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t857  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t857  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_ParameterModifier_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t857  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t857 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_ParameterModifier_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t857  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_ParameterModifier_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t857  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t857 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_ParameterModifier_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t857  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t857 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t125_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t125  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t125  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t125  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t125 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t125  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t125 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1374_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1374  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1374  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_KeyValuePair_2_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1374  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1374 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1374  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1374 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_KeyValuePair_2_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1374  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1374 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1374  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1374 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t694_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t694  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t694  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Link_t694 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t694  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t694 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Link_t694 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t694  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t694 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Link_t694 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t694  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t694 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Link_t694 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t694  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t694 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t376_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_DictionaryEntry_t376 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t376  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t376 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_DictionaryEntry_t376 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t376  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t376 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DictionaryEntry_t376 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t376  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t376 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_DictionaryEntry_t376 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t376  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t376 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1391_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1391  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1391  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_KeyValuePair_2_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1391  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1391 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1391  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1391 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_KeyValuePair_2_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1391  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1391 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1391  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1391 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t275_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t275  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t275  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_X509ChainStatus_t275 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t275  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t275 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_X509ChainStatus_t275 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t275  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t275 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_X509ChainStatus_t275 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t275  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t275 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_X509ChainStatus_t275 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t275  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t275 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t326_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t326  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t326  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Mark_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t326  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t326 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Mark_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t326  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Mark_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t326  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Mark_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t326  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t326 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t363_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t363  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t363  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_UriScheme_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t363  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t363 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_UriScheme_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t363  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_UriScheme_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t363  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t363 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_UriScheme_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t363  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t363 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ClientCertificateType_t539_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Double_t187 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TableRange_t627_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t627  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t627  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_TableRange_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t627  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t627 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_TableRange_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t627  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t627 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_TableRange_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t627  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t627 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_TableRange_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t627  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t627 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1445_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1445  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1445  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_KeyValuePair_2_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1445  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1445 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1445 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1445  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1445 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t704_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t704  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t704  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Slot_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t704  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t704 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Slot_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t704  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t704 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Slot_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t704  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t704 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Slot_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t704  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t704 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t712_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t712  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t712  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Slot_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t712  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t712 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_Slot_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t712  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t712 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Slot_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t712  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t712 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Slot_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t712  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t712 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t793_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t793  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t793  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_ILTokenInfo_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t793  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t793 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_ILTokenInfo_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t793  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t793 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_ILTokenInfo_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t793  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t793 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_ILTokenInfo_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t793  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t793 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t795_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t795  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t795  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_LabelData_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t795  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t795 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_LabelData_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t795  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t795 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_LabelData_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t795  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t795 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_LabelData_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t795  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t795 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t794_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t794  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t794  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_LabelFixup_t794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t794  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t794 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_LabelFixup_t794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t794  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t794 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_LabelFixup_t794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t794  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t794 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_LabelFixup_t794 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t794  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t794 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t838_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t838  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t838  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t838  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t838 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t838  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t838  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t838 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t838  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t838 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t837_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t837  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t837  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t837  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t837 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t837  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t837 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t837  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t837 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t837  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t837 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_CustomAttributeTypedArgumentU5BU5DU26_t1779_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1278** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1278**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_CustomAttributeTypedArgumentU5BU5DU26_t1779_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1278** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1278**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_CustomAttributeTypedArgument_t838_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t838  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t838 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t838  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t838 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_CustomAttributeNamedArgumentU5BU5DU26_t1780_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1279** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1279**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_CustomAttributeNamedArgumentU5BU5DU26_t1780_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1279** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1279**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t181_Object_t_CustomAttributeNamedArgument_t837_Int32_t181_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t837  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t837 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Object_t_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t837  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t837 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t868_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t868  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t868  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_ResourceInfo_t868 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t868  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t868 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_ResourceInfo_t868 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t868  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t868 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_ResourceInfo_t868 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t868  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t868 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_ResourceInfo_t868 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t868  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t868 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t869_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t869  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t869  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_ResourceCacheItem_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t869  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t869 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t180_ResourceCacheItem_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t869  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_ResourceCacheItem_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t869  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t869 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_ResourceCacheItem_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t869  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t869 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_DateTime_t118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t118  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t118 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t592  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t592 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t596_Int32_t181_Decimal_t592 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t592  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t592 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t278_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t278  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t278  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_TimeSpan_t278 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t278  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t278 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1007_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t596_Int32_t181_Byte_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcAchievementData_t108 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t108  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t108  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t109 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t109  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t109  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Keyframe_t93 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t93  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t93  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t857 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t857  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t857  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t125 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t125  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t125  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1374_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1374  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t1374  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Object_t_BooleanU26_t1734 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1381 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1381  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1381  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t376_Object_t_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1374 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1374  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1374  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t694 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t694  (*Func)(void* obj, const MethodInfo* method);
	Link_t694  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t376_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1374_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1374  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1374  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_SByte_t590_SByte_t590 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1391_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1391  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1391  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1394 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1394  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1394  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t376_Object_t_Int32_t181 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t376  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t376  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1391 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1391  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1391  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1391_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1391  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1391  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t275 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t275  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t275  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t326  (*Func)(void* obj, const MethodInfo* method);
	Mark_t326  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t363 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t363  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t363  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t539 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t627 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t627  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t627  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1445_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1445  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1445  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t704 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t704  (*Func)(void* obj, const MethodInfo* method);
	Slot_t704  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t712 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t712  (*Func)(void* obj, const MethodInfo* method);
	Slot_t712  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t793  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t793  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t795  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t795  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t794 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t794  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t794  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t838  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t838  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t837  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t837  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t838_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t838  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t838  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1472 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1472  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1472  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_CustomAttributeTypedArgument_t838_CustomAttributeTypedArgument_t838 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t838  p1, CustomAttributeTypedArgument_t838  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t838 *)args[0]), *((CustomAttributeTypedArgument_t838 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t837_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t837  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t837  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1479 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1479  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1479  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_CustomAttributeNamedArgument_t837_CustomAttributeNamedArgument_t837 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t837  p1, CustomAttributeNamedArgument_t837  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t837 *)args[0]), *((CustomAttributeNamedArgument_t837 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t868 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t868  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t868  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t869 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t869  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t869  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1007 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_DateTimeOffset_t1163_DateTimeOffset_t1163 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1163  p1, DateTimeOffset_t1163  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1163 *)args[0]), *((DateTimeOffset_t1163 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_DateTimeOffset_t1163_DateTimeOffset_t1163 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1163  p1, DateTimeOffset_t1163  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1163 *)args[0]), *((DateTimeOffset_t1163 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Nullable_1_t1269 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1269  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1269 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t181_Guid_t1185_Guid_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1185  p1, Guid_t1185  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1185 *)args[0]), *((Guid_t1185 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t180_Guid_t1185_Guid_t1185 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1185  p1, Guid_t1185  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1185 *)args[0]), *((Guid_t1185 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1065] = 
{
	RuntimeInvoker_Void_t596,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Single_t177,
	RuntimeInvoker_Void_t596_Object_t,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180,
	RuntimeInvoker_Void_t596_Object_t_Double_t187,
	RuntimeInvoker_Void_t596_Int64_t188_Object_t,
	RuntimeInvoker_Void_t596_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181,
	RuntimeInvoker_Void_t596_GcAchievementDescriptionData_t107_Int32_t181,
	RuntimeInvoker_Void_t596_GcUserProfileData_t106_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Double_t187_Object_t,
	RuntimeInvoker_Void_t596_Int64_t188_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t,
	RuntimeInvoker_Void_t596_UserProfileU5BU5DU26_t1713_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_UserProfileU5BU5DU26_t1713_Int32_t181,
	RuntimeInvoker_Void_t596_GcScoreData_t109,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Single_t177,
	RuntimeInvoker_Int32_t181,
	RuntimeInvoker_Boolean_t180_BoneWeight_t30_BoneWeight_t30,
	RuntimeInvoker_Object_t_Vector3_t49,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t1714,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_IntPtr_t,
	RuntimeInvoker_Void_t596_Object_t_IntPtr_t_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_IntPtr_t,
	RuntimeInvoker_Void_t596_CullingGroupEvent_t38,
	RuntimeInvoker_Object_t_CullingGroupEvent_t38_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Color_t45_Single_t177,
	RuntimeInvoker_Void_t596_Single_t177_Single_t177,
	RuntimeInvoker_Object_t_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t,
	RuntimeInvoker_Int32_t181_LayerMask_t48,
	RuntimeInvoker_LayerMask_t48_Int32_t181,
	RuntimeInvoker_Void_t596_Single_t177_Single_t177_Single_t177,
	RuntimeInvoker_Single_t177_Vector3_t49,
	RuntimeInvoker_Vector3_t49_Vector3_t49_Vector3_t49,
	RuntimeInvoker_Vector3_t49_Vector3_t49_Single_t177,
	RuntimeInvoker_Boolean_t180_Vector3_t49_Vector3_t49,
	RuntimeInvoker_Void_t596_Single_t177_Single_t177_Single_t177_Single_t177,
	RuntimeInvoker_Color_t45_Color_t45_Single_t177,
	RuntimeInvoker_Vector4_t54_Color_t45,
	RuntimeInvoker_Boolean_t180_Vector3_t49,
	RuntimeInvoker_Single_t177_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Single_t177,
	RuntimeInvoker_Single_t177_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Single_t177,
	RuntimeInvoker_Matrix4x4_t52_Matrix4x4_t52,
	RuntimeInvoker_Matrix4x4_t52_Matrix4x4U26_t1715,
	RuntimeInvoker_Boolean_t180_Matrix4x4_t52_Matrix4x4U26_t1715,
	RuntimeInvoker_Boolean_t180_Matrix4x4U26_t1715_Matrix4x4U26_t1715,
	RuntimeInvoker_Matrix4x4_t52,
	RuntimeInvoker_Vector4_t54_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Vector4_t54,
	RuntimeInvoker_Vector3_t49_Vector3_t49,
	RuntimeInvoker_Matrix4x4_t52_Vector3_t49,
	RuntimeInvoker_Void_t596_Vector3_t49_Quaternion_t50_Vector3_t49,
	RuntimeInvoker_Matrix4x4_t52_Vector3_t49_Quaternion_t50_Vector3_t49,
	RuntimeInvoker_Matrix4x4_t52_Vector3U26_t1714_QuaternionU26_t1716_Vector3U26_t1714,
	RuntimeInvoker_Matrix4x4_t52_Single_t177_Single_t177_Single_t177_Single_t177_Single_t177_Single_t177,
	RuntimeInvoker_Matrix4x4_t52_Single_t177_Single_t177_Single_t177_Single_t177,
	RuntimeInvoker_Matrix4x4_t52_Matrix4x4_t52_Matrix4x4_t52,
	RuntimeInvoker_Vector4_t54_Matrix4x4_t52_Vector4_t54,
	RuntimeInvoker_Boolean_t180_Matrix4x4_t52_Matrix4x4_t52,
	RuntimeInvoker_Void_t596_Vector3_t49_Vector3_t49,
	RuntimeInvoker_Vector3_t49,
	RuntimeInvoker_Void_t596_Vector3_t49,
	RuntimeInvoker_Void_t596_Bounds_t53,
	RuntimeInvoker_Boolean_t180_Bounds_t53,
	RuntimeInvoker_Boolean_t180_Bounds_t53_Vector3_t49,
	RuntimeInvoker_Boolean_t180_BoundsU26_t1717_Vector3U26_t1714,
	RuntimeInvoker_Single_t177_Bounds_t53_Vector3_t49,
	RuntimeInvoker_Single_t177_BoundsU26_t1717_Vector3U26_t1714,
	RuntimeInvoker_Boolean_t180_RayU26_t1718_BoundsU26_t1717_SingleU26_t1719,
	RuntimeInvoker_Boolean_t180_Ray_t55,
	RuntimeInvoker_Boolean_t180_Ray_t55_SingleU26_t1719,
	RuntimeInvoker_Vector3_t49_BoundsU26_t1717_Vector3U26_t1714,
	RuntimeInvoker_Boolean_t180_Bounds_t53_Bounds_t53,
	RuntimeInvoker_Single_t177_Vector4_t54_Vector4_t54,
	RuntimeInvoker_Single_t177_Vector4_t54,
	RuntimeInvoker_Vector4_t54_Vector4_t54_Vector4_t54,
	RuntimeInvoker_Boolean_t180_Vector4_t54_Vector4_t54,
	RuntimeInvoker_Single_t177_Single_t177,
	RuntimeInvoker_Single_t177_Single_t177_Single_t177,
	RuntimeInvoker_Boolean_t180_Single_t177_Single_t177,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_SphericalHarmonicsL2U26_t1720,
	RuntimeInvoker_Void_t596_Color_t45,
	RuntimeInvoker_Void_t596_Color_t45_SphericalHarmonicsL2U26_t1720,
	RuntimeInvoker_Void_t596_ColorU26_t1721_SphericalHarmonicsL2U26_t1720,
	RuntimeInvoker_Void_t596_Vector3_t49_Color_t45_Single_t177,
	RuntimeInvoker_Void_t596_Vector3_t49_Color_t45_SphericalHarmonicsL2U26_t1720,
	RuntimeInvoker_Void_t596_Vector3U26_t1714_ColorU26_t1721_SphericalHarmonicsL2U26_t1720,
	RuntimeInvoker_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66_Single_t177,
	RuntimeInvoker_SphericalHarmonicsL2_t66_Single_t177_SphericalHarmonicsL2_t66,
	RuntimeInvoker_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66,
	RuntimeInvoker_Boolean_t180_SphericalHarmonicsL2_t66_SphericalHarmonicsL2_t66,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Rect_t51,
	RuntimeInvoker_Void_t596_RectU26_t1722,
	RuntimeInvoker_CameraClearFlags_t112,
	RuntimeInvoker_Ray_t55_Vector3_t49,
	RuntimeInvoker_Ray_t55_Object_t_Vector3U26_t1714,
	RuntimeInvoker_Object_t_Ray_t55_Single_t177_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_RayU26_t1718_Single_t177_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_RayU26_t1718_Single_t177_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Void_t596_IntPtr_t,
	RuntimeInvoker_RenderBuffer_t111,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Void_t596_IntPtr_t_RenderBufferU26_t1724_RenderBufferU26_t1724,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Int32_t181,
	RuntimeInvoker_Void_t596_Vector3U26_t1714,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590,
	RuntimeInvoker_Color_t45,
	RuntimeInvoker_Object_t_SByte_t590_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Int32_t181_Object_t,
	RuntimeInvoker_UserState_t129,
	RuntimeInvoker_Void_t596_Object_t_Double_t187_SByte_t590_SByte_t590_DateTime_t118,
	RuntimeInvoker_Double_t187,
	RuntimeInvoker_Void_t596_Double_t187,
	RuntimeInvoker_DateTime_t118,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t590_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_DateTime_t118_Object_t_Int32_t181,
	RuntimeInvoker_Int64_t188,
	RuntimeInvoker_Void_t596_Int64_t188,
	RuntimeInvoker_UserScope_t130,
	RuntimeInvoker_Range_t123,
	RuntimeInvoker_Void_t596_Range_t123,
	RuntimeInvoker_TimeScope_t131,
	RuntimeInvoker_Void_t596_Int32_t181_HitInfo_t125,
	RuntimeInvoker_Boolean_t180_HitInfo_t125_HitInfo_t125,
	RuntimeInvoker_Boolean_t180_HitInfo_t125,
	RuntimeInvoker_Void_t596_Object_t_StringU26_t1725_StringU26_t1725,
	RuntimeInvoker_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_StreamingContext_t166,
	RuntimeInvoker_Void_t596_Int32_t181_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t1726,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t,
	RuntimeInvoker_DictionaryEntry_t376,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Int16_t591_Int16_t591,
	RuntimeInvoker_Boolean_t180_Object_t_IPAddressU26_t1727,
	RuntimeInvoker_AddressFamily_t218,
	RuntimeInvoker_Object_t_Int64_t188,
	RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Object_t_IPv6AddressU26_t1728,
	RuntimeInvoker_UInt16_t398_Int16_t591,
	RuntimeInvoker_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_DateTime_t118,
	RuntimeInvoker_SecurityProtocolType_t239,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_Object_t_Object_t,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_Int32_t181_SByte_t590,
	RuntimeInvoker_AsnDecodeStatus_t292_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_SByte_t590,
	RuntimeInvoker_X509ChainStatusFlags_t279_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t279_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_X509ChainStatusFlags_t279_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_X509ChainStatusFlags_t279,
	RuntimeInvoker_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32_t181_Int32_t181,
	RuntimeInvoker_X509RevocationFlag_t286,
	RuntimeInvoker_X509RevocationMode_t287,
	RuntimeInvoker_X509VerificationFlags_t291,
	RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_X509KeyUsageFlags_t284,
	RuntimeInvoker_X509KeyUsageFlags_t284_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Byte_t419_Int16_t591,
	RuntimeInvoker_Byte_t419_Int16_t591_Int16_t591,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_RegexOptions_t310,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Category_t317_Object_t,
	RuntimeInvoker_Boolean_t180_UInt16_t398_Int16_t591,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int16_t591,
	RuntimeInvoker_Void_t596_Int16_t591_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_UInt16_t398_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Int16_t591_Int16_t591_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Int16_t591_Object_t_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_UInt16_t398,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_SByte_t590_Object_t,
	RuntimeInvoker_Void_t596_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t,
	RuntimeInvoker_Void_t596_SByte_t590_Int32_t181_Object_t,
	RuntimeInvoker_UInt16_t398_UInt16_t398_UInt16_t398,
	RuntimeInvoker_OpFlags_t312_SByte_t590_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_UInt16_t398_UInt16_t398,
	RuntimeInvoker_Void_t596_Int16_t591,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int32U26_t1723_Int32U26_t1723_SByte_t590,
	RuntimeInvoker_Boolean_t180_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Boolean_t180_UInt16_t398_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int16_t591,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int32_t181_SByte_t590_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_SByte_t590_Int32_t181,
	RuntimeInvoker_Interval_t333,
	RuntimeInvoker_Boolean_t180_Interval_t333,
	RuntimeInvoker_Void_t596_Interval_t333,
	RuntimeInvoker_Interval_t333_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Double_t187_Interval_t333,
	RuntimeInvoker_Object_t_Interval_t333_Object_t_Object_t,
	RuntimeInvoker_Double_t187_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t1723,
	RuntimeInvoker_Object_t_RegexOptionsU26_t1729,
	RuntimeInvoker_Void_t596_RegexOptionsU26_t1729_SByte_t590,
	RuntimeInvoker_Boolean_t180_Int32U26_t1723_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Category_t317,
	RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int16_t591_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int16_t591,
	RuntimeInvoker_Char_t395_Int16_t591,
	RuntimeInvoker_Int32_t181_Int32U26_t1723,
	RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_UInt16_t398_SByte_t590,
	RuntimeInvoker_Void_t596_Int16_t591_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_UInt16_t398,
	RuntimeInvoker_Position_t313,
	RuntimeInvoker_UriHostNameType_t367_Object_t,
	RuntimeInvoker_Object_t_Int16_t591,
	RuntimeInvoker_Void_t596_StringU26_t1725,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Char_t395_Object_t_Int32U26_t1723_CharU26_t1730,
	RuntimeInvoker_Void_t596_Object_t_UriFormatExceptionU26_t1731,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_UInt32_t189_Int32_t181,
	RuntimeInvoker_UInt32_t189_Object_t_Int32_t181,
	RuntimeInvoker_Sign_t449_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_ConfidenceFactor_t453,
	RuntimeInvoker_Void_t596_SByte_t590_Object_t,
	RuntimeInvoker_Byte_t419,
	RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_ByteU26_t1732_Int32U26_t1723_ByteU5BU5DU26_t1733,
	RuntimeInvoker_DateTime_t118_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t407,
	RuntimeInvoker_RSAParameters_t405_SByte_t590,
	RuntimeInvoker_Void_t596_RSAParameters_t405,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_DSAParameters_t407_BooleanU26_t1734,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t_SByte_t590,
	RuntimeInvoker_Boolean_t180_DateTime_t118,
	RuntimeInvoker_X509ChainStatusFlags_t478,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_Byte_t419,
	RuntimeInvoker_Void_t596_Byte_t419_Byte_t419,
	RuntimeInvoker_AlertLevel_t491,
	RuntimeInvoker_AlertDescription_t492,
	RuntimeInvoker_Object_t_Byte_t419,
	RuntimeInvoker_Void_t596_Int16_t591_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_SByte_t590_SByte_t590_Int16_t591_SByte_t590_SByte_t590,
	RuntimeInvoker_CipherAlgorithmType_t494,
	RuntimeInvoker_HashAlgorithmType_t512,
	RuntimeInvoker_ExchangeAlgorithmType_t510,
	RuntimeInvoker_CipherMode_t442,
	RuntimeInvoker_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_ByteU5BU5DU26_t1733_ByteU5BU5DU26_t1733,
	RuntimeInvoker_Object_t_Byte_t419_Object_t,
	RuntimeInvoker_Object_t_Int16_t591_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_SByte_t590_SByte_t590_Int16_t591_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t526,
	RuntimeInvoker_SecurityCompressionType_t525,
	RuntimeInvoker_HandshakeType_t540,
	RuntimeInvoker_HandshakeState_t511,
	RuntimeInvoker_UInt64_t589,
	RuntimeInvoker_SecurityProtocolType_t526_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t419_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t419_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Byte_t419_Object_t,
	RuntimeInvoker_Object_t_Byte_t419_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Int64_t188_Int64_t188_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Byte_t419_Byte_t419_Object_t,
	RuntimeInvoker_RSAParameters_t405,
	RuntimeInvoker_Void_t596_Object_t_Byte_t419,
	RuntimeInvoker_Void_t596_Object_t_Byte_t419_Byte_t419,
	RuntimeInvoker_Void_t596_Object_t_Byte_t419_Object_t,
	RuntimeInvoker_ContentType_t505,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_ObjectU5BU5DU26_t1735,
	RuntimeInvoker_Int32_t181_Object_t_ObjectU5BU5DU26_t1735,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Byte_t419_Object_t,
	RuntimeInvoker_Char_t395_Object_t,
	RuntimeInvoker_Decimal_t592_Object_t,
	RuntimeInvoker_Int16_t591_Object_t,
	RuntimeInvoker_Int64_t188_Object_t,
	RuntimeInvoker_SByte_t590_Object_t,
	RuntimeInvoker_Single_t177_Object_t,
	RuntimeInvoker_UInt16_t398_Object_t,
	RuntimeInvoker_UInt32_t189_Object_t,
	RuntimeInvoker_UInt64_t589_Object_t,
	RuntimeInvoker_Boolean_t180_SByte_t590_Object_t_Int32_t181_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int32U26_t1723_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_SByte_t590_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_Void_t596_Int32U26_t1723_Object_t_Object_t_BooleanU26_t1734_BooleanU26_t1734,
	RuntimeInvoker_Void_t596_Int32U26_t1723_Object_t_Object_t_BooleanU26_t1734,
	RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_Int32U26_t1723_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Int32U26_t1723_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Int16_t591_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int32U26_t1723_ExceptionU26_t1736,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Int64_t188,
	RuntimeInvoker_Boolean_t180_Int64_t188,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int64U26_t1737_ExceptionU26_t1736,
	RuntimeInvoker_Int64_t188_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int64U26_t1737_ExceptionU26_t1736,
	RuntimeInvoker_Int64_t188_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int64U26_t1737,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int64U26_t1737,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_UInt32U26_t1738_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_UInt32U26_t1738_ExceptionU26_t1736,
	RuntimeInvoker_UInt32_t189_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_UInt32_t189_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_UInt32U26_t1738,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_UInt32U26_t1738,
	RuntimeInvoker_UInt64_t589_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_UInt64U26_t1739_ExceptionU26_t1736,
	RuntimeInvoker_UInt64_t589_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_UInt64U26_t1739,
	RuntimeInvoker_Int32_t181_SByte_t590,
	RuntimeInvoker_Boolean_t180_SByte_t590,
	RuntimeInvoker_Byte_t419_Object_t_Object_t,
	RuntimeInvoker_Byte_t419_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_ByteU26_t1732,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_ByteU26_t1732,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_SByteU26_t1740_ExceptionU26_t1736,
	RuntimeInvoker_SByte_t590_Object_t_Object_t,
	RuntimeInvoker_SByte_t590_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_SByteU26_t1740,
	RuntimeInvoker_Boolean_t180_Object_t_SByte_t590_Int16U26_t1741_ExceptionU26_t1736,
	RuntimeInvoker_Int16_t591_Object_t_Object_t,
	RuntimeInvoker_Int16_t591_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int16U26_t1741,
	RuntimeInvoker_UInt16_t398_Object_t_Object_t,
	RuntimeInvoker_UInt16_t398_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_UInt16U26_t1742,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_UInt16U26_t1742,
	RuntimeInvoker_Void_t596_ByteU2AU26_t1743_ByteU2AU26_t1743_DoubleU2AU26_t1744_UInt16U2AU26_t1745_UInt16U2AU26_t1745_UInt16U2AU26_t1745_UInt16U2AU26_t1745,
	RuntimeInvoker_UnicodeCategory_t740_Int16_t591,
	RuntimeInvoker_Char_t395_Int16_t591_Object_t,
	RuntimeInvoker_Void_t596_Int16_t591_Int32_t181,
	RuntimeInvoker_Char_t395_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_SByte_t590_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181_SByte_t590_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int16_t591_Int32_t181,
	RuntimeInvoker_Object_t_Int32_t181_Int16_t591,
	RuntimeInvoker_Object_t_Int16_t591_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_Int32U26_t1723_Int32U26_t1723_Int32U26_t1723_BooleanU26_t1734_StringU26_t1725,
	RuntimeInvoker_Void_t596_Int32_t181_Int16_t591,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Object_t_Int16_t591_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Single_t177,
	RuntimeInvoker_Boolean_t180_Single_t177,
	RuntimeInvoker_Single_t177_Object_t_Object_t,
	RuntimeInvoker_Int32_t181_Double_t187,
	RuntimeInvoker_Boolean_t180_Double_t187,
	RuntimeInvoker_Double_t187_Object_t_Object_t,
	RuntimeInvoker_Double_t187_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_DoubleU26_t1746_ExceptionU26_t1736,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_DoubleU26_t1746,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Object_t_Decimal_t592,
	RuntimeInvoker_Decimal_t592_Decimal_t592_Decimal_t592,
	RuntimeInvoker_UInt64_t589_Decimal_t592,
	RuntimeInvoker_Int64_t188_Decimal_t592,
	RuntimeInvoker_Boolean_t180_Decimal_t592_Decimal_t592,
	RuntimeInvoker_Decimal_t592_Decimal_t592,
	RuntimeInvoker_Int32_t181_Decimal_t592_Decimal_t592,
	RuntimeInvoker_Int32_t181_Decimal_t592,
	RuntimeInvoker_Boolean_t180_Decimal_t592,
	RuntimeInvoker_Decimal_t592_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Int32U26_t1723_BooleanU26_t1734_BooleanU26_t1734_Int32U26_t1723_SByte_t590,
	RuntimeInvoker_Decimal_t592_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_DecimalU26_t1747_SByte_t590,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_UInt64U26_t1739,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_Int64U26_t1737,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_DecimalU26_t1747,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_Int32_t181,
	RuntimeInvoker_Double_t187_DecimalU26_t1747,
	RuntimeInvoker_Void_t596_DecimalU26_t1747_Int32_t181,
	RuntimeInvoker_Int32_t181_DecimalU26_t1747_DecimalU26_t1747_DecimalU26_t1747,
	RuntimeInvoker_Byte_t419_Decimal_t592,
	RuntimeInvoker_SByte_t590_Decimal_t592,
	RuntimeInvoker_Int16_t591_Decimal_t592,
	RuntimeInvoker_UInt16_t398_Decimal_t592,
	RuntimeInvoker_UInt32_t189_Decimal_t592,
	RuntimeInvoker_Decimal_t592_SByte_t590,
	RuntimeInvoker_Decimal_t592_Int16_t591,
	RuntimeInvoker_Decimal_t592_Int32_t181,
	RuntimeInvoker_Decimal_t592_Int64_t188,
	RuntimeInvoker_Decimal_t592_Single_t177,
	RuntimeInvoker_Decimal_t592_Double_t187,
	RuntimeInvoker_Single_t177_Decimal_t592,
	RuntimeInvoker_Double_t187_Decimal_t592,
	RuntimeInvoker_Boolean_t180_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int32_t181,
	RuntimeInvoker_IntPtr_t_Int64_t188,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t181_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t189,
	RuntimeInvoker_UInt64_t589_IntPtr_t,
	RuntimeInvoker_UInt32_t189_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t188,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t1748,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_TypeCode_t1216,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_UInt64_t589_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Object_t_Int16_t591,
	RuntimeInvoker_Object_t_Object_t_Int64_t188,
	RuntimeInvoker_Int64_t188_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Object_t_Int64_t188_Int64_t188,
	RuntimeInvoker_Object_t_Int64_t188_Int64_t188_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int64_t188_Int64_t188,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_Int64_t188_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t864,
	RuntimeInvoker_MemberTypes_t843,
	RuntimeInvoker_RuntimeTypeHandle_t597,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_TypeCode_t1216_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t597,
	RuntimeInvoker_RuntimeTypeHandle_t597_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_RuntimeFieldHandle_t599,
	RuntimeInvoker_Void_t596_IntPtr_t_SByte_t590,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Object_t_ContractionU5BU5DU26_t1749_Level2MapU5BU5DU26_t1750,
	RuntimeInvoker_Void_t596_Object_t_CodePointIndexerU26_t1751_ByteU2AU26_t1743_ByteU2AU26_t1743_CodePointIndexerU26_t1751_ByteU2AU26_t1743,
	RuntimeInvoker_Byte_t419_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590,
	RuntimeInvoker_Byte_t419_Int32_t181_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_ExtenderType_t641_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734_BooleanU26_t1734_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734_BooleanU26_t1734_SByte_t590_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_Int32_t181_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_BooleanU26_t1734,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int16_t591_Int32_t181_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_ContextU26_t1752,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Object_t_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32_t181_ContractionU26_t1753_ContextU26_t1752,
	RuntimeInvoker_Boolean_t180_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181_Object_t_SByte_t590_ContextU26_t1752,
	RuntimeInvoker_Boolean_t180_Object_t_Int32U26_t1723_Int32_t181_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32_t181_ContractionU26_t1753_ContextU26_t1752,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Object_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_SByte_t590_ByteU5BU5DU26_t1733_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_ConfidenceFactor_t650,
	RuntimeInvoker_Sign_t652_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t407_SByte_t590,
	RuntimeInvoker_Void_t596_DSAParameters_t407,
	RuntimeInvoker_Int16_t591_Object_t_Int32_t181,
	RuntimeInvoker_Single_t177_Object_t_Int32_t181,
	RuntimeInvoker_Double_t187_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_Int16_t591_SByte_t590,
	RuntimeInvoker_Void_t596_Int32_t181_Single_t177_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Single_t177_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Int32_t181_Single_t177_Object_t,
	RuntimeInvoker_Boolean_t180_Int32_t181_SByte_t590_MethodBaseU26_t1754_Int32U26_t1723_Int32U26_t1723_StringU26_t1725_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Int32_t181_DateTime_t118,
	RuntimeInvoker_DayOfWeek_t1165_DateTime_t118,
	RuntimeInvoker_Int32_t181_Int32U26_t1723_Int32_t181_Int32_t181,
	RuntimeInvoker_DayOfWeek_t1165_Int32_t181,
	RuntimeInvoker_Void_t596_Int32U26_t1723_Int32U26_t1723_Int32U26_t1723_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Void_t596_DateTime_t118_DateTime_t118_TimeSpan_t278,
	RuntimeInvoker_TimeSpan_t278,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Object_t_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32U26_t1723,
	RuntimeInvoker_Char_t395,
	RuntimeInvoker_Decimal_t592,
	RuntimeInvoker_SByte_t590,
	RuntimeInvoker_UInt16_t398,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_SByte_t590_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_SByte_t590_Int32_t181,
	RuntimeInvoker_Int32_t181_IntPtr_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t181_Int32_t181_MonoIOErrorU26_t1755,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_FileAttributes_t750_Object_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_MonoFileType_t759_IntPtr_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_Boolean_t180_Object_t_MonoIOStatU26_t1756_MonoIOErrorU26_t1755,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_MonoIOErrorU26_t1755,
	RuntimeInvoker_Boolean_t180_IntPtr_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_Int32_t181_IntPtr_t_Object_t_Int32_t181_Int32_t181_MonoIOErrorU26_t1755,
	RuntimeInvoker_Int64_t188_IntPtr_t_Int64_t188_Int32_t181_MonoIOErrorU26_t1755,
	RuntimeInvoker_Int64_t188_IntPtr_t_MonoIOErrorU26_t1755,
	RuntimeInvoker_Boolean_t180_IntPtr_t_Int64_t188_MonoIOErrorU26_t1755,
	RuntimeInvoker_Void_t596_Object_t_Int32_t181_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t833,
	RuntimeInvoker_RuntimeMethodHandle_t1208,
	RuntimeInvoker_MethodAttributes_t844,
	RuntimeInvoker_MethodToken_t802,
	RuntimeInvoker_FieldAttributes_t841,
	RuntimeInvoker_RuntimeFieldHandle_t599,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Object_t_Object_t,
	RuntimeInvoker_Void_t596_OpCode_t806,
	RuntimeInvoker_Void_t596_OpCode_t806_Object_t,
	RuntimeInvoker_StackBehaviour_t810,
	RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_SByte_t590_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t1723_ModuleU26_t1757,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_AssemblyNameFlags_t828,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_ObjectU5BU5DU26_t1735_Object_t_Object_t_Object_t_ObjectU26_t1758,
	RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_ObjectU5BU5DU26_t1735_Object_t,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_EventAttributes_t839,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t599,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t166,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1208,
	RuntimeInvoker_Void_t596_Object_t_MonoEventInfoU26_t1759,
	RuntimeInvoker_MonoEventInfo_t848_Object_t,
	RuntimeInvoker_Void_t596_IntPtr_t_MonoMethodInfoU26_t1760,
	RuntimeInvoker_MonoMethodInfo_t852_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t844_IntPtr_t,
	RuntimeInvoker_CallingConventions_t833_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t1736,
	RuntimeInvoker_Void_t596_Object_t_MonoPropertyInfoU26_t1761_Int32_t181,
	RuntimeInvoker_PropertyAttributes_t860,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int32_t181_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t856,
	RuntimeInvoker_Void_t596_Int64_t188_ResourceInfoU26_t1762,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Int32_t181,
	RuntimeInvoker_GCHandle_t897_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Object_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Byte_t419_IntPtr_t_Int32_t181,
	RuntimeInvoker_Void_t596_IntPtr_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Void_t596_BooleanU26_t1734,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t1725,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t1725,
	RuntimeInvoker_Void_t596_SByte_t590_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_TimeSpan_t278,
	RuntimeInvoker_Void_t596_Object_t_Object_t_SByte_t590_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t166_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t166_ISurrogateSelectorU26_t1763,
	RuntimeInvoker_Void_t596_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t278_Object_t,
	RuntimeInvoker_Object_t_StringU26_t1725,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t1758,
	RuntimeInvoker_Boolean_t180_Object_t_StringU26_t1725_StringU26_t1725,
	RuntimeInvoker_WellKnownObjectMode_t1003,
	RuntimeInvoker_StreamingContext_t166,
	RuntimeInvoker_TypeFilterLevel_t1019,
	RuntimeInvoker_Void_t596_Object_t_BooleanU26_t1734,
	RuntimeInvoker_Object_t_Byte_t419_Object_t_SByte_t590_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t419_Object_t_SByte_t590_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_ObjectU26_t1758_HeaderU5BU5DU26_t1764,
	RuntimeInvoker_Void_t596_Byte_t419_Object_t_SByte_t590_ObjectU26_t1758_HeaderU5BU5DU26_t1764,
	RuntimeInvoker_Boolean_t180_Byte_t419_Object_t,
	RuntimeInvoker_Void_t596_Byte_t419_Object_t_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765,
	RuntimeInvoker_Void_t596_Object_t_SByte_t590_SByte_t590_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765,
	RuntimeInvoker_Void_t596_Object_t_Int64U26_t1737_ObjectU26_t1758_SerializationInfoU26_t1765,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188_ObjectU26_t1758_SerializationInfoU26_t1765,
	RuntimeInvoker_Void_t596_Int64_t188_Object_t_Object_t_Int64_t188_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Int64U26_t1737_ObjectU26_t1758,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int64U26_t1737_ObjectU26_t1758,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Int64_t188_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Int64_t188_Int64_t188_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t188_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t419,
	RuntimeInvoker_Void_t596_Int64_t188_Int32_t181_Int64_t188,
	RuntimeInvoker_Void_t596_Int64_t188_Object_t_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Int64_t188_Object_t_Int64_t188_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_SByte_t590_Object_t_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_StreamingContext_t166,
	RuntimeInvoker_Void_t596_Object_t_Object_t_StreamingContext_t166,
	RuntimeInvoker_Void_t596_StreamingContext_t166,
	RuntimeInvoker_Object_t_StreamingContext_t166_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Object_t_Int16_t591,
	RuntimeInvoker_Void_t596_Object_t_DateTime_t118,
	RuntimeInvoker_Void_t596_Object_t_Single_t177,
	RuntimeInvoker_SerializationEntry_t1036,
	RuntimeInvoker_StreamingContextStates_t1039,
	RuntimeInvoker_CspProviderFlags_t1043,
	RuntimeInvoker_UInt32_t189_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Object_t_Object_t_SByte_t590,
	RuntimeInvoker_Void_t596_Int64_t188_Object_t_Int32_t181,
	RuntimeInvoker_UInt32_t189_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_UInt32U26_t1738_Int32_t181_UInt32U26_t1738_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t596_Int64_t188_Int64_t188,
	RuntimeInvoker_UInt64_t589_Int64_t188_Int32_t181,
	RuntimeInvoker_UInt64_t589_Int64_t188_Int64_t188_Int64_t188,
	RuntimeInvoker_UInt64_t589_Int64_t188,
	RuntimeInvoker_PaddingMode_t444,
	RuntimeInvoker_Void_t596_StringBuilderU26_t1766_Int32_t181,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_EncoderFallbackBufferU26_t1767_CharU5BU5DU26_t1768,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_DecoderFallbackBufferU26_t1769,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int16_t591_Int32_t181,
	RuntimeInvoker_Boolean_t180_Int16_t591_Int16_t591_Int32_t181,
	RuntimeInvoker_Void_t596_Int16_t591_Int16_t591_Int32_t181,
	RuntimeInvoker_Object_t_Int32U26_t1723,
	RuntimeInvoker_Object_t_Int32_t181_Object_t_Int32_t181,
	RuntimeInvoker_Void_t596_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_SByte_t590_Int32_t181_SByte_t590_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_SByte_t590_Int32U26_t1723_BooleanU26_t1734_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_CharU26_t1730_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_CharU26_t1730_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_Object_t_Int64_t188_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_Object_t_Int64_t188_Int32_t181_Object_t_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Object_t_Int32_t181_UInt32U26_t1738_UInt32U26_t1738_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Int32_t181_UInt32U26_t1738_UInt32U26_t1738_Object_t_DecoderFallbackBufferU26_t1769_ByteU5BU5DU26_t1733_SByte_t590,
	RuntimeInvoker_Void_t596_SByte_t590_Int32_t181,
	RuntimeInvoker_IntPtr_t_SByte_t590_Object_t_BooleanU26_t1734,
	RuntimeInvoker_Boolean_t180_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t590_SByte_t590_Object_t_BooleanU26_t1734,
	RuntimeInvoker_Boolean_t180_TimeSpan_t278_TimeSpan_t278,
	RuntimeInvoker_Boolean_t180_Int64_t188_Int64_t188_SByte_t590,
	RuntimeInvoker_Boolean_t180_IntPtr_t_Int32_t181_SByte_t590,
	RuntimeInvoker_Int64_t188_Double_t187,
	RuntimeInvoker_Object_t_Double_t187,
	RuntimeInvoker_Int64_t188_Object_t_Int32_t181,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t181_Int32_t181,
	RuntimeInvoker_Byte_t419_SByte_t590,
	RuntimeInvoker_Byte_t419_Double_t187,
	RuntimeInvoker_Byte_t419_Single_t177,
	RuntimeInvoker_Byte_t419_Int64_t188,
	RuntimeInvoker_Char_t395_SByte_t590,
	RuntimeInvoker_Char_t395_Int64_t188,
	RuntimeInvoker_Char_t395_Single_t177,
	RuntimeInvoker_Char_t395_Object_t_Object_t,
	RuntimeInvoker_DateTime_t118_Object_t_Object_t,
	RuntimeInvoker_DateTime_t118_Int16_t591,
	RuntimeInvoker_DateTime_t118_Int32_t181,
	RuntimeInvoker_DateTime_t118_Int64_t188,
	RuntimeInvoker_DateTime_t118_Single_t177,
	RuntimeInvoker_DateTime_t118_SByte_t590,
	RuntimeInvoker_Double_t187_SByte_t590,
	RuntimeInvoker_Double_t187_Double_t187,
	RuntimeInvoker_Double_t187_Single_t177,
	RuntimeInvoker_Double_t187_Int32_t181,
	RuntimeInvoker_Double_t187_Int64_t188,
	RuntimeInvoker_Double_t187_Int16_t591,
	RuntimeInvoker_Int16_t591_SByte_t590,
	RuntimeInvoker_Int16_t591_Double_t187,
	RuntimeInvoker_Int16_t591_Single_t177,
	RuntimeInvoker_Int16_t591_Int32_t181,
	RuntimeInvoker_Int16_t591_Int64_t188,
	RuntimeInvoker_Int64_t188_SByte_t590,
	RuntimeInvoker_Int64_t188_Int16_t591,
	RuntimeInvoker_Int64_t188_Single_t177,
	RuntimeInvoker_Int64_t188_Int64_t188,
	RuntimeInvoker_SByte_t590_SByte_t590,
	RuntimeInvoker_SByte_t590_Int16_t591,
	RuntimeInvoker_SByte_t590_Double_t187,
	RuntimeInvoker_SByte_t590_Single_t177,
	RuntimeInvoker_SByte_t590_Int32_t181,
	RuntimeInvoker_SByte_t590_Int64_t188,
	RuntimeInvoker_Single_t177_SByte_t590,
	RuntimeInvoker_Single_t177_Double_t187,
	RuntimeInvoker_Single_t177_Int64_t188,
	RuntimeInvoker_Single_t177_Int16_t591,
	RuntimeInvoker_UInt16_t398_SByte_t590,
	RuntimeInvoker_UInt16_t398_Double_t187,
	RuntimeInvoker_UInt16_t398_Single_t177,
	RuntimeInvoker_UInt16_t398_Int32_t181,
	RuntimeInvoker_UInt16_t398_Int64_t188,
	RuntimeInvoker_UInt32_t189_SByte_t590,
	RuntimeInvoker_UInt32_t189_Int16_t591,
	RuntimeInvoker_UInt32_t189_Double_t187,
	RuntimeInvoker_UInt32_t189_Single_t177,
	RuntimeInvoker_UInt32_t189_Int64_t188,
	RuntimeInvoker_UInt64_t589_SByte_t590,
	RuntimeInvoker_UInt64_t589_Int16_t591,
	RuntimeInvoker_UInt64_t589_Double_t187,
	RuntimeInvoker_UInt64_t589_Single_t177,
	RuntimeInvoker_UInt64_t589_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_SByte_t590_TimeSpan_t278,
	RuntimeInvoker_Void_t596_Int64_t188_Int32_t181,
	RuntimeInvoker_DayOfWeek_t1165,
	RuntimeInvoker_DateTimeKind_t1162,
	RuntimeInvoker_DateTime_t118_TimeSpan_t278,
	RuntimeInvoker_DateTime_t118_Double_t187,
	RuntimeInvoker_Int32_t181_DateTime_t118_DateTime_t118,
	RuntimeInvoker_DateTime_t118_DateTime_t118_Int32_t181,
	RuntimeInvoker_DateTime_t118_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Int32_t181_DateTimeU26_t1770_DateTimeOffsetU26_t1771_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_SByte_t590_SByte_t590_Int32U26_t1723,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Object_t_Object_t_SByte_t590_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Object_t_Int32_t181_Object_t_SByte_t590_Int32U26_t1723,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_SByte_t590_DateTimeU26_t1770_DateTimeOffsetU26_t1771_Object_t_Int32_t181_SByte_t590_BooleanU26_t1734_BooleanU26_t1734,
	RuntimeInvoker_DateTime_t118_Object_t_Object_t_Object_t_Int32_t181,
	RuntimeInvoker_Boolean_t180_Object_t_Object_t_Object_t_Int32_t181_DateTimeU26_t1770_SByte_t590_BooleanU26_t1734_SByte_t590_ExceptionU26_t1736,
	RuntimeInvoker_DateTime_t118_DateTime_t118_TimeSpan_t278,
	RuntimeInvoker_Boolean_t180_DateTime_t118_DateTime_t118,
	RuntimeInvoker_Void_t596_DateTime_t118_TimeSpan_t278,
	RuntimeInvoker_Void_t596_Int64_t188_TimeSpan_t278,
	RuntimeInvoker_Int32_t181_DateTimeOffset_t1163,
	RuntimeInvoker_Boolean_t180_DateTimeOffset_t1163,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int16_t591,
	RuntimeInvoker_Object_t_Int16_t591_Object_t_BooleanU26_t1734_BooleanU26_t1734,
	RuntimeInvoker_Object_t_Int16_t591_Object_t_BooleanU26_t1734_BooleanU26_t1734_SByte_t590,
	RuntimeInvoker_Object_t_DateTime_t118_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t118_Nullable_1_t1269_Object_t_Object_t,
	RuntimeInvoker_Void_t596_MonoEnumInfo_t1176,
	RuntimeInvoker_Void_t596_Object_t_MonoEnumInfoU26_t1772,
	RuntimeInvoker_Int32_t181_Int16_t591_Int16_t591,
	RuntimeInvoker_Int32_t181_Int64_t188_Int64_t188,
	RuntimeInvoker_PlatformID_t1205,
	RuntimeInvoker_Void_t596_Int32_t181_Int16_t591_Int16_t591_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Int32_t181_Guid_t1185,
	RuntimeInvoker_Boolean_t180_Guid_t1185,
	RuntimeInvoker_Guid_t1185,
	RuntimeInvoker_Object_t_SByte_t590_SByte_t590_SByte_t590,
	RuntimeInvoker_Double_t187_Double_t187_Double_t187,
	RuntimeInvoker_TypeAttributes_t864_Object_t,
	RuntimeInvoker_Object_t_SByte_t590_SByte_t590,
	RuntimeInvoker_Void_t596_UInt64U2AU26_t1773_Int32U2AU26_t1774_CharU2AU26_t1775_CharU2AU26_t1775_Int64U2AU26_t1776_Int32U2AU26_t1774,
	RuntimeInvoker_Void_t596_Int32_t181_Int64_t188,
	RuntimeInvoker_Void_t596_Object_t_Double_t187_Int32_t181,
	RuntimeInvoker_Void_t596_Object_t_Decimal_t592,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t591_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t188_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t177_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t187_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t592_Object_t,
	RuntimeInvoker_Object_t_Single_t177_Object_t,
	RuntimeInvoker_Object_t_Double_t187_Object_t,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Void_t596_Object_t_BooleanU26_t1734_SByte_t590_Int32U26_t1723_Int32U26_t1723,
	RuntimeInvoker_Object_t_Object_t_Int32_t181_Int32_t181_Object_t_SByte_t590_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t596_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_Int64_t188_Int32_t181_Int32_t181_Int32_t181_Int32_t181_Int32_t181,
	RuntimeInvoker_TimeSpan_t278_TimeSpan_t278,
	RuntimeInvoker_Int32_t181_TimeSpan_t278_TimeSpan_t278,
	RuntimeInvoker_Int32_t181_TimeSpan_t278,
	RuntimeInvoker_Boolean_t180_TimeSpan_t278,
	RuntimeInvoker_TimeSpan_t278_Double_t187,
	RuntimeInvoker_TimeSpan_t278_Double_t187_Int64_t188,
	RuntimeInvoker_TimeSpan_t278_TimeSpan_t278_TimeSpan_t278,
	RuntimeInvoker_TimeSpan_t278_DateTime_t118,
	RuntimeInvoker_Boolean_t180_DateTime_t118_Object_t,
	RuntimeInvoker_DateTime_t118_DateTime_t118,
	RuntimeInvoker_TimeSpan_t278_DateTime_t118_TimeSpan_t278,
	RuntimeInvoker_Boolean_t180_Int32_t181_Int64U5BU5DU26_t1777_StringU5BU5DU26_t1778,
	RuntimeInvoker_Enumerator_t1351,
	RuntimeInvoker_Void_t596_Int32_t181_ObjectU26_t1758,
	RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Int32_t181,
	RuntimeInvoker_Void_t596_ObjectU5BU5DU26_t1735_Int32_t181_Int32_t181,
	RuntimeInvoker_Void_t596_KeyValuePair_2_t1445,
	RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1445,
	RuntimeInvoker_KeyValuePair_2_t1445_Object_t_Object_t,
	RuntimeInvoker_Boolean_t180_Object_t_ObjectU26_t1758,
	RuntimeInvoker_Enumerator_t1448,
	RuntimeInvoker_DictionaryEntry_t376_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1445,
	RuntimeInvoker_Enumerator_t1331,
	RuntimeInvoker_GcAchievementData_t108_Int32_t181,
	RuntimeInvoker_Void_t596_GcAchievementData_t108,
	RuntimeInvoker_Boolean_t180_GcAchievementData_t108,
	RuntimeInvoker_Int32_t181_GcAchievementData_t108,
	RuntimeInvoker_Void_t596_Int32_t181_GcAchievementData_t108,
	RuntimeInvoker_GcScoreData_t109_Int32_t181,
	RuntimeInvoker_Boolean_t180_GcScoreData_t109,
	RuntimeInvoker_Int32_t181_GcScoreData_t109,
	RuntimeInvoker_Void_t596_Int32_t181_GcScoreData_t109,
	RuntimeInvoker_Void_t596_Int32_t181_IntPtr_t,
	RuntimeInvoker_Keyframe_t93_Int32_t181,
	RuntimeInvoker_Void_t596_Keyframe_t93,
	RuntimeInvoker_Boolean_t180_Keyframe_t93,
	RuntimeInvoker_Int32_t181_Keyframe_t93,
	RuntimeInvoker_Void_t596_Int32_t181_Keyframe_t93,
	RuntimeInvoker_ParameterModifier_t857_Int32_t181,
	RuntimeInvoker_Void_t596_ParameterModifier_t857,
	RuntimeInvoker_Boolean_t180_ParameterModifier_t857,
	RuntimeInvoker_Int32_t181_ParameterModifier_t857,
	RuntimeInvoker_Void_t596_Int32_t181_ParameterModifier_t857,
	RuntimeInvoker_HitInfo_t125_Int32_t181,
	RuntimeInvoker_Void_t596_HitInfo_t125,
	RuntimeInvoker_Int32_t181_HitInfo_t125,
	RuntimeInvoker_KeyValuePair_2_t1374_Int32_t181,
	RuntimeInvoker_Void_t596_KeyValuePair_2_t1374,
	RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1374,
	RuntimeInvoker_Int32_t181_KeyValuePair_2_t1374,
	RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1374,
	RuntimeInvoker_Link_t694_Int32_t181,
	RuntimeInvoker_Void_t596_Link_t694,
	RuntimeInvoker_Boolean_t180_Link_t694,
	RuntimeInvoker_Int32_t181_Link_t694,
	RuntimeInvoker_Void_t596_Int32_t181_Link_t694,
	RuntimeInvoker_DictionaryEntry_t376_Int32_t181,
	RuntimeInvoker_Void_t596_DictionaryEntry_t376,
	RuntimeInvoker_Boolean_t180_DictionaryEntry_t376,
	RuntimeInvoker_Int32_t181_DictionaryEntry_t376,
	RuntimeInvoker_Void_t596_Int32_t181_DictionaryEntry_t376,
	RuntimeInvoker_KeyValuePair_2_t1391_Int32_t181,
	RuntimeInvoker_Void_t596_KeyValuePair_2_t1391,
	RuntimeInvoker_Boolean_t180_KeyValuePair_2_t1391,
	RuntimeInvoker_Int32_t181_KeyValuePair_2_t1391,
	RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1391,
	RuntimeInvoker_X509ChainStatus_t275_Int32_t181,
	RuntimeInvoker_Void_t596_X509ChainStatus_t275,
	RuntimeInvoker_Boolean_t180_X509ChainStatus_t275,
	RuntimeInvoker_Int32_t181_X509ChainStatus_t275,
	RuntimeInvoker_Void_t596_Int32_t181_X509ChainStatus_t275,
	RuntimeInvoker_Int32_t181_Object_t_Int32_t181_Int32_t181_Int32_t181_Object_t,
	RuntimeInvoker_Mark_t326_Int32_t181,
	RuntimeInvoker_Void_t596_Mark_t326,
	RuntimeInvoker_Boolean_t180_Mark_t326,
	RuntimeInvoker_Int32_t181_Mark_t326,
	RuntimeInvoker_Void_t596_Int32_t181_Mark_t326,
	RuntimeInvoker_UriScheme_t363_Int32_t181,
	RuntimeInvoker_Void_t596_UriScheme_t363,
	RuntimeInvoker_Boolean_t180_UriScheme_t363,
	RuntimeInvoker_Int32_t181_UriScheme_t363,
	RuntimeInvoker_Void_t596_Int32_t181_UriScheme_t363,
	RuntimeInvoker_ClientCertificateType_t539_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_Double_t187,
	RuntimeInvoker_TableRange_t627_Int32_t181,
	RuntimeInvoker_Void_t596_TableRange_t627,
	RuntimeInvoker_Boolean_t180_TableRange_t627,
	RuntimeInvoker_Int32_t181_TableRange_t627,
	RuntimeInvoker_Void_t596_Int32_t181_TableRange_t627,
	RuntimeInvoker_KeyValuePair_2_t1445_Int32_t181,
	RuntimeInvoker_Int32_t181_KeyValuePair_2_t1445,
	RuntimeInvoker_Void_t596_Int32_t181_KeyValuePair_2_t1445,
	RuntimeInvoker_Slot_t704_Int32_t181,
	RuntimeInvoker_Void_t596_Slot_t704,
	RuntimeInvoker_Boolean_t180_Slot_t704,
	RuntimeInvoker_Int32_t181_Slot_t704,
	RuntimeInvoker_Void_t596_Int32_t181_Slot_t704,
	RuntimeInvoker_Slot_t712_Int32_t181,
	RuntimeInvoker_Void_t596_Slot_t712,
	RuntimeInvoker_Boolean_t180_Slot_t712,
	RuntimeInvoker_Int32_t181_Slot_t712,
	RuntimeInvoker_Void_t596_Int32_t181_Slot_t712,
	RuntimeInvoker_ILTokenInfo_t793_Int32_t181,
	RuntimeInvoker_Void_t596_ILTokenInfo_t793,
	RuntimeInvoker_Boolean_t180_ILTokenInfo_t793,
	RuntimeInvoker_Int32_t181_ILTokenInfo_t793,
	RuntimeInvoker_Void_t596_Int32_t181_ILTokenInfo_t793,
	RuntimeInvoker_LabelData_t795_Int32_t181,
	RuntimeInvoker_Void_t596_LabelData_t795,
	RuntimeInvoker_Boolean_t180_LabelData_t795,
	RuntimeInvoker_Int32_t181_LabelData_t795,
	RuntimeInvoker_Void_t596_Int32_t181_LabelData_t795,
	RuntimeInvoker_LabelFixup_t794_Int32_t181,
	RuntimeInvoker_Void_t596_LabelFixup_t794,
	RuntimeInvoker_Boolean_t180_LabelFixup_t794,
	RuntimeInvoker_Int32_t181_LabelFixup_t794,
	RuntimeInvoker_Void_t596_Int32_t181_LabelFixup_t794,
	RuntimeInvoker_CustomAttributeTypedArgument_t838_Int32_t181,
	RuntimeInvoker_Void_t596_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_Boolean_t180_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_Int32_t181_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_Void_t596_Int32_t181_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_CustomAttributeNamedArgument_t837_Int32_t181,
	RuntimeInvoker_Void_t596_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_Boolean_t180_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_Int32_t181_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_Void_t596_Int32_t181_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_Void_t596_CustomAttributeTypedArgumentU5BU5DU26_t1779_Int32_t181,
	RuntimeInvoker_Void_t596_CustomAttributeTypedArgumentU5BU5DU26_t1779_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_CustomAttributeTypedArgument_t838_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_Void_t596_CustomAttributeNamedArgumentU5BU5DU26_t1780_Int32_t181,
	RuntimeInvoker_Void_t596_CustomAttributeNamedArgumentU5BU5DU26_t1780_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_CustomAttributeNamedArgument_t837_Int32_t181_Int32_t181,
	RuntimeInvoker_Int32_t181_Object_t_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_ResourceInfo_t868_Int32_t181,
	RuntimeInvoker_Void_t596_ResourceInfo_t868,
	RuntimeInvoker_Boolean_t180_ResourceInfo_t868,
	RuntimeInvoker_Int32_t181_ResourceInfo_t868,
	RuntimeInvoker_Void_t596_Int32_t181_ResourceInfo_t868,
	RuntimeInvoker_ResourceCacheItem_t869_Int32_t181,
	RuntimeInvoker_Void_t596_ResourceCacheItem_t869,
	RuntimeInvoker_Boolean_t180_ResourceCacheItem_t869,
	RuntimeInvoker_Int32_t181_ResourceCacheItem_t869,
	RuntimeInvoker_Void_t596_Int32_t181_ResourceCacheItem_t869,
	RuntimeInvoker_Void_t596_Int32_t181_DateTime_t118,
	RuntimeInvoker_Void_t596_Decimal_t592,
	RuntimeInvoker_Void_t596_Int32_t181_Decimal_t592,
	RuntimeInvoker_TimeSpan_t278_Int32_t181,
	RuntimeInvoker_Void_t596_Int32_t181_TimeSpan_t278,
	RuntimeInvoker_TypeTag_t1007_Int32_t181,
	RuntimeInvoker_Boolean_t180_Byte_t419,
	RuntimeInvoker_Int32_t181_Byte_t419,
	RuntimeInvoker_Void_t596_Int32_t181_Byte_t419,
	RuntimeInvoker_GcAchievementData_t108,
	RuntimeInvoker_GcScoreData_t109,
	RuntimeInvoker_Object_t_Object_t_SByte_t590_Object_t_Object_t,
	RuntimeInvoker_Keyframe_t93,
	RuntimeInvoker_ParameterModifier_t857,
	RuntimeInvoker_HitInfo_t125,
	RuntimeInvoker_KeyValuePair_2_t1374_Object_t_SByte_t590,
	RuntimeInvoker_Boolean_t180_Object_t_BooleanU26_t1734,
	RuntimeInvoker_Enumerator_t1381,
	RuntimeInvoker_DictionaryEntry_t376_Object_t_SByte_t590,
	RuntimeInvoker_KeyValuePair_2_t1374,
	RuntimeInvoker_Link_t694,
	RuntimeInvoker_DictionaryEntry_t376_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1374_Object_t,
	RuntimeInvoker_Boolean_t180_SByte_t590_SByte_t590,
	RuntimeInvoker_KeyValuePair_2_t1391_Object_t_Int32_t181,
	RuntimeInvoker_Enumerator_t1394,
	RuntimeInvoker_DictionaryEntry_t376_Object_t_Int32_t181,
	RuntimeInvoker_KeyValuePair_2_t1391,
	RuntimeInvoker_KeyValuePair_2_t1391_Object_t,
	RuntimeInvoker_X509ChainStatus_t275,
	RuntimeInvoker_Mark_t326,
	RuntimeInvoker_UriScheme_t363,
	RuntimeInvoker_ClientCertificateType_t539,
	RuntimeInvoker_TableRange_t627,
	RuntimeInvoker_KeyValuePair_2_t1445_Object_t,
	RuntimeInvoker_Slot_t704,
	RuntimeInvoker_Slot_t712,
	RuntimeInvoker_ILTokenInfo_t793,
	RuntimeInvoker_LabelData_t795,
	RuntimeInvoker_LabelFixup_t794,
	RuntimeInvoker_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_CustomAttributeTypedArgument_t838_Object_t,
	RuntimeInvoker_Enumerator_t1472,
	RuntimeInvoker_Boolean_t180_CustomAttributeTypedArgument_t838_CustomAttributeTypedArgument_t838,
	RuntimeInvoker_CustomAttributeNamedArgument_t837_Object_t,
	RuntimeInvoker_Enumerator_t1479,
	RuntimeInvoker_Boolean_t180_CustomAttributeNamedArgument_t837_CustomAttributeNamedArgument_t837,
	RuntimeInvoker_ResourceInfo_t868,
	RuntimeInvoker_ResourceCacheItem_t869,
	RuntimeInvoker_TypeTag_t1007,
	RuntimeInvoker_Int32_t181_DateTimeOffset_t1163_DateTimeOffset_t1163,
	RuntimeInvoker_Boolean_t180_DateTimeOffset_t1163_DateTimeOffset_t1163,
	RuntimeInvoker_Boolean_t180_Nullable_1_t1269,
	RuntimeInvoker_Int32_t181_Guid_t1185_Guid_t1185,
	RuntimeInvoker_Boolean_t180_Guid_t1185_Guid_t1185,
};
