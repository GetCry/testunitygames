﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_genMethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m8457(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1386 *, String_t*, bool, const MethodInfo*))KeyValuePair_2__ctor_m8383_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m8458(__this, method) (( String_t* (*) (KeyValuePair_2_t1386 *, const MethodInfo*))KeyValuePair_2_get_Key_m8384_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m8459(__this, ___value, method) (( void (*) (KeyValuePair_2_t1386 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m8385_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m8460(__this, method) (( bool (*) (KeyValuePair_2_t1386 *, const MethodInfo*))KeyValuePair_2_get_Value_m8386_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m8461(__this, ___value, method) (( void (*) (KeyValuePair_2_t1386 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m8387_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m8462(__this, method) (( String_t* (*) (KeyValuePair_2_t1386 *, const MethodInfo*))KeyValuePair_2_ToString_m8388_gshared)(__this, method)
