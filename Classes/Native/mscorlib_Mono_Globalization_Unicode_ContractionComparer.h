﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Globalization.Unicode.ContractionComparer
struct ContractionComparer_t632;

#include "mscorlib_System_Object.h"

// Mono.Globalization.Unicode.ContractionComparer
struct  ContractionComparer_t632  : public Object_t
{
};
struct ContractionComparer_t632_StaticFields{
	// Mono.Globalization.Unicode.ContractionComparer Mono.Globalization.Unicode.ContractionComparer::Instance
	ContractionComparer_t632 * ___Instance_0;
};
