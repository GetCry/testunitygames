﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t1482;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1526;
// System.Exception
struct Exception_t134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m9362_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgumentU5BU5D_t1279* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m9362(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgumentU5BU5D_t1279*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m9362_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9363_gshared (ArrayReadOnlyList_1_t1482 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9363(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1482 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9363_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t837  ArrayReadOnlyList_1_get_Item_m9364_gshared (ArrayReadOnlyList_1_t1482 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m9364(__this, ___index, method) (( CustomAttributeNamedArgument_t837  (*) (ArrayReadOnlyList_1_t1482 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m9364_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m9365_gshared (ArrayReadOnlyList_1_t1482 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m9365(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m9365_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m9366_gshared (ArrayReadOnlyList_1_t1482 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m9366(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1482 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m9366_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m9367_gshared (ArrayReadOnlyList_1_t1482 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m9367(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1482 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m9367_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m9368_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m9368(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_Add_m9368_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m9369_gshared (ArrayReadOnlyList_1_t1482 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m9369(__this, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m9369_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m9370_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m9370(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m9370_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m9371_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgumentU5BU5D_t1279* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m9371(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgumentU5BU5D_t1279*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m9371_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m9372_gshared (ArrayReadOnlyList_1_t1482 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m9372(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1482 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m9372_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m9373_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m9373(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m9373_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m9374_gshared (ArrayReadOnlyList_1_t1482 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m9374(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m9374_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m9375_gshared (ArrayReadOnlyList_1_t1482 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m9375(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1482 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m9375_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m9376_gshared (ArrayReadOnlyList_1_t1482 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m9376(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1482 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m9376_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern "C" Exception_t134 * ArrayReadOnlyList_1_ReadOnlyError_m9377_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m9377(__this /* static, unused */, method) (( Exception_t134 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m9377_gshared)(__this /* static, unused */, method)
