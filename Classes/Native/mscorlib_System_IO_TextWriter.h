﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t191;
// System.IO.TextWriter
struct TextWriter_t424;

#include "mscorlib_System_Object.h"

// System.IO.TextWriter
struct  TextWriter_t424  : public Object_t
{
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t191* ___CoreNewLine_0;
};
struct TextWriter_t424_StaticFields{
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t424 * ___Null_1;
};
