﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t1471;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m9220_gshared (Enumerator_t1472 * __this, List_1_t1471 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m9220(__this, ___l, method) (( void (*) (Enumerator_t1472 *, List_1_t1471 *, const MethodInfo*))Enumerator__ctor_m9220_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m9221_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m9221(__this, method) (( void (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m9221_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m9222_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m9222(__this, method) (( Object_t * (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m9222_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void Enumerator_Dispose_m9223_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m9223(__this, method) (( void (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_Dispose_m9223_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern "C" void Enumerator_VerifyState_m9224_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m9224(__this, method) (( void (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_VerifyState_m9224_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool Enumerator_MoveNext_m9225_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m9225(__this, method) (( bool (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_MoveNext_m9225_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C" CustomAttributeTypedArgument_t838  Enumerator_get_Current_m9226_gshared (Enumerator_t1472 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m9226(__this, method) (( CustomAttributeTypedArgument_t838  (*) (Enumerator_t1472 *, const MethodInfo*))Enumerator_get_Current_m9226_gshared)(__this, method)
