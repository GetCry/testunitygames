﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t1470;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1525;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t835;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void Collection_1__ctor_m9156_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1__ctor_m9156(__this, method) (( void (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1__ctor_m9156_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9157_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9157(__this, method) (( bool (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9157_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m9158_gshared (Collection_1_t1470 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m9158(__this, ___array, ___index, method) (( void (*) (Collection_1_t1470 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m9158_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m9159_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m9159(__this, method) (( Object_t * (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m9159_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m9160_gshared (Collection_1_t1470 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m9160(__this, ___value, method) (( int32_t (*) (Collection_1_t1470 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m9160_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m9161_gshared (Collection_1_t1470 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m9161(__this, ___value, method) (( bool (*) (Collection_1_t1470 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m9161_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m9162_gshared (Collection_1_t1470 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m9162(__this, ___value, method) (( int32_t (*) (Collection_1_t1470 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m9162_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m9163_gshared (Collection_1_t1470 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m9163(__this, ___index, ___value, method) (( void (*) (Collection_1_t1470 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m9163_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m9164_gshared (Collection_1_t1470 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m9164(__this, ___value, method) (( void (*) (Collection_1_t1470 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m9164_gshared)(__this, ___value, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m9165_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m9165(__this, method) (( Object_t * (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m9165_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m9166_gshared (Collection_1_t1470 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m9166(__this, ___index, method) (( Object_t * (*) (Collection_1_t1470 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m9166_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m9167_gshared (Collection_1_t1470 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m9167(__this, ___index, ___value, method) (( void (*) (Collection_1_t1470 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m9167_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void Collection_1_Add_m9168_gshared (Collection_1_t1470 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_Add_m9168(__this, ___item, method) (( void (*) (Collection_1_t1470 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_Add_m9168_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void Collection_1_Clear_m9169_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_Clear_m9169(__this, method) (( void (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_Clear_m9169_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m9170_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m9170(__this, method) (( void (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_ClearItems_m9170_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m9171_gshared (Collection_1_t1470 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_Contains_m9171(__this, ___item, method) (( bool (*) (Collection_1_t1470 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_Contains_m9171_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m9172_gshared (Collection_1_t1470 * __this, CustomAttributeTypedArgumentU5BU5D_t1278* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m9172(__this, ___array, ___index, method) (( void (*) (Collection_1_t1470 *, CustomAttributeTypedArgumentU5BU5D_t1278*, int32_t, const MethodInfo*))Collection_1_CopyTo_m9172_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m9173_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m9173(__this, method) (( Object_t* (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_GetEnumerator_m9173_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m9174_gshared (Collection_1_t1470 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m9174(__this, ___item, method) (( int32_t (*) (Collection_1_t1470 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_IndexOf_m9174_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m9175_gshared (Collection_1_t1470 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_Insert_m9175(__this, ___index, ___item, method) (( void (*) (Collection_1_t1470 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_Insert_m9175_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m9176_gshared (Collection_1_t1470 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m9176(__this, ___index, ___item, method) (( void (*) (Collection_1_t1470 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_InsertItem_m9176_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m9177_gshared (Collection_1_t1470 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_Remove_m9177(__this, ___item, method) (( bool (*) (Collection_1_t1470 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_Remove_m9177_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m9178_gshared (Collection_1_t1470 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m9178(__this, ___index, method) (( void (*) (Collection_1_t1470 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m9178_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m9179_gshared (Collection_1_t1470 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m9179(__this, ___index, method) (( void (*) (Collection_1_t1470 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m9179_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m9180_gshared (Collection_1_t1470 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m9180(__this, method) (( int32_t (*) (Collection_1_t1470 *, const MethodInfo*))Collection_1_get_Count_m9180_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t838  Collection_1_get_Item_m9181_gshared (Collection_1_t1470 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m9181(__this, ___index, method) (( CustomAttributeTypedArgument_t838  (*) (Collection_1_t1470 *, int32_t, const MethodInfo*))Collection_1_get_Item_m9181_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m9182_gshared (Collection_1_t1470 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m9182(__this, ___index, ___value, method) (( void (*) (Collection_1_t1470 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_set_Item_m9182_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m9183_gshared (Collection_1_t1470 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m9183(__this, ___index, ___item, method) (( void (*) (Collection_1_t1470 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))Collection_1_SetItem_m9183_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m9184_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m9184(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m9184_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeTypedArgument_t838  Collection_1_ConvertItem_m9185_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m9185(__this /* static, unused */, ___item, method) (( CustomAttributeTypedArgument_t838  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m9185_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m9186_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m9186(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m9186_gshared)(__this /* static, unused */, ___list, method)
