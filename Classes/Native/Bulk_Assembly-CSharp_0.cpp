﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Hello
struct Hello_t1;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_Hello.h"
#include "AssemblyU2DCSharp_HelloMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Hello::.ctor()
extern "C" void Hello__ctor_m0 (Hello_t1 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m3(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Hello::Start()
extern "C" void Hello_Start_m1 (Hello_t1 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Hello::Update()
extern "C" void Hello_Update_m2 (Hello_t1 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
