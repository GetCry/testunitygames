﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t916;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
extern "C" void CrossContextChannel__ctor_m5705 (CrossContextChannel_t916 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
