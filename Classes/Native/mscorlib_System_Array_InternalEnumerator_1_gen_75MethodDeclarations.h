﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_75.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9117_gshared (InternalEnumerator_1_t1468 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9117(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1468 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9117_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9118_gshared (InternalEnumerator_1_t1468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9118(__this, method) (( void (*) (InternalEnumerator_1_t1468 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9118_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9119_gshared (InternalEnumerator_1_t1468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9119(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1468 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9119_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9120_gshared (InternalEnumerator_1_t1468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9120(__this, method) (( void (*) (InternalEnumerator_1_t1468 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9120_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9121_gshared (InternalEnumerator_1_t1468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9121(__this, method) (( bool (*) (InternalEnumerator_1_t1468 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9121_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C" CustomAttributeTypedArgument_t838  InternalEnumerator_1_get_Current_m9122_gshared (InternalEnumerator_1_t1468 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9122(__this, method) (( CustomAttributeTypedArgument_t838  (*) (InternalEnumerator_1_t1468 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9122_gshared)(__this, method)
