﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.X509Certificates.PublicKey
struct PublicKey_t249;
// Mono.Security.X509.X509Certificate
struct X509Certificate_t263;
// System.Security.Cryptography.AsnEncodedData
struct AsnEncodedData_t251;
// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t250;
// System.Security.Cryptography.Oid
struct Oid_t252;
// System.Byte[]
struct ByteU5BU5D_t264;
// System.Security.Cryptography.DSA
struct DSA_t377;
// System.Security.Cryptography.RSA
struct RSA_t378;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.X509Certificates.PublicKey::.ctor(Mono.Security.X509.X509Certificate)
extern "C" void PublicKey__ctor_m869 (PublicKey_t249 * __this, X509Certificate_t263 * ___certificate, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedKeyValue()
extern "C" AsnEncodedData_t251 * PublicKey_get_EncodedKeyValue_m870 (PublicKey_t249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsnEncodedData System.Security.Cryptography.X509Certificates.PublicKey::get_EncodedParameters()
extern "C" AsnEncodedData_t251 * PublicKey_get_EncodedParameters_m871 (PublicKey_t249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.AsymmetricAlgorithm System.Security.Cryptography.X509Certificates.PublicKey::get_Key()
extern "C" AsymmetricAlgorithm_t250 * PublicKey_get_Key_m872 (PublicKey_t249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.Oid System.Security.Cryptography.X509Certificates.PublicKey::get_Oid()
extern "C" Oid_t252 * PublicKey_get_Oid_m873 (PublicKey_t249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.X509Certificates.PublicKey::GetUnsignedBigInteger(System.Byte[])
extern "C" ByteU5BU5D_t264* PublicKey_GetUnsignedBigInteger_m874 (Object_t * __this /* static, unused */, ByteU5BU5D_t264* ___integer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.DSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeDSA(System.Byte[],System.Byte[])
extern "C" DSA_t377 * PublicKey_DecodeDSA_m875 (Object_t * __this /* static, unused */, ByteU5BU5D_t264* ___rawPublicKey, ByteU5BU5D_t264* ___rawParameters, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA System.Security.Cryptography.X509Certificates.PublicKey::DecodeRSA(System.Byte[])
extern "C" RSA_t378 * PublicKey_DecodeRSA_m876 (Object_t * __this /* static, unused */, ByteU5BU5D_t264* ___rawPublicKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
