﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t1477;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1279;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1526;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t836;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void Collection_1__ctor_m9283_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1__ctor_m9283(__this, method) (( void (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1__ctor_m9283_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9284_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9284(__this, method) (( bool (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9284_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m9285_gshared (Collection_1_t1477 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m9285(__this, ___array, ___index, method) (( void (*) (Collection_1_t1477 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m9285_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m9286_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m9286(__this, method) (( Object_t * (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m9286_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m9287_gshared (Collection_1_t1477 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m9287(__this, ___value, method) (( int32_t (*) (Collection_1_t1477 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m9287_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m9288_gshared (Collection_1_t1477 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m9288(__this, ___value, method) (( bool (*) (Collection_1_t1477 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m9288_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m9289_gshared (Collection_1_t1477 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m9289(__this, ___value, method) (( int32_t (*) (Collection_1_t1477 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m9289_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m9290_gshared (Collection_1_t1477 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m9290(__this, ___index, ___value, method) (( void (*) (Collection_1_t1477 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m9290_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m9291_gshared (Collection_1_t1477 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m9291(__this, ___value, method) (( void (*) (Collection_1_t1477 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m9291_gshared)(__this, ___value, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m9292_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m9292(__this, method) (( Object_t * (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m9292_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m9293_gshared (Collection_1_t1477 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m9293(__this, ___index, method) (( Object_t * (*) (Collection_1_t1477 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m9293_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m9294_gshared (Collection_1_t1477 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m9294(__this, ___index, ___value, method) (( void (*) (Collection_1_t1477 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m9294_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void Collection_1_Add_m9295_gshared (Collection_1_t1477 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_Add_m9295(__this, ___item, method) (( void (*) (Collection_1_t1477 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_Add_m9295_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void Collection_1_Clear_m9296_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_Clear_m9296(__this, method) (( void (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_Clear_m9296_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m9297_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m9297(__this, method) (( void (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_ClearItems_m9297_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m9298_gshared (Collection_1_t1477 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_Contains_m9298(__this, ___item, method) (( bool (*) (Collection_1_t1477 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_Contains_m9298_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m9299_gshared (Collection_1_t1477 * __this, CustomAttributeNamedArgumentU5BU5D_t1279* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m9299(__this, ___array, ___index, method) (( void (*) (Collection_1_t1477 *, CustomAttributeNamedArgumentU5BU5D_t1279*, int32_t, const MethodInfo*))Collection_1_CopyTo_m9299_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m9300_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m9300(__this, method) (( Object_t* (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_GetEnumerator_m9300_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m9301_gshared (Collection_1_t1477 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m9301(__this, ___item, method) (( int32_t (*) (Collection_1_t1477 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_IndexOf_m9301_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m9302_gshared (Collection_1_t1477 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_Insert_m9302(__this, ___index, ___item, method) (( void (*) (Collection_1_t1477 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_Insert_m9302_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m9303_gshared (Collection_1_t1477 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m9303(__this, ___index, ___item, method) (( void (*) (Collection_1_t1477 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_InsertItem_m9303_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m9304_gshared (Collection_1_t1477 * __this, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_Remove_m9304(__this, ___item, method) (( bool (*) (Collection_1_t1477 *, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_Remove_m9304_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m9305_gshared (Collection_1_t1477 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m9305(__this, ___index, method) (( void (*) (Collection_1_t1477 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m9305_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m9306_gshared (Collection_1_t1477 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m9306(__this, ___index, method) (( void (*) (Collection_1_t1477 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m9306_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m9307_gshared (Collection_1_t1477 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m9307(__this, method) (( int32_t (*) (Collection_1_t1477 *, const MethodInfo*))Collection_1_get_Count_m9307_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t837  Collection_1_get_Item_m9308_gshared (Collection_1_t1477 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m9308(__this, ___index, method) (( CustomAttributeNamedArgument_t837  (*) (Collection_1_t1477 *, int32_t, const MethodInfo*))Collection_1_get_Item_m9308_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m9309_gshared (Collection_1_t1477 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m9309(__this, ___index, ___value, method) (( void (*) (Collection_1_t1477 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_set_Item_m9309_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m9310_gshared (Collection_1_t1477 * __this, int32_t ___index, CustomAttributeNamedArgument_t837  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m9310(__this, ___index, ___item, method) (( void (*) (Collection_1_t1477 *, int32_t, CustomAttributeNamedArgument_t837 , const MethodInfo*))Collection_1_SetItem_m9310_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m9311_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m9311(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m9311_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeNamedArgument_t837  Collection_1_ConvertItem_m9312_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m9312(__this /* static, unused */, ___item, method) (( CustomAttributeNamedArgument_t837  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m9312_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m9313_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m9313(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m9313_gshared)(__this /* static, unused */, ___list, method)
