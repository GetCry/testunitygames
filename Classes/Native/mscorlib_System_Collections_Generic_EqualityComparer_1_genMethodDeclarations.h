﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<System.Object>
struct EqualityComparer_1_t1332;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.ctor()
extern "C" void EqualityComparer_1__ctor_m8013_gshared (EqualityComparer_1_t1332 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m8013(__this, method) (( void (*) (EqualityComparer_1_t1332 *, const MethodInfo*))EqualityComparer_1__ctor_m8013_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Object>::.cctor()
extern "C" void EqualityComparer_1__cctor_m8014_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m8014(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m8014_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m8015_gshared (EqualityComparer_1_t1332 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m8015(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t1332 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m8015_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m8016_gshared (EqualityComparer_1_t1332 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m8016(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t1332 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m8016_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Object>::get_Default()
extern "C" EqualityComparer_1_t1332 * EqualityComparer_1_get_Default_m8017_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m8017(__this /* static, unused */, method) (( EqualityComparer_1_t1332 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m8017_gshared)(__this /* static, unused */, method)
