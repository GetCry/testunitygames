﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t117;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t117 * GcAchievementData_ToAchievement_m469 (GcAchievementData_t108 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void GcAchievementData_t108_marshal(const GcAchievementData_t108& unmarshaled, GcAchievementData_t108_marshaled& marshaled);
extern "C" void GcAchievementData_t108_marshal_back(const GcAchievementData_t108_marshaled& marshaled, GcAchievementData_t108& unmarshaled);
extern "C" void GcAchievementData_t108_marshal_cleanup(GcAchievementData_t108_marshaled& marshaled);
