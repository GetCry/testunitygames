﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.Delegate
struct Delegate_t179;
// System.IAsyncResult
struct IAsyncResult_t41;
// System.AsyncCallback
struct AsyncCallback_t42;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// System.Reflection.EventInfo/AddEventAdapter
struct  AddEventAdapter_t840  : public MulticastDelegate_t40
{
};
