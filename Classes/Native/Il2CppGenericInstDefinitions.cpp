﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t28_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t28_0_0_0_Types[] = { &GcLeaderboard_t28_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t28_0_0_0 = { 1, GenInst_GcLeaderboard_t28_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t171_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t171_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t171_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t171_0_0_0_Types };
extern const Il2CppType Boolean_t180_0_0_0;
static const Il2CppType* GenInst_Boolean_t180_0_0_0_Types[] = { &Boolean_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t180_0_0_0 = { 1, GenInst_Boolean_t180_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t173_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t173_0_0_0_Types[] = { &IAchievementU5BU5D_t173_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t173_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t173_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t121_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t121_0_0_0_Types[] = { &IScoreU5BU5D_t121_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t121_0_0_0 = { 1, GenInst_IScoreU5BU5D_t121_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t116_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t116_0_0_0_Types[] = { &IUserProfileU5BU5D_t116_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t116_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t116_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t180_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t180_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t180_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t33_0_0_0;
static const Il2CppType* GenInst_GUILayer_t33_0_0_0_Types[] = { &GUILayer_t33_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t33_0_0_0 = { 1, GenInst_GUILayer_t33_0_0_0_Types };
extern const Il2CppType PersistentCall_t140_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t140_0_0_0_Types[] = { &PersistentCall_t140_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t140_0_0_0 = { 1, GenInst_PersistentCall_t140_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t138_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t138_0_0_0_Types[] = { &BaseInvokableCall_t138_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t138_0_0_0 = { 1, GenInst_BaseInvokableCall_t138_0_0_0_Types };
extern const Il2CppType Int32_t181_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t181_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t181_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t181_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t181_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t181_0_0_0_Types[] = { &Int32_t181_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t181_0_0_0 = { 1, GenInst_Int32_t181_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t838_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t838_0_0_0_Types[] = { &CustomAttributeTypedArgument_t838_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t838_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t838_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t837_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t837_0_0_0_Types[] = { &CustomAttributeNamedArgument_t837_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t837_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t837_0_0_0_Types };
extern const Il2CppType StrongName_t1088_0_0_0;
static const Il2CppType* GenInst_StrongName_t1088_0_0_0_Types[] = { &StrongName_t1088_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1088_0_0_0 = { 1, GenInst_StrongName_t1088_0_0_0_Types };
extern const Il2CppType DateTime_t118_0_0_0;
static const Il2CppType* GenInst_DateTime_t118_0_0_0_Types[] = { &DateTime_t118_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t118_0_0_0 = { 1, GenInst_DateTime_t118_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1163_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1163_0_0_0_Types[] = { &DateTimeOffset_t1163_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1163_0_0_0 = { 1, GenInst_DateTimeOffset_t1163_0_0_0_Types };
extern const Il2CppType TimeSpan_t278_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t278_0_0_0_Types[] = { &TimeSpan_t278_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t278_0_0_0 = { 1, GenInst_TimeSpan_t278_0_0_0_Types };
extern const Il2CppType Guid_t1185_0_0_0;
static const Il2CppType* GenInst_Guid_t1185_0_0_0_Types[] = { &Guid_t1185_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t1185_0_0_0 = { 1, GenInst_Guid_t1185_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t834_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t834_0_0_0_Types[] = { &CustomAttributeData_t834_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t834_0_0_0 = { 1, GenInst_CustomAttributeData_t834_0_0_0_Types };
extern const Il2CppType Object_t8_0_0_0;
static const Il2CppType* GenInst_Object_t8_0_0_0_Types[] = { &Object_t8_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t8_0_0_0 = { 1, GenInst_Object_t8_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t1637_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t1637_0_0_0_Types[] = { &IAchievementDescription_t1637_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t1637_0_0_0 = { 1, GenInst_IAchievementDescription_t1637_0_0_0_Types };
extern const Il2CppType IAchievement_t159_0_0_0;
static const Il2CppType* GenInst_IAchievement_t159_0_0_0_Types[] = { &IAchievement_t159_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t159_0_0_0 = { 1, GenInst_IAchievement_t159_0_0_0_Types };
extern const Il2CppType IScore_t124_0_0_0;
static const Il2CppType* GenInst_IScore_t124_0_0_0_Types[] = { &IScore_t124_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t124_0_0_0 = { 1, GenInst_IScore_t124_0_0_0_Types };
extern const Il2CppType IUserProfile_t1636_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t1636_0_0_0_Types[] = { &IUserProfile_t1636_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t1636_0_0_0 = { 1, GenInst_IUserProfile_t1636_0_0_0_Types };
extern const Il2CppType AchievementDescription_t119_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t119_0_0_0_Types[] = { &AchievementDescription_t119_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t119_0_0_0 = { 1, GenInst_AchievementDescription_t119_0_0_0_Types };
extern const Il2CppType UserProfile_t115_0_0_0;
static const Il2CppType* GenInst_UserProfile_t115_0_0_0_Types[] = { &UserProfile_t115_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t115_0_0_0 = { 1, GenInst_UserProfile_t115_0_0_0_Types };
extern const Il2CppType IReflect_t1658_0_0_0;
static const Il2CppType* GenInst_IReflect_t1658_0_0_0_Types[] = { &IReflect_t1658_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t1658_0_0_0 = { 1, GenInst_IReflect_t1658_0_0_0_Types };
extern const Il2CppType _Type_t1656_0_0_0;
static const Il2CppType* GenInst__Type_t1656_0_0_0_Types[] = { &_Type_t1656_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t1656_0_0_0 = { 1, GenInst__Type_t1656_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1271_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1271_0_0_0_Types[] = { &ICustomAttributeProvider_t1271_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1271_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1271_0_0_0_Types };
extern const Il2CppType _MemberInfo_t1657_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t1657_0_0_0_Types[] = { &_MemberInfo_t1657_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t1657_0_0_0 = { 1, GenInst__MemberInfo_t1657_0_0_0_Types };
extern const Il2CppType IConvertible_t1276_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1276_0_0_0_Types[] = { &IConvertible_t1276_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1276_0_0_0 = { 1, GenInst_IConvertible_t1276_0_0_0_Types };
extern const Il2CppType IComparable_t1275_0_0_0;
static const Il2CppType* GenInst_IComparable_t1275_0_0_0_Types[] = { &IComparable_t1275_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1275_0_0_0 = { 1, GenInst_IComparable_t1275_0_0_0_Types };
extern const Il2CppType IEnumerable_t384_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t384_0_0_0_Types[] = { &IEnumerable_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t384_0_0_0 = { 1, GenInst_IEnumerable_t384_0_0_0_Types };
extern const Il2CppType ICloneable_t1286_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1286_0_0_0_Types[] = { &ICloneable_t1286_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1286_0_0_0 = { 1, GenInst_ICloneable_t1286_0_0_0_Types };
extern const Il2CppType IComparable_1_t1858_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1858_0_0_0_Types[] = { &IComparable_1_t1858_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1858_0_0_0 = { 1, GenInst_IComparable_1_t1858_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1863_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1863_0_0_0_Types[] = { &IEquatable_1_t1863_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1863_0_0_0 = { 1, GenInst_IEquatable_1_t1863_0_0_0_Types };
extern const Il2CppType GcAchievementData_t108_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t108_0_0_0_Types[] = { &GcAchievementData_t108_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t108_0_0_0 = { 1, GenInst_GcAchievementData_t108_0_0_0_Types };
extern const Il2CppType ValueType_t584_0_0_0;
static const Il2CppType* GenInst_ValueType_t584_0_0_0_Types[] = { &ValueType_t584_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t584_0_0_0 = { 1, GenInst_ValueType_t584_0_0_0_Types };
extern const Il2CppType Achievement_t117_0_0_0;
static const Il2CppType* GenInst_Achievement_t117_0_0_0_Types[] = { &Achievement_t117_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t117_0_0_0 = { 1, GenInst_Achievement_t117_0_0_0_Types };
extern const Il2CppType GcScoreData_t109_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t109_0_0_0_Types[] = { &GcScoreData_t109_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t109_0_0_0 = { 1, GenInst_GcScoreData_t109_0_0_0_Types };
extern const Il2CppType Score_t120_0_0_0;
static const Il2CppType* GenInst_Score_t120_0_0_0_Types[] = { &Score_t120_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t120_0_0_0 = { 1, GenInst_Score_t120_0_0_0_Types };
extern const Il2CppType Camera_t73_0_0_0;
static const Il2CppType* GenInst_Camera_t73_0_0_0_Types[] = { &Camera_t73_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t73_0_0_0 = { 1, GenInst_Camera_t73_0_0_0_Types };
extern const Il2CppType Behaviour_t32_0_0_0;
static const Il2CppType* GenInst_Behaviour_t32_0_0_0_Types[] = { &Behaviour_t32_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t32_0_0_0 = { 1, GenInst_Behaviour_t32_0_0_0_Types };
extern const Il2CppType Component_t71_0_0_0;
static const Il2CppType* GenInst_Component_t71_0_0_0_Types[] = { &Component_t71_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t71_0_0_0 = { 1, GenInst_Component_t71_0_0_0_Types };
extern const Il2CppType Display_t76_0_0_0;
static const Il2CppType* GenInst_Display_t76_0_0_0_Types[] = { &Display_t76_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t76_0_0_0 = { 1, GenInst_Display_t76_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t1305_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1305_0_0_0_Types[] = { &ISerializable_t1305_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1305_0_0_0 = { 1, GenInst_ISerializable_t1305_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t180_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_Types };
extern const Il2CppType Single_t177_0_0_0;
static const Il2CppType* GenInst_Single_t177_0_0_0_Types[] = { &Single_t177_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t177_0_0_0 = { 1, GenInst_Single_t177_0_0_0_Types };
extern const Il2CppType IFormattable_t1273_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1273_0_0_0_Types[] = { &IFormattable_t1273_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1273_0_0_0 = { 1, GenInst_IFormattable_t1273_0_0_0_Types };
extern const Il2CppType IComparable_1_t1916_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1916_0_0_0_Types[] = { &IComparable_1_t1916_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1916_0_0_0 = { 1, GenInst_IComparable_1_t1916_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1921_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1921_0_0_0_Types[] = { &IEquatable_1_t1921_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1921_0_0_0 = { 1, GenInst_IEquatable_1_t1921_0_0_0_Types };
extern const Il2CppType Keyframe_t93_0_0_0;
static const Il2CppType* GenInst_Keyframe_t93_0_0_0_Types[] = { &Keyframe_t93_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t93_0_0_0 = { 1, GenInst_Keyframe_t93_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t100_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t100_0_0_0_Types[] = { &DisallowMultipleComponent_t100_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t100_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t100_0_0_0_Types };
extern const Il2CppType Attribute_t64_0_0_0;
static const Il2CppType* GenInst_Attribute_t64_0_0_0_Types[] = { &Attribute_t64_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t64_0_0_0 = { 1, GenInst_Attribute_t64_0_0_0_Types };
extern const Il2CppType _Attribute_t1646_0_0_0;
static const Il2CppType* GenInst__Attribute_t1646_0_0_0_Types[] = { &_Attribute_t1646_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t1646_0_0_0 = { 1, GenInst__Attribute_t1646_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t102_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t102_0_0_0_Types[] = { &ExecuteInEditMode_t102_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t102_0_0_0 = { 1, GenInst_ExecuteInEditMode_t102_0_0_0_Types };
extern const Il2CppType RequireComponent_t101_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t101_0_0_0_Types[] = { &RequireComponent_t101_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t101_0_0_0 = { 1, GenInst_RequireComponent_t101_0_0_0_Types };
extern const Il2CppType ParameterModifier_t857_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t857_0_0_0_Types[] = { &ParameterModifier_t857_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t857_0_0_0 = { 1, GenInst_ParameterModifier_t857_0_0_0_Types };
extern const Il2CppType HitInfo_t125_0_0_0;
static const Il2CppType* GenInst_HitInfo_t125_0_0_0_Types[] = { &HitInfo_t125_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t125_0_0_0 = { 1, GenInst_HitInfo_t125_0_0_0_Types };
extern const Il2CppType Char_t395_0_0_0;
static const Il2CppType* GenInst_Char_t395_0_0_0_Types[] = { &Char_t395_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t395_0_0_0 = { 1, GenInst_Char_t395_0_0_0_Types };
extern const Il2CppType IComparable_1_t1958_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1958_0_0_0_Types[] = { &IComparable_1_t1958_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1958_0_0_0 = { 1, GenInst_IComparable_1_t1958_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1963_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1963_0_0_0_Types[] = { &IEquatable_1_t1963_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1963_0_0_0 = { 1, GenInst_IEquatable_1_t1963_0_0_0_Types };
extern const Il2CppType ParameterInfo_t195_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t195_0_0_0_Types[] = { &ParameterInfo_t195_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t195_0_0_0 = { 1, GenInst_ParameterInfo_t195_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t1695_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t1695_0_0_0_Types[] = { &_ParameterInfo_t1695_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t1695_0_0_0 = { 1, GenInst__ParameterInfo_t1695_0_0_0_Types };
extern const Il2CppType UInt16_t398_0_0_0;
static const Il2CppType* GenInst_UInt16_t398_0_0_0_Types[] = { &UInt16_t398_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t398_0_0_0 = { 1, GenInst_UInt16_t398_0_0_0_Types };
extern const Il2CppType IComparable_1_t1981_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1981_0_0_0_Types[] = { &IComparable_1_t1981_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1981_0_0_0 = { 1, GenInst_IComparable_1_t1981_0_0_0_Types };
extern const Il2CppType IEquatable_1_t1986_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t1986_0_0_0_Types[] = { &IEquatable_1_t1986_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t1986_0_0_0 = { 1, GenInst_IEquatable_1_t1986_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1374_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1374_0_0_0_Types[] = { &KeyValuePair_2_t1374_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1374_0_0_0 = { 1, GenInst_KeyValuePair_2_t1374_0_0_0_Types };
extern const Il2CppType IComparable_1_t1998_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1998_0_0_0_Types[] = { &IComparable_1_t1998_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1998_0_0_0 = { 1, GenInst_IComparable_1_t1998_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2003_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2003_0_0_0_Types[] = { &IEquatable_1_t2003_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2003_0_0_0 = { 1, GenInst_IEquatable_1_t2003_0_0_0_Types };
extern const Il2CppType Link_t694_0_0_0;
static const Il2CppType* GenInst_Link_t694_0_0_0_Types[] = { &Link_t694_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t694_0_0_0 = { 1, GenInst_Link_t694_0_0_0_Types };
extern const Il2CppType IComparable_1_t2014_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2014_0_0_0_Types[] = { &IComparable_1_t2014_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2014_0_0_0 = { 1, GenInst_IComparable_1_t2014_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2019_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2019_0_0_0_Types[] = { &IEquatable_1_t2019_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2019_0_0_0 = { 1, GenInst_IEquatable_1_t2019_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t376_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t180_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t376_0_0_0_Types[] = { &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t376_0_0_0 = { 1, GenInst_DictionaryEntry_t376_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_KeyValuePair_2_t1374_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t180_0_0_0, &KeyValuePair_2_t1374_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_KeyValuePair_2_t1374_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_KeyValuePair_2_t1374_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t180_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1386_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1386_0_0_0_Types[] = { &KeyValuePair_2_t1386_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1386_0_0_0 = { 1, GenInst_KeyValuePair_2_t1386_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t181_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t181_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t181_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t181_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1391_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1391_0_0_0_Types[] = { &KeyValuePair_2_t1391_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1391_0_0_0 = { 1, GenInst_KeyValuePair_2_t1391_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t181_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t181_0_0_0_KeyValuePair_2_t1391_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t181_0_0_0, &KeyValuePair_2_t1391_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t181_0_0_0_KeyValuePair_2_t1391_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t181_0_0_0_KeyValuePair_2_t1391_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t181_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1399_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1399_0_0_0_Types[] = { &KeyValuePair_2_t1399_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1399_0_0_0 = { 1, GenInst_KeyValuePair_2_t1399_0_0_0_Types };
extern const Il2CppType Byte_t419_0_0_0;
static const Il2CppType* GenInst_Byte_t419_0_0_0_Types[] = { &Byte_t419_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t419_0_0_0 = { 1, GenInst_Byte_t419_0_0_0_Types };
extern const Il2CppType IComparable_1_t2038_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2038_0_0_0_Types[] = { &IComparable_1_t2038_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2038_0_0_0 = { 1, GenInst_IComparable_1_t2038_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2043_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2043_0_0_0_Types[] = { &IEquatable_1_t2043_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2043_0_0_0 = { 1, GenInst_IEquatable_1_t2043_0_0_0_Types };
extern const Il2CppType X509Certificate_t261_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t261_0_0_0_Types[] = { &X509Certificate_t261_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t261_0_0_0 = { 1, GenInst_X509Certificate_t261_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1308_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1308_0_0_0_Types[] = { &IDeserializationCallback_t1308_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1308_0_0_0 = { 1, GenInst_IDeserializationCallback_t1308_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t275_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t275_0_0_0_Types[] = { &X509ChainStatus_t275_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t275_0_0_0 = { 1, GenInst_X509ChainStatus_t275_0_0_0_Types };
extern const Il2CppType Capture_t297_0_0_0;
static const Il2CppType* GenInst_Capture_t297_0_0_0_Types[] = { &Capture_t297_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t297_0_0_0 = { 1, GenInst_Capture_t297_0_0_0_Types };
extern const Il2CppType Group_t300_0_0_0;
static const Il2CppType* GenInst_Group_t300_0_0_0_Types[] = { &Group_t300_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t300_0_0_0 = { 1, GenInst_Group_t300_0_0_0_Types };
extern const Il2CppType Mark_t326_0_0_0;
static const Il2CppType* GenInst_Mark_t326_0_0_0_Types[] = { &Mark_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t326_0_0_0 = { 1, GenInst_Mark_t326_0_0_0_Types };
extern const Il2CppType UriScheme_t363_0_0_0;
static const Il2CppType* GenInst_UriScheme_t363_0_0_0_Types[] = { &UriScheme_t363_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t363_0_0_0 = { 1, GenInst_UriScheme_t363_0_0_0_Types };
extern const Il2CppType KeySizes_t446_0_0_0;
static const Il2CppType* GenInst_KeySizes_t446_0_0_0_Types[] = { &KeySizes_t446_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t446_0_0_0 = { 1, GenInst_KeySizes_t446_0_0_0_Types };
extern const Il2CppType UInt32_t189_0_0_0;
static const Il2CppType* GenInst_UInt32_t189_0_0_0_Types[] = { &UInt32_t189_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t189_0_0_0 = { 1, GenInst_UInt32_t189_0_0_0_Types };
extern const Il2CppType IComparable_1_t2080_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2080_0_0_0_Types[] = { &IComparable_1_t2080_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2080_0_0_0 = { 1, GenInst_IComparable_1_t2080_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2085_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2085_0_0_0_Types[] = { &IEquatable_1_t2085_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2085_0_0_0 = { 1, GenInst_IEquatable_1_t2085_0_0_0_Types };
extern const Il2CppType BigInteger_t451_0_0_0;
static const Il2CppType* GenInst_BigInteger_t451_0_0_0_Types[] = { &BigInteger_t451_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t451_0_0_0 = { 1, GenInst_BigInteger_t451_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t264_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t264_0_0_0_Types[] = { &ByteU5BU5D_t264_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t264_0_0_0 = { 1, GenInst_ByteU5BU5D_t264_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t386_0_0_0;
static const Il2CppType* GenInst_ICollection_t386_0_0_0_Types[] = { &ICollection_t386_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t386_0_0_0 = { 1, GenInst_ICollection_t386_0_0_0_Types };
extern const Il2CppType IList_t335_0_0_0;
static const Il2CppType* GenInst_IList_t335_0_0_0_Types[] = { &IList_t335_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t335_0_0_0 = { 1, GenInst_IList_t335_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t539_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t539_0_0_0_Types[] = { &ClientCertificateType_t539_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t539_0_0_0 = { 1, GenInst_ClientCertificateType_t539_0_0_0_Types };
extern const Il2CppType Enum_t422_0_0_0;
static const Il2CppType* GenInst_Enum_t422_0_0_0_Types[] = { &Enum_t422_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t422_0_0_0 = { 1, GenInst_Enum_t422_0_0_0_Types };
extern const Il2CppType Int64_t188_0_0_0;
static const Il2CppType* GenInst_Int64_t188_0_0_0_Types[] = { &Int64_t188_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t188_0_0_0 = { 1, GenInst_Int64_t188_0_0_0_Types };
extern const Il2CppType UInt64_t589_0_0_0;
static const Il2CppType* GenInst_UInt64_t589_0_0_0_Types[] = { &UInt64_t589_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t589_0_0_0 = { 1, GenInst_UInt64_t589_0_0_0_Types };
extern const Il2CppType SByte_t590_0_0_0;
static const Il2CppType* GenInst_SByte_t590_0_0_0_Types[] = { &SByte_t590_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t590_0_0_0 = { 1, GenInst_SByte_t590_0_0_0_Types };
extern const Il2CppType Int16_t591_0_0_0;
static const Il2CppType* GenInst_Int16_t591_0_0_0_Types[] = { &Int16_t591_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t591_0_0_0 = { 1, GenInst_Int16_t591_0_0_0_Types };
extern const Il2CppType Double_t187_0_0_0;
static const Il2CppType* GenInst_Double_t187_0_0_0_Types[] = { &Double_t187_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t187_0_0_0 = { 1, GenInst_Double_t187_0_0_0_Types };
extern const Il2CppType Decimal_t592_0_0_0;
static const Il2CppType* GenInst_Decimal_t592_0_0_0_Types[] = { &Decimal_t592_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t592_0_0_0 = { 1, GenInst_Decimal_t592_0_0_0_Types };
extern const Il2CppType Delegate_t179_0_0_0;
static const Il2CppType* GenInst_Delegate_t179_0_0_0_Types[] = { &Delegate_t179_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t179_0_0_0 = { 1, GenInst_Delegate_t179_0_0_0_Types };
extern const Il2CppType IComparable_1_t2119_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2119_0_0_0_Types[] = { &IComparable_1_t2119_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2119_0_0_0 = { 1, GenInst_IComparable_1_t2119_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2120_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2120_0_0_0_Types[] = { &IEquatable_1_t2120_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2120_0_0_0 = { 1, GenInst_IEquatable_1_t2120_0_0_0_Types };
extern const Il2CppType IComparable_1_t2123_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2123_0_0_0_Types[] = { &IComparable_1_t2123_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2123_0_0_0 = { 1, GenInst_IComparable_1_t2123_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2124_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2124_0_0_0_Types[] = { &IEquatable_1_t2124_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2124_0_0_0 = { 1, GenInst_IEquatable_1_t2124_0_0_0_Types };
extern const Il2CppType IComparable_1_t2121_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2121_0_0_0_Types[] = { &IComparable_1_t2121_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2121_0_0_0 = { 1, GenInst_IComparable_1_t2121_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2122_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2122_0_0_0_Types[] = { &IEquatable_1_t2122_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2122_0_0_0 = { 1, GenInst_IEquatable_1_t2122_0_0_0_Types };
extern const Il2CppType IComparable_1_t2117_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2117_0_0_0_Types[] = { &IComparable_1_t2117_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2117_0_0_0 = { 1, GenInst_IComparable_1_t2117_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2118_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2118_0_0_0_Types[] = { &IEquatable_1_t2118_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2118_0_0_0 = { 1, GenInst_IEquatable_1_t2118_0_0_0_Types };
extern const Il2CppType IComparable_1_t2125_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2125_0_0_0_Types[] = { &IComparable_1_t2125_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2125_0_0_0 = { 1, GenInst_IComparable_1_t2125_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2126_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2126_0_0_0_Types[] = { &IEquatable_1_t2126_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2126_0_0_0 = { 1, GenInst_IEquatable_1_t2126_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t1691_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t1691_0_0_0_Types[] = { &_FieldInfo_t1691_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t1691_0_0_0 = { 1, GenInst__FieldInfo_t1691_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t1693_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t1693_0_0_0_Types[] = { &_MethodInfo_t1693_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t1693_0_0_0 = { 1, GenInst__MethodInfo_t1693_0_0_0_Types };
extern const Il2CppType MethodBase_t193_0_0_0;
static const Il2CppType* GenInst_MethodBase_t193_0_0_0_Types[] = { &MethodBase_t193_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t193_0_0_0 = { 1, GenInst_MethodBase_t193_0_0_0_Types };
extern const Il2CppType _MethodBase_t1692_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1692_0_0_0_Types[] = { &_MethodBase_t1692_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1692_0_0_0 = { 1, GenInst__MethodBase_t1692_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t783_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t783_0_0_0_Types[] = { &ConstructorInfo_t783_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t783_0_0_0 = { 1, GenInst_ConstructorInfo_t783_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t1689_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t1689_0_0_0_Types[] = { &_ConstructorInfo_t1689_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1689_0_0_0 = { 1, GenInst__ConstructorInfo_t1689_0_0_0_Types };
extern const Il2CppType TableRange_t627_0_0_0;
static const Il2CppType* GenInst_TableRange_t627_0_0_0_Types[] = { &TableRange_t627_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t627_0_0_0 = { 1, GenInst_TableRange_t627_0_0_0_Types };
extern const Il2CppType TailoringInfo_t630_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t630_0_0_0_Types[] = { &TailoringInfo_t630_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t630_0_0_0 = { 1, GenInst_TailoringInfo_t630_0_0_0_Types };
extern const Il2CppType Contraction_t631_0_0_0;
static const Il2CppType* GenInst_Contraction_t631_0_0_0_Types[] = { &Contraction_t631_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t631_0_0_0 = { 1, GenInst_Contraction_t631_0_0_0_Types };
extern const Il2CppType Level2Map_t633_0_0_0;
static const Il2CppType* GenInst_Level2Map_t633_0_0_0_Types[] = { &Level2Map_t633_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t633_0_0_0 = { 1, GenInst_Level2Map_t633_0_0_0_Types };
extern const Il2CppType BigInteger_t654_0_0_0;
static const Il2CppType* GenInst_BigInteger_t654_0_0_0_Types[] = { &BigInteger_t654_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t654_0_0_0 = { 1, GenInst_BigInteger_t654_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1445_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1445_0_0_0_Types[] = { &KeyValuePair_2_t1445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1445_0_0_0 = { 1, GenInst_KeyValuePair_2_t1445_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t376_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1445_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1445_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1445_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1445_0_0_0_Types };
extern const Il2CppType Slot_t704_0_0_0;
static const Il2CppType* GenInst_Slot_t704_0_0_0_Types[] = { &Slot_t704_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t704_0_0_0 = { 1, GenInst_Slot_t704_0_0_0_Types };
extern const Il2CppType Slot_t712_0_0_0;
static const Il2CppType* GenInst_Slot_t712_0_0_0_Types[] = { &Slot_t712_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t712_0_0_0 = { 1, GenInst_Slot_t712_0_0_0_Types };
extern const Il2CppType StackFrame_t192_0_0_0;
static const Il2CppType* GenInst_StackFrame_t192_0_0_0_Types[] = { &StackFrame_t192_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t192_0_0_0 = { 1, GenInst_StackFrame_t192_0_0_0_Types };
extern const Il2CppType Calendar_t725_0_0_0;
static const Il2CppType* GenInst_Calendar_t725_0_0_0_Types[] = { &Calendar_t725_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t725_0_0_0 = { 1, GenInst_Calendar_t725_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t803_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t803_0_0_0_Types[] = { &ModuleBuilder_t803_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t803_0_0_0 = { 1, GenInst_ModuleBuilder_t803_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t1684_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t1684_0_0_0_Types[] = { &_ModuleBuilder_t1684_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t1684_0_0_0 = { 1, GenInst__ModuleBuilder_t1684_0_0_0_Types };
extern const Il2CppType Module_t799_0_0_0;
static const Il2CppType* GenInst_Module_t799_0_0_0_Types[] = { &Module_t799_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t799_0_0_0 = { 1, GenInst_Module_t799_0_0_0_Types };
extern const Il2CppType _Module_t1694_0_0_0;
static const Il2CppType* GenInst__Module_t1694_0_0_0_Types[] = { &_Module_t1694_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t1694_0_0_0 = { 1, GenInst__Module_t1694_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t809_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t809_0_0_0_Types[] = { &ParameterBuilder_t809_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t809_0_0_0 = { 1, GenInst_ParameterBuilder_t809_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t1685_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t1685_0_0_0_Types[] = { &_ParameterBuilder_t1685_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t1685_0_0_0 = { 1, GenInst__ParameterBuilder_t1685_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t163_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t163_0_0_0_Types[] = { &TypeU5BU5D_t163_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t163_0_0_0 = { 1, GenInst_TypeU5BU5D_t163_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t793_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t793_0_0_0_Types[] = { &ILTokenInfo_t793_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t793_0_0_0 = { 1, GenInst_ILTokenInfo_t793_0_0_0_Types };
extern const Il2CppType LabelData_t795_0_0_0;
static const Il2CppType* GenInst_LabelData_t795_0_0_0_Types[] = { &LabelData_t795_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t795_0_0_0 = { 1, GenInst_LabelData_t795_0_0_0_Types };
extern const Il2CppType LabelFixup_t794_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t794_0_0_0_Types[] = { &LabelFixup_t794_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t794_0_0_0 = { 1, GenInst_LabelFixup_t794_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t791_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t791_0_0_0_Types[] = { &GenericTypeParameterBuilder_t791_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t791_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t791_0_0_0_Types };
extern const Il2CppType TypeBuilder_t785_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t785_0_0_0_Types[] = { &TypeBuilder_t785_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t785_0_0_0 = { 1, GenInst_TypeBuilder_t785_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t1686_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t1686_0_0_0_Types[] = { &_TypeBuilder_t1686_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t1686_0_0_0 = { 1, GenInst__TypeBuilder_t1686_0_0_0_Types };
extern const Il2CppType MethodBuilder_t792_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t792_0_0_0_Types[] = { &MethodBuilder_t792_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t792_0_0_0 = { 1, GenInst_MethodBuilder_t792_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t1683_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t1683_0_0_0_Types[] = { &_MethodBuilder_t1683_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t1683_0_0_0 = { 1, GenInst__MethodBuilder_t1683_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t782_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t782_0_0_0_Types[] = { &ConstructorBuilder_t782_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t782_0_0_0 = { 1, GenInst_ConstructorBuilder_t782_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t1679_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t1679_0_0_0_Types[] = { &_ConstructorBuilder_t1679_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t1679_0_0_0 = { 1, GenInst__ConstructorBuilder_t1679_0_0_0_Types };
extern const Il2CppType FieldBuilder_t789_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t789_0_0_0_Types[] = { &FieldBuilder_t789_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t789_0_0_0 = { 1, GenInst_FieldBuilder_t789_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t1681_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t1681_0_0_0_Types[] = { &_FieldBuilder_t1681_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t1681_0_0_0 = { 1, GenInst__FieldBuilder_t1681_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t1696_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t1696_0_0_0_Types[] = { &_PropertyInfo_t1696_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t1696_0_0_0 = { 1, GenInst__PropertyInfo_t1696_0_0_0_Types };
extern const Il2CppType ResourceInfo_t868_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t868_0_0_0_Types[] = { &ResourceInfo_t868_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t868_0_0_0 = { 1, GenInst_ResourceInfo_t868_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t869_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t869_0_0_0_Types[] = { &ResourceCacheItem_t869_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t869_0_0_0 = { 1, GenInst_ResourceCacheItem_t869_0_0_0_Types };
extern const Il2CppType IContextProperty_t1265_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1265_0_0_0_Types[] = { &IContextProperty_t1265_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1265_0_0_0 = { 1, GenInst_IContextProperty_t1265_0_0_0_Types };
extern const Il2CppType Header_t951_0_0_0;
static const Il2CppType* GenInst_Header_t951_0_0_0_Types[] = { &Header_t951_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t951_0_0_0 = { 1, GenInst_Header_t951_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1300_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1300_0_0_0_Types[] = { &ITrackingHandler_t1300_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1300_0_0_0 = { 1, GenInst_ITrackingHandler_t1300_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1287_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1287_0_0_0_Types[] = { &IContextAttribute_t1287_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1287_0_0_0 = { 1, GenInst_IContextAttribute_t1287_0_0_0_Types };
extern const Il2CppType IComparable_1_t2370_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2370_0_0_0_Types[] = { &IComparable_1_t2370_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2370_0_0_0 = { 1, GenInst_IComparable_1_t2370_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2375_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2375_0_0_0_Types[] = { &IEquatable_1_t2375_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2375_0_0_0 = { 1, GenInst_IEquatable_1_t2375_0_0_0_Types };
extern const Il2CppType IComparable_1_t2127_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2127_0_0_0_Types[] = { &IComparable_1_t2127_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2127_0_0_0 = { 1, GenInst_IComparable_1_t2127_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2128_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2128_0_0_0_Types[] = { &IEquatable_1_t2128_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2128_0_0_0 = { 1, GenInst_IEquatable_1_t2128_0_0_0_Types };
extern const Il2CppType IComparable_1_t2394_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2394_0_0_0_Types[] = { &IComparable_1_t2394_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2394_0_0_0 = { 1, GenInst_IComparable_1_t2394_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2399_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2399_0_0_0_Types[] = { &IEquatable_1_t2399_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2399_0_0_0 = { 1, GenInst_IEquatable_1_t2399_0_0_0_Types };
extern const Il2CppType TypeTag_t1007_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1007_0_0_0_Types[] = { &TypeTag_t1007_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1007_0_0_0 = { 1, GenInst_TypeTag_t1007_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t232_0_0_0;
static const Il2CppType* GenInst_Version_t232_0_0_0_Types[] = { &Version_t232_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t232_0_0_0 = { 1, GenInst_Version_t232_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t376_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &DictionaryEntry_t376_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t376_0_0_0_DictionaryEntry_t376_0_0_0 = { 2, GenInst_DictionaryEntry_t376_0_0_0_DictionaryEntry_t376_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1374_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1374_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1374_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1374_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1374_0_0_0_KeyValuePair_2_t1374_0_0_0_Types[] = { &KeyValuePair_2_t1374_0_0_0, &KeyValuePair_2_t1374_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1374_0_0_0_KeyValuePair_2_t1374_0_0_0 = { 2, GenInst_KeyValuePair_2_t1374_0_0_0_KeyValuePair_2_t1374_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1391_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1391_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1391_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1391_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1391_0_0_0_KeyValuePair_2_t1391_0_0_0_Types[] = { &KeyValuePair_2_t1391_0_0_0, &KeyValuePair_2_t1391_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1391_0_0_0_KeyValuePair_2_t1391_0_0_0 = { 2, GenInst_KeyValuePair_2_t1391_0_0_0_KeyValuePair_2_t1391_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1445_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1445_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1445_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1445_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1445_0_0_0_KeyValuePair_2_t1445_0_0_0_Types[] = { &KeyValuePair_2_t1445_0_0_0, &KeyValuePair_2_t1445_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1445_0_0_0_KeyValuePair_2_t1445_0_0_0 = { 2, GenInst_KeyValuePair_2_t1445_0_0_0_KeyValuePair_2_t1445_0_0_0_Types };
extern const Il2CppType Stack_1_t1643_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t1643_gp_0_0_0_0_Types[] = { &Stack_1_t1643_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t1643_gp_0_0_0_0 = { 1, GenInst_Stack_1_t1643_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1644_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1644_gp_0_0_0_0_Types[] = { &Enumerator_t1644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1644_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1644_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1650_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1650_gp_0_0_0_0_Types[] = { &IEnumerable_1_t1650_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1650_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1650_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10266_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10266_gp_0_0_0_0_Array_Sort_m10266_gp_0_0_0_0_Types[] = { &Array_Sort_m10266_gp_0_0_0_0, &Array_Sort_m10266_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10266_gp_0_0_0_0_Array_Sort_m10266_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m10266_gp_0_0_0_0_Array_Sort_m10266_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10267_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m10267_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10267_gp_0_0_0_0_Array_Sort_m10267_gp_1_0_0_0_Types[] = { &Array_Sort_m10267_gp_0_0_0_0, &Array_Sort_m10267_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10267_gp_0_0_0_0_Array_Sort_m10267_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m10267_gp_0_0_0_0_Array_Sort_m10267_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m10268_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10268_gp_0_0_0_0_Types[] = { &Array_Sort_m10268_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10268_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10268_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m10268_gp_0_0_0_0_Array_Sort_m10268_gp_0_0_0_0_Types[] = { &Array_Sort_m10268_gp_0_0_0_0, &Array_Sort_m10268_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10268_gp_0_0_0_0_Array_Sort_m10268_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m10268_gp_0_0_0_0_Array_Sort_m10268_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10269_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10269_gp_0_0_0_0_Types[] = { &Array_Sort_m10269_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10269_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10269_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10269_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10269_gp_0_0_0_0_Array_Sort_m10269_gp_1_0_0_0_Types[] = { &Array_Sort_m10269_gp_0_0_0_0, &Array_Sort_m10269_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10269_gp_0_0_0_0_Array_Sort_m10269_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m10269_gp_0_0_0_0_Array_Sort_m10269_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m10270_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10270_gp_0_0_0_0_Array_Sort_m10270_gp_0_0_0_0_Types[] = { &Array_Sort_m10270_gp_0_0_0_0, &Array_Sort_m10270_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10270_gp_0_0_0_0_Array_Sort_m10270_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m10270_gp_0_0_0_0_Array_Sort_m10270_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10271_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m10271_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10271_gp_0_0_0_0_Array_Sort_m10271_gp_1_0_0_0_Types[] = { &Array_Sort_m10271_gp_0_0_0_0, &Array_Sort_m10271_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10271_gp_0_0_0_0_Array_Sort_m10271_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m10271_gp_0_0_0_0_Array_Sort_m10271_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m10272_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10272_gp_0_0_0_0_Types[] = { &Array_Sort_m10272_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10272_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10272_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m10272_gp_0_0_0_0_Array_Sort_m10272_gp_0_0_0_0_Types[] = { &Array_Sort_m10272_gp_0_0_0_0, &Array_Sort_m10272_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10272_gp_0_0_0_0_Array_Sort_m10272_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m10272_gp_0_0_0_0_Array_Sort_m10272_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10273_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10273_gp_0_0_0_0_Types[] = { &Array_Sort_m10273_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10273_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10273_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10273_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10273_gp_1_0_0_0_Types[] = { &Array_Sort_m10273_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10273_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m10273_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m10273_gp_0_0_0_0_Array_Sort_m10273_gp_1_0_0_0_Types[] = { &Array_Sort_m10273_gp_0_0_0_0, &Array_Sort_m10273_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10273_gp_0_0_0_0_Array_Sort_m10273_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m10273_gp_0_0_0_0_Array_Sort_m10273_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m10274_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10274_gp_0_0_0_0_Types[] = { &Array_Sort_m10274_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10274_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10274_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m10275_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m10275_gp_0_0_0_0_Types[] = { &Array_Sort_m10275_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m10275_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m10275_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m10276_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m10276_gp_0_0_0_0_Types[] = { &Array_qsort_m10276_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m10276_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m10276_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m10276_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m10276_gp_0_0_0_0_Array_qsort_m10276_gp_1_0_0_0_Types[] = { &Array_qsort_m10276_gp_0_0_0_0, &Array_qsort_m10276_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m10276_gp_0_0_0_0_Array_qsort_m10276_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m10276_gp_0_0_0_0_Array_qsort_m10276_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m10277_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m10277_gp_0_0_0_0_Types[] = { &Array_compare_m10277_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m10277_gp_0_0_0_0 = { 1, GenInst_Array_compare_m10277_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m10278_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m10278_gp_0_0_0_0_Types[] = { &Array_qsort_m10278_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m10278_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m10278_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m10281_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m10281_gp_0_0_0_0_Types[] = { &Array_Resize_m10281_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m10281_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m10281_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m10283_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m10283_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m10283_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m10283_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m10283_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m10284_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m10284_gp_0_0_0_0_Types[] = { &Array_ForEach_m10284_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m10284_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m10284_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m10285_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m10285_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m10285_gp_0_0_0_0_Array_ConvertAll_m10285_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m10285_gp_0_0_0_0, &Array_ConvertAll_m10285_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m10285_gp_0_0_0_0_Array_ConvertAll_m10285_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m10285_gp_0_0_0_0_Array_ConvertAll_m10285_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m10286_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m10286_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m10286_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10286_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m10286_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m10287_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m10287_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m10287_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10287_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m10287_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m10288_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m10288_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m10288_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m10288_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m10288_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m10289_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m10289_gp_0_0_0_0_Types[] = { &Array_FindIndex_m10289_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10289_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m10289_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m10290_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m10290_gp_0_0_0_0_Types[] = { &Array_FindIndex_m10290_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10290_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m10290_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m10291_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m10291_gp_0_0_0_0_Types[] = { &Array_FindIndex_m10291_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m10291_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m10291_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m10292_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m10292_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m10292_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10292_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m10292_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m10293_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m10293_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m10293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10293_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m10293_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m10294_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m10294_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m10294_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10294_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m10294_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m10295_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m10295_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m10295_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m10295_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m10295_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m10296_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m10296_gp_0_0_0_0_Types[] = { &Array_IndexOf_m10296_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10296_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m10296_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m10297_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m10297_gp_0_0_0_0_Types[] = { &Array_IndexOf_m10297_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10297_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m10297_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m10298_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m10298_gp_0_0_0_0_Types[] = { &Array_IndexOf_m10298_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m10298_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m10298_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m10299_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m10299_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m10299_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10299_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m10299_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m10300_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m10300_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m10300_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10300_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m10300_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m10301_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m10301_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m10301_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m10301_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m10301_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m10302_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m10302_gp_0_0_0_0_Types[] = { &Array_FindAll_m10302_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m10302_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m10302_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m10303_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m10303_gp_0_0_0_0_Types[] = { &Array_Exists_m10303_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m10303_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m10303_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m10304_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m10304_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m10304_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m10304_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m10304_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m10305_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m10305_gp_0_0_0_0_Types[] = { &Array_Find_m10305_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m10305_gp_0_0_0_0 = { 1, GenInst_Array_Find_m10305_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m10306_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m10306_gp_0_0_0_0_Types[] = { &Array_FindLast_m10306_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m10306_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m10306_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t1651_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t1651_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t1651_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t1651_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t1651_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t1652_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t1652_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t1652_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1652_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t1652_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t1654_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t1654_gp_0_0_0_0_Types[] = { &IList_1_t1654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1654_gp_0_0_0_0 = { 1, GenInst_IList_1_t1654_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1655_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1655_gp_0_0_0_0_Types[] = { &ICollection_1_t1655_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1655_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1655_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1277_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1277_gp_0_0_0_0_Types[] = { &Nullable_1_t1277_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1277_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1277_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t1662_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t1662_gp_0_0_0_0_Types[] = { &Comparer_1_t1662_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t1662_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1662_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1663_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1663_gp_0_0_0_0_Types[] = { &DefaultComparer_t1663_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1663_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1663_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t1632_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t1632_gp_0_0_0_0_Types[] = { &GenericComparer_1_t1632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t1632_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t1632_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1664_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t1664_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0, &Dictionary_2_t1664_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2494_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2494_0_0_0_Types[] = { &KeyValuePair_2_t2494_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2494_0_0_0 = { 1, GenInst_KeyValuePair_2_t2494_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0, &Dictionary_2_t1664_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0, &Dictionary_2_t1664_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_DictionaryEntry_t376_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0, &Dictionary_2_t1664_gp_1_0_0_0, &DictionaryEntry_t376_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_DictionaryEntry_t376_0_0_0 = { 3, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_DictionaryEntry_t376_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t1665_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1665_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t1665_gp_0_0_0_0_ShimEnumerator_t1665_gp_1_0_0_0_Types[] = { &ShimEnumerator_t1665_gp_0_0_0_0, &ShimEnumerator_t1665_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1665_gp_0_0_0_0_ShimEnumerator_t1665_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1665_gp_0_0_0_0_ShimEnumerator_t1665_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1666_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1666_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1666_gp_0_0_0_0_Enumerator_t1666_gp_1_0_0_0_Types[] = { &Enumerator_t1666_gp_0_0_0_0, &Enumerator_t1666_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1666_gp_0_0_0_0_Enumerator_t1666_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1666_gp_0_0_0_0_Enumerator_t1666_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2506_0_0_0_Types[] = { &KeyValuePair_2_t2506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2506_0_0_0 = { 1, GenInst_KeyValuePair_2_t2506_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_KeyValuePair_2_t2494_0_0_0_Types[] = { &Dictionary_2_t1664_gp_0_0_0_0, &Dictionary_2_t1664_gp_1_0_0_0, &KeyValuePair_2_t2494_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_KeyValuePair_2_t2494_0_0_0 = { 3, GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_KeyValuePair_2_t2494_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2494_0_0_0_KeyValuePair_2_t2494_0_0_0_Types[] = { &KeyValuePair_2_t2494_0_0_0, &KeyValuePair_2_t2494_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2494_0_0_0_KeyValuePair_2_t2494_0_0_0 = { 2, GenInst_KeyValuePair_2_t2494_0_0_0_KeyValuePair_2_t2494_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t1664_gp_1_0_0_0_Types[] = { &Dictionary_2_t1664_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t1664_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t1664_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t1668_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t1668_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t1668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1668_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1668_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t1669_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t1669_gp_0_0_0_0_Types[] = { &DefaultComparer_t1669_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t1669_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t1669_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t1631_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t1631_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t1631_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t1631_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t1631_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2523_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2523_0_0_0_Types[] = { &KeyValuePair_2_t2523_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2523_0_0_0 = { 1, GenInst_KeyValuePair_2_t2523_0_0_0_Types };
extern const Il2CppType IDictionary_2_t1671_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t1671_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t1671_gp_0_0_0_0_IDictionary_2_t1671_gp_1_0_0_0_Types[] = { &IDictionary_2_t1671_gp_0_0_0_0, &IDictionary_2_t1671_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t1671_gp_0_0_0_0_IDictionary_2_t1671_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t1671_gp_0_0_0_0_IDictionary_2_t1671_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1673_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t1673_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1673_gp_0_0_0_0_KeyValuePair_2_t1673_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t1673_gp_0_0_0_0, &KeyValuePair_2_t1673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1673_gp_0_0_0_0_KeyValuePair_2_t1673_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t1673_gp_0_0_0_0_KeyValuePair_2_t1673_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t1674_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t1674_gp_0_0_0_0_Types[] = { &List_1_t1674_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1674_gp_0_0_0_0 = { 1, GenInst_List_1_t1674_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t1675_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1675_gp_0_0_0_0_Types[] = { &Enumerator_t1675_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1675_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1675_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t1676_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t1676_gp_0_0_0_0_Types[] = { &Collection_1_t1676_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t1676_gp_0_0_0_0 = { 1, GenInst_Collection_1_t1676_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t1677_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t1677_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t1677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t1677_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t1677_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[276] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_GcLeaderboard_t28_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t171_0_0_0,
	&GenInst_Boolean_t180_0_0_0,
	&GenInst_IAchievementU5BU5D_t173_0_0_0,
	&GenInst_IScoreU5BU5D_t121_0_0_0,
	&GenInst_IUserProfileU5BU5D_t116_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t180_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t33_0_0_0,
	&GenInst_PersistentCall_t140_0_0_0,
	&GenInst_BaseInvokableCall_t138_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t181_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_Int32_t181_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t838_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t837_0_0_0,
	&GenInst_StrongName_t1088_0_0_0,
	&GenInst_DateTime_t118_0_0_0,
	&GenInst_DateTimeOffset_t1163_0_0_0,
	&GenInst_TimeSpan_t278_0_0_0,
	&GenInst_Guid_t1185_0_0_0,
	&GenInst_CustomAttributeData_t834_0_0_0,
	&GenInst_Object_t8_0_0_0,
	&GenInst_IAchievementDescription_t1637_0_0_0,
	&GenInst_IAchievement_t159_0_0_0,
	&GenInst_IScore_t124_0_0_0,
	&GenInst_IUserProfile_t1636_0_0_0,
	&GenInst_AchievementDescription_t119_0_0_0,
	&GenInst_UserProfile_t115_0_0_0,
	&GenInst_IReflect_t1658_0_0_0,
	&GenInst__Type_t1656_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1271_0_0_0,
	&GenInst__MemberInfo_t1657_0_0_0,
	&GenInst_IConvertible_t1276_0_0_0,
	&GenInst_IComparable_t1275_0_0_0,
	&GenInst_IEnumerable_t384_0_0_0,
	&GenInst_ICloneable_t1286_0_0_0,
	&GenInst_IComparable_1_t1858_0_0_0,
	&GenInst_IEquatable_1_t1863_0_0_0,
	&GenInst_GcAchievementData_t108_0_0_0,
	&GenInst_ValueType_t584_0_0_0,
	&GenInst_Achievement_t117_0_0_0,
	&GenInst_GcScoreData_t109_0_0_0,
	&GenInst_Score_t120_0_0_0,
	&GenInst_Camera_t73_0_0_0,
	&GenInst_Behaviour_t32_0_0_0,
	&GenInst_Component_t71_0_0_0,
	&GenInst_Display_t76_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t1305_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t180_0_0_0,
	&GenInst_Single_t177_0_0_0,
	&GenInst_IFormattable_t1273_0_0_0,
	&GenInst_IComparable_1_t1916_0_0_0,
	&GenInst_IEquatable_1_t1921_0_0_0,
	&GenInst_Keyframe_t93_0_0_0,
	&GenInst_DisallowMultipleComponent_t100_0_0_0,
	&GenInst_Attribute_t64_0_0_0,
	&GenInst__Attribute_t1646_0_0_0,
	&GenInst_ExecuteInEditMode_t102_0_0_0,
	&GenInst_RequireComponent_t101_0_0_0,
	&GenInst_ParameterModifier_t857_0_0_0,
	&GenInst_HitInfo_t125_0_0_0,
	&GenInst_Char_t395_0_0_0,
	&GenInst_IComparable_1_t1958_0_0_0,
	&GenInst_IEquatable_1_t1963_0_0_0,
	&GenInst_ParameterInfo_t195_0_0_0,
	&GenInst__ParameterInfo_t1695_0_0_0,
	&GenInst_UInt16_t398_0_0_0,
	&GenInst_IComparable_1_t1981_0_0_0,
	&GenInst_IEquatable_1_t1986_0_0_0,
	&GenInst_KeyValuePair_2_t1374_0_0_0,
	&GenInst_IComparable_1_t1998_0_0_0,
	&GenInst_IEquatable_1_t2003_0_0_0,
	&GenInst_Link_t694_0_0_0,
	&GenInst_IComparable_1_t2014_0_0_0,
	&GenInst_IEquatable_1_t2019_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_DictionaryEntry_t376_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t180_0_0_0_KeyValuePair_2_t1374_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t180_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_KeyValuePair_2_t1386_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t181_0_0_0,
	&GenInst_KeyValuePair_2_t1391_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t181_0_0_0_KeyValuePair_2_t1391_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t181_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_KeyValuePair_2_t1399_0_0_0,
	&GenInst_Byte_t419_0_0_0,
	&GenInst_IComparable_1_t2038_0_0_0,
	&GenInst_IEquatable_1_t2043_0_0_0,
	&GenInst_X509Certificate_t261_0_0_0,
	&GenInst_IDeserializationCallback_t1308_0_0_0,
	&GenInst_X509ChainStatus_t275_0_0_0,
	&GenInst_Capture_t297_0_0_0,
	&GenInst_Group_t300_0_0_0,
	&GenInst_Mark_t326_0_0_0,
	&GenInst_UriScheme_t363_0_0_0,
	&GenInst_KeySizes_t446_0_0_0,
	&GenInst_UInt32_t189_0_0_0,
	&GenInst_IComparable_1_t2080_0_0_0,
	&GenInst_IEquatable_1_t2085_0_0_0,
	&GenInst_BigInteger_t451_0_0_0,
	&GenInst_ByteU5BU5D_t264_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t386_0_0_0,
	&GenInst_IList_t335_0_0_0,
	&GenInst_ClientCertificateType_t539_0_0_0,
	&GenInst_Enum_t422_0_0_0,
	&GenInst_Int64_t188_0_0_0,
	&GenInst_UInt64_t589_0_0_0,
	&GenInst_SByte_t590_0_0_0,
	&GenInst_Int16_t591_0_0_0,
	&GenInst_Double_t187_0_0_0,
	&GenInst_Decimal_t592_0_0_0,
	&GenInst_Delegate_t179_0_0_0,
	&GenInst_IComparable_1_t2119_0_0_0,
	&GenInst_IEquatable_1_t2120_0_0_0,
	&GenInst_IComparable_1_t2123_0_0_0,
	&GenInst_IEquatable_1_t2124_0_0_0,
	&GenInst_IComparable_1_t2121_0_0_0,
	&GenInst_IEquatable_1_t2122_0_0_0,
	&GenInst_IComparable_1_t2117_0_0_0,
	&GenInst_IEquatable_1_t2118_0_0_0,
	&GenInst_IComparable_1_t2125_0_0_0,
	&GenInst_IEquatable_1_t2126_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t1691_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t1693_0_0_0,
	&GenInst_MethodBase_t193_0_0_0,
	&GenInst__MethodBase_t1692_0_0_0,
	&GenInst_ConstructorInfo_t783_0_0_0,
	&GenInst__ConstructorInfo_t1689_0_0_0,
	&GenInst_TableRange_t627_0_0_0,
	&GenInst_TailoringInfo_t630_0_0_0,
	&GenInst_Contraction_t631_0_0_0,
	&GenInst_Level2Map_t633_0_0_0,
	&GenInst_BigInteger_t654_0_0_0,
	&GenInst_KeyValuePair_2_t1445_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1445_0_0_0,
	&GenInst_Slot_t704_0_0_0,
	&GenInst_Slot_t712_0_0_0,
	&GenInst_StackFrame_t192_0_0_0,
	&GenInst_Calendar_t725_0_0_0,
	&GenInst_ModuleBuilder_t803_0_0_0,
	&GenInst__ModuleBuilder_t1684_0_0_0,
	&GenInst_Module_t799_0_0_0,
	&GenInst__Module_t1694_0_0_0,
	&GenInst_ParameterBuilder_t809_0_0_0,
	&GenInst__ParameterBuilder_t1685_0_0_0,
	&GenInst_TypeU5BU5D_t163_0_0_0,
	&GenInst_ILTokenInfo_t793_0_0_0,
	&GenInst_LabelData_t795_0_0_0,
	&GenInst_LabelFixup_t794_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t791_0_0_0,
	&GenInst_TypeBuilder_t785_0_0_0,
	&GenInst__TypeBuilder_t1686_0_0_0,
	&GenInst_MethodBuilder_t792_0_0_0,
	&GenInst__MethodBuilder_t1683_0_0_0,
	&GenInst_ConstructorBuilder_t782_0_0_0,
	&GenInst__ConstructorBuilder_t1679_0_0_0,
	&GenInst_FieldBuilder_t789_0_0_0,
	&GenInst__FieldBuilder_t1681_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t1696_0_0_0,
	&GenInst_ResourceInfo_t868_0_0_0,
	&GenInst_ResourceCacheItem_t869_0_0_0,
	&GenInst_IContextProperty_t1265_0_0_0,
	&GenInst_Header_t951_0_0_0,
	&GenInst_ITrackingHandler_t1300_0_0_0,
	&GenInst_IContextAttribute_t1287_0_0_0,
	&GenInst_IComparable_1_t2370_0_0_0,
	&GenInst_IEquatable_1_t2375_0_0_0,
	&GenInst_IComparable_1_t2127_0_0_0,
	&GenInst_IEquatable_1_t2128_0_0_0,
	&GenInst_IComparable_1_t2394_0_0_0,
	&GenInst_IEquatable_1_t2399_0_0_0,
	&GenInst_TypeTag_t1007_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t232_0_0_0,
	&GenInst_DictionaryEntry_t376_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_KeyValuePair_2_t1374_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1374_0_0_0_KeyValuePair_2_t1374_0_0_0,
	&GenInst_KeyValuePair_2_t1391_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1391_0_0_0_KeyValuePair_2_t1391_0_0_0,
	&GenInst_KeyValuePair_2_t1445_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1445_0_0_0_KeyValuePair_2_t1445_0_0_0,
	&GenInst_Stack_1_t1643_gp_0_0_0_0,
	&GenInst_Enumerator_t1644_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t1650_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m10254_gp_0_0_0_0,
	&GenInst_Array_Sort_m10266_gp_0_0_0_0_Array_Sort_m10266_gp_0_0_0_0,
	&GenInst_Array_Sort_m10267_gp_0_0_0_0_Array_Sort_m10267_gp_1_0_0_0,
	&GenInst_Array_Sort_m10268_gp_0_0_0_0,
	&GenInst_Array_Sort_m10268_gp_0_0_0_0_Array_Sort_m10268_gp_0_0_0_0,
	&GenInst_Array_Sort_m10269_gp_0_0_0_0,
	&GenInst_Array_Sort_m10269_gp_0_0_0_0_Array_Sort_m10269_gp_1_0_0_0,
	&GenInst_Array_Sort_m10270_gp_0_0_0_0_Array_Sort_m10270_gp_0_0_0_0,
	&GenInst_Array_Sort_m10271_gp_0_0_0_0_Array_Sort_m10271_gp_1_0_0_0,
	&GenInst_Array_Sort_m10272_gp_0_0_0_0,
	&GenInst_Array_Sort_m10272_gp_0_0_0_0_Array_Sort_m10272_gp_0_0_0_0,
	&GenInst_Array_Sort_m10273_gp_0_0_0_0,
	&GenInst_Array_Sort_m10273_gp_1_0_0_0,
	&GenInst_Array_Sort_m10273_gp_0_0_0_0_Array_Sort_m10273_gp_1_0_0_0,
	&GenInst_Array_Sort_m10274_gp_0_0_0_0,
	&GenInst_Array_Sort_m10275_gp_0_0_0_0,
	&GenInst_Array_qsort_m10276_gp_0_0_0_0,
	&GenInst_Array_qsort_m10276_gp_0_0_0_0_Array_qsort_m10276_gp_1_0_0_0,
	&GenInst_Array_compare_m10277_gp_0_0_0_0,
	&GenInst_Array_qsort_m10278_gp_0_0_0_0,
	&GenInst_Array_Resize_m10281_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m10283_gp_0_0_0_0,
	&GenInst_Array_ForEach_m10284_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m10285_gp_0_0_0_0_Array_ConvertAll_m10285_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m10286_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m10287_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m10288_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m10289_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m10290_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m10291_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m10292_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m10293_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m10294_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m10295_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m10296_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m10297_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m10298_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m10299_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m10300_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m10301_gp_0_0_0_0,
	&GenInst_Array_FindAll_m10302_gp_0_0_0_0,
	&GenInst_Array_Exists_m10303_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m10304_gp_0_0_0_0,
	&GenInst_Array_Find_m10305_gp_0_0_0_0,
	&GenInst_Array_FindLast_m10306_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t1651_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t1652_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1653_gp_0_0_0_0,
	&GenInst_IList_1_t1654_gp_0_0_0_0,
	&GenInst_ICollection_1_t1655_gp_0_0_0_0,
	&GenInst_Nullable_1_t1277_gp_0_0_0_0,
	&GenInst_Comparer_1_t1662_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1663_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t1632_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2494_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m10450_gp_0_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m10453_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_DictionaryEntry_t376_0_0_0,
	&GenInst_ShimEnumerator_t1665_gp_0_0_0_0_ShimEnumerator_t1665_gp_1_0_0_0,
	&GenInst_Enumerator_t1666_gp_0_0_0_0_Enumerator_t1666_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2506_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_0_0_0_0_Dictionary_2_t1664_gp_1_0_0_0_KeyValuePair_2_t2494_0_0_0,
	&GenInst_KeyValuePair_2_t2494_0_0_0_KeyValuePair_2_t2494_0_0_0,
	&GenInst_Dictionary_2_t1664_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1668_gp_0_0_0_0,
	&GenInst_DefaultComparer_t1669_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t1631_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t2523_0_0_0,
	&GenInst_IDictionary_2_t1671_gp_0_0_0_0_IDictionary_2_t1671_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1673_gp_0_0_0_0_KeyValuePair_2_t1673_gp_1_0_0_0,
	&GenInst_List_1_t1674_gp_0_0_0_0,
	&GenInst_Enumerator_t1675_gp_0_0_0_0,
	&GenInst_Collection_1_t1676_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t1677_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m10663_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m10663_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m10664_gp_0_0_0_0,
};
