﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t1330;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1516;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t147;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m7942_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1__ctor_m7942(__this, method) (( void (*) (List_1_t1330 *, const MethodInfo*))List_1__ctor_m7942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m7944_gshared (List_1_t1330 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m7944(__this, ___capacity, method) (( void (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1__ctor_m7944_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m7946_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m7946(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m7946_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948(__this, method) (( Object_t* (*) (List_1_t1330 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m7950_gshared (List_1_t1330 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m7950(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1330 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7950_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m7952(__this, method) (( Object_t * (*) (List_1_t1330 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m7954_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m7954(__this, ___item, method) (( int32_t (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m7956_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m7956(__this, ___item, method) (( bool (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7956_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m7958_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m7958(__this, ___item, method) (( int32_t (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7958_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m7960_gshared (List_1_t1330 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m7960(__this, ___index, ___item, method) (( void (*) (List_1_t1330 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m7962_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m7962(__this, ___item, method) (( void (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7962_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964(__this, method) (( bool (*) (List_1_t1330 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m7966(__this, method) (( Object_t * (*) (List_1_t1330 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m7968_gshared (List_1_t1330 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m7968(__this, ___index, method) (( Object_t * (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7968_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m7970_gshared (List_1_t1330 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m7970(__this, ___index, ___value, method) (( void (*) (List_1_t1330 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m7972_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m7972(__this, ___item, method) (( void (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_Add_m7972_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m7974_gshared (List_1_t1330 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m7974(__this, ___newCount, method) (( void (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m7974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m7976_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_Clear_m7976(__this, method) (( void (*) (List_1_t1330 *, const MethodInfo*))List_1_Clear_m7976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m7978_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m7978(__this, ___item, method) (( bool (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_Contains_m7978_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m7980_gshared (List_1_t1330 * __this, ObjectU5BU5D_t147* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m7980(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1330 *, ObjectU5BU5D_t147*, int32_t, const MethodInfo*))List_1_CopyTo_m7980_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1331  List_1_GetEnumerator_m7981_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m7981(__this, method) (( Enumerator_t1331  (*) (List_1_t1330 *, const MethodInfo*))List_1_GetEnumerator_m7981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m7983_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m7983(__this, ___item, method) (( int32_t (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7983_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m7985_gshared (List_1_t1330 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m7985(__this, ___start, ___delta, method) (( void (*) (List_1_t1330 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m7985_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m7987_gshared (List_1_t1330 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m7987(__this, ___index, method) (( void (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_CheckIndex_m7987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m7989_gshared (List_1_t1330 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m7989(__this, ___index, ___item, method) (( void (*) (List_1_t1330 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7989_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m7991_gshared (List_1_t1330 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m7991(__this, ___item, method) (( bool (*) (List_1_t1330 *, Object_t *, const MethodInfo*))List_1_Remove_m7991_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m7993_gshared (List_1_t1330 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m7993(__this, ___index, method) (( void (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7993_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t147* List_1_ToArray_m7995_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_ToArray_m7995(__this, method) (( ObjectU5BU5D_t147* (*) (List_1_t1330 *, const MethodInfo*))List_1_ToArray_m7995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m7997_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m7997(__this, method) (( int32_t (*) (List_1_t1330 *, const MethodInfo*))List_1_get_Capacity_m7997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m7999_gshared (List_1_t1330 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m7999(__this, ___value, method) (( void (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_set_Capacity_m7999_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m8001_gshared (List_1_t1330 * __this, const MethodInfo* method);
#define List_1_get_Count_m8001(__this, method) (( int32_t (*) (List_1_t1330 *, const MethodInfo*))List_1_get_Count_m8001_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m8003_gshared (List_1_t1330 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m8003(__this, ___index, method) (( Object_t * (*) (List_1_t1330 *, int32_t, const MethodInfo*))List_1_get_Item_m8003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m8005_gshared (List_1_t1330 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m8005(__this, ___index, ___value, method) (( void (*) (List_1_t1330 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m8005_gshared)(__this, ___index, ___value, method)
