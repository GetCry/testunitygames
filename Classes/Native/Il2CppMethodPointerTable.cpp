﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void Hello__ctor_m0 ();
extern "C" void Hello_Start_m1 ();
extern "C" void Hello_Update_m2 ();
extern "C" void AssetBundleCreateRequest__ctor_m4 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m5 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m6 ();
extern "C" void AssetBundleRequest__ctor_m7 ();
extern "C" void AssetBundleRequest_get_asset_m8 ();
extern "C" void AssetBundleRequest_get_allAssets_m9 ();
extern "C" void AssetBundle_LoadAsset_m10 ();
extern "C" void AssetBundle_LoadAsset_Internal_m11 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m12 ();
extern "C" void WaitForSeconds__ctor_m13 ();
extern "C" void WaitForFixedUpdate__ctor_m14 ();
extern "C" void WaitForEndOfFrame__ctor_m15 ();
extern "C" void Coroutine__ctor_m16 ();
extern "C" void Coroutine_ReleaseCoroutine_m17 ();
extern "C" void Coroutine_Finalize_m18 ();
extern "C" void ScriptableObject__ctor_m19 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m20 ();
extern "C" void ScriptableObject_CreateInstance_m21 ();
extern "C" void ScriptableObject_CreateInstance_m22 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m23 ();
extern "C" void UnhandledExceptionHandler__ctor_m24 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m25 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m26 ();
extern "C" void UnhandledExceptionHandler_PrintException_m27 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m28 ();
extern "C" void GameCenterPlatform__ctor_m29 ();
extern "C" void GameCenterPlatform__cctor_m30 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m31 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m32 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m33 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m34 ();
extern "C" void GameCenterPlatform_Internal_UserName_m35 ();
extern "C" void GameCenterPlatform_Internal_UserID_m36 ();
extern "C" void GameCenterPlatform_Internal_Underage_m37 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m38 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m39 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m40 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m41 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m42 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m43 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m44 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m45 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m46 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m47 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m48 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m49 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m50 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m51 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m52 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m53 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m54 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m55 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m56 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m57 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m58 ();
extern "C" void GameCenterPlatform_ClearFriends_m59 ();
extern "C" void GameCenterPlatform_SetFriends_m60 ();
extern "C" void GameCenterPlatform_SetFriendImage_m61 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m62 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m63 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m64 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m65 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m66 ();
extern "C" void GameCenterPlatform_get_localUser_m67 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m68 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m69 ();
extern "C" void GameCenterPlatform_ReportProgress_m70 ();
extern "C" void GameCenterPlatform_LoadAchievements_m71 ();
extern "C" void GameCenterPlatform_ReportScore_m72 ();
extern "C" void GameCenterPlatform_LoadScores_m73 ();
extern "C" void GameCenterPlatform_LoadScores_m74 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m75 ();
extern "C" void GameCenterPlatform_GetLoading_m76 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m77 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m78 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m79 ();
extern "C" void GameCenterPlatform_ClearUsers_m80 ();
extern "C" void GameCenterPlatform_SetUser_m81 ();
extern "C" void GameCenterPlatform_SetUserImage_m82 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m83 ();
extern "C" void GameCenterPlatform_LoadUsers_m84 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m85 ();
extern "C" void GameCenterPlatform_SafeClearArray_m86 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m87 ();
extern "C" void GameCenterPlatform_CreateAchievement_m88 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m89 ();
extern "C" void GcLeaderboard__ctor_m90 ();
extern "C" void GcLeaderboard_Finalize_m91 ();
extern "C" void GcLeaderboard_Contains_m92 ();
extern "C" void GcLeaderboard_SetScores_m93 ();
extern "C" void GcLeaderboard_SetLocalScore_m94 ();
extern "C" void GcLeaderboard_SetMaxRange_m95 ();
extern "C" void GcLeaderboard_SetTitle_m96 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m97 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m98 ();
extern "C" void GcLeaderboard_Loading_m99 ();
extern "C" void GcLeaderboard_Dispose_m100 ();
extern "C" void BoneWeight_get_weight0_m101 ();
extern "C" void BoneWeight_set_weight0_m102 ();
extern "C" void BoneWeight_get_weight1_m103 ();
extern "C" void BoneWeight_set_weight1_m104 ();
extern "C" void BoneWeight_get_weight2_m105 ();
extern "C" void BoneWeight_set_weight2_m106 ();
extern "C" void BoneWeight_get_weight3_m107 ();
extern "C" void BoneWeight_set_weight3_m108 ();
extern "C" void BoneWeight_get_boneIndex0_m109 ();
extern "C" void BoneWeight_set_boneIndex0_m110 ();
extern "C" void BoneWeight_get_boneIndex1_m111 ();
extern "C" void BoneWeight_set_boneIndex1_m112 ();
extern "C" void BoneWeight_get_boneIndex2_m113 ();
extern "C" void BoneWeight_set_boneIndex2_m114 ();
extern "C" void BoneWeight_get_boneIndex3_m115 ();
extern "C" void BoneWeight_set_boneIndex3_m116 ();
extern "C" void BoneWeight_GetHashCode_m117 ();
extern "C" void BoneWeight_Equals_m118 ();
extern "C" void BoneWeight_op_Equality_m119 ();
extern "C" void BoneWeight_op_Inequality_m120 ();
extern "C" void GUILayer_HitTest_m121 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m122 ();
extern "C" void Texture__ctor_m123 ();
extern "C" void Texture2D__ctor_m124 ();
extern "C" void Texture2D_Internal_Create_m125 ();
extern "C" void StateChanged__ctor_m126 ();
extern "C" void StateChanged_Invoke_m127 ();
extern "C" void StateChanged_BeginInvoke_m128 ();
extern "C" void StateChanged_EndInvoke_m129 ();
extern "C" void CullingGroup_Finalize_m130 ();
extern "C" void CullingGroup_Dispose_m131 ();
extern "C" void CullingGroup_SendEvents_m132 ();
extern "C" void CullingGroup_FinalizerFailure_m133 ();
extern "C" void GradientColorKey__ctor_m134 ();
extern "C" void GradientAlphaKey__ctor_m135 ();
extern "C" void Gradient__ctor_m136 ();
extern "C" void Gradient_Init_m137 ();
extern "C" void Gradient_Cleanup_m138 ();
extern "C" void Gradient_Finalize_m139 ();
extern "C" void LayerMask_get_value_m140 ();
extern "C" void LayerMask_set_value_m141 ();
extern "C" void LayerMask_LayerToName_m142 ();
extern "C" void LayerMask_NameToLayer_m143 ();
extern "C" void LayerMask_GetMask_m144 ();
extern "C" void LayerMask_op_Implicit_m145 ();
extern "C" void LayerMask_op_Implicit_m146 ();
extern "C" void Vector3__ctor_m147 ();
extern "C" void Vector3_GetHashCode_m148 ();
extern "C" void Vector3_Equals_m149 ();
extern "C" void Vector3_ToString_m150 ();
extern "C" void Vector3_ToString_m151 ();
extern "C" void Vector3_SqrMagnitude_m152 ();
extern "C" void Vector3_Min_m153 ();
extern "C" void Vector3_Max_m154 ();
extern "C" void Vector3_op_Addition_m155 ();
extern "C" void Vector3_op_Subtraction_m156 ();
extern "C" void Vector3_op_Multiply_m157 ();
extern "C" void Vector3_op_Equality_m158 ();
extern "C" void Color__ctor_m159 ();
extern "C" void Color_ToString_m160 ();
extern "C" void Color_GetHashCode_m161 ();
extern "C" void Color_Equals_m162 ();
extern "C" void Color_op_Multiply_m163 ();
extern "C" void Color_op_Implicit_m164 ();
extern "C" void Quaternion_ToString_m165 ();
extern "C" void Quaternion_GetHashCode_m166 ();
extern "C" void Quaternion_Equals_m167 ();
extern "C" void Rect_get_x_m168 ();
extern "C" void Rect_get_y_m169 ();
extern "C" void Rect_get_width_m170 ();
extern "C" void Rect_get_height_m171 ();
extern "C" void Rect_get_xMin_m172 ();
extern "C" void Rect_get_yMin_m173 ();
extern "C" void Rect_get_xMax_m174 ();
extern "C" void Rect_get_yMax_m175 ();
extern "C" void Rect_ToString_m176 ();
extern "C" void Rect_Contains_m177 ();
extern "C" void Rect_GetHashCode_m178 ();
extern "C" void Rect_Equals_m179 ();
extern "C" void Matrix4x4_get_Item_m180 ();
extern "C" void Matrix4x4_set_Item_m181 ();
extern "C" void Matrix4x4_get_Item_m182 ();
extern "C" void Matrix4x4_set_Item_m183 ();
extern "C" void Matrix4x4_GetHashCode_m184 ();
extern "C" void Matrix4x4_Equals_m185 ();
extern "C" void Matrix4x4_Inverse_m186 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m187 ();
extern "C" void Matrix4x4_Transpose_m188 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m189 ();
extern "C" void Matrix4x4_Invert_m190 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m191 ();
extern "C" void Matrix4x4_get_inverse_m192 ();
extern "C" void Matrix4x4_get_transpose_m193 ();
extern "C" void Matrix4x4_get_isIdentity_m194 ();
extern "C" void Matrix4x4_GetColumn_m195 ();
extern "C" void Matrix4x4_GetRow_m196 ();
extern "C" void Matrix4x4_SetColumn_m197 ();
extern "C" void Matrix4x4_SetRow_m198 ();
extern "C" void Matrix4x4_MultiplyPoint_m199 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m200 ();
extern "C" void Matrix4x4_MultiplyVector_m201 ();
extern "C" void Matrix4x4_Scale_m202 ();
extern "C" void Matrix4x4_get_zero_m203 ();
extern "C" void Matrix4x4_get_identity_m204 ();
extern "C" void Matrix4x4_SetTRS_m205 ();
extern "C" void Matrix4x4_TRS_m206 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m207 ();
extern "C" void Matrix4x4_ToString_m208 ();
extern "C" void Matrix4x4_ToString_m209 ();
extern "C" void Matrix4x4_Ortho_m210 ();
extern "C" void Matrix4x4_Perspective_m211 ();
extern "C" void Matrix4x4_op_Multiply_m212 ();
extern "C" void Matrix4x4_op_Multiply_m213 ();
extern "C" void Matrix4x4_op_Equality_m214 ();
extern "C" void Matrix4x4_op_Inequality_m215 ();
extern "C" void Bounds__ctor_m216 ();
extern "C" void Bounds_GetHashCode_m217 ();
extern "C" void Bounds_Equals_m218 ();
extern "C" void Bounds_get_center_m219 ();
extern "C" void Bounds_set_center_m220 ();
extern "C" void Bounds_get_size_m221 ();
extern "C" void Bounds_set_size_m222 ();
extern "C" void Bounds_get_extents_m223 ();
extern "C" void Bounds_set_extents_m224 ();
extern "C" void Bounds_get_min_m225 ();
extern "C" void Bounds_set_min_m226 ();
extern "C" void Bounds_get_max_m227 ();
extern "C" void Bounds_set_max_m228 ();
extern "C" void Bounds_SetMinMax_m229 ();
extern "C" void Bounds_Encapsulate_m230 ();
extern "C" void Bounds_Encapsulate_m231 ();
extern "C" void Bounds_Expand_m232 ();
extern "C" void Bounds_Expand_m233 ();
extern "C" void Bounds_Intersects_m234 ();
extern "C" void Bounds_Internal_Contains_m235 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m236 ();
extern "C" void Bounds_Contains_m237 ();
extern "C" void Bounds_Internal_SqrDistance_m238 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m239 ();
extern "C" void Bounds_SqrDistance_m240 ();
extern "C" void Bounds_Internal_IntersectRay_m241 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m242 ();
extern "C" void Bounds_IntersectRay_m243 ();
extern "C" void Bounds_IntersectRay_m244 ();
extern "C" void Bounds_Internal_GetClosestPoint_m245 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m246 ();
extern "C" void Bounds_ClosestPoint_m247 ();
extern "C" void Bounds_ToString_m248 ();
extern "C" void Bounds_ToString_m249 ();
extern "C" void Bounds_op_Equality_m250 ();
extern "C" void Bounds_op_Inequality_m251 ();
extern "C" void Vector4__ctor_m252 ();
extern "C" void Vector4_GetHashCode_m253 ();
extern "C" void Vector4_Equals_m254 ();
extern "C" void Vector4_ToString_m255 ();
extern "C" void Vector4_Dot_m256 ();
extern "C" void Vector4_SqrMagnitude_m257 ();
extern "C" void Vector4_op_Subtraction_m258 ();
extern "C" void Vector4_op_Equality_m259 ();
extern "C" void Ray_get_direction_m260 ();
extern "C" void Ray_ToString_m261 ();
extern "C" void MathfInternal__cctor_m262 ();
extern "C" void Mathf__cctor_m263 ();
extern "C" void Mathf_Abs_m264 ();
extern "C" void Mathf_Min_m265 ();
extern "C" void Mathf_Max_m266 ();
extern "C" void Mathf_Approximately_m267 ();
extern "C" void ReapplyDrivenProperties__ctor_m268 ();
extern "C" void ReapplyDrivenProperties_Invoke_m269 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m270 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m271 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m272 ();
extern "C" void ResourceRequest__ctor_m273 ();
extern "C" void ResourceRequest_get_asset_m274 ();
extern "C" void Resources_Load_m275 ();
extern "C" void SerializePrivateVariables__ctor_m276 ();
extern "C" void SerializeField__ctor_m277 ();
extern "C" void SphericalHarmonicsL2_Clear_m278 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m279 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m280 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m281 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m282 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m283 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m284 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m285 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m286 ();
extern "C" void SphericalHarmonicsL2_get_Item_m287 ();
extern "C" void SphericalHarmonicsL2_set_Item_m288 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m289 ();
extern "C" void SphericalHarmonicsL2_Equals_m290 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m291 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m292 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m293 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m294 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m295 ();
extern "C" void UnityString_Format_m296 ();
extern "C" void AsyncOperation__ctor_m297 ();
extern "C" void AsyncOperation_InternalDestroy_m298 ();
extern "C" void AsyncOperation_Finalize_m299 ();
extern "C" void LogCallback__ctor_m300 ();
extern "C" void LogCallback_Invoke_m301 ();
extern "C" void LogCallback_BeginInvoke_m302 ();
extern "C" void LogCallback_EndInvoke_m303 ();
extern "C" void Application_CallLogCallback_m304 ();
extern "C" void Behaviour__ctor_m305 ();
extern "C" void CameraCallback__ctor_m306 ();
extern "C" void CameraCallback_Invoke_m307 ();
extern "C" void CameraCallback_BeginInvoke_m308 ();
extern "C" void CameraCallback_EndInvoke_m309 ();
extern "C" void Camera_get_nearClipPlane_m310 ();
extern "C" void Camera_get_farClipPlane_m311 ();
extern "C" void Camera_get_cullingMask_m312 ();
extern "C" void Camera_get_eventMask_m313 ();
extern "C" void Camera_get_pixelRect_m314 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m315 ();
extern "C" void Camera_get_targetTexture_m316 ();
extern "C" void Camera_get_clearFlags_m317 ();
extern "C" void Camera_ScreenPointToRay_m318 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m319 ();
extern "C" void Camera_get_allCamerasCount_m320 ();
extern "C" void Camera_GetAllCameras_m321 ();
extern "C" void Camera_FireOnPreCull_m322 ();
extern "C" void Camera_FireOnPreRender_m323 ();
extern "C" void Camera_FireOnPostRender_m324 ();
extern "C" void Camera_RaycastTry_m325 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m326 ();
extern "C" void Camera_RaycastTry2D_m327 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m328 ();
extern "C" void Debug_Internal_Log_m329 ();
extern "C" void Debug_Log_m330 ();
extern "C" void Debug_LogError_m331 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m332 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m333 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m334 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m335 ();
extern "C" void Display__ctor_m336 ();
extern "C" void Display__ctor_m337 ();
extern "C" void Display__cctor_m338 ();
extern "C" void Display_add_onDisplaysUpdated_m339 ();
extern "C" void Display_remove_onDisplaysUpdated_m340 ();
extern "C" void Display_get_renderingWidth_m341 ();
extern "C" void Display_get_renderingHeight_m342 ();
extern "C" void Display_get_systemWidth_m343 ();
extern "C" void Display_get_systemHeight_m344 ();
extern "C" void Display_get_colorBuffer_m345 ();
extern "C" void Display_get_depthBuffer_m346 ();
extern "C" void Display_Activate_m347 ();
extern "C" void Display_Activate_m348 ();
extern "C" void Display_SetParams_m349 ();
extern "C" void Display_SetRenderingResolution_m350 ();
extern "C" void Display_MultiDisplayLicense_m351 ();
extern "C" void Display_RelativeMouseAt_m352 ();
extern "C" void Display_get_main_m353 ();
extern "C" void Display_RecreateDisplayList_m354 ();
extern "C" void Display_FireDisplaysUpdated_m355 ();
extern "C" void Display_GetSystemExtImpl_m356 ();
extern "C" void Display_GetRenderingExtImpl_m357 ();
extern "C" void Display_GetRenderingBuffersImpl_m358 ();
extern "C" void Display_SetRenderingResolutionImpl_m359 ();
extern "C" void Display_ActivateDisplayImpl_m360 ();
extern "C" void Display_SetParamsImpl_m361 ();
extern "C" void Display_MultiDisplayLicenseImpl_m362 ();
extern "C" void Display_RelativeMouseAtImpl_m363 ();
extern "C" void MonoBehaviour__ctor_m3 ();
extern "C" void Input__cctor_m364 ();
extern "C" void Input_GetMouseButton_m365 ();
extern "C" void Input_GetMouseButtonDown_m366 ();
extern "C" void Input_get_mousePosition_m367 ();
extern "C" void Input_INTERNAL_get_mousePosition_m368 ();
extern "C" void Object__ctor_m369 ();
extern "C" void Object_ToString_m370 ();
extern "C" void Object_Equals_m371 ();
extern "C" void Object_GetHashCode_m372 ();
extern "C" void Object_CompareBaseObjects_m373 ();
extern "C" void Object_IsNativeObjectAlive_m374 ();
extern "C" void Object_GetInstanceID_m375 ();
extern "C" void Object_GetCachedPtr_m376 ();
extern "C" void Object_op_Implicit_m377 ();
extern "C" void Object_op_Equality_m378 ();
extern "C" void Object_op_Inequality_m379 ();
extern "C" void Component__ctor_m380 ();
extern "C" void Component_get_gameObject_m381 ();
extern "C" void Component_GetComponentFastPath_m382 ();
extern "C" void GameObject_SendMessage_m383 ();
extern "C" void Enumerator__ctor_m384 ();
extern "C" void Enumerator_get_Current_m385 ();
extern "C" void Enumerator_MoveNext_m386 ();
extern "C" void Enumerator_Reset_m387 ();
extern "C" void Transform_get_childCount_m388 ();
extern "C" void Transform_GetEnumerator_m389 ();
extern "C" void Transform_GetChild_m390 ();
extern "C" void YieldInstruction__ctor_m391 ();
extern "C" void UnityAdsInternal__ctor_m392 ();
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m393 ();
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m394 ();
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m395 ();
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m396 ();
extern "C" void UnityAdsInternal_add_onShow_m397 ();
extern "C" void UnityAdsInternal_remove_onShow_m398 ();
extern "C" void UnityAdsInternal_add_onHide_m399 ();
extern "C" void UnityAdsInternal_remove_onHide_m400 ();
extern "C" void UnityAdsInternal_add_onVideoCompleted_m401 ();
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m402 ();
extern "C" void UnityAdsInternal_add_onVideoStarted_m403 ();
extern "C" void UnityAdsInternal_remove_onVideoStarted_m404 ();
extern "C" void UnityAdsInternal_RegisterNative_m405 ();
extern "C" void UnityAdsInternal_Init_m406 ();
extern "C" void UnityAdsInternal_Show_m407 ();
extern "C" void UnityAdsInternal_CanShowAds_m408 ();
extern "C" void UnityAdsInternal_SetLogLevel_m409 ();
extern "C" void UnityAdsInternal_SetCampaignDataURL_m410 ();
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m411 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m412 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m413 ();
extern "C" void UnityAdsInternal_CallUnityAdsShow_m414 ();
extern "C" void UnityAdsInternal_CallUnityAdsHide_m415 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m416 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m417 ();
extern "C" void Particle_get_position_m418 ();
extern "C" void Particle_set_position_m419 ();
extern "C" void Particle_get_velocity_m420 ();
extern "C" void Particle_set_velocity_m421 ();
extern "C" void Particle_get_energy_m422 ();
extern "C" void Particle_set_energy_m423 ();
extern "C" void Particle_get_startEnergy_m424 ();
extern "C" void Particle_set_startEnergy_m425 ();
extern "C" void Particle_get_size_m426 ();
extern "C" void Particle_set_size_m427 ();
extern "C" void Particle_get_rotation_m428 ();
extern "C" void Particle_set_rotation_m429 ();
extern "C" void Particle_get_angularVelocity_m430 ();
extern "C" void Particle_set_angularVelocity_m431 ();
extern "C" void Particle_get_color_m432 ();
extern "C" void Particle_set_color_m433 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m434 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m435 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m436 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m437 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m438 ();
extern "C" void PCMReaderCallback__ctor_m439 ();
extern "C" void PCMReaderCallback_Invoke_m440 ();
extern "C" void PCMReaderCallback_BeginInvoke_m441 ();
extern "C" void PCMReaderCallback_EndInvoke_m442 ();
extern "C" void PCMSetPositionCallback__ctor_m443 ();
extern "C" void PCMSetPositionCallback_Invoke_m444 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m445 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m446 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m447 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m448 ();
extern "C" void WebCamDevice_get_name_m449 ();
extern "C" void WebCamDevice_get_isFrontFacing_m450 ();
extern "C" void AnimationCurve__ctor_m451 ();
extern "C" void AnimationCurve__ctor_m452 ();
extern "C" void AnimationCurve_Cleanup_m453 ();
extern "C" void AnimationCurve_Finalize_m454 ();
extern "C" void AnimationCurve_Init_m455 ();
extern "C" void WrapperlessIcall__ctor_m456 ();
extern "C" void AttributeHelperEngine__cctor_m457 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m458 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m459 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m460 ();
extern "C" void SetupCoroutine__ctor_m461 ();
extern "C" void SetupCoroutine_InvokeMember_m462 ();
extern "C" void SetupCoroutine_InvokeStatic_m463 ();
extern "C" void WritableAttribute__ctor_m464 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m465 ();
extern "C" void GcUserProfileData_ToUserProfile_m466 ();
extern "C" void GcUserProfileData_AddToArray_m467 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m468 ();
extern "C" void GcAchievementData_ToAchievement_m469 ();
extern "C" void GcScoreData_ToScore_m470 ();
extern "C" void Resolution_get_width_m471 ();
extern "C" void Resolution_set_width_m472 ();
extern "C" void Resolution_get_height_m473 ();
extern "C" void Resolution_set_height_m474 ();
extern "C" void Resolution_get_refreshRate_m475 ();
extern "C" void Resolution_set_refreshRate_m476 ();
extern "C" void Resolution_ToString_m477 ();
extern "C" void LocalUser__ctor_m478 ();
extern "C" void LocalUser_SetFriends_m479 ();
extern "C" void LocalUser_SetAuthenticated_m480 ();
extern "C" void LocalUser_SetUnderage_m481 ();
extern "C" void LocalUser_get_authenticated_m482 ();
extern "C" void UserProfile__ctor_m483 ();
extern "C" void UserProfile__ctor_m484 ();
extern "C" void UserProfile_ToString_m485 ();
extern "C" void UserProfile_SetUserName_m486 ();
extern "C" void UserProfile_SetUserID_m487 ();
extern "C" void UserProfile_SetImage_m488 ();
extern "C" void UserProfile_get_userName_m489 ();
extern "C" void UserProfile_get_id_m490 ();
extern "C" void UserProfile_get_isFriend_m491 ();
extern "C" void UserProfile_get_state_m492 ();
extern "C" void Achievement__ctor_m493 ();
extern "C" void Achievement__ctor_m494 ();
extern "C" void Achievement__ctor_m495 ();
extern "C" void Achievement_ToString_m496 ();
extern "C" void Achievement_get_id_m497 ();
extern "C" void Achievement_set_id_m498 ();
extern "C" void Achievement_get_percentCompleted_m499 ();
extern "C" void Achievement_set_percentCompleted_m500 ();
extern "C" void Achievement_get_completed_m501 ();
extern "C" void Achievement_get_hidden_m502 ();
extern "C" void Achievement_get_lastReportedDate_m503 ();
extern "C" void AchievementDescription__ctor_m504 ();
extern "C" void AchievementDescription_ToString_m505 ();
extern "C" void AchievementDescription_SetImage_m506 ();
extern "C" void AchievementDescription_get_id_m507 ();
extern "C" void AchievementDescription_set_id_m508 ();
extern "C" void AchievementDescription_get_title_m509 ();
extern "C" void AchievementDescription_get_achievedDescription_m510 ();
extern "C" void AchievementDescription_get_unachievedDescription_m511 ();
extern "C" void AchievementDescription_get_hidden_m512 ();
extern "C" void AchievementDescription_get_points_m513 ();
extern "C" void Score__ctor_m514 ();
extern "C" void Score__ctor_m515 ();
extern "C" void Score_ToString_m516 ();
extern "C" void Score_get_leaderboardID_m517 ();
extern "C" void Score_set_leaderboardID_m518 ();
extern "C" void Score_get_value_m519 ();
extern "C" void Score_set_value_m520 ();
extern "C" void Leaderboard__ctor_m521 ();
extern "C" void Leaderboard_ToString_m522 ();
extern "C" void Leaderboard_SetLocalUserScore_m523 ();
extern "C" void Leaderboard_SetMaxRange_m524 ();
extern "C" void Leaderboard_SetScores_m525 ();
extern "C" void Leaderboard_SetTitle_m526 ();
extern "C" void Leaderboard_GetUserFilter_m527 ();
extern "C" void Leaderboard_get_id_m528 ();
extern "C" void Leaderboard_set_id_m529 ();
extern "C" void Leaderboard_get_userScope_m530 ();
extern "C" void Leaderboard_set_userScope_m531 ();
extern "C" void Leaderboard_get_range_m532 ();
extern "C" void Leaderboard_set_range_m533 ();
extern "C" void Leaderboard_get_timeScope_m534 ();
extern "C" void Leaderboard_set_timeScope_m535 ();
extern "C" void HitInfo_SendMessage_m536 ();
extern "C" void HitInfo_Compare_m537 ();
extern "C" void HitInfo_op_Implicit_m538 ();
extern "C" void SendMouseEvents__cctor_m539 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m540 ();
extern "C" void SendMouseEvents_SendEvents_m541 ();
extern "C" void Range__ctor_m542 ();
extern "C" void StackTraceUtility__ctor_m543 ();
extern "C" void StackTraceUtility__cctor_m544 ();
extern "C" void StackTraceUtility_SetProjectFolder_m545 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m546 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m547 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m548 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m549 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m550 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m551 ();
extern "C" void UnityException__ctor_m552 ();
extern "C" void UnityException__ctor_m553 ();
extern "C" void UnityException__ctor_m554 ();
extern "C" void UnityException__ctor_m555 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m556 ();
extern "C" void StateMachineBehaviour__ctor_m557 ();
extern "C" void ArgumentCache__ctor_m558 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m559 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m560 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m561 ();
extern "C" void PersistentCall__ctor_m562 ();
extern "C" void PersistentCallGroup__ctor_m563 ();
extern "C" void InvokableCallList__ctor_m564 ();
extern "C" void InvokableCallList_ClearPersistent_m565 ();
extern "C" void UnityEventBase__ctor_m566 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m567 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m568 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m569 ();
extern "C" void UnityEventBase_ToString_m570 ();
extern "C" void UnityEvent__ctor_m571 ();
extern "C" void DefaultValueAttribute__ctor_m572 ();
extern "C" void DefaultValueAttribute_get_Value_m573 ();
extern "C" void DefaultValueAttribute_Equals_m574 ();
extern "C" void DefaultValueAttribute_GetHashCode_m575 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m576 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m577 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m578 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m579 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m580 ();
extern "C" void UnityAdsDelegate__ctor_m581 ();
extern "C" void UnityAdsDelegate_Invoke_m582 ();
extern "C" void UnityAdsDelegate_BeginInvoke_m583 ();
extern "C" void UnityAdsDelegate_EndInvoke_m584 ();
extern "C" void Locale_GetText_m663 ();
extern "C" void Locale_GetText_m664 ();
extern "C" void MonoTODOAttribute__ctor_m665 ();
extern "C" void MonoTODOAttribute__ctor_m666 ();
extern "C" void HybridDictionary__ctor_m667 ();
extern "C" void HybridDictionary__ctor_m668 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m669 ();
extern "C" void HybridDictionary_get_inner_m670 ();
extern "C" void HybridDictionary_get_Count_m671 ();
extern "C" void HybridDictionary_get_Item_m672 ();
extern "C" void HybridDictionary_set_Item_m673 ();
extern "C" void HybridDictionary_get_SyncRoot_m674 ();
extern "C" void HybridDictionary_Add_m675 ();
extern "C" void HybridDictionary_Contains_m676 ();
extern "C" void HybridDictionary_CopyTo_m677 ();
extern "C" void HybridDictionary_GetEnumerator_m678 ();
extern "C" void HybridDictionary_Remove_m679 ();
extern "C" void HybridDictionary_Switch_m680 ();
extern "C" void DictionaryNode__ctor_m681 ();
extern "C" void DictionaryNodeEnumerator__ctor_m682 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m683 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m684 ();
extern "C" void DictionaryNodeEnumerator_Reset_m685 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m686 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m687 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m688 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m689 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m690 ();
extern "C" void ListDictionary__ctor_m691 ();
extern "C" void ListDictionary__ctor_m692 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m693 ();
extern "C" void ListDictionary_FindEntry_m694 ();
extern "C" void ListDictionary_FindEntry_m695 ();
extern "C" void ListDictionary_AddImpl_m696 ();
extern "C" void ListDictionary_get_Count_m697 ();
extern "C" void ListDictionary_get_SyncRoot_m698 ();
extern "C" void ListDictionary_CopyTo_m699 ();
extern "C" void ListDictionary_get_Item_m700 ();
extern "C" void ListDictionary_set_Item_m701 ();
extern "C" void ListDictionary_Add_m702 ();
extern "C" void ListDictionary_Clear_m703 ();
extern "C" void ListDictionary_Contains_m704 ();
extern "C" void ListDictionary_GetEnumerator_m705 ();
extern "C" void ListDictionary_Remove_m706 ();
extern "C" void _Item__ctor_m707 ();
extern "C" void _KeysEnumerator__ctor_m708 ();
extern "C" void _KeysEnumerator_get_Current_m709 ();
extern "C" void _KeysEnumerator_MoveNext_m710 ();
extern "C" void _KeysEnumerator_Reset_m711 ();
extern "C" void KeysCollection__ctor_m712 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m713 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m714 ();
extern "C" void KeysCollection_get_Count_m715 ();
extern "C" void KeysCollection_GetEnumerator_m716 ();
extern "C" void NameObjectCollectionBase__ctor_m717 ();
extern "C" void NameObjectCollectionBase__ctor_m718 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m719 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m720 ();
extern "C" void NameObjectCollectionBase_Init_m721 ();
extern "C" void NameObjectCollectionBase_get_Keys_m722 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m723 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m724 ();
extern "C" void NameObjectCollectionBase_get_Count_m725 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m726 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m727 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m728 ();
extern "C" void NameObjectCollectionBase_BaseGet_m729 ();
extern "C" void NameObjectCollectionBase_BaseGet_m730 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m731 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m732 ();
extern "C" void NameValueCollection__ctor_m733 ();
extern "C" void NameValueCollection__ctor_m734 ();
extern "C" void NameValueCollection_Add_m735 ();
extern "C" void NameValueCollection_Get_m736 ();
extern "C" void NameValueCollection_AsSingleString_m737 ();
extern "C" void NameValueCollection_GetKey_m738 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m739 ();
extern "C" void TypeConverterAttribute__ctor_m740 ();
extern "C" void TypeConverterAttribute__ctor_m741 ();
extern "C" void TypeConverterAttribute__cctor_m742 ();
extern "C" void TypeConverterAttribute_Equals_m743 ();
extern "C" void TypeConverterAttribute_GetHashCode_m744 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m745 ();
extern "C" void DefaultCertificatePolicy__ctor_m746 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m747 ();
extern "C" void FileWebRequest__ctor_m748 ();
extern "C" void FileWebRequest__ctor_m749 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m750 ();
extern "C" void FileWebRequest_GetObjectData_m751 ();
extern "C" void FileWebRequestCreator__ctor_m752 ();
extern "C" void FileWebRequestCreator_Create_m753 ();
extern "C" void FtpRequestCreator__ctor_m754 ();
extern "C" void FtpRequestCreator_Create_m755 ();
extern "C" void FtpWebRequest__ctor_m756 ();
extern "C" void FtpWebRequest__cctor_m757 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m758 ();
extern "C" void GlobalProxySelection_get_Select_m759 ();
extern "C" void HttpRequestCreator__ctor_m760 ();
extern "C" void HttpRequestCreator_Create_m761 ();
extern "C" void HttpVersion__cctor_m762 ();
extern "C" void HttpWebRequest__ctor_m763 ();
extern "C" void HttpWebRequest__ctor_m764 ();
extern "C" void HttpWebRequest__cctor_m765 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m766 ();
extern "C" void HttpWebRequest_get_Address_m767 ();
extern "C" void HttpWebRequest_get_ServicePoint_m768 ();
extern "C" void HttpWebRequest_GetServicePoint_m769 ();
extern "C" void HttpWebRequest_GetObjectData_m770 ();
extern "C" void IPAddress__ctor_m771 ();
extern "C" void IPAddress__ctor_m772 ();
extern "C" void IPAddress__cctor_m773 ();
extern "C" void IPAddress_SwapShort_m774 ();
extern "C" void IPAddress_HostToNetworkOrder_m775 ();
extern "C" void IPAddress_NetworkToHostOrder_m776 ();
extern "C" void IPAddress_Parse_m777 ();
extern "C" void IPAddress_TryParse_m778 ();
extern "C" void IPAddress_ParseIPV4_m779 ();
extern "C" void IPAddress_ParseIPV6_m780 ();
extern "C" void IPAddress_get_InternalIPv4Address_m781 ();
extern "C" void IPAddress_get_ScopeId_m782 ();
extern "C" void IPAddress_get_AddressFamily_m783 ();
extern "C" void IPAddress_IsLoopback_m784 ();
extern "C" void IPAddress_ToString_m785 ();
extern "C" void IPAddress_ToString_m786 ();
extern "C" void IPAddress_Equals_m787 ();
extern "C" void IPAddress_GetHashCode_m788 ();
extern "C" void IPAddress_Hash_m789 ();
extern "C" void IPv6Address__ctor_m790 ();
extern "C" void IPv6Address__ctor_m791 ();
extern "C" void IPv6Address__ctor_m792 ();
extern "C" void IPv6Address__cctor_m793 ();
extern "C" void IPv6Address_Parse_m794 ();
extern "C" void IPv6Address_Fill_m795 ();
extern "C" void IPv6Address_TryParse_m796 ();
extern "C" void IPv6Address_TryParse_m797 ();
extern "C" void IPv6Address_get_Address_m798 ();
extern "C" void IPv6Address_get_ScopeId_m799 ();
extern "C" void IPv6Address_set_ScopeId_m800 ();
extern "C" void IPv6Address_IsLoopback_m801 ();
extern "C" void IPv6Address_SwapUShort_m802 ();
extern "C" void IPv6Address_AsIPv4Int_m803 ();
extern "C" void IPv6Address_IsIPv4Compatible_m804 ();
extern "C" void IPv6Address_IsIPv4Mapped_m805 ();
extern "C" void IPv6Address_ToString_m806 ();
extern "C" void IPv6Address_ToString_m807 ();
extern "C" void IPv6Address_Equals_m808 ();
extern "C" void IPv6Address_GetHashCode_m809 ();
extern "C" void IPv6Address_Hash_m810 ();
extern "C" void ServicePoint__ctor_m811 ();
extern "C" void ServicePoint_get_Address_m812 ();
extern "C" void ServicePoint_get_CurrentConnections_m813 ();
extern "C" void ServicePoint_get_IdleSince_m814 ();
extern "C" void ServicePoint_set_IdleSince_m815 ();
extern "C" void ServicePoint_set_Expect100Continue_m816 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m817 ();
extern "C" void ServicePoint_set_SendContinue_m818 ();
extern "C" void ServicePoint_set_UsesProxy_m819 ();
extern "C" void ServicePoint_set_UseConnect_m820 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m821 ();
extern "C" void SPKey__ctor_m822 ();
extern "C" void SPKey_GetHashCode_m823 ();
extern "C" void SPKey_Equals_m824 ();
extern "C" void ServicePointManager__cctor_m825 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m826 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m827 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m828 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m829 ();
extern "C" void ServicePointManager_FindServicePoint_m830 ();
extern "C" void ServicePointManager_RecycleServicePoints_m831 ();
extern "C" void WebHeaderCollection__ctor_m832 ();
extern "C" void WebHeaderCollection__ctor_m833 ();
extern "C" void WebHeaderCollection__ctor_m834 ();
extern "C" void WebHeaderCollection__cctor_m835 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m836 ();
extern "C" void WebHeaderCollection_Add_m837 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m838 ();
extern "C" void WebHeaderCollection_IsRestricted_m839 ();
extern "C" void WebHeaderCollection_OnDeserialization_m840 ();
extern "C" void WebHeaderCollection_ToString_m841 ();
extern "C" void WebHeaderCollection_GetObjectData_m842 ();
extern "C" void WebHeaderCollection_get_Count_m843 ();
extern "C" void WebHeaderCollection_get_Keys_m844 ();
extern "C" void WebHeaderCollection_Get_m845 ();
extern "C" void WebHeaderCollection_GetKey_m846 ();
extern "C" void WebHeaderCollection_GetEnumerator_m847 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m848 ();
extern "C" void WebHeaderCollection_IsHeaderName_m849 ();
extern "C" void WebProxy__ctor_m850 ();
extern "C" void WebProxy__ctor_m851 ();
extern "C" void WebProxy__ctor_m852 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m853 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m854 ();
extern "C" void WebProxy_GetProxy_m855 ();
extern "C" void WebProxy_IsBypassed_m856 ();
extern "C" void WebProxy_GetObjectData_m857 ();
extern "C" void WebProxy_CheckBypassList_m858 ();
extern "C" void WebRequest__ctor_m859 ();
extern "C" void WebRequest__ctor_m860 ();
extern "C" void WebRequest__cctor_m861 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m862 ();
extern "C" void WebRequest_AddDynamicPrefix_m863 ();
extern "C" void WebRequest_GetMustImplement_m864 ();
extern "C" void WebRequest_get_DefaultWebProxy_m865 ();
extern "C" void WebRequest_GetDefaultWebProxy_m866 ();
extern "C" void WebRequest_GetObjectData_m867 ();
extern "C" void WebRequest_AddPrefix_m868 ();
extern "C" void PublicKey__ctor_m869 ();
extern "C" void PublicKey_get_EncodedKeyValue_m870 ();
extern "C" void PublicKey_get_EncodedParameters_m871 ();
extern "C" void PublicKey_get_Key_m872 ();
extern "C" void PublicKey_get_Oid_m873 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m874 ();
extern "C" void PublicKey_DecodeDSA_m875 ();
extern "C" void PublicKey_DecodeRSA_m876 ();
extern "C" void X500DistinguishedName__ctor_m877 ();
extern "C" void X500DistinguishedName_Decode_m878 ();
extern "C" void X500DistinguishedName_GetSeparator_m879 ();
extern "C" void X500DistinguishedName_DecodeRawData_m880 ();
extern "C" void X500DistinguishedName_Canonize_m881 ();
extern "C" void X500DistinguishedName_AreEqual_m882 ();
extern "C" void X509BasicConstraintsExtension__ctor_m883 ();
extern "C" void X509BasicConstraintsExtension__ctor_m884 ();
extern "C" void X509BasicConstraintsExtension__ctor_m885 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m886 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m887 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m888 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m889 ();
extern "C" void X509BasicConstraintsExtension_Decode_m890 ();
extern "C" void X509BasicConstraintsExtension_Encode_m891 ();
extern "C" void X509BasicConstraintsExtension_ToString_m892 ();
extern "C" void X509Certificate2__ctor_m893 ();
extern "C" void X509Certificate2__cctor_m894 ();
extern "C" void X509Certificate2_get_Extensions_m895 ();
extern "C" void X509Certificate2_get_IssuerName_m896 ();
extern "C" void X509Certificate2_get_NotAfter_m897 ();
extern "C" void X509Certificate2_get_NotBefore_m898 ();
extern "C" void X509Certificate2_get_PrivateKey_m899 ();
extern "C" void X509Certificate2_get_PublicKey_m900 ();
extern "C" void X509Certificate2_get_SerialNumber_m901 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m902 ();
extern "C" void X509Certificate2_get_SubjectName_m903 ();
extern "C" void X509Certificate2_get_Thumbprint_m904 ();
extern "C" void X509Certificate2_get_Version_m905 ();
extern "C" void X509Certificate2_GetNameInfo_m906 ();
extern "C" void X509Certificate2_Find_m907 ();
extern "C" void X509Certificate2_GetValueAsString_m908 ();
extern "C" void X509Certificate2_ImportPkcs12_m909 ();
extern "C" void X509Certificate2_Import_m910 ();
extern "C" void X509Certificate2_Reset_m911 ();
extern "C" void X509Certificate2_ToString_m912 ();
extern "C" void X509Certificate2_ToString_m913 ();
extern "C" void X509Certificate2_AppendBuffer_m914 ();
extern "C" void X509Certificate2_Verify_m915 ();
extern "C" void X509Certificate2_get_MonoCertificate_m916 ();
extern "C" void X509Certificate2Collection__ctor_m917 ();
extern "C" void X509Certificate2Collection__ctor_m918 ();
extern "C" void X509Certificate2Collection_get_Item_m919 ();
extern "C" void X509Certificate2Collection_Add_m920 ();
extern "C" void X509Certificate2Collection_AddRange_m921 ();
extern "C" void X509Certificate2Collection_Contains_m922 ();
extern "C" void X509Certificate2Collection_Find_m923 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m924 ();
extern "C" void X509Certificate2Enumerator__ctor_m925 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m926 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m927 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m928 ();
extern "C" void X509Certificate2Enumerator_get_Current_m929 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m930 ();
extern "C" void X509Certificate2Enumerator_Reset_m931 ();
extern "C" void X509CertificateEnumerator__ctor_m932 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m933 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m934 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m935 ();
extern "C" void X509CertificateEnumerator_get_Current_m936 ();
extern "C" void X509CertificateEnumerator_MoveNext_m937 ();
extern "C" void X509CertificateEnumerator_Reset_m938 ();
extern "C" void X509CertificateCollection__ctor_m939 ();
extern "C" void X509CertificateCollection__ctor_m940 ();
extern "C" void X509CertificateCollection_get_Item_m941 ();
extern "C" void X509CertificateCollection_AddRange_m942 ();
extern "C" void X509CertificateCollection_GetEnumerator_m943 ();
extern "C" void X509CertificateCollection_GetHashCode_m944 ();
extern "C" void X509Chain__ctor_m945 ();
extern "C" void X509Chain__ctor_m946 ();
extern "C" void X509Chain__cctor_m947 ();
extern "C" void X509Chain_get_ChainPolicy_m948 ();
extern "C" void X509Chain_Build_m949 ();
extern "C" void X509Chain_Reset_m950 ();
extern "C" void X509Chain_get_Roots_m951 ();
extern "C" void X509Chain_get_CertificateAuthorities_m952 ();
extern "C" void X509Chain_get_CertificateCollection_m953 ();
extern "C" void X509Chain_BuildChainFrom_m954 ();
extern "C" void X509Chain_SelectBestFromCollection_m955 ();
extern "C" void X509Chain_FindParent_m956 ();
extern "C" void X509Chain_IsChainComplete_m957 ();
extern "C" void X509Chain_IsSelfIssued_m958 ();
extern "C" void X509Chain_ValidateChain_m959 ();
extern "C" void X509Chain_Process_m960 ();
extern "C" void X509Chain_PrepareForNextCertificate_m961 ();
extern "C" void X509Chain_WrapUp_m962 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m963 ();
extern "C" void X509Chain_IsSignedWith_m964 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m965 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m966 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m967 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m968 ();
extern "C" void X509Chain_CheckRevocationOnChain_m969 ();
extern "C" void X509Chain_CheckRevocation_m970 ();
extern "C" void X509Chain_CheckRevocation_m971 ();
extern "C" void X509Chain_FindCrl_m972 ();
extern "C" void X509Chain_ProcessCrlExtensions_m973 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m974 ();
extern "C" void X509ChainElement__ctor_m975 ();
extern "C" void X509ChainElement_get_Certificate_m976 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m977 ();
extern "C" void X509ChainElement_get_StatusFlags_m978 ();
extern "C" void X509ChainElement_set_StatusFlags_m979 ();
extern "C" void X509ChainElement_Count_m980 ();
extern "C" void X509ChainElement_Set_m981 ();
extern "C" void X509ChainElement_UncompressFlags_m982 ();
extern "C" void X509ChainElementCollection__ctor_m983 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m984 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m985 ();
extern "C" void X509ChainElementCollection_get_Count_m986 ();
extern "C" void X509ChainElementCollection_get_Item_m987 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m988 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m989 ();
extern "C" void X509ChainElementCollection_Add_m990 ();
extern "C" void X509ChainElementCollection_Clear_m991 ();
extern "C" void X509ChainElementCollection_Contains_m992 ();
extern "C" void X509ChainElementEnumerator__ctor_m993 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m994 ();
extern "C" void X509ChainElementEnumerator_get_Current_m995 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m996 ();
extern "C" void X509ChainElementEnumerator_Reset_m997 ();
extern "C" void X509ChainPolicy__ctor_m998 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m999 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m1000 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m1001 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m1002 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m1003 ();
extern "C" void X509ChainPolicy_Reset_m1004 ();
extern "C" void X509ChainStatus__ctor_m1005 ();
extern "C" void X509ChainStatus_get_Status_m1006 ();
extern "C" void X509ChainStatus_set_Status_m1007 ();
extern "C" void X509ChainStatus_set_StatusInformation_m1008 ();
extern "C" void X509ChainStatus_GetInformation_m1009 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m1010 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m1011 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m1012 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m1013 ();
extern "C" void X509Extension__ctor_m1014 ();
extern "C" void X509Extension__ctor_m1015 ();
extern "C" void X509Extension_get_Critical_m1016 ();
extern "C" void X509Extension_set_Critical_m1017 ();
extern "C" void X509Extension_CopyFrom_m1018 ();
extern "C" void X509Extension_FormatUnkownData_m1019 ();
extern "C" void X509ExtensionCollection__ctor_m1020 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m1021 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1022 ();
extern "C" void X509ExtensionCollection_get_Count_m1023 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m1024 ();
extern "C" void X509ExtensionCollection_get_Item_m1025 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m1026 ();
extern "C" void X509ExtensionEnumerator__ctor_m1027 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m1028 ();
extern "C" void X509ExtensionEnumerator_get_Current_m1029 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m1030 ();
extern "C" void X509ExtensionEnumerator_Reset_m1031 ();
extern "C" void X509KeyUsageExtension__ctor_m1032 ();
extern "C" void X509KeyUsageExtension__ctor_m1033 ();
extern "C" void X509KeyUsageExtension__ctor_m1034 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m1035 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m1036 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m1037 ();
extern "C" void X509KeyUsageExtension_Decode_m1038 ();
extern "C" void X509KeyUsageExtension_Encode_m1039 ();
extern "C" void X509KeyUsageExtension_ToString_m1040 ();
extern "C" void X509Store__ctor_m1041 ();
extern "C" void X509Store_get_Certificates_m1042 ();
extern "C" void X509Store_get_Factory_m1043 ();
extern "C" void X509Store_get_Store_m1044 ();
extern "C" void X509Store_Close_m1045 ();
extern "C" void X509Store_Open_m1046 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1047 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1048 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1049 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1050 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1051 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m1052 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m1053 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m1054 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m1055 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m1056 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m1057 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m1058 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m1059 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m1060 ();
extern "C" void AsnEncodedData__ctor_m1061 ();
extern "C" void AsnEncodedData__ctor_m1062 ();
extern "C" void AsnEncodedData__ctor_m1063 ();
extern "C" void AsnEncodedData_get_Oid_m1064 ();
extern "C" void AsnEncodedData_set_Oid_m1065 ();
extern "C" void AsnEncodedData_get_RawData_m1066 ();
extern "C" void AsnEncodedData_set_RawData_m1067 ();
extern "C" void AsnEncodedData_CopyFrom_m1068 ();
extern "C" void AsnEncodedData_ToString_m1069 ();
extern "C" void AsnEncodedData_Default_m1070 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m1071 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m1072 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m1073 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m1074 ();
extern "C" void AsnEncodedData_SubjectAltName_m1075 ();
extern "C" void AsnEncodedData_NetscapeCertType_m1076 ();
extern "C" void Oid__ctor_m1077 ();
extern "C" void Oid__ctor_m1078 ();
extern "C" void Oid__ctor_m1079 ();
extern "C" void Oid__ctor_m1080 ();
extern "C" void Oid_get_FriendlyName_m1081 ();
extern "C" void Oid_get_Value_m1082 ();
extern "C" void Oid_GetName_m1083 ();
extern "C" void OidCollection__ctor_m1084 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m1085 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m1086 ();
extern "C" void OidCollection_get_Count_m1087 ();
extern "C" void OidCollection_get_Item_m1088 ();
extern "C" void OidCollection_get_SyncRoot_m1089 ();
extern "C" void OidCollection_Add_m1090 ();
extern "C" void OidEnumerator__ctor_m1091 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m1092 ();
extern "C" void OidEnumerator_MoveNext_m1093 ();
extern "C" void OidEnumerator_Reset_m1094 ();
extern "C" void MatchAppendEvaluator__ctor_m1095 ();
extern "C" void MatchAppendEvaluator_Invoke_m1096 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m1097 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m1098 ();
extern "C" void BaseMachine__ctor_m1099 ();
extern "C" void BaseMachine_Replace_m1100 ();
extern "C" void BaseMachine_Scan_m1101 ();
extern "C" void BaseMachine_LTRReplace_m1102 ();
extern "C" void BaseMachine_RTLReplace_m1103 ();
extern "C" void Capture__ctor_m1104 ();
extern "C" void Capture__ctor_m1105 ();
extern "C" void Capture_get_Index_m1106 ();
extern "C" void Capture_get_Length_m1107 ();
extern "C" void Capture_get_Value_m1108 ();
extern "C" void Capture_ToString_m1109 ();
extern "C" void Capture_get_Text_m1110 ();
extern "C" void CaptureCollection__ctor_m1111 ();
extern "C" void CaptureCollection_get_Count_m1112 ();
extern "C" void CaptureCollection_SetValue_m1113 ();
extern "C" void CaptureCollection_get_SyncRoot_m1114 ();
extern "C" void CaptureCollection_CopyTo_m1115 ();
extern "C" void CaptureCollection_GetEnumerator_m1116 ();
extern "C" void Group__ctor_m1117 ();
extern "C" void Group__ctor_m1118 ();
extern "C" void Group__ctor_m1119 ();
extern "C" void Group__cctor_m1120 ();
extern "C" void Group_get_Captures_m1121 ();
extern "C" void Group_get_Success_m1122 ();
extern "C" void GroupCollection__ctor_m1123 ();
extern "C" void GroupCollection_get_Count_m1124 ();
extern "C" void GroupCollection_get_Item_m1125 ();
extern "C" void GroupCollection_SetValue_m1126 ();
extern "C" void GroupCollection_get_SyncRoot_m1127 ();
extern "C" void GroupCollection_CopyTo_m1128 ();
extern "C" void GroupCollection_GetEnumerator_m1129 ();
extern "C" void Match__ctor_m1130 ();
extern "C" void Match__ctor_m1131 ();
extern "C" void Match__ctor_m1132 ();
extern "C" void Match__cctor_m1133 ();
extern "C" void Match_get_Empty_m1134 ();
extern "C" void Match_get_Groups_m1135 ();
extern "C" void Match_NextMatch_m1136 ();
extern "C" void Match_get_Regex_m1137 ();
extern "C" void Enumerator__ctor_m1138 ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m1139 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m1140 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m1141 ();
extern "C" void MatchCollection__ctor_m1142 ();
extern "C" void MatchCollection_get_Count_m1143 ();
extern "C" void MatchCollection_get_Item_m1144 ();
extern "C" void MatchCollection_get_SyncRoot_m1145 ();
extern "C" void MatchCollection_CopyTo_m1146 ();
extern "C" void MatchCollection_GetEnumerator_m1147 ();
extern "C" void MatchCollection_TryToGet_m1148 ();
extern "C" void MatchCollection_get_FullList_m1149 ();
extern "C" void Regex__ctor_m1150 ();
extern "C" void Regex__ctor_m1151 ();
extern "C" void Regex__ctor_m1152 ();
extern "C" void Regex__ctor_m1153 ();
extern "C" void Regex__cctor_m1154 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m1155 ();
extern "C" void Regex_Replace_m658 ();
extern "C" void Regex_Replace_m1156 ();
extern "C" void Regex_validate_options_m1157 ();
extern "C" void Regex_Init_m1158 ();
extern "C" void Regex_InitNewRegex_m1159 ();
extern "C" void Regex_CreateMachineFactory_m1160 ();
extern "C" void Regex_get_Options_m1161 ();
extern "C" void Regex_get_RightToLeft_m1162 ();
extern "C" void Regex_GroupNumberFromName_m1163 ();
extern "C" void Regex_GetGroupIndex_m1164 ();
extern "C" void Regex_default_startat_m1165 ();
extern "C" void Regex_IsMatch_m1166 ();
extern "C" void Regex_IsMatch_m1167 ();
extern "C" void Regex_Match_m1168 ();
extern "C" void Regex_Matches_m1169 ();
extern "C" void Regex_Matches_m1170 ();
extern "C" void Regex_Replace_m1171 ();
extern "C" void Regex_Replace_m1172 ();
extern "C" void Regex_ToString_m1173 ();
extern "C" void Regex_get_GroupCount_m1174 ();
extern "C" void Regex_get_Gap_m1175 ();
extern "C" void Regex_CreateMachine_m1176 ();
extern "C" void Regex_GetGroupNamesArray_m1177 ();
extern "C" void Regex_get_GroupNumbers_m1178 ();
extern "C" void Key__ctor_m1179 ();
extern "C" void Key_GetHashCode_m1180 ();
extern "C" void Key_Equals_m1181 ();
extern "C" void Key_ToString_m1182 ();
extern "C" void FactoryCache__ctor_m1183 ();
extern "C" void FactoryCache_Add_m1184 ();
extern "C" void FactoryCache_Cleanup_m1185 ();
extern "C" void FactoryCache_Lookup_m1186 ();
extern "C" void Node__ctor_m1187 ();
extern "C" void MRUList__ctor_m1188 ();
extern "C" void MRUList_Use_m1189 ();
extern "C" void MRUList_Evict_m1190 ();
extern "C" void CategoryUtils_CategoryFromName_m1191 ();
extern "C" void CategoryUtils_IsCategory_m1192 ();
extern "C" void CategoryUtils_IsCategory_m1193 ();
extern "C" void LinkRef__ctor_m1194 ();
extern "C" void InterpreterFactory__ctor_m1195 ();
extern "C" void InterpreterFactory_NewInstance_m1196 ();
extern "C" void InterpreterFactory_get_GroupCount_m1197 ();
extern "C" void InterpreterFactory_get_Gap_m1198 ();
extern "C" void InterpreterFactory_set_Gap_m1199 ();
extern "C" void InterpreterFactory_get_Mapping_m1200 ();
extern "C" void InterpreterFactory_set_Mapping_m1201 ();
extern "C" void InterpreterFactory_get_NamesMapping_m1202 ();
extern "C" void InterpreterFactory_set_NamesMapping_m1203 ();
extern "C" void PatternLinkStack__ctor_m1204 ();
extern "C" void PatternLinkStack_set_BaseAddress_m1205 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m1206 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m1207 ();
extern "C" void PatternLinkStack_GetOffset_m1208 ();
extern "C" void PatternLinkStack_GetCurrent_m1209 ();
extern "C" void PatternLinkStack_SetCurrent_m1210 ();
extern "C" void PatternCompiler__ctor_m1211 ();
extern "C" void PatternCompiler_EncodeOp_m1212 ();
extern "C" void PatternCompiler_GetMachineFactory_m1213 ();
extern "C" void PatternCompiler_EmitFalse_m1214 ();
extern "C" void PatternCompiler_EmitTrue_m1215 ();
extern "C" void PatternCompiler_EmitCount_m1216 ();
extern "C" void PatternCompiler_EmitCharacter_m1217 ();
extern "C" void PatternCompiler_EmitCategory_m1218 ();
extern "C" void PatternCompiler_EmitNotCategory_m1219 ();
extern "C" void PatternCompiler_EmitRange_m1220 ();
extern "C" void PatternCompiler_EmitSet_m1221 ();
extern "C" void PatternCompiler_EmitString_m1222 ();
extern "C" void PatternCompiler_EmitPosition_m1223 ();
extern "C" void PatternCompiler_EmitOpen_m1224 ();
extern "C" void PatternCompiler_EmitClose_m1225 ();
extern "C" void PatternCompiler_EmitBalanceStart_m1226 ();
extern "C" void PatternCompiler_EmitBalance_m1227 ();
extern "C" void PatternCompiler_EmitReference_m1228 ();
extern "C" void PatternCompiler_EmitIfDefined_m1229 ();
extern "C" void PatternCompiler_EmitSub_m1230 ();
extern "C" void PatternCompiler_EmitTest_m1231 ();
extern "C" void PatternCompiler_EmitBranch_m1232 ();
extern "C" void PatternCompiler_EmitJump_m1233 ();
extern "C" void PatternCompiler_EmitRepeat_m1234 ();
extern "C" void PatternCompiler_EmitUntil_m1235 ();
extern "C" void PatternCompiler_EmitFastRepeat_m1236 ();
extern "C" void PatternCompiler_EmitIn_m1237 ();
extern "C" void PatternCompiler_EmitAnchor_m1238 ();
extern "C" void PatternCompiler_EmitInfo_m1239 ();
extern "C" void PatternCompiler_NewLink_m1240 ();
extern "C" void PatternCompiler_ResolveLink_m1241 ();
extern "C" void PatternCompiler_EmitBranchEnd_m1242 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m1243 ();
extern "C" void PatternCompiler_MakeFlags_m1244 ();
extern "C" void PatternCompiler_Emit_m1245 ();
extern "C" void PatternCompiler_Emit_m1246 ();
extern "C" void PatternCompiler_Emit_m1247 ();
extern "C" void PatternCompiler_get_CurrentAddress_m1248 ();
extern "C" void PatternCompiler_BeginLink_m1249 ();
extern "C" void PatternCompiler_EmitLink_m1250 ();
extern "C" void LinkStack__ctor_m1251 ();
extern "C" void LinkStack_Push_m1252 ();
extern "C" void LinkStack_Pop_m1253 ();
extern "C" void Mark_get_IsDefined_m1254 ();
extern "C" void Mark_get_Index_m1255 ();
extern "C" void Mark_get_Length_m1256 ();
extern "C" void IntStack_Pop_m1257 ();
extern "C" void IntStack_Push_m1258 ();
extern "C" void IntStack_get_Count_m1259 ();
extern "C" void IntStack_set_Count_m1260 ();
extern "C" void RepeatContext__ctor_m1261 ();
extern "C" void RepeatContext_get_Count_m1262 ();
extern "C" void RepeatContext_set_Count_m1263 ();
extern "C" void RepeatContext_get_Start_m1264 ();
extern "C" void RepeatContext_set_Start_m1265 ();
extern "C" void RepeatContext_get_IsMinimum_m1266 ();
extern "C" void RepeatContext_get_IsMaximum_m1267 ();
extern "C" void RepeatContext_get_IsLazy_m1268 ();
extern "C" void RepeatContext_get_Expression_m1269 ();
extern "C" void RepeatContext_get_Previous_m1270 ();
extern "C" void Interpreter__ctor_m1271 ();
extern "C" void Interpreter_ReadProgramCount_m1272 ();
extern "C" void Interpreter_Scan_m1273 ();
extern "C" void Interpreter_Reset_m1274 ();
extern "C" void Interpreter_Eval_m1275 ();
extern "C" void Interpreter_EvalChar_m1276 ();
extern "C" void Interpreter_TryMatch_m1277 ();
extern "C" void Interpreter_IsPosition_m1278 ();
extern "C" void Interpreter_IsWordChar_m1279 ();
extern "C" void Interpreter_GetString_m1280 ();
extern "C" void Interpreter_Open_m1281 ();
extern "C" void Interpreter_Close_m1282 ();
extern "C" void Interpreter_Balance_m1283 ();
extern "C" void Interpreter_Checkpoint_m1284 ();
extern "C" void Interpreter_Backtrack_m1285 ();
extern "C" void Interpreter_ResetGroups_m1286 ();
extern "C" void Interpreter_GetLastDefined_m1287 ();
extern "C" void Interpreter_CreateMark_m1288 ();
extern "C" void Interpreter_GetGroupInfo_m1289 ();
extern "C" void Interpreter_PopulateGroup_m1290 ();
extern "C" void Interpreter_GenerateMatch_m1291 ();
extern "C" void Interval__ctor_m1292 ();
extern "C" void Interval_get_Empty_m1293 ();
extern "C" void Interval_get_IsDiscontiguous_m1294 ();
extern "C" void Interval_get_IsSingleton_m1295 ();
extern "C" void Interval_get_IsEmpty_m1296 ();
extern "C" void Interval_get_Size_m1297 ();
extern "C" void Interval_IsDisjoint_m1298 ();
extern "C" void Interval_IsAdjacent_m1299 ();
extern "C" void Interval_Contains_m1300 ();
extern "C" void Interval_Contains_m1301 ();
extern "C" void Interval_Intersects_m1302 ();
extern "C" void Interval_Merge_m1303 ();
extern "C" void Interval_CompareTo_m1304 ();
extern "C" void Enumerator__ctor_m1305 ();
extern "C" void Enumerator_get_Current_m1306 ();
extern "C" void Enumerator_MoveNext_m1307 ();
extern "C" void Enumerator_Reset_m1308 ();
extern "C" void CostDelegate__ctor_m1309 ();
extern "C" void CostDelegate_Invoke_m1310 ();
extern "C" void CostDelegate_BeginInvoke_m1311 ();
extern "C" void CostDelegate_EndInvoke_m1312 ();
extern "C" void IntervalCollection__ctor_m1313 ();
extern "C" void IntervalCollection_get_Item_m1314 ();
extern "C" void IntervalCollection_Add_m1315 ();
extern "C" void IntervalCollection_Normalize_m1316 ();
extern "C" void IntervalCollection_GetMetaCollection_m1317 ();
extern "C" void IntervalCollection_Optimize_m1318 ();
extern "C" void IntervalCollection_get_Count_m1319 ();
extern "C" void IntervalCollection_get_SyncRoot_m1320 ();
extern "C" void IntervalCollection_CopyTo_m1321 ();
extern "C" void IntervalCollection_GetEnumerator_m1322 ();
extern "C" void Parser__ctor_m1323 ();
extern "C" void Parser_ParseDecimal_m1324 ();
extern "C" void Parser_ParseOctal_m1325 ();
extern "C" void Parser_ParseHex_m1326 ();
extern "C" void Parser_ParseNumber_m1327 ();
extern "C" void Parser_ParseName_m1328 ();
extern "C" void Parser_ParseRegularExpression_m1329 ();
extern "C" void Parser_GetMapping_m1330 ();
extern "C" void Parser_ParseGroup_m1331 ();
extern "C" void Parser_ParseGroupingConstruct_m1332 ();
extern "C" void Parser_ParseAssertionType_m1333 ();
extern "C" void Parser_ParseOptions_m1334 ();
extern "C" void Parser_ParseCharacterClass_m1335 ();
extern "C" void Parser_ParseRepetitionBounds_m1336 ();
extern "C" void Parser_ParseUnicodeCategory_m1337 ();
extern "C" void Parser_ParseSpecial_m1338 ();
extern "C" void Parser_ParseEscape_m1339 ();
extern "C" void Parser_ParseName_m1340 ();
extern "C" void Parser_IsNameChar_m1341 ();
extern "C" void Parser_ParseNumber_m1342 ();
extern "C" void Parser_ParseDigit_m1343 ();
extern "C" void Parser_ConsumeWhitespace_m1344 ();
extern "C" void Parser_ResolveReferences_m1345 ();
extern "C" void Parser_HandleExplicitNumericGroups_m1346 ();
extern "C" void Parser_IsIgnoreCase_m1347 ();
extern "C" void Parser_IsMultiline_m1348 ();
extern "C" void Parser_IsExplicitCapture_m1349 ();
extern "C" void Parser_IsSingleline_m1350 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m1351 ();
extern "C" void Parser_IsECMAScript_m1352 ();
extern "C" void Parser_NewParseException_m1353 ();
extern "C" void QuickSearch__ctor_m1354 ();
extern "C" void QuickSearch__cctor_m1355 ();
extern "C" void QuickSearch_get_Length_m1356 ();
extern "C" void QuickSearch_Search_m1357 ();
extern "C" void QuickSearch_SetupShiftTable_m1358 ();
extern "C" void QuickSearch_GetShiftDistance_m1359 ();
extern "C" void QuickSearch_GetChar_m1360 ();
extern "C" void ReplacementEvaluator__ctor_m1361 ();
extern "C" void ReplacementEvaluator_Evaluate_m1362 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m1363 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m1364 ();
extern "C" void ReplacementEvaluator_Ensure_m1365 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m1366 ();
extern "C" void ReplacementEvaluator_AddInt_m1367 ();
extern "C" void ReplacementEvaluator_Compile_m1368 ();
extern "C" void ReplacementEvaluator_CompileTerm_m1369 ();
extern "C" void ExpressionCollection__ctor_m1370 ();
extern "C" void ExpressionCollection_Add_m1371 ();
extern "C" void ExpressionCollection_get_Item_m1372 ();
extern "C" void ExpressionCollection_set_Item_m1373 ();
extern "C" void ExpressionCollection_OnValidate_m1374 ();
extern "C" void Expression__ctor_m1375 ();
extern "C" void Expression_GetFixedWidth_m1376 ();
extern "C" void Expression_GetAnchorInfo_m1377 ();
extern "C" void CompositeExpression__ctor_m1378 ();
extern "C" void CompositeExpression_get_Expressions_m1379 ();
extern "C" void CompositeExpression_GetWidth_m1380 ();
extern "C" void CompositeExpression_IsComplex_m1381 ();
extern "C" void Group__ctor_m1382 ();
extern "C" void Group_AppendExpression_m1383 ();
extern "C" void Group_Compile_m1384 ();
extern "C" void Group_GetWidth_m1385 ();
extern "C" void Group_GetAnchorInfo_m1386 ();
extern "C" void RegularExpression__ctor_m1387 ();
extern "C" void RegularExpression_set_GroupCount_m1388 ();
extern "C" void RegularExpression_Compile_m1389 ();
extern "C" void CapturingGroup__ctor_m1390 ();
extern "C" void CapturingGroup_get_Index_m1391 ();
extern "C" void CapturingGroup_set_Index_m1392 ();
extern "C" void CapturingGroup_get_Name_m1393 ();
extern "C" void CapturingGroup_set_Name_m1394 ();
extern "C" void CapturingGroup_get_IsNamed_m1395 ();
extern "C" void CapturingGroup_Compile_m1396 ();
extern "C" void CapturingGroup_IsComplex_m1397 ();
extern "C" void CapturingGroup_CompareTo_m1398 ();
extern "C" void BalancingGroup__ctor_m1399 ();
extern "C" void BalancingGroup_set_Balance_m1400 ();
extern "C" void BalancingGroup_Compile_m1401 ();
extern "C" void NonBacktrackingGroup__ctor_m1402 ();
extern "C" void NonBacktrackingGroup_Compile_m1403 ();
extern "C" void NonBacktrackingGroup_IsComplex_m1404 ();
extern "C" void Repetition__ctor_m1405 ();
extern "C" void Repetition_get_Expression_m1406 ();
extern "C" void Repetition_set_Expression_m1407 ();
extern "C" void Repetition_get_Minimum_m1408 ();
extern "C" void Repetition_Compile_m1409 ();
extern "C" void Repetition_GetWidth_m1410 ();
extern "C" void Repetition_GetAnchorInfo_m1411 ();
extern "C" void Assertion__ctor_m1412 ();
extern "C" void Assertion_get_TrueExpression_m1413 ();
extern "C" void Assertion_set_TrueExpression_m1414 ();
extern "C" void Assertion_get_FalseExpression_m1415 ();
extern "C" void Assertion_set_FalseExpression_m1416 ();
extern "C" void Assertion_GetWidth_m1417 ();
extern "C" void CaptureAssertion__ctor_m1418 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m1419 ();
extern "C" void CaptureAssertion_Compile_m1420 ();
extern "C" void CaptureAssertion_IsComplex_m1421 ();
extern "C" void CaptureAssertion_get_Alternate_m1422 ();
extern "C" void ExpressionAssertion__ctor_m1423 ();
extern "C" void ExpressionAssertion_set_Reverse_m1424 ();
extern "C" void ExpressionAssertion_set_Negate_m1425 ();
extern "C" void ExpressionAssertion_get_TestExpression_m1426 ();
extern "C" void ExpressionAssertion_set_TestExpression_m1427 ();
extern "C" void ExpressionAssertion_Compile_m1428 ();
extern "C" void ExpressionAssertion_IsComplex_m1429 ();
extern "C" void Alternation__ctor_m1430 ();
extern "C" void Alternation_get_Alternatives_m1431 ();
extern "C" void Alternation_AddAlternative_m1432 ();
extern "C" void Alternation_Compile_m1433 ();
extern "C" void Alternation_GetWidth_m1434 ();
extern "C" void Literal__ctor_m1435 ();
extern "C" void Literal_CompileLiteral_m1436 ();
extern "C" void Literal_Compile_m1437 ();
extern "C" void Literal_GetWidth_m1438 ();
extern "C" void Literal_GetAnchorInfo_m1439 ();
extern "C" void Literal_IsComplex_m1440 ();
extern "C" void PositionAssertion__ctor_m1441 ();
extern "C" void PositionAssertion_Compile_m1442 ();
extern "C" void PositionAssertion_GetWidth_m1443 ();
extern "C" void PositionAssertion_IsComplex_m1444 ();
extern "C" void PositionAssertion_GetAnchorInfo_m1445 ();
extern "C" void Reference__ctor_m1446 ();
extern "C" void Reference_get_CapturingGroup_m1447 ();
extern "C" void Reference_set_CapturingGroup_m1448 ();
extern "C" void Reference_get_IgnoreCase_m1449 ();
extern "C" void Reference_Compile_m1450 ();
extern "C" void Reference_GetWidth_m1451 ();
extern "C" void Reference_IsComplex_m1452 ();
extern "C" void BackslashNumber__ctor_m1453 ();
extern "C" void BackslashNumber_ResolveReference_m1454 ();
extern "C" void BackslashNumber_Compile_m1455 ();
extern "C" void CharacterClass__ctor_m1456 ();
extern "C" void CharacterClass__ctor_m1457 ();
extern "C" void CharacterClass__cctor_m1458 ();
extern "C" void CharacterClass_AddCategory_m1459 ();
extern "C" void CharacterClass_AddCharacter_m1460 ();
extern "C" void CharacterClass_AddRange_m1461 ();
extern "C" void CharacterClass_Compile_m1462 ();
extern "C" void CharacterClass_GetWidth_m1463 ();
extern "C" void CharacterClass_IsComplex_m1464 ();
extern "C" void CharacterClass_GetIntervalCost_m1465 ();
extern "C" void AnchorInfo__ctor_m1466 ();
extern "C" void AnchorInfo__ctor_m1467 ();
extern "C" void AnchorInfo__ctor_m1468 ();
extern "C" void AnchorInfo_get_Offset_m1469 ();
extern "C" void AnchorInfo_get_Width_m1470 ();
extern "C" void AnchorInfo_get_Length_m1471 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m1472 ();
extern "C" void AnchorInfo_get_IsComplete_m1473 ();
extern "C" void AnchorInfo_get_Substring_m1474 ();
extern "C" void AnchorInfo_get_IgnoreCase_m1475 ();
extern "C" void AnchorInfo_get_Position_m1476 ();
extern "C" void AnchorInfo_get_IsSubstring_m1477 ();
extern "C" void AnchorInfo_get_IsPosition_m1478 ();
extern "C" void AnchorInfo_GetInterval_m1479 ();
extern "C" void DefaultUriParser__ctor_m1480 ();
extern "C" void DefaultUriParser__ctor_m1481 ();
extern "C" void UriScheme__ctor_m1482 ();
extern "C" void Uri__ctor_m1483 ();
extern "C" void Uri__ctor_m1484 ();
extern "C" void Uri__ctor_m1485 ();
extern "C" void Uri__cctor_m1486 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m1487 ();
extern "C" void Uri_get_AbsoluteUri_m1488 ();
extern "C" void Uri_get_Authority_m1489 ();
extern "C" void Uri_get_Host_m1490 ();
extern "C" void Uri_get_IsFile_m1491 ();
extern "C" void Uri_get_IsLoopback_m1492 ();
extern "C" void Uri_get_IsUnc_m1493 ();
extern "C" void Uri_get_Scheme_m1494 ();
extern "C" void Uri_get_IsAbsoluteUri_m1495 ();
extern "C" void Uri_CheckHostName_m1496 ();
extern "C" void Uri_IsIPv4Address_m1497 ();
extern "C" void Uri_IsDomainAddress_m1498 ();
extern "C" void Uri_CheckSchemeName_m1499 ();
extern "C" void Uri_IsAlpha_m1500 ();
extern "C" void Uri_Equals_m1501 ();
extern "C" void Uri_InternalEquals_m1502 ();
extern "C" void Uri_GetHashCode_m1503 ();
extern "C" void Uri_GetLeftPart_m1504 ();
extern "C" void Uri_FromHex_m1505 ();
extern "C" void Uri_HexEscape_m1506 ();
extern "C" void Uri_IsHexDigit_m1507 ();
extern "C" void Uri_IsHexEncoding_m1508 ();
extern "C" void Uri_AppendQueryAndFragment_m1509 ();
extern "C" void Uri_ToString_m1510 ();
extern "C" void Uri_EscapeString_m1511 ();
extern "C" void Uri_EscapeString_m1512 ();
extern "C" void Uri_ParseUri_m1513 ();
extern "C" void Uri_Unescape_m1514 ();
extern "C" void Uri_Unescape_m1515 ();
extern "C" void Uri_ParseAsWindowsUNC_m1516 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m1517 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m1518 ();
extern "C" void Uri_Parse_m1519 ();
extern "C" void Uri_ParseNoExceptions_m1520 ();
extern "C" void Uri_CompactEscaped_m1521 ();
extern "C" void Uri_Reduce_m1522 ();
extern "C" void Uri_HexUnescapeMultiByte_m1523 ();
extern "C" void Uri_GetSchemeDelimiter_m1524 ();
extern "C" void Uri_GetDefaultPort_m1525 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m1526 ();
extern "C" void Uri_IsPredefinedScheme_m1527 ();
extern "C" void Uri_get_Parser_m1528 ();
extern "C" void Uri_EnsureAbsoluteUri_m1529 ();
extern "C" void Uri_op_Equality_m1530 ();
extern "C" void UriFormatException__ctor_m1531 ();
extern "C" void UriFormatException__ctor_m1532 ();
extern "C" void UriFormatException__ctor_m1533 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m1534 ();
extern "C" void UriParser__ctor_m1535 ();
extern "C" void UriParser__cctor_m1536 ();
extern "C" void UriParser_InitializeAndValidate_m1537 ();
extern "C" void UriParser_OnRegister_m1538 ();
extern "C" void UriParser_set_SchemeName_m1539 ();
extern "C" void UriParser_get_DefaultPort_m1540 ();
extern "C" void UriParser_set_DefaultPort_m1541 ();
extern "C" void UriParser_CreateDefaults_m1542 ();
extern "C" void UriParser_InternalRegister_m1543 ();
extern "C" void UriParser_GetParser_m1544 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m1545 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m1546 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m1547 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m1548 ();
extern "C" void MatchEvaluator__ctor_m1549 ();
extern "C" void MatchEvaluator_Invoke_m1550 ();
extern "C" void MatchEvaluator_BeginInvoke_m1551 ();
extern "C" void MatchEvaluator_EndInvoke_m1552 ();
extern "C" void ExtensionAttribute__ctor_m1750 ();
extern "C" void Locale_GetText_m1751 ();
extern "C" void Locale_GetText_m1752 ();
extern "C" void KeyBuilder_get_Rng_m1753 ();
extern "C" void KeyBuilder_Key_m1754 ();
extern "C" void KeyBuilder_IV_m1755 ();
extern "C" void SymmetricTransform__ctor_m1756 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m1757 ();
extern "C" void SymmetricTransform_Finalize_m1758 ();
extern "C" void SymmetricTransform_Dispose_m1759 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m1760 ();
extern "C" void SymmetricTransform_Transform_m1761 ();
extern "C" void SymmetricTransform_CBC_m1762 ();
extern "C" void SymmetricTransform_CFB_m1763 ();
extern "C" void SymmetricTransform_OFB_m1764 ();
extern "C" void SymmetricTransform_CTS_m1765 ();
extern "C" void SymmetricTransform_CheckInput_m1766 ();
extern "C" void SymmetricTransform_TransformBlock_m1767 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m1768 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m1769 ();
extern "C" void SymmetricTransform_Random_m1770 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m1771 ();
extern "C" void SymmetricTransform_FinalEncrypt_m1772 ();
extern "C" void SymmetricTransform_FinalDecrypt_m1773 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m1774 ();
extern "C" void Aes__ctor_m1775 ();
extern "C" void AesManaged__ctor_m1776 ();
extern "C" void AesManaged_GenerateIV_m1777 ();
extern "C" void AesManaged_GenerateKey_m1778 ();
extern "C" void AesManaged_CreateDecryptor_m1779 ();
extern "C" void AesManaged_CreateEncryptor_m1780 ();
extern "C" void AesManaged_get_IV_m1781 ();
extern "C" void AesManaged_set_IV_m1782 ();
extern "C" void AesManaged_get_Key_m1783 ();
extern "C" void AesManaged_set_Key_m1784 ();
extern "C" void AesManaged_get_KeySize_m1785 ();
extern "C" void AesManaged_set_KeySize_m1786 ();
extern "C" void AesManaged_CreateDecryptor_m1787 ();
extern "C" void AesManaged_CreateEncryptor_m1788 ();
extern "C" void AesManaged_Dispose_m1789 ();
extern "C" void AesTransform__ctor_m1790 ();
extern "C" void AesTransform__cctor_m1791 ();
extern "C" void AesTransform_ECB_m1792 ();
extern "C" void AesTransform_SubByte_m1793 ();
extern "C" void AesTransform_Encrypt128_m1794 ();
extern "C" void AesTransform_Decrypt128_m1795 ();
extern "C" void Locale_GetText_m1811 ();
extern "C" void ModulusRing__ctor_m1812 ();
extern "C" void ModulusRing_BarrettReduction_m1813 ();
extern "C" void ModulusRing_Multiply_m1814 ();
extern "C" void ModulusRing_Difference_m1815 ();
extern "C" void ModulusRing_Pow_m1816 ();
extern "C" void ModulusRing_Pow_m1817 ();
extern "C" void Kernel_AddSameSign_m1818 ();
extern "C" void Kernel_Subtract_m1819 ();
extern "C" void Kernel_MinusEq_m1820 ();
extern "C" void Kernel_PlusEq_m1821 ();
extern "C" void Kernel_Compare_m1822 ();
extern "C" void Kernel_SingleByteDivideInPlace_m1823 ();
extern "C" void Kernel_DwordMod_m1824 ();
extern "C" void Kernel_DwordDivMod_m1825 ();
extern "C" void Kernel_multiByteDivide_m1826 ();
extern "C" void Kernel_LeftShift_m1827 ();
extern "C" void Kernel_RightShift_m1828 ();
extern "C" void Kernel_Multiply_m1829 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m1830 ();
extern "C" void Kernel_modInverse_m1831 ();
extern "C" void Kernel_modInverse_m1832 ();
extern "C" void BigInteger__ctor_m1833 ();
extern "C" void BigInteger__ctor_m1834 ();
extern "C" void BigInteger__ctor_m1835 ();
extern "C" void BigInteger__ctor_m1836 ();
extern "C" void BigInteger__ctor_m1837 ();
extern "C" void BigInteger__cctor_m1838 ();
extern "C" void BigInteger_get_Rng_m1839 ();
extern "C" void BigInteger_GenerateRandom_m1840 ();
extern "C" void BigInteger_GenerateRandom_m1841 ();
extern "C" void BigInteger_BitCount_m1842 ();
extern "C" void BigInteger_TestBit_m1843 ();
extern "C" void BigInteger_SetBit_m1844 ();
extern "C" void BigInteger_SetBit_m1845 ();
extern "C" void BigInteger_LowestSetBit_m1846 ();
extern "C" void BigInteger_GetBytes_m1847 ();
extern "C" void BigInteger_ToString_m1848 ();
extern "C" void BigInteger_ToString_m1849 ();
extern "C" void BigInteger_Normalize_m1850 ();
extern "C" void BigInteger_Clear_m1851 ();
extern "C" void BigInteger_GetHashCode_m1852 ();
extern "C" void BigInteger_ToString_m1853 ();
extern "C" void BigInteger_Equals_m1854 ();
extern "C" void BigInteger_ModInverse_m1855 ();
extern "C" void BigInteger_ModPow_m1856 ();
extern "C" void BigInteger_GeneratePseudoPrime_m1857 ();
extern "C" void BigInteger_Incr2_m1858 ();
extern "C" void BigInteger_op_Implicit_m1859 ();
extern "C" void BigInteger_op_Implicit_m1860 ();
extern "C" void BigInteger_op_Addition_m1861 ();
extern "C" void BigInteger_op_Subtraction_m1862 ();
extern "C" void BigInteger_op_Modulus_m1863 ();
extern "C" void BigInteger_op_Modulus_m1864 ();
extern "C" void BigInteger_op_Division_m1865 ();
extern "C" void BigInteger_op_Multiply_m1866 ();
extern "C" void BigInteger_op_LeftShift_m1867 ();
extern "C" void BigInteger_op_RightShift_m1868 ();
extern "C" void BigInteger_op_Equality_m1869 ();
extern "C" void BigInteger_op_Inequality_m1870 ();
extern "C" void BigInteger_op_Equality_m1871 ();
extern "C" void BigInteger_op_Inequality_m1872 ();
extern "C" void BigInteger_op_GreaterThan_m1873 ();
extern "C" void BigInteger_op_LessThan_m1874 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m1875 ();
extern "C" void BigInteger_op_LessThanOrEqual_m1876 ();
extern "C" void PrimalityTests_GetSPPRounds_m1877 ();
extern "C" void PrimalityTests_RabinMillerTest_m1878 ();
extern "C" void PrimeGeneratorBase__ctor_m1879 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m1880 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m1881 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m1882 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m1883 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1884 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1885 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1886 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1887 ();
extern "C" void ASN1__ctor_m1643 ();
extern "C" void ASN1__ctor_m1644 ();
extern "C" void ASN1__ctor_m1626 ();
extern "C" void ASN1_get_Count_m1630 ();
extern "C" void ASN1_get_Tag_m1627 ();
extern "C" void ASN1_get_Length_m1657 ();
extern "C" void ASN1_get_Value_m1629 ();
extern "C" void ASN1_set_Value_m1888 ();
extern "C" void ASN1_CompareArray_m1889 ();
extern "C" void ASN1_CompareValue_m1656 ();
extern "C" void ASN1_Add_m1645 ();
extern "C" void ASN1_GetBytes_m1890 ();
extern "C" void ASN1_Decode_m1891 ();
extern "C" void ASN1_DecodeTLV_m1892 ();
extern "C" void ASN1_get_Item_m1631 ();
extern "C" void ASN1_Element_m1893 ();
extern "C" void ASN1_ToString_m1894 ();
extern "C" void ASN1Convert_FromInt32_m1646 ();
extern "C" void ASN1Convert_FromOid_m1895 ();
extern "C" void ASN1Convert_ToInt32_m1642 ();
extern "C" void ASN1Convert_ToOid_m1697 ();
extern "C" void ASN1Convert_ToDateTime_m1896 ();
extern "C" void BitConverterLE_GetUIntBytes_m1897 ();
extern "C" void BitConverterLE_GetBytes_m1898 ();
extern "C" void ContentInfo__ctor_m1899 ();
extern "C" void ContentInfo__ctor_m1900 ();
extern "C" void ContentInfo__ctor_m1901 ();
extern "C" void ContentInfo__ctor_m1902 ();
extern "C" void ContentInfo_get_ASN1_m1903 ();
extern "C" void ContentInfo_get_Content_m1904 ();
extern "C" void ContentInfo_set_Content_m1905 ();
extern "C" void ContentInfo_get_ContentType_m1906 ();
extern "C" void ContentInfo_set_ContentType_m1907 ();
extern "C" void ContentInfo_GetASN1_m1908 ();
extern "C" void EncryptedData__ctor_m1909 ();
extern "C" void EncryptedData__ctor_m1910 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m1911 ();
extern "C" void EncryptedData_get_EncryptedContent_m1912 ();
extern "C" void ARC4Managed__ctor_m1913 ();
extern "C" void ARC4Managed_Finalize_m1914 ();
extern "C" void ARC4Managed_Dispose_m1915 ();
extern "C" void ARC4Managed_get_Key_m1916 ();
extern "C" void ARC4Managed_set_Key_m1917 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m1918 ();
extern "C" void ARC4Managed_CreateEncryptor_m1919 ();
extern "C" void ARC4Managed_CreateDecryptor_m1920 ();
extern "C" void ARC4Managed_GenerateIV_m1921 ();
extern "C" void ARC4Managed_GenerateKey_m1922 ();
extern "C" void ARC4Managed_KeySetup_m1923 ();
extern "C" void ARC4Managed_CheckInput_m1924 ();
extern "C" void ARC4Managed_TransformBlock_m1925 ();
extern "C" void ARC4Managed_InternalTransformBlock_m1926 ();
extern "C" void ARC4Managed_TransformFinalBlock_m1927 ();
extern "C" void CryptoConvert_ToHex_m1711 ();
extern "C" void KeyBuilder_get_Rng_m1928 ();
extern "C" void KeyBuilder_Key_m1929 ();
extern "C" void MD2__ctor_m1930 ();
extern "C" void MD2_Create_m1931 ();
extern "C" void MD2_Create_m1932 ();
extern "C" void MD2Managed__ctor_m1933 ();
extern "C" void MD2Managed__cctor_m1934 ();
extern "C" void MD2Managed_Padding_m1935 ();
extern "C" void MD2Managed_Initialize_m1936 ();
extern "C" void MD2Managed_HashCore_m1937 ();
extern "C" void MD2Managed_HashFinal_m1938 ();
extern "C" void MD2Managed_MD2Transform_m1939 ();
extern "C" void PKCS1__cctor_m1940 ();
extern "C" void PKCS1_Compare_m1941 ();
extern "C" void PKCS1_I2OSP_m1942 ();
extern "C" void PKCS1_OS2IP_m1943 ();
extern "C" void PKCS1_RSASP1_m1944 ();
extern "C" void PKCS1_RSAVP1_m1945 ();
extern "C" void PKCS1_Sign_v15_m1946 ();
extern "C" void PKCS1_Verify_v15_m1947 ();
extern "C" void PKCS1_Verify_v15_m1948 ();
extern "C" void PKCS1_Encode_v15_m1949 ();
extern "C" void PrivateKeyInfo__ctor_m1950 ();
extern "C" void PrivateKeyInfo__ctor_m1951 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m1952 ();
extern "C" void PrivateKeyInfo_Decode_m1953 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m1954 ();
extern "C" void PrivateKeyInfo_Normalize_m1955 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m1956 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m1957 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1958 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m1959 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m1960 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m1961 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m1962 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m1963 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m1964 ();
extern "C" void RC4__ctor_m1965 ();
extern "C" void RC4__cctor_m1966 ();
extern "C" void RC4_get_IV_m1967 ();
extern "C" void RC4_set_IV_m1968 ();
extern "C" void KeyGeneratedEventHandler__ctor_m1969 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m1970 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m1971 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m1972 ();
extern "C" void RSAManaged__ctor_m1973 ();
extern "C" void RSAManaged__ctor_m1974 ();
extern "C" void RSAManaged_Finalize_m1975 ();
extern "C" void RSAManaged_GenerateKeyPair_m1976 ();
extern "C" void RSAManaged_get_KeySize_m1977 ();
extern "C" void RSAManaged_get_PublicOnly_m1619 ();
extern "C" void RSAManaged_DecryptValue_m1978 ();
extern "C" void RSAManaged_EncryptValue_m1979 ();
extern "C" void RSAManaged_ExportParameters_m1980 ();
extern "C" void RSAManaged_ImportParameters_m1981 ();
extern "C" void RSAManaged_Dispose_m1982 ();
extern "C" void RSAManaged_ToXmlString_m1983 ();
extern "C" void RSAManaged_GetPaddedValue_m1984 ();
extern "C" void SafeBag__ctor_m1985 ();
extern "C" void SafeBag_get_BagOID_m1986 ();
extern "C" void SafeBag_get_ASN1_m1987 ();
extern "C" void DeriveBytes__ctor_m1988 ();
extern "C" void DeriveBytes__cctor_m1989 ();
extern "C" void DeriveBytes_set_HashName_m1990 ();
extern "C" void DeriveBytes_set_IterationCount_m1991 ();
extern "C" void DeriveBytes_set_Password_m1992 ();
extern "C" void DeriveBytes_set_Salt_m1993 ();
extern "C" void DeriveBytes_Adjust_m1994 ();
extern "C" void DeriveBytes_Derive_m1995 ();
extern "C" void DeriveBytes_DeriveKey_m1996 ();
extern "C" void DeriveBytes_DeriveIV_m1997 ();
extern "C" void DeriveBytes_DeriveMAC_m1998 ();
extern "C" void PKCS12__ctor_m1999 ();
extern "C" void PKCS12__ctor_m1659 ();
extern "C" void PKCS12__ctor_m1660 ();
extern "C" void PKCS12__cctor_m2000 ();
extern "C" void PKCS12_Decode_m2001 ();
extern "C" void PKCS12_Finalize_m2002 ();
extern "C" void PKCS12_set_Password_m2003 ();
extern "C" void PKCS12_get_IterationCount_m2004 ();
extern "C" void PKCS12_set_IterationCount_m2005 ();
extern "C" void PKCS12_get_Keys_m1663 ();
extern "C" void PKCS12_get_Certificates_m1661 ();
extern "C" void PKCS12_get_RNG_m2006 ();
extern "C" void PKCS12_Compare_m2007 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m2008 ();
extern "C" void PKCS12_Decrypt_m2009 ();
extern "C" void PKCS12_Decrypt_m2010 ();
extern "C" void PKCS12_Encrypt_m2011 ();
extern "C" void PKCS12_GetExistingParameters_m2012 ();
extern "C" void PKCS12_AddPrivateKey_m2013 ();
extern "C" void PKCS12_ReadSafeBag_m2014 ();
extern "C" void PKCS12_CertificateSafeBag_m2015 ();
extern "C" void PKCS12_MAC_m2016 ();
extern "C" void PKCS12_GetBytes_m2017 ();
extern "C" void PKCS12_EncryptedContentInfo_m2018 ();
extern "C" void PKCS12_AddCertificate_m2019 ();
extern "C" void PKCS12_AddCertificate_m2020 ();
extern "C" void PKCS12_RemoveCertificate_m2021 ();
extern "C" void PKCS12_RemoveCertificate_m2022 ();
extern "C" void PKCS12_Clone_m2023 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m2024 ();
extern "C" void X501__cctor_m2025 ();
extern "C" void X501_ToString_m2026 ();
extern "C" void X501_ToString_m1635 ();
extern "C" void X501_AppendEntry_m2027 ();
extern "C" void X509Certificate__ctor_m1666 ();
extern "C" void X509Certificate__cctor_m2028 ();
extern "C" void X509Certificate_Parse_m2029 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m2030 ();
extern "C" void X509Certificate_get_DSA_m1621 ();
extern "C" void X509Certificate_set_DSA_m1664 ();
extern "C" void X509Certificate_get_Extensions_m1683 ();
extern "C" void X509Certificate_get_Hash_m2031 ();
extern "C" void X509Certificate_get_IssuerName_m2032 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m2033 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m2034 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m2035 ();
extern "C" void X509Certificate_get_PublicKey_m2036 ();
extern "C" void X509Certificate_get_RSA_m2037 ();
extern "C" void X509Certificate_set_RSA_m2038 ();
extern "C" void X509Certificate_get_RawData_m2039 ();
extern "C" void X509Certificate_get_SerialNumber_m2040 ();
extern "C" void X509Certificate_get_Signature_m2041 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m2042 ();
extern "C" void X509Certificate_get_SubjectName_m2043 ();
extern "C" void X509Certificate_get_ValidFrom_m2044 ();
extern "C" void X509Certificate_get_ValidUntil_m2045 ();
extern "C" void X509Certificate_get_Version_m1655 ();
extern "C" void X509Certificate_get_IsCurrent_m2046 ();
extern "C" void X509Certificate_WasCurrent_m2047 ();
extern "C" void X509Certificate_VerifySignature_m2048 ();
extern "C" void X509Certificate_VerifySignature_m2049 ();
extern "C" void X509Certificate_VerifySignature_m1682 ();
extern "C" void X509Certificate_get_IsSelfSigned_m2050 ();
extern "C" void X509Certificate_GetIssuerName_m1650 ();
extern "C" void X509Certificate_GetSubjectName_m1653 ();
extern "C" void X509Certificate_GetObjectData_m2051 ();
extern "C" void X509Certificate_PEM_m2052 ();
extern "C" void X509CertificateEnumerator__ctor_m2053 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m2054 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2055 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m2056 ();
extern "C" void X509CertificateEnumerator_get_Current_m1708 ();
extern "C" void X509CertificateEnumerator_MoveNext_m2057 ();
extern "C" void X509CertificateEnumerator_Reset_m2058 ();
extern "C" void X509CertificateCollection__ctor_m2059 ();
extern "C" void X509CertificateCollection__ctor_m2060 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m2061 ();
extern "C" void X509CertificateCollection_get_Item_m1662 ();
extern "C" void X509CertificateCollection_Add_m2062 ();
extern "C" void X509CertificateCollection_AddRange_m2063 ();
extern "C" void X509CertificateCollection_Contains_m2064 ();
extern "C" void X509CertificateCollection_GetEnumerator_m1707 ();
extern "C" void X509CertificateCollection_GetHashCode_m2065 ();
extern "C" void X509CertificateCollection_IndexOf_m2066 ();
extern "C" void X509CertificateCollection_Remove_m2067 ();
extern "C" void X509CertificateCollection_Compare_m2068 ();
extern "C" void X509Chain__ctor_m2069 ();
extern "C" void X509Chain__ctor_m2070 ();
extern "C" void X509Chain_get_Status_m2071 ();
extern "C" void X509Chain_get_TrustAnchors_m2072 ();
extern "C" void X509Chain_Build_m2073 ();
extern "C" void X509Chain_IsValid_m2074 ();
extern "C" void X509Chain_FindCertificateParent_m2075 ();
extern "C" void X509Chain_FindCertificateRoot_m2076 ();
extern "C" void X509Chain_IsTrusted_m2077 ();
extern "C" void X509Chain_IsParent_m2078 ();
extern "C" void X509CrlEntry__ctor_m2079 ();
extern "C" void X509CrlEntry_get_SerialNumber_m2080 ();
extern "C" void X509CrlEntry_get_RevocationDate_m1690 ();
extern "C" void X509CrlEntry_get_Extensions_m1696 ();
extern "C" void X509Crl__ctor_m2081 ();
extern "C" void X509Crl_Parse_m2082 ();
extern "C" void X509Crl_get_Extensions_m1685 ();
extern "C" void X509Crl_get_Hash_m2083 ();
extern "C" void X509Crl_get_IssuerName_m1693 ();
extern "C" void X509Crl_get_NextUpdate_m1691 ();
extern "C" void X509Crl_Compare_m2084 ();
extern "C" void X509Crl_GetCrlEntry_m1689 ();
extern "C" void X509Crl_GetCrlEntry_m2085 ();
extern "C" void X509Crl_GetHashName_m2086 ();
extern "C" void X509Crl_VerifySignature_m2087 ();
extern "C" void X509Crl_VerifySignature_m2088 ();
extern "C" void X509Crl_VerifySignature_m1688 ();
extern "C" void X509Extension__ctor_m2089 ();
extern "C" void X509Extension__ctor_m2090 ();
extern "C" void X509Extension_Decode_m2091 ();
extern "C" void X509Extension_Encode_m2092 ();
extern "C" void X509Extension_get_Oid_m1695 ();
extern "C" void X509Extension_get_Critical_m1694 ();
extern "C" void X509Extension_get_Value_m1699 ();
extern "C" void X509Extension_Equals_m2093 ();
extern "C" void X509Extension_GetHashCode_m2094 ();
extern "C" void X509Extension_WriteLine_m2095 ();
extern "C" void X509Extension_ToString_m2096 ();
extern "C" void X509ExtensionCollection__ctor_m2097 ();
extern "C" void X509ExtensionCollection__ctor_m2098 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2099 ();
extern "C" void X509ExtensionCollection_IndexOf_m2100 ();
extern "C" void X509ExtensionCollection_get_Item_m1684 ();
extern "C" void X509Store__ctor_m2101 ();
extern "C" void X509Store_get_Certificates_m1706 ();
extern "C" void X509Store_get_Crls_m1692 ();
extern "C" void X509Store_Load_m2102 ();
extern "C" void X509Store_LoadCertificate_m2103 ();
extern "C" void X509Store_LoadCrl_m2104 ();
extern "C" void X509Store_CheckStore_m2105 ();
extern "C" void X509Store_BuildCertificatesCollection_m2106 ();
extern "C" void X509Store_BuildCrlsCollection_m2107 ();
extern "C" void X509StoreManager_get_CurrentUser_m1703 ();
extern "C" void X509StoreManager_get_LocalMachine_m1704 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m2108 ();
extern "C" void X509Stores__ctor_m2109 ();
extern "C" void X509Stores_get_TrustedRoot_m2110 ();
extern "C" void X509Stores_Open_m1705 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m1686 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m2111 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m1687 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m2112 ();
extern "C" void BasicConstraintsExtension__ctor_m2113 ();
extern "C" void BasicConstraintsExtension_Decode_m2114 ();
extern "C" void BasicConstraintsExtension_Encode_m2115 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m2116 ();
extern "C" void BasicConstraintsExtension_ToString_m2117 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m2118 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m2119 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m2120 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m2121 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m2122 ();
extern "C" void GeneralNames__ctor_m2123 ();
extern "C" void GeneralNames_get_DNSNames_m2124 ();
extern "C" void GeneralNames_get_IPAddresses_m2125 ();
extern "C" void GeneralNames_ToString_m2126 ();
extern "C" void KeyUsageExtension__ctor_m2127 ();
extern "C" void KeyUsageExtension_Decode_m2128 ();
extern "C" void KeyUsageExtension_Encode_m2129 ();
extern "C" void KeyUsageExtension_Support_m2130 ();
extern "C" void KeyUsageExtension_ToString_m2131 ();
extern "C" void NetscapeCertTypeExtension__ctor_m2132 ();
extern "C" void NetscapeCertTypeExtension_Decode_m2133 ();
extern "C" void NetscapeCertTypeExtension_Support_m2134 ();
extern "C" void NetscapeCertTypeExtension_ToString_m2135 ();
extern "C" void SubjectAltNameExtension__ctor_m2136 ();
extern "C" void SubjectAltNameExtension_Decode_m2137 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m2138 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m2139 ();
extern "C" void SubjectAltNameExtension_ToString_m2140 ();
extern "C" void HMAC__ctor_m2141 ();
extern "C" void HMAC_get_Key_m2142 ();
extern "C" void HMAC_set_Key_m2143 ();
extern "C" void HMAC_Initialize_m2144 ();
extern "C" void HMAC_HashFinal_m2145 ();
extern "C" void HMAC_HashCore_m2146 ();
extern "C" void HMAC_initializePad_m2147 ();
extern "C" void MD5SHA1__ctor_m2148 ();
extern "C" void MD5SHA1_Initialize_m2149 ();
extern "C" void MD5SHA1_HashFinal_m2150 ();
extern "C" void MD5SHA1_HashCore_m2151 ();
extern "C" void MD5SHA1_CreateSignature_m2152 ();
extern "C" void MD5SHA1_VerifySignature_m2153 ();
extern "C" void Alert__ctor_m2154 ();
extern "C" void Alert__ctor_m2155 ();
extern "C" void Alert_get_Level_m2156 ();
extern "C" void Alert_get_Description_m2157 ();
extern "C" void Alert_get_IsWarning_m2158 ();
extern "C" void Alert_get_IsCloseNotify_m2159 ();
extern "C" void Alert_inferAlertLevel_m2160 ();
extern "C" void Alert_GetAlertMessage_m2161 ();
extern "C" void CipherSuite__ctor_m2162 ();
extern "C" void CipherSuite__cctor_m2163 ();
extern "C" void CipherSuite_get_EncryptionCipher_m2164 ();
extern "C" void CipherSuite_get_DecryptionCipher_m2165 ();
extern "C" void CipherSuite_get_ClientHMAC_m2166 ();
extern "C" void CipherSuite_get_ServerHMAC_m2167 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m2168 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m2169 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m2170 ();
extern "C" void CipherSuite_get_HashSize_m2171 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m2172 ();
extern "C" void CipherSuite_get_CipherMode_m2173 ();
extern "C" void CipherSuite_get_Code_m2174 ();
extern "C" void CipherSuite_get_Name_m2175 ();
extern "C" void CipherSuite_get_IsExportable_m2176 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m2177 ();
extern "C" void CipherSuite_get_KeyBlockSize_m2178 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m2179 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m2180 ();
extern "C" void CipherSuite_get_IvSize_m2181 ();
extern "C" void CipherSuite_get_Context_m2182 ();
extern "C" void CipherSuite_set_Context_m2183 ();
extern "C" void CipherSuite_Write_m2184 ();
extern "C" void CipherSuite_Write_m2185 ();
extern "C" void CipherSuite_InitializeCipher_m2186 ();
extern "C" void CipherSuite_EncryptRecord_m2187 ();
extern "C" void CipherSuite_DecryptRecord_m2188 ();
extern "C" void CipherSuite_CreatePremasterSecret_m2189 ();
extern "C" void CipherSuite_PRF_m2190 ();
extern "C" void CipherSuite_Expand_m2191 ();
extern "C" void CipherSuite_createEncryptionCipher_m2192 ();
extern "C" void CipherSuite_createDecryptionCipher_m2193 ();
extern "C" void CipherSuiteCollection__ctor_m2194 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m2195 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m2196 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m2197 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m2198 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m2199 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m2200 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m2201 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m2202 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m2203 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m2204 ();
extern "C" void CipherSuiteCollection_get_Item_m2205 ();
extern "C" void CipherSuiteCollection_get_Item_m2206 ();
extern "C" void CipherSuiteCollection_set_Item_m2207 ();
extern "C" void CipherSuiteCollection_get_Item_m2208 ();
extern "C" void CipherSuiteCollection_get_Count_m2209 ();
extern "C" void CipherSuiteCollection_CopyTo_m2210 ();
extern "C" void CipherSuiteCollection_Clear_m2211 ();
extern "C" void CipherSuiteCollection_IndexOf_m2212 ();
extern "C" void CipherSuiteCollection_IndexOf_m2213 ();
extern "C" void CipherSuiteCollection_Add_m2214 ();
extern "C" void CipherSuiteCollection_add_m2215 ();
extern "C" void CipherSuiteCollection_add_m2216 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m2217 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m2218 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m2219 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m2220 ();
extern "C" void ClientContext__ctor_m2221 ();
extern "C" void ClientContext_get_SslStream_m2222 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m2223 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m2224 ();
extern "C" void ClientContext_Clear_m2225 ();
extern "C" void ClientRecordProtocol__ctor_m2226 ();
extern "C" void ClientRecordProtocol_GetMessage_m2227 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m2228 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m2229 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m2230 ();
extern "C" void ClientSessionInfo__ctor_m2231 ();
extern "C" void ClientSessionInfo__cctor_m2232 ();
extern "C" void ClientSessionInfo_Finalize_m2233 ();
extern "C" void ClientSessionInfo_get_HostName_m2234 ();
extern "C" void ClientSessionInfo_get_Id_m2235 ();
extern "C" void ClientSessionInfo_get_Valid_m2236 ();
extern "C" void ClientSessionInfo_GetContext_m2237 ();
extern "C" void ClientSessionInfo_SetContext_m2238 ();
extern "C" void ClientSessionInfo_KeepAlive_m2239 ();
extern "C" void ClientSessionInfo_Dispose_m2240 ();
extern "C" void ClientSessionInfo_Dispose_m2241 ();
extern "C" void ClientSessionInfo_CheckDisposed_m2242 ();
extern "C" void ClientSessionCache__cctor_m2243 ();
extern "C" void ClientSessionCache_Add_m2244 ();
extern "C" void ClientSessionCache_FromHost_m2245 ();
extern "C" void ClientSessionCache_FromContext_m2246 ();
extern "C" void ClientSessionCache_SetContextInCache_m2247 ();
extern "C" void ClientSessionCache_SetContextFromCache_m2248 ();
extern "C" void Context__ctor_m2249 ();
extern "C" void Context_get_AbbreviatedHandshake_m2250 ();
extern "C" void Context_set_AbbreviatedHandshake_m2251 ();
extern "C" void Context_get_ProtocolNegotiated_m2252 ();
extern "C" void Context_set_ProtocolNegotiated_m2253 ();
extern "C" void Context_get_SecurityProtocol_m2254 ();
extern "C" void Context_set_SecurityProtocol_m2255 ();
extern "C" void Context_get_SecurityProtocolFlags_m2256 ();
extern "C" void Context_get_Protocol_m2257 ();
extern "C" void Context_get_SessionId_m2258 ();
extern "C" void Context_set_SessionId_m2259 ();
extern "C" void Context_get_CompressionMethod_m2260 ();
extern "C" void Context_set_CompressionMethod_m2261 ();
extern "C" void Context_get_ServerSettings_m2262 ();
extern "C" void Context_get_ClientSettings_m2263 ();
extern "C" void Context_get_LastHandshakeMsg_m2264 ();
extern "C" void Context_set_LastHandshakeMsg_m2265 ();
extern "C" void Context_get_HandshakeState_m2266 ();
extern "C" void Context_set_HandshakeState_m2267 ();
extern "C" void Context_get_ReceivedConnectionEnd_m2268 ();
extern "C" void Context_set_ReceivedConnectionEnd_m2269 ();
extern "C" void Context_get_SentConnectionEnd_m2270 ();
extern "C" void Context_set_SentConnectionEnd_m2271 ();
extern "C" void Context_get_SupportedCiphers_m2272 ();
extern "C" void Context_set_SupportedCiphers_m2273 ();
extern "C" void Context_get_HandshakeMessages_m2274 ();
extern "C" void Context_get_WriteSequenceNumber_m2275 ();
extern "C" void Context_set_WriteSequenceNumber_m2276 ();
extern "C" void Context_get_ReadSequenceNumber_m2277 ();
extern "C" void Context_set_ReadSequenceNumber_m2278 ();
extern "C" void Context_get_ClientRandom_m2279 ();
extern "C" void Context_set_ClientRandom_m2280 ();
extern "C" void Context_get_ServerRandom_m2281 ();
extern "C" void Context_set_ServerRandom_m2282 ();
extern "C" void Context_get_RandomCS_m2283 ();
extern "C" void Context_set_RandomCS_m2284 ();
extern "C" void Context_get_RandomSC_m2285 ();
extern "C" void Context_set_RandomSC_m2286 ();
extern "C" void Context_get_MasterSecret_m2287 ();
extern "C" void Context_set_MasterSecret_m2288 ();
extern "C" void Context_get_ClientWriteKey_m2289 ();
extern "C" void Context_set_ClientWriteKey_m2290 ();
extern "C" void Context_get_ServerWriteKey_m2291 ();
extern "C" void Context_set_ServerWriteKey_m2292 ();
extern "C" void Context_get_ClientWriteIV_m2293 ();
extern "C" void Context_set_ClientWriteIV_m2294 ();
extern "C" void Context_get_ServerWriteIV_m2295 ();
extern "C" void Context_set_ServerWriteIV_m2296 ();
extern "C" void Context_get_RecordProtocol_m2297 ();
extern "C" void Context_set_RecordProtocol_m2298 ();
extern "C" void Context_GetUnixTime_m2299 ();
extern "C" void Context_GetSecureRandomBytes_m2300 ();
extern "C" void Context_Clear_m2301 ();
extern "C" void Context_ClearKeyInfo_m2302 ();
extern "C" void Context_DecodeProtocolCode_m2303 ();
extern "C" void Context_ChangeProtocol_m2304 ();
extern "C" void Context_get_Current_m2305 ();
extern "C" void Context_get_Negotiating_m2306 ();
extern "C" void Context_get_Read_m2307 ();
extern "C" void Context_get_Write_m2308 ();
extern "C" void Context_StartSwitchingSecurityParameters_m2309 ();
extern "C" void Context_EndSwitchingSecurityParameters_m2310 ();
extern "C" void HttpsClientStream__ctor_m2311 ();
extern "C" void HttpsClientStream_get_TrustFailure_m2312 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m2313 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m2314 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m2315 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m2316 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m2317 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m2318 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m2319 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m2320 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m2321 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m2322 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2323 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m2324 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2325 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2326 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2327 ();
extern "C" void SendRecordAsyncResult__ctor_m2328 ();
extern "C" void SendRecordAsyncResult_get_Message_m2329 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m2330 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m2331 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m2332 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m2333 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m2334 ();
extern "C" void SendRecordAsyncResult_SetComplete_m2335 ();
extern "C" void SendRecordAsyncResult_SetComplete_m2336 ();
extern "C" void RecordProtocol__ctor_m2337 ();
extern "C" void RecordProtocol__cctor_m2338 ();
extern "C" void RecordProtocol_get_Context_m2339 ();
extern "C" void RecordProtocol_SendRecord_m2340 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m2341 ();
extern "C" void RecordProtocol_GetMessage_m2342 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m2343 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m2344 ();
extern "C" void RecordProtocol_EndReceiveRecord_m2345 ();
extern "C" void RecordProtocol_ReceiveRecord_m2346 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m2347 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m2348 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m2349 ();
extern "C" void RecordProtocol_ProcessAlert_m2350 ();
extern "C" void RecordProtocol_SendAlert_m2351 ();
extern "C" void RecordProtocol_SendAlert_m2352 ();
extern "C" void RecordProtocol_SendAlert_m2353 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m2354 ();
extern "C" void RecordProtocol_BeginSendRecord_m2355 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m2356 ();
extern "C" void RecordProtocol_BeginSendRecord_m2357 ();
extern "C" void RecordProtocol_EndSendRecord_m2358 ();
extern "C" void RecordProtocol_SendRecord_m2359 ();
extern "C" void RecordProtocol_EncodeRecord_m2360 ();
extern "C" void RecordProtocol_EncodeRecord_m2361 ();
extern "C" void RecordProtocol_encryptRecordFragment_m2362 ();
extern "C" void RecordProtocol_decryptRecordFragment_m2363 ();
extern "C" void RecordProtocol_Compare_m2364 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m2365 ();
extern "C" void RecordProtocol_MapV2CipherCode_m2366 ();
extern "C" void RSASslSignatureDeformatter__ctor_m2367 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m2368 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m2369 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m2370 ();
extern "C" void RSASslSignatureFormatter__ctor_m2371 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m2372 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m2373 ();
extern "C" void RSASslSignatureFormatter_SetKey_m2374 ();
extern "C" void SecurityParameters__ctor_m2375 ();
extern "C" void SecurityParameters_get_Cipher_m2376 ();
extern "C" void SecurityParameters_set_Cipher_m2377 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m2378 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m2379 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m2380 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m2381 ();
extern "C" void SecurityParameters_Clear_m2382 ();
extern "C" void ValidationResult_get_Trusted_m2383 ();
extern "C" void ValidationResult_get_ErrorCode_m2384 ();
extern "C" void SslClientStream__ctor_m2385 ();
extern "C" void SslClientStream__ctor_m2386 ();
extern "C" void SslClientStream__ctor_m2387 ();
extern "C" void SslClientStream__ctor_m2388 ();
extern "C" void SslClientStream__ctor_m2389 ();
extern "C" void SslClientStream_add_ServerCertValidation_m2390 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m2391 ();
extern "C" void SslClientStream_add_ClientCertSelection_m2392 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m2393 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m2394 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m2395 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m2396 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m2397 ();
extern "C" void SslClientStream_get_InputBuffer_m2398 ();
extern "C" void SslClientStream_get_ClientCertificates_m2399 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m2400 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m2401 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m2402 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m2403 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m2404 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m2405 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m2406 ();
extern "C" void SslClientStream_Finalize_m2407 ();
extern "C" void SslClientStream_Dispose_m2408 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m2409 ();
extern "C" void SslClientStream_SafeReceiveRecord_m2410 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m2411 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m2412 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m2413 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m2414 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m2415 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m2416 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m2417 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m2418 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m2419 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m2420 ();
extern "C" void SslCipherSuite__ctor_m2421 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m2422 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m2423 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m2424 ();
extern "C" void SslCipherSuite_ComputeKeys_m2425 ();
extern "C" void SslCipherSuite_prf_m2426 ();
extern "C" void SslHandshakeHash__ctor_m2427 ();
extern "C" void SslHandshakeHash_Initialize_m2428 ();
extern "C" void SslHandshakeHash_HashFinal_m2429 ();
extern "C" void SslHandshakeHash_HashCore_m2430 ();
extern "C" void SslHandshakeHash_CreateSignature_m2431 ();
extern "C" void SslHandshakeHash_initializePad_m2432 ();
extern "C" void InternalAsyncResult__ctor_m2433 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m2434 ();
extern "C" void InternalAsyncResult_get_FromWrite_m2435 ();
extern "C" void InternalAsyncResult_get_Buffer_m2436 ();
extern "C" void InternalAsyncResult_get_Offset_m2437 ();
extern "C" void InternalAsyncResult_get_Count_m2438 ();
extern "C" void InternalAsyncResult_get_BytesRead_m2439 ();
extern "C" void InternalAsyncResult_get_AsyncState_m2440 ();
extern "C" void InternalAsyncResult_get_AsyncException_m2441 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m2442 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m2443 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m2444 ();
extern "C" void InternalAsyncResult_SetComplete_m2445 ();
extern "C" void InternalAsyncResult_SetComplete_m2446 ();
extern "C" void InternalAsyncResult_SetComplete_m2447 ();
extern "C" void InternalAsyncResult_SetComplete_m2448 ();
extern "C" void SslStreamBase__ctor_m2449 ();
extern "C" void SslStreamBase__cctor_m2450 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m2451 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m2452 ();
extern "C" void SslStreamBase_NegotiateHandshake_m2453 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m2454 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m2455 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m2456 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m2457 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m2458 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m2459 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m2460 ();
extern "C" void SslStreamBase_get_CipherStrength_m2461 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m2462 ();
extern "C" void SslStreamBase_get_HashStrength_m2463 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m2464 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m2465 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m2466 ();
extern "C" void SslStreamBase_get_ServerCertificate_m2467 ();
extern "C" void SslStreamBase_get_ServerCertificates_m2468 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m2469 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m2470 ();
extern "C" void SslStreamBase_BeginRead_m2471 ();
extern "C" void SslStreamBase_InternalBeginRead_m2472 ();
extern "C" void SslStreamBase_InternalReadCallback_m2473 ();
extern "C" void SslStreamBase_InternalBeginWrite_m2474 ();
extern "C" void SslStreamBase_InternalWriteCallback_m2475 ();
extern "C" void SslStreamBase_BeginWrite_m2476 ();
extern "C" void SslStreamBase_EndRead_m2477 ();
extern "C" void SslStreamBase_EndWrite_m2478 ();
extern "C" void SslStreamBase_Close_m2479 ();
extern "C" void SslStreamBase_Flush_m2480 ();
extern "C" void SslStreamBase_Read_m2481 ();
extern "C" void SslStreamBase_Read_m2482 ();
extern "C" void SslStreamBase_Seek_m2483 ();
extern "C" void SslStreamBase_SetLength_m2484 ();
extern "C" void SslStreamBase_Write_m2485 ();
extern "C" void SslStreamBase_Write_m2486 ();
extern "C" void SslStreamBase_get_CanRead_m2487 ();
extern "C" void SslStreamBase_get_CanSeek_m2488 ();
extern "C" void SslStreamBase_get_CanWrite_m2489 ();
extern "C" void SslStreamBase_get_Length_m2490 ();
extern "C" void SslStreamBase_get_Position_m2491 ();
extern "C" void SslStreamBase_set_Position_m2492 ();
extern "C" void SslStreamBase_Finalize_m2493 ();
extern "C" void SslStreamBase_Dispose_m2494 ();
extern "C" void SslStreamBase_resetBuffer_m2495 ();
extern "C" void SslStreamBase_checkDisposed_m2496 ();
extern "C" void TlsCipherSuite__ctor_m2497 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m2498 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m2499 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m2500 ();
extern "C" void TlsCipherSuite_ComputeKeys_m2501 ();
extern "C" void TlsClientSettings__ctor_m2502 ();
extern "C" void TlsClientSettings_get_TargetHost_m2503 ();
extern "C" void TlsClientSettings_set_TargetHost_m2504 ();
extern "C" void TlsClientSettings_get_Certificates_m2505 ();
extern "C" void TlsClientSettings_set_Certificates_m2506 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m2507 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m2508 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m2509 ();
extern "C" void TlsException__ctor_m2510 ();
extern "C" void TlsException__ctor_m2511 ();
extern "C" void TlsException__ctor_m2512 ();
extern "C" void TlsException__ctor_m2513 ();
extern "C" void TlsException__ctor_m2514 ();
extern "C" void TlsException__ctor_m2515 ();
extern "C" void TlsException_get_Alert_m2516 ();
extern "C" void TlsServerSettings__ctor_m2517 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m2518 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m2519 ();
extern "C" void TlsServerSettings_get_Certificates_m2520 ();
extern "C" void TlsServerSettings_set_Certificates_m2521 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m2522 ();
extern "C" void TlsServerSettings_get_RsaParameters_m2523 ();
extern "C" void TlsServerSettings_set_RsaParameters_m2524 ();
extern "C" void TlsServerSettings_set_SignedParams_m2525 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m2526 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m2527 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m2528 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m2529 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m2530 ();
extern "C" void TlsStream__ctor_m2531 ();
extern "C" void TlsStream__ctor_m2532 ();
extern "C" void TlsStream_get_EOF_m2533 ();
extern "C" void TlsStream_get_CanWrite_m2534 ();
extern "C" void TlsStream_get_CanRead_m2535 ();
extern "C" void TlsStream_get_CanSeek_m2536 ();
extern "C" void TlsStream_get_Position_m2537 ();
extern "C" void TlsStream_set_Position_m2538 ();
extern "C" void TlsStream_get_Length_m2539 ();
extern "C" void TlsStream_ReadSmallValue_m2540 ();
extern "C" void TlsStream_ReadByte_m2541 ();
extern "C" void TlsStream_ReadInt16_m2542 ();
extern "C" void TlsStream_ReadInt24_m2543 ();
extern "C" void TlsStream_ReadBytes_m2544 ();
extern "C" void TlsStream_Write_m2545 ();
extern "C" void TlsStream_Write_m2546 ();
extern "C" void TlsStream_WriteInt24_m2547 ();
extern "C" void TlsStream_Write_m2548 ();
extern "C" void TlsStream_Write_m2549 ();
extern "C" void TlsStream_Reset_m2550 ();
extern "C" void TlsStream_ToArray_m2551 ();
extern "C" void TlsStream_Flush_m2552 ();
extern "C" void TlsStream_SetLength_m2553 ();
extern "C" void TlsStream_Seek_m2554 ();
extern "C" void TlsStream_Read_m2555 ();
extern "C" void TlsStream_Write_m2556 ();
extern "C" void HandshakeMessage__ctor_m2557 ();
extern "C" void HandshakeMessage__ctor_m2558 ();
extern "C" void HandshakeMessage__ctor_m2559 ();
extern "C" void HandshakeMessage_get_Context_m2560 ();
extern "C" void HandshakeMessage_get_HandshakeType_m2561 ();
extern "C" void HandshakeMessage_get_ContentType_m2562 ();
extern "C" void HandshakeMessage_Process_m2563 ();
extern "C" void HandshakeMessage_Update_m2564 ();
extern "C" void HandshakeMessage_EncodeMessage_m2565 ();
extern "C" void HandshakeMessage_Compare_m2566 ();
extern "C" void TlsClientCertificate__ctor_m2567 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m2568 ();
extern "C" void TlsClientCertificate_Update_m2569 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m2570 ();
extern "C" void TlsClientCertificate_SendCertificates_m2571 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m2572 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m2573 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m2574 ();
extern "C" void TlsClientCertificateVerify__ctor_m2575 ();
extern "C" void TlsClientCertificateVerify_Update_m2576 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m2577 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m2578 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m2579 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m2580 ();
extern "C" void TlsClientFinished__ctor_m2581 ();
extern "C" void TlsClientFinished__cctor_m2582 ();
extern "C" void TlsClientFinished_Update_m2583 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m2584 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m2585 ();
extern "C" void TlsClientHello__ctor_m2586 ();
extern "C" void TlsClientHello_Update_m2587 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m2588 ();
extern "C" void TlsClientHello_ProcessAsTls1_m2589 ();
extern "C" void TlsClientKeyExchange__ctor_m2590 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m2591 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m2592 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m2593 ();
extern "C" void TlsServerCertificate__ctor_m2594 ();
extern "C" void TlsServerCertificate_Update_m2595 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m2596 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m2597 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m2598 ();
extern "C" void TlsServerCertificate_validateCertificates_m2599 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m2600 ();
extern "C" void TlsServerCertificate_checkDomainName_m2601 ();
extern "C" void TlsServerCertificate_Match_m2602 ();
extern "C" void TlsServerCertificateRequest__ctor_m2603 ();
extern "C" void TlsServerCertificateRequest_Update_m2604 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m2605 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m2606 ();
extern "C" void TlsServerFinished__ctor_m2607 ();
extern "C" void TlsServerFinished__cctor_m2608 ();
extern "C" void TlsServerFinished_Update_m2609 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m2610 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m2611 ();
extern "C" void TlsServerHello__ctor_m2612 ();
extern "C" void TlsServerHello_Update_m2613 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m2614 ();
extern "C" void TlsServerHello_ProcessAsTls1_m2615 ();
extern "C" void TlsServerHello_processProtocol_m2616 ();
extern "C" void TlsServerHelloDone__ctor_m2617 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m2618 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m2619 ();
extern "C" void TlsServerKeyExchange__ctor_m2620 ();
extern "C" void TlsServerKeyExchange_Update_m2621 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m2622 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m2623 ();
extern "C" void TlsServerKeyExchange_verifySignature_m2624 ();
extern "C" void PrimalityTest__ctor_m2625 ();
extern "C" void PrimalityTest_Invoke_m2626 ();
extern "C" void PrimalityTest_BeginInvoke_m2627 ();
extern "C" void PrimalityTest_EndInvoke_m2628 ();
extern "C" void CertificateValidationCallback__ctor_m2629 ();
extern "C" void CertificateValidationCallback_Invoke_m2630 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m2631 ();
extern "C" void CertificateValidationCallback_EndInvoke_m2632 ();
extern "C" void CertificateValidationCallback2__ctor_m2633 ();
extern "C" void CertificateValidationCallback2_Invoke_m2634 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m2635 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m2636 ();
extern "C" void CertificateSelectionCallback__ctor_m2637 ();
extern "C" void CertificateSelectionCallback_Invoke_m2638 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m2639 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m2640 ();
extern "C" void PrivateKeySelectionCallback__ctor_m2641 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m2642 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m2643 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m2644 ();
extern "C" void Object__ctor_m589 ();
extern "C" void Object_Equals_m2723 ();
extern "C" void Object_Equals_m1747 ();
extern "C" void Object_Finalize_m588 ();
extern "C" void Object_GetHashCode_m2724 ();
extern "C" void Object_GetType_m625 ();
extern "C" void Object_MemberwiseClone_m2725 ();
extern "C" void Object_ToString_m661 ();
extern "C" void Object_ReferenceEquals_m2726 ();
extern "C" void Object_InternalGetHashCode_m2727 ();
extern "C" void ValueType__ctor_m2728 ();
extern "C" void ValueType_InternalEquals_m2729 ();
extern "C" void ValueType_DefaultEquals_m2730 ();
extern "C" void ValueType_Equals_m2731 ();
extern "C" void ValueType_InternalGetHashCode_m2732 ();
extern "C" void ValueType_GetHashCode_m2733 ();
extern "C" void ValueType_ToString_m2734 ();
extern "C" void Attribute__ctor_m613 ();
extern "C" void Attribute_CheckParameters_m2735 ();
extern "C" void Attribute_GetCustomAttribute_m2736 ();
extern "C" void Attribute_GetCustomAttribute_m2737 ();
extern "C" void Attribute_GetHashCode_m662 ();
extern "C" void Attribute_IsDefined_m2738 ();
extern "C" void Attribute_IsDefined_m2739 ();
extern "C" void Attribute_IsDefined_m2740 ();
extern "C" void Attribute_IsDefined_m2741 ();
extern "C" void Attribute_Equals_m2742 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m2743 ();
extern "C" void Int32_System_IConvertible_ToByte_m2744 ();
extern "C" void Int32_System_IConvertible_ToChar_m2745 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m2746 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m2747 ();
extern "C" void Int32_System_IConvertible_ToDouble_m2748 ();
extern "C" void Int32_System_IConvertible_ToInt16_m2749 ();
extern "C" void Int32_System_IConvertible_ToInt32_m2750 ();
extern "C" void Int32_System_IConvertible_ToInt64_m2751 ();
extern "C" void Int32_System_IConvertible_ToSByte_m2752 ();
extern "C" void Int32_System_IConvertible_ToSingle_m2753 ();
extern "C" void Int32_System_IConvertible_ToType_m2754 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m2755 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m2756 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m2757 ();
extern "C" void Int32_CompareTo_m2758 ();
extern "C" void Int32_Equals_m2759 ();
extern "C" void Int32_GetHashCode_m605 ();
extern "C" void Int32_CompareTo_m2760 ();
extern "C" void Int32_Equals_m607 ();
extern "C" void Int32_ProcessTrailingWhitespace_m2761 ();
extern "C" void Int32_Parse_m2762 ();
extern "C" void Int32_Parse_m2763 ();
extern "C" void Int32_CheckStyle_m2764 ();
extern "C" void Int32_JumpOverWhite_m2765 ();
extern "C" void Int32_FindSign_m2766 ();
extern "C" void Int32_FindCurrency_m2767 ();
extern "C" void Int32_FindExponent_m2768 ();
extern "C" void Int32_FindOther_m2769 ();
extern "C" void Int32_ValidDigit_m2770 ();
extern "C" void Int32_GetFormatException_m2771 ();
extern "C" void Int32_Parse_m2772 ();
extern "C" void Int32_Parse_m1719 ();
extern "C" void Int32_Parse_m2773 ();
extern "C" void Int32_TryParse_m2774 ();
extern "C" void Int32_TryParse_m1599 ();
extern "C" void Int32_ToString_m652 ();
extern "C" void Int32_ToString_m2688 ();
extern "C" void Int32_ToString_m1713 ();
extern "C" void Int32_ToString_m2691 ();
extern "C" void SerializableAttribute__ctor_m2775 ();
extern "C" void AttributeUsageAttribute__ctor_m2776 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m2777 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m2778 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m2779 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m2780 ();
extern "C" void ComVisibleAttribute__ctor_m2781 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m2782 ();
extern "C" void Int64_System_IConvertible_ToByte_m2783 ();
extern "C" void Int64_System_IConvertible_ToChar_m2784 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m2785 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m2786 ();
extern "C" void Int64_System_IConvertible_ToDouble_m2787 ();
extern "C" void Int64_System_IConvertible_ToInt16_m2788 ();
extern "C" void Int64_System_IConvertible_ToInt32_m2789 ();
extern "C" void Int64_System_IConvertible_ToInt64_m2790 ();
extern "C" void Int64_System_IConvertible_ToSByte_m2791 ();
extern "C" void Int64_System_IConvertible_ToSingle_m2792 ();
extern "C" void Int64_System_IConvertible_ToType_m2793 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m2794 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m2795 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m2796 ();
extern "C" void Int64_CompareTo_m2797 ();
extern "C" void Int64_Equals_m2798 ();
extern "C" void Int64_GetHashCode_m2799 ();
extern "C" void Int64_CompareTo_m2800 ();
extern "C" void Int64_Equals_m2801 ();
extern "C" void Int64_Parse_m2802 ();
extern "C" void Int64_Parse_m2803 ();
extern "C" void Int64_Parse_m2804 ();
extern "C" void Int64_Parse_m2805 ();
extern "C" void Int64_Parse_m2806 ();
extern "C" void Int64_TryParse_m2807 ();
extern "C" void Int64_TryParse_m1595 ();
extern "C" void Int64_ToString_m1596 ();
extern "C" void Int64_ToString_m2808 ();
extern "C" void Int64_ToString_m2809 ();
extern "C" void Int64_ToString_m2810 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m2811 ();
extern "C" void UInt32_System_IConvertible_ToByte_m2812 ();
extern "C" void UInt32_System_IConvertible_ToChar_m2813 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m2814 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m2815 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m2816 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m2817 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m2818 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m2819 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m2820 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m2821 ();
extern "C" void UInt32_System_IConvertible_ToType_m2822 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m2823 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m2824 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m2825 ();
extern "C" void UInt32_CompareTo_m2826 ();
extern "C" void UInt32_Equals_m2827 ();
extern "C" void UInt32_GetHashCode_m2828 ();
extern "C" void UInt32_CompareTo_m2829 ();
extern "C" void UInt32_Equals_m2830 ();
extern "C" void UInt32_Parse_m2831 ();
extern "C" void UInt32_Parse_m2832 ();
extern "C" void UInt32_Parse_m2833 ();
extern "C" void UInt32_Parse_m2834 ();
extern "C" void UInt32_TryParse_m1740 ();
extern "C" void UInt32_TryParse_m2835 ();
extern "C" void UInt32_ToString_m2836 ();
extern "C" void UInt32_ToString_m2837 ();
extern "C" void UInt32_ToString_m2838 ();
extern "C" void UInt32_ToString_m2839 ();
extern "C" void CLSCompliantAttribute__ctor_m2840 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m2841 ();
extern "C" void UInt64_System_IConvertible_ToByte_m2842 ();
extern "C" void UInt64_System_IConvertible_ToChar_m2843 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m2844 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m2845 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m2846 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m2847 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m2848 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m2849 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m2850 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m2851 ();
extern "C" void UInt64_System_IConvertible_ToType_m2852 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m2853 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m2854 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m2855 ();
extern "C" void UInt64_CompareTo_m2856 ();
extern "C" void UInt64_Equals_m2857 ();
extern "C" void UInt64_GetHashCode_m2858 ();
extern "C" void UInt64_CompareTo_m2859 ();
extern "C" void UInt64_Equals_m2860 ();
extern "C" void UInt64_Parse_m2861 ();
extern "C" void UInt64_Parse_m2862 ();
extern "C" void UInt64_Parse_m2863 ();
extern "C" void UInt64_TryParse_m2864 ();
extern "C" void UInt64_ToString_m2865 ();
extern "C" void UInt64_ToString_m2650 ();
extern "C" void UInt64_ToString_m2866 ();
extern "C" void UInt64_ToString_m2867 ();
extern "C" void Byte_System_IConvertible_ToType_m2868 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m2869 ();
extern "C" void Byte_System_IConvertible_ToByte_m2870 ();
extern "C" void Byte_System_IConvertible_ToChar_m2871 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m2872 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m2873 ();
extern "C" void Byte_System_IConvertible_ToDouble_m2874 ();
extern "C" void Byte_System_IConvertible_ToInt16_m2875 ();
extern "C" void Byte_System_IConvertible_ToInt32_m2876 ();
extern "C" void Byte_System_IConvertible_ToInt64_m2877 ();
extern "C" void Byte_System_IConvertible_ToSByte_m2878 ();
extern "C" void Byte_System_IConvertible_ToSingle_m2879 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m2880 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m2881 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m2882 ();
extern "C" void Byte_CompareTo_m2883 ();
extern "C" void Byte_Equals_m2884 ();
extern "C" void Byte_GetHashCode_m2885 ();
extern "C" void Byte_CompareTo_m2886 ();
extern "C" void Byte_Equals_m2887 ();
extern "C" void Byte_Parse_m2888 ();
extern "C" void Byte_Parse_m2889 ();
extern "C" void Byte_Parse_m2890 ();
extern "C" void Byte_TryParse_m2891 ();
extern "C" void Byte_TryParse_m2892 ();
extern "C" void Byte_ToString_m2689 ();
extern "C" void Byte_ToString_m1652 ();
extern "C" void Byte_ToString_m2649 ();
extern "C" void Byte_ToString_m2654 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m2893 ();
extern "C" void SByte_System_IConvertible_ToByte_m2894 ();
extern "C" void SByte_System_IConvertible_ToChar_m2895 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m2896 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m2897 ();
extern "C" void SByte_System_IConvertible_ToDouble_m2898 ();
extern "C" void SByte_System_IConvertible_ToInt16_m2899 ();
extern "C" void SByte_System_IConvertible_ToInt32_m2900 ();
extern "C" void SByte_System_IConvertible_ToInt64_m2901 ();
extern "C" void SByte_System_IConvertible_ToSByte_m2902 ();
extern "C" void SByte_System_IConvertible_ToSingle_m2903 ();
extern "C" void SByte_System_IConvertible_ToType_m2904 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m2905 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m2906 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m2907 ();
extern "C" void SByte_CompareTo_m2908 ();
extern "C" void SByte_Equals_m2909 ();
extern "C" void SByte_GetHashCode_m2910 ();
extern "C" void SByte_CompareTo_m2911 ();
extern "C" void SByte_Equals_m2912 ();
extern "C" void SByte_Parse_m2913 ();
extern "C" void SByte_Parse_m2914 ();
extern "C" void SByte_Parse_m2915 ();
extern "C" void SByte_TryParse_m2916 ();
extern "C" void SByte_ToString_m2917 ();
extern "C" void SByte_ToString_m2918 ();
extern "C" void SByte_ToString_m2919 ();
extern "C" void SByte_ToString_m2920 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m2921 ();
extern "C" void Int16_System_IConvertible_ToByte_m2922 ();
extern "C" void Int16_System_IConvertible_ToChar_m2923 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m2924 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m2925 ();
extern "C" void Int16_System_IConvertible_ToDouble_m2926 ();
extern "C" void Int16_System_IConvertible_ToInt16_m2927 ();
extern "C" void Int16_System_IConvertible_ToInt32_m2928 ();
extern "C" void Int16_System_IConvertible_ToInt64_m2929 ();
extern "C" void Int16_System_IConvertible_ToSByte_m2930 ();
extern "C" void Int16_System_IConvertible_ToSingle_m2931 ();
extern "C" void Int16_System_IConvertible_ToType_m2932 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m2933 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m2934 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m2935 ();
extern "C" void Int16_CompareTo_m2936 ();
extern "C" void Int16_Equals_m2937 ();
extern "C" void Int16_GetHashCode_m2938 ();
extern "C" void Int16_CompareTo_m2939 ();
extern "C" void Int16_Equals_m2940 ();
extern "C" void Int16_Parse_m2941 ();
extern "C" void Int16_Parse_m2942 ();
extern "C" void Int16_Parse_m2943 ();
extern "C" void Int16_TryParse_m2944 ();
extern "C" void Int16_ToString_m2945 ();
extern "C" void Int16_ToString_m2946 ();
extern "C" void Int16_ToString_m2947 ();
extern "C" void Int16_ToString_m2948 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m2949 ();
extern "C" void UInt16_System_IConvertible_ToByte_m2950 ();
extern "C" void UInt16_System_IConvertible_ToChar_m2951 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m2952 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m2953 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m2954 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m2955 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m2956 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m2957 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m2958 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m2959 ();
extern "C" void UInt16_System_IConvertible_ToType_m2960 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m2961 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m2962 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m2963 ();
extern "C" void UInt16_CompareTo_m2964 ();
extern "C" void UInt16_Equals_m2965 ();
extern "C" void UInt16_GetHashCode_m2966 ();
extern "C" void UInt16_CompareTo_m2967 ();
extern "C" void UInt16_Equals_m2968 ();
extern "C" void UInt16_Parse_m2969 ();
extern "C" void UInt16_Parse_m2970 ();
extern "C" void UInt16_TryParse_m2971 ();
extern "C" void UInt16_TryParse_m2972 ();
extern "C" void UInt16_ToString_m2973 ();
extern "C" void UInt16_ToString_m2974 ();
extern "C" void UInt16_ToString_m2975 ();
extern "C" void UInt16_ToString_m2976 ();
extern "C" void Char__cctor_m2977 ();
extern "C" void Char_System_IConvertible_ToType_m2978 ();
extern "C" void Char_System_IConvertible_ToBoolean_m2979 ();
extern "C" void Char_System_IConvertible_ToByte_m2980 ();
extern "C" void Char_System_IConvertible_ToChar_m2981 ();
extern "C" void Char_System_IConvertible_ToDateTime_m2982 ();
extern "C" void Char_System_IConvertible_ToDecimal_m2983 ();
extern "C" void Char_System_IConvertible_ToDouble_m2984 ();
extern "C" void Char_System_IConvertible_ToInt16_m2985 ();
extern "C" void Char_System_IConvertible_ToInt32_m2986 ();
extern "C" void Char_System_IConvertible_ToInt64_m2987 ();
extern "C" void Char_System_IConvertible_ToSByte_m2988 ();
extern "C" void Char_System_IConvertible_ToSingle_m2989 ();
extern "C" void Char_System_IConvertible_ToUInt16_m2990 ();
extern "C" void Char_System_IConvertible_ToUInt32_m2991 ();
extern "C" void Char_System_IConvertible_ToUInt64_m2992 ();
extern "C" void Char_GetDataTablePointers_m2993 ();
extern "C" void Char_CompareTo_m2994 ();
extern "C" void Char_Equals_m2995 ();
extern "C" void Char_CompareTo_m2996 ();
extern "C" void Char_Equals_m2997 ();
extern "C" void Char_GetHashCode_m2998 ();
extern "C" void Char_GetUnicodeCategory_m1726 ();
extern "C" void Char_IsDigit_m1724 ();
extern "C" void Char_IsLetter_m2999 ();
extern "C" void Char_IsLetterOrDigit_m1723 ();
extern "C" void Char_IsLower_m3000 ();
extern "C" void Char_IsSurrogate_m3001 ();
extern "C" void Char_IsWhiteSpace_m1725 ();
extern "C" void Char_IsWhiteSpace_m1638 ();
extern "C" void Char_CheckParameter_m3002 ();
extern "C" void Char_Parse_m3003 ();
extern "C" void Char_ToLower_m1727 ();
extern "C" void Char_ToLowerInvariant_m3004 ();
extern "C" void Char_ToLower_m3005 ();
extern "C" void Char_ToUpper_m1732 ();
extern "C" void Char_ToUpperInvariant_m1640 ();
extern "C" void Char_ToString_m1734 ();
extern "C" void Char_ToString_m3006 ();
extern "C" void String__ctor_m3007 ();
extern "C" void String__ctor_m3008 ();
extern "C" void String__ctor_m3009 ();
extern "C" void String__ctor_m3010 ();
extern "C" void String__cctor_m3011 ();
extern "C" void String_System_IConvertible_ToBoolean_m3012 ();
extern "C" void String_System_IConvertible_ToByte_m3013 ();
extern "C" void String_System_IConvertible_ToChar_m3014 ();
extern "C" void String_System_IConvertible_ToDateTime_m3015 ();
extern "C" void String_System_IConvertible_ToDecimal_m3016 ();
extern "C" void String_System_IConvertible_ToDouble_m3017 ();
extern "C" void String_System_IConvertible_ToInt16_m3018 ();
extern "C" void String_System_IConvertible_ToInt32_m3019 ();
extern "C" void String_System_IConvertible_ToInt64_m3020 ();
extern "C" void String_System_IConvertible_ToSByte_m3021 ();
extern "C" void String_System_IConvertible_ToSingle_m3022 ();
extern "C" void String_System_IConvertible_ToType_m3023 ();
extern "C" void String_System_IConvertible_ToUInt16_m3024 ();
extern "C" void String_System_IConvertible_ToUInt32_m3025 ();
extern "C" void String_System_IConvertible_ToUInt64_m3026 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m3027 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m3028 ();
extern "C" void String_Equals_m3029 ();
extern "C" void String_Equals_m3030 ();
extern "C" void String_Equals_m1701 ();
extern "C" void String_get_Chars_m642 ();
extern "C" void String_Clone_m3031 ();
extern "C" void String_CopyTo_m3032 ();
extern "C" void String_ToCharArray_m1594 ();
extern "C" void String_ToCharArray_m3033 ();
extern "C" void String_Split_m641 ();
extern "C" void String_Split_m3034 ();
extern "C" void String_Split_m3035 ();
extern "C" void String_Split_m3036 ();
extern "C" void String_Split_m1641 ();
extern "C" void String_Substring_m1593 ();
extern "C" void String_Substring_m644 ();
extern "C" void String_SubstringUnchecked_m3037 ();
extern "C" void String_Trim_m637 ();
extern "C" void String_Trim_m3038 ();
extern "C" void String_TrimStart_m1742 ();
extern "C" void String_TrimEnd_m1639 ();
extern "C" void String_FindNotWhiteSpace_m3039 ();
extern "C" void String_FindNotInTable_m3040 ();
extern "C" void String_Compare_m3041 ();
extern "C" void String_Compare_m3042 ();
extern "C" void String_Compare_m1613 ();
extern "C" void String_Compare_m2722 ();
extern "C" void String_CompareTo_m3043 ();
extern "C" void String_CompareTo_m3044 ();
extern "C" void String_CompareOrdinal_m3045 ();
extern "C" void String_CompareOrdinalUnchecked_m3046 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m3047 ();
extern "C" void String_EndsWith_m645 ();
extern "C" void String_IndexOfAny_m3048 ();
extern "C" void String_IndexOfAny_m3049 ();
extern "C" void String_IndexOfAny_m2673 ();
extern "C" void String_IndexOfAnyUnchecked_m3050 ();
extern "C" void String_IndexOf_m1676 ();
extern "C" void String_IndexOf_m3051 ();
extern "C" void String_IndexOfOrdinal_m3052 ();
extern "C" void String_IndexOfOrdinalUnchecked_m3053 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m3054 ();
extern "C" void String_IndexOf_m1592 ();
extern "C" void String_IndexOf_m1743 ();
extern "C" void String_IndexOf_m1744 ();
extern "C" void String_IndexOfUnchecked_m3055 ();
extern "C" void String_IndexOf_m643 ();
extern "C" void String_IndexOf_m647 ();
extern "C" void String_IndexOf_m3056 ();
extern "C" void String_LastIndexOfAny_m3057 ();
extern "C" void String_LastIndexOfAnyUnchecked_m3058 ();
extern "C" void String_LastIndexOf_m1600 ();
extern "C" void String_LastIndexOf_m3059 ();
extern "C" void String_LastIndexOf_m1745 ();
extern "C" void String_LastIndexOfUnchecked_m3060 ();
extern "C" void String_LastIndexOf_m650 ();
extern "C" void String_LastIndexOf_m3061 ();
extern "C" void String_IsNullOrEmpty_m657 ();
extern "C" void String_PadRight_m3062 ();
extern "C" void String_StartsWith_m634 ();
extern "C" void String_Replace_m649 ();
extern "C" void String_Replace_m648 ();
extern "C" void String_ReplaceUnchecked_m3063 ();
extern "C" void String_ReplaceFallback_m3064 ();
extern "C" void String_Remove_m646 ();
extern "C" void String_ToLower_m1730 ();
extern "C" void String_ToLower_m1741 ();
extern "C" void String_ToLowerInvariant_m3065 ();
extern "C" void String_ToString_m633 ();
extern "C" void String_ToString_m3066 ();
extern "C" void String_Format_m1647 ();
extern "C" void String_Format_m3067 ();
extern "C" void String_Format_m3068 ();
extern "C" void String_Format_m614 ();
extern "C" void String_Format_m2700 ();
extern "C" void String_FormatHelper_m3069 ();
extern "C" void String_Concat_m1587 ();
extern "C" void String_Concat_m1577 ();
extern "C" void String_Concat_m594 ();
extern "C" void String_Concat_m635 ();
extern "C" void String_Concat_m638 ();
extern "C" void String_Concat_m628 ();
extern "C" void String_Concat_m1597 ();
extern "C" void String_ConcatInternal_m3070 ();
extern "C" void String_Insert_m651 ();
extern "C" void String_Join_m3071 ();
extern "C" void String_Join_m3072 ();
extern "C" void String_JoinUnchecked_m3073 ();
extern "C" void String_get_Length_m586 ();
extern "C" void String_ParseFormatSpecifier_m3074 ();
extern "C" void String_ParseDecimal_m3075 ();
extern "C" void String_InternalSetChar_m3076 ();
extern "C" void String_InternalSetLength_m3077 ();
extern "C" void String_GetHashCode_m1581 ();
extern "C" void String_GetCaseInsensitiveHashCode_m3078 ();
extern "C" void String_CreateString_m3079 ();
extern "C" void String_CreateString_m3080 ();
extern "C" void String_CreateString_m3081 ();
extern "C" void String_CreateString_m3082 ();
extern "C" void String_CreateString_m3083 ();
extern "C" void String_CreateString_m3084 ();
extern "C" void String_CreateString_m1733 ();
extern "C" void String_CreateString_m3085 ();
extern "C" void String_memcpy4_m3086 ();
extern "C" void String_memcpy2_m3087 ();
extern "C" void String_memcpy1_m3088 ();
extern "C" void String_memcpy_m3089 ();
extern "C" void String_CharCopy_m3090 ();
extern "C" void String_CharCopyReverse_m3091 ();
extern "C" void String_CharCopy_m3092 ();
extern "C" void String_CharCopy_m3093 ();
extern "C" void String_CharCopyReverse_m3094 ();
extern "C" void String_InternalSplit_m3095 ();
extern "C" void String_InternalAllocateStr_m3096 ();
extern "C" void String_op_Equality_m600 ();
extern "C" void String_op_Inequality_m1606 ();
extern "C" void Single_System_IConvertible_ToBoolean_m3097 ();
extern "C" void Single_System_IConvertible_ToByte_m3098 ();
extern "C" void Single_System_IConvertible_ToChar_m3099 ();
extern "C" void Single_System_IConvertible_ToDateTime_m3100 ();
extern "C" void Single_System_IConvertible_ToDecimal_m3101 ();
extern "C" void Single_System_IConvertible_ToDouble_m3102 ();
extern "C" void Single_System_IConvertible_ToInt16_m3103 ();
extern "C" void Single_System_IConvertible_ToInt32_m3104 ();
extern "C" void Single_System_IConvertible_ToInt64_m3105 ();
extern "C" void Single_System_IConvertible_ToSByte_m3106 ();
extern "C" void Single_System_IConvertible_ToSingle_m3107 ();
extern "C" void Single_System_IConvertible_ToType_m3108 ();
extern "C" void Single_System_IConvertible_ToUInt16_m3109 ();
extern "C" void Single_System_IConvertible_ToUInt32_m3110 ();
extern "C" void Single_System_IConvertible_ToUInt64_m3111 ();
extern "C" void Single_CompareTo_m3112 ();
extern "C" void Single_Equals_m3113 ();
extern "C" void Single_CompareTo_m3114 ();
extern "C" void Single_Equals_m610 ();
extern "C" void Single_GetHashCode_m606 ();
extern "C" void Single_IsInfinity_m3115 ();
extern "C" void Single_IsNaN_m3116 ();
extern "C" void Single_IsNegativeInfinity_m3117 ();
extern "C" void Single_IsPositiveInfinity_m3118 ();
extern "C" void Single_Parse_m3119 ();
extern "C" void Single_ToString_m3120 ();
extern "C" void Single_ToString_m3121 ();
extern "C" void Single_ToString_m611 ();
extern "C" void Single_ToString_m3122 ();
extern "C" void Double_System_IConvertible_ToType_m3123 ();
extern "C" void Double_System_IConvertible_ToBoolean_m3124 ();
extern "C" void Double_System_IConvertible_ToByte_m3125 ();
extern "C" void Double_System_IConvertible_ToChar_m3126 ();
extern "C" void Double_System_IConvertible_ToDateTime_m3127 ();
extern "C" void Double_System_IConvertible_ToDecimal_m3128 ();
extern "C" void Double_System_IConvertible_ToDouble_m3129 ();
extern "C" void Double_System_IConvertible_ToInt16_m3130 ();
extern "C" void Double_System_IConvertible_ToInt32_m3131 ();
extern "C" void Double_System_IConvertible_ToInt64_m3132 ();
extern "C" void Double_System_IConvertible_ToSByte_m3133 ();
extern "C" void Double_System_IConvertible_ToSingle_m3134 ();
extern "C" void Double_System_IConvertible_ToUInt16_m3135 ();
extern "C" void Double_System_IConvertible_ToUInt32_m3136 ();
extern "C" void Double_System_IConvertible_ToUInt64_m3137 ();
extern "C" void Double_CompareTo_m3138 ();
extern "C" void Double_Equals_m3139 ();
extern "C" void Double_CompareTo_m3140 ();
extern "C" void Double_Equals_m3141 ();
extern "C" void Double_GetHashCode_m3142 ();
extern "C" void Double_IsInfinity_m3143 ();
extern "C" void Double_IsNaN_m3144 ();
extern "C" void Double_IsNegativeInfinity_m3145 ();
extern "C" void Double_IsPositiveInfinity_m3146 ();
extern "C" void Double_Parse_m3147 ();
extern "C" void Double_Parse_m3148 ();
extern "C" void Double_Parse_m3149 ();
extern "C" void Double_Parse_m3150 ();
extern "C" void Double_TryParseStringConstant_m3151 ();
extern "C" void Double_ParseImpl_m3152 ();
extern "C" void Double_ToString_m3153 ();
extern "C" void Double_ToString_m3154 ();
extern "C" void Double_ToString_m3155 ();
extern "C" void Decimal__ctor_m3156 ();
extern "C" void Decimal__ctor_m3157 ();
extern "C" void Decimal__ctor_m3158 ();
extern "C" void Decimal__ctor_m3159 ();
extern "C" void Decimal__ctor_m3160 ();
extern "C" void Decimal__ctor_m3161 ();
extern "C" void Decimal__ctor_m3162 ();
extern "C" void Decimal__cctor_m3163 ();
extern "C" void Decimal_System_IConvertible_ToType_m3164 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m3165 ();
extern "C" void Decimal_System_IConvertible_ToByte_m3166 ();
extern "C" void Decimal_System_IConvertible_ToChar_m3167 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m3168 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m3169 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m3170 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m3171 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m3172 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m3173 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m3174 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m3175 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m3176 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m3177 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m3178 ();
extern "C" void Decimal_GetBits_m3179 ();
extern "C" void Decimal_Add_m3180 ();
extern "C" void Decimal_Subtract_m3181 ();
extern "C" void Decimal_GetHashCode_m3182 ();
extern "C" void Decimal_u64_m3183 ();
extern "C" void Decimal_s64_m3184 ();
extern "C" void Decimal_Equals_m3185 ();
extern "C" void Decimal_Equals_m3186 ();
extern "C" void Decimal_IsZero_m3187 ();
extern "C" void Decimal_Floor_m3188 ();
extern "C" void Decimal_Multiply_m3189 ();
extern "C" void Decimal_Divide_m3190 ();
extern "C" void Decimal_Compare_m3191 ();
extern "C" void Decimal_CompareTo_m3192 ();
extern "C" void Decimal_CompareTo_m3193 ();
extern "C" void Decimal_Equals_m3194 ();
extern "C" void Decimal_Parse_m3195 ();
extern "C" void Decimal_ThrowAtPos_m3196 ();
extern "C" void Decimal_ThrowInvalidExp_m3197 ();
extern "C" void Decimal_stripStyles_m3198 ();
extern "C" void Decimal_Parse_m3199 ();
extern "C" void Decimal_PerformParse_m3200 ();
extern "C" void Decimal_ToString_m3201 ();
extern "C" void Decimal_ToString_m3202 ();
extern "C" void Decimal_ToString_m3203 ();
extern "C" void Decimal_decimal2UInt64_m3204 ();
extern "C" void Decimal_decimal2Int64_m3205 ();
extern "C" void Decimal_decimalIncr_m3206 ();
extern "C" void Decimal_string2decimal_m3207 ();
extern "C" void Decimal_decimalSetExponent_m3208 ();
extern "C" void Decimal_decimal2double_m3209 ();
extern "C" void Decimal_decimalFloorAndTrunc_m3210 ();
extern "C" void Decimal_decimalMult_m3211 ();
extern "C" void Decimal_decimalDiv_m3212 ();
extern "C" void Decimal_decimalCompare_m3213 ();
extern "C" void Decimal_op_Increment_m3214 ();
extern "C" void Decimal_op_Subtraction_m3215 ();
extern "C" void Decimal_op_Multiply_m3216 ();
extern "C" void Decimal_op_Division_m3217 ();
extern "C" void Decimal_op_Explicit_m3218 ();
extern "C" void Decimal_op_Explicit_m3219 ();
extern "C" void Decimal_op_Explicit_m3220 ();
extern "C" void Decimal_op_Explicit_m3221 ();
extern "C" void Decimal_op_Explicit_m3222 ();
extern "C" void Decimal_op_Explicit_m3223 ();
extern "C" void Decimal_op_Explicit_m3224 ();
extern "C" void Decimal_op_Explicit_m3225 ();
extern "C" void Decimal_op_Implicit_m3226 ();
extern "C" void Decimal_op_Implicit_m3227 ();
extern "C" void Decimal_op_Implicit_m3228 ();
extern "C" void Decimal_op_Implicit_m3229 ();
extern "C" void Decimal_op_Implicit_m3230 ();
extern "C" void Decimal_op_Implicit_m3231 ();
extern "C" void Decimal_op_Implicit_m3232 ();
extern "C" void Decimal_op_Implicit_m3233 ();
extern "C" void Decimal_op_Explicit_m3234 ();
extern "C" void Decimal_op_Explicit_m3235 ();
extern "C" void Decimal_op_Explicit_m3236 ();
extern "C" void Decimal_op_Explicit_m3237 ();
extern "C" void Decimal_op_Inequality_m3238 ();
extern "C" void Decimal_op_Equality_m3239 ();
extern "C" void Decimal_op_GreaterThan_m3240 ();
extern "C" void Decimal_op_LessThan_m3241 ();
extern "C" void Boolean__cctor_m3242 ();
extern "C" void Boolean_System_IConvertible_ToType_m3243 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m3244 ();
extern "C" void Boolean_System_IConvertible_ToByte_m3245 ();
extern "C" void Boolean_System_IConvertible_ToChar_m3246 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m3247 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m3248 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m3249 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m3250 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m3251 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m3252 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m3253 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m3254 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m3255 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m3256 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m3257 ();
extern "C" void Boolean_CompareTo_m3258 ();
extern "C" void Boolean_Equals_m3259 ();
extern "C" void Boolean_CompareTo_m3260 ();
extern "C" void Boolean_Equals_m3261 ();
extern "C" void Boolean_GetHashCode_m3262 ();
extern "C" void Boolean_Parse_m3263 ();
extern "C" void Boolean_ToString_m3264 ();
extern "C" void Boolean_ToString_m3265 ();
extern "C" void IntPtr__ctor_m615 ();
extern "C" void IntPtr__ctor_m3266 ();
extern "C" void IntPtr__ctor_m3267 ();
extern "C" void IntPtr__ctor_m3268 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3269 ();
extern "C" void IntPtr_get_Size_m3270 ();
extern "C" void IntPtr_Equals_m3271 ();
extern "C" void IntPtr_GetHashCode_m3272 ();
extern "C" void IntPtr_ToInt64_m3273 ();
extern "C" void IntPtr_ToPointer_m609 ();
extern "C" void IntPtr_ToString_m3274 ();
extern "C" void IntPtr_ToString_m3275 ();
extern "C" void IntPtr_op_Equality_m3276 ();
extern "C" void IntPtr_op_Inequality_m608 ();
extern "C" void IntPtr_op_Explicit_m3277 ();
extern "C" void IntPtr_op_Explicit_m3278 ();
extern "C" void IntPtr_op_Explicit_m3279 ();
extern "C" void IntPtr_op_Explicit_m3280 ();
extern "C" void IntPtr_op_Explicit_m3281 ();
extern "C" void UIntPtr__ctor_m3282 ();
extern "C" void UIntPtr__ctor_m3283 ();
extern "C" void UIntPtr__ctor_m3284 ();
extern "C" void UIntPtr__cctor_m3285 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3286 ();
extern "C" void UIntPtr_Equals_m3287 ();
extern "C" void UIntPtr_GetHashCode_m3288 ();
extern "C" void UIntPtr_ToUInt32_m3289 ();
extern "C" void UIntPtr_ToUInt64_m3290 ();
extern "C" void UIntPtr_ToPointer_m3291 ();
extern "C" void UIntPtr_ToString_m3292 ();
extern "C" void UIntPtr_get_Size_m3293 ();
extern "C" void UIntPtr_op_Equality_m3294 ();
extern "C" void UIntPtr_op_Inequality_m3295 ();
extern "C" void UIntPtr_op_Explicit_m3296 ();
extern "C" void UIntPtr_op_Explicit_m3297 ();
extern "C" void UIntPtr_op_Explicit_m3298 ();
extern "C" void UIntPtr_op_Explicit_m3299 ();
extern "C" void UIntPtr_op_Explicit_m3300 ();
extern "C" void UIntPtr_op_Explicit_m3301 ();
extern "C" void MulticastDelegate_GetObjectData_m3302 ();
extern "C" void MulticastDelegate_Equals_m3303 ();
extern "C" void MulticastDelegate_GetHashCode_m3304 ();
extern "C" void MulticastDelegate_GetInvocationList_m3305 ();
extern "C" void MulticastDelegate_CombineImpl_m3306 ();
extern "C" void MulticastDelegate_BaseEquals_m3307 ();
extern "C" void MulticastDelegate_KPM_m3308 ();
extern "C" void MulticastDelegate_RemoveImpl_m3309 ();
extern "C" void Delegate_get_Method_m3310 ();
extern "C" void Delegate_get_Target_m3311 ();
extern "C" void Delegate_CreateDelegate_internal_m3312 ();
extern "C" void Delegate_SetMulticastInvoke_m3313 ();
extern "C" void Delegate_arg_type_match_m3314 ();
extern "C" void Delegate_return_type_match_m3315 ();
extern "C" void Delegate_CreateDelegate_m3316 ();
extern "C" void Delegate_CreateDelegate_m3317 ();
extern "C" void Delegate_CreateDelegate_m3318 ();
extern "C" void Delegate_CreateDelegate_m3319 ();
extern "C" void Delegate_GetCandidateMethod_m3320 ();
extern "C" void Delegate_CreateDelegate_m3321 ();
extern "C" void Delegate_CreateDelegate_m3322 ();
extern "C" void Delegate_CreateDelegate_m3323 ();
extern "C" void Delegate_CreateDelegate_m3324 ();
extern "C" void Delegate_Clone_m3325 ();
extern "C" void Delegate_Equals_m3326 ();
extern "C" void Delegate_GetHashCode_m3327 ();
extern "C" void Delegate_GetObjectData_m3328 ();
extern "C" void Delegate_GetInvocationList_m3329 ();
extern "C" void Delegate_Combine_m616 ();
extern "C" void Delegate_Combine_m3330 ();
extern "C" void Delegate_CombineImpl_m3331 ();
extern "C" void Delegate_Remove_m617 ();
extern "C" void Delegate_RemoveImpl_m3332 ();
extern "C" void Enum__ctor_m3333 ();
extern "C" void Enum__cctor_m3334 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m3335 ();
extern "C" void Enum_System_IConvertible_ToByte_m3336 ();
extern "C" void Enum_System_IConvertible_ToChar_m3337 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m3338 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m3339 ();
extern "C" void Enum_System_IConvertible_ToDouble_m3340 ();
extern "C" void Enum_System_IConvertible_ToInt16_m3341 ();
extern "C" void Enum_System_IConvertible_ToInt32_m3342 ();
extern "C" void Enum_System_IConvertible_ToInt64_m3343 ();
extern "C" void Enum_System_IConvertible_ToSByte_m3344 ();
extern "C" void Enum_System_IConvertible_ToSingle_m3345 ();
extern "C" void Enum_System_IConvertible_ToType_m3346 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m3347 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m3348 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m3349 ();
extern "C" void Enum_GetTypeCode_m3350 ();
extern "C" void Enum_get_value_m3351 ();
extern "C" void Enum_get_Value_m3352 ();
extern "C" void Enum_FindPosition_m3353 ();
extern "C" void Enum_GetName_m3354 ();
extern "C" void Enum_IsDefined_m2709 ();
extern "C" void Enum_get_underlying_type_m3355 ();
extern "C" void Enum_GetUnderlyingType_m3356 ();
extern "C" void Enum_FindName_m3357 ();
extern "C" void Enum_GetValue_m3358 ();
extern "C" void Enum_Parse_m1722 ();
extern "C" void Enum_compare_value_to_m3359 ();
extern "C" void Enum_CompareTo_m3360 ();
extern "C" void Enum_ToString_m3361 ();
extern "C" void Enum_ToString_m3362 ();
extern "C" void Enum_ToString_m3363 ();
extern "C" void Enum_ToString_m3364 ();
extern "C" void Enum_ToObject_m3365 ();
extern "C" void Enum_ToObject_m3366 ();
extern "C" void Enum_ToObject_m3367 ();
extern "C" void Enum_ToObject_m3368 ();
extern "C" void Enum_ToObject_m3369 ();
extern "C" void Enum_ToObject_m3370 ();
extern "C" void Enum_ToObject_m3371 ();
extern "C" void Enum_ToObject_m3372 ();
extern "C" void Enum_ToObject_m3373 ();
extern "C" void Enum_Equals_m3374 ();
extern "C" void Enum_get_hashcode_m3375 ();
extern "C" void Enum_GetHashCode_m3376 ();
extern "C" void Enum_FormatSpecifier_X_m3377 ();
extern "C" void Enum_FormatFlags_m3378 ();
extern "C" void Enum_Format_m3379 ();
extern "C" void SimpleEnumerator__ctor_m3380 ();
extern "C" void SimpleEnumerator_get_Current_m3381 ();
extern "C" void SimpleEnumerator_MoveNext_m3382 ();
extern "C" void SimpleEnumerator_Reset_m3383 ();
extern "C" void SimpleEnumerator_Clone_m3384 ();
extern "C" void Swapper__ctor_m3385 ();
extern "C" void Swapper_Invoke_m3386 ();
extern "C" void Swapper_BeginInvoke_m3387 ();
extern "C" void Swapper_EndInvoke_m3388 ();
extern "C" void Array__ctor_m3389 ();
extern "C" void Array_System_Collections_IList_get_Item_m3390 ();
extern "C" void Array_System_Collections_IList_set_Item_m3391 ();
extern "C" void Array_System_Collections_IList_Add_m3392 ();
extern "C" void Array_System_Collections_IList_Clear_m3393 ();
extern "C" void Array_System_Collections_IList_Contains_m3394 ();
extern "C" void Array_System_Collections_IList_IndexOf_m3395 ();
extern "C" void Array_System_Collections_IList_Insert_m3396 ();
extern "C" void Array_System_Collections_IList_Remove_m3397 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m3398 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m3399 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m3400 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m3401 ();
extern "C" void Array_InternalArray__ICollection_Clear_m3402 ();
extern "C" void Array_InternalArray__RemoveAt_m3403 ();
extern "C" void Array_get_Length_m1561 ();
extern "C" void Array_get_LongLength_m3404 ();
extern "C" void Array_get_Rank_m1567 ();
extern "C" void Array_GetRank_m3405 ();
extern "C" void Array_GetLength_m3406 ();
extern "C" void Array_GetLongLength_m3407 ();
extern "C" void Array_GetLowerBound_m3408 ();
extern "C" void Array_GetValue_m3409 ();
extern "C" void Array_SetValue_m3410 ();
extern "C" void Array_GetValueImpl_m3411 ();
extern "C" void Array_SetValueImpl_m3412 ();
extern "C" void Array_FastCopy_m3413 ();
extern "C" void Array_CreateInstanceImpl_m3414 ();
extern "C" void Array_get_IsSynchronized_m3415 ();
extern "C" void Array_get_SyncRoot_m3416 ();
extern "C" void Array_get_IsFixedSize_m3417 ();
extern "C" void Array_get_IsReadOnly_m3418 ();
extern "C" void Array_GetEnumerator_m3419 ();
extern "C" void Array_GetUpperBound_m3420 ();
extern "C" void Array_GetValue_m3421 ();
extern "C" void Array_GetValue_m3422 ();
extern "C" void Array_GetValue_m3423 ();
extern "C" void Array_GetValue_m3424 ();
extern "C" void Array_GetValue_m3425 ();
extern "C" void Array_GetValue_m3426 ();
extern "C" void Array_SetValue_m3427 ();
extern "C" void Array_SetValue_m3428 ();
extern "C" void Array_SetValue_m3429 ();
extern "C" void Array_SetValue_m1562 ();
extern "C" void Array_SetValue_m3430 ();
extern "C" void Array_SetValue_m3431 ();
extern "C" void Array_CreateInstance_m3432 ();
extern "C" void Array_CreateInstance_m3433 ();
extern "C" void Array_CreateInstance_m3434 ();
extern "C" void Array_CreateInstance_m3435 ();
extern "C" void Array_CreateInstance_m3436 ();
extern "C" void Array_GetIntArray_m3437 ();
extern "C" void Array_CreateInstance_m3438 ();
extern "C" void Array_GetValue_m3439 ();
extern "C" void Array_SetValue_m3440 ();
extern "C" void Array_BinarySearch_m3441 ();
extern "C" void Array_BinarySearch_m3442 ();
extern "C" void Array_BinarySearch_m3443 ();
extern "C" void Array_BinarySearch_m3444 ();
extern "C" void Array_DoBinarySearch_m3445 ();
extern "C" void Array_Clear_m1799 ();
extern "C" void Array_ClearInternal_m3446 ();
extern "C" void Array_Clone_m3447 ();
extern "C" void Array_Copy_m1735 ();
extern "C" void Array_Copy_m3448 ();
extern "C" void Array_Copy_m3449 ();
extern "C" void Array_Copy_m3450 ();
extern "C" void Array_IndexOf_m3451 ();
extern "C" void Array_IndexOf_m3452 ();
extern "C" void Array_IndexOf_m3453 ();
extern "C" void Array_Initialize_m3454 ();
extern "C" void Array_LastIndexOf_m3455 ();
extern "C" void Array_LastIndexOf_m3456 ();
extern "C" void Array_LastIndexOf_m3457 ();
extern "C" void Array_get_swapper_m3458 ();
extern "C" void Array_Reverse_m2648 ();
extern "C" void Array_Reverse_m2674 ();
extern "C" void Array_Sort_m3459 ();
extern "C" void Array_Sort_m3460 ();
extern "C" void Array_Sort_m3461 ();
extern "C" void Array_Sort_m3462 ();
extern "C" void Array_Sort_m3463 ();
extern "C" void Array_Sort_m3464 ();
extern "C" void Array_Sort_m3465 ();
extern "C" void Array_Sort_m3466 ();
extern "C" void Array_int_swapper_m3467 ();
extern "C" void Array_obj_swapper_m3468 ();
extern "C" void Array_slow_swapper_m3469 ();
extern "C" void Array_double_swapper_m3470 ();
extern "C" void Array_new_gap_m3471 ();
extern "C" void Array_combsort_m3472 ();
extern "C" void Array_combsort_m3473 ();
extern "C" void Array_combsort_m3474 ();
extern "C" void Array_qsort_m3475 ();
extern "C" void Array_swap_m3476 ();
extern "C" void Array_compare_m3477 ();
extern "C" void Array_CopyTo_m3478 ();
extern "C" void Array_CopyTo_m3479 ();
extern "C" void Array_ConstrainedCopy_m3480 ();
extern "C" void Type__ctor_m3481 ();
extern "C" void Type__cctor_m3482 ();
extern "C" void Type_FilterName_impl_m3483 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m3484 ();
extern "C" void Type_FilterAttribute_impl_m3485 ();
extern "C" void Type_get_Attributes_m3486 ();
extern "C" void Type_get_DeclaringType_m3487 ();
extern "C" void Type_get_HasElementType_m3488 ();
extern "C" void Type_get_IsAbstract_m3489 ();
extern "C" void Type_get_IsArray_m3490 ();
extern "C" void Type_get_IsByRef_m3491 ();
extern "C" void Type_get_IsClass_m3492 ();
extern "C" void Type_get_IsContextful_m3493 ();
extern "C" void Type_get_IsEnum_m3494 ();
extern "C" void Type_get_IsExplicitLayout_m3495 ();
extern "C" void Type_get_IsInterface_m3496 ();
extern "C" void Type_get_IsMarshalByRef_m3497 ();
extern "C" void Type_get_IsPointer_m3498 ();
extern "C" void Type_get_IsPrimitive_m3499 ();
extern "C" void Type_get_IsSealed_m3500 ();
extern "C" void Type_get_IsSerializable_m3501 ();
extern "C" void Type_get_IsValueType_m3502 ();
extern "C" void Type_get_MemberType_m3503 ();
extern "C" void Type_get_ReflectedType_m3504 ();
extern "C" void Type_get_TypeHandle_m3505 ();
extern "C" void Type_Equals_m3506 ();
extern "C" void Type_Equals_m3507 ();
extern "C" void Type_EqualsInternal_m3508 ();
extern "C" void Type_internal_from_handle_m3509 ();
extern "C" void Type_internal_from_name_m3510 ();
extern "C" void Type_GetType_m3511 ();
extern "C" void Type_GetType_m3512 ();
extern "C" void Type_GetTypeCodeInternal_m3513 ();
extern "C" void Type_GetTypeCode_m3514 ();
extern "C" void Type_GetTypeFromHandle_m621 ();
extern "C" void Type_GetTypeHandle_m3515 ();
extern "C" void Type_type_is_subtype_of_m3516 ();
extern "C" void Type_type_is_assignable_from_m3517 ();
extern "C" void Type_IsSubclassOf_m3518 ();
extern "C" void Type_IsAssignableFrom_m3519 ();
extern "C" void Type_IsInstanceOfType_m3520 ();
extern "C" void Type_GetHashCode_m3521 ();
extern "C" void Type_GetMethod_m3522 ();
extern "C" void Type_GetMethod_m3523 ();
extern "C" void Type_GetMethod_m3524 ();
extern "C" void Type_GetMethod_m3525 ();
extern "C" void Type_GetProperty_m3526 ();
extern "C" void Type_GetProperty_m3527 ();
extern "C" void Type_GetProperty_m3528 ();
extern "C" void Type_GetProperty_m3529 ();
extern "C" void Type_IsArrayImpl_m3530 ();
extern "C" void Type_IsValueTypeImpl_m3531 ();
extern "C" void Type_IsContextfulImpl_m3532 ();
extern "C" void Type_IsMarshalByRefImpl_m3533 ();
extern "C" void Type_GetConstructor_m3534 ();
extern "C" void Type_GetConstructor_m3535 ();
extern "C" void Type_GetConstructor_m3536 ();
extern "C" void Type_ToString_m3537 ();
extern "C" void Type_get_IsSystemType_m3538 ();
extern "C" void Type_GetGenericArguments_m3539 ();
extern "C" void Type_get_ContainsGenericParameters_m3540 ();
extern "C" void Type_get_IsGenericTypeDefinition_m3541 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m3542 ();
extern "C" void Type_GetGenericTypeDefinition_m3543 ();
extern "C" void Type_get_IsGenericType_m3544 ();
extern "C" void Type_MakeGenericType_m3545 ();
extern "C" void Type_MakeGenericType_m3546 ();
extern "C" void Type_get_IsGenericParameter_m3547 ();
extern "C" void Type_get_IsNested_m3548 ();
extern "C" void Type_GetPseudoCustomAttributes_m3549 ();
extern "C" void MemberInfo__ctor_m3550 ();
extern "C" void MemberInfo_get_Module_m3551 ();
extern "C" void Exception__ctor_m2645 ();
extern "C" void Exception__ctor_m653 ();
extern "C" void Exception__ctor_m656 ();
extern "C" void Exception__ctor_m655 ();
extern "C" void Exception_get_InnerException_m3552 ();
extern "C" void Exception_set_HResult_m654 ();
extern "C" void Exception_get_ClassName_m3553 ();
extern "C" void Exception_get_Message_m3554 ();
extern "C" void Exception_get_Source_m3555 ();
extern "C" void Exception_get_StackTrace_m3556 ();
extern "C" void Exception_GetObjectData_m1749 ();
extern "C" void Exception_ToString_m3557 ();
extern "C" void Exception_GetFullNameForStackTrace_m3558 ();
extern "C" void Exception_GetType_m3559 ();
extern "C" void RuntimeFieldHandle__ctor_m3560 ();
extern "C" void RuntimeFieldHandle_get_Value_m3561 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m3562 ();
extern "C" void RuntimeFieldHandle_Equals_m3563 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m3564 ();
extern "C" void RuntimeTypeHandle__ctor_m3565 ();
extern "C" void RuntimeTypeHandle_get_Value_m3566 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m3567 ();
extern "C" void RuntimeTypeHandle_Equals_m3568 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m3569 ();
extern "C" void ParamArrayAttribute__ctor_m3570 ();
extern "C" void OutAttribute__ctor_m3571 ();
extern "C" void ObsoleteAttribute__ctor_m3572 ();
extern "C" void ObsoleteAttribute__ctor_m3573 ();
extern "C" void ObsoleteAttribute__ctor_m3574 ();
extern "C" void DllImportAttribute__ctor_m3575 ();
extern "C" void DllImportAttribute_get_Value_m3576 ();
extern "C" void MarshalAsAttribute__ctor_m3577 ();
extern "C" void InAttribute__ctor_m3578 ();
extern "C" void GuidAttribute__ctor_m3579 ();
extern "C" void ComImportAttribute__ctor_m3580 ();
extern "C" void OptionalAttribute__ctor_m3581 ();
extern "C" void CompilerGeneratedAttribute__ctor_m3582 ();
extern "C" void InternalsVisibleToAttribute__ctor_m3583 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m3584 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m3585 ();
extern "C" void DebuggerHiddenAttribute__ctor_m3586 ();
extern "C" void DefaultMemberAttribute__ctor_m3587 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m3588 ();
extern "C" void DecimalConstantAttribute__ctor_m3589 ();
extern "C" void FieldOffsetAttribute__ctor_m3590 ();
extern "C" void AsyncCallback__ctor_m2708 ();
extern "C" void AsyncCallback_Invoke_m3591 ();
extern "C" void AsyncCallback_BeginInvoke_m2706 ();
extern "C" void AsyncCallback_EndInvoke_m3592 ();
extern "C" void TypedReference_Equals_m3593 ();
extern "C" void TypedReference_GetHashCode_m3594 ();
extern "C" void ArgIterator_Equals_m3595 ();
extern "C" void ArgIterator_GetHashCode_m3596 ();
extern "C" void MarshalByRefObject__ctor_m1614 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m3597 ();
extern "C" void RuntimeHelpers_InitializeArray_m3598 ();
extern "C" void RuntimeHelpers_InitializeArray_m1608 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m3599 ();
extern "C" void Locale_GetText_m3600 ();
extern "C" void Locale_GetText_m3601 ();
extern "C" void MonoTODOAttribute__ctor_m3602 ();
extern "C" void MonoTODOAttribute__ctor_m3603 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m3604 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m3605 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m3606 ();
extern "C" void SafeWaitHandle__ctor_m3607 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m3608 ();
extern "C" void TableRange__ctor_m3609 ();
extern "C" void CodePointIndexer__ctor_m3610 ();
extern "C" void CodePointIndexer_ToIndex_m3611 ();
extern "C" void TailoringInfo__ctor_m3612 ();
extern "C" void Contraction__ctor_m3613 ();
extern "C" void ContractionComparer__ctor_m3614 ();
extern "C" void ContractionComparer__cctor_m3615 ();
extern "C" void ContractionComparer_Compare_m3616 ();
extern "C" void Level2Map__ctor_m3617 ();
extern "C" void Level2MapComparer__ctor_m3618 ();
extern "C" void Level2MapComparer__cctor_m3619 ();
extern "C" void Level2MapComparer_Compare_m3620 ();
extern "C" void MSCompatUnicodeTable__cctor_m3621 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m3622 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m3623 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m3624 ();
extern "C" void MSCompatUnicodeTable_Category_m3625 ();
extern "C" void MSCompatUnicodeTable_Level1_m3626 ();
extern "C" void MSCompatUnicodeTable_Level2_m3627 ();
extern "C" void MSCompatUnicodeTable_Level3_m3628 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m3629 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m3630 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m3631 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m3632 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m3633 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m3634 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m3635 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m3636 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m3637 ();
extern "C" void MSCompatUnicodeTable_GetResource_m3638 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m3639 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m3640 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m3641 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m3642 ();
extern "C" void Context__ctor_m3643 ();
extern "C" void PreviousInfo__ctor_m3644 ();
extern "C" void SimpleCollator__ctor_m3645 ();
extern "C" void SimpleCollator__cctor_m3646 ();
extern "C" void SimpleCollator_SetCJKTable_m3647 ();
extern "C" void SimpleCollator_GetNeutralCulture_m3648 ();
extern "C" void SimpleCollator_Category_m3649 ();
extern "C" void SimpleCollator_Level1_m3650 ();
extern "C" void SimpleCollator_Level2_m3651 ();
extern "C" void SimpleCollator_IsHalfKana_m3652 ();
extern "C" void SimpleCollator_GetContraction_m3653 ();
extern "C" void SimpleCollator_GetContraction_m3654 ();
extern "C" void SimpleCollator_GetTailContraction_m3655 ();
extern "C" void SimpleCollator_GetTailContraction_m3656 ();
extern "C" void SimpleCollator_FilterOptions_m3657 ();
extern "C" void SimpleCollator_GetExtenderType_m3658 ();
extern "C" void SimpleCollator_ToDashTypeValue_m3659 ();
extern "C" void SimpleCollator_FilterExtender_m3660 ();
extern "C" void SimpleCollator_IsIgnorable_m3661 ();
extern "C" void SimpleCollator_IsSafe_m3662 ();
extern "C" void SimpleCollator_GetSortKey_m3663 ();
extern "C" void SimpleCollator_GetSortKey_m3664 ();
extern "C" void SimpleCollator_GetSortKey_m3665 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m3666 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m3667 ();
extern "C" void SimpleCollator_CompareOrdinal_m3668 ();
extern "C" void SimpleCollator_CompareQuick_m3669 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m3670 ();
extern "C" void SimpleCollator_Compare_m3671 ();
extern "C" void SimpleCollator_ClearBuffer_m3672 ();
extern "C" void SimpleCollator_QuickCheckPossible_m3673 ();
extern "C" void SimpleCollator_CompareInternal_m3674 ();
extern "C" void SimpleCollator_CompareFlagPair_m3675 ();
extern "C" void SimpleCollator_IsPrefix_m3676 ();
extern "C" void SimpleCollator_IsPrefix_m3677 ();
extern "C" void SimpleCollator_IsPrefix_m3678 ();
extern "C" void SimpleCollator_IsSuffix_m3679 ();
extern "C" void SimpleCollator_IsSuffix_m3680 ();
extern "C" void SimpleCollator_QuickIndexOf_m3681 ();
extern "C" void SimpleCollator_IndexOf_m3682 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m3683 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m3684 ();
extern "C" void SimpleCollator_IndexOfSortKey_m3685 ();
extern "C" void SimpleCollator_IndexOf_m3686 ();
extern "C" void SimpleCollator_LastIndexOf_m3687 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m3688 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m3689 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m3690 ();
extern "C" void SimpleCollator_LastIndexOf_m3691 ();
extern "C" void SimpleCollator_MatchesForward_m3692 ();
extern "C" void SimpleCollator_MatchesForwardCore_m3693 ();
extern "C" void SimpleCollator_MatchesPrimitive_m3694 ();
extern "C" void SimpleCollator_MatchesBackward_m3695 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m3696 ();
extern "C" void SortKey__ctor_m3697 ();
extern "C" void SortKey__ctor_m3698 ();
extern "C" void SortKey_Compare_m3699 ();
extern "C" void SortKey_get_OriginalString_m3700 ();
extern "C" void SortKey_get_KeyData_m3701 ();
extern "C" void SortKey_Equals_m3702 ();
extern "C" void SortKey_GetHashCode_m3703 ();
extern "C" void SortKey_ToString_m3704 ();
extern "C" void SortKeyBuffer__ctor_m3705 ();
extern "C" void SortKeyBuffer_Reset_m3706 ();
extern "C" void SortKeyBuffer_Initialize_m3707 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m3708 ();
extern "C" void SortKeyBuffer_AppendKana_m3709 ();
extern "C" void SortKeyBuffer_AppendNormal_m3710 ();
extern "C" void SortKeyBuffer_AppendLevel5_m3711 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m3712 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m3713 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m3714 ();
extern "C" void SortKeyBuffer_GetResult_m3715 ();
extern "C" void PrimeGeneratorBase__ctor_m3716 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m3717 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m3718 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m3719 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m3720 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3721 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3722 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3723 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3724 ();
extern "C" void PrimalityTests_GetSPPRounds_m3725 ();
extern "C" void PrimalityTests_Test_m3726 ();
extern "C" void PrimalityTests_RabinMillerTest_m3727 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m3728 ();
extern "C" void ModulusRing__ctor_m3729 ();
extern "C" void ModulusRing_BarrettReduction_m3730 ();
extern "C" void ModulusRing_Multiply_m3731 ();
extern "C" void ModulusRing_Difference_m3732 ();
extern "C" void ModulusRing_Pow_m3733 ();
extern "C" void ModulusRing_Pow_m3734 ();
extern "C" void Kernel_AddSameSign_m3735 ();
extern "C" void Kernel_Subtract_m3736 ();
extern "C" void Kernel_MinusEq_m3737 ();
extern "C" void Kernel_PlusEq_m3738 ();
extern "C" void Kernel_Compare_m3739 ();
extern "C" void Kernel_SingleByteDivideInPlace_m3740 ();
extern "C" void Kernel_DwordMod_m3741 ();
extern "C" void Kernel_DwordDivMod_m3742 ();
extern "C" void Kernel_multiByteDivide_m3743 ();
extern "C" void Kernel_LeftShift_m3744 ();
extern "C" void Kernel_RightShift_m3745 ();
extern "C" void Kernel_MultiplyByDword_m3746 ();
extern "C" void Kernel_Multiply_m3747 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m3748 ();
extern "C" void Kernel_modInverse_m3749 ();
extern "C" void Kernel_modInverse_m3750 ();
extern "C" void BigInteger__ctor_m3751 ();
extern "C" void BigInteger__ctor_m3752 ();
extern "C" void BigInteger__ctor_m3753 ();
extern "C" void BigInteger__ctor_m3754 ();
extern "C" void BigInteger__ctor_m3755 ();
extern "C" void BigInteger__cctor_m3756 ();
extern "C" void BigInteger_get_Rng_m3757 ();
extern "C" void BigInteger_GenerateRandom_m3758 ();
extern "C" void BigInteger_GenerateRandom_m3759 ();
extern "C" void BigInteger_Randomize_m3760 ();
extern "C" void BigInteger_Randomize_m3761 ();
extern "C" void BigInteger_BitCount_m3762 ();
extern "C" void BigInteger_TestBit_m3763 ();
extern "C" void BigInteger_TestBit_m3764 ();
extern "C" void BigInteger_SetBit_m3765 ();
extern "C" void BigInteger_SetBit_m3766 ();
extern "C" void BigInteger_LowestSetBit_m3767 ();
extern "C" void BigInteger_GetBytes_m3768 ();
extern "C" void BigInteger_ToString_m3769 ();
extern "C" void BigInteger_ToString_m3770 ();
extern "C" void BigInteger_Normalize_m3771 ();
extern "C" void BigInteger_Clear_m3772 ();
extern "C" void BigInteger_GetHashCode_m3773 ();
extern "C" void BigInteger_ToString_m3774 ();
extern "C" void BigInteger_Equals_m3775 ();
extern "C" void BigInteger_ModInverse_m3776 ();
extern "C" void BigInteger_ModPow_m3777 ();
extern "C" void BigInteger_IsProbablePrime_m3778 ();
extern "C" void BigInteger_GeneratePseudoPrime_m3779 ();
extern "C" void BigInteger_Incr2_m3780 ();
extern "C" void BigInteger_op_Implicit_m3781 ();
extern "C" void BigInteger_op_Implicit_m3782 ();
extern "C" void BigInteger_op_Addition_m3783 ();
extern "C" void BigInteger_op_Subtraction_m3784 ();
extern "C" void BigInteger_op_Modulus_m3785 ();
extern "C" void BigInteger_op_Modulus_m3786 ();
extern "C" void BigInteger_op_Division_m3787 ();
extern "C" void BigInteger_op_Multiply_m3788 ();
extern "C" void BigInteger_op_Multiply_m3789 ();
extern "C" void BigInteger_op_LeftShift_m3790 ();
extern "C" void BigInteger_op_RightShift_m3791 ();
extern "C" void BigInteger_op_Equality_m3792 ();
extern "C" void BigInteger_op_Inequality_m3793 ();
extern "C" void BigInteger_op_Equality_m3794 ();
extern "C" void BigInteger_op_Inequality_m3795 ();
extern "C" void BigInteger_op_GreaterThan_m3796 ();
extern "C" void BigInteger_op_LessThan_m3797 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m3798 ();
extern "C" void BigInteger_op_LessThanOrEqual_m3799 ();
extern "C" void CryptoConvert_ToInt32LE_m3800 ();
extern "C" void CryptoConvert_ToUInt32LE_m3801 ();
extern "C" void CryptoConvert_GetBytesLE_m3802 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m3803 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m3804 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m3805 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m3806 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m3807 ();
extern "C" void KeyBuilder_get_Rng_m3808 ();
extern "C" void KeyBuilder_Key_m3809 ();
extern "C" void KeyBuilder_IV_m3810 ();
extern "C" void BlockProcessor__ctor_m3811 ();
extern "C" void BlockProcessor_Finalize_m3812 ();
extern "C" void BlockProcessor_Initialize_m3813 ();
extern "C" void BlockProcessor_Core_m3814 ();
extern "C" void BlockProcessor_Core_m3815 ();
extern "C" void BlockProcessor_Final_m3816 ();
extern "C" void KeyGeneratedEventHandler__ctor_m3817 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3818 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m3819 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m3820 ();
extern "C" void DSAManaged__ctor_m3821 ();
extern "C" void DSAManaged_add_KeyGenerated_m3822 ();
extern "C" void DSAManaged_remove_KeyGenerated_m3823 ();
extern "C" void DSAManaged_Finalize_m3824 ();
extern "C" void DSAManaged_Generate_m3825 ();
extern "C" void DSAManaged_GenerateKeyPair_m3826 ();
extern "C" void DSAManaged_add_m3827 ();
extern "C" void DSAManaged_GenerateParams_m3828 ();
extern "C" void DSAManaged_get_Random_m3829 ();
extern "C" void DSAManaged_get_KeySize_m3830 ();
extern "C" void DSAManaged_get_PublicOnly_m3831 ();
extern "C" void DSAManaged_NormalizeArray_m3832 ();
extern "C" void DSAManaged_ExportParameters_m3833 ();
extern "C" void DSAManaged_ImportParameters_m3834 ();
extern "C" void DSAManaged_CreateSignature_m3835 ();
extern "C" void DSAManaged_VerifySignature_m3836 ();
extern "C" void DSAManaged_Dispose_m3837 ();
extern "C" void KeyPairPersistence__ctor_m3838 ();
extern "C" void KeyPairPersistence__ctor_m3839 ();
extern "C" void KeyPairPersistence__cctor_m3840 ();
extern "C" void KeyPairPersistence_get_Filename_m3841 ();
extern "C" void KeyPairPersistence_get_KeyValue_m3842 ();
extern "C" void KeyPairPersistence_set_KeyValue_m3843 ();
extern "C" void KeyPairPersistence_Load_m3844 ();
extern "C" void KeyPairPersistence_Save_m3845 ();
extern "C" void KeyPairPersistence_Remove_m3846 ();
extern "C" void KeyPairPersistence_get_UserPath_m3847 ();
extern "C" void KeyPairPersistence_get_MachinePath_m3848 ();
extern "C" void KeyPairPersistence__CanSecure_m3849 ();
extern "C" void KeyPairPersistence__ProtectUser_m3850 ();
extern "C" void KeyPairPersistence__ProtectMachine_m3851 ();
extern "C" void KeyPairPersistence__IsUserProtected_m3852 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m3853 ();
extern "C" void KeyPairPersistence_CanSecure_m3854 ();
extern "C" void KeyPairPersistence_ProtectUser_m3855 ();
extern "C" void KeyPairPersistence_ProtectMachine_m3856 ();
extern "C" void KeyPairPersistence_IsUserProtected_m3857 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m3858 ();
extern "C" void KeyPairPersistence_get_CanChange_m3859 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m3860 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m3861 ();
extern "C" void KeyPairPersistence_get_ContainerName_m3862 ();
extern "C" void KeyPairPersistence_Copy_m3863 ();
extern "C" void KeyPairPersistence_FromXml_m3864 ();
extern "C" void KeyPairPersistence_ToXml_m3865 ();
extern "C" void MACAlgorithm__ctor_m3866 ();
extern "C" void MACAlgorithm_Initialize_m3867 ();
extern "C" void MACAlgorithm_Core_m3868 ();
extern "C" void MACAlgorithm_Final_m3869 ();
extern "C" void PKCS1__cctor_m3870 ();
extern "C" void PKCS1_Compare_m3871 ();
extern "C" void PKCS1_I2OSP_m3872 ();
extern "C" void PKCS1_OS2IP_m3873 ();
extern "C" void PKCS1_RSAEP_m3874 ();
extern "C" void PKCS1_RSASP1_m3875 ();
extern "C" void PKCS1_RSAVP1_m3876 ();
extern "C" void PKCS1_Encrypt_v15_m3877 ();
extern "C" void PKCS1_Sign_v15_m3878 ();
extern "C" void PKCS1_Verify_v15_m3879 ();
extern "C" void PKCS1_Verify_v15_m3880 ();
extern "C" void PKCS1_Encode_v15_m3881 ();
extern "C" void PrivateKeyInfo__ctor_m3882 ();
extern "C" void PrivateKeyInfo__ctor_m3883 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m3884 ();
extern "C" void PrivateKeyInfo_Decode_m3885 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m3886 ();
extern "C" void PrivateKeyInfo_Normalize_m3887 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m3888 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m3889 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3890 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3891 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m3892 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m3893 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m3894 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m3895 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m3896 ();
extern "C" void KeyGeneratedEventHandler__ctor_m3897 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3898 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m3899 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m3900 ();
extern "C" void RSAManaged__ctor_m3901 ();
extern "C" void RSAManaged_add_KeyGenerated_m3902 ();
extern "C" void RSAManaged_remove_KeyGenerated_m3903 ();
extern "C" void RSAManaged_Finalize_m3904 ();
extern "C" void RSAManaged_GenerateKeyPair_m3905 ();
extern "C" void RSAManaged_get_KeySize_m3906 ();
extern "C" void RSAManaged_get_PublicOnly_m3907 ();
extern "C" void RSAManaged_DecryptValue_m3908 ();
extern "C" void RSAManaged_EncryptValue_m3909 ();
extern "C" void RSAManaged_ExportParameters_m3910 ();
extern "C" void RSAManaged_ImportParameters_m3911 ();
extern "C" void RSAManaged_Dispose_m3912 ();
extern "C" void RSAManaged_ToXmlString_m3913 ();
extern "C" void RSAManaged_get_IsCrtPossible_m3914 ();
extern "C" void RSAManaged_GetPaddedValue_m3915 ();
extern "C" void SymmetricTransform__ctor_m3916 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m3917 ();
extern "C" void SymmetricTransform_Finalize_m3918 ();
extern "C" void SymmetricTransform_Dispose_m3919 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m3920 ();
extern "C" void SymmetricTransform_Transform_m3921 ();
extern "C" void SymmetricTransform_CBC_m3922 ();
extern "C" void SymmetricTransform_CFB_m3923 ();
extern "C" void SymmetricTransform_OFB_m3924 ();
extern "C" void SymmetricTransform_CTS_m3925 ();
extern "C" void SymmetricTransform_CheckInput_m3926 ();
extern "C" void SymmetricTransform_TransformBlock_m3927 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m3928 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m3929 ();
extern "C" void SymmetricTransform_Random_m3930 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m3931 ();
extern "C" void SymmetricTransform_FinalEncrypt_m3932 ();
extern "C" void SymmetricTransform_FinalDecrypt_m3933 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m3934 ();
extern "C" void SafeBag__ctor_m3935 ();
extern "C" void SafeBag_get_BagOID_m3936 ();
extern "C" void SafeBag_get_ASN1_m3937 ();
extern "C" void DeriveBytes__ctor_m3938 ();
extern "C" void DeriveBytes__cctor_m3939 ();
extern "C" void DeriveBytes_set_HashName_m3940 ();
extern "C" void DeriveBytes_set_IterationCount_m3941 ();
extern "C" void DeriveBytes_set_Password_m3942 ();
extern "C" void DeriveBytes_set_Salt_m3943 ();
extern "C" void DeriveBytes_Adjust_m3944 ();
extern "C" void DeriveBytes_Derive_m3945 ();
extern "C" void DeriveBytes_DeriveKey_m3946 ();
extern "C" void DeriveBytes_DeriveIV_m3947 ();
extern "C" void DeriveBytes_DeriveMAC_m3948 ();
extern "C" void PKCS12__ctor_m3949 ();
extern "C" void PKCS12__ctor_m3950 ();
extern "C" void PKCS12__ctor_m3951 ();
extern "C" void PKCS12__cctor_m3952 ();
extern "C" void PKCS12_Decode_m3953 ();
extern "C" void PKCS12_Finalize_m3954 ();
extern "C" void PKCS12_set_Password_m3955 ();
extern "C" void PKCS12_get_IterationCount_m3956 ();
extern "C" void PKCS12_set_IterationCount_m3957 ();
extern "C" void PKCS12_get_Certificates_m3958 ();
extern "C" void PKCS12_get_RNG_m3959 ();
extern "C" void PKCS12_Compare_m3960 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m3961 ();
extern "C" void PKCS12_Decrypt_m3962 ();
extern "C" void PKCS12_Decrypt_m3963 ();
extern "C" void PKCS12_Encrypt_m3964 ();
extern "C" void PKCS12_GetExistingParameters_m3965 ();
extern "C" void PKCS12_AddPrivateKey_m3966 ();
extern "C" void PKCS12_ReadSafeBag_m3967 ();
extern "C" void PKCS12_CertificateSafeBag_m3968 ();
extern "C" void PKCS12_MAC_m3969 ();
extern "C" void PKCS12_GetBytes_m3970 ();
extern "C" void PKCS12_EncryptedContentInfo_m3971 ();
extern "C" void PKCS12_AddCertificate_m3972 ();
extern "C" void PKCS12_AddCertificate_m3973 ();
extern "C" void PKCS12_RemoveCertificate_m3974 ();
extern "C" void PKCS12_RemoveCertificate_m3975 ();
extern "C" void PKCS12_Clone_m3976 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m3977 ();
extern "C" void X501__cctor_m3978 ();
extern "C" void X501_ToString_m3979 ();
extern "C" void X501_ToString_m3980 ();
extern "C" void X501_AppendEntry_m3981 ();
extern "C" void X509Certificate__ctor_m3982 ();
extern "C" void X509Certificate__cctor_m3983 ();
extern "C" void X509Certificate_Parse_m3984 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m3985 ();
extern "C" void X509Certificate_get_DSA_m3986 ();
extern "C" void X509Certificate_get_IssuerName_m3987 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m3988 ();
extern "C" void X509Certificate_get_PublicKey_m3989 ();
extern "C" void X509Certificate_get_RawData_m3990 ();
extern "C" void X509Certificate_get_SubjectName_m3991 ();
extern "C" void X509Certificate_get_ValidFrom_m3992 ();
extern "C" void X509Certificate_get_ValidUntil_m3993 ();
extern "C" void X509Certificate_GetIssuerName_m3994 ();
extern "C" void X509Certificate_GetSubjectName_m3995 ();
extern "C" void X509Certificate_GetObjectData_m3996 ();
extern "C" void X509Certificate_PEM_m3997 ();
extern "C" void X509CertificateEnumerator__ctor_m3998 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3999 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4000 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4001 ();
extern "C" void X509CertificateEnumerator_get_Current_m4002 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4003 ();
extern "C" void X509CertificateEnumerator_Reset_m4004 ();
extern "C" void X509CertificateCollection__ctor_m4005 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4006 ();
extern "C" void X509CertificateCollection_get_Item_m4007 ();
extern "C" void X509CertificateCollection_Add_m4008 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4009 ();
extern "C" void X509CertificateCollection_GetHashCode_m4010 ();
extern "C" void X509Extension__ctor_m4011 ();
extern "C" void X509Extension_Decode_m4012 ();
extern "C" void X509Extension_Equals_m4013 ();
extern "C" void X509Extension_GetHashCode_m4014 ();
extern "C" void X509Extension_WriteLine_m4015 ();
extern "C" void X509Extension_ToString_m4016 ();
extern "C" void X509ExtensionCollection__ctor_m4017 ();
extern "C" void X509ExtensionCollection__ctor_m4018 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4019 ();
extern "C" void ASN1__ctor_m4020 ();
extern "C" void ASN1__ctor_m4021 ();
extern "C" void ASN1__ctor_m4022 ();
extern "C" void ASN1_get_Count_m4023 ();
extern "C" void ASN1_get_Tag_m4024 ();
extern "C" void ASN1_get_Length_m4025 ();
extern "C" void ASN1_get_Value_m4026 ();
extern "C" void ASN1_set_Value_m4027 ();
extern "C" void ASN1_CompareArray_m4028 ();
extern "C" void ASN1_CompareValue_m4029 ();
extern "C" void ASN1_Add_m4030 ();
extern "C" void ASN1_GetBytes_m4031 ();
extern "C" void ASN1_Decode_m4032 ();
extern "C" void ASN1_DecodeTLV_m4033 ();
extern "C" void ASN1_get_Item_m4034 ();
extern "C" void ASN1_Element_m4035 ();
extern "C" void ASN1_ToString_m4036 ();
extern "C" void ASN1Convert_FromInt32_m4037 ();
extern "C" void ASN1Convert_FromOid_m4038 ();
extern "C" void ASN1Convert_ToInt32_m4039 ();
extern "C" void ASN1Convert_ToOid_m4040 ();
extern "C" void ASN1Convert_ToDateTime_m4041 ();
extern "C" void BitConverterLE_GetUIntBytes_m4042 ();
extern "C" void BitConverterLE_GetBytes_m4043 ();
extern "C" void BitConverterLE_UShortFromBytes_m4044 ();
extern "C" void BitConverterLE_UIntFromBytes_m4045 ();
extern "C" void BitConverterLE_ULongFromBytes_m4046 ();
extern "C" void BitConverterLE_ToInt16_m4047 ();
extern "C" void BitConverterLE_ToInt32_m4048 ();
extern "C" void BitConverterLE_ToSingle_m4049 ();
extern "C" void BitConverterLE_ToDouble_m4050 ();
extern "C" void ContentInfo__ctor_m4051 ();
extern "C" void ContentInfo__ctor_m4052 ();
extern "C" void ContentInfo__ctor_m4053 ();
extern "C" void ContentInfo__ctor_m4054 ();
extern "C" void ContentInfo_get_ASN1_m4055 ();
extern "C" void ContentInfo_get_Content_m4056 ();
extern "C" void ContentInfo_set_Content_m4057 ();
extern "C" void ContentInfo_get_ContentType_m4058 ();
extern "C" void ContentInfo_set_ContentType_m4059 ();
extern "C" void ContentInfo_GetASN1_m4060 ();
extern "C" void EncryptedData__ctor_m4061 ();
extern "C" void EncryptedData__ctor_m4062 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m4063 ();
extern "C" void EncryptedData_get_EncryptedContent_m4064 ();
extern "C" void StrongName__cctor_m4065 ();
extern "C" void StrongName_get_PublicKey_m4066 ();
extern "C" void StrongName_get_PublicKeyToken_m4067 ();
extern "C" void StrongName_get_TokenAlgorithm_m4068 ();
extern "C" void SecurityParser__ctor_m4069 ();
extern "C" void SecurityParser_LoadXml_m4070 ();
extern "C" void SecurityParser_ToXml_m4071 ();
extern "C" void SecurityParser_OnStartParsing_m4072 ();
extern "C" void SecurityParser_OnProcessingInstruction_m4073 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m4074 ();
extern "C" void SecurityParser_OnStartElement_m4075 ();
extern "C" void SecurityParser_OnEndElement_m4076 ();
extern "C" void SecurityParser_OnChars_m4077 ();
extern "C" void SecurityParser_OnEndParsing_m4078 ();
extern "C" void AttrListImpl__ctor_m4079 ();
extern "C" void AttrListImpl_get_Length_m4080 ();
extern "C" void AttrListImpl_GetName_m4081 ();
extern "C" void AttrListImpl_GetValue_m4082 ();
extern "C" void AttrListImpl_GetValue_m4083 ();
extern "C" void AttrListImpl_get_Names_m4084 ();
extern "C" void AttrListImpl_get_Values_m4085 ();
extern "C" void AttrListImpl_Clear_m4086 ();
extern "C" void AttrListImpl_Add_m4087 ();
extern "C" void SmallXmlParser__ctor_m4088 ();
extern "C" void SmallXmlParser_Error_m4089 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m4090 ();
extern "C" void SmallXmlParser_IsNameChar_m4091 ();
extern "C" void SmallXmlParser_IsWhitespace_m4092 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m4093 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m4094 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m4095 ();
extern "C" void SmallXmlParser_Peek_m4096 ();
extern "C" void SmallXmlParser_Read_m4097 ();
extern "C" void SmallXmlParser_Expect_m4098 ();
extern "C" void SmallXmlParser_ReadUntil_m4099 ();
extern "C" void SmallXmlParser_ReadName_m4100 ();
extern "C" void SmallXmlParser_Parse_m4101 ();
extern "C" void SmallXmlParser_Cleanup_m4102 ();
extern "C" void SmallXmlParser_ReadContent_m4103 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m4104 ();
extern "C" void SmallXmlParser_ReadCharacters_m4105 ();
extern "C" void SmallXmlParser_ReadReference_m4106 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m4107 ();
extern "C" void SmallXmlParser_ReadAttribute_m4108 ();
extern "C" void SmallXmlParser_ReadCDATASection_m4109 ();
extern "C" void SmallXmlParser_ReadComment_m4110 ();
extern "C" void SmallXmlParserException__ctor_m4111 ();
extern "C" void Runtime_GetDisplayName_m4112 ();
extern "C" void KeyNotFoundException__ctor_m4113 ();
extern "C" void KeyNotFoundException__ctor_m4114 ();
extern "C" void SimpleEnumerator__ctor_m4115 ();
extern "C" void SimpleEnumerator__cctor_m4116 ();
extern "C" void SimpleEnumerator_Clone_m4117 ();
extern "C" void SimpleEnumerator_MoveNext_m4118 ();
extern "C" void SimpleEnumerator_get_Current_m4119 ();
extern "C" void SimpleEnumerator_Reset_m4120 ();
extern "C" void ArrayListWrapper__ctor_m4121 ();
extern "C" void ArrayListWrapper_get_Item_m4122 ();
extern "C" void ArrayListWrapper_set_Item_m4123 ();
extern "C" void ArrayListWrapper_get_Count_m4124 ();
extern "C" void ArrayListWrapper_get_Capacity_m4125 ();
extern "C" void ArrayListWrapper_set_Capacity_m4126 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m4127 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m4128 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m4129 ();
extern "C" void ArrayListWrapper_Add_m4130 ();
extern "C" void ArrayListWrapper_Clear_m4131 ();
extern "C" void ArrayListWrapper_Contains_m4132 ();
extern "C" void ArrayListWrapper_IndexOf_m4133 ();
extern "C" void ArrayListWrapper_IndexOf_m4134 ();
extern "C" void ArrayListWrapper_IndexOf_m4135 ();
extern "C" void ArrayListWrapper_Insert_m4136 ();
extern "C" void ArrayListWrapper_InsertRange_m4137 ();
extern "C" void ArrayListWrapper_Remove_m4138 ();
extern "C" void ArrayListWrapper_RemoveAt_m4139 ();
extern "C" void ArrayListWrapper_CopyTo_m4140 ();
extern "C" void ArrayListWrapper_CopyTo_m4141 ();
extern "C" void ArrayListWrapper_CopyTo_m4142 ();
extern "C" void ArrayListWrapper_GetEnumerator_m4143 ();
extern "C" void ArrayListWrapper_AddRange_m4144 ();
extern "C" void ArrayListWrapper_Clone_m4145 ();
extern "C" void ArrayListWrapper_Sort_m4146 ();
extern "C" void ArrayListWrapper_Sort_m4147 ();
extern "C" void ArrayListWrapper_ToArray_m4148 ();
extern "C" void ArrayListWrapper_ToArray_m4149 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m4150 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m4151 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m4152 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m4153 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m4154 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m4155 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m4156 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m4157 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m4158 ();
extern "C" void SynchronizedArrayListWrapper_Add_m4159 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m4160 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m4161 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4162 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4163 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m4164 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m4165 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m4166 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m4167 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m4168 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4169 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4170 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m4171 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m4172 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m4173 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m4174 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m4175 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m4176 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m4177 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m4178 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m4179 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m4180 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m4181 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m4182 ();
extern "C" void FixedSizeArrayListWrapper_Add_m4183 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m4184 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m4185 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m4186 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m4187 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m4188 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m4189 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m4190 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m4191 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m4192 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m4193 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m4194 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m4195 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m4196 ();
extern "C" void ArrayList__ctor_m1569 ();
extern "C" void ArrayList__ctor_m1612 ();
extern "C" void ArrayList__ctor_m1698 ();
extern "C" void ArrayList__ctor_m4197 ();
extern "C" void ArrayList__cctor_m4198 ();
extern "C" void ArrayList_get_Item_m4199 ();
extern "C" void ArrayList_set_Item_m4200 ();
extern "C" void ArrayList_get_Count_m4201 ();
extern "C" void ArrayList_get_Capacity_m4202 ();
extern "C" void ArrayList_set_Capacity_m4203 ();
extern "C" void ArrayList_get_IsReadOnly_m4204 ();
extern "C" void ArrayList_get_IsSynchronized_m4205 ();
extern "C" void ArrayList_get_SyncRoot_m4206 ();
extern "C" void ArrayList_EnsureCapacity_m4207 ();
extern "C" void ArrayList_Shift_m4208 ();
extern "C" void ArrayList_Add_m4209 ();
extern "C" void ArrayList_Clear_m4210 ();
extern "C" void ArrayList_Contains_m4211 ();
extern "C" void ArrayList_IndexOf_m4212 ();
extern "C" void ArrayList_IndexOf_m4213 ();
extern "C" void ArrayList_IndexOf_m4214 ();
extern "C" void ArrayList_Insert_m4215 ();
extern "C" void ArrayList_InsertRange_m4216 ();
extern "C" void ArrayList_Remove_m4217 ();
extern "C" void ArrayList_RemoveAt_m4218 ();
extern "C" void ArrayList_CopyTo_m4219 ();
extern "C" void ArrayList_CopyTo_m4220 ();
extern "C" void ArrayList_CopyTo_m4221 ();
extern "C" void ArrayList_GetEnumerator_m4222 ();
extern "C" void ArrayList_AddRange_m4223 ();
extern "C" void ArrayList_Sort_m4224 ();
extern "C" void ArrayList_Sort_m4225 ();
extern "C" void ArrayList_ToArray_m4226 ();
extern "C" void ArrayList_ToArray_m4227 ();
extern "C" void ArrayList_Clone_m4228 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m4229 ();
extern "C" void ArrayList_Synchronized_m4230 ();
extern "C" void ArrayList_ReadOnly_m2668 ();
extern "C" void BitArrayEnumerator__ctor_m4231 ();
extern "C" void BitArrayEnumerator_Clone_m4232 ();
extern "C" void BitArrayEnumerator_get_Current_m4233 ();
extern "C" void BitArrayEnumerator_MoveNext_m4234 ();
extern "C" void BitArrayEnumerator_Reset_m4235 ();
extern "C" void BitArrayEnumerator_checkVersion_m4236 ();
extern "C" void BitArray__ctor_m4237 ();
extern "C" void BitArray__ctor_m1738 ();
extern "C" void BitArray_getByte_m4238 ();
extern "C" void BitArray_get_Count_m4239 ();
extern "C" void BitArray_get_Item_m1729 ();
extern "C" void BitArray_set_Item_m1739 ();
extern "C" void BitArray_get_Length_m1728 ();
extern "C" void BitArray_get_SyncRoot_m4240 ();
extern "C" void BitArray_Clone_m4241 ();
extern "C" void BitArray_CopyTo_m4242 ();
extern "C" void BitArray_Get_m4243 ();
extern "C" void BitArray_Set_m4244 ();
extern "C" void BitArray_GetEnumerator_m4245 ();
extern "C" void CaseInsensitiveComparer__ctor_m4246 ();
extern "C" void CaseInsensitiveComparer__ctor_m4247 ();
extern "C" void CaseInsensitiveComparer__cctor_m4248 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m1553 ();
extern "C" void CaseInsensitiveComparer_Compare_m4249 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m4250 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m4251 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m4252 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m4253 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m4254 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1554 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m4255 ();
extern "C" void CollectionBase__ctor_m1680 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m4256 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m4257 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m4258 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m4259 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m4260 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m4261 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m4262 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m4263 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m4264 ();
extern "C" void CollectionBase_get_Count_m4265 ();
extern "C" void CollectionBase_GetEnumerator_m4266 ();
extern "C" void CollectionBase_Clear_m4267 ();
extern "C" void CollectionBase_RemoveAt_m4268 ();
extern "C" void CollectionBase_get_InnerList_m1674 ();
extern "C" void CollectionBase_get_List_m1736 ();
extern "C" void CollectionBase_OnClear_m4269 ();
extern "C" void CollectionBase_OnClearComplete_m4270 ();
extern "C" void CollectionBase_OnInsert_m4271 ();
extern "C" void CollectionBase_OnInsertComplete_m4272 ();
extern "C" void CollectionBase_OnRemove_m4273 ();
extern "C" void CollectionBase_OnRemoveComplete_m4274 ();
extern "C" void CollectionBase_OnSet_m4275 ();
extern "C" void CollectionBase_OnSetComplete_m4276 ();
extern "C" void CollectionBase_OnValidate_m4277 ();
extern "C" void Comparer__ctor_m4278 ();
extern "C" void Comparer__ctor_m4279 ();
extern "C" void Comparer__cctor_m4280 ();
extern "C" void Comparer_Compare_m4281 ();
extern "C" void Comparer_GetObjectData_m4282 ();
extern "C" void DictionaryEntry__ctor_m1558 ();
extern "C" void DictionaryEntry_get_Key_m4283 ();
extern "C" void DictionaryEntry_get_Value_m4284 ();
extern "C" void KeyMarker__ctor_m4285 ();
extern "C" void KeyMarker__cctor_m4286 ();
extern "C" void Enumerator__ctor_m4287 ();
extern "C" void Enumerator__cctor_m4288 ();
extern "C" void Enumerator_FailFast_m4289 ();
extern "C" void Enumerator_Reset_m4290 ();
extern "C" void Enumerator_MoveNext_m4291 ();
extern "C" void Enumerator_get_Entry_m4292 ();
extern "C" void Enumerator_get_Key_m4293 ();
extern "C" void Enumerator_get_Value_m4294 ();
extern "C" void Enumerator_get_Current_m4295 ();
extern "C" void HashKeys__ctor_m4296 ();
extern "C" void HashKeys_get_Count_m4297 ();
extern "C" void HashKeys_get_SyncRoot_m4298 ();
extern "C" void HashKeys_CopyTo_m4299 ();
extern "C" void HashKeys_GetEnumerator_m4300 ();
extern "C" void HashValues__ctor_m4301 ();
extern "C" void HashValues_get_Count_m4302 ();
extern "C" void HashValues_get_SyncRoot_m4303 ();
extern "C" void HashValues_CopyTo_m4304 ();
extern "C" void HashValues_GetEnumerator_m4305 ();
extern "C" void SyncHashtable__ctor_m4306 ();
extern "C" void SyncHashtable__ctor_m4307 ();
extern "C" void SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m4308 ();
extern "C" void SyncHashtable_GetObjectData_m4309 ();
extern "C" void SyncHashtable_get_Count_m4310 ();
extern "C" void SyncHashtable_get_SyncRoot_m4311 ();
extern "C" void SyncHashtable_get_Keys_m4312 ();
extern "C" void SyncHashtable_get_Values_m4313 ();
extern "C" void SyncHashtable_get_Item_m4314 ();
extern "C" void SyncHashtable_set_Item_m4315 ();
extern "C" void SyncHashtable_CopyTo_m4316 ();
extern "C" void SyncHashtable_Add_m4317 ();
extern "C" void SyncHashtable_Clear_m4318 ();
extern "C" void SyncHashtable_Contains_m4319 ();
extern "C" void SyncHashtable_GetEnumerator_m4320 ();
extern "C" void SyncHashtable_Remove_m4321 ();
extern "C" void SyncHashtable_ContainsKey_m4322 ();
extern "C" void SyncHashtable_Clone_m4323 ();
extern "C" void Hashtable__ctor_m1718 ();
extern "C" void Hashtable__ctor_m4324 ();
extern "C" void Hashtable__ctor_m4325 ();
extern "C" void Hashtable__ctor_m1721 ();
extern "C" void Hashtable__ctor_m4326 ();
extern "C" void Hashtable__ctor_m1555 ();
extern "C" void Hashtable__ctor_m4327 ();
extern "C" void Hashtable__ctor_m1556 ();
extern "C" void Hashtable__ctor_m1609 ();
extern "C" void Hashtable__ctor_m4328 ();
extern "C" void Hashtable__ctor_m1568 ();
extern "C" void Hashtable__ctor_m4329 ();
extern "C" void Hashtable__cctor_m4330 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m4331 ();
extern "C" void Hashtable_set_comparer_m4332 ();
extern "C" void Hashtable_set_hcp_m4333 ();
extern "C" void Hashtable_get_Count_m4334 ();
extern "C" void Hashtable_get_SyncRoot_m4335 ();
extern "C" void Hashtable_get_Keys_m4336 ();
extern "C" void Hashtable_get_Values_m4337 ();
extern "C" void Hashtable_get_Item_m4338 ();
extern "C" void Hashtable_set_Item_m4339 ();
extern "C" void Hashtable_CopyTo_m4340 ();
extern "C" void Hashtable_Add_m4341 ();
extern "C" void Hashtable_Clear_m4342 ();
extern "C" void Hashtable_Contains_m4343 ();
extern "C" void Hashtable_GetEnumerator_m4344 ();
extern "C" void Hashtable_Remove_m4345 ();
extern "C" void Hashtable_ContainsKey_m4346 ();
extern "C" void Hashtable_Clone_m4347 ();
extern "C" void Hashtable_GetObjectData_m4348 ();
extern "C" void Hashtable_OnDeserialization_m4349 ();
extern "C" void Hashtable_Synchronized_m4350 ();
extern "C" void Hashtable_GetHash_m4351 ();
extern "C" void Hashtable_KeyEquals_m4352 ();
extern "C" void Hashtable_AdjustThreshold_m4353 ();
extern "C" void Hashtable_SetTable_m4354 ();
extern "C" void Hashtable_Find_m4355 ();
extern "C" void Hashtable_Rehash_m4356 ();
extern "C" void Hashtable_PutImpl_m4357 ();
extern "C" void Hashtable_CopyToArray_m4358 ();
extern "C" void Hashtable_TestPrime_m4359 ();
extern "C" void Hashtable_CalcPrime_m4360 ();
extern "C" void Hashtable_ToPrime_m4361 ();
extern "C" void Enumerator__ctor_m4362 ();
extern "C" void Enumerator__cctor_m4363 ();
extern "C" void Enumerator_Reset_m4364 ();
extern "C" void Enumerator_MoveNext_m4365 ();
extern "C" void Enumerator_get_Entry_m4366 ();
extern "C" void Enumerator_get_Key_m4367 ();
extern "C" void Enumerator_get_Value_m4368 ();
extern "C" void Enumerator_get_Current_m4369 ();
extern "C" void Enumerator_Clone_m4370 ();
extern "C" void SortedList__ctor_m4371 ();
extern "C" void SortedList__ctor_m1607 ();
extern "C" void SortedList__ctor_m4372 ();
extern "C" void SortedList__ctor_m4373 ();
extern "C" void SortedList__cctor_m4374 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m4375 ();
extern "C" void SortedList_get_Count_m4376 ();
extern "C" void SortedList_get_SyncRoot_m4377 ();
extern "C" void SortedList_get_IsFixedSize_m4378 ();
extern "C" void SortedList_get_IsReadOnly_m4379 ();
extern "C" void SortedList_get_Item_m4380 ();
extern "C" void SortedList_set_Item_m4381 ();
extern "C" void SortedList_get_Capacity_m4382 ();
extern "C" void SortedList_set_Capacity_m4383 ();
extern "C" void SortedList_Add_m4384 ();
extern "C" void SortedList_Contains_m4385 ();
extern "C" void SortedList_GetEnumerator_m4386 ();
extern "C" void SortedList_Remove_m4387 ();
extern "C" void SortedList_CopyTo_m4388 ();
extern "C" void SortedList_Clone_m4389 ();
extern "C" void SortedList_RemoveAt_m4390 ();
extern "C" void SortedList_IndexOfKey_m4391 ();
extern "C" void SortedList_ContainsKey_m4392 ();
extern "C" void SortedList_GetByIndex_m4393 ();
extern "C" void SortedList_EnsureCapacity_m4394 ();
extern "C" void SortedList_PutImpl_m4395 ();
extern "C" void SortedList_GetImpl_m4396 ();
extern "C" void SortedList_InitTable_m4397 ();
extern "C" void SortedList_Find_m4398 ();
extern "C" void Enumerator__ctor_m4399 ();
extern "C" void Enumerator_Clone_m4400 ();
extern "C" void Enumerator_get_Current_m4401 ();
extern "C" void Enumerator_MoveNext_m4402 ();
extern "C" void Enumerator_Reset_m4403 ();
extern "C" void Stack__ctor_m1731 ();
extern "C" void Stack__ctor_m4404 ();
extern "C" void Stack__ctor_m4405 ();
extern "C" void Stack_Resize_m4406 ();
extern "C" void Stack_get_Count_m4407 ();
extern "C" void Stack_get_SyncRoot_m4408 ();
extern "C" void Stack_Clear_m4409 ();
extern "C" void Stack_Clone_m4410 ();
extern "C" void Stack_CopyTo_m4411 ();
extern "C" void Stack_GetEnumerator_m4412 ();
extern "C" void Stack_Peek_m4413 ();
extern "C" void Stack_Pop_m4414 ();
extern "C" void Stack_Push_m4415 ();
extern "C" void DebuggableAttribute__ctor_m4416 ();
extern "C" void DebuggerDisplayAttribute__ctor_m4417 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m4418 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m4419 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m4420 ();
extern "C" void StackFrame__ctor_m4421 ();
extern "C" void StackFrame__ctor_m4422 ();
extern "C" void StackFrame_get_frame_info_m4423 ();
extern "C" void StackFrame_GetFileLineNumber_m4424 ();
extern "C" void StackFrame_GetFileName_m4425 ();
extern "C" void StackFrame_GetSecureFileName_m4426 ();
extern "C" void StackFrame_GetILOffset_m4427 ();
extern "C" void StackFrame_GetMethod_m4428 ();
extern "C" void StackFrame_GetNativeOffset_m4429 ();
extern "C" void StackFrame_GetInternalMethodName_m4430 ();
extern "C" void StackFrame_ToString_m4431 ();
extern "C" void StackTrace__ctor_m4432 ();
extern "C" void StackTrace__ctor_m632 ();
extern "C" void StackTrace__ctor_m4433 ();
extern "C" void StackTrace__ctor_m4434 ();
extern "C" void StackTrace__ctor_m4435 ();
extern "C" void StackTrace_init_frames_m4436 ();
extern "C" void StackTrace_get_trace_m4437 ();
extern "C" void StackTrace_get_FrameCount_m4438 ();
extern "C" void StackTrace_GetFrame_m4439 ();
extern "C" void StackTrace_ToString_m4440 ();
extern "C" void Calendar__ctor_m4441 ();
extern "C" void Calendar_Clone_m4442 ();
extern "C" void Calendar_CheckReadOnly_m4443 ();
extern "C" void Calendar_get_EraNames_m4444 ();
extern "C" void CCMath_div_m4445 ();
extern "C" void CCMath_mod_m4446 ();
extern "C" void CCMath_div_mod_m4447 ();
extern "C" void CCFixed_FromDateTime_m4448 ();
extern "C" void CCFixed_day_of_week_m4449 ();
extern "C" void CCGregorianCalendar_is_leap_year_m4450 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m4451 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m4452 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m4453 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m4454 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m4455 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m4456 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m4457 ();
extern "C" void CCGregorianCalendar_GetMonth_m4458 ();
extern "C" void CCGregorianCalendar_GetYear_m4459 ();
extern "C" void CompareInfo__ctor_m4460 ();
extern "C" void CompareInfo__ctor_m4461 ();
extern "C" void CompareInfo__cctor_m4462 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m4463 ();
extern "C" void CompareInfo_get_UseManagedCollation_m4464 ();
extern "C" void CompareInfo_construct_compareinfo_m4465 ();
extern "C" void CompareInfo_free_internal_collator_m4466 ();
extern "C" void CompareInfo_internal_compare_m4467 ();
extern "C" void CompareInfo_assign_sortkey_m4468 ();
extern "C" void CompareInfo_internal_index_m4469 ();
extern "C" void CompareInfo_Finalize_m4470 ();
extern "C" void CompareInfo_internal_compare_managed_m4471 ();
extern "C" void CompareInfo_internal_compare_switch_m4472 ();
extern "C" void CompareInfo_Compare_m4473 ();
extern "C" void CompareInfo_Compare_m4474 ();
extern "C" void CompareInfo_Compare_m4475 ();
extern "C" void CompareInfo_Equals_m4476 ();
extern "C" void CompareInfo_GetHashCode_m4477 ();
extern "C" void CompareInfo_GetSortKey_m4478 ();
extern "C" void CompareInfo_IndexOf_m4479 ();
extern "C" void CompareInfo_internal_index_managed_m4480 ();
extern "C" void CompareInfo_internal_index_switch_m4481 ();
extern "C" void CompareInfo_IndexOf_m4482 ();
extern "C" void CompareInfo_IsPrefix_m4483 ();
extern "C" void CompareInfo_IsSuffix_m4484 ();
extern "C" void CompareInfo_LastIndexOf_m4485 ();
extern "C" void CompareInfo_LastIndexOf_m4486 ();
extern "C" void CompareInfo_ToString_m4487 ();
extern "C" void CompareInfo_get_LCID_m4488 ();
extern "C" void CultureInfo__ctor_m4489 ();
extern "C" void CultureInfo__ctor_m4490 ();
extern "C" void CultureInfo__ctor_m4491 ();
extern "C" void CultureInfo__ctor_m4492 ();
extern "C" void CultureInfo__ctor_m4493 ();
extern "C" void CultureInfo__cctor_m4494 ();
extern "C" void CultureInfo_get_InvariantCulture_m1598 ();
extern "C" void CultureInfo_get_CurrentCulture_m2698 ();
extern "C" void CultureInfo_get_CurrentUICulture_m2699 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m4495 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m4496 ();
extern "C" void CultureInfo_get_LCID_m4497 ();
extern "C" void CultureInfo_get_Name_m4498 ();
extern "C" void CultureInfo_get_Parent_m4499 ();
extern "C" void CultureInfo_get_TextInfo_m4500 ();
extern "C" void CultureInfo_get_IcuName_m4501 ();
extern "C" void CultureInfo_Clone_m4502 ();
extern "C" void CultureInfo_Equals_m4503 ();
extern "C" void CultureInfo_GetHashCode_m4504 ();
extern "C" void CultureInfo_ToString_m4505 ();
extern "C" void CultureInfo_get_CompareInfo_m4506 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m4507 ();
extern "C" void CultureInfo_CheckNeutral_m4508 ();
extern "C" void CultureInfo_get_NumberFormat_m4509 ();
extern "C" void CultureInfo_set_NumberFormat_m4510 ();
extern "C" void CultureInfo_get_DateTimeFormat_m4511 ();
extern "C" void CultureInfo_set_DateTimeFormat_m4512 ();
extern "C" void CultureInfo_get_IsReadOnly_m4513 ();
extern "C" void CultureInfo_GetFormat_m4514 ();
extern "C" void CultureInfo_Construct_m4515 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m4516 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m4517 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m4518 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m4519 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m4520 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m4521 ();
extern "C" void CultureInfo_construct_datetime_format_m4522 ();
extern "C" void CultureInfo_construct_number_format_m4523 ();
extern "C" void CultureInfo_ConstructInvariant_m4524 ();
extern "C" void CultureInfo_CreateTextInfo_m4525 ();
extern "C" void CultureInfo_CreateCulture_m4526 ();
extern "C" void DateTimeFormatInfo__ctor_m4527 ();
extern "C" void DateTimeFormatInfo__ctor_m4528 ();
extern "C" void DateTimeFormatInfo__cctor_m4529 ();
extern "C" void DateTimeFormatInfo_GetInstance_m4530 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m4531 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m4532 ();
extern "C" void DateTimeFormatInfo_Clone_m4533 ();
extern "C" void DateTimeFormatInfo_GetFormat_m4534 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m4535 ();
extern "C" void DateTimeFormatInfo_GetEraName_m4536 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m4537 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m4538 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m4539 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m4540 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m4541 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m4542 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m4543 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m4544 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m4545 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m4546 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m4547 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m4548 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m4549 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m4550 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m4551 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m4552 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m4553 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m4554 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m4555 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m4556 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m4557 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m4558 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m4559 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m4560 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m4561 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m4562 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m4563 ();
extern "C" void DateTimeFormatInfo_GetDayName_m4564 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m4565 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m4566 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m4567 ();
extern "C" void DaylightTime__ctor_m4568 ();
extern "C" void DaylightTime_get_Start_m4569 ();
extern "C" void DaylightTime_get_End_m4570 ();
extern "C" void DaylightTime_get_Delta_m4571 ();
extern "C" void GregorianCalendar__ctor_m4572 ();
extern "C" void GregorianCalendar__ctor_m4573 ();
extern "C" void GregorianCalendar_get_Eras_m4574 ();
extern "C" void GregorianCalendar_set_CalendarType_m4575 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m4576 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m4577 ();
extern "C" void GregorianCalendar_GetEra_m4578 ();
extern "C" void GregorianCalendar_GetMonth_m4579 ();
extern "C" void GregorianCalendar_GetYear_m4580 ();
extern "C" void NumberFormatInfo__ctor_m4581 ();
extern "C" void NumberFormatInfo__ctor_m4582 ();
extern "C" void NumberFormatInfo__ctor_m4583 ();
extern "C" void NumberFormatInfo__cctor_m4584 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m4585 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m4586 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m4587 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m4588 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m4589 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m4590 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m4591 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m4592 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m4593 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m4594 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m4595 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m4596 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m4597 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m4598 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m4599 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m4600 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m4601 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m4602 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m4603 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m4604 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m4605 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m4606 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m4607 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m4608 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m4609 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m4610 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m4611 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m4612 ();
extern "C" void NumberFormatInfo_GetFormat_m4613 ();
extern "C" void NumberFormatInfo_Clone_m4614 ();
extern "C" void NumberFormatInfo_GetInstance_m4615 ();
extern "C" void TextInfo__ctor_m4616 ();
extern "C" void TextInfo__ctor_m4617 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m4618 ();
extern "C" void TextInfo_get_ListSeparator_m4619 ();
extern "C" void TextInfo_get_CultureName_m4620 ();
extern "C" void TextInfo_Equals_m4621 ();
extern "C" void TextInfo_GetHashCode_m4622 ();
extern "C" void TextInfo_ToString_m4623 ();
extern "C" void TextInfo_ToLower_m4624 ();
extern "C" void TextInfo_ToUpper_m4625 ();
extern "C" void TextInfo_ToLower_m4626 ();
extern "C" void TextInfo_Clone_m4627 ();
extern "C" void IsolatedStorageException__ctor_m4628 ();
extern "C" void IsolatedStorageException__ctor_m4629 ();
extern "C" void IsolatedStorageException__ctor_m4630 ();
extern "C" void BinaryReader__ctor_m4631 ();
extern "C" void BinaryReader__ctor_m4632 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m4633 ();
extern "C" void BinaryReader_get_BaseStream_m4634 ();
extern "C" void BinaryReader_Close_m4635 ();
extern "C" void BinaryReader_Dispose_m4636 ();
extern "C" void BinaryReader_FillBuffer_m4637 ();
extern "C" void BinaryReader_Read_m4638 ();
extern "C" void BinaryReader_Read_m4639 ();
extern "C" void BinaryReader_Read_m4640 ();
extern "C" void BinaryReader_ReadCharBytes_m4641 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m4642 ();
extern "C" void BinaryReader_ReadBoolean_m4643 ();
extern "C" void BinaryReader_ReadByte_m4644 ();
extern "C" void BinaryReader_ReadBytes_m4645 ();
extern "C" void BinaryReader_ReadChar_m4646 ();
extern "C" void BinaryReader_ReadDecimal_m4647 ();
extern "C" void BinaryReader_ReadDouble_m4648 ();
extern "C" void BinaryReader_ReadInt16_m4649 ();
extern "C" void BinaryReader_ReadInt32_m4650 ();
extern "C" void BinaryReader_ReadInt64_m4651 ();
extern "C" void BinaryReader_ReadSByte_m4652 ();
extern "C" void BinaryReader_ReadString_m4653 ();
extern "C" void BinaryReader_ReadSingle_m4654 ();
extern "C" void BinaryReader_ReadUInt16_m4655 ();
extern "C" void BinaryReader_ReadUInt32_m4656 ();
extern "C" void BinaryReader_ReadUInt64_m4657 ();
extern "C" void BinaryReader_CheckBuffer_m4658 ();
extern "C" void Directory_CreateDirectory_m2684 ();
extern "C" void Directory_CreateDirectoriesInternal_m4659 ();
extern "C" void Directory_Exists_m2683 ();
extern "C" void Directory_GetCurrentDirectory_m4660 ();
extern "C" void Directory_GetFiles_m2686 ();
extern "C" void Directory_GetFileSystemEntries_m4661 ();
extern "C" void DirectoryInfo__ctor_m4662 ();
extern "C" void DirectoryInfo__ctor_m4663 ();
extern "C" void DirectoryInfo__ctor_m4664 ();
extern "C" void DirectoryInfo_Initialize_m4665 ();
extern "C" void DirectoryInfo_get_Exists_m4666 ();
extern "C" void DirectoryInfo_get_Parent_m4667 ();
extern "C" void DirectoryInfo_Create_m4668 ();
extern "C" void DirectoryInfo_ToString_m4669 ();
extern "C" void DirectoryNotFoundException__ctor_m4670 ();
extern "C" void DirectoryNotFoundException__ctor_m4671 ();
extern "C" void DirectoryNotFoundException__ctor_m4672 ();
extern "C" void EndOfStreamException__ctor_m4673 ();
extern "C" void EndOfStreamException__ctor_m4674 ();
extern "C" void File_Delete_m4675 ();
extern "C" void File_Exists_m4676 ();
extern "C" void File_Open_m4677 ();
extern "C" void File_OpenRead_m2682 ();
extern "C" void File_OpenText_m4678 ();
extern "C" void FileNotFoundException__ctor_m4679 ();
extern "C" void FileNotFoundException__ctor_m4680 ();
extern "C" void FileNotFoundException__ctor_m4681 ();
extern "C" void FileNotFoundException_get_Message_m4682 ();
extern "C" void FileNotFoundException_GetObjectData_m4683 ();
extern "C" void FileNotFoundException_ToString_m4684 ();
extern "C" void ReadDelegate__ctor_m4685 ();
extern "C" void ReadDelegate_Invoke_m4686 ();
extern "C" void ReadDelegate_BeginInvoke_m4687 ();
extern "C" void ReadDelegate_EndInvoke_m4688 ();
extern "C" void WriteDelegate__ctor_m4689 ();
extern "C" void WriteDelegate_Invoke_m4690 ();
extern "C" void WriteDelegate_BeginInvoke_m4691 ();
extern "C" void WriteDelegate_EndInvoke_m4692 ();
extern "C" void FileStream__ctor_m4693 ();
extern "C" void FileStream__ctor_m4694 ();
extern "C" void FileStream__ctor_m4695 ();
extern "C" void FileStream__ctor_m4696 ();
extern "C" void FileStream__ctor_m4697 ();
extern "C" void FileStream_get_CanRead_m4698 ();
extern "C" void FileStream_get_CanWrite_m4699 ();
extern "C" void FileStream_get_CanSeek_m4700 ();
extern "C" void FileStream_get_Length_m4701 ();
extern "C" void FileStream_get_Position_m4702 ();
extern "C" void FileStream_set_Position_m4703 ();
extern "C" void FileStream_ReadByte_m4704 ();
extern "C" void FileStream_WriteByte_m4705 ();
extern "C" void FileStream_Read_m4706 ();
extern "C" void FileStream_ReadInternal_m4707 ();
extern "C" void FileStream_BeginRead_m4708 ();
extern "C" void FileStream_EndRead_m4709 ();
extern "C" void FileStream_Write_m4710 ();
extern "C" void FileStream_WriteInternal_m4711 ();
extern "C" void FileStream_BeginWrite_m4712 ();
extern "C" void FileStream_EndWrite_m4713 ();
extern "C" void FileStream_Seek_m4714 ();
extern "C" void FileStream_SetLength_m4715 ();
extern "C" void FileStream_Flush_m4716 ();
extern "C" void FileStream_Finalize_m4717 ();
extern "C" void FileStream_Dispose_m4718 ();
extern "C" void FileStream_ReadSegment_m4719 ();
extern "C" void FileStream_WriteSegment_m4720 ();
extern "C" void FileStream_FlushBuffer_m4721 ();
extern "C" void FileStream_FlushBuffer_m4722 ();
extern "C" void FileStream_FlushBufferIfDirty_m4723 ();
extern "C" void FileStream_RefillBuffer_m4724 ();
extern "C" void FileStream_ReadData_m4725 ();
extern "C" void FileStream_InitBuffer_m4726 ();
extern "C" void FileStream_GetSecureFileName_m4727 ();
extern "C" void FileStream_GetSecureFileName_m4728 ();
extern "C" void FileStreamAsyncResult__ctor_m4729 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m4730 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m4731 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m4732 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m4733 ();
extern "C" void FileSystemInfo__ctor_m4734 ();
extern "C" void FileSystemInfo__ctor_m4735 ();
extern "C" void FileSystemInfo_GetObjectData_m4736 ();
extern "C" void FileSystemInfo_get_FullName_m4737 ();
extern "C" void FileSystemInfo_Refresh_m4738 ();
extern "C" void FileSystemInfo_InternalRefresh_m4739 ();
extern "C" void FileSystemInfo_CheckPath_m4740 ();
extern "C" void IOException__ctor_m4741 ();
extern "C" void IOException__ctor_m4742 ();
extern "C" void IOException__ctor_m2712 ();
extern "C" void IOException__ctor_m4743 ();
extern "C" void IOException__ctor_m4744 ();
extern "C" void MemoryStream__ctor_m2713 ();
extern "C" void MemoryStream__ctor_m2718 ();
extern "C" void MemoryStream__ctor_m2719 ();
extern "C" void MemoryStream_InternalConstructor_m4745 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m4746 ();
extern "C" void MemoryStream_get_CanRead_m4747 ();
extern "C" void MemoryStream_get_CanSeek_m4748 ();
extern "C" void MemoryStream_get_CanWrite_m4749 ();
extern "C" void MemoryStream_set_Capacity_m4750 ();
extern "C" void MemoryStream_get_Length_m4751 ();
extern "C" void MemoryStream_get_Position_m4752 ();
extern "C" void MemoryStream_set_Position_m4753 ();
extern "C" void MemoryStream_Dispose_m4754 ();
extern "C" void MemoryStream_Flush_m4755 ();
extern "C" void MemoryStream_Read_m4756 ();
extern "C" void MemoryStream_ReadByte_m4757 ();
extern "C" void MemoryStream_Seek_m4758 ();
extern "C" void MemoryStream_CalculateNewCapacity_m4759 ();
extern "C" void MemoryStream_Expand_m4760 ();
extern "C" void MemoryStream_SetLength_m4761 ();
extern "C" void MemoryStream_ToArray_m4762 ();
extern "C" void MemoryStream_Write_m4763 ();
extern "C" void MemoryStream_WriteByte_m4764 ();
extern "C" void MonoIO__cctor_m4765 ();
extern "C" void MonoIO_GetException_m4766 ();
extern "C" void MonoIO_GetException_m4767 ();
extern "C" void MonoIO_CreateDirectory_m4768 ();
extern "C" void MonoIO_GetFileSystemEntries_m4769 ();
extern "C" void MonoIO_GetCurrentDirectory_m4770 ();
extern "C" void MonoIO_DeleteFile_m4771 ();
extern "C" void MonoIO_GetFileAttributes_m4772 ();
extern "C" void MonoIO_GetFileType_m4773 ();
extern "C" void MonoIO_ExistsFile_m4774 ();
extern "C" void MonoIO_ExistsDirectory_m4775 ();
extern "C" void MonoIO_GetFileStat_m4776 ();
extern "C" void MonoIO_Open_m4777 ();
extern "C" void MonoIO_Close_m4778 ();
extern "C" void MonoIO_Read_m4779 ();
extern "C" void MonoIO_Write_m4780 ();
extern "C" void MonoIO_Seek_m4781 ();
extern "C" void MonoIO_GetLength_m4782 ();
extern "C" void MonoIO_SetLength_m4783 ();
extern "C" void MonoIO_get_ConsoleOutput_m4784 ();
extern "C" void MonoIO_get_ConsoleInput_m4785 ();
extern "C" void MonoIO_get_ConsoleError_m4786 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m4787 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m4788 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m4789 ();
extern "C" void MonoIO_get_PathSeparator_m4790 ();
extern "C" void Path__cctor_m4791 ();
extern "C" void Path_Combine_m2685 ();
extern "C" void Path_CleanPath_m4792 ();
extern "C" void Path_GetDirectoryName_m4793 ();
extern "C" void Path_GetFileName_m4794 ();
extern "C" void Path_GetFullPath_m4795 ();
extern "C" void Path_WindowsDriveAdjustment_m4796 ();
extern "C" void Path_InsecureGetFullPath_m4797 ();
extern "C" void Path_IsDsc_m4798 ();
extern "C" void Path_GetPathRoot_m4799 ();
extern "C" void Path_IsPathRooted_m4800 ();
extern "C" void Path_GetInvalidPathChars_m4801 ();
extern "C" void Path_GetServerAndShare_m4802 ();
extern "C" void Path_SameRoot_m4803 ();
extern "C" void Path_CanonicalizePath_m4804 ();
extern "C" void PathTooLongException__ctor_m4805 ();
extern "C" void PathTooLongException__ctor_m4806 ();
extern "C" void PathTooLongException__ctor_m4807 ();
extern "C" void SearchPattern__cctor_m4808 ();
extern "C" void Stream__ctor_m2714 ();
extern "C" void Stream__cctor_m4809 ();
extern "C" void Stream_Dispose_m4810 ();
extern "C" void Stream_Dispose_m2717 ();
extern "C" void Stream_Close_m2716 ();
extern "C" void Stream_ReadByte_m4811 ();
extern "C" void Stream_WriteByte_m4812 ();
extern "C" void Stream_BeginRead_m4813 ();
extern "C" void Stream_BeginWrite_m4814 ();
extern "C" void Stream_EndRead_m4815 ();
extern "C" void Stream_EndWrite_m4816 ();
extern "C" void NullStream__ctor_m4817 ();
extern "C" void NullStream_get_CanRead_m4818 ();
extern "C" void NullStream_get_CanSeek_m4819 ();
extern "C" void NullStream_get_CanWrite_m4820 ();
extern "C" void NullStream_get_Length_m4821 ();
extern "C" void NullStream_get_Position_m4822 ();
extern "C" void NullStream_set_Position_m4823 ();
extern "C" void NullStream_Flush_m4824 ();
extern "C" void NullStream_Read_m4825 ();
extern "C" void NullStream_ReadByte_m4826 ();
extern "C" void NullStream_Seek_m4827 ();
extern "C" void NullStream_SetLength_m4828 ();
extern "C" void NullStream_Write_m4829 ();
extern "C" void NullStream_WriteByte_m4830 ();
extern "C" void StreamAsyncResult__ctor_m4831 ();
extern "C" void StreamAsyncResult_SetComplete_m4832 ();
extern "C" void StreamAsyncResult_SetComplete_m4833 ();
extern "C" void StreamAsyncResult_get_AsyncState_m4834 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m4835 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m4836 ();
extern "C" void StreamAsyncResult_get_Exception_m4837 ();
extern "C" void StreamAsyncResult_get_NBytes_m4838 ();
extern "C" void StreamAsyncResult_get_Done_m4839 ();
extern "C" void StreamAsyncResult_set_Done_m4840 ();
extern "C" void NullStreamReader__ctor_m4841 ();
extern "C" void NullStreamReader_Peek_m4842 ();
extern "C" void NullStreamReader_Read_m4843 ();
extern "C" void NullStreamReader_Read_m4844 ();
extern "C" void NullStreamReader_ReadLine_m4845 ();
extern "C" void NullStreamReader_ReadToEnd_m4846 ();
extern "C" void StreamReader__ctor_m4847 ();
extern "C" void StreamReader__ctor_m4848 ();
extern "C" void StreamReader__ctor_m4849 ();
extern "C" void StreamReader__ctor_m4850 ();
extern "C" void StreamReader__ctor_m4851 ();
extern "C" void StreamReader__cctor_m4852 ();
extern "C" void StreamReader_Initialize_m4853 ();
extern "C" void StreamReader_Dispose_m4854 ();
extern "C" void StreamReader_DoChecks_m4855 ();
extern "C" void StreamReader_ReadBuffer_m4856 ();
extern "C" void StreamReader_Peek_m4857 ();
extern "C" void StreamReader_Read_m4858 ();
extern "C" void StreamReader_Read_m4859 ();
extern "C" void StreamReader_FindNextEOL_m4860 ();
extern "C" void StreamReader_ReadLine_m4861 ();
extern "C" void StreamReader_ReadToEnd_m4862 ();
extern "C" void StreamWriter__ctor_m4863 ();
extern "C" void StreamWriter__ctor_m4864 ();
extern "C" void StreamWriter__cctor_m4865 ();
extern "C" void StreamWriter_Initialize_m4866 ();
extern "C" void StreamWriter_set_AutoFlush_m4867 ();
extern "C" void StreamWriter_Dispose_m4868 ();
extern "C" void StreamWriter_Flush_m4869 ();
extern "C" void StreamWriter_FlushBytes_m4870 ();
extern "C" void StreamWriter_Decode_m4871 ();
extern "C" void StreamWriter_Write_m4872 ();
extern "C" void StreamWriter_LowLevelWrite_m4873 ();
extern "C" void StreamWriter_LowLevelWrite_m4874 ();
extern "C" void StreamWriter_Write_m4875 ();
extern "C" void StreamWriter_Write_m4876 ();
extern "C" void StreamWriter_Write_m4877 ();
extern "C" void StreamWriter_Close_m4878 ();
extern "C" void StreamWriter_Finalize_m4879 ();
extern "C" void StringReader__ctor_m4880 ();
extern "C" void StringReader_Dispose_m4881 ();
extern "C" void StringReader_Peek_m4882 ();
extern "C" void StringReader_Read_m4883 ();
extern "C" void StringReader_Read_m4884 ();
extern "C" void StringReader_ReadLine_m4885 ();
extern "C" void StringReader_ReadToEnd_m4886 ();
extern "C" void StringReader_CheckObjectDisposedException_m4887 ();
extern "C" void NullTextReader__ctor_m4888 ();
extern "C" void NullTextReader_ReadLine_m4889 ();
extern "C" void TextReader__ctor_m4890 ();
extern "C" void TextReader__cctor_m4891 ();
extern "C" void TextReader_Dispose_m4892 ();
extern "C" void TextReader_Dispose_m4893 ();
extern "C" void TextReader_Peek_m4894 ();
extern "C" void TextReader_Read_m4895 ();
extern "C" void TextReader_Read_m4896 ();
extern "C" void TextReader_ReadLine_m4897 ();
extern "C" void TextReader_ReadToEnd_m4898 ();
extern "C" void TextReader_Synchronized_m4899 ();
extern "C" void SynchronizedReader__ctor_m4900 ();
extern "C" void SynchronizedReader_Peek_m4901 ();
extern "C" void SynchronizedReader_ReadLine_m4902 ();
extern "C" void SynchronizedReader_ReadToEnd_m4903 ();
extern "C" void SynchronizedReader_Read_m4904 ();
extern "C" void SynchronizedReader_Read_m4905 ();
extern "C" void NullTextWriter__ctor_m4906 ();
extern "C" void NullTextWriter_Write_m4907 ();
extern "C" void NullTextWriter_Write_m4908 ();
extern "C" void NullTextWriter_Write_m4909 ();
extern "C" void TextWriter__ctor_m4910 ();
extern "C" void TextWriter__cctor_m4911 ();
extern "C" void TextWriter_Close_m4912 ();
extern "C" void TextWriter_Dispose_m4913 ();
extern "C" void TextWriter_Dispose_m4914 ();
extern "C" void TextWriter_Flush_m4915 ();
extern "C" void TextWriter_Synchronized_m4916 ();
extern "C" void TextWriter_Write_m4917 ();
extern "C" void TextWriter_Write_m4918 ();
extern "C" void TextWriter_Write_m4919 ();
extern "C" void TextWriter_Write_m4920 ();
extern "C" void TextWriter_WriteLine_m4921 ();
extern "C" void TextWriter_WriteLine_m4922 ();
extern "C" void SynchronizedWriter__ctor_m4923 ();
extern "C" void SynchronizedWriter_Close_m4924 ();
extern "C" void SynchronizedWriter_Flush_m4925 ();
extern "C" void SynchronizedWriter_Write_m4926 ();
extern "C" void SynchronizedWriter_Write_m4927 ();
extern "C" void SynchronizedWriter_Write_m4928 ();
extern "C" void SynchronizedWriter_Write_m4929 ();
extern "C" void SynchronizedWriter_WriteLine_m4930 ();
extern "C" void SynchronizedWriter_WriteLine_m4931 ();
extern "C" void UnexceptionalStreamReader__ctor_m4932 ();
extern "C" void UnexceptionalStreamReader__cctor_m4933 ();
extern "C" void UnexceptionalStreamReader_Peek_m4934 ();
extern "C" void UnexceptionalStreamReader_Read_m4935 ();
extern "C" void UnexceptionalStreamReader_Read_m4936 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m4937 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m4938 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m4939 ();
extern "C" void UnexceptionalStreamWriter__ctor_m4940 ();
extern "C" void UnexceptionalStreamWriter_Flush_m4941 ();
extern "C" void UnexceptionalStreamWriter_Write_m4942 ();
extern "C" void UnexceptionalStreamWriter_Write_m4943 ();
extern "C" void UnexceptionalStreamWriter_Write_m4944 ();
extern "C" void UnexceptionalStreamWriter_Write_m4945 ();
extern "C" void UnmanagedMemoryStream_get_CanRead_m4946 ();
extern "C" void UnmanagedMemoryStream_get_CanSeek_m4947 ();
extern "C" void UnmanagedMemoryStream_get_CanWrite_m4948 ();
extern "C" void UnmanagedMemoryStream_get_Length_m4949 ();
extern "C" void UnmanagedMemoryStream_get_Position_m4950 ();
extern "C" void UnmanagedMemoryStream_set_Position_m4951 ();
extern "C" void UnmanagedMemoryStream_Read_m4952 ();
extern "C" void UnmanagedMemoryStream_ReadByte_m4953 ();
extern "C" void UnmanagedMemoryStream_Seek_m4954 ();
extern "C" void UnmanagedMemoryStream_SetLength_m4955 ();
extern "C" void UnmanagedMemoryStream_Flush_m4956 ();
extern "C" void UnmanagedMemoryStream_Dispose_m4957 ();
extern "C" void UnmanagedMemoryStream_Write_m4958 ();
extern "C" void UnmanagedMemoryStream_WriteByte_m4959 ();
extern "C" void AssemblyBuilder_get_Location_m4960 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m4961 ();
extern "C" void AssemblyBuilder_GetTypes_m4962 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m4963 ();
extern "C" void AssemblyBuilder_not_supported_m4964 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m4965 ();
extern "C" void ConstructorBuilder__ctor_m4966 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m4967 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m4968 ();
extern "C" void ConstructorBuilder_GetParameters_m4969 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m4970 ();
extern "C" void ConstructorBuilder_GetParameterCount_m4971 ();
extern "C" void ConstructorBuilder_Invoke_m4972 ();
extern "C" void ConstructorBuilder_Invoke_m4973 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m4974 ();
extern "C" void ConstructorBuilder_get_Attributes_m4975 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m4976 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m4977 ();
extern "C" void ConstructorBuilder_get_Name_m4978 ();
extern "C" void ConstructorBuilder_IsDefined_m4979 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m4980 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m4981 ();
extern "C" void ConstructorBuilder_GetILGenerator_m4982 ();
extern "C" void ConstructorBuilder_GetILGenerator_m4983 ();
extern "C" void ConstructorBuilder_GetToken_m4984 ();
extern "C" void ConstructorBuilder_get_Module_m4985 ();
extern "C" void ConstructorBuilder_ToString_m4986 ();
extern "C" void ConstructorBuilder_fixup_m4987 ();
extern "C" void ConstructorBuilder_get_next_table_index_m4988 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m4989 ();
extern "C" void ConstructorBuilder_not_supported_m4990 ();
extern "C" void ConstructorBuilder_not_created_m4991 ();
extern "C" void EnumBuilder_get_Assembly_m4992 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m4993 ();
extern "C" void EnumBuilder_get_BaseType_m4994 ();
extern "C" void EnumBuilder_get_DeclaringType_m4995 ();
extern "C" void EnumBuilder_get_FullName_m4996 ();
extern "C" void EnumBuilder_get_Module_m4997 ();
extern "C" void EnumBuilder_get_Name_m4998 ();
extern "C" void EnumBuilder_get_Namespace_m4999 ();
extern "C" void EnumBuilder_get_ReflectedType_m5000 ();
extern "C" void EnumBuilder_get_TypeHandle_m5001 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m5002 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m5003 ();
extern "C" void EnumBuilder_GetConstructorImpl_m5004 ();
extern "C" void EnumBuilder_GetConstructors_m5005 ();
extern "C" void EnumBuilder_GetCustomAttributes_m5006 ();
extern "C" void EnumBuilder_GetCustomAttributes_m5007 ();
extern "C" void EnumBuilder_GetElementType_m5008 ();
extern "C" void EnumBuilder_GetEvent_m5009 ();
extern "C" void EnumBuilder_GetField_m5010 ();
extern "C" void EnumBuilder_GetFields_m5011 ();
extern "C" void EnumBuilder_GetInterfaces_m5012 ();
extern "C" void EnumBuilder_GetMethodImpl_m5013 ();
extern "C" void EnumBuilder_GetMethods_m5014 ();
extern "C" void EnumBuilder_GetPropertyImpl_m5015 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m5016 ();
extern "C" void EnumBuilder_InvokeMember_m5017 ();
extern "C" void EnumBuilder_IsArrayImpl_m5018 ();
extern "C" void EnumBuilder_IsByRefImpl_m5019 ();
extern "C" void EnumBuilder_IsPointerImpl_m5020 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m5021 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m5022 ();
extern "C" void EnumBuilder_IsDefined_m5023 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m5024 ();
extern "C" void FieldBuilder_get_Attributes_m5025 ();
extern "C" void FieldBuilder_get_DeclaringType_m5026 ();
extern "C" void FieldBuilder_get_FieldHandle_m5027 ();
extern "C" void FieldBuilder_get_FieldType_m5028 ();
extern "C" void FieldBuilder_get_Name_m5029 ();
extern "C" void FieldBuilder_get_ReflectedType_m5030 ();
extern "C" void FieldBuilder_GetCustomAttributes_m5031 ();
extern "C" void FieldBuilder_GetCustomAttributes_m5032 ();
extern "C" void FieldBuilder_GetValue_m5033 ();
extern "C" void FieldBuilder_IsDefined_m5034 ();
extern "C" void FieldBuilder_GetFieldOffset_m5035 ();
extern "C" void FieldBuilder_SetValue_m5036 ();
extern "C" void FieldBuilder_get_UMarshal_m5037 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m5038 ();
extern "C" void FieldBuilder_get_Module_m5039 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m5040 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m5041 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m5042 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m5043 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m5044 ();
extern "C" void GenericTypeParameterBuilder_GetField_m5045 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m5046 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m5047 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m5048 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m5049 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m5050 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m5051 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m5052 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m5053 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m5054 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m5055 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m5056 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m5057 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m5058 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m5059 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m5060 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m5061 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m5062 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m5063 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m5064 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m5065 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m5066 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m5067 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m5068 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m5069 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m5070 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m5071 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m5072 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m5073 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m5074 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m5075 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m5076 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m5077 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m5078 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m5079 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m5080 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m5081 ();
extern "C" void GenericTypeParameterBuilder_ToString_m5082 ();
extern "C" void GenericTypeParameterBuilder_Equals_m5083 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m5084 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m5085 ();
extern "C" void ILGenerator__ctor_m5086 ();
extern "C" void ILGenerator__cctor_m5087 ();
extern "C" void ILGenerator_add_token_fixup_m5088 ();
extern "C" void ILGenerator_make_room_m5089 ();
extern "C" void ILGenerator_emit_int_m5090 ();
extern "C" void ILGenerator_ll_emit_m5091 ();
extern "C" void ILGenerator_Emit_m5092 ();
extern "C" void ILGenerator_Emit_m5093 ();
extern "C" void ILGenerator_label_fixup_m5094 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m5095 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m5096 ();
extern "C" void MethodBuilder_get_MethodHandle_m5097 ();
extern "C" void MethodBuilder_get_ReturnType_m5098 ();
extern "C" void MethodBuilder_get_ReflectedType_m5099 ();
extern "C" void MethodBuilder_get_DeclaringType_m5100 ();
extern "C" void MethodBuilder_get_Name_m5101 ();
extern "C" void MethodBuilder_get_Attributes_m5102 ();
extern "C" void MethodBuilder_get_CallingConvention_m5103 ();
extern "C" void MethodBuilder_GetBaseDefinition_m5104 ();
extern "C" void MethodBuilder_GetParameters_m5105 ();
extern "C" void MethodBuilder_GetParameterCount_m5106 ();
extern "C" void MethodBuilder_Invoke_m5107 ();
extern "C" void MethodBuilder_IsDefined_m5108 ();
extern "C" void MethodBuilder_GetCustomAttributes_m5109 ();
extern "C" void MethodBuilder_GetCustomAttributes_m5110 ();
extern "C" void MethodBuilder_check_override_m5111 ();
extern "C" void MethodBuilder_fixup_m5112 ();
extern "C" void MethodBuilder_ToString_m5113 ();
extern "C" void MethodBuilder_Equals_m5114 ();
extern "C" void MethodBuilder_GetHashCode_m5115 ();
extern "C" void MethodBuilder_get_next_table_index_m5116 ();
extern "C" void MethodBuilder_NotSupported_m5117 ();
extern "C" void MethodBuilder_MakeGenericMethod_m5118 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m5119 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m5120 ();
extern "C" void MethodBuilder_GetGenericArguments_m5121 ();
extern "C" void MethodBuilder_get_Module_m5122 ();
extern "C" void MethodToken__ctor_m5123 ();
extern "C" void MethodToken__cctor_m5124 ();
extern "C" void MethodToken_Equals_m5125 ();
extern "C" void MethodToken_GetHashCode_m5126 ();
extern "C" void MethodToken_get_Token_m5127 ();
extern "C" void ModuleBuilder__cctor_m5128 ();
extern "C" void ModuleBuilder_get_next_table_index_m5129 ();
extern "C" void ModuleBuilder_GetTypes_m5130 ();
extern "C" void ModuleBuilder_getToken_m5131 ();
extern "C" void ModuleBuilder_GetToken_m5132 ();
extern "C" void ModuleBuilder_RegisterToken_m5133 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m5134 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m5135 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m5136 ();
extern "C" void OpCode__ctor_m5137 ();
extern "C" void OpCode_GetHashCode_m5138 ();
extern "C" void OpCode_Equals_m5139 ();
extern "C" void OpCode_ToString_m5140 ();
extern "C" void OpCode_get_Name_m5141 ();
extern "C" void OpCode_get_Size_m5142 ();
extern "C" void OpCode_get_StackBehaviourPop_m5143 ();
extern "C" void OpCode_get_StackBehaviourPush_m5144 ();
extern "C" void OpCodeNames__cctor_m5145 ();
extern "C" void OpCodes__cctor_m5146 ();
extern "C" void ParameterBuilder_get_Attributes_m5147 ();
extern "C" void ParameterBuilder_get_Name_m5148 ();
extern "C" void ParameterBuilder_get_Position_m5149 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m5150 ();
extern "C" void TypeBuilder_setup_internal_class_m5151 ();
extern "C" void TypeBuilder_create_generic_class_m5152 ();
extern "C" void TypeBuilder_get_Assembly_m5153 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m5154 ();
extern "C" void TypeBuilder_get_BaseType_m5155 ();
extern "C" void TypeBuilder_get_DeclaringType_m5156 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m5157 ();
extern "C" void TypeBuilder_get_FullName_m5158 ();
extern "C" void TypeBuilder_get_Module_m5159 ();
extern "C" void TypeBuilder_get_Name_m5160 ();
extern "C" void TypeBuilder_get_Namespace_m5161 ();
extern "C" void TypeBuilder_get_ReflectedType_m5162 ();
extern "C" void TypeBuilder_GetConstructorImpl_m5163 ();
extern "C" void TypeBuilder_IsDefined_m5164 ();
extern "C" void TypeBuilder_GetCustomAttributes_m5165 ();
extern "C" void TypeBuilder_GetCustomAttributes_m5166 ();
extern "C" void TypeBuilder_DefineConstructor_m5167 ();
extern "C" void TypeBuilder_DefineConstructor_m5168 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m5169 ();
extern "C" void TypeBuilder_create_runtime_class_m5170 ();
extern "C" void TypeBuilder_is_nested_in_m5171 ();
extern "C" void TypeBuilder_has_ctor_method_m5172 ();
extern "C" void TypeBuilder_CreateType_m5173 ();
extern "C" void TypeBuilder_GetConstructors_m5174 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m5175 ();
extern "C" void TypeBuilder_GetElementType_m5176 ();
extern "C" void TypeBuilder_GetEvent_m5177 ();
extern "C" void TypeBuilder_GetField_m5178 ();
extern "C" void TypeBuilder_GetFields_m5179 ();
extern "C" void TypeBuilder_GetInterfaces_m5180 ();
extern "C" void TypeBuilder_GetMethodsByName_m5181 ();
extern "C" void TypeBuilder_GetMethods_m5182 ();
extern "C" void TypeBuilder_GetMethodImpl_m5183 ();
extern "C" void TypeBuilder_GetPropertyImpl_m5184 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m5185 ();
extern "C" void TypeBuilder_InvokeMember_m5186 ();
extern "C" void TypeBuilder_IsArrayImpl_m5187 ();
extern "C" void TypeBuilder_IsByRefImpl_m5188 ();
extern "C" void TypeBuilder_IsPointerImpl_m5189 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m5190 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m5191 ();
extern "C" void TypeBuilder_MakeGenericType_m5192 ();
extern "C" void TypeBuilder_get_TypeHandle_m5193 ();
extern "C" void TypeBuilder_SetParent_m5194 ();
extern "C" void TypeBuilder_get_next_table_index_m5195 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m5196 ();
extern "C" void TypeBuilder_get_is_created_m5197 ();
extern "C" void TypeBuilder_not_supported_m5198 ();
extern "C" void TypeBuilder_check_not_created_m5199 ();
extern "C" void TypeBuilder_check_created_m5200 ();
extern "C" void TypeBuilder_ToString_m5201 ();
extern "C" void TypeBuilder_IsAssignableFrom_m5202 ();
extern "C" void TypeBuilder_IsSubclassOf_m5203 ();
extern "C" void TypeBuilder_IsAssignableTo_m5204 ();
extern "C" void TypeBuilder_GetGenericArguments_m5205 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m5206 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m5207 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m5208 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m5209 ();
extern "C" void TypeBuilder_get_IsGenericType_m5210 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m5211 ();
extern "C" void AmbiguousMatchException__ctor_m5212 ();
extern "C" void AmbiguousMatchException__ctor_m5213 ();
extern "C" void AmbiguousMatchException__ctor_m5214 ();
extern "C" void ResolveEventHolder__ctor_m5215 ();
extern "C" void Assembly__ctor_m5216 ();
extern "C" void Assembly_get_code_base_m5217 ();
extern "C" void Assembly_get_fullname_m5218 ();
extern "C" void Assembly_get_location_m5219 ();
extern "C" void Assembly_GetCodeBase_m5220 ();
extern "C" void Assembly_get_FullName_m5221 ();
extern "C" void Assembly_get_Location_m5222 ();
extern "C" void Assembly_IsDefined_m5223 ();
extern "C" void Assembly_GetCustomAttributes_m5224 ();
extern "C" void Assembly_GetManifestResourceInternal_m5225 ();
extern "C" void Assembly_GetTypes_m5226 ();
extern "C" void Assembly_GetTypes_m5227 ();
extern "C" void Assembly_GetType_m5228 ();
extern "C" void Assembly_GetType_m5229 ();
extern "C" void Assembly_InternalGetType_m5230 ();
extern "C" void Assembly_GetType_m5231 ();
extern "C" void Assembly_FillName_m5232 ();
extern "C" void Assembly_GetName_m5233 ();
extern "C" void Assembly_GetName_m5234 ();
extern "C" void Assembly_UnprotectedGetName_m5235 ();
extern "C" void Assembly_ToString_m5236 ();
extern "C" void Assembly_Load_m5237 ();
extern "C" void Assembly_GetModule_m5238 ();
extern "C" void Assembly_GetModulesInternal_m5239 ();
extern "C" void Assembly_GetModules_m5240 ();
extern "C" void Assembly_GetExecutingAssembly_m5241 ();
extern "C" void AssemblyCompanyAttribute__ctor_m5242 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m5243 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m5244 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m5245 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m5246 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m5247 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m5248 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m5249 ();
extern "C" void AssemblyName__ctor_m5250 ();
extern "C" void AssemblyName__ctor_m5251 ();
extern "C" void AssemblyName_get_Name_m5252 ();
extern "C" void AssemblyName_get_Flags_m5253 ();
extern "C" void AssemblyName_get_FullName_m5254 ();
extern "C" void AssemblyName_get_Version_m5255 ();
extern "C" void AssemblyName_set_Version_m5256 ();
extern "C" void AssemblyName_ToString_m5257 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m5258 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m5259 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m5260 ();
extern "C" void AssemblyName_SetPublicKey_m5261 ();
extern "C" void AssemblyName_SetPublicKeyToken_m5262 ();
extern "C" void AssemblyName_GetObjectData_m5263 ();
extern "C" void AssemblyName_Clone_m5264 ();
extern "C" void AssemblyName_OnDeserialization_m5265 ();
extern "C" void AssemblyProductAttribute__ctor_m5266 ();
extern "C" void AssemblyTitleAttribute__ctor_m5267 ();
extern "C" void Default__ctor_m5268 ();
extern "C" void Default_BindToMethod_m5269 ();
extern "C" void Default_ReorderParameters_m5270 ();
extern "C" void Default_IsArrayAssignable_m5271 ();
extern "C" void Default_ChangeType_m5272 ();
extern "C" void Default_ReorderArgumentArray_m5273 ();
extern "C" void Default_check_type_m5274 ();
extern "C" void Default_check_arguments_m5275 ();
extern "C" void Default_SelectMethod_m5276 ();
extern "C" void Default_SelectMethod_m5277 ();
extern "C" void Default_GetBetterMethod_m5278 ();
extern "C" void Default_CompareCloserType_m5279 ();
extern "C" void Default_SelectProperty_m5280 ();
extern "C" void Default_check_arguments_with_score_m5281 ();
extern "C" void Default_check_type_with_score_m5282 ();
extern "C" void Binder__ctor_m5283 ();
extern "C" void Binder__cctor_m5284 ();
extern "C" void Binder_get_DefaultBinder_m5285 ();
extern "C" void Binder_ConvertArgs_m5286 ();
extern "C" void Binder_GetDerivedLevel_m5287 ();
extern "C" void Binder_FindMostDerivedMatch_m5288 ();
extern "C" void ConstructorInfo__ctor_m5289 ();
extern "C" void ConstructorInfo__cctor_m5290 ();
extern "C" void ConstructorInfo_get_MemberType_m5291 ();
extern "C" void ConstructorInfo_Invoke_m5292 ();
extern "C" void CustomAttributeData__ctor_m5293 ();
extern "C" void CustomAttributeData_get_Constructor_m5294 ();
extern "C" void CustomAttributeData_get_ConstructorArguments_m5295 ();
extern "C" void CustomAttributeData_get_NamedArguments_m5296 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m5297 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m5298 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m5299 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m5300 ();
extern "C" void CustomAttributeData_ToString_m5301 ();
extern "C" void CustomAttributeData_Equals_m5302 ();
extern "C" void CustomAttributeData_GetHashCode_m5303 ();
extern "C" void CustomAttributeNamedArgument_ToString_m5304 ();
extern "C" void CustomAttributeNamedArgument_Equals_m5305 ();
extern "C" void CustomAttributeNamedArgument_GetHashCode_m5306 ();
extern "C" void CustomAttributeTypedArgument_ToString_m5307 ();
extern "C" void CustomAttributeTypedArgument_Equals_m5308 ();
extern "C" void CustomAttributeTypedArgument_GetHashCode_m5309 ();
extern "C" void AddEventAdapter__ctor_m5310 ();
extern "C" void AddEventAdapter_Invoke_m5311 ();
extern "C" void AddEventAdapter_BeginInvoke_m5312 ();
extern "C" void AddEventAdapter_EndInvoke_m5313 ();
extern "C" void EventInfo__ctor_m5314 ();
extern "C" void EventInfo_get_EventHandlerType_m5315 ();
extern "C" void EventInfo_get_MemberType_m5316 ();
extern "C" void FieldInfo__ctor_m5317 ();
extern "C" void FieldInfo_get_MemberType_m5318 ();
extern "C" void FieldInfo_get_IsLiteral_m5319 ();
extern "C" void FieldInfo_get_IsStatic_m5320 ();
extern "C" void FieldInfo_get_IsNotSerialized_m5321 ();
extern "C" void FieldInfo_SetValue_m5322 ();
extern "C" void FieldInfo_internal_from_handle_type_m5323 ();
extern "C" void FieldInfo_GetFieldFromHandle_m5324 ();
extern "C" void FieldInfo_GetFieldOffset_m5325 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m5326 ();
extern "C" void FieldInfo_get_UMarshal_m5327 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m5328 ();
extern "C" void MemberInfoSerializationHolder__ctor_m5329 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m5330 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m5331 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m5332 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m5333 ();
extern "C" void MethodBase__ctor_m5334 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m5335 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m5336 ();
extern "C" void MethodBase_GetMethodFromHandle_m5337 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m5338 ();
extern "C" void MethodBase_GetParameterCount_m5339 ();
extern "C" void MethodBase_Invoke_m5340 ();
extern "C" void MethodBase_get_CallingConvention_m5341 ();
extern "C" void MethodBase_get_IsPublic_m5342 ();
extern "C" void MethodBase_get_IsStatic_m5343 ();
extern "C" void MethodBase_get_IsVirtual_m5344 ();
extern "C" void MethodBase_get_IsAbstract_m5345 ();
extern "C" void MethodBase_get_next_table_index_m5346 ();
extern "C" void MethodBase_GetGenericArguments_m5347 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m5348 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m5349 ();
extern "C" void MethodBase_get_IsGenericMethod_m5350 ();
extern "C" void MethodInfo__ctor_m5351 ();
extern "C" void MethodInfo_get_MemberType_m5352 ();
extern "C" void MethodInfo_get_ReturnType_m5353 ();
extern "C" void MethodInfo_MakeGenericMethod_m5354 ();
extern "C" void MethodInfo_GetGenericArguments_m5355 ();
extern "C" void MethodInfo_get_IsGenericMethod_m5356 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m5357 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m5358 ();
extern "C" void Missing__ctor_m5359 ();
extern "C" void Missing__cctor_m5360 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m5361 ();
extern "C" void Module__ctor_m5362 ();
extern "C" void Module__cctor_m5363 ();
extern "C" void Module_get_Assembly_m5364 ();
extern "C" void Module_get_ScopeName_m5365 ();
extern "C" void Module_GetCustomAttributes_m5366 ();
extern "C" void Module_GetObjectData_m5367 ();
extern "C" void Module_InternalGetTypes_m5368 ();
extern "C" void Module_GetTypes_m5369 ();
extern "C" void Module_IsDefined_m5370 ();
extern "C" void Module_IsResource_m5371 ();
extern "C" void Module_ToString_m5372 ();
extern "C" void Module_filter_by_type_name_m5373 ();
extern "C" void Module_filter_by_type_name_ignore_case_m5374 ();
extern "C" void MonoEventInfo_get_event_info_m5375 ();
extern "C" void MonoEventInfo_GetEventInfo_m5376 ();
extern "C" void MonoEvent__ctor_m5377 ();
extern "C" void MonoEvent_get_Attributes_m5378 ();
extern "C" void MonoEvent_GetAddMethod_m5379 ();
extern "C" void MonoEvent_get_DeclaringType_m5380 ();
extern "C" void MonoEvent_get_ReflectedType_m5381 ();
extern "C" void MonoEvent_get_Name_m5382 ();
extern "C" void MonoEvent_ToString_m5383 ();
extern "C" void MonoEvent_IsDefined_m5384 ();
extern "C" void MonoEvent_GetCustomAttributes_m5385 ();
extern "C" void MonoEvent_GetCustomAttributes_m5386 ();
extern "C" void MonoEvent_GetObjectData_m5387 ();
extern "C" void MonoField__ctor_m5388 ();
extern "C" void MonoField_get_Attributes_m5389 ();
extern "C" void MonoField_get_FieldHandle_m5390 ();
extern "C" void MonoField_get_FieldType_m5391 ();
extern "C" void MonoField_GetParentType_m5392 ();
extern "C" void MonoField_get_ReflectedType_m5393 ();
extern "C" void MonoField_get_DeclaringType_m5394 ();
extern "C" void MonoField_get_Name_m5395 ();
extern "C" void MonoField_IsDefined_m5396 ();
extern "C" void MonoField_GetCustomAttributes_m5397 ();
extern "C" void MonoField_GetCustomAttributes_m5398 ();
extern "C" void MonoField_GetFieldOffset_m5399 ();
extern "C" void MonoField_GetValueInternal_m5400 ();
extern "C" void MonoField_GetValue_m5401 ();
extern "C" void MonoField_ToString_m5402 ();
extern "C" void MonoField_SetValueInternal_m5403 ();
extern "C" void MonoField_SetValue_m5404 ();
extern "C" void MonoField_GetObjectData_m5405 ();
extern "C" void MonoField_CheckGeneric_m5406 ();
extern "C" void MonoGenericMethod__ctor_m5407 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m5408 ();
extern "C" void MonoGenericCMethod__ctor_m5409 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m5410 ();
extern "C" void MonoMethodInfo_get_method_info_m5411 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m5412 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m5413 ();
extern "C" void MonoMethodInfo_GetReturnType_m5414 ();
extern "C" void MonoMethodInfo_GetAttributes_m5415 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m5416 ();
extern "C" void MonoMethodInfo_get_parameter_info_m5417 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m5418 ();
extern "C" void MonoMethod__ctor_m5419 ();
extern "C" void MonoMethod_get_name_m5420 ();
extern "C" void MonoMethod_get_base_definition_m5421 ();
extern "C" void MonoMethod_GetBaseDefinition_m5422 ();
extern "C" void MonoMethod_get_ReturnType_m5423 ();
extern "C" void MonoMethod_GetParameters_m5424 ();
extern "C" void MonoMethod_InternalInvoke_m5425 ();
extern "C" void MonoMethod_Invoke_m5426 ();
extern "C" void MonoMethod_get_MethodHandle_m5427 ();
extern "C" void MonoMethod_get_Attributes_m5428 ();
extern "C" void MonoMethod_get_CallingConvention_m5429 ();
extern "C" void MonoMethod_get_ReflectedType_m5430 ();
extern "C" void MonoMethod_get_DeclaringType_m5431 ();
extern "C" void MonoMethod_get_Name_m5432 ();
extern "C" void MonoMethod_IsDefined_m5433 ();
extern "C" void MonoMethod_GetCustomAttributes_m5434 ();
extern "C" void MonoMethod_GetCustomAttributes_m5435 ();
extern "C" void MonoMethod_GetDllImportAttribute_m5436 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m5437 ();
extern "C" void MonoMethod_ShouldPrintFullName_m5438 ();
extern "C" void MonoMethod_ToString_m5439 ();
extern "C" void MonoMethod_GetObjectData_m5440 ();
extern "C" void MonoMethod_MakeGenericMethod_m5441 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m5442 ();
extern "C" void MonoMethod_GetGenericArguments_m5443 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m5444 ();
extern "C" void MonoMethod_get_IsGenericMethod_m5445 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m5446 ();
extern "C" void MonoCMethod__ctor_m5447 ();
extern "C" void MonoCMethod_GetParameters_m5448 ();
extern "C" void MonoCMethod_InternalInvoke_m5449 ();
extern "C" void MonoCMethod_Invoke_m5450 ();
extern "C" void MonoCMethod_Invoke_m5451 ();
extern "C" void MonoCMethod_get_MethodHandle_m5452 ();
extern "C" void MonoCMethod_get_Attributes_m5453 ();
extern "C" void MonoCMethod_get_CallingConvention_m5454 ();
extern "C" void MonoCMethod_get_ReflectedType_m5455 ();
extern "C" void MonoCMethod_get_DeclaringType_m5456 ();
extern "C" void MonoCMethod_get_Name_m5457 ();
extern "C" void MonoCMethod_IsDefined_m5458 ();
extern "C" void MonoCMethod_GetCustomAttributes_m5459 ();
extern "C" void MonoCMethod_GetCustomAttributes_m5460 ();
extern "C" void MonoCMethod_ToString_m5461 ();
extern "C" void MonoCMethod_GetObjectData_m5462 ();
extern "C" void MonoPropertyInfo_get_property_info_m5463 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m5464 ();
extern "C" void GetterAdapter__ctor_m5465 ();
extern "C" void GetterAdapter_Invoke_m5466 ();
extern "C" void GetterAdapter_BeginInvoke_m5467 ();
extern "C" void GetterAdapter_EndInvoke_m5468 ();
extern "C" void MonoProperty__ctor_m5469 ();
extern "C" void MonoProperty_CachePropertyInfo_m5470 ();
extern "C" void MonoProperty_get_Attributes_m5471 ();
extern "C" void MonoProperty_get_CanRead_m5472 ();
extern "C" void MonoProperty_get_CanWrite_m5473 ();
extern "C" void MonoProperty_get_PropertyType_m5474 ();
extern "C" void MonoProperty_get_ReflectedType_m5475 ();
extern "C" void MonoProperty_get_DeclaringType_m5476 ();
extern "C" void MonoProperty_get_Name_m5477 ();
extern "C" void MonoProperty_GetAccessors_m5478 ();
extern "C" void MonoProperty_GetGetMethod_m5479 ();
extern "C" void MonoProperty_GetIndexParameters_m5480 ();
extern "C" void MonoProperty_GetSetMethod_m5481 ();
extern "C" void MonoProperty_IsDefined_m5482 ();
extern "C" void MonoProperty_GetCustomAttributes_m5483 ();
extern "C" void MonoProperty_GetCustomAttributes_m5484 ();
extern "C" void MonoProperty_CreateGetterDelegate_m5485 ();
extern "C" void MonoProperty_GetValue_m5486 ();
extern "C" void MonoProperty_GetValue_m5487 ();
extern "C" void MonoProperty_SetValue_m5488 ();
extern "C" void MonoProperty_ToString_m5489 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m5490 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m5491 ();
extern "C" void MonoProperty_GetObjectData_m5492 ();
extern "C" void ParameterInfo__ctor_m5493 ();
extern "C" void ParameterInfo__ctor_m5494 ();
extern "C" void ParameterInfo__ctor_m5495 ();
extern "C" void ParameterInfo_ToString_m5496 ();
extern "C" void ParameterInfo_get_ParameterType_m5497 ();
extern "C" void ParameterInfo_get_Attributes_m5498 ();
extern "C" void ParameterInfo_get_IsIn_m5499 ();
extern "C" void ParameterInfo_get_IsOptional_m5500 ();
extern "C" void ParameterInfo_get_IsOut_m5501 ();
extern "C" void ParameterInfo_get_IsRetval_m5502 ();
extern "C" void ParameterInfo_get_Member_m5503 ();
extern "C" void ParameterInfo_get_Name_m5504 ();
extern "C" void ParameterInfo_get_Position_m5505 ();
extern "C" void ParameterInfo_GetCustomAttributes_m5506 ();
extern "C" void ParameterInfo_IsDefined_m5507 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m5508 ();
extern "C" void Pointer__ctor_m5509 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m5510 ();
extern "C" void PropertyInfo__ctor_m5511 ();
extern "C" void PropertyInfo_get_MemberType_m5512 ();
extern "C" void PropertyInfo_GetValue_m5513 ();
extern "C" void PropertyInfo_SetValue_m5514 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m5515 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m5516 ();
extern "C" void StrongNameKeyPair__ctor_m5517 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m5518 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5519 ();
extern "C" void TargetException__ctor_m5520 ();
extern "C" void TargetException__ctor_m5521 ();
extern "C" void TargetException__ctor_m5522 ();
extern "C" void TargetInvocationException__ctor_m5523 ();
extern "C" void TargetInvocationException__ctor_m5524 ();
extern "C" void TargetParameterCountException__ctor_m5525 ();
extern "C" void TargetParameterCountException__ctor_m5526 ();
extern "C" void TargetParameterCountException__ctor_m5527 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m5528 ();
extern "C" void ResourceManager__ctor_m5529 ();
extern "C" void ResourceManager__cctor_m5530 ();
extern "C" void ResourceInfo__ctor_m5531 ();
extern "C" void ResourceCacheItem__ctor_m5532 ();
extern "C" void ResourceEnumerator__ctor_m5533 ();
extern "C" void ResourceEnumerator_get_Entry_m5534 ();
extern "C" void ResourceEnumerator_get_Key_m5535 ();
extern "C" void ResourceEnumerator_get_Value_m5536 ();
extern "C" void ResourceEnumerator_get_Current_m5537 ();
extern "C" void ResourceEnumerator_MoveNext_m5538 ();
extern "C" void ResourceEnumerator_Reset_m5539 ();
extern "C" void ResourceEnumerator_FillCache_m5540 ();
extern "C" void ResourceReader__ctor_m5541 ();
extern "C" void ResourceReader__ctor_m5542 ();
extern "C" void ResourceReader_System_Collections_IEnumerable_GetEnumerator_m5543 ();
extern "C" void ResourceReader_System_IDisposable_Dispose_m5544 ();
extern "C" void ResourceReader_ReadHeaders_m5545 ();
extern "C" void ResourceReader_CreateResourceInfo_m5546 ();
extern "C" void ResourceReader_Read7BitEncodedInt_m5547 ();
extern "C" void ResourceReader_ReadValueVer2_m5548 ();
extern "C" void ResourceReader_ReadValueVer1_m5549 ();
extern "C" void ResourceReader_ReadNonPredefinedValue_m5550 ();
extern "C" void ResourceReader_LoadResourceValues_m5551 ();
extern "C" void ResourceReader_Close_m5552 ();
extern "C" void ResourceReader_GetEnumerator_m5553 ();
extern "C" void ResourceReader_Dispose_m5554 ();
extern "C" void ResourceSet__ctor_m5555 ();
extern "C" void ResourceSet__ctor_m5556 ();
extern "C" void ResourceSet__ctor_m5557 ();
extern "C" void ResourceSet__ctor_m5558 ();
extern "C" void ResourceSet_System_Collections_IEnumerable_GetEnumerator_m5559 ();
extern "C" void ResourceSet_Dispose_m5560 ();
extern "C" void ResourceSet_Dispose_m5561 ();
extern "C" void ResourceSet_GetEnumerator_m5562 ();
extern "C" void ResourceSet_GetObjectInternal_m5563 ();
extern "C" void ResourceSet_GetObject_m5564 ();
extern "C" void ResourceSet_GetObject_m5565 ();
extern "C" void ResourceSet_ReadResources_m5566 ();
extern "C" void RuntimeResourceSet__ctor_m5567 ();
extern "C" void RuntimeResourceSet__ctor_m5568 ();
extern "C" void RuntimeResourceSet__ctor_m5569 ();
extern "C" void RuntimeResourceSet_GetObject_m5570 ();
extern "C" void RuntimeResourceSet_GetObject_m5571 ();
extern "C" void RuntimeResourceSet_CloneDisposableObjectIfPossible_m5572 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m5573 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m5574 ();
extern "C" void DefaultDependencyAttribute__ctor_m5575 ();
extern "C" void StringFreezingAttribute__ctor_m5576 ();
extern "C" void CriticalFinalizerObject__ctor_m5577 ();
extern "C" void CriticalFinalizerObject_Finalize_m5578 ();
extern "C" void ReliabilityContractAttribute__ctor_m5579 ();
extern "C" void ClassInterfaceAttribute__ctor_m5580 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m5581 ();
extern "C" void DispIdAttribute__ctor_m5582 ();
extern "C" void GCHandle__ctor_m5583 ();
extern "C" void GCHandle_get_IsAllocated_m5584 ();
extern "C" void GCHandle_get_Target_m5585 ();
extern "C" void GCHandle_Alloc_m5586 ();
extern "C" void GCHandle_Free_m5587 ();
extern "C" void GCHandle_GetTarget_m5588 ();
extern "C" void GCHandle_GetTargetHandle_m5589 ();
extern "C" void GCHandle_FreeHandle_m5590 ();
extern "C" void GCHandle_Equals_m5591 ();
extern "C" void GCHandle_GetHashCode_m5592 ();
extern "C" void InterfaceTypeAttribute__ctor_m5593 ();
extern "C" void Marshal__cctor_m5594 ();
extern "C" void Marshal_copy_from_unmanaged_m5595 ();
extern "C" void Marshal_Copy_m5596 ();
extern "C" void Marshal_Copy_m5597 ();
extern "C" void Marshal_ReadByte_m5598 ();
extern "C" void Marshal_WriteByte_m5599 ();
extern "C" void MarshalDirectiveException__ctor_m5600 ();
extern "C" void MarshalDirectiveException__ctor_m5601 ();
extern "C" void PreserveSigAttribute__ctor_m5602 ();
extern "C" void SafeHandle__ctor_m5603 ();
extern "C" void SafeHandle_Close_m5604 ();
extern "C" void SafeHandle_DangerousAddRef_m5605 ();
extern "C" void SafeHandle_DangerousGetHandle_m5606 ();
extern "C" void SafeHandle_DangerousRelease_m5607 ();
extern "C" void SafeHandle_Dispose_m5608 ();
extern "C" void SafeHandle_Dispose_m5609 ();
extern "C" void SafeHandle_SetHandle_m5610 ();
extern "C" void SafeHandle_Finalize_m5611 ();
extern "C" void TypeLibImportClassAttribute__ctor_m5612 ();
extern "C" void TypeLibVersionAttribute__ctor_m5613 ();
extern "C" void ActivationServices_get_ConstructionActivator_m5614 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m5615 ();
extern "C" void ActivationServices_CreateConstructionCall_m5616 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m5617 ();
extern "C" void ActivationServices_EnableProxyActivation_m5618 ();
extern "C" void AppDomainLevelActivator__ctor_m5619 ();
extern "C" void ConstructionLevelActivator__ctor_m5620 ();
extern "C" void ContextLevelActivator__ctor_m5621 ();
extern "C" void UrlAttribute_get_UrlValue_m5622 ();
extern "C" void UrlAttribute_Equals_m5623 ();
extern "C" void UrlAttribute_GetHashCode_m5624 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m5625 ();
extern "C" void UrlAttribute_IsContextOK_m5626 ();
extern "C" void ChannelInfo__ctor_m5627 ();
extern "C" void ChannelInfo_get_ChannelData_m5628 ();
extern "C" void ChannelServices__cctor_m5629 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m5630 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m5631 ();
extern "C" void ChannelServices_RegisterChannel_m5632 ();
extern "C" void ChannelServices_RegisterChannel_m5633 ();
extern "C" void ChannelServices_RegisterChannelConfig_m5634 ();
extern "C" void ChannelServices_CreateProvider_m5635 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m5636 ();
extern "C" void CrossAppDomainData__ctor_m5637 ();
extern "C" void CrossAppDomainData_get_DomainID_m5638 ();
extern "C" void CrossAppDomainData_get_ProcessID_m5639 ();
extern "C" void CrossAppDomainChannel__ctor_m5640 ();
extern "C" void CrossAppDomainChannel__cctor_m5641 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m5642 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m5643 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m5644 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m5645 ();
extern "C" void CrossAppDomainChannel_StartListening_m5646 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m5647 ();
extern "C" void CrossAppDomainSink__ctor_m5648 ();
extern "C" void CrossAppDomainSink__cctor_m5649 ();
extern "C" void CrossAppDomainSink_GetSink_m5650 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m5651 ();
extern "C" void SinkProviderData__ctor_m5652 ();
extern "C" void SinkProviderData_get_Children_m5653 ();
extern "C" void SinkProviderData_get_Properties_m5654 ();
extern "C" void Context__ctor_m5655 ();
extern "C" void Context__cctor_m5656 ();
extern "C" void Context_Finalize_m5657 ();
extern "C" void Context_get_DefaultContext_m5658 ();
extern "C" void Context_get_ContextID_m5659 ();
extern "C" void Context_get_ContextProperties_m5660 ();
extern "C" void Context_get_IsDefaultContext_m5661 ();
extern "C" void Context_get_NeedsContextSink_m5662 ();
extern "C" void Context_RegisterDynamicProperty_m5663 ();
extern "C" void Context_UnregisterDynamicProperty_m5664 ();
extern "C" void Context_GetDynamicPropertyCollection_m5665 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m5666 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m5667 ();
extern "C" void Context_NotifyDynamicSinks_m5668 ();
extern "C" void Context_get_HasDynamicSinks_m5669 ();
extern "C" void Context_get_HasExitSinks_m5670 ();
extern "C" void Context_GetProperty_m5671 ();
extern "C" void Context_SetProperty_m5672 ();
extern "C" void Context_Freeze_m5673 ();
extern "C" void Context_ToString_m5674 ();
extern "C" void Context_GetServerContextSinkChain_m5675 ();
extern "C" void Context_GetClientContextSinkChain_m5676 ();
extern "C" void Context_CreateServerObjectSinkChain_m5677 ();
extern "C" void Context_CreateEnvoySink_m5678 ();
extern "C" void Context_SwitchToContext_m5679 ();
extern "C" void Context_CreateNewContext_m5680 ();
extern "C" void Context_DoCallBack_m5681 ();
extern "C" void Context_AllocateDataSlot_m5682 ();
extern "C" void Context_AllocateNamedDataSlot_m5683 ();
extern "C" void Context_FreeNamedDataSlot_m5684 ();
extern "C" void Context_GetData_m5685 ();
extern "C" void Context_GetNamedDataSlot_m5686 ();
extern "C" void Context_SetData_m5687 ();
extern "C" void DynamicPropertyReg__ctor_m5688 ();
extern "C" void DynamicPropertyCollection__ctor_m5689 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m5690 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m5691 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m5692 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m5693 ();
extern "C" void DynamicPropertyCollection_FindProperty_m5694 ();
extern "C" void ContextCallbackObject__ctor_m5695 ();
extern "C" void ContextCallbackObject_DoCallBack_m5696 ();
extern "C" void ContextAttribute__ctor_m5697 ();
extern "C" void ContextAttribute_get_Name_m5698 ();
extern "C" void ContextAttribute_Equals_m5699 ();
extern "C" void ContextAttribute_Freeze_m5700 ();
extern "C" void ContextAttribute_GetHashCode_m5701 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m5702 ();
extern "C" void ContextAttribute_IsContextOK_m5703 ();
extern "C" void ContextAttribute_IsNewContextOK_m5704 ();
extern "C" void CrossContextChannel__ctor_m5705 ();
extern "C" void SynchronizationAttribute__ctor_m5706 ();
extern "C" void SynchronizationAttribute__ctor_m5707 ();
extern "C" void SynchronizationAttribute_set_Locked_m5708 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m5709 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m5710 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m5711 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m5712 ();
extern "C" void SynchronizationAttribute_IsContextOK_m5713 ();
extern "C" void SynchronizationAttribute_ExitContext_m5714 ();
extern "C" void SynchronizationAttribute_EnterContext_m5715 ();
extern "C" void SynchronizedClientContextSink__ctor_m5716 ();
extern "C" void SynchronizedServerContextSink__ctor_m5717 ();
extern "C" void LeaseManager__ctor_m5718 ();
extern "C" void LeaseManager_SetPollTime_m5719 ();
extern "C" void LeaseSink__ctor_m5720 ();
extern "C" void LifetimeServices__cctor_m5721 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m5722 ();
extern "C" void LifetimeServices_set_LeaseTime_m5723 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m5724 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m5725 ();
extern "C" void ArgInfo__ctor_m5726 ();
extern "C" void ArgInfo_GetInOutArgs_m5727 ();
extern "C" void AsyncResult__ctor_m5728 ();
extern "C" void AsyncResult_get_AsyncState_m5729 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m5730 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m5731 ();
extern "C" void AsyncResult_get_IsCompleted_m5732 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m5733 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m5734 ();
extern "C" void AsyncResult_get_AsyncDelegate_m5735 ();
extern "C" void AsyncResult_get_NextSink_m5736 ();
extern "C" void AsyncResult_AsyncProcessMessage_m5737 ();
extern "C" void AsyncResult_GetReplyMessage_m5738 ();
extern "C" void AsyncResult_SetMessageCtrl_m5739 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m5740 ();
extern "C" void AsyncResult_EndInvoke_m5741 ();
extern "C" void AsyncResult_SyncProcessMessage_m5742 ();
extern "C" void AsyncResult_get_CallMessage_m5743 ();
extern "C" void AsyncResult_set_CallMessage_m5744 ();
extern "C" void ClientContextTerminatorSink__ctor_m5745 ();
extern "C" void ConstructionCall__ctor_m5746 ();
extern "C" void ConstructionCall__ctor_m5747 ();
extern "C" void ConstructionCall_InitDictionary_m5748 ();
extern "C" void ConstructionCall_set_IsContextOk_m5749 ();
extern "C" void ConstructionCall_get_ActivationType_m5750 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m5751 ();
extern "C" void ConstructionCall_get_Activator_m5752 ();
extern "C" void ConstructionCall_set_Activator_m5753 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m5754 ();
extern "C" void ConstructionCall_SetActivationAttributes_m5755 ();
extern "C" void ConstructionCall_get_ContextProperties_m5756 ();
extern "C" void ConstructionCall_InitMethodProperty_m5757 ();
extern "C" void ConstructionCall_GetObjectData_m5758 ();
extern "C" void ConstructionCall_get_Properties_m5759 ();
extern "C" void ConstructionCallDictionary__ctor_m5760 ();
extern "C" void ConstructionCallDictionary__cctor_m5761 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m5762 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m5763 ();
extern "C" void EnvoyTerminatorSink__ctor_m5764 ();
extern "C" void EnvoyTerminatorSink__cctor_m5765 ();
extern "C" void Header__ctor_m5766 ();
extern "C" void Header__ctor_m5767 ();
extern "C" void Header__ctor_m5768 ();
extern "C" void LogicalCallContext__ctor_m5769 ();
extern "C" void LogicalCallContext__ctor_m5770 ();
extern "C" void LogicalCallContext_GetObjectData_m5771 ();
extern "C" void LogicalCallContext_SetData_m5772 ();
extern "C" void LogicalCallContext_Clone_m5773 ();
extern "C" void CallContextRemotingData__ctor_m5774 ();
extern "C" void CallContextRemotingData_Clone_m5775 ();
extern "C" void MethodCall__ctor_m5776 ();
extern "C" void MethodCall__ctor_m5777 ();
extern "C" void MethodCall__ctor_m5778 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m5779 ();
extern "C" void MethodCall_InitMethodProperty_m5780 ();
extern "C" void MethodCall_GetObjectData_m5781 ();
extern "C" void MethodCall_get_Args_m5782 ();
extern "C" void MethodCall_get_LogicalCallContext_m5783 ();
extern "C" void MethodCall_get_MethodBase_m5784 ();
extern "C" void MethodCall_get_MethodName_m5785 ();
extern "C" void MethodCall_get_MethodSignature_m5786 ();
extern "C" void MethodCall_get_Properties_m5787 ();
extern "C" void MethodCall_InitDictionary_m5788 ();
extern "C" void MethodCall_get_TypeName_m5789 ();
extern "C" void MethodCall_get_Uri_m5790 ();
extern "C" void MethodCall_set_Uri_m5791 ();
extern "C" void MethodCall_Init_m5792 ();
extern "C" void MethodCall_ResolveMethod_m5793 ();
extern "C" void MethodCall_CastTo_m5794 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m5795 ();
extern "C" void MethodCall_get_GenericArguments_m5796 ();
extern "C" void MethodCallDictionary__ctor_m5797 ();
extern "C" void MethodCallDictionary__cctor_m5798 ();
extern "C" void DictionaryEnumerator__ctor_m5799 ();
extern "C" void DictionaryEnumerator_get_Current_m5800 ();
extern "C" void DictionaryEnumerator_MoveNext_m5801 ();
extern "C" void DictionaryEnumerator_Reset_m5802 ();
extern "C" void DictionaryEnumerator_get_Entry_m5803 ();
extern "C" void DictionaryEnumerator_get_Key_m5804 ();
extern "C" void DictionaryEnumerator_get_Value_m5805 ();
extern "C" void MethodDictionary__ctor_m5806 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m5807 ();
extern "C" void MethodDictionary_set_MethodKeys_m5808 ();
extern "C" void MethodDictionary_AllocInternalProperties_m5809 ();
extern "C" void MethodDictionary_GetInternalProperties_m5810 ();
extern "C" void MethodDictionary_IsOverridenKey_m5811 ();
extern "C" void MethodDictionary_get_Item_m5812 ();
extern "C" void MethodDictionary_set_Item_m5813 ();
extern "C" void MethodDictionary_GetMethodProperty_m5814 ();
extern "C" void MethodDictionary_SetMethodProperty_m5815 ();
extern "C" void MethodDictionary_get_Values_m5816 ();
extern "C" void MethodDictionary_Add_m5817 ();
extern "C" void MethodDictionary_Contains_m5818 ();
extern "C" void MethodDictionary_Remove_m5819 ();
extern "C" void MethodDictionary_get_Count_m5820 ();
extern "C" void MethodDictionary_get_SyncRoot_m5821 ();
extern "C" void MethodDictionary_CopyTo_m5822 ();
extern "C" void MethodDictionary_GetEnumerator_m5823 ();
extern "C" void MethodReturnDictionary__ctor_m5824 ();
extern "C" void MethodReturnDictionary__cctor_m5825 ();
extern "C" void MonoMethodMessage_get_Args_m5826 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m5827 ();
extern "C" void MonoMethodMessage_get_MethodBase_m5828 ();
extern "C" void MonoMethodMessage_get_MethodName_m5829 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m5830 ();
extern "C" void MonoMethodMessage_get_TypeName_m5831 ();
extern "C" void MonoMethodMessage_get_Uri_m5832 ();
extern "C" void MonoMethodMessage_set_Uri_m5833 ();
extern "C" void MonoMethodMessage_get_Exception_m5834 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m5835 ();
extern "C" void MonoMethodMessage_get_OutArgs_m5836 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m5837 ();
extern "C" void RemotingSurrogate__ctor_m5838 ();
extern "C" void RemotingSurrogate_SetObjectData_m5839 ();
extern "C" void ObjRefSurrogate__ctor_m5840 ();
extern "C" void ObjRefSurrogate_SetObjectData_m5841 ();
extern "C" void RemotingSurrogateSelector__ctor_m5842 ();
extern "C" void RemotingSurrogateSelector__cctor_m5843 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m5844 ();
extern "C" void ReturnMessage__ctor_m5845 ();
extern "C" void ReturnMessage__ctor_m5846 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m5847 ();
extern "C" void ReturnMessage_get_Args_m5848 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m5849 ();
extern "C" void ReturnMessage_get_MethodBase_m5850 ();
extern "C" void ReturnMessage_get_MethodName_m5851 ();
extern "C" void ReturnMessage_get_MethodSignature_m5852 ();
extern "C" void ReturnMessage_get_Properties_m5853 ();
extern "C" void ReturnMessage_get_TypeName_m5854 ();
extern "C" void ReturnMessage_get_Uri_m5855 ();
extern "C" void ReturnMessage_set_Uri_m5856 ();
extern "C" void ReturnMessage_get_Exception_m5857 ();
extern "C" void ReturnMessage_get_OutArgs_m5858 ();
extern "C" void ReturnMessage_get_ReturnValue_m5859 ();
extern "C" void ServerContextTerminatorSink__ctor_m5860 ();
extern "C" void ServerObjectTerminatorSink__ctor_m5861 ();
extern "C" void StackBuilderSink__ctor_m5862 ();
extern "C" void SoapAttribute__ctor_m5863 ();
extern "C" void SoapAttribute_get_UseAttribute_m5864 ();
extern "C" void SoapAttribute_get_XmlNamespace_m5865 ();
extern "C" void SoapAttribute_SetReflectionObject_m5866 ();
extern "C" void SoapFieldAttribute__ctor_m5867 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m5868 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m5869 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m5870 ();
extern "C" void SoapMethodAttribute__ctor_m5871 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m5872 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m5873 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m5874 ();
extern "C" void SoapParameterAttribute__ctor_m5875 ();
extern "C" void SoapTypeAttribute__ctor_m5876 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m5877 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m5878 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m5879 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m5880 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m5881 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m5882 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m5883 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m5884 ();
extern "C" void ProxyAttribute_CreateInstance_m5885 ();
extern "C" void ProxyAttribute_CreateProxy_m5886 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m5887 ();
extern "C" void ProxyAttribute_IsContextOK_m5888 ();
extern "C" void RealProxy__ctor_m5889 ();
extern "C" void RealProxy__ctor_m5890 ();
extern "C" void RealProxy__ctor_m5891 ();
extern "C" void RealProxy_InternalGetProxyType_m5892 ();
extern "C" void RealProxy_GetProxiedType_m5893 ();
extern "C" void RealProxy_get_ObjectIdentity_m5894 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m5895 ();
extern "C" void RealProxy_GetTransparentProxy_m5896 ();
extern "C" void RealProxy_SetTargetDomain_m5897 ();
extern "C" void RemotingProxy__ctor_m5898 ();
extern "C" void RemotingProxy__ctor_m5899 ();
extern "C" void RemotingProxy__cctor_m5900 ();
extern "C" void RemotingProxy_get_TypeName_m5901 ();
extern "C" void RemotingProxy_Finalize_m5902 ();
extern "C" void TrackingServices__cctor_m5903 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m5904 ();
extern "C" void ActivatedClientTypeEntry__ctor_m5905 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m5906 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m5907 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m5908 ();
extern "C" void ActivatedClientTypeEntry_ToString_m5909 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m5910 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m5911 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m5912 ();
extern "C" void EnvoyInfo__ctor_m5913 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m5914 ();
extern "C" void Identity__ctor_m5915 ();
extern "C" void Identity_get_ChannelSink_m5916 ();
extern "C" void Identity_set_ChannelSink_m5917 ();
extern "C" void Identity_get_ObjectUri_m5918 ();
extern "C" void Identity_get_Disposed_m5919 ();
extern "C" void Identity_set_Disposed_m5920 ();
extern "C" void Identity_get_ClientDynamicProperties_m5921 ();
extern "C" void Identity_get_ServerDynamicProperties_m5922 ();
extern "C" void ClientIdentity__ctor_m5923 ();
extern "C" void ClientIdentity_get_ClientProxy_m5924 ();
extern "C" void ClientIdentity_set_ClientProxy_m5925 ();
extern "C" void ClientIdentity_CreateObjRef_m5926 ();
extern "C" void ClientIdentity_get_TargetUri_m5927 ();
extern "C" void InternalRemotingServices__cctor_m5928 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m5929 ();
extern "C" void ObjRef__ctor_m5930 ();
extern "C" void ObjRef__ctor_m5931 ();
extern "C" void ObjRef__cctor_m5932 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m5933 ();
extern "C" void ObjRef_get_ChannelInfo_m5934 ();
extern "C" void ObjRef_get_EnvoyInfo_m5935 ();
extern "C" void ObjRef_set_EnvoyInfo_m5936 ();
extern "C" void ObjRef_get_TypeInfo_m5937 ();
extern "C" void ObjRef_set_TypeInfo_m5938 ();
extern "C" void ObjRef_get_URI_m5939 ();
extern "C" void ObjRef_set_URI_m5940 ();
extern "C" void ObjRef_GetObjectData_m5941 ();
extern "C" void ObjRef_GetRealObject_m5942 ();
extern "C" void ObjRef_UpdateChannelInfo_m5943 ();
extern "C" void ObjRef_get_ServerType_m5944 ();
extern "C" void RemotingConfiguration__cctor_m5945 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m5946 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m5947 ();
extern "C" void RemotingConfiguration_get_ProcessId_m5948 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m5949 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m5950 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m5951 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m5952 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m5953 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m5954 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m5955 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m5956 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m5957 ();
extern "C" void RemotingConfiguration_RegisterChannels_m5958 ();
extern "C" void RemotingConfiguration_RegisterTypes_m5959 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m5960 ();
extern "C" void ConfigHandler__ctor_m5961 ();
extern "C" void ConfigHandler_ValidatePath_m5962 ();
extern "C" void ConfigHandler_CheckPath_m5963 ();
extern "C" void ConfigHandler_OnStartParsing_m5964 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m5965 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m5966 ();
extern "C" void ConfigHandler_OnStartElement_m5967 ();
extern "C" void ConfigHandler_ParseElement_m5968 ();
extern "C" void ConfigHandler_OnEndElement_m5969 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m5970 ();
extern "C" void ConfigHandler_ReadLifetine_m5971 ();
extern "C" void ConfigHandler_ParseTime_m5972 ();
extern "C" void ConfigHandler_ReadChannel_m5973 ();
extern "C" void ConfigHandler_ReadProvider_m5974 ();
extern "C" void ConfigHandler_ReadClientActivated_m5975 ();
extern "C" void ConfigHandler_ReadServiceActivated_m5976 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m5977 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m5978 ();
extern "C" void ConfigHandler_ReadInteropXml_m5979 ();
extern "C" void ConfigHandler_ReadPreload_m5980 ();
extern "C" void ConfigHandler_GetNotNull_m5981 ();
extern "C" void ConfigHandler_ExtractAssembly_m5982 ();
extern "C" void ConfigHandler_OnChars_m5983 ();
extern "C" void ConfigHandler_OnEndParsing_m5984 ();
extern "C" void ChannelData__ctor_m5985 ();
extern "C" void ChannelData_get_ServerProviders_m5986 ();
extern "C" void ChannelData_get_ClientProviders_m5987 ();
extern "C" void ChannelData_get_CustomProperties_m5988 ();
extern "C" void ChannelData_CopyFrom_m5989 ();
extern "C" void ProviderData__ctor_m5990 ();
extern "C" void ProviderData_CopyFrom_m5991 ();
extern "C" void FormatterData__ctor_m5992 ();
extern "C" void RemotingException__ctor_m5993 ();
extern "C" void RemotingException__ctor_m5994 ();
extern "C" void RemotingException__ctor_m5995 ();
extern "C" void RemotingException__ctor_m5996 ();
extern "C" void RemotingServices__cctor_m5997 ();
extern "C" void RemotingServices_GetVirtualMethod_m5998 ();
extern "C" void RemotingServices_IsTransparentProxy_m5999 ();
extern "C" void RemotingServices_GetServerTypeForUri_m6000 ();
extern "C" void RemotingServices_Unmarshal_m6001 ();
extern "C" void RemotingServices_Unmarshal_m6002 ();
extern "C" void RemotingServices_GetRealProxy_m6003 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m6004 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m6005 ();
extern "C" void RemotingServices_FindInterfaceMethod_m6006 ();
extern "C" void RemotingServices_CreateClientProxy_m6007 ();
extern "C" void RemotingServices_CreateClientProxy_m6008 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m6009 ();
extern "C" void RemotingServices_GetIdentityForUri_m6010 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m6011 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m6012 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m6013 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m6014 ();
extern "C" void RemotingServices_RegisterServerIdentity_m6015 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m6016 ();
extern "C" void RemotingServices_GetRemoteObject_m6017 ();
extern "C" void RemotingServices_RegisterInternalChannels_m6018 ();
extern "C" void RemotingServices_DisposeIdentity_m6019 ();
extern "C" void RemotingServices_GetNormalizedUri_m6020 ();
extern "C" void ServerIdentity__ctor_m6021 ();
extern "C" void ServerIdentity_get_ObjectType_m6022 ();
extern "C" void ServerIdentity_CreateObjRef_m6023 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m6024 ();
extern "C" void SingletonIdentity__ctor_m6025 ();
extern "C" void SingleCallIdentity__ctor_m6026 ();
extern "C" void TypeInfo__ctor_m6027 ();
extern "C" void SoapServices__cctor_m6028 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m6029 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m6030 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m6031 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m6032 ();
extern "C" void SoapServices_GetNameKey_m6033 ();
extern "C" void SoapServices_GetAssemblyName_m6034 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m6035 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m6036 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m6037 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m6038 ();
extern "C" void SoapServices_PreLoad_m6039 ();
extern "C" void SoapServices_PreLoad_m6040 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m6041 ();
extern "C" void SoapServices_RegisterInteropXmlType_m6042 ();
extern "C" void SoapServices_EncodeNs_m6043 ();
extern "C" void TypeEntry__ctor_m6044 ();
extern "C" void TypeEntry_get_AssemblyName_m6045 ();
extern "C" void TypeEntry_set_AssemblyName_m6046 ();
extern "C" void TypeEntry_get_TypeName_m6047 ();
extern "C" void TypeEntry_set_TypeName_m6048 ();
extern "C" void TypeInfo__ctor_m6049 ();
extern "C" void TypeInfo_get_TypeName_m6050 ();
extern "C" void WellKnownClientTypeEntry__ctor_m6051 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m6052 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m6053 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m6054 ();
extern "C" void WellKnownClientTypeEntry_ToString_m6055 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m6056 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m6057 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m6058 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m6059 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m6060 ();
extern "C" void BinaryCommon__cctor_m6061 ();
extern "C" void BinaryCommon_IsPrimitive_m6062 ();
extern "C" void BinaryCommon_GetTypeFromCode_m6063 ();
extern "C" void BinaryCommon_SwapBytes_m6064 ();
extern "C" void BinaryFormatter__ctor_m6065 ();
extern "C" void BinaryFormatter__ctor_m6066 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m6067 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m6068 ();
extern "C" void BinaryFormatter_get_Binder_m6069 ();
extern "C" void BinaryFormatter_get_Context_m6070 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m6071 ();
extern "C" void BinaryFormatter_get_FilterLevel_m6072 ();
extern "C" void BinaryFormatter_Deserialize_m6073 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m6074 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m6075 ();
extern "C" void MessageFormatter_ReadMethodCall_m6076 ();
extern "C" void MessageFormatter_ReadMethodResponse_m6077 ();
extern "C" void TypeMetadata__ctor_m6078 ();
extern "C" void ArrayNullFiller__ctor_m6079 ();
extern "C" void ObjectReader__ctor_m6080 ();
extern "C" void ObjectReader_ReadObjectGraph_m6081 ();
extern "C" void ObjectReader_ReadObjectGraph_m6082 ();
extern "C" void ObjectReader_ReadNextObject_m6083 ();
extern "C" void ObjectReader_ReadNextObject_m6084 ();
extern "C" void ObjectReader_get_CurrentObject_m6085 ();
extern "C" void ObjectReader_ReadObject_m6086 ();
extern "C" void ObjectReader_ReadAssembly_m6087 ();
extern "C" void ObjectReader_ReadObjectInstance_m6088 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m6089 ();
extern "C" void ObjectReader_ReadObjectContent_m6090 ();
extern "C" void ObjectReader_RegisterObject_m6091 ();
extern "C" void ObjectReader_ReadStringIntance_m6092 ();
extern "C" void ObjectReader_ReadGenericArray_m6093 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m6094 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m6095 ();
extern "C" void ObjectReader_BlockRead_m6096 ();
extern "C" void ObjectReader_ReadArrayOfObject_m6097 ();
extern "C" void ObjectReader_ReadArrayOfString_m6098 ();
extern "C" void ObjectReader_ReadSimpleArray_m6099 ();
extern "C" void ObjectReader_ReadTypeMetadata_m6100 ();
extern "C" void ObjectReader_ReadValue_m6101 ();
extern "C" void ObjectReader_SetObjectValue_m6102 ();
extern "C" void ObjectReader_RecordFixup_m6103 ();
extern "C" void ObjectReader_GetDeserializationType_m6104 ();
extern "C" void ObjectReader_ReadType_m6105 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m6106 ();
extern "C" void FormatterConverter__ctor_m6107 ();
extern "C" void FormatterConverter_Convert_m6108 ();
extern "C" void FormatterConverter_ToBoolean_m6109 ();
extern "C" void FormatterConverter_ToInt16_m6110 ();
extern "C" void FormatterConverter_ToInt32_m6111 ();
extern "C" void FormatterConverter_ToInt64_m6112 ();
extern "C" void FormatterConverter_ToString_m6113 ();
extern "C" void FormatterServices_GetUninitializedObject_m6114 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m6115 ();
extern "C" void ObjectManager__ctor_m6116 ();
extern "C" void ObjectManager_DoFixups_m6117 ();
extern "C" void ObjectManager_GetObjectRecord_m6118 ();
extern "C" void ObjectManager_GetObject_m6119 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m6120 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m6121 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m6122 ();
extern "C" void ObjectManager_AddFixup_m6123 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m6124 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m6125 ();
extern "C" void ObjectManager_RecordDelayedFixup_m6126 ();
extern "C" void ObjectManager_RecordFixup_m6127 ();
extern "C" void ObjectManager_RegisterObjectInternal_m6128 ();
extern "C" void ObjectManager_RegisterObject_m6129 ();
extern "C" void BaseFixupRecord__ctor_m6130 ();
extern "C" void BaseFixupRecord_DoFixup_m6131 ();
extern "C" void ArrayFixupRecord__ctor_m6132 ();
extern "C" void ArrayFixupRecord_FixupImpl_m6133 ();
extern "C" void MultiArrayFixupRecord__ctor_m6134 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m6135 ();
extern "C" void FixupRecord__ctor_m6136 ();
extern "C" void FixupRecord_FixupImpl_m6137 ();
extern "C" void DelayedFixupRecord__ctor_m6138 ();
extern "C" void DelayedFixupRecord_FixupImpl_m6139 ();
extern "C" void ObjectRecord__ctor_m6140 ();
extern "C" void ObjectRecord_SetMemberValue_m6141 ();
extern "C" void ObjectRecord_SetArrayValue_m6142 ();
extern "C" void ObjectRecord_SetMemberValue_m6143 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m6144 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m6145 ();
extern "C" void ObjectRecord_get_IsRegistered_m6146 ();
extern "C" void ObjectRecord_DoFixups_m6147 ();
extern "C" void ObjectRecord_RemoveFixup_m6148 ();
extern "C" void ObjectRecord_UnchainFixup_m6149 ();
extern "C" void ObjectRecord_ChainFixup_m6150 ();
extern "C" void ObjectRecord_LoadData_m6151 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m6152 ();
extern "C" void SerializationBinder__ctor_m6153 ();
extern "C" void CallbackHandler__ctor_m6154 ();
extern "C" void CallbackHandler_Invoke_m6155 ();
extern "C" void CallbackHandler_BeginInvoke_m6156 ();
extern "C" void CallbackHandler_EndInvoke_m6157 ();
extern "C" void SerializationCallbacks__ctor_m6158 ();
extern "C" void SerializationCallbacks__cctor_m6159 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m6160 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m6161 ();
extern "C" void SerializationCallbacks_Invoke_m6162 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m6163 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m6164 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m6165 ();
extern "C" void SerializationEntry__ctor_m6166 ();
extern "C" void SerializationEntry_get_Name_m6167 ();
extern "C" void SerializationEntry_get_Value_m6168 ();
extern "C" void SerializationException__ctor_m6169 ();
extern "C" void SerializationException__ctor_m1574 ();
extern "C" void SerializationException__ctor_m6170 ();
extern "C" void SerializationInfo__ctor_m6171 ();
extern "C" void SerializationInfo_AddValue_m1570 ();
extern "C" void SerializationInfo_GetValue_m1573 ();
extern "C" void SerializationInfo_SetType_m6172 ();
extern "C" void SerializationInfo_GetEnumerator_m6173 ();
extern "C" void SerializationInfo_AddValue_m6174 ();
extern "C" void SerializationInfo_AddValue_m1572 ();
extern "C" void SerializationInfo_AddValue_m1571 ();
extern "C" void SerializationInfo_AddValue_m6175 ();
extern "C" void SerializationInfo_AddValue_m6176 ();
extern "C" void SerializationInfo_AddValue_m1586 ();
extern "C" void SerializationInfo_AddValue_m6177 ();
extern "C" void SerializationInfo_AddValue_m1585 ();
extern "C" void SerializationInfo_GetBoolean_m1575 ();
extern "C" void SerializationInfo_GetInt16_m6178 ();
extern "C" void SerializationInfo_GetInt32_m1584 ();
extern "C" void SerializationInfo_GetInt64_m1583 ();
extern "C" void SerializationInfo_GetString_m1582 ();
extern "C" void SerializationInfoEnumerator__ctor_m6179 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m6180 ();
extern "C" void SerializationInfoEnumerator_get_Current_m6181 ();
extern "C" void SerializationInfoEnumerator_get_Name_m6182 ();
extern "C" void SerializationInfoEnumerator_get_Value_m6183 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m6184 ();
extern "C" void SerializationInfoEnumerator_Reset_m6185 ();
extern "C" void StreamingContext__ctor_m6186 ();
extern "C" void StreamingContext__ctor_m6187 ();
extern "C" void StreamingContext_get_State_m6188 ();
extern "C" void StreamingContext_Equals_m6189 ();
extern "C" void StreamingContext_GetHashCode_m6190 ();
extern "C" void X509Certificate__ctor_m6191 ();
extern "C" void X509Certificate__ctor_m2715 ();
extern "C" void X509Certificate__ctor_m1649 ();
extern "C" void X509Certificate__ctor_m6192 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6193 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6194 ();
extern "C" void X509Certificate_tostr_m6195 ();
extern "C" void X509Certificate_Equals_m6196 ();
extern "C" void X509Certificate_GetCertHash_m6197 ();
extern "C" void X509Certificate_GetCertHashString_m1654 ();
extern "C" void X509Certificate_GetEffectiveDateString_m6198 ();
extern "C" void X509Certificate_GetExpirationDateString_m6199 ();
extern "C" void X509Certificate_GetHashCode_m6200 ();
extern "C" void X509Certificate_GetIssuerName_m6201 ();
extern "C" void X509Certificate_GetName_m6202 ();
extern "C" void X509Certificate_GetPublicKey_m6203 ();
extern "C" void X509Certificate_GetRawCertData_m6204 ();
extern "C" void X509Certificate_ToString_m6205 ();
extern "C" void X509Certificate_ToString_m1668 ();
extern "C" void X509Certificate_get_Issuer_m1671 ();
extern "C" void X509Certificate_get_Subject_m1670 ();
extern "C" void X509Certificate_Equals_m6206 ();
extern "C" void X509Certificate_Import_m1665 ();
extern "C" void X509Certificate_Reset_m1667 ();
extern "C" void AsymmetricAlgorithm__ctor_m6207 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m6208 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m2663 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m2662 ();
extern "C" void AsymmetricAlgorithm_Clear_m2721 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m6209 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m6210 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m2710 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m2711 ();
extern "C" void Base64Constants__cctor_m6211 ();
extern "C" void CryptoConfig__cctor_m6212 ();
extern "C" void CryptoConfig_Initialize_m6213 ();
extern "C" void CryptoConfig_CreateFromName_m1673 ();
extern "C" void CryptoConfig_CreateFromName_m1700 ();
extern "C" void CryptoConfig_MapNameToOID_m2656 ();
extern "C" void CryptoConfig_EncodeOID_m1675 ();
extern "C" void CryptoConfig_EncodeLongNumber_m6214 ();
extern "C" void CryptographicException__ctor_m6215 ();
extern "C" void CryptographicException__ctor_m1628 ();
extern "C" void CryptographicException__ctor_m1632 ();
extern "C" void CryptographicException__ctor_m1801 ();
extern "C" void CryptographicException__ctor_m6216 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m6217 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m2693 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m6218 ();
extern "C" void CspParameters__ctor_m2658 ();
extern "C" void CspParameters__ctor_m6219 ();
extern "C" void CspParameters__ctor_m6220 ();
extern "C" void CspParameters__ctor_m6221 ();
extern "C" void CspParameters_get_Flags_m6222 ();
extern "C" void CspParameters_set_Flags_m2659 ();
extern "C" void DES__ctor_m6223 ();
extern "C" void DES__cctor_m6224 ();
extern "C" void DES_Create_m2694 ();
extern "C" void DES_Create_m6225 ();
extern "C" void DES_IsWeakKey_m6226 ();
extern "C" void DES_IsSemiWeakKey_m6227 ();
extern "C" void DES_get_Key_m6228 ();
extern "C" void DES_set_Key_m6229 ();
extern "C" void DESTransform__ctor_m6230 ();
extern "C" void DESTransform__cctor_m6231 ();
extern "C" void DESTransform_CipherFunct_m6232 ();
extern "C" void DESTransform_Permutation_m6233 ();
extern "C" void DESTransform_BSwap_m6234 ();
extern "C" void DESTransform_SetKey_m6235 ();
extern "C" void DESTransform_ProcessBlock_m6236 ();
extern "C" void DESTransform_ECB_m6237 ();
extern "C" void DESTransform_GetStrongKey_m6238 ();
extern "C" void DESCryptoServiceProvider__ctor_m6239 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m6240 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m6241 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m6242 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m6243 ();
extern "C" void DSA__ctor_m6244 ();
extern "C" void DSA_Create_m1623 ();
extern "C" void DSA_Create_m6245 ();
extern "C" void DSA_ZeroizePrivateKey_m6246 ();
extern "C" void DSA_FromXmlString_m6247 ();
extern "C" void DSA_ToXmlString_m6248 ();
extern "C" void DSACryptoServiceProvider__ctor_m6249 ();
extern "C" void DSACryptoServiceProvider__ctor_m1633 ();
extern "C" void DSACryptoServiceProvider__ctor_m6250 ();
extern "C" void DSACryptoServiceProvider__cctor_m6251 ();
extern "C" void DSACryptoServiceProvider_Finalize_m6252 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m6253 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m1622 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m6254 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m6255 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m6256 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m6257 ();
extern "C" void DSACryptoServiceProvider_Dispose_m6258 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m6259 ();
extern "C" void DSASignatureDeformatter__ctor_m6260 ();
extern "C" void DSASignatureDeformatter__ctor_m2678 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m6261 ();
extern "C" void DSASignatureDeformatter_SetKey_m6262 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m6263 ();
extern "C" void DSASignatureFormatter__ctor_m6264 ();
extern "C" void DSASignatureFormatter_CreateSignature_m6265 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m6266 ();
extern "C" void DSASignatureFormatter_SetKey_m6267 ();
extern "C" void HMAC__ctor_m6268 ();
extern "C" void HMAC_get_BlockSizeValue_m6269 ();
extern "C" void HMAC_set_BlockSizeValue_m6270 ();
extern "C" void HMAC_set_HashName_m6271 ();
extern "C" void HMAC_get_Key_m6272 ();
extern "C" void HMAC_set_Key_m6273 ();
extern "C" void HMAC_get_Block_m6274 ();
extern "C" void HMAC_KeySetup_m6275 ();
extern "C" void HMAC_Dispose_m6276 ();
extern "C" void HMAC_HashCore_m6277 ();
extern "C" void HMAC_HashFinal_m6278 ();
extern "C" void HMAC_Initialize_m6279 ();
extern "C" void HMAC_Create_m2671 ();
extern "C" void HMAC_Create_m6280 ();
extern "C" void HMACMD5__ctor_m6281 ();
extern "C" void HMACMD5__ctor_m6282 ();
extern "C" void HMACRIPEMD160__ctor_m6283 ();
extern "C" void HMACRIPEMD160__ctor_m6284 ();
extern "C" void HMACSHA1__ctor_m6285 ();
extern "C" void HMACSHA1__ctor_m6286 ();
extern "C" void HMACSHA256__ctor_m6287 ();
extern "C" void HMACSHA256__ctor_m6288 ();
extern "C" void HMACSHA384__ctor_m6289 ();
extern "C" void HMACSHA384__ctor_m6290 ();
extern "C" void HMACSHA384__cctor_m6291 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m6292 ();
extern "C" void HMACSHA512__ctor_m6293 ();
extern "C" void HMACSHA512__ctor_m6294 ();
extern "C" void HMACSHA512__cctor_m6295 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m6296 ();
extern "C" void HashAlgorithm__ctor_m2655 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m6297 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m6298 ();
extern "C" void HashAlgorithm_ComputeHash_m1710 ();
extern "C" void HashAlgorithm_ComputeHash_m2666 ();
extern "C" void HashAlgorithm_Create_m2665 ();
extern "C" void HashAlgorithm_get_Hash_m6299 ();
extern "C" void HashAlgorithm_get_HashSize_m6300 ();
extern "C" void HashAlgorithm_Dispose_m6301 ();
extern "C" void HashAlgorithm_TransformBlock_m6302 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m6303 ();
extern "C" void KeySizes__ctor_m1803 ();
extern "C" void KeySizes_get_MaxSize_m6304 ();
extern "C" void KeySizes_get_MinSize_m6305 ();
extern "C" void KeySizes_get_SkipSize_m6306 ();
extern "C" void KeySizes_IsLegal_m6307 ();
extern "C" void KeySizes_IsLegalKeySize_m6308 ();
extern "C" void KeyedHashAlgorithm__ctor_m2692 ();
extern "C" void KeyedHashAlgorithm_Finalize_m6309 ();
extern "C" void KeyedHashAlgorithm_get_Key_m6310 ();
extern "C" void KeyedHashAlgorithm_set_Key_m6311 ();
extern "C" void KeyedHashAlgorithm_Dispose_m6312 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m6313 ();
extern "C" void MACTripleDES__ctor_m6314 ();
extern "C" void MACTripleDES_Setup_m6315 ();
extern "C" void MACTripleDES_Finalize_m6316 ();
extern "C" void MACTripleDES_Dispose_m6317 ();
extern "C" void MACTripleDES_Initialize_m6318 ();
extern "C" void MACTripleDES_HashCore_m6319 ();
extern "C" void MACTripleDES_HashFinal_m6320 ();
extern "C" void MD5__ctor_m6321 ();
extern "C" void MD5_Create_m2675 ();
extern "C" void MD5_Create_m6322 ();
extern "C" void MD5CryptoServiceProvider__ctor_m6323 ();
extern "C" void MD5CryptoServiceProvider__cctor_m6324 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m6325 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m6326 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m6327 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m6328 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m6329 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m6330 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m6331 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m6332 ();
extern "C" void RC2__ctor_m6333 ();
extern "C" void RC2_Create_m2695 ();
extern "C" void RC2_Create_m6334 ();
extern "C" void RC2_get_EffectiveKeySize_m6335 ();
extern "C" void RC2_get_KeySize_m6336 ();
extern "C" void RC2_set_KeySize_m6337 ();
extern "C" void RC2CryptoServiceProvider__ctor_m6338 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m6339 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m6340 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m6341 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m6342 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m6343 ();
extern "C" void RC2Transform__ctor_m6344 ();
extern "C" void RC2Transform__cctor_m6345 ();
extern "C" void RC2Transform_ECB_m6346 ();
extern "C" void RIPEMD160__ctor_m6347 ();
extern "C" void RIPEMD160Managed__ctor_m6348 ();
extern "C" void RIPEMD160Managed_Initialize_m6349 ();
extern "C" void RIPEMD160Managed_HashCore_m6350 ();
extern "C" void RIPEMD160Managed_HashFinal_m6351 ();
extern "C" void RIPEMD160Managed_Finalize_m6352 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m6353 ();
extern "C" void RIPEMD160Managed_Compress_m6354 ();
extern "C" void RIPEMD160Managed_CompressFinal_m6355 ();
extern "C" void RIPEMD160Managed_ROL_m6356 ();
extern "C" void RIPEMD160Managed_F_m6357 ();
extern "C" void RIPEMD160Managed_G_m6358 ();
extern "C" void RIPEMD160Managed_H_m6359 ();
extern "C" void RIPEMD160Managed_I_m6360 ();
extern "C" void RIPEMD160Managed_J_m6361 ();
extern "C" void RIPEMD160Managed_FF_m6362 ();
extern "C" void RIPEMD160Managed_GG_m6363 ();
extern "C" void RIPEMD160Managed_HH_m6364 ();
extern "C" void RIPEMD160Managed_II_m6365 ();
extern "C" void RIPEMD160Managed_JJ_m6366 ();
extern "C" void RIPEMD160Managed_FFF_m6367 ();
extern "C" void RIPEMD160Managed_GGG_m6368 ();
extern "C" void RIPEMD160Managed_HHH_m6369 ();
extern "C" void RIPEMD160Managed_III_m6370 ();
extern "C" void RIPEMD160Managed_JJJ_m6371 ();
extern "C" void RNGCryptoServiceProvider__ctor_m6372 ();
extern "C" void RNGCryptoServiceProvider__cctor_m6373 ();
extern "C" void RNGCryptoServiceProvider_Check_m6374 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m6375 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m6376 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m6377 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m6378 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m6379 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m6380 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m6381 ();
extern "C" void RSA__ctor_m2661 ();
extern "C" void RSA_Create_m1620 ();
extern "C" void RSA_Create_m6382 ();
extern "C" void RSA_ZeroizePrivateKey_m6383 ();
extern "C" void RSA_FromXmlString_m6384 ();
extern "C" void RSA_ToXmlString_m6385 ();
extern "C" void RSACryptoServiceProvider__ctor_m6386 ();
extern "C" void RSACryptoServiceProvider__ctor_m2660 ();
extern "C" void RSACryptoServiceProvider__ctor_m1634 ();
extern "C" void RSACryptoServiceProvider__cctor_m6387 ();
extern "C" void RSACryptoServiceProvider_Common_m6388 ();
extern "C" void RSACryptoServiceProvider_Finalize_m6389 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m6390 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m1618 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m6391 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m6392 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m6393 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m6394 ();
extern "C" void RSACryptoServiceProvider_Dispose_m6395 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m6396 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m2720 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m6397 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m6398 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m6399 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m2679 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m6400 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m6401 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m6402 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m6403 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m6404 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m6405 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m6406 ();
extern "C" void RandomNumberGenerator__ctor_m6407 ();
extern "C" void RandomNumberGenerator_Create_m1796 ();
extern "C" void RandomNumberGenerator_Create_m6408 ();
extern "C" void Rijndael__ctor_m6409 ();
extern "C" void Rijndael_Create_m2697 ();
extern "C" void Rijndael_Create_m6410 ();
extern "C" void RijndaelManaged__ctor_m6411 ();
extern "C" void RijndaelManaged_GenerateIV_m6412 ();
extern "C" void RijndaelManaged_GenerateKey_m6413 ();
extern "C" void RijndaelManaged_CreateDecryptor_m6414 ();
extern "C" void RijndaelManaged_CreateEncryptor_m6415 ();
extern "C" void RijndaelTransform__ctor_m6416 ();
extern "C" void RijndaelTransform__cctor_m6417 ();
extern "C" void RijndaelTransform_Clear_m6418 ();
extern "C" void RijndaelTransform_ECB_m6419 ();
extern "C" void RijndaelTransform_SubByte_m6420 ();
extern "C" void RijndaelTransform_Encrypt128_m6421 ();
extern "C" void RijndaelTransform_Encrypt192_m6422 ();
extern "C" void RijndaelTransform_Encrypt256_m6423 ();
extern "C" void RijndaelTransform_Decrypt128_m6424 ();
extern "C" void RijndaelTransform_Decrypt192_m6425 ();
extern "C" void RijndaelTransform_Decrypt256_m6426 ();
extern "C" void RijndaelManagedTransform__ctor_m6427 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m6428 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m6429 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m6430 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m6431 ();
extern "C" void SHA1__ctor_m6432 ();
extern "C" void SHA1_Create_m1709 ();
extern "C" void SHA1_Create_m6433 ();
extern "C" void SHA1Internal__ctor_m6434 ();
extern "C" void SHA1Internal_HashCore_m6435 ();
extern "C" void SHA1Internal_HashFinal_m6436 ();
extern "C" void SHA1Internal_Initialize_m6437 ();
extern "C" void SHA1Internal_ProcessBlock_m6438 ();
extern "C" void SHA1Internal_InitialiseBuff_m6439 ();
extern "C" void SHA1Internal_FillBuff_m6440 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m6441 ();
extern "C" void SHA1Internal_AddLength_m6442 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m6443 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m6444 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m6445 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m6446 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m6447 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m6448 ();
extern "C" void SHA1Managed__ctor_m6449 ();
extern "C" void SHA1Managed_HashCore_m6450 ();
extern "C" void SHA1Managed_HashFinal_m6451 ();
extern "C" void SHA1Managed_Initialize_m6452 ();
extern "C" void SHA256__ctor_m6453 ();
extern "C" void SHA256_Create_m2676 ();
extern "C" void SHA256_Create_m6454 ();
extern "C" void SHA256Managed__ctor_m6455 ();
extern "C" void SHA256Managed_HashCore_m6456 ();
extern "C" void SHA256Managed_HashFinal_m6457 ();
extern "C" void SHA256Managed_Initialize_m6458 ();
extern "C" void SHA256Managed_ProcessBlock_m6459 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m6460 ();
extern "C" void SHA256Managed_AddLength_m6461 ();
extern "C" void SHA384__ctor_m6462 ();
extern "C" void SHA384Managed__ctor_m6463 ();
extern "C" void SHA384Managed_Initialize_m6464 ();
extern "C" void SHA384Managed_Initialize_m6465 ();
extern "C" void SHA384Managed_HashCore_m6466 ();
extern "C" void SHA384Managed_HashFinal_m6467 ();
extern "C" void SHA384Managed_update_m6468 ();
extern "C" void SHA384Managed_processWord_m6469 ();
extern "C" void SHA384Managed_unpackWord_m6470 ();
extern "C" void SHA384Managed_adjustByteCounts_m6471 ();
extern "C" void SHA384Managed_processLength_m6472 ();
extern "C" void SHA384Managed_processBlock_m6473 ();
extern "C" void SHA512__ctor_m6474 ();
extern "C" void SHA512Managed__ctor_m6475 ();
extern "C" void SHA512Managed_Initialize_m6476 ();
extern "C" void SHA512Managed_Initialize_m6477 ();
extern "C" void SHA512Managed_HashCore_m6478 ();
extern "C" void SHA512Managed_HashFinal_m6479 ();
extern "C" void SHA512Managed_update_m6480 ();
extern "C" void SHA512Managed_processWord_m6481 ();
extern "C" void SHA512Managed_unpackWord_m6482 ();
extern "C" void SHA512Managed_adjustByteCounts_m6483 ();
extern "C" void SHA512Managed_processLength_m6484 ();
extern "C" void SHA512Managed_processBlock_m6485 ();
extern "C" void SHA512Managed_rotateRight_m6486 ();
extern "C" void SHA512Managed_Ch_m6487 ();
extern "C" void SHA512Managed_Maj_m6488 ();
extern "C" void SHA512Managed_Sum0_m6489 ();
extern "C" void SHA512Managed_Sum1_m6490 ();
extern "C" void SHA512Managed_Sigma0_m6491 ();
extern "C" void SHA512Managed_Sigma1_m6492 ();
extern "C" void SHAConstants__cctor_m6493 ();
extern "C" void SignatureDescription__ctor_m6494 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m6495 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m6496 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m6497 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m6498 ();
extern "C" void DSASignatureDescription__ctor_m6499 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m6500 ();
extern "C" void SymmetricAlgorithm__ctor_m1802 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m6501 ();
extern "C" void SymmetricAlgorithm_Finalize_m2653 ();
extern "C" void SymmetricAlgorithm_Clear_m2670 ();
extern "C" void SymmetricAlgorithm_Dispose_m1810 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m6502 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m6503 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m6504 ();
extern "C" void SymmetricAlgorithm_get_IV_m1804 ();
extern "C" void SymmetricAlgorithm_set_IV_m1805 ();
extern "C" void SymmetricAlgorithm_get_Key_m1806 ();
extern "C" void SymmetricAlgorithm_set_Key_m1807 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m1808 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m1809 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m6505 ();
extern "C" void SymmetricAlgorithm_get_Mode_m6506 ();
extern "C" void SymmetricAlgorithm_set_Mode_m6507 ();
extern "C" void SymmetricAlgorithm_get_Padding_m6508 ();
extern "C" void SymmetricAlgorithm_set_Padding_m6509 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m6510 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m6511 ();
extern "C" void SymmetricAlgorithm_Create_m2669 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m6512 ();
extern "C" void ToBase64Transform_Finalize_m6513 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m6514 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m6515 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m6516 ();
extern "C" void ToBase64Transform_Dispose_m6517 ();
extern "C" void ToBase64Transform_TransformBlock_m6518 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m6519 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m6520 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m6521 ();
extern "C" void TripleDES__ctor_m6522 ();
extern "C" void TripleDES_get_Key_m6523 ();
extern "C" void TripleDES_set_Key_m6524 ();
extern "C" void TripleDES_IsWeakKey_m6525 ();
extern "C" void TripleDES_Create_m2696 ();
extern "C" void TripleDES_Create_m6526 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m6527 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m6528 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m6529 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m6530 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m6531 ();
extern "C" void TripleDESTransform__ctor_m6532 ();
extern "C" void TripleDESTransform_ECB_m6533 ();
extern "C" void TripleDESTransform_GetStrongKey_m6534 ();
extern "C" void SecurityPermission__ctor_m6535 ();
extern "C" void SecurityPermission_set_Flags_m6536 ();
extern "C" void SecurityPermission_IsUnrestricted_m6537 ();
extern "C" void SecurityPermission_IsSubsetOf_m6538 ();
extern "C" void SecurityPermission_ToXml_m6539 ();
extern "C" void SecurityPermission_IsEmpty_m6540 ();
extern "C" void SecurityPermission_Cast_m6541 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m6542 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m6543 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m6544 ();
extern "C" void ApplicationTrust__ctor_m6545 ();
extern "C" void EvidenceEnumerator__ctor_m6546 ();
extern "C" void EvidenceEnumerator_MoveNext_m6547 ();
extern "C" void EvidenceEnumerator_Reset_m6548 ();
extern "C" void EvidenceEnumerator_get_Current_m6549 ();
extern "C" void Evidence__ctor_m6550 ();
extern "C" void Evidence_get_Count_m6551 ();
extern "C" void Evidence_get_SyncRoot_m6552 ();
extern "C" void Evidence_get_HostEvidenceList_m6553 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m6554 ();
extern "C" void Evidence_CopyTo_m6555 ();
extern "C" void Evidence_Equals_m6556 ();
extern "C" void Evidence_GetEnumerator_m6557 ();
extern "C" void Evidence_GetHashCode_m6558 ();
extern "C" void Hash__ctor_m6559 ();
extern "C" void Hash__ctor_m6560 ();
extern "C" void Hash_GetObjectData_m6561 ();
extern "C" void Hash_ToString_m6562 ();
extern "C" void Hash_GetData_m6563 ();
extern "C" void StrongName_get_Name_m6564 ();
extern "C" void StrongName_get_PublicKey_m6565 ();
extern "C" void StrongName_get_Version_m6566 ();
extern "C" void StrongName_Equals_m6567 ();
extern "C" void StrongName_GetHashCode_m6568 ();
extern "C" void StrongName_ToString_m6569 ();
extern "C" void WindowsIdentity__ctor_m6570 ();
extern "C" void WindowsIdentity__cctor_m6571 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6572 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m6573 ();
extern "C" void WindowsIdentity_Dispose_m6574 ();
extern "C" void WindowsIdentity_GetCurrentToken_m6575 ();
extern "C" void WindowsIdentity_GetTokenName_m6576 ();
extern "C" void CodeAccessPermission__ctor_m6577 ();
extern "C" void CodeAccessPermission_Equals_m6578 ();
extern "C" void CodeAccessPermission_GetHashCode_m6579 ();
extern "C" void CodeAccessPermission_ToString_m6580 ();
extern "C" void CodeAccessPermission_Element_m6581 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m6582 ();
extern "C" void PermissionSet__ctor_m6583 ();
extern "C" void PermissionSet__ctor_m6584 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m6585 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m6586 ();
extern "C" void SecurityContext__ctor_m6587 ();
extern "C" void SecurityContext__ctor_m6588 ();
extern "C" void SecurityContext_Capture_m6589 ();
extern "C" void SecurityContext_get_FlowSuppressed_m6590 ();
extern "C" void SecurityContext_get_CompressedStack_m6591 ();
extern "C" void SecurityAttribute__ctor_m6592 ();
extern "C" void SecurityAttribute_get_Name_m6593 ();
extern "C" void SecurityAttribute_get_Value_m6594 ();
extern "C" void SecurityElement__ctor_m6595 ();
extern "C" void SecurityElement__ctor_m6596 ();
extern "C" void SecurityElement__cctor_m6597 ();
extern "C" void SecurityElement_get_Children_m6598 ();
extern "C" void SecurityElement_get_Tag_m6599 ();
extern "C" void SecurityElement_set_Text_m6600 ();
extern "C" void SecurityElement_AddAttribute_m6601 ();
extern "C" void SecurityElement_AddChild_m6602 ();
extern "C" void SecurityElement_Escape_m6603 ();
extern "C" void SecurityElement_Unescape_m6604 ();
extern "C" void SecurityElement_IsValidAttributeName_m6605 ();
extern "C" void SecurityElement_IsValidAttributeValue_m6606 ();
extern "C" void SecurityElement_IsValidTag_m6607 ();
extern "C" void SecurityElement_IsValidText_m6608 ();
extern "C" void SecurityElement_SearchForChildByTag_m6609 ();
extern "C" void SecurityElement_ToString_m6610 ();
extern "C" void SecurityElement_ToXml_m6611 ();
extern "C" void SecurityElement_GetAttribute_m6612 ();
extern "C" void SecurityException__ctor_m6613 ();
extern "C" void SecurityException__ctor_m6614 ();
extern "C" void SecurityException__ctor_m6615 ();
extern "C" void SecurityException_get_Demanded_m6616 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m6617 ();
extern "C" void SecurityException_get_PermissionState_m6618 ();
extern "C" void SecurityException_get_PermissionType_m6619 ();
extern "C" void SecurityException_get_GrantedSet_m6620 ();
extern "C" void SecurityException_get_RefusedSet_m6621 ();
extern "C" void SecurityException_GetObjectData_m6622 ();
extern "C" void SecurityException_ToString_m6623 ();
extern "C" void SecurityFrame__ctor_m6624 ();
extern "C" void SecurityFrame__GetSecurityStack_m6625 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m6626 ();
extern "C" void SecurityFrame_get_Assembly_m6627 ();
extern "C" void SecurityFrame_get_Domain_m6628 ();
extern "C" void SecurityFrame_ToString_m6629 ();
extern "C" void SecurityFrame_GetStack_m6630 ();
extern "C" void SecurityManager__cctor_m6631 ();
extern "C" void SecurityManager_get_SecurityEnabled_m6632 ();
extern "C" void SecurityManager_Decode_m6633 ();
extern "C" void SecurityManager_Decode_m6634 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m6635 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m6636 ();
extern "C" void UnverifiableCodeAttribute__ctor_m6637 ();
extern "C" void ASCIIEncoding__ctor_m6638 ();
extern "C" void ASCIIEncoding_GetByteCount_m6639 ();
extern "C" void ASCIIEncoding_GetByteCount_m6640 ();
extern "C" void ASCIIEncoding_GetBytes_m6641 ();
extern "C" void ASCIIEncoding_GetBytes_m6642 ();
extern "C" void ASCIIEncoding_GetBytes_m6643 ();
extern "C" void ASCIIEncoding_GetBytes_m6644 ();
extern "C" void ASCIIEncoding_GetCharCount_m6645 ();
extern "C" void ASCIIEncoding_GetChars_m6646 ();
extern "C" void ASCIIEncoding_GetChars_m6647 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m6648 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m6649 ();
extern "C" void ASCIIEncoding_GetString_m6650 ();
extern "C" void ASCIIEncoding_GetBytes_m6651 ();
extern "C" void ASCIIEncoding_GetByteCount_m6652 ();
extern "C" void ASCIIEncoding_GetDecoder_m6653 ();
extern "C" void Decoder__ctor_m6654 ();
extern "C" void Decoder_set_Fallback_m6655 ();
extern "C" void Decoder_get_FallbackBuffer_m6656 ();
extern "C" void DecoderExceptionFallback__ctor_m6657 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m6658 ();
extern "C" void DecoderExceptionFallback_Equals_m6659 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m6660 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m6661 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m6662 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m6663 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m6664 ();
extern "C" void DecoderFallback__ctor_m6665 ();
extern "C" void DecoderFallback__cctor_m6666 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m6667 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m6668 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m6669 ();
extern "C" void DecoderFallbackBuffer__ctor_m6670 ();
extern "C" void DecoderFallbackBuffer_Reset_m6671 ();
extern "C" void DecoderFallbackException__ctor_m6672 ();
extern "C" void DecoderFallbackException__ctor_m6673 ();
extern "C" void DecoderFallbackException__ctor_m6674 ();
extern "C" void DecoderReplacementFallback__ctor_m6675 ();
extern "C" void DecoderReplacementFallback__ctor_m6676 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m6677 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m6678 ();
extern "C" void DecoderReplacementFallback_Equals_m6679 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m6680 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m6681 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m6682 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m6683 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m6684 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m6685 ();
extern "C" void EncoderExceptionFallback__ctor_m6686 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m6687 ();
extern "C" void EncoderExceptionFallback_Equals_m6688 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m6689 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m6690 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m6691 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m6692 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m6693 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m6694 ();
extern "C" void EncoderFallback__ctor_m6695 ();
extern "C" void EncoderFallback__cctor_m6696 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m6697 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m6698 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m6699 ();
extern "C" void EncoderFallbackBuffer__ctor_m6700 ();
extern "C" void EncoderFallbackException__ctor_m6701 ();
extern "C" void EncoderFallbackException__ctor_m6702 ();
extern "C" void EncoderFallbackException__ctor_m6703 ();
extern "C" void EncoderFallbackException__ctor_m6704 ();
extern "C" void EncoderReplacementFallback__ctor_m6705 ();
extern "C" void EncoderReplacementFallback__ctor_m6706 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m6707 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m6708 ();
extern "C" void EncoderReplacementFallback_Equals_m6709 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m6710 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m6711 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m6712 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m6713 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m6714 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m6715 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m6716 ();
extern "C" void ForwardingDecoder__ctor_m6717 ();
extern "C" void ForwardingDecoder_GetChars_m6718 ();
extern "C" void Encoding__ctor_m6719 ();
extern "C" void Encoding__ctor_m6720 ();
extern "C" void Encoding__cctor_m6721 ();
extern "C" void Encoding___m6722 ();
extern "C" void Encoding_get_IsReadOnly_m6723 ();
extern "C" void Encoding_get_DecoderFallback_m6724 ();
extern "C" void Encoding_set_DecoderFallback_m6725 ();
extern "C" void Encoding_get_EncoderFallback_m6726 ();
extern "C" void Encoding_SetFallbackInternal_m6727 ();
extern "C" void Encoding_Equals_m6728 ();
extern "C" void Encoding_GetByteCount_m6729 ();
extern "C" void Encoding_GetByteCount_m6730 ();
extern "C" void Encoding_GetBytes_m6731 ();
extern "C" void Encoding_GetBytes_m6732 ();
extern "C" void Encoding_GetBytes_m6733 ();
extern "C" void Encoding_GetBytes_m6734 ();
extern "C" void Encoding_GetChars_m6735 ();
extern "C" void Encoding_GetDecoder_m6736 ();
extern "C" void Encoding_InvokeI18N_m6737 ();
extern "C" void Encoding_GetEncoding_m6738 ();
extern "C" void Encoding_Clone_m6739 ();
extern "C" void Encoding_GetEncoding_m6740 ();
extern "C" void Encoding_GetHashCode_m6741 ();
extern "C" void Encoding_GetPreamble_m6742 ();
extern "C" void Encoding_GetString_m6743 ();
extern "C" void Encoding_GetString_m6744 ();
extern "C" void Encoding_get_ASCII_m1712 ();
extern "C" void Encoding_get_BigEndianUnicode_m2667 ();
extern "C" void Encoding_InternalCodePage_m6745 ();
extern "C" void Encoding_get_Default_m6746 ();
extern "C" void Encoding_get_ISOLatin1_m6747 ();
extern "C" void Encoding_get_UTF7_m2672 ();
extern "C" void Encoding_get_UTF8_m1658 ();
extern "C" void Encoding_get_UTF8Unmarked_m6748 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m6749 ();
extern "C" void Encoding_get_Unicode_m6750 ();
extern "C" void Encoding_get_UTF32_m6751 ();
extern "C" void Encoding_get_BigEndianUTF32_m6752 ();
extern "C" void Encoding_GetByteCount_m6753 ();
extern "C" void Encoding_GetBytes_m6754 ();
extern "C" void Latin1Encoding__ctor_m6755 ();
extern "C" void Latin1Encoding_GetByteCount_m6756 ();
extern "C" void Latin1Encoding_GetByteCount_m6757 ();
extern "C" void Latin1Encoding_GetBytes_m6758 ();
extern "C" void Latin1Encoding_GetBytes_m6759 ();
extern "C" void Latin1Encoding_GetBytes_m6760 ();
extern "C" void Latin1Encoding_GetBytes_m6761 ();
extern "C" void Latin1Encoding_GetCharCount_m6762 ();
extern "C" void Latin1Encoding_GetChars_m6763 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m6764 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m6765 ();
extern "C" void Latin1Encoding_GetString_m6766 ();
extern "C" void Latin1Encoding_GetString_m6767 ();
extern "C" void StringBuilder__ctor_m6768 ();
extern "C" void StringBuilder__ctor_m6769 ();
extern "C" void StringBuilder__ctor_m1601 ();
extern "C" void StringBuilder__ctor_m636 ();
extern "C" void StringBuilder__ctor_m1637 ();
extern "C" void StringBuilder__ctor_m1578 ();
extern "C" void StringBuilder__ctor_m6770 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m6771 ();
extern "C" void StringBuilder_get_Capacity_m6772 ();
extern "C" void StringBuilder_set_Capacity_m6773 ();
extern "C" void StringBuilder_get_Length_m1702 ();
extern "C" void StringBuilder_set_Length_m1746 ();
extern "C" void StringBuilder_get_Chars_m6774 ();
extern "C" void StringBuilder_set_Chars_m6775 ();
extern "C" void StringBuilder_ToString_m640 ();
extern "C" void StringBuilder_ToString_m6776 ();
extern "C" void StringBuilder_Remove_m6777 ();
extern "C" void StringBuilder_Replace_m6778 ();
extern "C" void StringBuilder_Replace_m6779 ();
extern "C" void StringBuilder_Append_m639 ();
extern "C" void StringBuilder_Append_m1648 ();
extern "C" void StringBuilder_Append_m1603 ();
extern "C" void StringBuilder_Append_m1580 ();
extern "C" void StringBuilder_Append_m1579 ();
extern "C" void StringBuilder_Append_m6780 ();
extern "C" void StringBuilder_Append_m6781 ();
extern "C" void StringBuilder_Append_m1715 ();
extern "C" void StringBuilder_AppendFormat_m2647 ();
extern "C" void StringBuilder_AppendFormat_m6782 ();
extern "C" void StringBuilder_AppendFormat_m1602 ();
extern "C" void StringBuilder_AppendFormat_m1669 ();
extern "C" void StringBuilder_AppendFormat_m1672 ();
extern "C" void StringBuilder_Insert_m6783 ();
extern "C" void StringBuilder_Insert_m6784 ();
extern "C" void StringBuilder_Insert_m6785 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m6786 ();
extern "C" void UTF32Decoder__ctor_m6787 ();
extern "C" void UTF32Decoder_GetChars_m6788 ();
extern "C" void UTF32Encoding__ctor_m6789 ();
extern "C" void UTF32Encoding__ctor_m6790 ();
extern "C" void UTF32Encoding__ctor_m6791 ();
extern "C" void UTF32Encoding_GetByteCount_m6792 ();
extern "C" void UTF32Encoding_GetBytes_m6793 ();
extern "C" void UTF32Encoding_GetCharCount_m6794 ();
extern "C" void UTF32Encoding_GetChars_m6795 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m6796 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m6797 ();
extern "C" void UTF32Encoding_GetDecoder_m6798 ();
extern "C" void UTF32Encoding_GetPreamble_m6799 ();
extern "C" void UTF32Encoding_Equals_m6800 ();
extern "C" void UTF32Encoding_GetHashCode_m6801 ();
extern "C" void UTF32Encoding_GetByteCount_m6802 ();
extern "C" void UTF32Encoding_GetByteCount_m6803 ();
extern "C" void UTF32Encoding_GetBytes_m6804 ();
extern "C" void UTF32Encoding_GetBytes_m6805 ();
extern "C" void UTF32Encoding_GetString_m6806 ();
extern "C" void UTF7Decoder__ctor_m6807 ();
extern "C" void UTF7Decoder_GetChars_m6808 ();
extern "C" void UTF7Encoding__ctor_m6809 ();
extern "C" void UTF7Encoding__ctor_m6810 ();
extern "C" void UTF7Encoding__cctor_m6811 ();
extern "C" void UTF7Encoding_GetHashCode_m6812 ();
extern "C" void UTF7Encoding_Equals_m6813 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m6814 ();
extern "C" void UTF7Encoding_GetByteCount_m6815 ();
extern "C" void UTF7Encoding_InternalGetBytes_m6816 ();
extern "C" void UTF7Encoding_GetBytes_m6817 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m6818 ();
extern "C" void UTF7Encoding_GetCharCount_m6819 ();
extern "C" void UTF7Encoding_InternalGetChars_m6820 ();
extern "C" void UTF7Encoding_GetChars_m6821 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m6822 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m6823 ();
extern "C" void UTF7Encoding_GetDecoder_m6824 ();
extern "C" void UTF7Encoding_GetByteCount_m6825 ();
extern "C" void UTF7Encoding_GetByteCount_m6826 ();
extern "C" void UTF7Encoding_GetBytes_m6827 ();
extern "C" void UTF7Encoding_GetBytes_m6828 ();
extern "C" void UTF7Encoding_GetString_m6829 ();
extern "C" void UTF8Decoder__ctor_m6830 ();
extern "C" void UTF8Decoder_GetChars_m6831 ();
extern "C" void UTF8Encoding__ctor_m6832 ();
extern "C" void UTF8Encoding__ctor_m6833 ();
extern "C" void UTF8Encoding__ctor_m6834 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m6835 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m6836 ();
extern "C" void UTF8Encoding_GetByteCount_m6837 ();
extern "C" void UTF8Encoding_GetByteCount_m6838 ();
extern "C" void UTF8Encoding_InternalGetBytes_m6839 ();
extern "C" void UTF8Encoding_InternalGetBytes_m6840 ();
extern "C" void UTF8Encoding_GetBytes_m6841 ();
extern "C" void UTF8Encoding_GetBytes_m6842 ();
extern "C" void UTF8Encoding_GetBytes_m6843 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m6844 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m6845 ();
extern "C" void UTF8Encoding_Fallback_m6846 ();
extern "C" void UTF8Encoding_Fallback_m6847 ();
extern "C" void UTF8Encoding_GetCharCount_m6848 ();
extern "C" void UTF8Encoding_InternalGetChars_m6849 ();
extern "C" void UTF8Encoding_InternalGetChars_m6850 ();
extern "C" void UTF8Encoding_GetChars_m6851 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m6852 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m6853 ();
extern "C" void UTF8Encoding_GetDecoder_m6854 ();
extern "C" void UTF8Encoding_GetPreamble_m6855 ();
extern "C" void UTF8Encoding_Equals_m6856 ();
extern "C" void UTF8Encoding_GetHashCode_m6857 ();
extern "C" void UTF8Encoding_GetByteCount_m6858 ();
extern "C" void UTF8Encoding_GetString_m6859 ();
extern "C" void UnicodeDecoder__ctor_m6860 ();
extern "C" void UnicodeDecoder_GetChars_m6861 ();
extern "C" void UnicodeEncoding__ctor_m6862 ();
extern "C" void UnicodeEncoding__ctor_m6863 ();
extern "C" void UnicodeEncoding__ctor_m6864 ();
extern "C" void UnicodeEncoding_GetByteCount_m6865 ();
extern "C" void UnicodeEncoding_GetByteCount_m6866 ();
extern "C" void UnicodeEncoding_GetByteCount_m6867 ();
extern "C" void UnicodeEncoding_GetBytes_m6868 ();
extern "C" void UnicodeEncoding_GetBytes_m6869 ();
extern "C" void UnicodeEncoding_GetBytes_m6870 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m6871 ();
extern "C" void UnicodeEncoding_GetCharCount_m6872 ();
extern "C" void UnicodeEncoding_GetChars_m6873 ();
extern "C" void UnicodeEncoding_GetString_m6874 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m6875 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m6876 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m6877 ();
extern "C" void UnicodeEncoding_GetDecoder_m6878 ();
extern "C" void UnicodeEncoding_GetPreamble_m6879 ();
extern "C" void UnicodeEncoding_Equals_m6880 ();
extern "C" void UnicodeEncoding_GetHashCode_m6881 ();
extern "C" void UnicodeEncoding_CopyChars_m6882 ();
extern "C" void CompressedStack__ctor_m6883 ();
extern "C" void CompressedStack__ctor_m6884 ();
extern "C" void CompressedStack_CreateCopy_m6885 ();
extern "C" void CompressedStack_Capture_m6886 ();
extern "C" void CompressedStack_GetObjectData_m6887 ();
extern "C" void CompressedStack_IsEmpty_m6888 ();
extern "C" void EventWaitHandle__ctor_m6889 ();
extern "C" void EventWaitHandle_IsManualReset_m6890 ();
extern "C" void EventWaitHandle_Reset_m2707 ();
extern "C" void EventWaitHandle_Set_m2705 ();
extern "C" void ExecutionContext__ctor_m6891 ();
extern "C" void ExecutionContext__ctor_m6892 ();
extern "C" void ExecutionContext__ctor_m6893 ();
extern "C" void ExecutionContext_Capture_m6894 ();
extern "C" void ExecutionContext_GetObjectData_m6895 ();
extern "C" void ExecutionContext_get_SecurityContext_m6896 ();
extern "C" void ExecutionContext_set_SecurityContext_m6897 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m6898 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m6899 ();
extern "C" void Interlocked_CompareExchange_m6900 ();
extern "C" void ManualResetEvent__ctor_m2704 ();
extern "C" void Monitor_Enter_m1589 ();
extern "C" void Monitor_Exit_m1590 ();
extern "C" void Monitor_Monitor_pulse_m6901 ();
extern "C" void Monitor_Monitor_test_synchronised_m6902 ();
extern "C" void Monitor_Pulse_m6903 ();
extern "C" void Monitor_Monitor_wait_m6904 ();
extern "C" void Monitor_Wait_m6905 ();
extern "C" void Mutex__ctor_m6906 ();
extern "C" void Mutex_CreateMutex_internal_m6907 ();
extern "C" void Mutex_ReleaseMutex_internal_m6908 ();
extern "C" void Mutex_ReleaseMutex_m6909 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m6910 ();
extern "C" void NativeEventCalls_SetEvent_internal_m6911 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m6912 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m6913 ();
extern "C" void SynchronizationLockException__ctor_m6914 ();
extern "C" void SynchronizationLockException__ctor_m6915 ();
extern "C" void SynchronizationLockException__ctor_m6916 ();
extern "C" void Thread__ctor_m6917 ();
extern "C" void Thread__cctor_m6918 ();
extern "C" void Thread_get_CurrentContext_m6919 ();
extern "C" void Thread_CurrentThread_internal_m6920 ();
extern "C" void Thread_get_CurrentThread_m6921 ();
extern "C" void Thread_FreeLocalSlotValues_m6922 ();
extern "C" void Thread_GetDomainID_m6923 ();
extern "C" void Thread_Thread_internal_m6924 ();
extern "C" void Thread_Thread_init_m6925 ();
extern "C" void Thread_GetCachedCurrentCulture_m6926 ();
extern "C" void Thread_GetSerializedCurrentCulture_m6927 ();
extern "C" void Thread_SetCachedCurrentCulture_m6928 ();
extern "C" void Thread_GetCachedCurrentUICulture_m6929 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m6930 ();
extern "C" void Thread_SetCachedCurrentUICulture_m6931 ();
extern "C" void Thread_get_CurrentCulture_m6932 ();
extern "C" void Thread_get_CurrentUICulture_m6933 ();
extern "C" void Thread_set_IsBackground_m6934 ();
extern "C" void Thread_SetName_internal_m6935 ();
extern "C" void Thread_set_Name_m6936 ();
extern "C" void Thread_Start_m6937 ();
extern "C" void Thread_Thread_free_internal_m6938 ();
extern "C" void Thread_Finalize_m6939 ();
extern "C" void Thread_SetState_m6940 ();
extern "C" void Thread_ClrState_m6941 ();
extern "C" void Thread_GetNewManagedId_m6942 ();
extern "C" void Thread_GetNewManagedId_internal_m6943 ();
extern "C" void Thread_get_ExecutionContext_m6944 ();
extern "C" void Thread_get_ManagedThreadId_m6945 ();
extern "C" void Thread_GetHashCode_m6946 ();
extern "C" void Thread_GetCompressedStack_m6947 ();
extern "C" void ThreadAbortException__ctor_m6948 ();
extern "C" void ThreadAbortException__ctor_m6949 ();
extern "C" void ThreadInterruptedException__ctor_m6950 ();
extern "C" void ThreadInterruptedException__ctor_m6951 ();
extern "C" void ThreadPool_QueueUserWorkItem_m6952 ();
extern "C" void ThreadStateException__ctor_m6953 ();
extern "C" void ThreadStateException__ctor_m6954 ();
extern "C" void TimerComparer__ctor_m6955 ();
extern "C" void TimerComparer_Compare_m6956 ();
extern "C" void Scheduler__ctor_m6957 ();
extern "C" void Scheduler__cctor_m6958 ();
extern "C" void Scheduler_get_Instance_m6959 ();
extern "C" void Scheduler_Remove_m6960 ();
extern "C" void Scheduler_Change_m6961 ();
extern "C" void Scheduler_Add_m6962 ();
extern "C" void Scheduler_InternalRemove_m6963 ();
extern "C" void Scheduler_SchedulerThread_m6964 ();
extern "C" void Scheduler_ShrinkIfNeeded_m6965 ();
extern "C" void Timer__cctor_m6966 ();
extern "C" void Timer_Change_m6967 ();
extern "C" void Timer_Dispose_m6968 ();
extern "C" void Timer_Change_m6969 ();
extern "C" void WaitHandle__ctor_m6970 ();
extern "C" void WaitHandle__cctor_m6971 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m6972 ();
extern "C" void WaitHandle_get_Handle_m6973 ();
extern "C" void WaitHandle_set_Handle_m6974 ();
extern "C" void WaitHandle_WaitOne_internal_m6975 ();
extern "C" void WaitHandle_Dispose_m6976 ();
extern "C" void WaitHandle_WaitOne_m6977 ();
extern "C" void WaitHandle_WaitOne_m6978 ();
extern "C" void WaitHandle_CheckDisposed_m6979 ();
extern "C" void WaitHandle_Finalize_m6980 ();
extern "C" void AccessViolationException__ctor_m6981 ();
extern "C" void AccessViolationException__ctor_m6982 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m6983 ();
extern "C" void ActivationContext_Finalize_m6984 ();
extern "C" void ActivationContext_Dispose_m6985 ();
extern "C" void ActivationContext_Dispose_m6986 ();
extern "C" void Activator_CreateInstance_m6987 ();
extern "C" void Activator_CreateInstance_m6988 ();
extern "C" void Activator_CreateInstance_m6989 ();
extern "C" void Activator_CreateInstance_m6990 ();
extern "C" void Activator_CreateInstance_m1617 ();
extern "C" void Activator_CheckType_m6991 ();
extern "C" void Activator_CheckAbstractType_m6992 ();
extern "C" void Activator_CreateInstanceInternal_m6993 ();
extern "C" void AppDomain_add_UnhandledException_m592 ();
extern "C" void AppDomain_remove_UnhandledException_m6994 ();
extern "C" void AppDomain_getFriendlyName_m6995 ();
extern "C" void AppDomain_getCurDomain_m6996 ();
extern "C" void AppDomain_get_CurrentDomain_m590 ();
extern "C" void AppDomain_LoadAssembly_m6997 ();
extern "C" void AppDomain_Load_m6998 ();
extern "C" void AppDomain_Load_m6999 ();
extern "C" void AppDomain_InternalSetContext_m7000 ();
extern "C" void AppDomain_InternalGetContext_m7001 ();
extern "C" void AppDomain_InternalGetDefaultContext_m7002 ();
extern "C" void AppDomain_InternalGetProcessGuid_m7003 ();
extern "C" void AppDomain_GetProcessGuid_m7004 ();
extern "C" void AppDomain_ToString_m7005 ();
extern "C" void AppDomain_DoTypeResolve_m7006 ();
extern "C" void AppDomainSetup__ctor_m7007 ();
extern "C" void ApplicationException__ctor_m7008 ();
extern "C" void ApplicationException__ctor_m7009 ();
extern "C" void ApplicationException__ctor_m7010 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7011 ();
extern "C" void ApplicationIdentity_ToString_m7012 ();
extern "C" void ArgumentException__ctor_m7013 ();
extern "C" void ArgumentException__ctor_m587 ();
extern "C" void ArgumentException__ctor_m1681 ();
extern "C" void ArgumentException__ctor_m1563 ();
extern "C" void ArgumentException__ctor_m7014 ();
extern "C" void ArgumentException__ctor_m7015 ();
extern "C" void ArgumentException_get_ParamName_m7016 ();
extern "C" void ArgumentException_get_Message_m7017 ();
extern "C" void ArgumentException_GetObjectData_m7018 ();
extern "C" void ArgumentNullException__ctor_m7019 ();
extern "C" void ArgumentNullException__ctor_m1565 ();
extern "C" void ArgumentNullException__ctor_m1559 ();
extern "C" void ArgumentNullException__ctor_m7020 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1714 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1566 ();
extern "C" void ArgumentOutOfRangeException__ctor_m1560 ();
extern "C" void ArgumentOutOfRangeException__ctor_m7021 ();
extern "C" void ArgumentOutOfRangeException__ctor_m7022 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m7023 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m7024 ();
extern "C" void ArithmeticException__ctor_m7025 ();
extern "C" void ArithmeticException__ctor_m2646 ();
extern "C" void ArithmeticException__ctor_m7026 ();
extern "C" void ArrayTypeMismatchException__ctor_m7027 ();
extern "C" void ArrayTypeMismatchException__ctor_m7028 ();
extern "C" void ArrayTypeMismatchException__ctor_m7029 ();
extern "C" void BitConverter__cctor_m7030 ();
extern "C" void BitConverter_AmILittleEndian_m7031 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m7032 ();
extern "C" void BitConverter_DoubleToInt64Bits_m7033 ();
extern "C" void BitConverter_GetBytes_m7034 ();
extern "C" void BitConverter_GetBytes_m7035 ();
extern "C" void BitConverter_PutBytes_m7036 ();
extern "C" void BitConverter_ToInt64_m7037 ();
extern "C" void BitConverter_ToString_m2702 ();
extern "C" void BitConverter_ToString_m7038 ();
extern "C" void Buffer_ByteLength_m7039 ();
extern "C" void Buffer_BlockCopy_m1625 ();
extern "C" void Buffer_ByteLengthInternal_m7040 ();
extern "C" void Buffer_BlockCopyInternal_m7041 ();
extern "C" void CharEnumerator__ctor_m7042 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m7043 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m7044 ();
extern "C" void CharEnumerator_get_Current_m7045 ();
extern "C" void CharEnumerator_Clone_m7046 ();
extern "C" void CharEnumerator_MoveNext_m7047 ();
extern "C" void CharEnumerator_Reset_m7048 ();
extern "C" void Console__cctor_m7049 ();
extern "C" void Console_SetEncodings_m7050 ();
extern "C" void Console_get_Error_m1737 ();
extern "C" void Console_Open_m7051 ();
extern "C" void Console_OpenStandardError_m7052 ();
extern "C" void Console_OpenStandardInput_m7053 ();
extern "C" void Console_OpenStandardOutput_m7054 ();
extern "C" void ContextBoundObject__ctor_m7055 ();
extern "C" void Convert__cctor_m7056 ();
extern "C" void Convert_InternalFromBase64String_m7057 ();
extern "C" void Convert_FromBase64String_m2680 ();
extern "C" void Convert_ToBase64String_m2664 ();
extern "C" void Convert_ToBase64String_m7058 ();
extern "C" void Convert_ToBoolean_m7059 ();
extern "C" void Convert_ToBoolean_m7060 ();
extern "C" void Convert_ToBoolean_m7061 ();
extern "C" void Convert_ToBoolean_m7062 ();
extern "C" void Convert_ToBoolean_m7063 ();
extern "C" void Convert_ToBoolean_m7064 ();
extern "C" void Convert_ToBoolean_m7065 ();
extern "C" void Convert_ToBoolean_m7066 ();
extern "C" void Convert_ToBoolean_m7067 ();
extern "C" void Convert_ToBoolean_m7068 ();
extern "C" void Convert_ToBoolean_m7069 ();
extern "C" void Convert_ToBoolean_m7070 ();
extern "C" void Convert_ToBoolean_m7071 ();
extern "C" void Convert_ToBoolean_m7072 ();
extern "C" void Convert_ToByte_m7073 ();
extern "C" void Convert_ToByte_m7074 ();
extern "C" void Convert_ToByte_m7075 ();
extern "C" void Convert_ToByte_m7076 ();
extern "C" void Convert_ToByte_m7077 ();
extern "C" void Convert_ToByte_m7078 ();
extern "C" void Convert_ToByte_m7079 ();
extern "C" void Convert_ToByte_m7080 ();
extern "C" void Convert_ToByte_m7081 ();
extern "C" void Convert_ToByte_m7082 ();
extern "C" void Convert_ToByte_m7083 ();
extern "C" void Convert_ToByte_m7084 ();
extern "C" void Convert_ToByte_m7085 ();
extern "C" void Convert_ToByte_m7086 ();
extern "C" void Convert_ToByte_m7087 ();
extern "C" void Convert_ToChar_m2681 ();
extern "C" void Convert_ToChar_m7088 ();
extern "C" void Convert_ToChar_m7089 ();
extern "C" void Convert_ToChar_m7090 ();
extern "C" void Convert_ToChar_m7091 ();
extern "C" void Convert_ToChar_m7092 ();
extern "C" void Convert_ToChar_m7093 ();
extern "C" void Convert_ToChar_m7094 ();
extern "C" void Convert_ToChar_m7095 ();
extern "C" void Convert_ToChar_m7096 ();
extern "C" void Convert_ToChar_m7097 ();
extern "C" void Convert_ToDateTime_m7098 ();
extern "C" void Convert_ToDateTime_m7099 ();
extern "C" void Convert_ToDateTime_m7100 ();
extern "C" void Convert_ToDateTime_m7101 ();
extern "C" void Convert_ToDateTime_m7102 ();
extern "C" void Convert_ToDateTime_m7103 ();
extern "C" void Convert_ToDateTime_m7104 ();
extern "C" void Convert_ToDateTime_m7105 ();
extern "C" void Convert_ToDateTime_m7106 ();
extern "C" void Convert_ToDateTime_m7107 ();
extern "C" void Convert_ToDecimal_m7108 ();
extern "C" void Convert_ToDecimal_m7109 ();
extern "C" void Convert_ToDecimal_m7110 ();
extern "C" void Convert_ToDecimal_m7111 ();
extern "C" void Convert_ToDecimal_m7112 ();
extern "C" void Convert_ToDecimal_m7113 ();
extern "C" void Convert_ToDecimal_m7114 ();
extern "C" void Convert_ToDecimal_m7115 ();
extern "C" void Convert_ToDecimal_m7116 ();
extern "C" void Convert_ToDecimal_m7117 ();
extern "C" void Convert_ToDecimal_m7118 ();
extern "C" void Convert_ToDecimal_m7119 ();
extern "C" void Convert_ToDecimal_m7120 ();
extern "C" void Convert_ToDouble_m7121 ();
extern "C" void Convert_ToDouble_m7122 ();
extern "C" void Convert_ToDouble_m7123 ();
extern "C" void Convert_ToDouble_m7124 ();
extern "C" void Convert_ToDouble_m7125 ();
extern "C" void Convert_ToDouble_m7126 ();
extern "C" void Convert_ToDouble_m7127 ();
extern "C" void Convert_ToDouble_m7128 ();
extern "C" void Convert_ToDouble_m7129 ();
extern "C" void Convert_ToDouble_m7130 ();
extern "C" void Convert_ToDouble_m7131 ();
extern "C" void Convert_ToDouble_m7132 ();
extern "C" void Convert_ToDouble_m7133 ();
extern "C" void Convert_ToDouble_m7134 ();
extern "C" void Convert_ToInt16_m7135 ();
extern "C" void Convert_ToInt16_m7136 ();
extern "C" void Convert_ToInt16_m7137 ();
extern "C" void Convert_ToInt16_m7138 ();
extern "C" void Convert_ToInt16_m7139 ();
extern "C" void Convert_ToInt16_m7140 ();
extern "C" void Convert_ToInt16_m7141 ();
extern "C" void Convert_ToInt16_m7142 ();
extern "C" void Convert_ToInt16_m7143 ();
extern "C" void Convert_ToInt16_m7144 ();
extern "C" void Convert_ToInt16_m2651 ();
extern "C" void Convert_ToInt16_m7145 ();
extern "C" void Convert_ToInt16_m7146 ();
extern "C" void Convert_ToInt16_m7147 ();
extern "C" void Convert_ToInt16_m7148 ();
extern "C" void Convert_ToInt16_m7149 ();
extern "C" void Convert_ToInt32_m7150 ();
extern "C" void Convert_ToInt32_m7151 ();
extern "C" void Convert_ToInt32_m7152 ();
extern "C" void Convert_ToInt32_m7153 ();
extern "C" void Convert_ToInt32_m7154 ();
extern "C" void Convert_ToInt32_m7155 ();
extern "C" void Convert_ToInt32_m7156 ();
extern "C" void Convert_ToInt32_m7157 ();
extern "C" void Convert_ToInt32_m7158 ();
extern "C" void Convert_ToInt32_m7159 ();
extern "C" void Convert_ToInt32_m7160 ();
extern "C" void Convert_ToInt32_m7161 ();
extern "C" void Convert_ToInt32_m7162 ();
extern "C" void Convert_ToInt32_m7163 ();
extern "C" void Convert_ToInt32_m2690 ();
extern "C" void Convert_ToInt64_m7164 ();
extern "C" void Convert_ToInt64_m7165 ();
extern "C" void Convert_ToInt64_m7166 ();
extern "C" void Convert_ToInt64_m7167 ();
extern "C" void Convert_ToInt64_m7168 ();
extern "C" void Convert_ToInt64_m7169 ();
extern "C" void Convert_ToInt64_m7170 ();
extern "C" void Convert_ToInt64_m7171 ();
extern "C" void Convert_ToInt64_m7172 ();
extern "C" void Convert_ToInt64_m7173 ();
extern "C" void Convert_ToInt64_m7174 ();
extern "C" void Convert_ToInt64_m7175 ();
extern "C" void Convert_ToInt64_m7176 ();
extern "C" void Convert_ToInt64_m7177 ();
extern "C" void Convert_ToInt64_m7178 ();
extern "C" void Convert_ToInt64_m7179 ();
extern "C" void Convert_ToInt64_m7180 ();
extern "C" void Convert_ToSByte_m7181 ();
extern "C" void Convert_ToSByte_m7182 ();
extern "C" void Convert_ToSByte_m7183 ();
extern "C" void Convert_ToSByte_m7184 ();
extern "C" void Convert_ToSByte_m7185 ();
extern "C" void Convert_ToSByte_m7186 ();
extern "C" void Convert_ToSByte_m7187 ();
extern "C" void Convert_ToSByte_m7188 ();
extern "C" void Convert_ToSByte_m7189 ();
extern "C" void Convert_ToSByte_m7190 ();
extern "C" void Convert_ToSByte_m7191 ();
extern "C" void Convert_ToSByte_m7192 ();
extern "C" void Convert_ToSByte_m7193 ();
extern "C" void Convert_ToSByte_m7194 ();
extern "C" void Convert_ToSingle_m7195 ();
extern "C" void Convert_ToSingle_m7196 ();
extern "C" void Convert_ToSingle_m7197 ();
extern "C" void Convert_ToSingle_m7198 ();
extern "C" void Convert_ToSingle_m7199 ();
extern "C" void Convert_ToSingle_m7200 ();
extern "C" void Convert_ToSingle_m7201 ();
extern "C" void Convert_ToSingle_m7202 ();
extern "C" void Convert_ToSingle_m7203 ();
extern "C" void Convert_ToSingle_m7204 ();
extern "C" void Convert_ToSingle_m7205 ();
extern "C" void Convert_ToSingle_m7206 ();
extern "C" void Convert_ToSingle_m7207 ();
extern "C" void Convert_ToSingle_m7208 ();
extern "C" void Convert_ToString_m7209 ();
extern "C" void Convert_ToString_m7210 ();
extern "C" void Convert_ToUInt16_m7211 ();
extern "C" void Convert_ToUInt16_m7212 ();
extern "C" void Convert_ToUInt16_m7213 ();
extern "C" void Convert_ToUInt16_m7214 ();
extern "C" void Convert_ToUInt16_m7215 ();
extern "C" void Convert_ToUInt16_m7216 ();
extern "C" void Convert_ToUInt16_m7217 ();
extern "C" void Convert_ToUInt16_m7218 ();
extern "C" void Convert_ToUInt16_m7219 ();
extern "C" void Convert_ToUInt16_m7220 ();
extern "C" void Convert_ToUInt16_m7221 ();
extern "C" void Convert_ToUInt16_m7222 ();
extern "C" void Convert_ToUInt16_m7223 ();
extern "C" void Convert_ToUInt16_m7224 ();
extern "C" void Convert_ToUInt32_m7225 ();
extern "C" void Convert_ToUInt32_m7226 ();
extern "C" void Convert_ToUInt32_m7227 ();
extern "C" void Convert_ToUInt32_m7228 ();
extern "C" void Convert_ToUInt32_m7229 ();
extern "C" void Convert_ToUInt32_m7230 ();
extern "C" void Convert_ToUInt32_m7231 ();
extern "C" void Convert_ToUInt32_m7232 ();
extern "C" void Convert_ToUInt32_m7233 ();
extern "C" void Convert_ToUInt32_m7234 ();
extern "C" void Convert_ToUInt32_m7235 ();
extern "C" void Convert_ToUInt32_m7236 ();
extern "C" void Convert_ToUInt32_m7237 ();
extern "C" void Convert_ToUInt32_m7238 ();
extern "C" void Convert_ToUInt64_m7239 ();
extern "C" void Convert_ToUInt64_m7240 ();
extern "C" void Convert_ToUInt64_m7241 ();
extern "C" void Convert_ToUInt64_m7242 ();
extern "C" void Convert_ToUInt64_m7243 ();
extern "C" void Convert_ToUInt64_m7244 ();
extern "C" void Convert_ToUInt64_m7245 ();
extern "C" void Convert_ToUInt64_m7246 ();
extern "C" void Convert_ToUInt64_m7247 ();
extern "C" void Convert_ToUInt64_m7248 ();
extern "C" void Convert_ToUInt64_m7249 ();
extern "C" void Convert_ToUInt64_m7250 ();
extern "C" void Convert_ToUInt64_m7251 ();
extern "C" void Convert_ToUInt64_m7252 ();
extern "C" void Convert_ToUInt64_m7253 ();
extern "C" void Convert_ChangeType_m7254 ();
extern "C" void Convert_ToType_m7255 ();
extern "C" void DBNull__ctor_m7256 ();
extern "C" void DBNull__ctor_m7257 ();
extern "C" void DBNull__cctor_m7258 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m7259 ();
extern "C" void DBNull_System_IConvertible_ToByte_m7260 ();
extern "C" void DBNull_System_IConvertible_ToChar_m7261 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m7262 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m7263 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m7264 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m7265 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m7266 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m7267 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m7268 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m7269 ();
extern "C" void DBNull_System_IConvertible_ToType_m7270 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m7271 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m7272 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m7273 ();
extern "C" void DBNull_GetObjectData_m7274 ();
extern "C" void DBNull_ToString_m7275 ();
extern "C" void DBNull_ToString_m7276 ();
extern "C" void DateTime__ctor_m7277 ();
extern "C" void DateTime__ctor_m7278 ();
extern "C" void DateTime__ctor_m626 ();
extern "C" void DateTime__ctor_m7279 ();
extern "C" void DateTime__ctor_m7280 ();
extern "C" void DateTime__cctor_m7281 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m7282 ();
extern "C" void DateTime_System_IConvertible_ToByte_m7283 ();
extern "C" void DateTime_System_IConvertible_ToChar_m7284 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m7285 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m7286 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m7287 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m7288 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m7289 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m7290 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m7291 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m7292 ();
extern "C" void DateTime_System_IConvertible_ToType_m7293 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m7294 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m7295 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m7296 ();
extern "C" void DateTime_AbsoluteDays_m7297 ();
extern "C" void DateTime_FromTicks_m7298 ();
extern "C" void DateTime_get_Month_m7299 ();
extern "C" void DateTime_get_Day_m7300 ();
extern "C" void DateTime_get_DayOfWeek_m7301 ();
extern "C" void DateTime_get_Hour_m7302 ();
extern "C" void DateTime_get_Minute_m7303 ();
extern "C" void DateTime_get_Second_m7304 ();
extern "C" void DateTime_GetTimeMonotonic_m7305 ();
extern "C" void DateTime_GetNow_m7306 ();
extern "C" void DateTime_get_Now_m629 ();
extern "C" void DateTime_get_Ticks_m2703 ();
extern "C" void DateTime_get_Today_m7307 ();
extern "C" void DateTime_get_UtcNow_m2677 ();
extern "C" void DateTime_get_Year_m7308 ();
extern "C" void DateTime_get_Kind_m7309 ();
extern "C" void DateTime_Add_m7310 ();
extern "C" void DateTime_AddTicks_m7311 ();
extern "C" void DateTime_AddMilliseconds_m1604 ();
extern "C" void DateTime_AddSeconds_m627 ();
extern "C" void DateTime_Compare_m7312 ();
extern "C" void DateTime_CompareTo_m7313 ();
extern "C" void DateTime_CompareTo_m7314 ();
extern "C" void DateTime_Equals_m7315 ();
extern "C" void DateTime_FromBinary_m7316 ();
extern "C" void DateTime_SpecifyKind_m7317 ();
extern "C" void DateTime_DaysInMonth_m7318 ();
extern "C" void DateTime_Equals_m7319 ();
extern "C" void DateTime_CheckDateTimeKind_m7320 ();
extern "C" void DateTime_GetHashCode_m7321 ();
extern "C" void DateTime_IsLeapYear_m7322 ();
extern "C" void DateTime_Parse_m7323 ();
extern "C" void DateTime_Parse_m7324 ();
extern "C" void DateTime_CoreParse_m7325 ();
extern "C" void DateTime_YearMonthDayFormats_m7326 ();
extern "C" void DateTime__ParseNumber_m7327 ();
extern "C" void DateTime__ParseEnum_m7328 ();
extern "C" void DateTime__ParseString_m7329 ();
extern "C" void DateTime__ParseAmPm_m7330 ();
extern "C" void DateTime__ParseTimeSeparator_m7331 ();
extern "C" void DateTime__ParseDateSeparator_m7332 ();
extern "C" void DateTime_IsLetter_m7333 ();
extern "C" void DateTime__DoParse_m7334 ();
extern "C" void DateTime_ParseExact_m2652 ();
extern "C" void DateTime_ParseExact_m7335 ();
extern "C" void DateTime_CheckStyle_m7336 ();
extern "C" void DateTime_ParseExact_m7337 ();
extern "C" void DateTime_Subtract_m7338 ();
extern "C" void DateTime_ToString_m7339 ();
extern "C" void DateTime_ToString_m7340 ();
extern "C" void DateTime_ToString_m7341 ();
extern "C" void DateTime_ToLocalTime_m1651 ();
extern "C" void DateTime_ToUniversalTime_m7342 ();
extern "C" void DateTime_op_Addition_m7343 ();
extern "C" void DateTime_op_Equality_m7344 ();
extern "C" void DateTime_op_GreaterThan_m1679 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m1605 ();
extern "C" void DateTime_op_Inequality_m7345 ();
extern "C" void DateTime_op_LessThan_m1678 ();
extern "C" void DateTime_op_LessThanOrEqual_m1677 ();
extern "C" void DateTime_op_Subtraction_m7346 ();
extern "C" void DateTimeOffset__ctor_m7347 ();
extern "C" void DateTimeOffset__ctor_m7348 ();
extern "C" void DateTimeOffset__ctor_m7349 ();
extern "C" void DateTimeOffset__ctor_m7350 ();
extern "C" void DateTimeOffset__cctor_m7351 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m7352 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m7353 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7354 ();
extern "C" void DateTimeOffset_CompareTo_m7355 ();
extern "C" void DateTimeOffset_Equals_m7356 ();
extern "C" void DateTimeOffset_Equals_m7357 ();
extern "C" void DateTimeOffset_GetHashCode_m7358 ();
extern "C" void DateTimeOffset_ToString_m7359 ();
extern "C" void DateTimeOffset_ToString_m7360 ();
extern "C" void DateTimeOffset_get_DateTime_m7361 ();
extern "C" void DateTimeOffset_get_Offset_m7362 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m7363 ();
extern "C" void DateTimeUtils_CountRepeat_m7364 ();
extern "C" void DateTimeUtils_ZeroPad_m7365 ();
extern "C" void DateTimeUtils_ParseQuotedString_m7366 ();
extern "C" void DateTimeUtils_GetStandardPattern_m7367 ();
extern "C" void DateTimeUtils_GetStandardPattern_m7368 ();
extern "C" void DateTimeUtils_ToString_m7369 ();
extern "C" void DateTimeUtils_ToString_m7370 ();
extern "C" void DelegateEntry__ctor_m7371 ();
extern "C" void DelegateEntry_DeserializeDelegate_m7372 ();
extern "C" void DelegateSerializationHolder__ctor_m7373 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m7374 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m7375 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m7376 ();
extern "C" void DivideByZeroException__ctor_m7377 ();
extern "C" void DivideByZeroException__ctor_m7378 ();
extern "C" void DllNotFoundException__ctor_m7379 ();
extern "C" void DllNotFoundException__ctor_m7380 ();
extern "C" void EntryPointNotFoundException__ctor_m7381 ();
extern "C" void EntryPointNotFoundException__ctor_m7382 ();
extern "C" void SByteComparer__ctor_m7383 ();
extern "C" void SByteComparer_Compare_m7384 ();
extern "C" void SByteComparer_Compare_m7385 ();
extern "C" void ShortComparer__ctor_m7386 ();
extern "C" void ShortComparer_Compare_m7387 ();
extern "C" void ShortComparer_Compare_m7388 ();
extern "C" void IntComparer__ctor_m7389 ();
extern "C" void IntComparer_Compare_m7390 ();
extern "C" void IntComparer_Compare_m7391 ();
extern "C" void LongComparer__ctor_m7392 ();
extern "C" void LongComparer_Compare_m7393 ();
extern "C" void LongComparer_Compare_m7394 ();
extern "C" void MonoEnumInfo__ctor_m7395 ();
extern "C" void MonoEnumInfo__cctor_m7396 ();
extern "C" void MonoEnumInfo_get_enum_info_m7397 ();
extern "C" void MonoEnumInfo_get_Cache_m7398 ();
extern "C" void MonoEnumInfo_GetInfo_m7399 ();
extern "C" void Environment_get_SocketSecurityEnabled_m7400 ();
extern "C" void Environment_get_NewLine_m1636 ();
extern "C" void Environment_get_Platform_m7401 ();
extern "C" void Environment_GetOSVersionString_m7402 ();
extern "C" void Environment_get_OSVersion_m7403 ();
extern "C" void Environment_internalGetEnvironmentVariable_m7404 ();
extern "C" void Environment_GetEnvironmentVariable_m2701 ();
extern "C" void Environment_GetWindowsFolderPath_m7405 ();
extern "C" void Environment_GetFolderPath_m2687 ();
extern "C" void Environment_ReadXdgUserDir_m7406 ();
extern "C" void Environment_InternalGetFolderPath_m7407 ();
extern "C" void Environment_get_IsRunningOnWindows_m7408 ();
extern "C" void Environment_GetMachineConfigPath_m7409 ();
extern "C" void Environment_internalGetHome_m7410 ();
extern "C" void EventArgs__ctor_m7411 ();
extern "C" void EventArgs__cctor_m7412 ();
extern "C" void ExecutionEngineException__ctor_m7413 ();
extern "C" void ExecutionEngineException__ctor_m7414 ();
extern "C" void FieldAccessException__ctor_m7415 ();
extern "C" void FieldAccessException__ctor_m7416 ();
extern "C" void FieldAccessException__ctor_m7417 ();
extern "C" void FlagsAttribute__ctor_m7418 ();
extern "C" void FormatException__ctor_m7419 ();
extern "C" void FormatException__ctor_m1591 ();
extern "C" void FormatException__ctor_m1748 ();
extern "C" void GC_SuppressFinalize_m1798 ();
extern "C" void Guid__ctor_m7420 ();
extern "C" void Guid__ctor_m7421 ();
extern "C" void Guid__cctor_m7422 ();
extern "C" void Guid_CheckNull_m7423 ();
extern "C" void Guid_CheckLength_m7424 ();
extern "C" void Guid_CheckArray_m7425 ();
extern "C" void Guid_Compare_m7426 ();
extern "C" void Guid_CompareTo_m7427 ();
extern "C" void Guid_Equals_m7428 ();
extern "C" void Guid_CompareTo_m7429 ();
extern "C" void Guid_Equals_m7430 ();
extern "C" void Guid_GetHashCode_m7431 ();
extern "C" void Guid_ToHex_m7432 ();
extern "C" void Guid_NewGuid_m7433 ();
extern "C" void Guid_AppendInt_m7434 ();
extern "C" void Guid_AppendShort_m7435 ();
extern "C" void Guid_AppendByte_m7436 ();
extern "C" void Guid_BaseToString_m7437 ();
extern "C" void Guid_ToString_m7438 ();
extern "C" void Guid_ToString_m7439 ();
extern "C" void Guid_ToString_m7440 ();
extern "C" void IndexOutOfRangeException__ctor_m7441 ();
extern "C" void IndexOutOfRangeException__ctor_m612 ();
extern "C" void IndexOutOfRangeException__ctor_m7442 ();
extern "C" void InvalidCastException__ctor_m7443 ();
extern "C" void InvalidCastException__ctor_m7444 ();
extern "C" void InvalidCastException__ctor_m7445 ();
extern "C" void InvalidOperationException__ctor_m1564 ();
extern "C" void InvalidOperationException__ctor_m1557 ();
extern "C" void InvalidOperationException__ctor_m7446 ();
extern "C" void InvalidOperationException__ctor_m7447 ();
extern "C" void LocalDataStoreSlot__ctor_m7448 ();
extern "C" void LocalDataStoreSlot__cctor_m7449 ();
extern "C" void LocalDataStoreSlot_Finalize_m7450 ();
extern "C" void Math_Abs_m7451 ();
extern "C" void Math_Abs_m7452 ();
extern "C" void Math_Abs_m7453 ();
extern "C" void Math_Floor_m7454 ();
extern "C" void Math_Max_m2657 ();
extern "C" void Math_Min_m1797 ();
extern "C" void Math_Round_m7455 ();
extern "C" void Math_Round_m7456 ();
extern "C" void Math_Pow_m7457 ();
extern "C" void Math_Sqrt_m7458 ();
extern "C" void MemberAccessException__ctor_m7459 ();
extern "C" void MemberAccessException__ctor_m7460 ();
extern "C" void MemberAccessException__ctor_m7461 ();
extern "C" void MethodAccessException__ctor_m7462 ();
extern "C" void MethodAccessException__ctor_m7463 ();
extern "C" void MissingFieldException__ctor_m7464 ();
extern "C" void MissingFieldException__ctor_m7465 ();
extern "C" void MissingFieldException__ctor_m7466 ();
extern "C" void MissingFieldException_get_Message_m7467 ();
extern "C" void MissingMemberException__ctor_m7468 ();
extern "C" void MissingMemberException__ctor_m7469 ();
extern "C" void MissingMemberException__ctor_m7470 ();
extern "C" void MissingMemberException__ctor_m7471 ();
extern "C" void MissingMemberException_GetObjectData_m7472 ();
extern "C" void MissingMemberException_get_Message_m7473 ();
extern "C" void MissingMethodException__ctor_m7474 ();
extern "C" void MissingMethodException__ctor_m7475 ();
extern "C" void MissingMethodException__ctor_m7476 ();
extern "C" void MissingMethodException__ctor_m7477 ();
extern "C" void MissingMethodException_get_Message_m7478 ();
extern "C" void MonoAsyncCall__ctor_m7479 ();
extern "C" void AttributeInfo__ctor_m7480 ();
extern "C" void AttributeInfo_get_Usage_m7481 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m7482 ();
extern "C" void MonoCustomAttrs__cctor_m7483 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m7484 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m7485 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m7486 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m7487 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m7488 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m7489 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m7490 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesDataInternal_m7491 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesData_m7492 ();
extern "C" void MonoCustomAttrs_IsDefined_m7493 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m7494 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m7495 ();
extern "C" void MonoCustomAttrs_GetBase_m7496 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m7497 ();
extern "C" void MonoTouchAOTHelper__cctor_m7498 ();
extern "C" void MonoTypeInfo__ctor_m7499 ();
extern "C" void MonoType_get_attributes_m7500 ();
extern "C" void MonoType_GetDefaultConstructor_m7501 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m7502 ();
extern "C" void MonoType_GetConstructorImpl_m7503 ();
extern "C" void MonoType_GetConstructors_internal_m7504 ();
extern "C" void MonoType_GetConstructors_m7505 ();
extern "C" void MonoType_InternalGetEvent_m7506 ();
extern "C" void MonoType_GetEvent_m7507 ();
extern "C" void MonoType_GetField_m7508 ();
extern "C" void MonoType_GetFields_internal_m7509 ();
extern "C" void MonoType_GetFields_m7510 ();
extern "C" void MonoType_GetInterfaces_m7511 ();
extern "C" void MonoType_GetMethodsByName_m7512 ();
extern "C" void MonoType_GetMethods_m7513 ();
extern "C" void MonoType_GetMethodImpl_m7514 ();
extern "C" void MonoType_GetPropertiesByName_m7515 ();
extern "C" void MonoType_GetPropertyImpl_m7516 ();
extern "C" void MonoType_HasElementTypeImpl_m7517 ();
extern "C" void MonoType_IsArrayImpl_m7518 ();
extern "C" void MonoType_IsByRefImpl_m7519 ();
extern "C" void MonoType_IsPointerImpl_m7520 ();
extern "C" void MonoType_IsPrimitiveImpl_m7521 ();
extern "C" void MonoType_IsSubclassOf_m7522 ();
extern "C" void MonoType_InvokeMember_m7523 ();
extern "C" void MonoType_GetElementType_m7524 ();
extern "C" void MonoType_get_UnderlyingSystemType_m7525 ();
extern "C" void MonoType_get_Assembly_m7526 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m7527 ();
extern "C" void MonoType_getFullName_m7528 ();
extern "C" void MonoType_get_BaseType_m7529 ();
extern "C" void MonoType_get_FullName_m7530 ();
extern "C" void MonoType_IsDefined_m7531 ();
extern "C" void MonoType_GetCustomAttributes_m7532 ();
extern "C" void MonoType_GetCustomAttributes_m7533 ();
extern "C" void MonoType_get_MemberType_m7534 ();
extern "C" void MonoType_get_Name_m7535 ();
extern "C" void MonoType_get_Namespace_m7536 ();
extern "C" void MonoType_get_Module_m7537 ();
extern "C" void MonoType_get_DeclaringType_m7538 ();
extern "C" void MonoType_get_ReflectedType_m7539 ();
extern "C" void MonoType_get_TypeHandle_m7540 ();
extern "C" void MonoType_GetObjectData_m7541 ();
extern "C" void MonoType_ToString_m7542 ();
extern "C" void MonoType_GetGenericArguments_m7543 ();
extern "C" void MonoType_get_ContainsGenericParameters_m7544 ();
extern "C" void MonoType_get_IsGenericParameter_m7545 ();
extern "C" void MonoType_GetGenericTypeDefinition_m7546 ();
extern "C" void MonoType_CheckMethodSecurity_m7547 ();
extern "C" void MonoType_ReorderParamArrayArguments_m7548 ();
extern "C" void MulticastNotSupportedException__ctor_m7549 ();
extern "C" void MulticastNotSupportedException__ctor_m7550 ();
extern "C" void MulticastNotSupportedException__ctor_m7551 ();
extern "C" void NonSerializedAttribute__ctor_m7552 ();
extern "C" void NotImplementedException__ctor_m7553 ();
extern "C" void NotImplementedException__ctor_m1616 ();
extern "C" void NotImplementedException__ctor_m7554 ();
extern "C" void NotSupportedException__ctor_m1615 ();
extern "C" void NotSupportedException__ctor_m1576 ();
extern "C" void NotSupportedException__ctor_m7555 ();
extern "C" void NullReferenceException__ctor_m7556 ();
extern "C" void NullReferenceException__ctor_m585 ();
extern "C" void NullReferenceException__ctor_m7557 ();
extern "C" void CustomInfo__ctor_m7558 ();
extern "C" void CustomInfo_GetActiveSection_m7559 ();
extern "C" void CustomInfo_Parse_m7560 ();
extern "C" void CustomInfo_Format_m7561 ();
extern "C" void NumberFormatter__ctor_m7562 ();
extern "C" void NumberFormatter__cctor_m7563 ();
extern "C" void NumberFormatter_GetFormatterTables_m7564 ();
extern "C" void NumberFormatter_GetTenPowerOf_m7565 ();
extern "C" void NumberFormatter_InitDecHexDigits_m7566 ();
extern "C" void NumberFormatter_InitDecHexDigits_m7567 ();
extern "C" void NumberFormatter_InitDecHexDigits_m7568 ();
extern "C" void NumberFormatter_FastToDecHex_m7569 ();
extern "C" void NumberFormatter_ToDecHex_m7570 ();
extern "C" void NumberFormatter_FastDecHexLen_m7571 ();
extern "C" void NumberFormatter_DecHexLen_m7572 ();
extern "C" void NumberFormatter_DecHexLen_m7573 ();
extern "C" void NumberFormatter_ScaleOrder_m7574 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m7575 ();
extern "C" void NumberFormatter_ParsePrecision_m7576 ();
extern "C" void NumberFormatter_Init_m7577 ();
extern "C" void NumberFormatter_InitHex_m7578 ();
extern "C" void NumberFormatter_Init_m7579 ();
extern "C" void NumberFormatter_Init_m7580 ();
extern "C" void NumberFormatter_Init_m7581 ();
extern "C" void NumberFormatter_Init_m7582 ();
extern "C" void NumberFormatter_Init_m7583 ();
extern "C" void NumberFormatter_Init_m7584 ();
extern "C" void NumberFormatter_ResetCharBuf_m7585 ();
extern "C" void NumberFormatter_Resize_m7586 ();
extern "C" void NumberFormatter_Append_m7587 ();
extern "C" void NumberFormatter_Append_m7588 ();
extern "C" void NumberFormatter_Append_m7589 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m7590 ();
extern "C" void NumberFormatter_set_CurrentCulture_m7591 ();
extern "C" void NumberFormatter_get_IntegerDigits_m7592 ();
extern "C" void NumberFormatter_get_DecimalDigits_m7593 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m7594 ();
extern "C" void NumberFormatter_get_IsZero_m7595 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m7596 ();
extern "C" void NumberFormatter_RoundPos_m7597 ();
extern "C" void NumberFormatter_RoundDecimal_m7598 ();
extern "C" void NumberFormatter_RoundBits_m7599 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m7600 ();
extern "C" void NumberFormatter_AddOneToDecHex_m7601 ();
extern "C" void NumberFormatter_AddOneToDecHex_m7602 ();
extern "C" void NumberFormatter_CountTrailingZeros_m7603 ();
extern "C" void NumberFormatter_CountTrailingZeros_m7604 ();
extern "C" void NumberFormatter_GetInstance_m7605 ();
extern "C" void NumberFormatter_Release_m7606 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m7607 ();
extern "C" void NumberFormatter_NumberToString_m7608 ();
extern "C" void NumberFormatter_NumberToString_m7609 ();
extern "C" void NumberFormatter_NumberToString_m7610 ();
extern "C" void NumberFormatter_NumberToString_m7611 ();
extern "C" void NumberFormatter_NumberToString_m7612 ();
extern "C" void NumberFormatter_NumberToString_m7613 ();
extern "C" void NumberFormatter_NumberToString_m7614 ();
extern "C" void NumberFormatter_NumberToString_m7615 ();
extern "C" void NumberFormatter_NumberToString_m7616 ();
extern "C" void NumberFormatter_NumberToString_m7617 ();
extern "C" void NumberFormatter_NumberToString_m7618 ();
extern "C" void NumberFormatter_NumberToString_m7619 ();
extern "C" void NumberFormatter_NumberToString_m7620 ();
extern "C" void NumberFormatter_NumberToString_m7621 ();
extern "C" void NumberFormatter_NumberToString_m7622 ();
extern "C" void NumberFormatter_NumberToString_m7623 ();
extern "C" void NumberFormatter_NumberToString_m7624 ();
extern "C" void NumberFormatter_FastIntegerToString_m7625 ();
extern "C" void NumberFormatter_IntegerToString_m7626 ();
extern "C" void NumberFormatter_NumberToString_m7627 ();
extern "C" void NumberFormatter_FormatCurrency_m7628 ();
extern "C" void NumberFormatter_FormatDecimal_m7629 ();
extern "C" void NumberFormatter_FormatHexadecimal_m7630 ();
extern "C" void NumberFormatter_FormatFixedPoint_m7631 ();
extern "C" void NumberFormatter_FormatRoundtrip_m7632 ();
extern "C" void NumberFormatter_FormatRoundtrip_m7633 ();
extern "C" void NumberFormatter_FormatGeneral_m7634 ();
extern "C" void NumberFormatter_FormatNumber_m7635 ();
extern "C" void NumberFormatter_FormatPercent_m7636 ();
extern "C" void NumberFormatter_FormatExponential_m7637 ();
extern "C" void NumberFormatter_FormatExponential_m7638 ();
extern "C" void NumberFormatter_FormatCustom_m7639 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m7640 ();
extern "C" void NumberFormatter_IsZeroOnly_m7641 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m7642 ();
extern "C" void NumberFormatter_AppendIntegerString_m7643 ();
extern "C" void NumberFormatter_AppendIntegerString_m7644 ();
extern "C" void NumberFormatter_AppendDecimalString_m7645 ();
extern "C" void NumberFormatter_AppendDecimalString_m7646 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m7647 ();
extern "C" void NumberFormatter_AppendExponent_m7648 ();
extern "C" void NumberFormatter_AppendOneDigit_m7649 ();
extern "C" void NumberFormatter_FastAppendDigits_m7650 ();
extern "C" void NumberFormatter_AppendDigits_m7651 ();
extern "C" void NumberFormatter_AppendDigits_m7652 ();
extern "C" void NumberFormatter_Multiply10_m7653 ();
extern "C" void NumberFormatter_Divide10_m7654 ();
extern "C" void NumberFormatter_GetClone_m7655 ();
extern "C" void ObjectDisposedException__ctor_m1800 ();
extern "C" void ObjectDisposedException__ctor_m7656 ();
extern "C" void ObjectDisposedException__ctor_m7657 ();
extern "C" void ObjectDisposedException_get_Message_m7658 ();
extern "C" void ObjectDisposedException_GetObjectData_m7659 ();
extern "C" void OperatingSystem__ctor_m7660 ();
extern "C" void OperatingSystem_get_Platform_m7661 ();
extern "C" void OperatingSystem_Clone_m7662 ();
extern "C" void OperatingSystem_GetObjectData_m7663 ();
extern "C" void OperatingSystem_ToString_m7664 ();
extern "C" void OutOfMemoryException__ctor_m7665 ();
extern "C" void OutOfMemoryException__ctor_m7666 ();
extern "C" void OverflowException__ctor_m7667 ();
extern "C" void OverflowException__ctor_m7668 ();
extern "C" void OverflowException__ctor_m7669 ();
extern "C" void RankException__ctor_m7670 ();
extern "C" void RankException__ctor_m7671 ();
extern "C" void RankException__ctor_m7672 ();
extern "C" void ResolveEventArgs__ctor_m7673 ();
extern "C" void RuntimeMethodHandle__ctor_m7674 ();
extern "C" void RuntimeMethodHandle__ctor_m7675 ();
extern "C" void RuntimeMethodHandle_get_Value_m7676 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m7677 ();
extern "C" void RuntimeMethodHandle_Equals_m7678 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m7679 ();
extern "C" void StringComparer__ctor_m7680 ();
extern "C" void StringComparer__cctor_m7681 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m1610 ();
extern "C" void StringComparer_Compare_m7682 ();
extern "C" void StringComparer_Equals_m7683 ();
extern "C" void StringComparer_GetHashCode_m7684 ();
extern "C" void CultureAwareComparer__ctor_m7685 ();
extern "C" void CultureAwareComparer_Compare_m7686 ();
extern "C" void CultureAwareComparer_Equals_m7687 ();
extern "C" void CultureAwareComparer_GetHashCode_m7688 ();
extern "C" void OrdinalComparer__ctor_m7689 ();
extern "C" void OrdinalComparer_Compare_m7690 ();
extern "C" void OrdinalComparer_Equals_m7691 ();
extern "C" void OrdinalComparer_GetHashCode_m7692 ();
extern "C" void SystemException__ctor_m7693 ();
extern "C" void SystemException__ctor_m1716 ();
extern "C" void SystemException__ctor_m7694 ();
extern "C" void SystemException__ctor_m7695 ();
extern "C" void ThreadStaticAttribute__ctor_m7696 ();
extern "C" void TimeSpan__ctor_m7697 ();
extern "C" void TimeSpan__ctor_m7698 ();
extern "C" void TimeSpan__ctor_m7699 ();
extern "C" void TimeSpan__cctor_m7700 ();
extern "C" void TimeSpan_CalculateTicks_m7701 ();
extern "C" void TimeSpan_get_Days_m7702 ();
extern "C" void TimeSpan_get_Hours_m7703 ();
extern "C" void TimeSpan_get_Milliseconds_m7704 ();
extern "C" void TimeSpan_get_Minutes_m7705 ();
extern "C" void TimeSpan_get_Seconds_m7706 ();
extern "C" void TimeSpan_get_Ticks_m7707 ();
extern "C" void TimeSpan_get_TotalDays_m7708 ();
extern "C" void TimeSpan_get_TotalHours_m7709 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m7710 ();
extern "C" void TimeSpan_get_TotalMinutes_m7711 ();
extern "C" void TimeSpan_get_TotalSeconds_m7712 ();
extern "C" void TimeSpan_Add_m7713 ();
extern "C" void TimeSpan_Compare_m7714 ();
extern "C" void TimeSpan_CompareTo_m7715 ();
extern "C" void TimeSpan_CompareTo_m7716 ();
extern "C" void TimeSpan_Equals_m7717 ();
extern "C" void TimeSpan_Duration_m7718 ();
extern "C" void TimeSpan_Equals_m7719 ();
extern "C" void TimeSpan_FromDays_m7720 ();
extern "C" void TimeSpan_FromHours_m7721 ();
extern "C" void TimeSpan_FromMinutes_m7722 ();
extern "C" void TimeSpan_FromSeconds_m7723 ();
extern "C" void TimeSpan_FromMilliseconds_m7724 ();
extern "C" void TimeSpan_From_m7725 ();
extern "C" void TimeSpan_GetHashCode_m7726 ();
extern "C" void TimeSpan_Negate_m7727 ();
extern "C" void TimeSpan_Subtract_m7728 ();
extern "C" void TimeSpan_ToString_m7729 ();
extern "C" void TimeSpan_op_Addition_m7730 ();
extern "C" void TimeSpan_op_Equality_m7731 ();
extern "C" void TimeSpan_op_GreaterThan_m7732 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m7733 ();
extern "C" void TimeSpan_op_Inequality_m7734 ();
extern "C" void TimeSpan_op_LessThan_m7735 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m7736 ();
extern "C" void TimeSpan_op_Subtraction_m7737 ();
extern "C" void TimeZone__ctor_m7738 ();
extern "C" void TimeZone__cctor_m7739 ();
extern "C" void TimeZone_get_CurrentTimeZone_m7740 ();
extern "C" void TimeZone_IsDaylightSavingTime_m7741 ();
extern "C" void TimeZone_IsDaylightSavingTime_m7742 ();
extern "C" void TimeZone_ToLocalTime_m7743 ();
extern "C" void TimeZone_ToUniversalTime_m7744 ();
extern "C" void TimeZone_GetLocalTimeDiff_m7745 ();
extern "C" void TimeZone_GetLocalTimeDiff_m7746 ();
extern "C" void CurrentSystemTimeZone__ctor_m7747 ();
extern "C" void CurrentSystemTimeZone__ctor_m7748 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7749 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m7750 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m7751 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m7752 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m7753 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m7754 ();
extern "C" void TypeInitializationException__ctor_m7755 ();
extern "C" void TypeInitializationException_GetObjectData_m7756 ();
extern "C" void TypeLoadException__ctor_m7757 ();
extern "C" void TypeLoadException__ctor_m7758 ();
extern "C" void TypeLoadException__ctor_m7759 ();
extern "C" void TypeLoadException_get_Message_m7760 ();
extern "C" void TypeLoadException_GetObjectData_m7761 ();
extern "C" void UnauthorizedAccessException__ctor_m7762 ();
extern "C" void UnauthorizedAccessException__ctor_m7763 ();
extern "C" void UnauthorizedAccessException__ctor_m7764 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m7765 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m593 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m7766 ();
extern "C" void UnitySerializationHolder__ctor_m7767 ();
extern "C" void UnitySerializationHolder_GetTypeData_m7768 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m7769 ();
extern "C" void UnitySerializationHolder_GetModuleData_m7770 ();
extern "C" void UnitySerializationHolder_GetObjectData_m7771 ();
extern "C" void UnitySerializationHolder_GetRealObject_m7772 ();
extern "C" void Version__ctor_m7773 ();
extern "C" void Version__ctor_m7774 ();
extern "C" void Version__ctor_m1588 ();
extern "C" void Version__ctor_m7775 ();
extern "C" void Version__ctor_m7776 ();
extern "C" void Version_CheckedSet_m7777 ();
extern "C" void Version_get_Build_m7778 ();
extern "C" void Version_get_Major_m7779 ();
extern "C" void Version_get_Minor_m7780 ();
extern "C" void Version_get_Revision_m7781 ();
extern "C" void Version_Clone_m7782 ();
extern "C" void Version_CompareTo_m7783 ();
extern "C" void Version_Equals_m7784 ();
extern "C" void Version_CompareTo_m7785 ();
extern "C" void Version_Equals_m7786 ();
extern "C" void Version_GetHashCode_m7787 ();
extern "C" void Version_ToString_m7788 ();
extern "C" void Version_CreateFromString_m7789 ();
extern "C" void Version_op_Equality_m7790 ();
extern "C" void Version_op_Inequality_m7791 ();
extern "C" void WeakReference__ctor_m7792 ();
extern "C" void WeakReference__ctor_m7793 ();
extern "C" void WeakReference__ctor_m7794 ();
extern "C" void WeakReference__ctor_m7795 ();
extern "C" void WeakReference_AllocateHandle_m7796 ();
extern "C" void WeakReference_get_Target_m7797 ();
extern "C" void WeakReference_get_TrackResurrection_m7798 ();
extern "C" void WeakReference_Finalize_m7799 ();
extern "C" void WeakReference_GetObjectData_m7800 ();
extern "C" void PrimalityTest__ctor_m7801 ();
extern "C" void PrimalityTest_Invoke_m7802 ();
extern "C" void PrimalityTest_BeginInvoke_m7803 ();
extern "C" void PrimalityTest_EndInvoke_m7804 ();
extern "C" void MemberFilter__ctor_m7805 ();
extern "C" void MemberFilter_Invoke_m7806 ();
extern "C" void MemberFilter_BeginInvoke_m7807 ();
extern "C" void MemberFilter_EndInvoke_m7808 ();
extern "C" void TypeFilter__ctor_m7809 ();
extern "C" void TypeFilter_Invoke_m7810 ();
extern "C" void TypeFilter_BeginInvoke_m7811 ();
extern "C" void TypeFilter_EndInvoke_m7812 ();
extern "C" void CrossContextDelegate__ctor_m7813 ();
extern "C" void CrossContextDelegate_Invoke_m7814 ();
extern "C" void CrossContextDelegate_BeginInvoke_m7815 ();
extern "C" void CrossContextDelegate_EndInvoke_m7816 ();
extern "C" void HeaderHandler__ctor_m7817 ();
extern "C" void HeaderHandler_Invoke_m7818 ();
extern "C" void HeaderHandler_BeginInvoke_m7819 ();
extern "C" void HeaderHandler_EndInvoke_m7820 ();
extern "C" void ThreadStart__ctor_m7821 ();
extern "C" void ThreadStart_Invoke_m7822 ();
extern "C" void ThreadStart_BeginInvoke_m7823 ();
extern "C" void ThreadStart_EndInvoke_m7824 ();
extern "C" void TimerCallback__ctor_m7825 ();
extern "C" void TimerCallback_Invoke_m7826 ();
extern "C" void TimerCallback_BeginInvoke_m7827 ();
extern "C" void TimerCallback_EndInvoke_m7828 ();
extern "C" void WaitCallback__ctor_m7829 ();
extern "C" void WaitCallback_Invoke_m7830 ();
extern "C" void WaitCallback_BeginInvoke_m7831 ();
extern "C" void WaitCallback_EndInvoke_m7832 ();
extern "C" void AppDomainInitializer__ctor_m7833 ();
extern "C" void AppDomainInitializer_Invoke_m7834 ();
extern "C" void AppDomainInitializer_BeginInvoke_m7835 ();
extern "C" void AppDomainInitializer_EndInvoke_m7836 ();
extern "C" void AssemblyLoadEventHandler__ctor_m7837 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m7838 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m7839 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m7840 ();
extern "C" void EventHandler__ctor_m7841 ();
extern "C" void EventHandler_Invoke_m7842 ();
extern "C" void EventHandler_BeginInvoke_m7843 ();
extern "C" void EventHandler_EndInvoke_m7844 ();
extern "C" void ResolveEventHandler__ctor_m7845 ();
extern "C" void ResolveEventHandler_Invoke_m7846 ();
extern "C" void ResolveEventHandler_BeginInvoke_m7847 ();
extern "C" void ResolveEventHandler_EndInvoke_m7848 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m591 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m7849 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m7850 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m7851 ();
extern const methodPointerType g_MethodPointers[7829] = 
{
	Hello__ctor_m0,
	Hello_Start_m1,
	Hello_Update_m2,
	AssetBundleCreateRequest__ctor_m4,
	AssetBundleCreateRequest_get_assetBundle_m5,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m6,
	AssetBundleRequest__ctor_m7,
	AssetBundleRequest_get_asset_m8,
	AssetBundleRequest_get_allAssets_m9,
	AssetBundle_LoadAsset_m10,
	AssetBundle_LoadAsset_Internal_m11,
	AssetBundle_LoadAssetWithSubAssets_Internal_m12,
	WaitForSeconds__ctor_m13,
	WaitForFixedUpdate__ctor_m14,
	WaitForEndOfFrame__ctor_m15,
	Coroutine__ctor_m16,
	Coroutine_ReleaseCoroutine_m17,
	Coroutine_Finalize_m18,
	ScriptableObject__ctor_m19,
	ScriptableObject_Internal_CreateScriptableObject_m20,
	ScriptableObject_CreateInstance_m21,
	ScriptableObject_CreateInstance_m22,
	ScriptableObject_CreateInstanceFromType_m23,
	UnhandledExceptionHandler__ctor_m24,
	UnhandledExceptionHandler_RegisterUECatcher_m25,
	UnhandledExceptionHandler_HandleUnhandledException_m26,
	UnhandledExceptionHandler_PrintException_m27,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m28,
	GameCenterPlatform__ctor_m29,
	GameCenterPlatform__cctor_m30,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m31,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m32,
	GameCenterPlatform_Internal_Authenticate_m33,
	GameCenterPlatform_Internal_Authenticated_m34,
	GameCenterPlatform_Internal_UserName_m35,
	GameCenterPlatform_Internal_UserID_m36,
	GameCenterPlatform_Internal_Underage_m37,
	GameCenterPlatform_Internal_UserImage_m38,
	GameCenterPlatform_Internal_LoadFriends_m39,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m40,
	GameCenterPlatform_Internal_LoadAchievements_m41,
	GameCenterPlatform_Internal_ReportProgress_m42,
	GameCenterPlatform_Internal_ReportScore_m43,
	GameCenterPlatform_Internal_LoadScores_m44,
	GameCenterPlatform_Internal_ShowAchievementsUI_m45,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m46,
	GameCenterPlatform_Internal_LoadUsers_m47,
	GameCenterPlatform_Internal_ResetAllAchievements_m48,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m49,
	GameCenterPlatform_ResetAllAchievements_m50,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m51,
	GameCenterPlatform_ShowLeaderboardUI_m52,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m53,
	GameCenterPlatform_ClearAchievementDescriptions_m54,
	GameCenterPlatform_SetAchievementDescription_m55,
	GameCenterPlatform_SetAchievementDescriptionImage_m56,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m57,
	GameCenterPlatform_AuthenticateCallbackWrapper_m58,
	GameCenterPlatform_ClearFriends_m59,
	GameCenterPlatform_SetFriends_m60,
	GameCenterPlatform_SetFriendImage_m61,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m62,
	GameCenterPlatform_AchievementCallbackWrapper_m63,
	GameCenterPlatform_ProgressCallbackWrapper_m64,
	GameCenterPlatform_ScoreCallbackWrapper_m65,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m66,
	GameCenterPlatform_get_localUser_m67,
	GameCenterPlatform_PopulateLocalUser_m68,
	GameCenterPlatform_LoadAchievementDescriptions_m69,
	GameCenterPlatform_ReportProgress_m70,
	GameCenterPlatform_LoadAchievements_m71,
	GameCenterPlatform_ReportScore_m72,
	GameCenterPlatform_LoadScores_m73,
	GameCenterPlatform_LoadScores_m74,
	GameCenterPlatform_LeaderboardCallbackWrapper_m75,
	GameCenterPlatform_GetLoading_m76,
	GameCenterPlatform_VerifyAuthentication_m77,
	GameCenterPlatform_ShowAchievementsUI_m78,
	GameCenterPlatform_ShowLeaderboardUI_m79,
	GameCenterPlatform_ClearUsers_m80,
	GameCenterPlatform_SetUser_m81,
	GameCenterPlatform_SetUserImage_m82,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m83,
	GameCenterPlatform_LoadUsers_m84,
	GameCenterPlatform_SafeSetUserImage_m85,
	GameCenterPlatform_SafeClearArray_m86,
	GameCenterPlatform_CreateLeaderboard_m87,
	GameCenterPlatform_CreateAchievement_m88,
	GameCenterPlatform_TriggerResetAchievementCallback_m89,
	GcLeaderboard__ctor_m90,
	GcLeaderboard_Finalize_m91,
	GcLeaderboard_Contains_m92,
	GcLeaderboard_SetScores_m93,
	GcLeaderboard_SetLocalScore_m94,
	GcLeaderboard_SetMaxRange_m95,
	GcLeaderboard_SetTitle_m96,
	GcLeaderboard_Internal_LoadScores_m97,
	GcLeaderboard_Internal_LoadScoresWithUsers_m98,
	GcLeaderboard_Loading_m99,
	GcLeaderboard_Dispose_m100,
	BoneWeight_get_weight0_m101,
	BoneWeight_set_weight0_m102,
	BoneWeight_get_weight1_m103,
	BoneWeight_set_weight1_m104,
	BoneWeight_get_weight2_m105,
	BoneWeight_set_weight2_m106,
	BoneWeight_get_weight3_m107,
	BoneWeight_set_weight3_m108,
	BoneWeight_get_boneIndex0_m109,
	BoneWeight_set_boneIndex0_m110,
	BoneWeight_get_boneIndex1_m111,
	BoneWeight_set_boneIndex1_m112,
	BoneWeight_get_boneIndex2_m113,
	BoneWeight_set_boneIndex2_m114,
	BoneWeight_get_boneIndex3_m115,
	BoneWeight_set_boneIndex3_m116,
	BoneWeight_GetHashCode_m117,
	BoneWeight_Equals_m118,
	BoneWeight_op_Equality_m119,
	BoneWeight_op_Inequality_m120,
	GUILayer_HitTest_m121,
	GUILayer_INTERNAL_CALL_HitTest_m122,
	Texture__ctor_m123,
	Texture2D__ctor_m124,
	Texture2D_Internal_Create_m125,
	StateChanged__ctor_m126,
	StateChanged_Invoke_m127,
	StateChanged_BeginInvoke_m128,
	StateChanged_EndInvoke_m129,
	CullingGroup_Finalize_m130,
	CullingGroup_Dispose_m131,
	CullingGroup_SendEvents_m132,
	CullingGroup_FinalizerFailure_m133,
	GradientColorKey__ctor_m134,
	GradientAlphaKey__ctor_m135,
	Gradient__ctor_m136,
	Gradient_Init_m137,
	Gradient_Cleanup_m138,
	Gradient_Finalize_m139,
	LayerMask_get_value_m140,
	LayerMask_set_value_m141,
	LayerMask_LayerToName_m142,
	LayerMask_NameToLayer_m143,
	LayerMask_GetMask_m144,
	LayerMask_op_Implicit_m145,
	LayerMask_op_Implicit_m146,
	Vector3__ctor_m147,
	Vector3_GetHashCode_m148,
	Vector3_Equals_m149,
	Vector3_ToString_m150,
	Vector3_ToString_m151,
	Vector3_SqrMagnitude_m152,
	Vector3_Min_m153,
	Vector3_Max_m154,
	Vector3_op_Addition_m155,
	Vector3_op_Subtraction_m156,
	Vector3_op_Multiply_m157,
	Vector3_op_Equality_m158,
	Color__ctor_m159,
	Color_ToString_m160,
	Color_GetHashCode_m161,
	Color_Equals_m162,
	Color_op_Multiply_m163,
	Color_op_Implicit_m164,
	Quaternion_ToString_m165,
	Quaternion_GetHashCode_m166,
	Quaternion_Equals_m167,
	Rect_get_x_m168,
	Rect_get_y_m169,
	Rect_get_width_m170,
	Rect_get_height_m171,
	Rect_get_xMin_m172,
	Rect_get_yMin_m173,
	Rect_get_xMax_m174,
	Rect_get_yMax_m175,
	Rect_ToString_m176,
	Rect_Contains_m177,
	Rect_GetHashCode_m178,
	Rect_Equals_m179,
	Matrix4x4_get_Item_m180,
	Matrix4x4_set_Item_m181,
	Matrix4x4_get_Item_m182,
	Matrix4x4_set_Item_m183,
	Matrix4x4_GetHashCode_m184,
	Matrix4x4_Equals_m185,
	Matrix4x4_Inverse_m186,
	Matrix4x4_INTERNAL_CALL_Inverse_m187,
	Matrix4x4_Transpose_m188,
	Matrix4x4_INTERNAL_CALL_Transpose_m189,
	Matrix4x4_Invert_m190,
	Matrix4x4_INTERNAL_CALL_Invert_m191,
	Matrix4x4_get_inverse_m192,
	Matrix4x4_get_transpose_m193,
	Matrix4x4_get_isIdentity_m194,
	Matrix4x4_GetColumn_m195,
	Matrix4x4_GetRow_m196,
	Matrix4x4_SetColumn_m197,
	Matrix4x4_SetRow_m198,
	Matrix4x4_MultiplyPoint_m199,
	Matrix4x4_MultiplyPoint3x4_m200,
	Matrix4x4_MultiplyVector_m201,
	Matrix4x4_Scale_m202,
	Matrix4x4_get_zero_m203,
	Matrix4x4_get_identity_m204,
	Matrix4x4_SetTRS_m205,
	Matrix4x4_TRS_m206,
	Matrix4x4_INTERNAL_CALL_TRS_m207,
	Matrix4x4_ToString_m208,
	Matrix4x4_ToString_m209,
	Matrix4x4_Ortho_m210,
	Matrix4x4_Perspective_m211,
	Matrix4x4_op_Multiply_m212,
	Matrix4x4_op_Multiply_m213,
	Matrix4x4_op_Equality_m214,
	Matrix4x4_op_Inequality_m215,
	Bounds__ctor_m216,
	Bounds_GetHashCode_m217,
	Bounds_Equals_m218,
	Bounds_get_center_m219,
	Bounds_set_center_m220,
	Bounds_get_size_m221,
	Bounds_set_size_m222,
	Bounds_get_extents_m223,
	Bounds_set_extents_m224,
	Bounds_get_min_m225,
	Bounds_set_min_m226,
	Bounds_get_max_m227,
	Bounds_set_max_m228,
	Bounds_SetMinMax_m229,
	Bounds_Encapsulate_m230,
	Bounds_Encapsulate_m231,
	Bounds_Expand_m232,
	Bounds_Expand_m233,
	Bounds_Intersects_m234,
	Bounds_Internal_Contains_m235,
	Bounds_INTERNAL_CALL_Internal_Contains_m236,
	Bounds_Contains_m237,
	Bounds_Internal_SqrDistance_m238,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m239,
	Bounds_SqrDistance_m240,
	Bounds_Internal_IntersectRay_m241,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m242,
	Bounds_IntersectRay_m243,
	Bounds_IntersectRay_m244,
	Bounds_Internal_GetClosestPoint_m245,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m246,
	Bounds_ClosestPoint_m247,
	Bounds_ToString_m248,
	Bounds_ToString_m249,
	Bounds_op_Equality_m250,
	Bounds_op_Inequality_m251,
	Vector4__ctor_m252,
	Vector4_GetHashCode_m253,
	Vector4_Equals_m254,
	Vector4_ToString_m255,
	Vector4_Dot_m256,
	Vector4_SqrMagnitude_m257,
	Vector4_op_Subtraction_m258,
	Vector4_op_Equality_m259,
	Ray_get_direction_m260,
	Ray_ToString_m261,
	MathfInternal__cctor_m262,
	Mathf__cctor_m263,
	Mathf_Abs_m264,
	Mathf_Min_m265,
	Mathf_Max_m266,
	Mathf_Approximately_m267,
	ReapplyDrivenProperties__ctor_m268,
	ReapplyDrivenProperties_Invoke_m269,
	ReapplyDrivenProperties_BeginInvoke_m270,
	ReapplyDrivenProperties_EndInvoke_m271,
	RectTransform_SendReapplyDrivenProperties_m272,
	ResourceRequest__ctor_m273,
	ResourceRequest_get_asset_m274,
	Resources_Load_m275,
	SerializePrivateVariables__ctor_m276,
	SerializeField__ctor_m277,
	SphericalHarmonicsL2_Clear_m278,
	SphericalHarmonicsL2_ClearInternal_m279,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m280,
	SphericalHarmonicsL2_AddAmbientLight_m281,
	SphericalHarmonicsL2_AddAmbientLightInternal_m282,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m283,
	SphericalHarmonicsL2_AddDirectionalLight_m284,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m285,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m286,
	SphericalHarmonicsL2_get_Item_m287,
	SphericalHarmonicsL2_set_Item_m288,
	SphericalHarmonicsL2_GetHashCode_m289,
	SphericalHarmonicsL2_Equals_m290,
	SphericalHarmonicsL2_op_Multiply_m291,
	SphericalHarmonicsL2_op_Multiply_m292,
	SphericalHarmonicsL2_op_Addition_m293,
	SphericalHarmonicsL2_op_Equality_m294,
	SphericalHarmonicsL2_op_Inequality_m295,
	UnityString_Format_m296,
	AsyncOperation__ctor_m297,
	AsyncOperation_InternalDestroy_m298,
	AsyncOperation_Finalize_m299,
	LogCallback__ctor_m300,
	LogCallback_Invoke_m301,
	LogCallback_BeginInvoke_m302,
	LogCallback_EndInvoke_m303,
	Application_CallLogCallback_m304,
	Behaviour__ctor_m305,
	CameraCallback__ctor_m306,
	CameraCallback_Invoke_m307,
	CameraCallback_BeginInvoke_m308,
	CameraCallback_EndInvoke_m309,
	Camera_get_nearClipPlane_m310,
	Camera_get_farClipPlane_m311,
	Camera_get_cullingMask_m312,
	Camera_get_eventMask_m313,
	Camera_get_pixelRect_m314,
	Camera_INTERNAL_get_pixelRect_m315,
	Camera_get_targetTexture_m316,
	Camera_get_clearFlags_m317,
	Camera_ScreenPointToRay_m318,
	Camera_INTERNAL_CALL_ScreenPointToRay_m319,
	Camera_get_allCamerasCount_m320,
	Camera_GetAllCameras_m321,
	Camera_FireOnPreCull_m322,
	Camera_FireOnPreRender_m323,
	Camera_FireOnPostRender_m324,
	Camera_RaycastTry_m325,
	Camera_INTERNAL_CALL_RaycastTry_m326,
	Camera_RaycastTry2D_m327,
	Camera_INTERNAL_CALL_RaycastTry2D_m328,
	Debug_Internal_Log_m329,
	Debug_Log_m330,
	Debug_LogError_m331,
	DisplaysUpdatedDelegate__ctor_m332,
	DisplaysUpdatedDelegate_Invoke_m333,
	DisplaysUpdatedDelegate_BeginInvoke_m334,
	DisplaysUpdatedDelegate_EndInvoke_m335,
	Display__ctor_m336,
	Display__ctor_m337,
	Display__cctor_m338,
	Display_add_onDisplaysUpdated_m339,
	Display_remove_onDisplaysUpdated_m340,
	Display_get_renderingWidth_m341,
	Display_get_renderingHeight_m342,
	Display_get_systemWidth_m343,
	Display_get_systemHeight_m344,
	Display_get_colorBuffer_m345,
	Display_get_depthBuffer_m346,
	Display_Activate_m347,
	Display_Activate_m348,
	Display_SetParams_m349,
	Display_SetRenderingResolution_m350,
	Display_MultiDisplayLicense_m351,
	Display_RelativeMouseAt_m352,
	Display_get_main_m353,
	Display_RecreateDisplayList_m354,
	Display_FireDisplaysUpdated_m355,
	Display_GetSystemExtImpl_m356,
	Display_GetRenderingExtImpl_m357,
	Display_GetRenderingBuffersImpl_m358,
	Display_SetRenderingResolutionImpl_m359,
	Display_ActivateDisplayImpl_m360,
	Display_SetParamsImpl_m361,
	Display_MultiDisplayLicenseImpl_m362,
	Display_RelativeMouseAtImpl_m363,
	MonoBehaviour__ctor_m3,
	Input__cctor_m364,
	Input_GetMouseButton_m365,
	Input_GetMouseButtonDown_m366,
	Input_get_mousePosition_m367,
	Input_INTERNAL_get_mousePosition_m368,
	Object__ctor_m369,
	Object_ToString_m370,
	Object_Equals_m371,
	Object_GetHashCode_m372,
	Object_CompareBaseObjects_m373,
	Object_IsNativeObjectAlive_m374,
	Object_GetInstanceID_m375,
	Object_GetCachedPtr_m376,
	Object_op_Implicit_m377,
	Object_op_Equality_m378,
	Object_op_Inequality_m379,
	Component__ctor_m380,
	Component_get_gameObject_m381,
	Component_GetComponentFastPath_m382,
	GameObject_SendMessage_m383,
	Enumerator__ctor_m384,
	Enumerator_get_Current_m385,
	Enumerator_MoveNext_m386,
	Enumerator_Reset_m387,
	Transform_get_childCount_m388,
	Transform_GetEnumerator_m389,
	Transform_GetChild_m390,
	YieldInstruction__ctor_m391,
	UnityAdsInternal__ctor_m392,
	UnityAdsInternal_add_onCampaignsAvailable_m393,
	UnityAdsInternal_remove_onCampaignsAvailable_m394,
	UnityAdsInternal_add_onCampaignsFetchFailed_m395,
	UnityAdsInternal_remove_onCampaignsFetchFailed_m396,
	UnityAdsInternal_add_onShow_m397,
	UnityAdsInternal_remove_onShow_m398,
	UnityAdsInternal_add_onHide_m399,
	UnityAdsInternal_remove_onHide_m400,
	UnityAdsInternal_add_onVideoCompleted_m401,
	UnityAdsInternal_remove_onVideoCompleted_m402,
	UnityAdsInternal_add_onVideoStarted_m403,
	UnityAdsInternal_remove_onVideoStarted_m404,
	UnityAdsInternal_RegisterNative_m405,
	UnityAdsInternal_Init_m406,
	UnityAdsInternal_Show_m407,
	UnityAdsInternal_CanShowAds_m408,
	UnityAdsInternal_SetLogLevel_m409,
	UnityAdsInternal_SetCampaignDataURL_m410,
	UnityAdsInternal_RemoveAllEventHandlers_m411,
	UnityAdsInternal_CallUnityAdsCampaignsAvailable_m412,
	UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m413,
	UnityAdsInternal_CallUnityAdsShow_m414,
	UnityAdsInternal_CallUnityAdsHide_m415,
	UnityAdsInternal_CallUnityAdsVideoCompleted_m416,
	UnityAdsInternal_CallUnityAdsVideoStarted_m417,
	Particle_get_position_m418,
	Particle_set_position_m419,
	Particle_get_velocity_m420,
	Particle_set_velocity_m421,
	Particle_get_energy_m422,
	Particle_set_energy_m423,
	Particle_get_startEnergy_m424,
	Particle_set_startEnergy_m425,
	Particle_get_size_m426,
	Particle_set_size_m427,
	Particle_get_rotation_m428,
	Particle_set_rotation_m429,
	Particle_get_angularVelocity_m430,
	Particle_set_angularVelocity_m431,
	Particle_get_color_m432,
	Particle_set_color_m433,
	AudioConfigurationChangeHandler__ctor_m434,
	AudioConfigurationChangeHandler_Invoke_m435,
	AudioConfigurationChangeHandler_BeginInvoke_m436,
	AudioConfigurationChangeHandler_EndInvoke_m437,
	AudioSettings_InvokeOnAudioConfigurationChanged_m438,
	PCMReaderCallback__ctor_m439,
	PCMReaderCallback_Invoke_m440,
	PCMReaderCallback_BeginInvoke_m441,
	PCMReaderCallback_EndInvoke_m442,
	PCMSetPositionCallback__ctor_m443,
	PCMSetPositionCallback_Invoke_m444,
	PCMSetPositionCallback_BeginInvoke_m445,
	PCMSetPositionCallback_EndInvoke_m446,
	AudioClip_InvokePCMReaderCallback_Internal_m447,
	AudioClip_InvokePCMSetPositionCallback_Internal_m448,
	WebCamDevice_get_name_m449,
	WebCamDevice_get_isFrontFacing_m450,
	AnimationCurve__ctor_m451,
	AnimationCurve__ctor_m452,
	AnimationCurve_Cleanup_m453,
	AnimationCurve_Finalize_m454,
	AnimationCurve_Init_m455,
	WrapperlessIcall__ctor_m456,
	AttributeHelperEngine__cctor_m457,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m458,
	AttributeHelperEngine_GetRequiredComponents_m459,
	AttributeHelperEngine_CheckIsEditorScript_m460,
	SetupCoroutine__ctor_m461,
	SetupCoroutine_InvokeMember_m462,
	SetupCoroutine_InvokeStatic_m463,
	WritableAttribute__ctor_m464,
	AssemblyIsEditorAssembly__ctor_m465,
	GcUserProfileData_ToUserProfile_m466,
	GcUserProfileData_AddToArray_m467,
	GcAchievementDescriptionData_ToAchievementDescription_m468,
	GcAchievementData_ToAchievement_m469,
	GcScoreData_ToScore_m470,
	Resolution_get_width_m471,
	Resolution_set_width_m472,
	Resolution_get_height_m473,
	Resolution_set_height_m474,
	Resolution_get_refreshRate_m475,
	Resolution_set_refreshRate_m476,
	Resolution_ToString_m477,
	LocalUser__ctor_m478,
	LocalUser_SetFriends_m479,
	LocalUser_SetAuthenticated_m480,
	LocalUser_SetUnderage_m481,
	LocalUser_get_authenticated_m482,
	UserProfile__ctor_m483,
	UserProfile__ctor_m484,
	UserProfile_ToString_m485,
	UserProfile_SetUserName_m486,
	UserProfile_SetUserID_m487,
	UserProfile_SetImage_m488,
	UserProfile_get_userName_m489,
	UserProfile_get_id_m490,
	UserProfile_get_isFriend_m491,
	UserProfile_get_state_m492,
	Achievement__ctor_m493,
	Achievement__ctor_m494,
	Achievement__ctor_m495,
	Achievement_ToString_m496,
	Achievement_get_id_m497,
	Achievement_set_id_m498,
	Achievement_get_percentCompleted_m499,
	Achievement_set_percentCompleted_m500,
	Achievement_get_completed_m501,
	Achievement_get_hidden_m502,
	Achievement_get_lastReportedDate_m503,
	AchievementDescription__ctor_m504,
	AchievementDescription_ToString_m505,
	AchievementDescription_SetImage_m506,
	AchievementDescription_get_id_m507,
	AchievementDescription_set_id_m508,
	AchievementDescription_get_title_m509,
	AchievementDescription_get_achievedDescription_m510,
	AchievementDescription_get_unachievedDescription_m511,
	AchievementDescription_get_hidden_m512,
	AchievementDescription_get_points_m513,
	Score__ctor_m514,
	Score__ctor_m515,
	Score_ToString_m516,
	Score_get_leaderboardID_m517,
	Score_set_leaderboardID_m518,
	Score_get_value_m519,
	Score_set_value_m520,
	Leaderboard__ctor_m521,
	Leaderboard_ToString_m522,
	Leaderboard_SetLocalUserScore_m523,
	Leaderboard_SetMaxRange_m524,
	Leaderboard_SetScores_m525,
	Leaderboard_SetTitle_m526,
	Leaderboard_GetUserFilter_m527,
	Leaderboard_get_id_m528,
	Leaderboard_set_id_m529,
	Leaderboard_get_userScope_m530,
	Leaderboard_set_userScope_m531,
	Leaderboard_get_range_m532,
	Leaderboard_set_range_m533,
	Leaderboard_get_timeScope_m534,
	Leaderboard_set_timeScope_m535,
	HitInfo_SendMessage_m536,
	HitInfo_Compare_m537,
	HitInfo_op_Implicit_m538,
	SendMouseEvents__cctor_m539,
	SendMouseEvents_DoSendMouseEvents_m540,
	SendMouseEvents_SendEvents_m541,
	Range__ctor_m542,
	StackTraceUtility__ctor_m543,
	StackTraceUtility__cctor_m544,
	StackTraceUtility_SetProjectFolder_m545,
	StackTraceUtility_ExtractStackTrace_m546,
	StackTraceUtility_IsSystemStacktraceType_m547,
	StackTraceUtility_ExtractStringFromException_m548,
	StackTraceUtility_ExtractStringFromExceptionInternal_m549,
	StackTraceUtility_PostprocessStacktrace_m550,
	StackTraceUtility_ExtractFormattedStackTrace_m551,
	UnityException__ctor_m552,
	UnityException__ctor_m553,
	UnityException__ctor_m554,
	UnityException__ctor_m555,
	SharedBetweenAnimatorsAttribute__ctor_m556,
	StateMachineBehaviour__ctor_m557,
	ArgumentCache__ctor_m558,
	ArgumentCache_TidyAssemblyTypeName_m559,
	ArgumentCache_OnBeforeSerialize_m560,
	ArgumentCache_OnAfterDeserialize_m561,
	PersistentCall__ctor_m562,
	PersistentCallGroup__ctor_m563,
	InvokableCallList__ctor_m564,
	InvokableCallList_ClearPersistent_m565,
	UnityEventBase__ctor_m566,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m567,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m568,
	UnityEventBase_DirtyPersistentCalls_m569,
	UnityEventBase_ToString_m570,
	UnityEvent__ctor_m571,
	DefaultValueAttribute__ctor_m572,
	DefaultValueAttribute_get_Value_m573,
	DefaultValueAttribute_Equals_m574,
	DefaultValueAttribute_GetHashCode_m575,
	ExcludeFromDocsAttribute__ctor_m576,
	FormerlySerializedAsAttribute__ctor_m577,
	TypeInferenceRuleAttribute__ctor_m578,
	TypeInferenceRuleAttribute__ctor_m579,
	TypeInferenceRuleAttribute_ToString_m580,
	UnityAdsDelegate__ctor_m581,
	UnityAdsDelegate_Invoke_m582,
	UnityAdsDelegate_BeginInvoke_m583,
	UnityAdsDelegate_EndInvoke_m584,
	Locale_GetText_m663,
	Locale_GetText_m664,
	MonoTODOAttribute__ctor_m665,
	MonoTODOAttribute__ctor_m666,
	HybridDictionary__ctor_m667,
	HybridDictionary__ctor_m668,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m669,
	HybridDictionary_get_inner_m670,
	HybridDictionary_get_Count_m671,
	HybridDictionary_get_Item_m672,
	HybridDictionary_set_Item_m673,
	HybridDictionary_get_SyncRoot_m674,
	HybridDictionary_Add_m675,
	HybridDictionary_Contains_m676,
	HybridDictionary_CopyTo_m677,
	HybridDictionary_GetEnumerator_m678,
	HybridDictionary_Remove_m679,
	HybridDictionary_Switch_m680,
	DictionaryNode__ctor_m681,
	DictionaryNodeEnumerator__ctor_m682,
	DictionaryNodeEnumerator_FailFast_m683,
	DictionaryNodeEnumerator_MoveNext_m684,
	DictionaryNodeEnumerator_Reset_m685,
	DictionaryNodeEnumerator_get_Current_m686,
	DictionaryNodeEnumerator_get_DictionaryNode_m687,
	DictionaryNodeEnumerator_get_Entry_m688,
	DictionaryNodeEnumerator_get_Key_m689,
	DictionaryNodeEnumerator_get_Value_m690,
	ListDictionary__ctor_m691,
	ListDictionary__ctor_m692,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m693,
	ListDictionary_FindEntry_m694,
	ListDictionary_FindEntry_m695,
	ListDictionary_AddImpl_m696,
	ListDictionary_get_Count_m697,
	ListDictionary_get_SyncRoot_m698,
	ListDictionary_CopyTo_m699,
	ListDictionary_get_Item_m700,
	ListDictionary_set_Item_m701,
	ListDictionary_Add_m702,
	ListDictionary_Clear_m703,
	ListDictionary_Contains_m704,
	ListDictionary_GetEnumerator_m705,
	ListDictionary_Remove_m706,
	_Item__ctor_m707,
	_KeysEnumerator__ctor_m708,
	_KeysEnumerator_get_Current_m709,
	_KeysEnumerator_MoveNext_m710,
	_KeysEnumerator_Reset_m711,
	KeysCollection__ctor_m712,
	KeysCollection_System_Collections_ICollection_CopyTo_m713,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m714,
	KeysCollection_get_Count_m715,
	KeysCollection_GetEnumerator_m716,
	NameObjectCollectionBase__ctor_m717,
	NameObjectCollectionBase__ctor_m718,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m719,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m720,
	NameObjectCollectionBase_Init_m721,
	NameObjectCollectionBase_get_Keys_m722,
	NameObjectCollectionBase_GetEnumerator_m723,
	NameObjectCollectionBase_GetObjectData_m724,
	NameObjectCollectionBase_get_Count_m725,
	NameObjectCollectionBase_OnDeserialization_m726,
	NameObjectCollectionBase_get_IsReadOnly_m727,
	NameObjectCollectionBase_BaseAdd_m728,
	NameObjectCollectionBase_BaseGet_m729,
	NameObjectCollectionBase_BaseGet_m730,
	NameObjectCollectionBase_BaseGetKey_m731,
	NameObjectCollectionBase_FindFirstMatchedItem_m732,
	NameValueCollection__ctor_m733,
	NameValueCollection__ctor_m734,
	NameValueCollection_Add_m735,
	NameValueCollection_Get_m736,
	NameValueCollection_AsSingleString_m737,
	NameValueCollection_GetKey_m738,
	NameValueCollection_InvalidateCachedArrays_m739,
	TypeConverterAttribute__ctor_m740,
	TypeConverterAttribute__ctor_m741,
	TypeConverterAttribute__cctor_m742,
	TypeConverterAttribute_Equals_m743,
	TypeConverterAttribute_GetHashCode_m744,
	TypeConverterAttribute_get_ConverterTypeName_m745,
	DefaultCertificatePolicy__ctor_m746,
	DefaultCertificatePolicy_CheckValidationResult_m747,
	FileWebRequest__ctor_m748,
	FileWebRequest__ctor_m749,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m750,
	FileWebRequest_GetObjectData_m751,
	FileWebRequestCreator__ctor_m752,
	FileWebRequestCreator_Create_m753,
	FtpRequestCreator__ctor_m754,
	FtpRequestCreator_Create_m755,
	FtpWebRequest__ctor_m756,
	FtpWebRequest__cctor_m757,
	FtpWebRequest_U3CcallbackU3Em__B_m758,
	GlobalProxySelection_get_Select_m759,
	HttpRequestCreator__ctor_m760,
	HttpRequestCreator_Create_m761,
	HttpVersion__cctor_m762,
	HttpWebRequest__ctor_m763,
	HttpWebRequest__ctor_m764,
	HttpWebRequest__cctor_m765,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m766,
	HttpWebRequest_get_Address_m767,
	HttpWebRequest_get_ServicePoint_m768,
	HttpWebRequest_GetServicePoint_m769,
	HttpWebRequest_GetObjectData_m770,
	IPAddress__ctor_m771,
	IPAddress__ctor_m772,
	IPAddress__cctor_m773,
	IPAddress_SwapShort_m774,
	IPAddress_HostToNetworkOrder_m775,
	IPAddress_NetworkToHostOrder_m776,
	IPAddress_Parse_m777,
	IPAddress_TryParse_m778,
	IPAddress_ParseIPV4_m779,
	IPAddress_ParseIPV6_m780,
	IPAddress_get_InternalIPv4Address_m781,
	IPAddress_get_ScopeId_m782,
	IPAddress_get_AddressFamily_m783,
	IPAddress_IsLoopback_m784,
	IPAddress_ToString_m785,
	IPAddress_ToString_m786,
	IPAddress_Equals_m787,
	IPAddress_GetHashCode_m788,
	IPAddress_Hash_m789,
	IPv6Address__ctor_m790,
	IPv6Address__ctor_m791,
	IPv6Address__ctor_m792,
	IPv6Address__cctor_m793,
	IPv6Address_Parse_m794,
	IPv6Address_Fill_m795,
	IPv6Address_TryParse_m796,
	IPv6Address_TryParse_m797,
	IPv6Address_get_Address_m798,
	IPv6Address_get_ScopeId_m799,
	IPv6Address_set_ScopeId_m800,
	IPv6Address_IsLoopback_m801,
	IPv6Address_SwapUShort_m802,
	IPv6Address_AsIPv4Int_m803,
	IPv6Address_IsIPv4Compatible_m804,
	IPv6Address_IsIPv4Mapped_m805,
	IPv6Address_ToString_m806,
	IPv6Address_ToString_m807,
	IPv6Address_Equals_m808,
	IPv6Address_GetHashCode_m809,
	IPv6Address_Hash_m810,
	ServicePoint__ctor_m811,
	ServicePoint_get_Address_m812,
	ServicePoint_get_CurrentConnections_m813,
	ServicePoint_get_IdleSince_m814,
	ServicePoint_set_IdleSince_m815,
	ServicePoint_set_Expect100Continue_m816,
	ServicePoint_set_UseNagleAlgorithm_m817,
	ServicePoint_set_SendContinue_m818,
	ServicePoint_set_UsesProxy_m819,
	ServicePoint_set_UseConnect_m820,
	ServicePoint_get_AvailableForRecycling_m821,
	SPKey__ctor_m822,
	SPKey_GetHashCode_m823,
	SPKey_Equals_m824,
	ServicePointManager__cctor_m825,
	ServicePointManager_get_CertificatePolicy_m826,
	ServicePointManager_get_CheckCertificateRevocationList_m827,
	ServicePointManager_get_SecurityProtocol_m828,
	ServicePointManager_get_ServerCertificateValidationCallback_m829,
	ServicePointManager_FindServicePoint_m830,
	ServicePointManager_RecycleServicePoints_m831,
	WebHeaderCollection__ctor_m832,
	WebHeaderCollection__ctor_m833,
	WebHeaderCollection__ctor_m834,
	WebHeaderCollection__cctor_m835,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m836,
	WebHeaderCollection_Add_m837,
	WebHeaderCollection_AddWithoutValidate_m838,
	WebHeaderCollection_IsRestricted_m839,
	WebHeaderCollection_OnDeserialization_m840,
	WebHeaderCollection_ToString_m841,
	WebHeaderCollection_GetObjectData_m842,
	WebHeaderCollection_get_Count_m843,
	WebHeaderCollection_get_Keys_m844,
	WebHeaderCollection_Get_m845,
	WebHeaderCollection_GetKey_m846,
	WebHeaderCollection_GetEnumerator_m847,
	WebHeaderCollection_IsHeaderValue_m848,
	WebHeaderCollection_IsHeaderName_m849,
	WebProxy__ctor_m850,
	WebProxy__ctor_m851,
	WebProxy__ctor_m852,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m853,
	WebProxy_get_UseDefaultCredentials_m854,
	WebProxy_GetProxy_m855,
	WebProxy_IsBypassed_m856,
	WebProxy_GetObjectData_m857,
	WebProxy_CheckBypassList_m858,
	WebRequest__ctor_m859,
	WebRequest__ctor_m860,
	WebRequest__cctor_m861,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m862,
	WebRequest_AddDynamicPrefix_m863,
	WebRequest_GetMustImplement_m864,
	WebRequest_get_DefaultWebProxy_m865,
	WebRequest_GetDefaultWebProxy_m866,
	WebRequest_GetObjectData_m867,
	WebRequest_AddPrefix_m868,
	PublicKey__ctor_m869,
	PublicKey_get_EncodedKeyValue_m870,
	PublicKey_get_EncodedParameters_m871,
	PublicKey_get_Key_m872,
	PublicKey_get_Oid_m873,
	PublicKey_GetUnsignedBigInteger_m874,
	PublicKey_DecodeDSA_m875,
	PublicKey_DecodeRSA_m876,
	X500DistinguishedName__ctor_m877,
	X500DistinguishedName_Decode_m878,
	X500DistinguishedName_GetSeparator_m879,
	X500DistinguishedName_DecodeRawData_m880,
	X500DistinguishedName_Canonize_m881,
	X500DistinguishedName_AreEqual_m882,
	X509BasicConstraintsExtension__ctor_m883,
	X509BasicConstraintsExtension__ctor_m884,
	X509BasicConstraintsExtension__ctor_m885,
	X509BasicConstraintsExtension_get_CertificateAuthority_m886,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m887,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m888,
	X509BasicConstraintsExtension_CopyFrom_m889,
	X509BasicConstraintsExtension_Decode_m890,
	X509BasicConstraintsExtension_Encode_m891,
	X509BasicConstraintsExtension_ToString_m892,
	X509Certificate2__ctor_m893,
	X509Certificate2__cctor_m894,
	X509Certificate2_get_Extensions_m895,
	X509Certificate2_get_IssuerName_m896,
	X509Certificate2_get_NotAfter_m897,
	X509Certificate2_get_NotBefore_m898,
	X509Certificate2_get_PrivateKey_m899,
	X509Certificate2_get_PublicKey_m900,
	X509Certificate2_get_SerialNumber_m901,
	X509Certificate2_get_SignatureAlgorithm_m902,
	X509Certificate2_get_SubjectName_m903,
	X509Certificate2_get_Thumbprint_m904,
	X509Certificate2_get_Version_m905,
	X509Certificate2_GetNameInfo_m906,
	X509Certificate2_Find_m907,
	X509Certificate2_GetValueAsString_m908,
	X509Certificate2_ImportPkcs12_m909,
	X509Certificate2_Import_m910,
	X509Certificate2_Reset_m911,
	X509Certificate2_ToString_m912,
	X509Certificate2_ToString_m913,
	X509Certificate2_AppendBuffer_m914,
	X509Certificate2_Verify_m915,
	X509Certificate2_get_MonoCertificate_m916,
	X509Certificate2Collection__ctor_m917,
	X509Certificate2Collection__ctor_m918,
	X509Certificate2Collection_get_Item_m919,
	X509Certificate2Collection_Add_m920,
	X509Certificate2Collection_AddRange_m921,
	X509Certificate2Collection_Contains_m922,
	X509Certificate2Collection_Find_m923,
	X509Certificate2Collection_GetEnumerator_m924,
	X509Certificate2Enumerator__ctor_m925,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m926,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m927,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m928,
	X509Certificate2Enumerator_get_Current_m929,
	X509Certificate2Enumerator_MoveNext_m930,
	X509Certificate2Enumerator_Reset_m931,
	X509CertificateEnumerator__ctor_m932,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m933,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m934,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m935,
	X509CertificateEnumerator_get_Current_m936,
	X509CertificateEnumerator_MoveNext_m937,
	X509CertificateEnumerator_Reset_m938,
	X509CertificateCollection__ctor_m939,
	X509CertificateCollection__ctor_m940,
	X509CertificateCollection_get_Item_m941,
	X509CertificateCollection_AddRange_m942,
	X509CertificateCollection_GetEnumerator_m943,
	X509CertificateCollection_GetHashCode_m944,
	X509Chain__ctor_m945,
	X509Chain__ctor_m946,
	X509Chain__cctor_m947,
	X509Chain_get_ChainPolicy_m948,
	X509Chain_Build_m949,
	X509Chain_Reset_m950,
	X509Chain_get_Roots_m951,
	X509Chain_get_CertificateAuthorities_m952,
	X509Chain_get_CertificateCollection_m953,
	X509Chain_BuildChainFrom_m954,
	X509Chain_SelectBestFromCollection_m955,
	X509Chain_FindParent_m956,
	X509Chain_IsChainComplete_m957,
	X509Chain_IsSelfIssued_m958,
	X509Chain_ValidateChain_m959,
	X509Chain_Process_m960,
	X509Chain_PrepareForNextCertificate_m961,
	X509Chain_WrapUp_m962,
	X509Chain_ProcessCertificateExtensions_m963,
	X509Chain_IsSignedWith_m964,
	X509Chain_GetSubjectKeyIdentifier_m965,
	X509Chain_GetAuthorityKeyIdentifier_m966,
	X509Chain_GetAuthorityKeyIdentifier_m967,
	X509Chain_GetAuthorityKeyIdentifier_m968,
	X509Chain_CheckRevocationOnChain_m969,
	X509Chain_CheckRevocation_m970,
	X509Chain_CheckRevocation_m971,
	X509Chain_FindCrl_m972,
	X509Chain_ProcessCrlExtensions_m973,
	X509Chain_ProcessCrlEntryExtensions_m974,
	X509ChainElement__ctor_m975,
	X509ChainElement_get_Certificate_m976,
	X509ChainElement_get_ChainElementStatus_m977,
	X509ChainElement_get_StatusFlags_m978,
	X509ChainElement_set_StatusFlags_m979,
	X509ChainElement_Count_m980,
	X509ChainElement_Set_m981,
	X509ChainElement_UncompressFlags_m982,
	X509ChainElementCollection__ctor_m983,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m984,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m985,
	X509ChainElementCollection_get_Count_m986,
	X509ChainElementCollection_get_Item_m987,
	X509ChainElementCollection_get_SyncRoot_m988,
	X509ChainElementCollection_GetEnumerator_m989,
	X509ChainElementCollection_Add_m990,
	X509ChainElementCollection_Clear_m991,
	X509ChainElementCollection_Contains_m992,
	X509ChainElementEnumerator__ctor_m993,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m994,
	X509ChainElementEnumerator_get_Current_m995,
	X509ChainElementEnumerator_MoveNext_m996,
	X509ChainElementEnumerator_Reset_m997,
	X509ChainPolicy__ctor_m998,
	X509ChainPolicy_get_ExtraStore_m999,
	X509ChainPolicy_get_RevocationFlag_m1000,
	X509ChainPolicy_get_RevocationMode_m1001,
	X509ChainPolicy_get_VerificationFlags_m1002,
	X509ChainPolicy_get_VerificationTime_m1003,
	X509ChainPolicy_Reset_m1004,
	X509ChainStatus__ctor_m1005,
	X509ChainStatus_get_Status_m1006,
	X509ChainStatus_set_Status_m1007,
	X509ChainStatus_set_StatusInformation_m1008,
	X509ChainStatus_GetInformation_m1009,
	X509EnhancedKeyUsageExtension__ctor_m1010,
	X509EnhancedKeyUsageExtension_CopyFrom_m1011,
	X509EnhancedKeyUsageExtension_Decode_m1012,
	X509EnhancedKeyUsageExtension_ToString_m1013,
	X509Extension__ctor_m1014,
	X509Extension__ctor_m1015,
	X509Extension_get_Critical_m1016,
	X509Extension_set_Critical_m1017,
	X509Extension_CopyFrom_m1018,
	X509Extension_FormatUnkownData_m1019,
	X509ExtensionCollection__ctor_m1020,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m1021,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m1022,
	X509ExtensionCollection_get_Count_m1023,
	X509ExtensionCollection_get_SyncRoot_m1024,
	X509ExtensionCollection_get_Item_m1025,
	X509ExtensionCollection_GetEnumerator_m1026,
	X509ExtensionEnumerator__ctor_m1027,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m1028,
	X509ExtensionEnumerator_get_Current_m1029,
	X509ExtensionEnumerator_MoveNext_m1030,
	X509ExtensionEnumerator_Reset_m1031,
	X509KeyUsageExtension__ctor_m1032,
	X509KeyUsageExtension__ctor_m1033,
	X509KeyUsageExtension__ctor_m1034,
	X509KeyUsageExtension_get_KeyUsages_m1035,
	X509KeyUsageExtension_CopyFrom_m1036,
	X509KeyUsageExtension_GetValidFlags_m1037,
	X509KeyUsageExtension_Decode_m1038,
	X509KeyUsageExtension_Encode_m1039,
	X509KeyUsageExtension_ToString_m1040,
	X509Store__ctor_m1041,
	X509Store_get_Certificates_m1042,
	X509Store_get_Factory_m1043,
	X509Store_get_Store_m1044,
	X509Store_Close_m1045,
	X509Store_Open_m1046,
	X509SubjectKeyIdentifierExtension__ctor_m1047,
	X509SubjectKeyIdentifierExtension__ctor_m1048,
	X509SubjectKeyIdentifierExtension__ctor_m1049,
	X509SubjectKeyIdentifierExtension__ctor_m1050,
	X509SubjectKeyIdentifierExtension__ctor_m1051,
	X509SubjectKeyIdentifierExtension__ctor_m1052,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m1053,
	X509SubjectKeyIdentifierExtension_CopyFrom_m1054,
	X509SubjectKeyIdentifierExtension_FromHexChar_m1055,
	X509SubjectKeyIdentifierExtension_FromHexChars_m1056,
	X509SubjectKeyIdentifierExtension_FromHex_m1057,
	X509SubjectKeyIdentifierExtension_Decode_m1058,
	X509SubjectKeyIdentifierExtension_Encode_m1059,
	X509SubjectKeyIdentifierExtension_ToString_m1060,
	AsnEncodedData__ctor_m1061,
	AsnEncodedData__ctor_m1062,
	AsnEncodedData__ctor_m1063,
	AsnEncodedData_get_Oid_m1064,
	AsnEncodedData_set_Oid_m1065,
	AsnEncodedData_get_RawData_m1066,
	AsnEncodedData_set_RawData_m1067,
	AsnEncodedData_CopyFrom_m1068,
	AsnEncodedData_ToString_m1069,
	AsnEncodedData_Default_m1070,
	AsnEncodedData_BasicConstraintsExtension_m1071,
	AsnEncodedData_EnhancedKeyUsageExtension_m1072,
	AsnEncodedData_KeyUsageExtension_m1073,
	AsnEncodedData_SubjectKeyIdentifierExtension_m1074,
	AsnEncodedData_SubjectAltName_m1075,
	AsnEncodedData_NetscapeCertType_m1076,
	Oid__ctor_m1077,
	Oid__ctor_m1078,
	Oid__ctor_m1079,
	Oid__ctor_m1080,
	Oid_get_FriendlyName_m1081,
	Oid_get_Value_m1082,
	Oid_GetName_m1083,
	OidCollection__ctor_m1084,
	OidCollection_System_Collections_ICollection_CopyTo_m1085,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m1086,
	OidCollection_get_Count_m1087,
	OidCollection_get_Item_m1088,
	OidCollection_get_SyncRoot_m1089,
	OidCollection_Add_m1090,
	OidEnumerator__ctor_m1091,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m1092,
	OidEnumerator_MoveNext_m1093,
	OidEnumerator_Reset_m1094,
	MatchAppendEvaluator__ctor_m1095,
	MatchAppendEvaluator_Invoke_m1096,
	MatchAppendEvaluator_BeginInvoke_m1097,
	MatchAppendEvaluator_EndInvoke_m1098,
	BaseMachine__ctor_m1099,
	BaseMachine_Replace_m1100,
	BaseMachine_Scan_m1101,
	BaseMachine_LTRReplace_m1102,
	BaseMachine_RTLReplace_m1103,
	Capture__ctor_m1104,
	Capture__ctor_m1105,
	Capture_get_Index_m1106,
	Capture_get_Length_m1107,
	Capture_get_Value_m1108,
	Capture_ToString_m1109,
	Capture_get_Text_m1110,
	CaptureCollection__ctor_m1111,
	CaptureCollection_get_Count_m1112,
	CaptureCollection_SetValue_m1113,
	CaptureCollection_get_SyncRoot_m1114,
	CaptureCollection_CopyTo_m1115,
	CaptureCollection_GetEnumerator_m1116,
	Group__ctor_m1117,
	Group__ctor_m1118,
	Group__ctor_m1119,
	Group__cctor_m1120,
	Group_get_Captures_m1121,
	Group_get_Success_m1122,
	GroupCollection__ctor_m1123,
	GroupCollection_get_Count_m1124,
	GroupCollection_get_Item_m1125,
	GroupCollection_SetValue_m1126,
	GroupCollection_get_SyncRoot_m1127,
	GroupCollection_CopyTo_m1128,
	GroupCollection_GetEnumerator_m1129,
	Match__ctor_m1130,
	Match__ctor_m1131,
	Match__ctor_m1132,
	Match__cctor_m1133,
	Match_get_Empty_m1134,
	Match_get_Groups_m1135,
	Match_NextMatch_m1136,
	Match_get_Regex_m1137,
	Enumerator__ctor_m1138,
	Enumerator_System_Collections_IEnumerator_Reset_m1139,
	Enumerator_System_Collections_IEnumerator_get_Current_m1140,
	Enumerator_System_Collections_IEnumerator_MoveNext_m1141,
	MatchCollection__ctor_m1142,
	MatchCollection_get_Count_m1143,
	MatchCollection_get_Item_m1144,
	MatchCollection_get_SyncRoot_m1145,
	MatchCollection_CopyTo_m1146,
	MatchCollection_GetEnumerator_m1147,
	MatchCollection_TryToGet_m1148,
	MatchCollection_get_FullList_m1149,
	Regex__ctor_m1150,
	Regex__ctor_m1151,
	Regex__ctor_m1152,
	Regex__ctor_m1153,
	Regex__cctor_m1154,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m1155,
	Regex_Replace_m658,
	Regex_Replace_m1156,
	Regex_validate_options_m1157,
	Regex_Init_m1158,
	Regex_InitNewRegex_m1159,
	Regex_CreateMachineFactory_m1160,
	Regex_get_Options_m1161,
	Regex_get_RightToLeft_m1162,
	Regex_GroupNumberFromName_m1163,
	Regex_GetGroupIndex_m1164,
	Regex_default_startat_m1165,
	Regex_IsMatch_m1166,
	Regex_IsMatch_m1167,
	Regex_Match_m1168,
	Regex_Matches_m1169,
	Regex_Matches_m1170,
	Regex_Replace_m1171,
	Regex_Replace_m1172,
	Regex_ToString_m1173,
	Regex_get_GroupCount_m1174,
	Regex_get_Gap_m1175,
	Regex_CreateMachine_m1176,
	Regex_GetGroupNamesArray_m1177,
	Regex_get_GroupNumbers_m1178,
	Key__ctor_m1179,
	Key_GetHashCode_m1180,
	Key_Equals_m1181,
	Key_ToString_m1182,
	FactoryCache__ctor_m1183,
	FactoryCache_Add_m1184,
	FactoryCache_Cleanup_m1185,
	FactoryCache_Lookup_m1186,
	Node__ctor_m1187,
	MRUList__ctor_m1188,
	MRUList_Use_m1189,
	MRUList_Evict_m1190,
	CategoryUtils_CategoryFromName_m1191,
	CategoryUtils_IsCategory_m1192,
	CategoryUtils_IsCategory_m1193,
	LinkRef__ctor_m1194,
	InterpreterFactory__ctor_m1195,
	InterpreterFactory_NewInstance_m1196,
	InterpreterFactory_get_GroupCount_m1197,
	InterpreterFactory_get_Gap_m1198,
	InterpreterFactory_set_Gap_m1199,
	InterpreterFactory_get_Mapping_m1200,
	InterpreterFactory_set_Mapping_m1201,
	InterpreterFactory_get_NamesMapping_m1202,
	InterpreterFactory_set_NamesMapping_m1203,
	PatternLinkStack__ctor_m1204,
	PatternLinkStack_set_BaseAddress_m1205,
	PatternLinkStack_get_OffsetAddress_m1206,
	PatternLinkStack_set_OffsetAddress_m1207,
	PatternLinkStack_GetOffset_m1208,
	PatternLinkStack_GetCurrent_m1209,
	PatternLinkStack_SetCurrent_m1210,
	PatternCompiler__ctor_m1211,
	PatternCompiler_EncodeOp_m1212,
	PatternCompiler_GetMachineFactory_m1213,
	PatternCompiler_EmitFalse_m1214,
	PatternCompiler_EmitTrue_m1215,
	PatternCompiler_EmitCount_m1216,
	PatternCompiler_EmitCharacter_m1217,
	PatternCompiler_EmitCategory_m1218,
	PatternCompiler_EmitNotCategory_m1219,
	PatternCompiler_EmitRange_m1220,
	PatternCompiler_EmitSet_m1221,
	PatternCompiler_EmitString_m1222,
	PatternCompiler_EmitPosition_m1223,
	PatternCompiler_EmitOpen_m1224,
	PatternCompiler_EmitClose_m1225,
	PatternCompiler_EmitBalanceStart_m1226,
	PatternCompiler_EmitBalance_m1227,
	PatternCompiler_EmitReference_m1228,
	PatternCompiler_EmitIfDefined_m1229,
	PatternCompiler_EmitSub_m1230,
	PatternCompiler_EmitTest_m1231,
	PatternCompiler_EmitBranch_m1232,
	PatternCompiler_EmitJump_m1233,
	PatternCompiler_EmitRepeat_m1234,
	PatternCompiler_EmitUntil_m1235,
	PatternCompiler_EmitFastRepeat_m1236,
	PatternCompiler_EmitIn_m1237,
	PatternCompiler_EmitAnchor_m1238,
	PatternCompiler_EmitInfo_m1239,
	PatternCompiler_NewLink_m1240,
	PatternCompiler_ResolveLink_m1241,
	PatternCompiler_EmitBranchEnd_m1242,
	PatternCompiler_EmitAlternationEnd_m1243,
	PatternCompiler_MakeFlags_m1244,
	PatternCompiler_Emit_m1245,
	PatternCompiler_Emit_m1246,
	PatternCompiler_Emit_m1247,
	PatternCompiler_get_CurrentAddress_m1248,
	PatternCompiler_BeginLink_m1249,
	PatternCompiler_EmitLink_m1250,
	LinkStack__ctor_m1251,
	LinkStack_Push_m1252,
	LinkStack_Pop_m1253,
	Mark_get_IsDefined_m1254,
	Mark_get_Index_m1255,
	Mark_get_Length_m1256,
	IntStack_Pop_m1257,
	IntStack_Push_m1258,
	IntStack_get_Count_m1259,
	IntStack_set_Count_m1260,
	RepeatContext__ctor_m1261,
	RepeatContext_get_Count_m1262,
	RepeatContext_set_Count_m1263,
	RepeatContext_get_Start_m1264,
	RepeatContext_set_Start_m1265,
	RepeatContext_get_IsMinimum_m1266,
	RepeatContext_get_IsMaximum_m1267,
	RepeatContext_get_IsLazy_m1268,
	RepeatContext_get_Expression_m1269,
	RepeatContext_get_Previous_m1270,
	Interpreter__ctor_m1271,
	Interpreter_ReadProgramCount_m1272,
	Interpreter_Scan_m1273,
	Interpreter_Reset_m1274,
	Interpreter_Eval_m1275,
	Interpreter_EvalChar_m1276,
	Interpreter_TryMatch_m1277,
	Interpreter_IsPosition_m1278,
	Interpreter_IsWordChar_m1279,
	Interpreter_GetString_m1280,
	Interpreter_Open_m1281,
	Interpreter_Close_m1282,
	Interpreter_Balance_m1283,
	Interpreter_Checkpoint_m1284,
	Interpreter_Backtrack_m1285,
	Interpreter_ResetGroups_m1286,
	Interpreter_GetLastDefined_m1287,
	Interpreter_CreateMark_m1288,
	Interpreter_GetGroupInfo_m1289,
	Interpreter_PopulateGroup_m1290,
	Interpreter_GenerateMatch_m1291,
	Interval__ctor_m1292,
	Interval_get_Empty_m1293,
	Interval_get_IsDiscontiguous_m1294,
	Interval_get_IsSingleton_m1295,
	Interval_get_IsEmpty_m1296,
	Interval_get_Size_m1297,
	Interval_IsDisjoint_m1298,
	Interval_IsAdjacent_m1299,
	Interval_Contains_m1300,
	Interval_Contains_m1301,
	Interval_Intersects_m1302,
	Interval_Merge_m1303,
	Interval_CompareTo_m1304,
	Enumerator__ctor_m1305,
	Enumerator_get_Current_m1306,
	Enumerator_MoveNext_m1307,
	Enumerator_Reset_m1308,
	CostDelegate__ctor_m1309,
	CostDelegate_Invoke_m1310,
	CostDelegate_BeginInvoke_m1311,
	CostDelegate_EndInvoke_m1312,
	IntervalCollection__ctor_m1313,
	IntervalCollection_get_Item_m1314,
	IntervalCollection_Add_m1315,
	IntervalCollection_Normalize_m1316,
	IntervalCollection_GetMetaCollection_m1317,
	IntervalCollection_Optimize_m1318,
	IntervalCollection_get_Count_m1319,
	IntervalCollection_get_SyncRoot_m1320,
	IntervalCollection_CopyTo_m1321,
	IntervalCollection_GetEnumerator_m1322,
	Parser__ctor_m1323,
	Parser_ParseDecimal_m1324,
	Parser_ParseOctal_m1325,
	Parser_ParseHex_m1326,
	Parser_ParseNumber_m1327,
	Parser_ParseName_m1328,
	Parser_ParseRegularExpression_m1329,
	Parser_GetMapping_m1330,
	Parser_ParseGroup_m1331,
	Parser_ParseGroupingConstruct_m1332,
	Parser_ParseAssertionType_m1333,
	Parser_ParseOptions_m1334,
	Parser_ParseCharacterClass_m1335,
	Parser_ParseRepetitionBounds_m1336,
	Parser_ParseUnicodeCategory_m1337,
	Parser_ParseSpecial_m1338,
	Parser_ParseEscape_m1339,
	Parser_ParseName_m1340,
	Parser_IsNameChar_m1341,
	Parser_ParseNumber_m1342,
	Parser_ParseDigit_m1343,
	Parser_ConsumeWhitespace_m1344,
	Parser_ResolveReferences_m1345,
	Parser_HandleExplicitNumericGroups_m1346,
	Parser_IsIgnoreCase_m1347,
	Parser_IsMultiline_m1348,
	Parser_IsExplicitCapture_m1349,
	Parser_IsSingleline_m1350,
	Parser_IsIgnorePatternWhitespace_m1351,
	Parser_IsECMAScript_m1352,
	Parser_NewParseException_m1353,
	QuickSearch__ctor_m1354,
	QuickSearch__cctor_m1355,
	QuickSearch_get_Length_m1356,
	QuickSearch_Search_m1357,
	QuickSearch_SetupShiftTable_m1358,
	QuickSearch_GetShiftDistance_m1359,
	QuickSearch_GetChar_m1360,
	ReplacementEvaluator__ctor_m1361,
	ReplacementEvaluator_Evaluate_m1362,
	ReplacementEvaluator_EvaluateAppend_m1363,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m1364,
	ReplacementEvaluator_Ensure_m1365,
	ReplacementEvaluator_AddFromReplacement_m1366,
	ReplacementEvaluator_AddInt_m1367,
	ReplacementEvaluator_Compile_m1368,
	ReplacementEvaluator_CompileTerm_m1369,
	ExpressionCollection__ctor_m1370,
	ExpressionCollection_Add_m1371,
	ExpressionCollection_get_Item_m1372,
	ExpressionCollection_set_Item_m1373,
	ExpressionCollection_OnValidate_m1374,
	Expression__ctor_m1375,
	Expression_GetFixedWidth_m1376,
	Expression_GetAnchorInfo_m1377,
	CompositeExpression__ctor_m1378,
	CompositeExpression_get_Expressions_m1379,
	CompositeExpression_GetWidth_m1380,
	CompositeExpression_IsComplex_m1381,
	Group__ctor_m1382,
	Group_AppendExpression_m1383,
	Group_Compile_m1384,
	Group_GetWidth_m1385,
	Group_GetAnchorInfo_m1386,
	RegularExpression__ctor_m1387,
	RegularExpression_set_GroupCount_m1388,
	RegularExpression_Compile_m1389,
	CapturingGroup__ctor_m1390,
	CapturingGroup_get_Index_m1391,
	CapturingGroup_set_Index_m1392,
	CapturingGroup_get_Name_m1393,
	CapturingGroup_set_Name_m1394,
	CapturingGroup_get_IsNamed_m1395,
	CapturingGroup_Compile_m1396,
	CapturingGroup_IsComplex_m1397,
	CapturingGroup_CompareTo_m1398,
	BalancingGroup__ctor_m1399,
	BalancingGroup_set_Balance_m1400,
	BalancingGroup_Compile_m1401,
	NonBacktrackingGroup__ctor_m1402,
	NonBacktrackingGroup_Compile_m1403,
	NonBacktrackingGroup_IsComplex_m1404,
	Repetition__ctor_m1405,
	Repetition_get_Expression_m1406,
	Repetition_set_Expression_m1407,
	Repetition_get_Minimum_m1408,
	Repetition_Compile_m1409,
	Repetition_GetWidth_m1410,
	Repetition_GetAnchorInfo_m1411,
	Assertion__ctor_m1412,
	Assertion_get_TrueExpression_m1413,
	Assertion_set_TrueExpression_m1414,
	Assertion_get_FalseExpression_m1415,
	Assertion_set_FalseExpression_m1416,
	Assertion_GetWidth_m1417,
	CaptureAssertion__ctor_m1418,
	CaptureAssertion_set_CapturingGroup_m1419,
	CaptureAssertion_Compile_m1420,
	CaptureAssertion_IsComplex_m1421,
	CaptureAssertion_get_Alternate_m1422,
	ExpressionAssertion__ctor_m1423,
	ExpressionAssertion_set_Reverse_m1424,
	ExpressionAssertion_set_Negate_m1425,
	ExpressionAssertion_get_TestExpression_m1426,
	ExpressionAssertion_set_TestExpression_m1427,
	ExpressionAssertion_Compile_m1428,
	ExpressionAssertion_IsComplex_m1429,
	Alternation__ctor_m1430,
	Alternation_get_Alternatives_m1431,
	Alternation_AddAlternative_m1432,
	Alternation_Compile_m1433,
	Alternation_GetWidth_m1434,
	Literal__ctor_m1435,
	Literal_CompileLiteral_m1436,
	Literal_Compile_m1437,
	Literal_GetWidth_m1438,
	Literal_GetAnchorInfo_m1439,
	Literal_IsComplex_m1440,
	PositionAssertion__ctor_m1441,
	PositionAssertion_Compile_m1442,
	PositionAssertion_GetWidth_m1443,
	PositionAssertion_IsComplex_m1444,
	PositionAssertion_GetAnchorInfo_m1445,
	Reference__ctor_m1446,
	Reference_get_CapturingGroup_m1447,
	Reference_set_CapturingGroup_m1448,
	Reference_get_IgnoreCase_m1449,
	Reference_Compile_m1450,
	Reference_GetWidth_m1451,
	Reference_IsComplex_m1452,
	BackslashNumber__ctor_m1453,
	BackslashNumber_ResolveReference_m1454,
	BackslashNumber_Compile_m1455,
	CharacterClass__ctor_m1456,
	CharacterClass__ctor_m1457,
	CharacterClass__cctor_m1458,
	CharacterClass_AddCategory_m1459,
	CharacterClass_AddCharacter_m1460,
	CharacterClass_AddRange_m1461,
	CharacterClass_Compile_m1462,
	CharacterClass_GetWidth_m1463,
	CharacterClass_IsComplex_m1464,
	CharacterClass_GetIntervalCost_m1465,
	AnchorInfo__ctor_m1466,
	AnchorInfo__ctor_m1467,
	AnchorInfo__ctor_m1468,
	AnchorInfo_get_Offset_m1469,
	AnchorInfo_get_Width_m1470,
	AnchorInfo_get_Length_m1471,
	AnchorInfo_get_IsUnknownWidth_m1472,
	AnchorInfo_get_IsComplete_m1473,
	AnchorInfo_get_Substring_m1474,
	AnchorInfo_get_IgnoreCase_m1475,
	AnchorInfo_get_Position_m1476,
	AnchorInfo_get_IsSubstring_m1477,
	AnchorInfo_get_IsPosition_m1478,
	AnchorInfo_GetInterval_m1479,
	DefaultUriParser__ctor_m1480,
	DefaultUriParser__ctor_m1481,
	UriScheme__ctor_m1482,
	Uri__ctor_m1483,
	Uri__ctor_m1484,
	Uri__ctor_m1485,
	Uri__cctor_m1486,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m1487,
	Uri_get_AbsoluteUri_m1488,
	Uri_get_Authority_m1489,
	Uri_get_Host_m1490,
	Uri_get_IsFile_m1491,
	Uri_get_IsLoopback_m1492,
	Uri_get_IsUnc_m1493,
	Uri_get_Scheme_m1494,
	Uri_get_IsAbsoluteUri_m1495,
	Uri_CheckHostName_m1496,
	Uri_IsIPv4Address_m1497,
	Uri_IsDomainAddress_m1498,
	Uri_CheckSchemeName_m1499,
	Uri_IsAlpha_m1500,
	Uri_Equals_m1501,
	Uri_InternalEquals_m1502,
	Uri_GetHashCode_m1503,
	Uri_GetLeftPart_m1504,
	Uri_FromHex_m1505,
	Uri_HexEscape_m1506,
	Uri_IsHexDigit_m1507,
	Uri_IsHexEncoding_m1508,
	Uri_AppendQueryAndFragment_m1509,
	Uri_ToString_m1510,
	Uri_EscapeString_m1511,
	Uri_EscapeString_m1512,
	Uri_ParseUri_m1513,
	Uri_Unescape_m1514,
	Uri_Unescape_m1515,
	Uri_ParseAsWindowsUNC_m1516,
	Uri_ParseAsWindowsAbsoluteFilePath_m1517,
	Uri_ParseAsUnixAbsoluteFilePath_m1518,
	Uri_Parse_m1519,
	Uri_ParseNoExceptions_m1520,
	Uri_CompactEscaped_m1521,
	Uri_Reduce_m1522,
	Uri_HexUnescapeMultiByte_m1523,
	Uri_GetSchemeDelimiter_m1524,
	Uri_GetDefaultPort_m1525,
	Uri_GetOpaqueWiseSchemeDelimiter_m1526,
	Uri_IsPredefinedScheme_m1527,
	Uri_get_Parser_m1528,
	Uri_EnsureAbsoluteUri_m1529,
	Uri_op_Equality_m1530,
	UriFormatException__ctor_m1531,
	UriFormatException__ctor_m1532,
	UriFormatException__ctor_m1533,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m1534,
	UriParser__ctor_m1535,
	UriParser__cctor_m1536,
	UriParser_InitializeAndValidate_m1537,
	UriParser_OnRegister_m1538,
	UriParser_set_SchemeName_m1539,
	UriParser_get_DefaultPort_m1540,
	UriParser_set_DefaultPort_m1541,
	UriParser_CreateDefaults_m1542,
	UriParser_InternalRegister_m1543,
	UriParser_GetParser_m1544,
	RemoteCertificateValidationCallback__ctor_m1545,
	RemoteCertificateValidationCallback_Invoke_m1546,
	RemoteCertificateValidationCallback_BeginInvoke_m1547,
	RemoteCertificateValidationCallback_EndInvoke_m1548,
	MatchEvaluator__ctor_m1549,
	MatchEvaluator_Invoke_m1550,
	MatchEvaluator_BeginInvoke_m1551,
	MatchEvaluator_EndInvoke_m1552,
	ExtensionAttribute__ctor_m1750,
	Locale_GetText_m1751,
	Locale_GetText_m1752,
	KeyBuilder_get_Rng_m1753,
	KeyBuilder_Key_m1754,
	KeyBuilder_IV_m1755,
	SymmetricTransform__ctor_m1756,
	SymmetricTransform_System_IDisposable_Dispose_m1757,
	SymmetricTransform_Finalize_m1758,
	SymmetricTransform_Dispose_m1759,
	SymmetricTransform_get_CanReuseTransform_m1760,
	SymmetricTransform_Transform_m1761,
	SymmetricTransform_CBC_m1762,
	SymmetricTransform_CFB_m1763,
	SymmetricTransform_OFB_m1764,
	SymmetricTransform_CTS_m1765,
	SymmetricTransform_CheckInput_m1766,
	SymmetricTransform_TransformBlock_m1767,
	SymmetricTransform_get_KeepLastBlock_m1768,
	SymmetricTransform_InternalTransformBlock_m1769,
	SymmetricTransform_Random_m1770,
	SymmetricTransform_ThrowBadPaddingException_m1771,
	SymmetricTransform_FinalEncrypt_m1772,
	SymmetricTransform_FinalDecrypt_m1773,
	SymmetricTransform_TransformFinalBlock_m1774,
	Aes__ctor_m1775,
	AesManaged__ctor_m1776,
	AesManaged_GenerateIV_m1777,
	AesManaged_GenerateKey_m1778,
	AesManaged_CreateDecryptor_m1779,
	AesManaged_CreateEncryptor_m1780,
	AesManaged_get_IV_m1781,
	AesManaged_set_IV_m1782,
	AesManaged_get_Key_m1783,
	AesManaged_set_Key_m1784,
	AesManaged_get_KeySize_m1785,
	AesManaged_set_KeySize_m1786,
	AesManaged_CreateDecryptor_m1787,
	AesManaged_CreateEncryptor_m1788,
	AesManaged_Dispose_m1789,
	AesTransform__ctor_m1790,
	AesTransform__cctor_m1791,
	AesTransform_ECB_m1792,
	AesTransform_SubByte_m1793,
	AesTransform_Encrypt128_m1794,
	AesTransform_Decrypt128_m1795,
	Locale_GetText_m1811,
	ModulusRing__ctor_m1812,
	ModulusRing_BarrettReduction_m1813,
	ModulusRing_Multiply_m1814,
	ModulusRing_Difference_m1815,
	ModulusRing_Pow_m1816,
	ModulusRing_Pow_m1817,
	Kernel_AddSameSign_m1818,
	Kernel_Subtract_m1819,
	Kernel_MinusEq_m1820,
	Kernel_PlusEq_m1821,
	Kernel_Compare_m1822,
	Kernel_SingleByteDivideInPlace_m1823,
	Kernel_DwordMod_m1824,
	Kernel_DwordDivMod_m1825,
	Kernel_multiByteDivide_m1826,
	Kernel_LeftShift_m1827,
	Kernel_RightShift_m1828,
	Kernel_Multiply_m1829,
	Kernel_MultiplyMod2p32pmod_m1830,
	Kernel_modInverse_m1831,
	Kernel_modInverse_m1832,
	BigInteger__ctor_m1833,
	BigInteger__ctor_m1834,
	BigInteger__ctor_m1835,
	BigInteger__ctor_m1836,
	BigInteger__ctor_m1837,
	BigInteger__cctor_m1838,
	BigInteger_get_Rng_m1839,
	BigInteger_GenerateRandom_m1840,
	BigInteger_GenerateRandom_m1841,
	BigInteger_BitCount_m1842,
	BigInteger_TestBit_m1843,
	BigInteger_SetBit_m1844,
	BigInteger_SetBit_m1845,
	BigInteger_LowestSetBit_m1846,
	BigInteger_GetBytes_m1847,
	BigInteger_ToString_m1848,
	BigInteger_ToString_m1849,
	BigInteger_Normalize_m1850,
	BigInteger_Clear_m1851,
	BigInteger_GetHashCode_m1852,
	BigInteger_ToString_m1853,
	BigInteger_Equals_m1854,
	BigInteger_ModInverse_m1855,
	BigInteger_ModPow_m1856,
	BigInteger_GeneratePseudoPrime_m1857,
	BigInteger_Incr2_m1858,
	BigInteger_op_Implicit_m1859,
	BigInteger_op_Implicit_m1860,
	BigInteger_op_Addition_m1861,
	BigInteger_op_Subtraction_m1862,
	BigInteger_op_Modulus_m1863,
	BigInteger_op_Modulus_m1864,
	BigInteger_op_Division_m1865,
	BigInteger_op_Multiply_m1866,
	BigInteger_op_LeftShift_m1867,
	BigInteger_op_RightShift_m1868,
	BigInteger_op_Equality_m1869,
	BigInteger_op_Inequality_m1870,
	BigInteger_op_Equality_m1871,
	BigInteger_op_Inequality_m1872,
	BigInteger_op_GreaterThan_m1873,
	BigInteger_op_LessThan_m1874,
	BigInteger_op_GreaterThanOrEqual_m1875,
	BigInteger_op_LessThanOrEqual_m1876,
	PrimalityTests_GetSPPRounds_m1877,
	PrimalityTests_RabinMillerTest_m1878,
	PrimeGeneratorBase__ctor_m1879,
	PrimeGeneratorBase_get_Confidence_m1880,
	PrimeGeneratorBase_get_PrimalityTest_m1881,
	PrimeGeneratorBase_get_TrialDivisionBounds_m1882,
	SequentialSearchPrimeGeneratorBase__ctor_m1883,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m1884,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1885,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m1886,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m1887,
	ASN1__ctor_m1643,
	ASN1__ctor_m1644,
	ASN1__ctor_m1626,
	ASN1_get_Count_m1630,
	ASN1_get_Tag_m1627,
	ASN1_get_Length_m1657,
	ASN1_get_Value_m1629,
	ASN1_set_Value_m1888,
	ASN1_CompareArray_m1889,
	ASN1_CompareValue_m1656,
	ASN1_Add_m1645,
	ASN1_GetBytes_m1890,
	ASN1_Decode_m1891,
	ASN1_DecodeTLV_m1892,
	ASN1_get_Item_m1631,
	ASN1_Element_m1893,
	ASN1_ToString_m1894,
	ASN1Convert_FromInt32_m1646,
	ASN1Convert_FromOid_m1895,
	ASN1Convert_ToInt32_m1642,
	ASN1Convert_ToOid_m1697,
	ASN1Convert_ToDateTime_m1896,
	BitConverterLE_GetUIntBytes_m1897,
	BitConverterLE_GetBytes_m1898,
	ContentInfo__ctor_m1899,
	ContentInfo__ctor_m1900,
	ContentInfo__ctor_m1901,
	ContentInfo__ctor_m1902,
	ContentInfo_get_ASN1_m1903,
	ContentInfo_get_Content_m1904,
	ContentInfo_set_Content_m1905,
	ContentInfo_get_ContentType_m1906,
	ContentInfo_set_ContentType_m1907,
	ContentInfo_GetASN1_m1908,
	EncryptedData__ctor_m1909,
	EncryptedData__ctor_m1910,
	EncryptedData_get_EncryptionAlgorithm_m1911,
	EncryptedData_get_EncryptedContent_m1912,
	ARC4Managed__ctor_m1913,
	ARC4Managed_Finalize_m1914,
	ARC4Managed_Dispose_m1915,
	ARC4Managed_get_Key_m1916,
	ARC4Managed_set_Key_m1917,
	ARC4Managed_get_CanReuseTransform_m1918,
	ARC4Managed_CreateEncryptor_m1919,
	ARC4Managed_CreateDecryptor_m1920,
	ARC4Managed_GenerateIV_m1921,
	ARC4Managed_GenerateKey_m1922,
	ARC4Managed_KeySetup_m1923,
	ARC4Managed_CheckInput_m1924,
	ARC4Managed_TransformBlock_m1925,
	ARC4Managed_InternalTransformBlock_m1926,
	ARC4Managed_TransformFinalBlock_m1927,
	CryptoConvert_ToHex_m1711,
	KeyBuilder_get_Rng_m1928,
	KeyBuilder_Key_m1929,
	MD2__ctor_m1930,
	MD2_Create_m1931,
	MD2_Create_m1932,
	MD2Managed__ctor_m1933,
	MD2Managed__cctor_m1934,
	MD2Managed_Padding_m1935,
	MD2Managed_Initialize_m1936,
	MD2Managed_HashCore_m1937,
	MD2Managed_HashFinal_m1938,
	MD2Managed_MD2Transform_m1939,
	PKCS1__cctor_m1940,
	PKCS1_Compare_m1941,
	PKCS1_I2OSP_m1942,
	PKCS1_OS2IP_m1943,
	PKCS1_RSASP1_m1944,
	PKCS1_RSAVP1_m1945,
	PKCS1_Sign_v15_m1946,
	PKCS1_Verify_v15_m1947,
	PKCS1_Verify_v15_m1948,
	PKCS1_Encode_v15_m1949,
	PrivateKeyInfo__ctor_m1950,
	PrivateKeyInfo__ctor_m1951,
	PrivateKeyInfo_get_PrivateKey_m1952,
	PrivateKeyInfo_Decode_m1953,
	PrivateKeyInfo_RemoveLeadingZero_m1954,
	PrivateKeyInfo_Normalize_m1955,
	PrivateKeyInfo_DecodeRSA_m1956,
	PrivateKeyInfo_DecodeDSA_m1957,
	EncryptedPrivateKeyInfo__ctor_m1958,
	EncryptedPrivateKeyInfo__ctor_m1959,
	EncryptedPrivateKeyInfo_get_Algorithm_m1960,
	EncryptedPrivateKeyInfo_get_EncryptedData_m1961,
	EncryptedPrivateKeyInfo_get_Salt_m1962,
	EncryptedPrivateKeyInfo_get_IterationCount_m1963,
	EncryptedPrivateKeyInfo_Decode_m1964,
	RC4__ctor_m1965,
	RC4__cctor_m1966,
	RC4_get_IV_m1967,
	RC4_set_IV_m1968,
	KeyGeneratedEventHandler__ctor_m1969,
	KeyGeneratedEventHandler_Invoke_m1970,
	KeyGeneratedEventHandler_BeginInvoke_m1971,
	KeyGeneratedEventHandler_EndInvoke_m1972,
	RSAManaged__ctor_m1973,
	RSAManaged__ctor_m1974,
	RSAManaged_Finalize_m1975,
	RSAManaged_GenerateKeyPair_m1976,
	RSAManaged_get_KeySize_m1977,
	RSAManaged_get_PublicOnly_m1619,
	RSAManaged_DecryptValue_m1978,
	RSAManaged_EncryptValue_m1979,
	RSAManaged_ExportParameters_m1980,
	RSAManaged_ImportParameters_m1981,
	RSAManaged_Dispose_m1982,
	RSAManaged_ToXmlString_m1983,
	RSAManaged_GetPaddedValue_m1984,
	SafeBag__ctor_m1985,
	SafeBag_get_BagOID_m1986,
	SafeBag_get_ASN1_m1987,
	DeriveBytes__ctor_m1988,
	DeriveBytes__cctor_m1989,
	DeriveBytes_set_HashName_m1990,
	DeriveBytes_set_IterationCount_m1991,
	DeriveBytes_set_Password_m1992,
	DeriveBytes_set_Salt_m1993,
	DeriveBytes_Adjust_m1994,
	DeriveBytes_Derive_m1995,
	DeriveBytes_DeriveKey_m1996,
	DeriveBytes_DeriveIV_m1997,
	DeriveBytes_DeriveMAC_m1998,
	PKCS12__ctor_m1999,
	PKCS12__ctor_m1659,
	PKCS12__ctor_m1660,
	PKCS12__cctor_m2000,
	PKCS12_Decode_m2001,
	PKCS12_Finalize_m2002,
	PKCS12_set_Password_m2003,
	PKCS12_get_IterationCount_m2004,
	PKCS12_set_IterationCount_m2005,
	PKCS12_get_Keys_m1663,
	PKCS12_get_Certificates_m1661,
	PKCS12_get_RNG_m2006,
	PKCS12_Compare_m2007,
	PKCS12_GetSymmetricAlgorithm_m2008,
	PKCS12_Decrypt_m2009,
	PKCS12_Decrypt_m2010,
	PKCS12_Encrypt_m2011,
	PKCS12_GetExistingParameters_m2012,
	PKCS12_AddPrivateKey_m2013,
	PKCS12_ReadSafeBag_m2014,
	PKCS12_CertificateSafeBag_m2015,
	PKCS12_MAC_m2016,
	PKCS12_GetBytes_m2017,
	PKCS12_EncryptedContentInfo_m2018,
	PKCS12_AddCertificate_m2019,
	PKCS12_AddCertificate_m2020,
	PKCS12_RemoveCertificate_m2021,
	PKCS12_RemoveCertificate_m2022,
	PKCS12_Clone_m2023,
	PKCS12_get_MaximumPasswordLength_m2024,
	X501__cctor_m2025,
	X501_ToString_m2026,
	X501_ToString_m1635,
	X501_AppendEntry_m2027,
	X509Certificate__ctor_m1666,
	X509Certificate__cctor_m2028,
	X509Certificate_Parse_m2029,
	X509Certificate_GetUnsignedBigInteger_m2030,
	X509Certificate_get_DSA_m1621,
	X509Certificate_set_DSA_m1664,
	X509Certificate_get_Extensions_m1683,
	X509Certificate_get_Hash_m2031,
	X509Certificate_get_IssuerName_m2032,
	X509Certificate_get_KeyAlgorithm_m2033,
	X509Certificate_get_KeyAlgorithmParameters_m2034,
	X509Certificate_set_KeyAlgorithmParameters_m2035,
	X509Certificate_get_PublicKey_m2036,
	X509Certificate_get_RSA_m2037,
	X509Certificate_set_RSA_m2038,
	X509Certificate_get_RawData_m2039,
	X509Certificate_get_SerialNumber_m2040,
	X509Certificate_get_Signature_m2041,
	X509Certificate_get_SignatureAlgorithm_m2042,
	X509Certificate_get_SubjectName_m2043,
	X509Certificate_get_ValidFrom_m2044,
	X509Certificate_get_ValidUntil_m2045,
	X509Certificate_get_Version_m1655,
	X509Certificate_get_IsCurrent_m2046,
	X509Certificate_WasCurrent_m2047,
	X509Certificate_VerifySignature_m2048,
	X509Certificate_VerifySignature_m2049,
	X509Certificate_VerifySignature_m1682,
	X509Certificate_get_IsSelfSigned_m2050,
	X509Certificate_GetIssuerName_m1650,
	X509Certificate_GetSubjectName_m1653,
	X509Certificate_GetObjectData_m2051,
	X509Certificate_PEM_m2052,
	X509CertificateEnumerator__ctor_m2053,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m2054,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m2055,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m2056,
	X509CertificateEnumerator_get_Current_m1708,
	X509CertificateEnumerator_MoveNext_m2057,
	X509CertificateEnumerator_Reset_m2058,
	X509CertificateCollection__ctor_m2059,
	X509CertificateCollection__ctor_m2060,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m2061,
	X509CertificateCollection_get_Item_m1662,
	X509CertificateCollection_Add_m2062,
	X509CertificateCollection_AddRange_m2063,
	X509CertificateCollection_Contains_m2064,
	X509CertificateCollection_GetEnumerator_m1707,
	X509CertificateCollection_GetHashCode_m2065,
	X509CertificateCollection_IndexOf_m2066,
	X509CertificateCollection_Remove_m2067,
	X509CertificateCollection_Compare_m2068,
	X509Chain__ctor_m2069,
	X509Chain__ctor_m2070,
	X509Chain_get_Status_m2071,
	X509Chain_get_TrustAnchors_m2072,
	X509Chain_Build_m2073,
	X509Chain_IsValid_m2074,
	X509Chain_FindCertificateParent_m2075,
	X509Chain_FindCertificateRoot_m2076,
	X509Chain_IsTrusted_m2077,
	X509Chain_IsParent_m2078,
	X509CrlEntry__ctor_m2079,
	X509CrlEntry_get_SerialNumber_m2080,
	X509CrlEntry_get_RevocationDate_m1690,
	X509CrlEntry_get_Extensions_m1696,
	X509Crl__ctor_m2081,
	X509Crl_Parse_m2082,
	X509Crl_get_Extensions_m1685,
	X509Crl_get_Hash_m2083,
	X509Crl_get_IssuerName_m1693,
	X509Crl_get_NextUpdate_m1691,
	X509Crl_Compare_m2084,
	X509Crl_GetCrlEntry_m1689,
	X509Crl_GetCrlEntry_m2085,
	X509Crl_GetHashName_m2086,
	X509Crl_VerifySignature_m2087,
	X509Crl_VerifySignature_m2088,
	X509Crl_VerifySignature_m1688,
	X509Extension__ctor_m2089,
	X509Extension__ctor_m2090,
	X509Extension_Decode_m2091,
	X509Extension_Encode_m2092,
	X509Extension_get_Oid_m1695,
	X509Extension_get_Critical_m1694,
	X509Extension_get_Value_m1699,
	X509Extension_Equals_m2093,
	X509Extension_GetHashCode_m2094,
	X509Extension_WriteLine_m2095,
	X509Extension_ToString_m2096,
	X509ExtensionCollection__ctor_m2097,
	X509ExtensionCollection__ctor_m2098,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m2099,
	X509ExtensionCollection_IndexOf_m2100,
	X509ExtensionCollection_get_Item_m1684,
	X509Store__ctor_m2101,
	X509Store_get_Certificates_m1706,
	X509Store_get_Crls_m1692,
	X509Store_Load_m2102,
	X509Store_LoadCertificate_m2103,
	X509Store_LoadCrl_m2104,
	X509Store_CheckStore_m2105,
	X509Store_BuildCertificatesCollection_m2106,
	X509Store_BuildCrlsCollection_m2107,
	X509StoreManager_get_CurrentUser_m1703,
	X509StoreManager_get_LocalMachine_m1704,
	X509StoreManager_get_TrustedRootCertificates_m2108,
	X509Stores__ctor_m2109,
	X509Stores_get_TrustedRoot_m2110,
	X509Stores_Open_m1705,
	AuthorityKeyIdentifierExtension__ctor_m1686,
	AuthorityKeyIdentifierExtension_Decode_m2111,
	AuthorityKeyIdentifierExtension_get_Identifier_m1687,
	AuthorityKeyIdentifierExtension_ToString_m2112,
	BasicConstraintsExtension__ctor_m2113,
	BasicConstraintsExtension_Decode_m2114,
	BasicConstraintsExtension_Encode_m2115,
	BasicConstraintsExtension_get_CertificateAuthority_m2116,
	BasicConstraintsExtension_ToString_m2117,
	ExtendedKeyUsageExtension__ctor_m2118,
	ExtendedKeyUsageExtension_Decode_m2119,
	ExtendedKeyUsageExtension_Encode_m2120,
	ExtendedKeyUsageExtension_get_KeyPurpose_m2121,
	ExtendedKeyUsageExtension_ToString_m2122,
	GeneralNames__ctor_m2123,
	GeneralNames_get_DNSNames_m2124,
	GeneralNames_get_IPAddresses_m2125,
	GeneralNames_ToString_m2126,
	KeyUsageExtension__ctor_m2127,
	KeyUsageExtension_Decode_m2128,
	KeyUsageExtension_Encode_m2129,
	KeyUsageExtension_Support_m2130,
	KeyUsageExtension_ToString_m2131,
	NetscapeCertTypeExtension__ctor_m2132,
	NetscapeCertTypeExtension_Decode_m2133,
	NetscapeCertTypeExtension_Support_m2134,
	NetscapeCertTypeExtension_ToString_m2135,
	SubjectAltNameExtension__ctor_m2136,
	SubjectAltNameExtension_Decode_m2137,
	SubjectAltNameExtension_get_DNSNames_m2138,
	SubjectAltNameExtension_get_IPAddresses_m2139,
	SubjectAltNameExtension_ToString_m2140,
	HMAC__ctor_m2141,
	HMAC_get_Key_m2142,
	HMAC_set_Key_m2143,
	HMAC_Initialize_m2144,
	HMAC_HashFinal_m2145,
	HMAC_HashCore_m2146,
	HMAC_initializePad_m2147,
	MD5SHA1__ctor_m2148,
	MD5SHA1_Initialize_m2149,
	MD5SHA1_HashFinal_m2150,
	MD5SHA1_HashCore_m2151,
	MD5SHA1_CreateSignature_m2152,
	MD5SHA1_VerifySignature_m2153,
	Alert__ctor_m2154,
	Alert__ctor_m2155,
	Alert_get_Level_m2156,
	Alert_get_Description_m2157,
	Alert_get_IsWarning_m2158,
	Alert_get_IsCloseNotify_m2159,
	Alert_inferAlertLevel_m2160,
	Alert_GetAlertMessage_m2161,
	CipherSuite__ctor_m2162,
	CipherSuite__cctor_m2163,
	CipherSuite_get_EncryptionCipher_m2164,
	CipherSuite_get_DecryptionCipher_m2165,
	CipherSuite_get_ClientHMAC_m2166,
	CipherSuite_get_ServerHMAC_m2167,
	CipherSuite_get_CipherAlgorithmType_m2168,
	CipherSuite_get_HashAlgorithmName_m2169,
	CipherSuite_get_HashAlgorithmType_m2170,
	CipherSuite_get_HashSize_m2171,
	CipherSuite_get_ExchangeAlgorithmType_m2172,
	CipherSuite_get_CipherMode_m2173,
	CipherSuite_get_Code_m2174,
	CipherSuite_get_Name_m2175,
	CipherSuite_get_IsExportable_m2176,
	CipherSuite_get_KeyMaterialSize_m2177,
	CipherSuite_get_KeyBlockSize_m2178,
	CipherSuite_get_ExpandedKeyMaterialSize_m2179,
	CipherSuite_get_EffectiveKeyBits_m2180,
	CipherSuite_get_IvSize_m2181,
	CipherSuite_get_Context_m2182,
	CipherSuite_set_Context_m2183,
	CipherSuite_Write_m2184,
	CipherSuite_Write_m2185,
	CipherSuite_InitializeCipher_m2186,
	CipherSuite_EncryptRecord_m2187,
	CipherSuite_DecryptRecord_m2188,
	CipherSuite_CreatePremasterSecret_m2189,
	CipherSuite_PRF_m2190,
	CipherSuite_Expand_m2191,
	CipherSuite_createEncryptionCipher_m2192,
	CipherSuite_createDecryptionCipher_m2193,
	CipherSuiteCollection__ctor_m2194,
	CipherSuiteCollection_System_Collections_IList_get_Item_m2195,
	CipherSuiteCollection_System_Collections_IList_set_Item_m2196,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m2197,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m2198,
	CipherSuiteCollection_System_Collections_IList_Contains_m2199,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m2200,
	CipherSuiteCollection_System_Collections_IList_Insert_m2201,
	CipherSuiteCollection_System_Collections_IList_Remove_m2202,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m2203,
	CipherSuiteCollection_System_Collections_IList_Add_m2204,
	CipherSuiteCollection_get_Item_m2205,
	CipherSuiteCollection_get_Item_m2206,
	CipherSuiteCollection_set_Item_m2207,
	CipherSuiteCollection_get_Item_m2208,
	CipherSuiteCollection_get_Count_m2209,
	CipherSuiteCollection_CopyTo_m2210,
	CipherSuiteCollection_Clear_m2211,
	CipherSuiteCollection_IndexOf_m2212,
	CipherSuiteCollection_IndexOf_m2213,
	CipherSuiteCollection_Add_m2214,
	CipherSuiteCollection_add_m2215,
	CipherSuiteCollection_add_m2216,
	CipherSuiteCollection_cultureAwareCompare_m2217,
	CipherSuiteFactory_GetSupportedCiphers_m2218,
	CipherSuiteFactory_GetTls1SupportedCiphers_m2219,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m2220,
	ClientContext__ctor_m2221,
	ClientContext_get_SslStream_m2222,
	ClientContext_get_ClientHelloProtocol_m2223,
	ClientContext_set_ClientHelloProtocol_m2224,
	ClientContext_Clear_m2225,
	ClientRecordProtocol__ctor_m2226,
	ClientRecordProtocol_GetMessage_m2227,
	ClientRecordProtocol_ProcessHandshakeMessage_m2228,
	ClientRecordProtocol_createClientHandshakeMessage_m2229,
	ClientRecordProtocol_createServerHandshakeMessage_m2230,
	ClientSessionInfo__ctor_m2231,
	ClientSessionInfo__cctor_m2232,
	ClientSessionInfo_Finalize_m2233,
	ClientSessionInfo_get_HostName_m2234,
	ClientSessionInfo_get_Id_m2235,
	ClientSessionInfo_get_Valid_m2236,
	ClientSessionInfo_GetContext_m2237,
	ClientSessionInfo_SetContext_m2238,
	ClientSessionInfo_KeepAlive_m2239,
	ClientSessionInfo_Dispose_m2240,
	ClientSessionInfo_Dispose_m2241,
	ClientSessionInfo_CheckDisposed_m2242,
	ClientSessionCache__cctor_m2243,
	ClientSessionCache_Add_m2244,
	ClientSessionCache_FromHost_m2245,
	ClientSessionCache_FromContext_m2246,
	ClientSessionCache_SetContextInCache_m2247,
	ClientSessionCache_SetContextFromCache_m2248,
	Context__ctor_m2249,
	Context_get_AbbreviatedHandshake_m2250,
	Context_set_AbbreviatedHandshake_m2251,
	Context_get_ProtocolNegotiated_m2252,
	Context_set_ProtocolNegotiated_m2253,
	Context_get_SecurityProtocol_m2254,
	Context_set_SecurityProtocol_m2255,
	Context_get_SecurityProtocolFlags_m2256,
	Context_get_Protocol_m2257,
	Context_get_SessionId_m2258,
	Context_set_SessionId_m2259,
	Context_get_CompressionMethod_m2260,
	Context_set_CompressionMethod_m2261,
	Context_get_ServerSettings_m2262,
	Context_get_ClientSettings_m2263,
	Context_get_LastHandshakeMsg_m2264,
	Context_set_LastHandshakeMsg_m2265,
	Context_get_HandshakeState_m2266,
	Context_set_HandshakeState_m2267,
	Context_get_ReceivedConnectionEnd_m2268,
	Context_set_ReceivedConnectionEnd_m2269,
	Context_get_SentConnectionEnd_m2270,
	Context_set_SentConnectionEnd_m2271,
	Context_get_SupportedCiphers_m2272,
	Context_set_SupportedCiphers_m2273,
	Context_get_HandshakeMessages_m2274,
	Context_get_WriteSequenceNumber_m2275,
	Context_set_WriteSequenceNumber_m2276,
	Context_get_ReadSequenceNumber_m2277,
	Context_set_ReadSequenceNumber_m2278,
	Context_get_ClientRandom_m2279,
	Context_set_ClientRandom_m2280,
	Context_get_ServerRandom_m2281,
	Context_set_ServerRandom_m2282,
	Context_get_RandomCS_m2283,
	Context_set_RandomCS_m2284,
	Context_get_RandomSC_m2285,
	Context_set_RandomSC_m2286,
	Context_get_MasterSecret_m2287,
	Context_set_MasterSecret_m2288,
	Context_get_ClientWriteKey_m2289,
	Context_set_ClientWriteKey_m2290,
	Context_get_ServerWriteKey_m2291,
	Context_set_ServerWriteKey_m2292,
	Context_get_ClientWriteIV_m2293,
	Context_set_ClientWriteIV_m2294,
	Context_get_ServerWriteIV_m2295,
	Context_set_ServerWriteIV_m2296,
	Context_get_RecordProtocol_m2297,
	Context_set_RecordProtocol_m2298,
	Context_GetUnixTime_m2299,
	Context_GetSecureRandomBytes_m2300,
	Context_Clear_m2301,
	Context_ClearKeyInfo_m2302,
	Context_DecodeProtocolCode_m2303,
	Context_ChangeProtocol_m2304,
	Context_get_Current_m2305,
	Context_get_Negotiating_m2306,
	Context_get_Read_m2307,
	Context_get_Write_m2308,
	Context_StartSwitchingSecurityParameters_m2309,
	Context_EndSwitchingSecurityParameters_m2310,
	HttpsClientStream__ctor_m2311,
	HttpsClientStream_get_TrustFailure_m2312,
	HttpsClientStream_RaiseServerCertificateValidation_m2313,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m2314,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m2315,
	ReceiveRecordAsyncResult__ctor_m2316,
	ReceiveRecordAsyncResult_get_Record_m2317,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m2318,
	ReceiveRecordAsyncResult_get_InitialBuffer_m2319,
	ReceiveRecordAsyncResult_get_AsyncState_m2320,
	ReceiveRecordAsyncResult_get_AsyncException_m2321,
	ReceiveRecordAsyncResult_get_CompletedWithError_m2322,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2323,
	ReceiveRecordAsyncResult_get_IsCompleted_m2324,
	ReceiveRecordAsyncResult_SetComplete_m2325,
	ReceiveRecordAsyncResult_SetComplete_m2326,
	ReceiveRecordAsyncResult_SetComplete_m2327,
	SendRecordAsyncResult__ctor_m2328,
	SendRecordAsyncResult_get_Message_m2329,
	SendRecordAsyncResult_get_AsyncState_m2330,
	SendRecordAsyncResult_get_AsyncException_m2331,
	SendRecordAsyncResult_get_CompletedWithError_m2332,
	SendRecordAsyncResult_get_AsyncWaitHandle_m2333,
	SendRecordAsyncResult_get_IsCompleted_m2334,
	SendRecordAsyncResult_SetComplete_m2335,
	SendRecordAsyncResult_SetComplete_m2336,
	RecordProtocol__ctor_m2337,
	RecordProtocol__cctor_m2338,
	RecordProtocol_get_Context_m2339,
	RecordProtocol_SendRecord_m2340,
	RecordProtocol_ProcessChangeCipherSpec_m2341,
	RecordProtocol_GetMessage_m2342,
	RecordProtocol_BeginReceiveRecord_m2343,
	RecordProtocol_InternalReceiveRecordCallback_m2344,
	RecordProtocol_EndReceiveRecord_m2345,
	RecordProtocol_ReceiveRecord_m2346,
	RecordProtocol_ReadRecordBuffer_m2347,
	RecordProtocol_ReadClientHelloV2_m2348,
	RecordProtocol_ReadStandardRecordBuffer_m2349,
	RecordProtocol_ProcessAlert_m2350,
	RecordProtocol_SendAlert_m2351,
	RecordProtocol_SendAlert_m2352,
	RecordProtocol_SendAlert_m2353,
	RecordProtocol_SendChangeCipherSpec_m2354,
	RecordProtocol_BeginSendRecord_m2355,
	RecordProtocol_InternalSendRecordCallback_m2356,
	RecordProtocol_BeginSendRecord_m2357,
	RecordProtocol_EndSendRecord_m2358,
	RecordProtocol_SendRecord_m2359,
	RecordProtocol_EncodeRecord_m2360,
	RecordProtocol_EncodeRecord_m2361,
	RecordProtocol_encryptRecordFragment_m2362,
	RecordProtocol_decryptRecordFragment_m2363,
	RecordProtocol_Compare_m2364,
	RecordProtocol_ProcessCipherSpecV2Buffer_m2365,
	RecordProtocol_MapV2CipherCode_m2366,
	RSASslSignatureDeformatter__ctor_m2367,
	RSASslSignatureDeformatter_VerifySignature_m2368,
	RSASslSignatureDeformatter_SetHashAlgorithm_m2369,
	RSASslSignatureDeformatter_SetKey_m2370,
	RSASslSignatureFormatter__ctor_m2371,
	RSASslSignatureFormatter_CreateSignature_m2372,
	RSASslSignatureFormatter_SetHashAlgorithm_m2373,
	RSASslSignatureFormatter_SetKey_m2374,
	SecurityParameters__ctor_m2375,
	SecurityParameters_get_Cipher_m2376,
	SecurityParameters_set_Cipher_m2377,
	SecurityParameters_get_ClientWriteMAC_m2378,
	SecurityParameters_set_ClientWriteMAC_m2379,
	SecurityParameters_get_ServerWriteMAC_m2380,
	SecurityParameters_set_ServerWriteMAC_m2381,
	SecurityParameters_Clear_m2382,
	ValidationResult_get_Trusted_m2383,
	ValidationResult_get_ErrorCode_m2384,
	SslClientStream__ctor_m2385,
	SslClientStream__ctor_m2386,
	SslClientStream__ctor_m2387,
	SslClientStream__ctor_m2388,
	SslClientStream__ctor_m2389,
	SslClientStream_add_ServerCertValidation_m2390,
	SslClientStream_remove_ServerCertValidation_m2391,
	SslClientStream_add_ClientCertSelection_m2392,
	SslClientStream_remove_ClientCertSelection_m2393,
	SslClientStream_add_PrivateKeySelection_m2394,
	SslClientStream_remove_PrivateKeySelection_m2395,
	SslClientStream_add_ServerCertValidation2_m2396,
	SslClientStream_remove_ServerCertValidation2_m2397,
	SslClientStream_get_InputBuffer_m2398,
	SslClientStream_get_ClientCertificates_m2399,
	SslClientStream_get_SelectedClientCertificate_m2400,
	SslClientStream_get_ServerCertValidationDelegate_m2401,
	SslClientStream_set_ServerCertValidationDelegate_m2402,
	SslClientStream_get_ClientCertSelectionDelegate_m2403,
	SslClientStream_set_ClientCertSelectionDelegate_m2404,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m2405,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m2406,
	SslClientStream_Finalize_m2407,
	SslClientStream_Dispose_m2408,
	SslClientStream_OnBeginNegotiateHandshake_m2409,
	SslClientStream_SafeReceiveRecord_m2410,
	SslClientStream_OnNegotiateHandshakeCallback_m2411,
	SslClientStream_OnLocalCertificateSelection_m2412,
	SslClientStream_get_HaveRemoteValidation2Callback_m2413,
	SslClientStream_OnRemoteCertificateValidation2_m2414,
	SslClientStream_OnRemoteCertificateValidation_m2415,
	SslClientStream_RaiseServerCertificateValidation_m2416,
	SslClientStream_RaiseServerCertificateValidation2_m2417,
	SslClientStream_RaiseClientCertificateSelection_m2418,
	SslClientStream_OnLocalPrivateKeySelection_m2419,
	SslClientStream_RaisePrivateKeySelection_m2420,
	SslCipherSuite__ctor_m2421,
	SslCipherSuite_ComputeServerRecordMAC_m2422,
	SslCipherSuite_ComputeClientRecordMAC_m2423,
	SslCipherSuite_ComputeMasterSecret_m2424,
	SslCipherSuite_ComputeKeys_m2425,
	SslCipherSuite_prf_m2426,
	SslHandshakeHash__ctor_m2427,
	SslHandshakeHash_Initialize_m2428,
	SslHandshakeHash_HashFinal_m2429,
	SslHandshakeHash_HashCore_m2430,
	SslHandshakeHash_CreateSignature_m2431,
	SslHandshakeHash_initializePad_m2432,
	InternalAsyncResult__ctor_m2433,
	InternalAsyncResult_get_ProceedAfterHandshake_m2434,
	InternalAsyncResult_get_FromWrite_m2435,
	InternalAsyncResult_get_Buffer_m2436,
	InternalAsyncResult_get_Offset_m2437,
	InternalAsyncResult_get_Count_m2438,
	InternalAsyncResult_get_BytesRead_m2439,
	InternalAsyncResult_get_AsyncState_m2440,
	InternalAsyncResult_get_AsyncException_m2441,
	InternalAsyncResult_get_CompletedWithError_m2442,
	InternalAsyncResult_get_AsyncWaitHandle_m2443,
	InternalAsyncResult_get_IsCompleted_m2444,
	InternalAsyncResult_SetComplete_m2445,
	InternalAsyncResult_SetComplete_m2446,
	InternalAsyncResult_SetComplete_m2447,
	InternalAsyncResult_SetComplete_m2448,
	SslStreamBase__ctor_m2449,
	SslStreamBase__cctor_m2450,
	SslStreamBase_AsyncHandshakeCallback_m2451,
	SslStreamBase_get_MightNeedHandshake_m2452,
	SslStreamBase_NegotiateHandshake_m2453,
	SslStreamBase_RaiseLocalCertificateSelection_m2454,
	SslStreamBase_RaiseRemoteCertificateValidation_m2455,
	SslStreamBase_RaiseRemoteCertificateValidation2_m2456,
	SslStreamBase_RaiseLocalPrivateKeySelection_m2457,
	SslStreamBase_get_CheckCertRevocationStatus_m2458,
	SslStreamBase_set_CheckCertRevocationStatus_m2459,
	SslStreamBase_get_CipherAlgorithm_m2460,
	SslStreamBase_get_CipherStrength_m2461,
	SslStreamBase_get_HashAlgorithm_m2462,
	SslStreamBase_get_HashStrength_m2463,
	SslStreamBase_get_KeyExchangeStrength_m2464,
	SslStreamBase_get_KeyExchangeAlgorithm_m2465,
	SslStreamBase_get_SecurityProtocol_m2466,
	SslStreamBase_get_ServerCertificate_m2467,
	SslStreamBase_get_ServerCertificates_m2468,
	SslStreamBase_BeginNegotiateHandshake_m2469,
	SslStreamBase_EndNegotiateHandshake_m2470,
	SslStreamBase_BeginRead_m2471,
	SslStreamBase_InternalBeginRead_m2472,
	SslStreamBase_InternalReadCallback_m2473,
	SslStreamBase_InternalBeginWrite_m2474,
	SslStreamBase_InternalWriteCallback_m2475,
	SslStreamBase_BeginWrite_m2476,
	SslStreamBase_EndRead_m2477,
	SslStreamBase_EndWrite_m2478,
	SslStreamBase_Close_m2479,
	SslStreamBase_Flush_m2480,
	SslStreamBase_Read_m2481,
	SslStreamBase_Read_m2482,
	SslStreamBase_Seek_m2483,
	SslStreamBase_SetLength_m2484,
	SslStreamBase_Write_m2485,
	SslStreamBase_Write_m2486,
	SslStreamBase_get_CanRead_m2487,
	SslStreamBase_get_CanSeek_m2488,
	SslStreamBase_get_CanWrite_m2489,
	SslStreamBase_get_Length_m2490,
	SslStreamBase_get_Position_m2491,
	SslStreamBase_set_Position_m2492,
	SslStreamBase_Finalize_m2493,
	SslStreamBase_Dispose_m2494,
	SslStreamBase_resetBuffer_m2495,
	SslStreamBase_checkDisposed_m2496,
	TlsCipherSuite__ctor_m2497,
	TlsCipherSuite_ComputeServerRecordMAC_m2498,
	TlsCipherSuite_ComputeClientRecordMAC_m2499,
	TlsCipherSuite_ComputeMasterSecret_m2500,
	TlsCipherSuite_ComputeKeys_m2501,
	TlsClientSettings__ctor_m2502,
	TlsClientSettings_get_TargetHost_m2503,
	TlsClientSettings_set_TargetHost_m2504,
	TlsClientSettings_get_Certificates_m2505,
	TlsClientSettings_set_Certificates_m2506,
	TlsClientSettings_get_ClientCertificate_m2507,
	TlsClientSettings_set_ClientCertificate_m2508,
	TlsClientSettings_UpdateCertificateRSA_m2509,
	TlsException__ctor_m2510,
	TlsException__ctor_m2511,
	TlsException__ctor_m2512,
	TlsException__ctor_m2513,
	TlsException__ctor_m2514,
	TlsException__ctor_m2515,
	TlsException_get_Alert_m2516,
	TlsServerSettings__ctor_m2517,
	TlsServerSettings_get_ServerKeyExchange_m2518,
	TlsServerSettings_set_ServerKeyExchange_m2519,
	TlsServerSettings_get_Certificates_m2520,
	TlsServerSettings_set_Certificates_m2521,
	TlsServerSettings_get_CertificateRSA_m2522,
	TlsServerSettings_get_RsaParameters_m2523,
	TlsServerSettings_set_RsaParameters_m2524,
	TlsServerSettings_set_SignedParams_m2525,
	TlsServerSettings_get_CertificateRequest_m2526,
	TlsServerSettings_set_CertificateRequest_m2527,
	TlsServerSettings_set_CertificateTypes_m2528,
	TlsServerSettings_set_DistinguisedNames_m2529,
	TlsServerSettings_UpdateCertificateRSA_m2530,
	TlsStream__ctor_m2531,
	TlsStream__ctor_m2532,
	TlsStream_get_EOF_m2533,
	TlsStream_get_CanWrite_m2534,
	TlsStream_get_CanRead_m2535,
	TlsStream_get_CanSeek_m2536,
	TlsStream_get_Position_m2537,
	TlsStream_set_Position_m2538,
	TlsStream_get_Length_m2539,
	TlsStream_ReadSmallValue_m2540,
	TlsStream_ReadByte_m2541,
	TlsStream_ReadInt16_m2542,
	TlsStream_ReadInt24_m2543,
	TlsStream_ReadBytes_m2544,
	TlsStream_Write_m2545,
	TlsStream_Write_m2546,
	TlsStream_WriteInt24_m2547,
	TlsStream_Write_m2548,
	TlsStream_Write_m2549,
	TlsStream_Reset_m2550,
	TlsStream_ToArray_m2551,
	TlsStream_Flush_m2552,
	TlsStream_SetLength_m2553,
	TlsStream_Seek_m2554,
	TlsStream_Read_m2555,
	TlsStream_Write_m2556,
	HandshakeMessage__ctor_m2557,
	HandshakeMessage__ctor_m2558,
	HandshakeMessage__ctor_m2559,
	HandshakeMessage_get_Context_m2560,
	HandshakeMessage_get_HandshakeType_m2561,
	HandshakeMessage_get_ContentType_m2562,
	HandshakeMessage_Process_m2563,
	HandshakeMessage_Update_m2564,
	HandshakeMessage_EncodeMessage_m2565,
	HandshakeMessage_Compare_m2566,
	TlsClientCertificate__ctor_m2567,
	TlsClientCertificate_get_ClientCertificate_m2568,
	TlsClientCertificate_Update_m2569,
	TlsClientCertificate_GetClientCertificate_m2570,
	TlsClientCertificate_SendCertificates_m2571,
	TlsClientCertificate_ProcessAsSsl3_m2572,
	TlsClientCertificate_ProcessAsTls1_m2573,
	TlsClientCertificate_FindParentCertificate_m2574,
	TlsClientCertificateVerify__ctor_m2575,
	TlsClientCertificateVerify_Update_m2576,
	TlsClientCertificateVerify_ProcessAsSsl3_m2577,
	TlsClientCertificateVerify_ProcessAsTls1_m2578,
	TlsClientCertificateVerify_getClientCertRSA_m2579,
	TlsClientCertificateVerify_getUnsignedBigInteger_m2580,
	TlsClientFinished__ctor_m2581,
	TlsClientFinished__cctor_m2582,
	TlsClientFinished_Update_m2583,
	TlsClientFinished_ProcessAsSsl3_m2584,
	TlsClientFinished_ProcessAsTls1_m2585,
	TlsClientHello__ctor_m2586,
	TlsClientHello_Update_m2587,
	TlsClientHello_ProcessAsSsl3_m2588,
	TlsClientHello_ProcessAsTls1_m2589,
	TlsClientKeyExchange__ctor_m2590,
	TlsClientKeyExchange_ProcessAsSsl3_m2591,
	TlsClientKeyExchange_ProcessAsTls1_m2592,
	TlsClientKeyExchange_ProcessCommon_m2593,
	TlsServerCertificate__ctor_m2594,
	TlsServerCertificate_Update_m2595,
	TlsServerCertificate_ProcessAsSsl3_m2596,
	TlsServerCertificate_ProcessAsTls1_m2597,
	TlsServerCertificate_checkCertificateUsage_m2598,
	TlsServerCertificate_validateCertificates_m2599,
	TlsServerCertificate_checkServerIdentity_m2600,
	TlsServerCertificate_checkDomainName_m2601,
	TlsServerCertificate_Match_m2602,
	TlsServerCertificateRequest__ctor_m2603,
	TlsServerCertificateRequest_Update_m2604,
	TlsServerCertificateRequest_ProcessAsSsl3_m2605,
	TlsServerCertificateRequest_ProcessAsTls1_m2606,
	TlsServerFinished__ctor_m2607,
	TlsServerFinished__cctor_m2608,
	TlsServerFinished_Update_m2609,
	TlsServerFinished_ProcessAsSsl3_m2610,
	TlsServerFinished_ProcessAsTls1_m2611,
	TlsServerHello__ctor_m2612,
	TlsServerHello_Update_m2613,
	TlsServerHello_ProcessAsSsl3_m2614,
	TlsServerHello_ProcessAsTls1_m2615,
	TlsServerHello_processProtocol_m2616,
	TlsServerHelloDone__ctor_m2617,
	TlsServerHelloDone_ProcessAsSsl3_m2618,
	TlsServerHelloDone_ProcessAsTls1_m2619,
	TlsServerKeyExchange__ctor_m2620,
	TlsServerKeyExchange_Update_m2621,
	TlsServerKeyExchange_ProcessAsSsl3_m2622,
	TlsServerKeyExchange_ProcessAsTls1_m2623,
	TlsServerKeyExchange_verifySignature_m2624,
	PrimalityTest__ctor_m2625,
	PrimalityTest_Invoke_m2626,
	PrimalityTest_BeginInvoke_m2627,
	PrimalityTest_EndInvoke_m2628,
	CertificateValidationCallback__ctor_m2629,
	CertificateValidationCallback_Invoke_m2630,
	CertificateValidationCallback_BeginInvoke_m2631,
	CertificateValidationCallback_EndInvoke_m2632,
	CertificateValidationCallback2__ctor_m2633,
	CertificateValidationCallback2_Invoke_m2634,
	CertificateValidationCallback2_BeginInvoke_m2635,
	CertificateValidationCallback2_EndInvoke_m2636,
	CertificateSelectionCallback__ctor_m2637,
	CertificateSelectionCallback_Invoke_m2638,
	CertificateSelectionCallback_BeginInvoke_m2639,
	CertificateSelectionCallback_EndInvoke_m2640,
	PrivateKeySelectionCallback__ctor_m2641,
	PrivateKeySelectionCallback_Invoke_m2642,
	PrivateKeySelectionCallback_BeginInvoke_m2643,
	PrivateKeySelectionCallback_EndInvoke_m2644,
	Object__ctor_m589,
	Object_Equals_m2723,
	Object_Equals_m1747,
	Object_Finalize_m588,
	Object_GetHashCode_m2724,
	Object_GetType_m625,
	Object_MemberwiseClone_m2725,
	Object_ToString_m661,
	Object_ReferenceEquals_m2726,
	Object_InternalGetHashCode_m2727,
	ValueType__ctor_m2728,
	ValueType_InternalEquals_m2729,
	ValueType_DefaultEquals_m2730,
	ValueType_Equals_m2731,
	ValueType_InternalGetHashCode_m2732,
	ValueType_GetHashCode_m2733,
	ValueType_ToString_m2734,
	Attribute__ctor_m613,
	Attribute_CheckParameters_m2735,
	Attribute_GetCustomAttribute_m2736,
	Attribute_GetCustomAttribute_m2737,
	Attribute_GetHashCode_m662,
	Attribute_IsDefined_m2738,
	Attribute_IsDefined_m2739,
	Attribute_IsDefined_m2740,
	Attribute_IsDefined_m2741,
	Attribute_Equals_m2742,
	Int32_System_IConvertible_ToBoolean_m2743,
	Int32_System_IConvertible_ToByte_m2744,
	Int32_System_IConvertible_ToChar_m2745,
	Int32_System_IConvertible_ToDateTime_m2746,
	Int32_System_IConvertible_ToDecimal_m2747,
	Int32_System_IConvertible_ToDouble_m2748,
	Int32_System_IConvertible_ToInt16_m2749,
	Int32_System_IConvertible_ToInt32_m2750,
	Int32_System_IConvertible_ToInt64_m2751,
	Int32_System_IConvertible_ToSByte_m2752,
	Int32_System_IConvertible_ToSingle_m2753,
	Int32_System_IConvertible_ToType_m2754,
	Int32_System_IConvertible_ToUInt16_m2755,
	Int32_System_IConvertible_ToUInt32_m2756,
	Int32_System_IConvertible_ToUInt64_m2757,
	Int32_CompareTo_m2758,
	Int32_Equals_m2759,
	Int32_GetHashCode_m605,
	Int32_CompareTo_m2760,
	Int32_Equals_m607,
	Int32_ProcessTrailingWhitespace_m2761,
	Int32_Parse_m2762,
	Int32_Parse_m2763,
	Int32_CheckStyle_m2764,
	Int32_JumpOverWhite_m2765,
	Int32_FindSign_m2766,
	Int32_FindCurrency_m2767,
	Int32_FindExponent_m2768,
	Int32_FindOther_m2769,
	Int32_ValidDigit_m2770,
	Int32_GetFormatException_m2771,
	Int32_Parse_m2772,
	Int32_Parse_m1719,
	Int32_Parse_m2773,
	Int32_TryParse_m2774,
	Int32_TryParse_m1599,
	Int32_ToString_m652,
	Int32_ToString_m2688,
	Int32_ToString_m1713,
	Int32_ToString_m2691,
	SerializableAttribute__ctor_m2775,
	AttributeUsageAttribute__ctor_m2776,
	AttributeUsageAttribute_get_AllowMultiple_m2777,
	AttributeUsageAttribute_set_AllowMultiple_m2778,
	AttributeUsageAttribute_get_Inherited_m2779,
	AttributeUsageAttribute_set_Inherited_m2780,
	ComVisibleAttribute__ctor_m2781,
	Int64_System_IConvertible_ToBoolean_m2782,
	Int64_System_IConvertible_ToByte_m2783,
	Int64_System_IConvertible_ToChar_m2784,
	Int64_System_IConvertible_ToDateTime_m2785,
	Int64_System_IConvertible_ToDecimal_m2786,
	Int64_System_IConvertible_ToDouble_m2787,
	Int64_System_IConvertible_ToInt16_m2788,
	Int64_System_IConvertible_ToInt32_m2789,
	Int64_System_IConvertible_ToInt64_m2790,
	Int64_System_IConvertible_ToSByte_m2791,
	Int64_System_IConvertible_ToSingle_m2792,
	Int64_System_IConvertible_ToType_m2793,
	Int64_System_IConvertible_ToUInt16_m2794,
	Int64_System_IConvertible_ToUInt32_m2795,
	Int64_System_IConvertible_ToUInt64_m2796,
	Int64_CompareTo_m2797,
	Int64_Equals_m2798,
	Int64_GetHashCode_m2799,
	Int64_CompareTo_m2800,
	Int64_Equals_m2801,
	Int64_Parse_m2802,
	Int64_Parse_m2803,
	Int64_Parse_m2804,
	Int64_Parse_m2805,
	Int64_Parse_m2806,
	Int64_TryParse_m2807,
	Int64_TryParse_m1595,
	Int64_ToString_m1596,
	Int64_ToString_m2808,
	Int64_ToString_m2809,
	Int64_ToString_m2810,
	UInt32_System_IConvertible_ToBoolean_m2811,
	UInt32_System_IConvertible_ToByte_m2812,
	UInt32_System_IConvertible_ToChar_m2813,
	UInt32_System_IConvertible_ToDateTime_m2814,
	UInt32_System_IConvertible_ToDecimal_m2815,
	UInt32_System_IConvertible_ToDouble_m2816,
	UInt32_System_IConvertible_ToInt16_m2817,
	UInt32_System_IConvertible_ToInt32_m2818,
	UInt32_System_IConvertible_ToInt64_m2819,
	UInt32_System_IConvertible_ToSByte_m2820,
	UInt32_System_IConvertible_ToSingle_m2821,
	UInt32_System_IConvertible_ToType_m2822,
	UInt32_System_IConvertible_ToUInt16_m2823,
	UInt32_System_IConvertible_ToUInt32_m2824,
	UInt32_System_IConvertible_ToUInt64_m2825,
	UInt32_CompareTo_m2826,
	UInt32_Equals_m2827,
	UInt32_GetHashCode_m2828,
	UInt32_CompareTo_m2829,
	UInt32_Equals_m2830,
	UInt32_Parse_m2831,
	UInt32_Parse_m2832,
	UInt32_Parse_m2833,
	UInt32_Parse_m2834,
	UInt32_TryParse_m1740,
	UInt32_TryParse_m2835,
	UInt32_ToString_m2836,
	UInt32_ToString_m2837,
	UInt32_ToString_m2838,
	UInt32_ToString_m2839,
	CLSCompliantAttribute__ctor_m2840,
	UInt64_System_IConvertible_ToBoolean_m2841,
	UInt64_System_IConvertible_ToByte_m2842,
	UInt64_System_IConvertible_ToChar_m2843,
	UInt64_System_IConvertible_ToDateTime_m2844,
	UInt64_System_IConvertible_ToDecimal_m2845,
	UInt64_System_IConvertible_ToDouble_m2846,
	UInt64_System_IConvertible_ToInt16_m2847,
	UInt64_System_IConvertible_ToInt32_m2848,
	UInt64_System_IConvertible_ToInt64_m2849,
	UInt64_System_IConvertible_ToSByte_m2850,
	UInt64_System_IConvertible_ToSingle_m2851,
	UInt64_System_IConvertible_ToType_m2852,
	UInt64_System_IConvertible_ToUInt16_m2853,
	UInt64_System_IConvertible_ToUInt32_m2854,
	UInt64_System_IConvertible_ToUInt64_m2855,
	UInt64_CompareTo_m2856,
	UInt64_Equals_m2857,
	UInt64_GetHashCode_m2858,
	UInt64_CompareTo_m2859,
	UInt64_Equals_m2860,
	UInt64_Parse_m2861,
	UInt64_Parse_m2862,
	UInt64_Parse_m2863,
	UInt64_TryParse_m2864,
	UInt64_ToString_m2865,
	UInt64_ToString_m2650,
	UInt64_ToString_m2866,
	UInt64_ToString_m2867,
	Byte_System_IConvertible_ToType_m2868,
	Byte_System_IConvertible_ToBoolean_m2869,
	Byte_System_IConvertible_ToByte_m2870,
	Byte_System_IConvertible_ToChar_m2871,
	Byte_System_IConvertible_ToDateTime_m2872,
	Byte_System_IConvertible_ToDecimal_m2873,
	Byte_System_IConvertible_ToDouble_m2874,
	Byte_System_IConvertible_ToInt16_m2875,
	Byte_System_IConvertible_ToInt32_m2876,
	Byte_System_IConvertible_ToInt64_m2877,
	Byte_System_IConvertible_ToSByte_m2878,
	Byte_System_IConvertible_ToSingle_m2879,
	Byte_System_IConvertible_ToUInt16_m2880,
	Byte_System_IConvertible_ToUInt32_m2881,
	Byte_System_IConvertible_ToUInt64_m2882,
	Byte_CompareTo_m2883,
	Byte_Equals_m2884,
	Byte_GetHashCode_m2885,
	Byte_CompareTo_m2886,
	Byte_Equals_m2887,
	Byte_Parse_m2888,
	Byte_Parse_m2889,
	Byte_Parse_m2890,
	Byte_TryParse_m2891,
	Byte_TryParse_m2892,
	Byte_ToString_m2689,
	Byte_ToString_m1652,
	Byte_ToString_m2649,
	Byte_ToString_m2654,
	SByte_System_IConvertible_ToBoolean_m2893,
	SByte_System_IConvertible_ToByte_m2894,
	SByte_System_IConvertible_ToChar_m2895,
	SByte_System_IConvertible_ToDateTime_m2896,
	SByte_System_IConvertible_ToDecimal_m2897,
	SByte_System_IConvertible_ToDouble_m2898,
	SByte_System_IConvertible_ToInt16_m2899,
	SByte_System_IConvertible_ToInt32_m2900,
	SByte_System_IConvertible_ToInt64_m2901,
	SByte_System_IConvertible_ToSByte_m2902,
	SByte_System_IConvertible_ToSingle_m2903,
	SByte_System_IConvertible_ToType_m2904,
	SByte_System_IConvertible_ToUInt16_m2905,
	SByte_System_IConvertible_ToUInt32_m2906,
	SByte_System_IConvertible_ToUInt64_m2907,
	SByte_CompareTo_m2908,
	SByte_Equals_m2909,
	SByte_GetHashCode_m2910,
	SByte_CompareTo_m2911,
	SByte_Equals_m2912,
	SByte_Parse_m2913,
	SByte_Parse_m2914,
	SByte_Parse_m2915,
	SByte_TryParse_m2916,
	SByte_ToString_m2917,
	SByte_ToString_m2918,
	SByte_ToString_m2919,
	SByte_ToString_m2920,
	Int16_System_IConvertible_ToBoolean_m2921,
	Int16_System_IConvertible_ToByte_m2922,
	Int16_System_IConvertible_ToChar_m2923,
	Int16_System_IConvertible_ToDateTime_m2924,
	Int16_System_IConvertible_ToDecimal_m2925,
	Int16_System_IConvertible_ToDouble_m2926,
	Int16_System_IConvertible_ToInt16_m2927,
	Int16_System_IConvertible_ToInt32_m2928,
	Int16_System_IConvertible_ToInt64_m2929,
	Int16_System_IConvertible_ToSByte_m2930,
	Int16_System_IConvertible_ToSingle_m2931,
	Int16_System_IConvertible_ToType_m2932,
	Int16_System_IConvertible_ToUInt16_m2933,
	Int16_System_IConvertible_ToUInt32_m2934,
	Int16_System_IConvertible_ToUInt64_m2935,
	Int16_CompareTo_m2936,
	Int16_Equals_m2937,
	Int16_GetHashCode_m2938,
	Int16_CompareTo_m2939,
	Int16_Equals_m2940,
	Int16_Parse_m2941,
	Int16_Parse_m2942,
	Int16_Parse_m2943,
	Int16_TryParse_m2944,
	Int16_ToString_m2945,
	Int16_ToString_m2946,
	Int16_ToString_m2947,
	Int16_ToString_m2948,
	UInt16_System_IConvertible_ToBoolean_m2949,
	UInt16_System_IConvertible_ToByte_m2950,
	UInt16_System_IConvertible_ToChar_m2951,
	UInt16_System_IConvertible_ToDateTime_m2952,
	UInt16_System_IConvertible_ToDecimal_m2953,
	UInt16_System_IConvertible_ToDouble_m2954,
	UInt16_System_IConvertible_ToInt16_m2955,
	UInt16_System_IConvertible_ToInt32_m2956,
	UInt16_System_IConvertible_ToInt64_m2957,
	UInt16_System_IConvertible_ToSByte_m2958,
	UInt16_System_IConvertible_ToSingle_m2959,
	UInt16_System_IConvertible_ToType_m2960,
	UInt16_System_IConvertible_ToUInt16_m2961,
	UInt16_System_IConvertible_ToUInt32_m2962,
	UInt16_System_IConvertible_ToUInt64_m2963,
	UInt16_CompareTo_m2964,
	UInt16_Equals_m2965,
	UInt16_GetHashCode_m2966,
	UInt16_CompareTo_m2967,
	UInt16_Equals_m2968,
	UInt16_Parse_m2969,
	UInt16_Parse_m2970,
	UInt16_TryParse_m2971,
	UInt16_TryParse_m2972,
	UInt16_ToString_m2973,
	UInt16_ToString_m2974,
	UInt16_ToString_m2975,
	UInt16_ToString_m2976,
	Char__cctor_m2977,
	Char_System_IConvertible_ToType_m2978,
	Char_System_IConvertible_ToBoolean_m2979,
	Char_System_IConvertible_ToByte_m2980,
	Char_System_IConvertible_ToChar_m2981,
	Char_System_IConvertible_ToDateTime_m2982,
	Char_System_IConvertible_ToDecimal_m2983,
	Char_System_IConvertible_ToDouble_m2984,
	Char_System_IConvertible_ToInt16_m2985,
	Char_System_IConvertible_ToInt32_m2986,
	Char_System_IConvertible_ToInt64_m2987,
	Char_System_IConvertible_ToSByte_m2988,
	Char_System_IConvertible_ToSingle_m2989,
	Char_System_IConvertible_ToUInt16_m2990,
	Char_System_IConvertible_ToUInt32_m2991,
	Char_System_IConvertible_ToUInt64_m2992,
	Char_GetDataTablePointers_m2993,
	Char_CompareTo_m2994,
	Char_Equals_m2995,
	Char_CompareTo_m2996,
	Char_Equals_m2997,
	Char_GetHashCode_m2998,
	Char_GetUnicodeCategory_m1726,
	Char_IsDigit_m1724,
	Char_IsLetter_m2999,
	Char_IsLetterOrDigit_m1723,
	Char_IsLower_m3000,
	Char_IsSurrogate_m3001,
	Char_IsWhiteSpace_m1725,
	Char_IsWhiteSpace_m1638,
	Char_CheckParameter_m3002,
	Char_Parse_m3003,
	Char_ToLower_m1727,
	Char_ToLowerInvariant_m3004,
	Char_ToLower_m3005,
	Char_ToUpper_m1732,
	Char_ToUpperInvariant_m1640,
	Char_ToString_m1734,
	Char_ToString_m3006,
	String__ctor_m3007,
	String__ctor_m3008,
	String__ctor_m3009,
	String__ctor_m3010,
	String__cctor_m3011,
	String_System_IConvertible_ToBoolean_m3012,
	String_System_IConvertible_ToByte_m3013,
	String_System_IConvertible_ToChar_m3014,
	String_System_IConvertible_ToDateTime_m3015,
	String_System_IConvertible_ToDecimal_m3016,
	String_System_IConvertible_ToDouble_m3017,
	String_System_IConvertible_ToInt16_m3018,
	String_System_IConvertible_ToInt32_m3019,
	String_System_IConvertible_ToInt64_m3020,
	String_System_IConvertible_ToSByte_m3021,
	String_System_IConvertible_ToSingle_m3022,
	String_System_IConvertible_ToType_m3023,
	String_System_IConvertible_ToUInt16_m3024,
	String_System_IConvertible_ToUInt32_m3025,
	String_System_IConvertible_ToUInt64_m3026,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m3027,
	String_System_Collections_IEnumerable_GetEnumerator_m3028,
	String_Equals_m3029,
	String_Equals_m3030,
	String_Equals_m1701,
	String_get_Chars_m642,
	String_Clone_m3031,
	String_CopyTo_m3032,
	String_ToCharArray_m1594,
	String_ToCharArray_m3033,
	String_Split_m641,
	String_Split_m3034,
	String_Split_m3035,
	String_Split_m3036,
	String_Split_m1641,
	String_Substring_m1593,
	String_Substring_m644,
	String_SubstringUnchecked_m3037,
	String_Trim_m637,
	String_Trim_m3038,
	String_TrimStart_m1742,
	String_TrimEnd_m1639,
	String_FindNotWhiteSpace_m3039,
	String_FindNotInTable_m3040,
	String_Compare_m3041,
	String_Compare_m3042,
	String_Compare_m1613,
	String_Compare_m2722,
	String_CompareTo_m3043,
	String_CompareTo_m3044,
	String_CompareOrdinal_m3045,
	String_CompareOrdinalUnchecked_m3046,
	String_CompareOrdinalCaseInsensitiveUnchecked_m3047,
	String_EndsWith_m645,
	String_IndexOfAny_m3048,
	String_IndexOfAny_m3049,
	String_IndexOfAny_m2673,
	String_IndexOfAnyUnchecked_m3050,
	String_IndexOf_m1676,
	String_IndexOf_m3051,
	String_IndexOfOrdinal_m3052,
	String_IndexOfOrdinalUnchecked_m3053,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m3054,
	String_IndexOf_m1592,
	String_IndexOf_m1743,
	String_IndexOf_m1744,
	String_IndexOfUnchecked_m3055,
	String_IndexOf_m643,
	String_IndexOf_m647,
	String_IndexOf_m3056,
	String_LastIndexOfAny_m3057,
	String_LastIndexOfAnyUnchecked_m3058,
	String_LastIndexOf_m1600,
	String_LastIndexOf_m3059,
	String_LastIndexOf_m1745,
	String_LastIndexOfUnchecked_m3060,
	String_LastIndexOf_m650,
	String_LastIndexOf_m3061,
	String_IsNullOrEmpty_m657,
	String_PadRight_m3062,
	String_StartsWith_m634,
	String_Replace_m649,
	String_Replace_m648,
	String_ReplaceUnchecked_m3063,
	String_ReplaceFallback_m3064,
	String_Remove_m646,
	String_ToLower_m1730,
	String_ToLower_m1741,
	String_ToLowerInvariant_m3065,
	String_ToString_m633,
	String_ToString_m3066,
	String_Format_m1647,
	String_Format_m3067,
	String_Format_m3068,
	String_Format_m614,
	String_Format_m2700,
	String_FormatHelper_m3069,
	String_Concat_m1587,
	String_Concat_m1577,
	String_Concat_m594,
	String_Concat_m635,
	String_Concat_m638,
	String_Concat_m628,
	String_Concat_m1597,
	String_ConcatInternal_m3070,
	String_Insert_m651,
	String_Join_m3071,
	String_Join_m3072,
	String_JoinUnchecked_m3073,
	String_get_Length_m586,
	String_ParseFormatSpecifier_m3074,
	String_ParseDecimal_m3075,
	String_InternalSetChar_m3076,
	String_InternalSetLength_m3077,
	String_GetHashCode_m1581,
	String_GetCaseInsensitiveHashCode_m3078,
	String_CreateString_m3079,
	String_CreateString_m3080,
	String_CreateString_m3081,
	String_CreateString_m3082,
	String_CreateString_m3083,
	String_CreateString_m3084,
	String_CreateString_m1733,
	String_CreateString_m3085,
	String_memcpy4_m3086,
	String_memcpy2_m3087,
	String_memcpy1_m3088,
	String_memcpy_m3089,
	String_CharCopy_m3090,
	String_CharCopyReverse_m3091,
	String_CharCopy_m3092,
	String_CharCopy_m3093,
	String_CharCopyReverse_m3094,
	String_InternalSplit_m3095,
	String_InternalAllocateStr_m3096,
	String_op_Equality_m600,
	String_op_Inequality_m1606,
	Single_System_IConvertible_ToBoolean_m3097,
	Single_System_IConvertible_ToByte_m3098,
	Single_System_IConvertible_ToChar_m3099,
	Single_System_IConvertible_ToDateTime_m3100,
	Single_System_IConvertible_ToDecimal_m3101,
	Single_System_IConvertible_ToDouble_m3102,
	Single_System_IConvertible_ToInt16_m3103,
	Single_System_IConvertible_ToInt32_m3104,
	Single_System_IConvertible_ToInt64_m3105,
	Single_System_IConvertible_ToSByte_m3106,
	Single_System_IConvertible_ToSingle_m3107,
	Single_System_IConvertible_ToType_m3108,
	Single_System_IConvertible_ToUInt16_m3109,
	Single_System_IConvertible_ToUInt32_m3110,
	Single_System_IConvertible_ToUInt64_m3111,
	Single_CompareTo_m3112,
	Single_Equals_m3113,
	Single_CompareTo_m3114,
	Single_Equals_m610,
	Single_GetHashCode_m606,
	Single_IsInfinity_m3115,
	Single_IsNaN_m3116,
	Single_IsNegativeInfinity_m3117,
	Single_IsPositiveInfinity_m3118,
	Single_Parse_m3119,
	Single_ToString_m3120,
	Single_ToString_m3121,
	Single_ToString_m611,
	Single_ToString_m3122,
	Double_System_IConvertible_ToType_m3123,
	Double_System_IConvertible_ToBoolean_m3124,
	Double_System_IConvertible_ToByte_m3125,
	Double_System_IConvertible_ToChar_m3126,
	Double_System_IConvertible_ToDateTime_m3127,
	Double_System_IConvertible_ToDecimal_m3128,
	Double_System_IConvertible_ToDouble_m3129,
	Double_System_IConvertible_ToInt16_m3130,
	Double_System_IConvertible_ToInt32_m3131,
	Double_System_IConvertible_ToInt64_m3132,
	Double_System_IConvertible_ToSByte_m3133,
	Double_System_IConvertible_ToSingle_m3134,
	Double_System_IConvertible_ToUInt16_m3135,
	Double_System_IConvertible_ToUInt32_m3136,
	Double_System_IConvertible_ToUInt64_m3137,
	Double_CompareTo_m3138,
	Double_Equals_m3139,
	Double_CompareTo_m3140,
	Double_Equals_m3141,
	Double_GetHashCode_m3142,
	Double_IsInfinity_m3143,
	Double_IsNaN_m3144,
	Double_IsNegativeInfinity_m3145,
	Double_IsPositiveInfinity_m3146,
	Double_Parse_m3147,
	Double_Parse_m3148,
	Double_Parse_m3149,
	Double_Parse_m3150,
	Double_TryParseStringConstant_m3151,
	Double_ParseImpl_m3152,
	Double_ToString_m3153,
	Double_ToString_m3154,
	Double_ToString_m3155,
	Decimal__ctor_m3156,
	Decimal__ctor_m3157,
	Decimal__ctor_m3158,
	Decimal__ctor_m3159,
	Decimal__ctor_m3160,
	Decimal__ctor_m3161,
	Decimal__ctor_m3162,
	Decimal__cctor_m3163,
	Decimal_System_IConvertible_ToType_m3164,
	Decimal_System_IConvertible_ToBoolean_m3165,
	Decimal_System_IConvertible_ToByte_m3166,
	Decimal_System_IConvertible_ToChar_m3167,
	Decimal_System_IConvertible_ToDateTime_m3168,
	Decimal_System_IConvertible_ToDecimal_m3169,
	Decimal_System_IConvertible_ToDouble_m3170,
	Decimal_System_IConvertible_ToInt16_m3171,
	Decimal_System_IConvertible_ToInt32_m3172,
	Decimal_System_IConvertible_ToInt64_m3173,
	Decimal_System_IConvertible_ToSByte_m3174,
	Decimal_System_IConvertible_ToSingle_m3175,
	Decimal_System_IConvertible_ToUInt16_m3176,
	Decimal_System_IConvertible_ToUInt32_m3177,
	Decimal_System_IConvertible_ToUInt64_m3178,
	Decimal_GetBits_m3179,
	Decimal_Add_m3180,
	Decimal_Subtract_m3181,
	Decimal_GetHashCode_m3182,
	Decimal_u64_m3183,
	Decimal_s64_m3184,
	Decimal_Equals_m3185,
	Decimal_Equals_m3186,
	Decimal_IsZero_m3187,
	Decimal_Floor_m3188,
	Decimal_Multiply_m3189,
	Decimal_Divide_m3190,
	Decimal_Compare_m3191,
	Decimal_CompareTo_m3192,
	Decimal_CompareTo_m3193,
	Decimal_Equals_m3194,
	Decimal_Parse_m3195,
	Decimal_ThrowAtPos_m3196,
	Decimal_ThrowInvalidExp_m3197,
	Decimal_stripStyles_m3198,
	Decimal_Parse_m3199,
	Decimal_PerformParse_m3200,
	Decimal_ToString_m3201,
	Decimal_ToString_m3202,
	Decimal_ToString_m3203,
	Decimal_decimal2UInt64_m3204,
	Decimal_decimal2Int64_m3205,
	Decimal_decimalIncr_m3206,
	Decimal_string2decimal_m3207,
	Decimal_decimalSetExponent_m3208,
	Decimal_decimal2double_m3209,
	Decimal_decimalFloorAndTrunc_m3210,
	Decimal_decimalMult_m3211,
	Decimal_decimalDiv_m3212,
	Decimal_decimalCompare_m3213,
	Decimal_op_Increment_m3214,
	Decimal_op_Subtraction_m3215,
	Decimal_op_Multiply_m3216,
	Decimal_op_Division_m3217,
	Decimal_op_Explicit_m3218,
	Decimal_op_Explicit_m3219,
	Decimal_op_Explicit_m3220,
	Decimal_op_Explicit_m3221,
	Decimal_op_Explicit_m3222,
	Decimal_op_Explicit_m3223,
	Decimal_op_Explicit_m3224,
	Decimal_op_Explicit_m3225,
	Decimal_op_Implicit_m3226,
	Decimal_op_Implicit_m3227,
	Decimal_op_Implicit_m3228,
	Decimal_op_Implicit_m3229,
	Decimal_op_Implicit_m3230,
	Decimal_op_Implicit_m3231,
	Decimal_op_Implicit_m3232,
	Decimal_op_Implicit_m3233,
	Decimal_op_Explicit_m3234,
	Decimal_op_Explicit_m3235,
	Decimal_op_Explicit_m3236,
	Decimal_op_Explicit_m3237,
	Decimal_op_Inequality_m3238,
	Decimal_op_Equality_m3239,
	Decimal_op_GreaterThan_m3240,
	Decimal_op_LessThan_m3241,
	Boolean__cctor_m3242,
	Boolean_System_IConvertible_ToType_m3243,
	Boolean_System_IConvertible_ToBoolean_m3244,
	Boolean_System_IConvertible_ToByte_m3245,
	Boolean_System_IConvertible_ToChar_m3246,
	Boolean_System_IConvertible_ToDateTime_m3247,
	Boolean_System_IConvertible_ToDecimal_m3248,
	Boolean_System_IConvertible_ToDouble_m3249,
	Boolean_System_IConvertible_ToInt16_m3250,
	Boolean_System_IConvertible_ToInt32_m3251,
	Boolean_System_IConvertible_ToInt64_m3252,
	Boolean_System_IConvertible_ToSByte_m3253,
	Boolean_System_IConvertible_ToSingle_m3254,
	Boolean_System_IConvertible_ToUInt16_m3255,
	Boolean_System_IConvertible_ToUInt32_m3256,
	Boolean_System_IConvertible_ToUInt64_m3257,
	Boolean_CompareTo_m3258,
	Boolean_Equals_m3259,
	Boolean_CompareTo_m3260,
	Boolean_Equals_m3261,
	Boolean_GetHashCode_m3262,
	Boolean_Parse_m3263,
	Boolean_ToString_m3264,
	Boolean_ToString_m3265,
	IntPtr__ctor_m615,
	IntPtr__ctor_m3266,
	IntPtr__ctor_m3267,
	IntPtr__ctor_m3268,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3269,
	IntPtr_get_Size_m3270,
	IntPtr_Equals_m3271,
	IntPtr_GetHashCode_m3272,
	IntPtr_ToInt64_m3273,
	IntPtr_ToPointer_m609,
	IntPtr_ToString_m3274,
	IntPtr_ToString_m3275,
	IntPtr_op_Equality_m3276,
	IntPtr_op_Inequality_m608,
	IntPtr_op_Explicit_m3277,
	IntPtr_op_Explicit_m3278,
	IntPtr_op_Explicit_m3279,
	IntPtr_op_Explicit_m3280,
	IntPtr_op_Explicit_m3281,
	UIntPtr__ctor_m3282,
	UIntPtr__ctor_m3283,
	UIntPtr__ctor_m3284,
	UIntPtr__cctor_m3285,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m3286,
	UIntPtr_Equals_m3287,
	UIntPtr_GetHashCode_m3288,
	UIntPtr_ToUInt32_m3289,
	UIntPtr_ToUInt64_m3290,
	UIntPtr_ToPointer_m3291,
	UIntPtr_ToString_m3292,
	UIntPtr_get_Size_m3293,
	UIntPtr_op_Equality_m3294,
	UIntPtr_op_Inequality_m3295,
	UIntPtr_op_Explicit_m3296,
	UIntPtr_op_Explicit_m3297,
	UIntPtr_op_Explicit_m3298,
	UIntPtr_op_Explicit_m3299,
	UIntPtr_op_Explicit_m3300,
	UIntPtr_op_Explicit_m3301,
	MulticastDelegate_GetObjectData_m3302,
	MulticastDelegate_Equals_m3303,
	MulticastDelegate_GetHashCode_m3304,
	MulticastDelegate_GetInvocationList_m3305,
	MulticastDelegate_CombineImpl_m3306,
	MulticastDelegate_BaseEquals_m3307,
	MulticastDelegate_KPM_m3308,
	MulticastDelegate_RemoveImpl_m3309,
	Delegate_get_Method_m3310,
	Delegate_get_Target_m3311,
	Delegate_CreateDelegate_internal_m3312,
	Delegate_SetMulticastInvoke_m3313,
	Delegate_arg_type_match_m3314,
	Delegate_return_type_match_m3315,
	Delegate_CreateDelegate_m3316,
	Delegate_CreateDelegate_m3317,
	Delegate_CreateDelegate_m3318,
	Delegate_CreateDelegate_m3319,
	Delegate_GetCandidateMethod_m3320,
	Delegate_CreateDelegate_m3321,
	Delegate_CreateDelegate_m3322,
	Delegate_CreateDelegate_m3323,
	Delegate_CreateDelegate_m3324,
	Delegate_Clone_m3325,
	Delegate_Equals_m3326,
	Delegate_GetHashCode_m3327,
	Delegate_GetObjectData_m3328,
	Delegate_GetInvocationList_m3329,
	Delegate_Combine_m616,
	Delegate_Combine_m3330,
	Delegate_CombineImpl_m3331,
	Delegate_Remove_m617,
	Delegate_RemoveImpl_m3332,
	Enum__ctor_m3333,
	Enum__cctor_m3334,
	Enum_System_IConvertible_ToBoolean_m3335,
	Enum_System_IConvertible_ToByte_m3336,
	Enum_System_IConvertible_ToChar_m3337,
	Enum_System_IConvertible_ToDateTime_m3338,
	Enum_System_IConvertible_ToDecimal_m3339,
	Enum_System_IConvertible_ToDouble_m3340,
	Enum_System_IConvertible_ToInt16_m3341,
	Enum_System_IConvertible_ToInt32_m3342,
	Enum_System_IConvertible_ToInt64_m3343,
	Enum_System_IConvertible_ToSByte_m3344,
	Enum_System_IConvertible_ToSingle_m3345,
	Enum_System_IConvertible_ToType_m3346,
	Enum_System_IConvertible_ToUInt16_m3347,
	Enum_System_IConvertible_ToUInt32_m3348,
	Enum_System_IConvertible_ToUInt64_m3349,
	Enum_GetTypeCode_m3350,
	Enum_get_value_m3351,
	Enum_get_Value_m3352,
	Enum_FindPosition_m3353,
	Enum_GetName_m3354,
	Enum_IsDefined_m2709,
	Enum_get_underlying_type_m3355,
	Enum_GetUnderlyingType_m3356,
	Enum_FindName_m3357,
	Enum_GetValue_m3358,
	Enum_Parse_m1722,
	Enum_compare_value_to_m3359,
	Enum_CompareTo_m3360,
	Enum_ToString_m3361,
	Enum_ToString_m3362,
	Enum_ToString_m3363,
	Enum_ToString_m3364,
	Enum_ToObject_m3365,
	Enum_ToObject_m3366,
	Enum_ToObject_m3367,
	Enum_ToObject_m3368,
	Enum_ToObject_m3369,
	Enum_ToObject_m3370,
	Enum_ToObject_m3371,
	Enum_ToObject_m3372,
	Enum_ToObject_m3373,
	Enum_Equals_m3374,
	Enum_get_hashcode_m3375,
	Enum_GetHashCode_m3376,
	Enum_FormatSpecifier_X_m3377,
	Enum_FormatFlags_m3378,
	Enum_Format_m3379,
	SimpleEnumerator__ctor_m3380,
	SimpleEnumerator_get_Current_m3381,
	SimpleEnumerator_MoveNext_m3382,
	SimpleEnumerator_Reset_m3383,
	SimpleEnumerator_Clone_m3384,
	Swapper__ctor_m3385,
	Swapper_Invoke_m3386,
	Swapper_BeginInvoke_m3387,
	Swapper_EndInvoke_m3388,
	Array__ctor_m3389,
	Array_System_Collections_IList_get_Item_m3390,
	Array_System_Collections_IList_set_Item_m3391,
	Array_System_Collections_IList_Add_m3392,
	Array_System_Collections_IList_Clear_m3393,
	Array_System_Collections_IList_Contains_m3394,
	Array_System_Collections_IList_IndexOf_m3395,
	Array_System_Collections_IList_Insert_m3396,
	Array_System_Collections_IList_Remove_m3397,
	Array_System_Collections_IList_RemoveAt_m3398,
	Array_System_Collections_ICollection_get_Count_m3399,
	Array_InternalArray__ICollection_get_Count_m3400,
	Array_InternalArray__ICollection_get_IsReadOnly_m3401,
	Array_InternalArray__ICollection_Clear_m3402,
	Array_InternalArray__RemoveAt_m3403,
	Array_get_Length_m1561,
	Array_get_LongLength_m3404,
	Array_get_Rank_m1567,
	Array_GetRank_m3405,
	Array_GetLength_m3406,
	Array_GetLongLength_m3407,
	Array_GetLowerBound_m3408,
	Array_GetValue_m3409,
	Array_SetValue_m3410,
	Array_GetValueImpl_m3411,
	Array_SetValueImpl_m3412,
	Array_FastCopy_m3413,
	Array_CreateInstanceImpl_m3414,
	Array_get_IsSynchronized_m3415,
	Array_get_SyncRoot_m3416,
	Array_get_IsFixedSize_m3417,
	Array_get_IsReadOnly_m3418,
	Array_GetEnumerator_m3419,
	Array_GetUpperBound_m3420,
	Array_GetValue_m3421,
	Array_GetValue_m3422,
	Array_GetValue_m3423,
	Array_GetValue_m3424,
	Array_GetValue_m3425,
	Array_GetValue_m3426,
	Array_SetValue_m3427,
	Array_SetValue_m3428,
	Array_SetValue_m3429,
	Array_SetValue_m1562,
	Array_SetValue_m3430,
	Array_SetValue_m3431,
	Array_CreateInstance_m3432,
	Array_CreateInstance_m3433,
	Array_CreateInstance_m3434,
	Array_CreateInstance_m3435,
	Array_CreateInstance_m3436,
	Array_GetIntArray_m3437,
	Array_CreateInstance_m3438,
	Array_GetValue_m3439,
	Array_SetValue_m3440,
	Array_BinarySearch_m3441,
	Array_BinarySearch_m3442,
	Array_BinarySearch_m3443,
	Array_BinarySearch_m3444,
	Array_DoBinarySearch_m3445,
	Array_Clear_m1799,
	Array_ClearInternal_m3446,
	Array_Clone_m3447,
	Array_Copy_m1735,
	Array_Copy_m3448,
	Array_Copy_m3449,
	Array_Copy_m3450,
	Array_IndexOf_m3451,
	Array_IndexOf_m3452,
	Array_IndexOf_m3453,
	Array_Initialize_m3454,
	Array_LastIndexOf_m3455,
	Array_LastIndexOf_m3456,
	Array_LastIndexOf_m3457,
	Array_get_swapper_m3458,
	Array_Reverse_m2648,
	Array_Reverse_m2674,
	Array_Sort_m3459,
	Array_Sort_m3460,
	Array_Sort_m3461,
	Array_Sort_m3462,
	Array_Sort_m3463,
	Array_Sort_m3464,
	Array_Sort_m3465,
	Array_Sort_m3466,
	Array_int_swapper_m3467,
	Array_obj_swapper_m3468,
	Array_slow_swapper_m3469,
	Array_double_swapper_m3470,
	Array_new_gap_m3471,
	Array_combsort_m3472,
	Array_combsort_m3473,
	Array_combsort_m3474,
	Array_qsort_m3475,
	Array_swap_m3476,
	Array_compare_m3477,
	Array_CopyTo_m3478,
	Array_CopyTo_m3479,
	Array_ConstrainedCopy_m3480,
	Type__ctor_m3481,
	Type__cctor_m3482,
	Type_FilterName_impl_m3483,
	Type_FilterNameIgnoreCase_impl_m3484,
	Type_FilterAttribute_impl_m3485,
	Type_get_Attributes_m3486,
	Type_get_DeclaringType_m3487,
	Type_get_HasElementType_m3488,
	Type_get_IsAbstract_m3489,
	Type_get_IsArray_m3490,
	Type_get_IsByRef_m3491,
	Type_get_IsClass_m3492,
	Type_get_IsContextful_m3493,
	Type_get_IsEnum_m3494,
	Type_get_IsExplicitLayout_m3495,
	Type_get_IsInterface_m3496,
	Type_get_IsMarshalByRef_m3497,
	Type_get_IsPointer_m3498,
	Type_get_IsPrimitive_m3499,
	Type_get_IsSealed_m3500,
	Type_get_IsSerializable_m3501,
	Type_get_IsValueType_m3502,
	Type_get_MemberType_m3503,
	Type_get_ReflectedType_m3504,
	Type_get_TypeHandle_m3505,
	Type_Equals_m3506,
	Type_Equals_m3507,
	Type_EqualsInternal_m3508,
	Type_internal_from_handle_m3509,
	Type_internal_from_name_m3510,
	Type_GetType_m3511,
	Type_GetType_m3512,
	Type_GetTypeCodeInternal_m3513,
	Type_GetTypeCode_m3514,
	Type_GetTypeFromHandle_m621,
	Type_GetTypeHandle_m3515,
	Type_type_is_subtype_of_m3516,
	Type_type_is_assignable_from_m3517,
	Type_IsSubclassOf_m3518,
	Type_IsAssignableFrom_m3519,
	Type_IsInstanceOfType_m3520,
	Type_GetHashCode_m3521,
	Type_GetMethod_m3522,
	Type_GetMethod_m3523,
	Type_GetMethod_m3524,
	Type_GetMethod_m3525,
	Type_GetProperty_m3526,
	Type_GetProperty_m3527,
	Type_GetProperty_m3528,
	Type_GetProperty_m3529,
	Type_IsArrayImpl_m3530,
	Type_IsValueTypeImpl_m3531,
	Type_IsContextfulImpl_m3532,
	Type_IsMarshalByRefImpl_m3533,
	Type_GetConstructor_m3534,
	Type_GetConstructor_m3535,
	Type_GetConstructor_m3536,
	Type_ToString_m3537,
	Type_get_IsSystemType_m3538,
	Type_GetGenericArguments_m3539,
	Type_get_ContainsGenericParameters_m3540,
	Type_get_IsGenericTypeDefinition_m3541,
	Type_GetGenericTypeDefinition_impl_m3542,
	Type_GetGenericTypeDefinition_m3543,
	Type_get_IsGenericType_m3544,
	Type_MakeGenericType_m3545,
	Type_MakeGenericType_m3546,
	Type_get_IsGenericParameter_m3547,
	Type_get_IsNested_m3548,
	Type_GetPseudoCustomAttributes_m3549,
	MemberInfo__ctor_m3550,
	MemberInfo_get_Module_m3551,
	Exception__ctor_m2645,
	Exception__ctor_m653,
	Exception__ctor_m656,
	Exception__ctor_m655,
	Exception_get_InnerException_m3552,
	Exception_set_HResult_m654,
	Exception_get_ClassName_m3553,
	Exception_get_Message_m3554,
	Exception_get_Source_m3555,
	Exception_get_StackTrace_m3556,
	Exception_GetObjectData_m1749,
	Exception_ToString_m3557,
	Exception_GetFullNameForStackTrace_m3558,
	Exception_GetType_m3559,
	RuntimeFieldHandle__ctor_m3560,
	RuntimeFieldHandle_get_Value_m3561,
	RuntimeFieldHandle_GetObjectData_m3562,
	RuntimeFieldHandle_Equals_m3563,
	RuntimeFieldHandle_GetHashCode_m3564,
	RuntimeTypeHandle__ctor_m3565,
	RuntimeTypeHandle_get_Value_m3566,
	RuntimeTypeHandle_GetObjectData_m3567,
	RuntimeTypeHandle_Equals_m3568,
	RuntimeTypeHandle_GetHashCode_m3569,
	ParamArrayAttribute__ctor_m3570,
	OutAttribute__ctor_m3571,
	ObsoleteAttribute__ctor_m3572,
	ObsoleteAttribute__ctor_m3573,
	ObsoleteAttribute__ctor_m3574,
	DllImportAttribute__ctor_m3575,
	DllImportAttribute_get_Value_m3576,
	MarshalAsAttribute__ctor_m3577,
	InAttribute__ctor_m3578,
	GuidAttribute__ctor_m3579,
	ComImportAttribute__ctor_m3580,
	OptionalAttribute__ctor_m3581,
	CompilerGeneratedAttribute__ctor_m3582,
	InternalsVisibleToAttribute__ctor_m3583,
	RuntimeCompatibilityAttribute__ctor_m3584,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m3585,
	DebuggerHiddenAttribute__ctor_m3586,
	DefaultMemberAttribute__ctor_m3587,
	DefaultMemberAttribute_get_MemberName_m3588,
	DecimalConstantAttribute__ctor_m3589,
	FieldOffsetAttribute__ctor_m3590,
	AsyncCallback__ctor_m2708,
	AsyncCallback_Invoke_m3591,
	AsyncCallback_BeginInvoke_m2706,
	AsyncCallback_EndInvoke_m3592,
	TypedReference_Equals_m3593,
	TypedReference_GetHashCode_m3594,
	ArgIterator_Equals_m3595,
	ArgIterator_GetHashCode_m3596,
	MarshalByRefObject__ctor_m1614,
	MarshalByRefObject_get_ObjectIdentity_m3597,
	RuntimeHelpers_InitializeArray_m3598,
	RuntimeHelpers_InitializeArray_m1608,
	RuntimeHelpers_get_OffsetToStringData_m3599,
	Locale_GetText_m3600,
	Locale_GetText_m3601,
	MonoTODOAttribute__ctor_m3602,
	MonoTODOAttribute__ctor_m3603,
	MonoDocumentationNoteAttribute__ctor_m3604,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m3605,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m3606,
	SafeWaitHandle__ctor_m3607,
	SafeWaitHandle_ReleaseHandle_m3608,
	TableRange__ctor_m3609,
	CodePointIndexer__ctor_m3610,
	CodePointIndexer_ToIndex_m3611,
	TailoringInfo__ctor_m3612,
	Contraction__ctor_m3613,
	ContractionComparer__ctor_m3614,
	ContractionComparer__cctor_m3615,
	ContractionComparer_Compare_m3616,
	Level2Map__ctor_m3617,
	Level2MapComparer__ctor_m3618,
	Level2MapComparer__cctor_m3619,
	Level2MapComparer_Compare_m3620,
	MSCompatUnicodeTable__cctor_m3621,
	MSCompatUnicodeTable_GetTailoringInfo_m3622,
	MSCompatUnicodeTable_BuildTailoringTables_m3623,
	MSCompatUnicodeTable_SetCJKReferences_m3624,
	MSCompatUnicodeTable_Category_m3625,
	MSCompatUnicodeTable_Level1_m3626,
	MSCompatUnicodeTable_Level2_m3627,
	MSCompatUnicodeTable_Level3_m3628,
	MSCompatUnicodeTable_IsIgnorable_m3629,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m3630,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m3631,
	MSCompatUnicodeTable_ToWidthCompat_m3632,
	MSCompatUnicodeTable_HasSpecialWeight_m3633,
	MSCompatUnicodeTable_IsHalfWidthKana_m3634,
	MSCompatUnicodeTable_IsHiragana_m3635,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m3636,
	MSCompatUnicodeTable_get_IsReady_m3637,
	MSCompatUnicodeTable_GetResource_m3638,
	MSCompatUnicodeTable_UInt32FromBytePtr_m3639,
	MSCompatUnicodeTable_FillCJK_m3640,
	MSCompatUnicodeTable_FillCJKCore_m3641,
	MSCompatUnicodeTableUtil__cctor_m3642,
	Context__ctor_m3643,
	PreviousInfo__ctor_m3644,
	SimpleCollator__ctor_m3645,
	SimpleCollator__cctor_m3646,
	SimpleCollator_SetCJKTable_m3647,
	SimpleCollator_GetNeutralCulture_m3648,
	SimpleCollator_Category_m3649,
	SimpleCollator_Level1_m3650,
	SimpleCollator_Level2_m3651,
	SimpleCollator_IsHalfKana_m3652,
	SimpleCollator_GetContraction_m3653,
	SimpleCollator_GetContraction_m3654,
	SimpleCollator_GetTailContraction_m3655,
	SimpleCollator_GetTailContraction_m3656,
	SimpleCollator_FilterOptions_m3657,
	SimpleCollator_GetExtenderType_m3658,
	SimpleCollator_ToDashTypeValue_m3659,
	SimpleCollator_FilterExtender_m3660,
	SimpleCollator_IsIgnorable_m3661,
	SimpleCollator_IsSafe_m3662,
	SimpleCollator_GetSortKey_m3663,
	SimpleCollator_GetSortKey_m3664,
	SimpleCollator_GetSortKey_m3665,
	SimpleCollator_FillSortKeyRaw_m3666,
	SimpleCollator_FillSurrogateSortKeyRaw_m3667,
	SimpleCollator_CompareOrdinal_m3668,
	SimpleCollator_CompareQuick_m3669,
	SimpleCollator_CompareOrdinalIgnoreCase_m3670,
	SimpleCollator_Compare_m3671,
	SimpleCollator_ClearBuffer_m3672,
	SimpleCollator_QuickCheckPossible_m3673,
	SimpleCollator_CompareInternal_m3674,
	SimpleCollator_CompareFlagPair_m3675,
	SimpleCollator_IsPrefix_m3676,
	SimpleCollator_IsPrefix_m3677,
	SimpleCollator_IsPrefix_m3678,
	SimpleCollator_IsSuffix_m3679,
	SimpleCollator_IsSuffix_m3680,
	SimpleCollator_QuickIndexOf_m3681,
	SimpleCollator_IndexOf_m3682,
	SimpleCollator_IndexOfOrdinal_m3683,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m3684,
	SimpleCollator_IndexOfSortKey_m3685,
	SimpleCollator_IndexOf_m3686,
	SimpleCollator_LastIndexOf_m3687,
	SimpleCollator_LastIndexOfOrdinal_m3688,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m3689,
	SimpleCollator_LastIndexOfSortKey_m3690,
	SimpleCollator_LastIndexOf_m3691,
	SimpleCollator_MatchesForward_m3692,
	SimpleCollator_MatchesForwardCore_m3693,
	SimpleCollator_MatchesPrimitive_m3694,
	SimpleCollator_MatchesBackward_m3695,
	SimpleCollator_MatchesBackwardCore_m3696,
	SortKey__ctor_m3697,
	SortKey__ctor_m3698,
	SortKey_Compare_m3699,
	SortKey_get_OriginalString_m3700,
	SortKey_get_KeyData_m3701,
	SortKey_Equals_m3702,
	SortKey_GetHashCode_m3703,
	SortKey_ToString_m3704,
	SortKeyBuffer__ctor_m3705,
	SortKeyBuffer_Reset_m3706,
	SortKeyBuffer_Initialize_m3707,
	SortKeyBuffer_AppendCJKExtension_m3708,
	SortKeyBuffer_AppendKana_m3709,
	SortKeyBuffer_AppendNormal_m3710,
	SortKeyBuffer_AppendLevel5_m3711,
	SortKeyBuffer_AppendBufferPrimitive_m3712,
	SortKeyBuffer_GetResultAndReset_m3713,
	SortKeyBuffer_GetOptimizedLength_m3714,
	SortKeyBuffer_GetResult_m3715,
	PrimeGeneratorBase__ctor_m3716,
	PrimeGeneratorBase_get_Confidence_m3717,
	PrimeGeneratorBase_get_PrimalityTest_m3718,
	PrimeGeneratorBase_get_TrialDivisionBounds_m3719,
	SequentialSearchPrimeGeneratorBase__ctor_m3720,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3721,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3722,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3723,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3724,
	PrimalityTests_GetSPPRounds_m3725,
	PrimalityTests_Test_m3726,
	PrimalityTests_RabinMillerTest_m3727,
	PrimalityTests_SmallPrimeSppTest_m3728,
	ModulusRing__ctor_m3729,
	ModulusRing_BarrettReduction_m3730,
	ModulusRing_Multiply_m3731,
	ModulusRing_Difference_m3732,
	ModulusRing_Pow_m3733,
	ModulusRing_Pow_m3734,
	Kernel_AddSameSign_m3735,
	Kernel_Subtract_m3736,
	Kernel_MinusEq_m3737,
	Kernel_PlusEq_m3738,
	Kernel_Compare_m3739,
	Kernel_SingleByteDivideInPlace_m3740,
	Kernel_DwordMod_m3741,
	Kernel_DwordDivMod_m3742,
	Kernel_multiByteDivide_m3743,
	Kernel_LeftShift_m3744,
	Kernel_RightShift_m3745,
	Kernel_MultiplyByDword_m3746,
	Kernel_Multiply_m3747,
	Kernel_MultiplyMod2p32pmod_m3748,
	Kernel_modInverse_m3749,
	Kernel_modInverse_m3750,
	BigInteger__ctor_m3751,
	BigInteger__ctor_m3752,
	BigInteger__ctor_m3753,
	BigInteger__ctor_m3754,
	BigInteger__ctor_m3755,
	BigInteger__cctor_m3756,
	BigInteger_get_Rng_m3757,
	BigInteger_GenerateRandom_m3758,
	BigInteger_GenerateRandom_m3759,
	BigInteger_Randomize_m3760,
	BigInteger_Randomize_m3761,
	BigInteger_BitCount_m3762,
	BigInteger_TestBit_m3763,
	BigInteger_TestBit_m3764,
	BigInteger_SetBit_m3765,
	BigInteger_SetBit_m3766,
	BigInteger_LowestSetBit_m3767,
	BigInteger_GetBytes_m3768,
	BigInteger_ToString_m3769,
	BigInteger_ToString_m3770,
	BigInteger_Normalize_m3771,
	BigInteger_Clear_m3772,
	BigInteger_GetHashCode_m3773,
	BigInteger_ToString_m3774,
	BigInteger_Equals_m3775,
	BigInteger_ModInverse_m3776,
	BigInteger_ModPow_m3777,
	BigInteger_IsProbablePrime_m3778,
	BigInteger_GeneratePseudoPrime_m3779,
	BigInteger_Incr2_m3780,
	BigInteger_op_Implicit_m3781,
	BigInteger_op_Implicit_m3782,
	BigInteger_op_Addition_m3783,
	BigInteger_op_Subtraction_m3784,
	BigInteger_op_Modulus_m3785,
	BigInteger_op_Modulus_m3786,
	BigInteger_op_Division_m3787,
	BigInteger_op_Multiply_m3788,
	BigInteger_op_Multiply_m3789,
	BigInteger_op_LeftShift_m3790,
	BigInteger_op_RightShift_m3791,
	BigInteger_op_Equality_m3792,
	BigInteger_op_Inequality_m3793,
	BigInteger_op_Equality_m3794,
	BigInteger_op_Inequality_m3795,
	BigInteger_op_GreaterThan_m3796,
	BigInteger_op_LessThan_m3797,
	BigInteger_op_GreaterThanOrEqual_m3798,
	BigInteger_op_LessThanOrEqual_m3799,
	CryptoConvert_ToInt32LE_m3800,
	CryptoConvert_ToUInt32LE_m3801,
	CryptoConvert_GetBytesLE_m3802,
	CryptoConvert_ToCapiPrivateKeyBlob_m3803,
	CryptoConvert_FromCapiPublicKeyBlob_m3804,
	CryptoConvert_FromCapiPublicKeyBlob_m3805,
	CryptoConvert_ToCapiPublicKeyBlob_m3806,
	CryptoConvert_ToCapiKeyBlob_m3807,
	KeyBuilder_get_Rng_m3808,
	KeyBuilder_Key_m3809,
	KeyBuilder_IV_m3810,
	BlockProcessor__ctor_m3811,
	BlockProcessor_Finalize_m3812,
	BlockProcessor_Initialize_m3813,
	BlockProcessor_Core_m3814,
	BlockProcessor_Core_m3815,
	BlockProcessor_Final_m3816,
	KeyGeneratedEventHandler__ctor_m3817,
	KeyGeneratedEventHandler_Invoke_m3818,
	KeyGeneratedEventHandler_BeginInvoke_m3819,
	KeyGeneratedEventHandler_EndInvoke_m3820,
	DSAManaged__ctor_m3821,
	DSAManaged_add_KeyGenerated_m3822,
	DSAManaged_remove_KeyGenerated_m3823,
	DSAManaged_Finalize_m3824,
	DSAManaged_Generate_m3825,
	DSAManaged_GenerateKeyPair_m3826,
	DSAManaged_add_m3827,
	DSAManaged_GenerateParams_m3828,
	DSAManaged_get_Random_m3829,
	DSAManaged_get_KeySize_m3830,
	DSAManaged_get_PublicOnly_m3831,
	DSAManaged_NormalizeArray_m3832,
	DSAManaged_ExportParameters_m3833,
	DSAManaged_ImportParameters_m3834,
	DSAManaged_CreateSignature_m3835,
	DSAManaged_VerifySignature_m3836,
	DSAManaged_Dispose_m3837,
	KeyPairPersistence__ctor_m3838,
	KeyPairPersistence__ctor_m3839,
	KeyPairPersistence__cctor_m3840,
	KeyPairPersistence_get_Filename_m3841,
	KeyPairPersistence_get_KeyValue_m3842,
	KeyPairPersistence_set_KeyValue_m3843,
	KeyPairPersistence_Load_m3844,
	KeyPairPersistence_Save_m3845,
	KeyPairPersistence_Remove_m3846,
	KeyPairPersistence_get_UserPath_m3847,
	KeyPairPersistence_get_MachinePath_m3848,
	KeyPairPersistence__CanSecure_m3849,
	KeyPairPersistence__ProtectUser_m3850,
	KeyPairPersistence__ProtectMachine_m3851,
	KeyPairPersistence__IsUserProtected_m3852,
	KeyPairPersistence__IsMachineProtected_m3853,
	KeyPairPersistence_CanSecure_m3854,
	KeyPairPersistence_ProtectUser_m3855,
	KeyPairPersistence_ProtectMachine_m3856,
	KeyPairPersistence_IsUserProtected_m3857,
	KeyPairPersistence_IsMachineProtected_m3858,
	KeyPairPersistence_get_CanChange_m3859,
	KeyPairPersistence_get_UseDefaultKeyContainer_m3860,
	KeyPairPersistence_get_UseMachineKeyStore_m3861,
	KeyPairPersistence_get_ContainerName_m3862,
	KeyPairPersistence_Copy_m3863,
	KeyPairPersistence_FromXml_m3864,
	KeyPairPersistence_ToXml_m3865,
	MACAlgorithm__ctor_m3866,
	MACAlgorithm_Initialize_m3867,
	MACAlgorithm_Core_m3868,
	MACAlgorithm_Final_m3869,
	PKCS1__cctor_m3870,
	PKCS1_Compare_m3871,
	PKCS1_I2OSP_m3872,
	PKCS1_OS2IP_m3873,
	PKCS1_RSAEP_m3874,
	PKCS1_RSASP1_m3875,
	PKCS1_RSAVP1_m3876,
	PKCS1_Encrypt_v15_m3877,
	PKCS1_Sign_v15_m3878,
	PKCS1_Verify_v15_m3879,
	PKCS1_Verify_v15_m3880,
	PKCS1_Encode_v15_m3881,
	PrivateKeyInfo__ctor_m3882,
	PrivateKeyInfo__ctor_m3883,
	PrivateKeyInfo_get_PrivateKey_m3884,
	PrivateKeyInfo_Decode_m3885,
	PrivateKeyInfo_RemoveLeadingZero_m3886,
	PrivateKeyInfo_Normalize_m3887,
	PrivateKeyInfo_DecodeRSA_m3888,
	PrivateKeyInfo_DecodeDSA_m3889,
	EncryptedPrivateKeyInfo__ctor_m3890,
	EncryptedPrivateKeyInfo__ctor_m3891,
	EncryptedPrivateKeyInfo_get_Algorithm_m3892,
	EncryptedPrivateKeyInfo_get_EncryptedData_m3893,
	EncryptedPrivateKeyInfo_get_Salt_m3894,
	EncryptedPrivateKeyInfo_get_IterationCount_m3895,
	EncryptedPrivateKeyInfo_Decode_m3896,
	KeyGeneratedEventHandler__ctor_m3897,
	KeyGeneratedEventHandler_Invoke_m3898,
	KeyGeneratedEventHandler_BeginInvoke_m3899,
	KeyGeneratedEventHandler_EndInvoke_m3900,
	RSAManaged__ctor_m3901,
	RSAManaged_add_KeyGenerated_m3902,
	RSAManaged_remove_KeyGenerated_m3903,
	RSAManaged_Finalize_m3904,
	RSAManaged_GenerateKeyPair_m3905,
	RSAManaged_get_KeySize_m3906,
	RSAManaged_get_PublicOnly_m3907,
	RSAManaged_DecryptValue_m3908,
	RSAManaged_EncryptValue_m3909,
	RSAManaged_ExportParameters_m3910,
	RSAManaged_ImportParameters_m3911,
	RSAManaged_Dispose_m3912,
	RSAManaged_ToXmlString_m3913,
	RSAManaged_get_IsCrtPossible_m3914,
	RSAManaged_GetPaddedValue_m3915,
	SymmetricTransform__ctor_m3916,
	SymmetricTransform_System_IDisposable_Dispose_m3917,
	SymmetricTransform_Finalize_m3918,
	SymmetricTransform_Dispose_m3919,
	SymmetricTransform_get_CanReuseTransform_m3920,
	SymmetricTransform_Transform_m3921,
	SymmetricTransform_CBC_m3922,
	SymmetricTransform_CFB_m3923,
	SymmetricTransform_OFB_m3924,
	SymmetricTransform_CTS_m3925,
	SymmetricTransform_CheckInput_m3926,
	SymmetricTransform_TransformBlock_m3927,
	SymmetricTransform_get_KeepLastBlock_m3928,
	SymmetricTransform_InternalTransformBlock_m3929,
	SymmetricTransform_Random_m3930,
	SymmetricTransform_ThrowBadPaddingException_m3931,
	SymmetricTransform_FinalEncrypt_m3932,
	SymmetricTransform_FinalDecrypt_m3933,
	SymmetricTransform_TransformFinalBlock_m3934,
	SafeBag__ctor_m3935,
	SafeBag_get_BagOID_m3936,
	SafeBag_get_ASN1_m3937,
	DeriveBytes__ctor_m3938,
	DeriveBytes__cctor_m3939,
	DeriveBytes_set_HashName_m3940,
	DeriveBytes_set_IterationCount_m3941,
	DeriveBytes_set_Password_m3942,
	DeriveBytes_set_Salt_m3943,
	DeriveBytes_Adjust_m3944,
	DeriveBytes_Derive_m3945,
	DeriveBytes_DeriveKey_m3946,
	DeriveBytes_DeriveIV_m3947,
	DeriveBytes_DeriveMAC_m3948,
	PKCS12__ctor_m3949,
	PKCS12__ctor_m3950,
	PKCS12__ctor_m3951,
	PKCS12__cctor_m3952,
	PKCS12_Decode_m3953,
	PKCS12_Finalize_m3954,
	PKCS12_set_Password_m3955,
	PKCS12_get_IterationCount_m3956,
	PKCS12_set_IterationCount_m3957,
	PKCS12_get_Certificates_m3958,
	PKCS12_get_RNG_m3959,
	PKCS12_Compare_m3960,
	PKCS12_GetSymmetricAlgorithm_m3961,
	PKCS12_Decrypt_m3962,
	PKCS12_Decrypt_m3963,
	PKCS12_Encrypt_m3964,
	PKCS12_GetExistingParameters_m3965,
	PKCS12_AddPrivateKey_m3966,
	PKCS12_ReadSafeBag_m3967,
	PKCS12_CertificateSafeBag_m3968,
	PKCS12_MAC_m3969,
	PKCS12_GetBytes_m3970,
	PKCS12_EncryptedContentInfo_m3971,
	PKCS12_AddCertificate_m3972,
	PKCS12_AddCertificate_m3973,
	PKCS12_RemoveCertificate_m3974,
	PKCS12_RemoveCertificate_m3975,
	PKCS12_Clone_m3976,
	PKCS12_get_MaximumPasswordLength_m3977,
	X501__cctor_m3978,
	X501_ToString_m3979,
	X501_ToString_m3980,
	X501_AppendEntry_m3981,
	X509Certificate__ctor_m3982,
	X509Certificate__cctor_m3983,
	X509Certificate_Parse_m3984,
	X509Certificate_GetUnsignedBigInteger_m3985,
	X509Certificate_get_DSA_m3986,
	X509Certificate_get_IssuerName_m3987,
	X509Certificate_get_KeyAlgorithmParameters_m3988,
	X509Certificate_get_PublicKey_m3989,
	X509Certificate_get_RawData_m3990,
	X509Certificate_get_SubjectName_m3991,
	X509Certificate_get_ValidFrom_m3992,
	X509Certificate_get_ValidUntil_m3993,
	X509Certificate_GetIssuerName_m3994,
	X509Certificate_GetSubjectName_m3995,
	X509Certificate_GetObjectData_m3996,
	X509Certificate_PEM_m3997,
	X509CertificateEnumerator__ctor_m3998,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3999,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4000,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4001,
	X509CertificateEnumerator_get_Current_m4002,
	X509CertificateEnumerator_MoveNext_m4003,
	X509CertificateEnumerator_Reset_m4004,
	X509CertificateCollection__ctor_m4005,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4006,
	X509CertificateCollection_get_Item_m4007,
	X509CertificateCollection_Add_m4008,
	X509CertificateCollection_GetEnumerator_m4009,
	X509CertificateCollection_GetHashCode_m4010,
	X509Extension__ctor_m4011,
	X509Extension_Decode_m4012,
	X509Extension_Equals_m4013,
	X509Extension_GetHashCode_m4014,
	X509Extension_WriteLine_m4015,
	X509Extension_ToString_m4016,
	X509ExtensionCollection__ctor_m4017,
	X509ExtensionCollection__ctor_m4018,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4019,
	ASN1__ctor_m4020,
	ASN1__ctor_m4021,
	ASN1__ctor_m4022,
	ASN1_get_Count_m4023,
	ASN1_get_Tag_m4024,
	ASN1_get_Length_m4025,
	ASN1_get_Value_m4026,
	ASN1_set_Value_m4027,
	ASN1_CompareArray_m4028,
	ASN1_CompareValue_m4029,
	ASN1_Add_m4030,
	ASN1_GetBytes_m4031,
	ASN1_Decode_m4032,
	ASN1_DecodeTLV_m4033,
	ASN1_get_Item_m4034,
	ASN1_Element_m4035,
	ASN1_ToString_m4036,
	ASN1Convert_FromInt32_m4037,
	ASN1Convert_FromOid_m4038,
	ASN1Convert_ToInt32_m4039,
	ASN1Convert_ToOid_m4040,
	ASN1Convert_ToDateTime_m4041,
	BitConverterLE_GetUIntBytes_m4042,
	BitConverterLE_GetBytes_m4043,
	BitConverterLE_UShortFromBytes_m4044,
	BitConverterLE_UIntFromBytes_m4045,
	BitConverterLE_ULongFromBytes_m4046,
	BitConverterLE_ToInt16_m4047,
	BitConverterLE_ToInt32_m4048,
	BitConverterLE_ToSingle_m4049,
	BitConverterLE_ToDouble_m4050,
	ContentInfo__ctor_m4051,
	ContentInfo__ctor_m4052,
	ContentInfo__ctor_m4053,
	ContentInfo__ctor_m4054,
	ContentInfo_get_ASN1_m4055,
	ContentInfo_get_Content_m4056,
	ContentInfo_set_Content_m4057,
	ContentInfo_get_ContentType_m4058,
	ContentInfo_set_ContentType_m4059,
	ContentInfo_GetASN1_m4060,
	EncryptedData__ctor_m4061,
	EncryptedData__ctor_m4062,
	EncryptedData_get_EncryptionAlgorithm_m4063,
	EncryptedData_get_EncryptedContent_m4064,
	StrongName__cctor_m4065,
	StrongName_get_PublicKey_m4066,
	StrongName_get_PublicKeyToken_m4067,
	StrongName_get_TokenAlgorithm_m4068,
	SecurityParser__ctor_m4069,
	SecurityParser_LoadXml_m4070,
	SecurityParser_ToXml_m4071,
	SecurityParser_OnStartParsing_m4072,
	SecurityParser_OnProcessingInstruction_m4073,
	SecurityParser_OnIgnorableWhitespace_m4074,
	SecurityParser_OnStartElement_m4075,
	SecurityParser_OnEndElement_m4076,
	SecurityParser_OnChars_m4077,
	SecurityParser_OnEndParsing_m4078,
	AttrListImpl__ctor_m4079,
	AttrListImpl_get_Length_m4080,
	AttrListImpl_GetName_m4081,
	AttrListImpl_GetValue_m4082,
	AttrListImpl_GetValue_m4083,
	AttrListImpl_get_Names_m4084,
	AttrListImpl_get_Values_m4085,
	AttrListImpl_Clear_m4086,
	AttrListImpl_Add_m4087,
	SmallXmlParser__ctor_m4088,
	SmallXmlParser_Error_m4089,
	SmallXmlParser_UnexpectedEndError_m4090,
	SmallXmlParser_IsNameChar_m4091,
	SmallXmlParser_IsWhitespace_m4092,
	SmallXmlParser_SkipWhitespaces_m4093,
	SmallXmlParser_HandleWhitespaces_m4094,
	SmallXmlParser_SkipWhitespaces_m4095,
	SmallXmlParser_Peek_m4096,
	SmallXmlParser_Read_m4097,
	SmallXmlParser_Expect_m4098,
	SmallXmlParser_ReadUntil_m4099,
	SmallXmlParser_ReadName_m4100,
	SmallXmlParser_Parse_m4101,
	SmallXmlParser_Cleanup_m4102,
	SmallXmlParser_ReadContent_m4103,
	SmallXmlParser_HandleBufferedContent_m4104,
	SmallXmlParser_ReadCharacters_m4105,
	SmallXmlParser_ReadReference_m4106,
	SmallXmlParser_ReadCharacterReference_m4107,
	SmallXmlParser_ReadAttribute_m4108,
	SmallXmlParser_ReadCDATASection_m4109,
	SmallXmlParser_ReadComment_m4110,
	SmallXmlParserException__ctor_m4111,
	Runtime_GetDisplayName_m4112,
	KeyNotFoundException__ctor_m4113,
	KeyNotFoundException__ctor_m4114,
	SimpleEnumerator__ctor_m4115,
	SimpleEnumerator__cctor_m4116,
	SimpleEnumerator_Clone_m4117,
	SimpleEnumerator_MoveNext_m4118,
	SimpleEnumerator_get_Current_m4119,
	SimpleEnumerator_Reset_m4120,
	ArrayListWrapper__ctor_m4121,
	ArrayListWrapper_get_Item_m4122,
	ArrayListWrapper_set_Item_m4123,
	ArrayListWrapper_get_Count_m4124,
	ArrayListWrapper_get_Capacity_m4125,
	ArrayListWrapper_set_Capacity_m4126,
	ArrayListWrapper_get_IsReadOnly_m4127,
	ArrayListWrapper_get_IsSynchronized_m4128,
	ArrayListWrapper_get_SyncRoot_m4129,
	ArrayListWrapper_Add_m4130,
	ArrayListWrapper_Clear_m4131,
	ArrayListWrapper_Contains_m4132,
	ArrayListWrapper_IndexOf_m4133,
	ArrayListWrapper_IndexOf_m4134,
	ArrayListWrapper_IndexOf_m4135,
	ArrayListWrapper_Insert_m4136,
	ArrayListWrapper_InsertRange_m4137,
	ArrayListWrapper_Remove_m4138,
	ArrayListWrapper_RemoveAt_m4139,
	ArrayListWrapper_CopyTo_m4140,
	ArrayListWrapper_CopyTo_m4141,
	ArrayListWrapper_CopyTo_m4142,
	ArrayListWrapper_GetEnumerator_m4143,
	ArrayListWrapper_AddRange_m4144,
	ArrayListWrapper_Clone_m4145,
	ArrayListWrapper_Sort_m4146,
	ArrayListWrapper_Sort_m4147,
	ArrayListWrapper_ToArray_m4148,
	ArrayListWrapper_ToArray_m4149,
	SynchronizedArrayListWrapper__ctor_m4150,
	SynchronizedArrayListWrapper_get_Item_m4151,
	SynchronizedArrayListWrapper_set_Item_m4152,
	SynchronizedArrayListWrapper_get_Count_m4153,
	SynchronizedArrayListWrapper_get_Capacity_m4154,
	SynchronizedArrayListWrapper_set_Capacity_m4155,
	SynchronizedArrayListWrapper_get_IsReadOnly_m4156,
	SynchronizedArrayListWrapper_get_IsSynchronized_m4157,
	SynchronizedArrayListWrapper_get_SyncRoot_m4158,
	SynchronizedArrayListWrapper_Add_m4159,
	SynchronizedArrayListWrapper_Clear_m4160,
	SynchronizedArrayListWrapper_Contains_m4161,
	SynchronizedArrayListWrapper_IndexOf_m4162,
	SynchronizedArrayListWrapper_IndexOf_m4163,
	SynchronizedArrayListWrapper_IndexOf_m4164,
	SynchronizedArrayListWrapper_Insert_m4165,
	SynchronizedArrayListWrapper_InsertRange_m4166,
	SynchronizedArrayListWrapper_Remove_m4167,
	SynchronizedArrayListWrapper_RemoveAt_m4168,
	SynchronizedArrayListWrapper_CopyTo_m4169,
	SynchronizedArrayListWrapper_CopyTo_m4170,
	SynchronizedArrayListWrapper_CopyTo_m4171,
	SynchronizedArrayListWrapper_GetEnumerator_m4172,
	SynchronizedArrayListWrapper_AddRange_m4173,
	SynchronizedArrayListWrapper_Clone_m4174,
	SynchronizedArrayListWrapper_Sort_m4175,
	SynchronizedArrayListWrapper_Sort_m4176,
	SynchronizedArrayListWrapper_ToArray_m4177,
	SynchronizedArrayListWrapper_ToArray_m4178,
	FixedSizeArrayListWrapper__ctor_m4179,
	FixedSizeArrayListWrapper_get_ErrorMessage_m4180,
	FixedSizeArrayListWrapper_get_Capacity_m4181,
	FixedSizeArrayListWrapper_set_Capacity_m4182,
	FixedSizeArrayListWrapper_Add_m4183,
	FixedSizeArrayListWrapper_AddRange_m4184,
	FixedSizeArrayListWrapper_Clear_m4185,
	FixedSizeArrayListWrapper_Insert_m4186,
	FixedSizeArrayListWrapper_InsertRange_m4187,
	FixedSizeArrayListWrapper_Remove_m4188,
	FixedSizeArrayListWrapper_RemoveAt_m4189,
	ReadOnlyArrayListWrapper__ctor_m4190,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m4191,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m4192,
	ReadOnlyArrayListWrapper_get_Item_m4193,
	ReadOnlyArrayListWrapper_set_Item_m4194,
	ReadOnlyArrayListWrapper_Sort_m4195,
	ReadOnlyArrayListWrapper_Sort_m4196,
	ArrayList__ctor_m1569,
	ArrayList__ctor_m1612,
	ArrayList__ctor_m1698,
	ArrayList__ctor_m4197,
	ArrayList__cctor_m4198,
	ArrayList_get_Item_m4199,
	ArrayList_set_Item_m4200,
	ArrayList_get_Count_m4201,
	ArrayList_get_Capacity_m4202,
	ArrayList_set_Capacity_m4203,
	ArrayList_get_IsReadOnly_m4204,
	ArrayList_get_IsSynchronized_m4205,
	ArrayList_get_SyncRoot_m4206,
	ArrayList_EnsureCapacity_m4207,
	ArrayList_Shift_m4208,
	ArrayList_Add_m4209,
	ArrayList_Clear_m4210,
	ArrayList_Contains_m4211,
	ArrayList_IndexOf_m4212,
	ArrayList_IndexOf_m4213,
	ArrayList_IndexOf_m4214,
	ArrayList_Insert_m4215,
	ArrayList_InsertRange_m4216,
	ArrayList_Remove_m4217,
	ArrayList_RemoveAt_m4218,
	ArrayList_CopyTo_m4219,
	ArrayList_CopyTo_m4220,
	ArrayList_CopyTo_m4221,
	ArrayList_GetEnumerator_m4222,
	ArrayList_AddRange_m4223,
	ArrayList_Sort_m4224,
	ArrayList_Sort_m4225,
	ArrayList_ToArray_m4226,
	ArrayList_ToArray_m4227,
	ArrayList_Clone_m4228,
	ArrayList_ThrowNewArgumentOutOfRangeException_m4229,
	ArrayList_Synchronized_m4230,
	ArrayList_ReadOnly_m2668,
	BitArrayEnumerator__ctor_m4231,
	BitArrayEnumerator_Clone_m4232,
	BitArrayEnumerator_get_Current_m4233,
	BitArrayEnumerator_MoveNext_m4234,
	BitArrayEnumerator_Reset_m4235,
	BitArrayEnumerator_checkVersion_m4236,
	BitArray__ctor_m4237,
	BitArray__ctor_m1738,
	BitArray_getByte_m4238,
	BitArray_get_Count_m4239,
	BitArray_get_Item_m1729,
	BitArray_set_Item_m1739,
	BitArray_get_Length_m1728,
	BitArray_get_SyncRoot_m4240,
	BitArray_Clone_m4241,
	BitArray_CopyTo_m4242,
	BitArray_Get_m4243,
	BitArray_Set_m4244,
	BitArray_GetEnumerator_m4245,
	CaseInsensitiveComparer__ctor_m4246,
	CaseInsensitiveComparer__ctor_m4247,
	CaseInsensitiveComparer__cctor_m4248,
	CaseInsensitiveComparer_get_DefaultInvariant_m1553,
	CaseInsensitiveComparer_Compare_m4249,
	CaseInsensitiveHashCodeProvider__ctor_m4250,
	CaseInsensitiveHashCodeProvider__ctor_m4251,
	CaseInsensitiveHashCodeProvider__cctor_m4252,
	CaseInsensitiveHashCodeProvider_AreEqual_m4253,
	CaseInsensitiveHashCodeProvider_AreEqual_m4254,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m1554,
	CaseInsensitiveHashCodeProvider_GetHashCode_m4255,
	CollectionBase__ctor_m1680,
	CollectionBase_System_Collections_ICollection_CopyTo_m4256,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m4257,
	CollectionBase_System_Collections_IList_Add_m4258,
	CollectionBase_System_Collections_IList_Contains_m4259,
	CollectionBase_System_Collections_IList_IndexOf_m4260,
	CollectionBase_System_Collections_IList_Insert_m4261,
	CollectionBase_System_Collections_IList_Remove_m4262,
	CollectionBase_System_Collections_IList_get_Item_m4263,
	CollectionBase_System_Collections_IList_set_Item_m4264,
	CollectionBase_get_Count_m4265,
	CollectionBase_GetEnumerator_m4266,
	CollectionBase_Clear_m4267,
	CollectionBase_RemoveAt_m4268,
	CollectionBase_get_InnerList_m1674,
	CollectionBase_get_List_m1736,
	CollectionBase_OnClear_m4269,
	CollectionBase_OnClearComplete_m4270,
	CollectionBase_OnInsert_m4271,
	CollectionBase_OnInsertComplete_m4272,
	CollectionBase_OnRemove_m4273,
	CollectionBase_OnRemoveComplete_m4274,
	CollectionBase_OnSet_m4275,
	CollectionBase_OnSetComplete_m4276,
	CollectionBase_OnValidate_m4277,
	Comparer__ctor_m4278,
	Comparer__ctor_m4279,
	Comparer__cctor_m4280,
	Comparer_Compare_m4281,
	Comparer_GetObjectData_m4282,
	DictionaryEntry__ctor_m1558,
	DictionaryEntry_get_Key_m4283,
	DictionaryEntry_get_Value_m4284,
	KeyMarker__ctor_m4285,
	KeyMarker__cctor_m4286,
	Enumerator__ctor_m4287,
	Enumerator__cctor_m4288,
	Enumerator_FailFast_m4289,
	Enumerator_Reset_m4290,
	Enumerator_MoveNext_m4291,
	Enumerator_get_Entry_m4292,
	Enumerator_get_Key_m4293,
	Enumerator_get_Value_m4294,
	Enumerator_get_Current_m4295,
	HashKeys__ctor_m4296,
	HashKeys_get_Count_m4297,
	HashKeys_get_SyncRoot_m4298,
	HashKeys_CopyTo_m4299,
	HashKeys_GetEnumerator_m4300,
	HashValues__ctor_m4301,
	HashValues_get_Count_m4302,
	HashValues_get_SyncRoot_m4303,
	HashValues_CopyTo_m4304,
	HashValues_GetEnumerator_m4305,
	SyncHashtable__ctor_m4306,
	SyncHashtable__ctor_m4307,
	SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m4308,
	SyncHashtable_GetObjectData_m4309,
	SyncHashtable_get_Count_m4310,
	SyncHashtable_get_SyncRoot_m4311,
	SyncHashtable_get_Keys_m4312,
	SyncHashtable_get_Values_m4313,
	SyncHashtable_get_Item_m4314,
	SyncHashtable_set_Item_m4315,
	SyncHashtable_CopyTo_m4316,
	SyncHashtable_Add_m4317,
	SyncHashtable_Clear_m4318,
	SyncHashtable_Contains_m4319,
	SyncHashtable_GetEnumerator_m4320,
	SyncHashtable_Remove_m4321,
	SyncHashtable_ContainsKey_m4322,
	SyncHashtable_Clone_m4323,
	Hashtable__ctor_m1718,
	Hashtable__ctor_m4324,
	Hashtable__ctor_m4325,
	Hashtable__ctor_m1721,
	Hashtable__ctor_m4326,
	Hashtable__ctor_m1555,
	Hashtable__ctor_m4327,
	Hashtable__ctor_m1556,
	Hashtable__ctor_m1609,
	Hashtable__ctor_m4328,
	Hashtable__ctor_m1568,
	Hashtable__ctor_m4329,
	Hashtable__cctor_m4330,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m4331,
	Hashtable_set_comparer_m4332,
	Hashtable_set_hcp_m4333,
	Hashtable_get_Count_m4334,
	Hashtable_get_SyncRoot_m4335,
	Hashtable_get_Keys_m4336,
	Hashtable_get_Values_m4337,
	Hashtable_get_Item_m4338,
	Hashtable_set_Item_m4339,
	Hashtable_CopyTo_m4340,
	Hashtable_Add_m4341,
	Hashtable_Clear_m4342,
	Hashtable_Contains_m4343,
	Hashtable_GetEnumerator_m4344,
	Hashtable_Remove_m4345,
	Hashtable_ContainsKey_m4346,
	Hashtable_Clone_m4347,
	Hashtable_GetObjectData_m4348,
	Hashtable_OnDeserialization_m4349,
	Hashtable_Synchronized_m4350,
	Hashtable_GetHash_m4351,
	Hashtable_KeyEquals_m4352,
	Hashtable_AdjustThreshold_m4353,
	Hashtable_SetTable_m4354,
	Hashtable_Find_m4355,
	Hashtable_Rehash_m4356,
	Hashtable_PutImpl_m4357,
	Hashtable_CopyToArray_m4358,
	Hashtable_TestPrime_m4359,
	Hashtable_CalcPrime_m4360,
	Hashtable_ToPrime_m4361,
	Enumerator__ctor_m4362,
	Enumerator__cctor_m4363,
	Enumerator_Reset_m4364,
	Enumerator_MoveNext_m4365,
	Enumerator_get_Entry_m4366,
	Enumerator_get_Key_m4367,
	Enumerator_get_Value_m4368,
	Enumerator_get_Current_m4369,
	Enumerator_Clone_m4370,
	SortedList__ctor_m4371,
	SortedList__ctor_m1607,
	SortedList__ctor_m4372,
	SortedList__ctor_m4373,
	SortedList__cctor_m4374,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m4375,
	SortedList_get_Count_m4376,
	SortedList_get_SyncRoot_m4377,
	SortedList_get_IsFixedSize_m4378,
	SortedList_get_IsReadOnly_m4379,
	SortedList_get_Item_m4380,
	SortedList_set_Item_m4381,
	SortedList_get_Capacity_m4382,
	SortedList_set_Capacity_m4383,
	SortedList_Add_m4384,
	SortedList_Contains_m4385,
	SortedList_GetEnumerator_m4386,
	SortedList_Remove_m4387,
	SortedList_CopyTo_m4388,
	SortedList_Clone_m4389,
	SortedList_RemoveAt_m4390,
	SortedList_IndexOfKey_m4391,
	SortedList_ContainsKey_m4392,
	SortedList_GetByIndex_m4393,
	SortedList_EnsureCapacity_m4394,
	SortedList_PutImpl_m4395,
	SortedList_GetImpl_m4396,
	SortedList_InitTable_m4397,
	SortedList_Find_m4398,
	Enumerator__ctor_m4399,
	Enumerator_Clone_m4400,
	Enumerator_get_Current_m4401,
	Enumerator_MoveNext_m4402,
	Enumerator_Reset_m4403,
	Stack__ctor_m1731,
	Stack__ctor_m4404,
	Stack__ctor_m4405,
	Stack_Resize_m4406,
	Stack_get_Count_m4407,
	Stack_get_SyncRoot_m4408,
	Stack_Clear_m4409,
	Stack_Clone_m4410,
	Stack_CopyTo_m4411,
	Stack_GetEnumerator_m4412,
	Stack_Peek_m4413,
	Stack_Pop_m4414,
	Stack_Push_m4415,
	DebuggableAttribute__ctor_m4416,
	DebuggerDisplayAttribute__ctor_m4417,
	DebuggerDisplayAttribute_set_Name_m4418,
	DebuggerStepThroughAttribute__ctor_m4419,
	DebuggerTypeProxyAttribute__ctor_m4420,
	StackFrame__ctor_m4421,
	StackFrame__ctor_m4422,
	StackFrame_get_frame_info_m4423,
	StackFrame_GetFileLineNumber_m4424,
	StackFrame_GetFileName_m4425,
	StackFrame_GetSecureFileName_m4426,
	StackFrame_GetILOffset_m4427,
	StackFrame_GetMethod_m4428,
	StackFrame_GetNativeOffset_m4429,
	StackFrame_GetInternalMethodName_m4430,
	StackFrame_ToString_m4431,
	StackTrace__ctor_m4432,
	StackTrace__ctor_m632,
	StackTrace__ctor_m4433,
	StackTrace__ctor_m4434,
	StackTrace__ctor_m4435,
	StackTrace_init_frames_m4436,
	StackTrace_get_trace_m4437,
	StackTrace_get_FrameCount_m4438,
	StackTrace_GetFrame_m4439,
	StackTrace_ToString_m4440,
	Calendar__ctor_m4441,
	Calendar_Clone_m4442,
	Calendar_CheckReadOnly_m4443,
	Calendar_get_EraNames_m4444,
	CCMath_div_m4445,
	CCMath_mod_m4446,
	CCMath_div_mod_m4447,
	CCFixed_FromDateTime_m4448,
	CCFixed_day_of_week_m4449,
	CCGregorianCalendar_is_leap_year_m4450,
	CCGregorianCalendar_fixed_from_dmy_m4451,
	CCGregorianCalendar_year_from_fixed_m4452,
	CCGregorianCalendar_my_from_fixed_m4453,
	CCGregorianCalendar_dmy_from_fixed_m4454,
	CCGregorianCalendar_month_from_fixed_m4455,
	CCGregorianCalendar_day_from_fixed_m4456,
	CCGregorianCalendar_GetDayOfMonth_m4457,
	CCGregorianCalendar_GetMonth_m4458,
	CCGregorianCalendar_GetYear_m4459,
	CompareInfo__ctor_m4460,
	CompareInfo__ctor_m4461,
	CompareInfo__cctor_m4462,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m4463,
	CompareInfo_get_UseManagedCollation_m4464,
	CompareInfo_construct_compareinfo_m4465,
	CompareInfo_free_internal_collator_m4466,
	CompareInfo_internal_compare_m4467,
	CompareInfo_assign_sortkey_m4468,
	CompareInfo_internal_index_m4469,
	CompareInfo_Finalize_m4470,
	CompareInfo_internal_compare_managed_m4471,
	CompareInfo_internal_compare_switch_m4472,
	CompareInfo_Compare_m4473,
	CompareInfo_Compare_m4474,
	CompareInfo_Compare_m4475,
	CompareInfo_Equals_m4476,
	CompareInfo_GetHashCode_m4477,
	CompareInfo_GetSortKey_m4478,
	CompareInfo_IndexOf_m4479,
	CompareInfo_internal_index_managed_m4480,
	CompareInfo_internal_index_switch_m4481,
	CompareInfo_IndexOf_m4482,
	CompareInfo_IsPrefix_m4483,
	CompareInfo_IsSuffix_m4484,
	CompareInfo_LastIndexOf_m4485,
	CompareInfo_LastIndexOf_m4486,
	CompareInfo_ToString_m4487,
	CompareInfo_get_LCID_m4488,
	CultureInfo__ctor_m4489,
	CultureInfo__ctor_m4490,
	CultureInfo__ctor_m4491,
	CultureInfo__ctor_m4492,
	CultureInfo__ctor_m4493,
	CultureInfo__cctor_m4494,
	CultureInfo_get_InvariantCulture_m1598,
	CultureInfo_get_CurrentCulture_m2698,
	CultureInfo_get_CurrentUICulture_m2699,
	CultureInfo_ConstructCurrentCulture_m4495,
	CultureInfo_ConstructCurrentUICulture_m4496,
	CultureInfo_get_LCID_m4497,
	CultureInfo_get_Name_m4498,
	CultureInfo_get_Parent_m4499,
	CultureInfo_get_TextInfo_m4500,
	CultureInfo_get_IcuName_m4501,
	CultureInfo_Clone_m4502,
	CultureInfo_Equals_m4503,
	CultureInfo_GetHashCode_m4504,
	CultureInfo_ToString_m4505,
	CultureInfo_get_CompareInfo_m4506,
	CultureInfo_get_IsNeutralCulture_m4507,
	CultureInfo_CheckNeutral_m4508,
	CultureInfo_get_NumberFormat_m4509,
	CultureInfo_set_NumberFormat_m4510,
	CultureInfo_get_DateTimeFormat_m4511,
	CultureInfo_set_DateTimeFormat_m4512,
	CultureInfo_get_IsReadOnly_m4513,
	CultureInfo_GetFormat_m4514,
	CultureInfo_Construct_m4515,
	CultureInfo_ConstructInternalLocaleFromName_m4516,
	CultureInfo_ConstructInternalLocaleFromLcid_m4517,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m4518,
	CultureInfo_construct_internal_locale_from_lcid_m4519,
	CultureInfo_construct_internal_locale_from_name_m4520,
	CultureInfo_construct_internal_locale_from_current_locale_m4521,
	CultureInfo_construct_datetime_format_m4522,
	CultureInfo_construct_number_format_m4523,
	CultureInfo_ConstructInvariant_m4524,
	CultureInfo_CreateTextInfo_m4525,
	CultureInfo_CreateCulture_m4526,
	DateTimeFormatInfo__ctor_m4527,
	DateTimeFormatInfo__ctor_m4528,
	DateTimeFormatInfo__cctor_m4529,
	DateTimeFormatInfo_GetInstance_m4530,
	DateTimeFormatInfo_get_IsReadOnly_m4531,
	DateTimeFormatInfo_ReadOnly_m4532,
	DateTimeFormatInfo_Clone_m4533,
	DateTimeFormatInfo_GetFormat_m4534,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m4535,
	DateTimeFormatInfo_GetEraName_m4536,
	DateTimeFormatInfo_GetMonthName_m4537,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m4538,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m4539,
	DateTimeFormatInfo_get_RawDayNames_m4540,
	DateTimeFormatInfo_get_RawMonthNames_m4541,
	DateTimeFormatInfo_get_AMDesignator_m4542,
	DateTimeFormatInfo_get_PMDesignator_m4543,
	DateTimeFormatInfo_get_DateSeparator_m4544,
	DateTimeFormatInfo_get_TimeSeparator_m4545,
	DateTimeFormatInfo_get_LongDatePattern_m4546,
	DateTimeFormatInfo_get_ShortDatePattern_m4547,
	DateTimeFormatInfo_get_ShortTimePattern_m4548,
	DateTimeFormatInfo_get_LongTimePattern_m4549,
	DateTimeFormatInfo_get_MonthDayPattern_m4550,
	DateTimeFormatInfo_get_YearMonthPattern_m4551,
	DateTimeFormatInfo_get_FullDateTimePattern_m4552,
	DateTimeFormatInfo_get_CurrentInfo_m4553,
	DateTimeFormatInfo_get_InvariantInfo_m4554,
	DateTimeFormatInfo_get_Calendar_m4555,
	DateTimeFormatInfo_set_Calendar_m4556,
	DateTimeFormatInfo_get_RFC1123Pattern_m4557,
	DateTimeFormatInfo_get_RoundtripPattern_m4558,
	DateTimeFormatInfo_get_SortableDateTimePattern_m4559,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m4560,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m4561,
	DateTimeFormatInfo_FillAllDateTimePatterns_m4562,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m4563,
	DateTimeFormatInfo_GetDayName_m4564,
	DateTimeFormatInfo_GetAbbreviatedDayName_m4565,
	DateTimeFormatInfo_FillInvariantPatterns_m4566,
	DateTimeFormatInfo_PopulateCombinedList_m4567,
	DaylightTime__ctor_m4568,
	DaylightTime_get_Start_m4569,
	DaylightTime_get_End_m4570,
	DaylightTime_get_Delta_m4571,
	GregorianCalendar__ctor_m4572,
	GregorianCalendar__ctor_m4573,
	GregorianCalendar_get_Eras_m4574,
	GregorianCalendar_set_CalendarType_m4575,
	GregorianCalendar_GetDayOfMonth_m4576,
	GregorianCalendar_GetDayOfWeek_m4577,
	GregorianCalendar_GetEra_m4578,
	GregorianCalendar_GetMonth_m4579,
	GregorianCalendar_GetYear_m4580,
	NumberFormatInfo__ctor_m4581,
	NumberFormatInfo__ctor_m4582,
	NumberFormatInfo__ctor_m4583,
	NumberFormatInfo__cctor_m4584,
	NumberFormatInfo_get_CurrencyDecimalDigits_m4585,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m4586,
	NumberFormatInfo_get_CurrencyGroupSeparator_m4587,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m4588,
	NumberFormatInfo_get_CurrencyNegativePattern_m4589,
	NumberFormatInfo_get_CurrencyPositivePattern_m4590,
	NumberFormatInfo_get_CurrencySymbol_m4591,
	NumberFormatInfo_get_CurrentInfo_m4592,
	NumberFormatInfo_get_InvariantInfo_m4593,
	NumberFormatInfo_get_NaNSymbol_m4594,
	NumberFormatInfo_get_NegativeInfinitySymbol_m4595,
	NumberFormatInfo_get_NegativeSign_m4596,
	NumberFormatInfo_get_NumberDecimalDigits_m4597,
	NumberFormatInfo_get_NumberDecimalSeparator_m4598,
	NumberFormatInfo_get_NumberGroupSeparator_m4599,
	NumberFormatInfo_get_RawNumberGroupSizes_m4600,
	NumberFormatInfo_get_NumberNegativePattern_m4601,
	NumberFormatInfo_set_NumberNegativePattern_m4602,
	NumberFormatInfo_get_PercentDecimalDigits_m4603,
	NumberFormatInfo_get_PercentDecimalSeparator_m4604,
	NumberFormatInfo_get_PercentGroupSeparator_m4605,
	NumberFormatInfo_get_RawPercentGroupSizes_m4606,
	NumberFormatInfo_get_PercentNegativePattern_m4607,
	NumberFormatInfo_get_PercentPositivePattern_m4608,
	NumberFormatInfo_get_PercentSymbol_m4609,
	NumberFormatInfo_get_PerMilleSymbol_m4610,
	NumberFormatInfo_get_PositiveInfinitySymbol_m4611,
	NumberFormatInfo_get_PositiveSign_m4612,
	NumberFormatInfo_GetFormat_m4613,
	NumberFormatInfo_Clone_m4614,
	NumberFormatInfo_GetInstance_m4615,
	TextInfo__ctor_m4616,
	TextInfo__ctor_m4617,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m4618,
	TextInfo_get_ListSeparator_m4619,
	TextInfo_get_CultureName_m4620,
	TextInfo_Equals_m4621,
	TextInfo_GetHashCode_m4622,
	TextInfo_ToString_m4623,
	TextInfo_ToLower_m4624,
	TextInfo_ToUpper_m4625,
	TextInfo_ToLower_m4626,
	TextInfo_Clone_m4627,
	IsolatedStorageException__ctor_m4628,
	IsolatedStorageException__ctor_m4629,
	IsolatedStorageException__ctor_m4630,
	BinaryReader__ctor_m4631,
	BinaryReader__ctor_m4632,
	BinaryReader_System_IDisposable_Dispose_m4633,
	BinaryReader_get_BaseStream_m4634,
	BinaryReader_Close_m4635,
	BinaryReader_Dispose_m4636,
	BinaryReader_FillBuffer_m4637,
	BinaryReader_Read_m4638,
	BinaryReader_Read_m4639,
	BinaryReader_Read_m4640,
	BinaryReader_ReadCharBytes_m4641,
	BinaryReader_Read7BitEncodedInt_m4642,
	BinaryReader_ReadBoolean_m4643,
	BinaryReader_ReadByte_m4644,
	BinaryReader_ReadBytes_m4645,
	BinaryReader_ReadChar_m4646,
	BinaryReader_ReadDecimal_m4647,
	BinaryReader_ReadDouble_m4648,
	BinaryReader_ReadInt16_m4649,
	BinaryReader_ReadInt32_m4650,
	BinaryReader_ReadInt64_m4651,
	BinaryReader_ReadSByte_m4652,
	BinaryReader_ReadString_m4653,
	BinaryReader_ReadSingle_m4654,
	BinaryReader_ReadUInt16_m4655,
	BinaryReader_ReadUInt32_m4656,
	BinaryReader_ReadUInt64_m4657,
	BinaryReader_CheckBuffer_m4658,
	Directory_CreateDirectory_m2684,
	Directory_CreateDirectoriesInternal_m4659,
	Directory_Exists_m2683,
	Directory_GetCurrentDirectory_m4660,
	Directory_GetFiles_m2686,
	Directory_GetFileSystemEntries_m4661,
	DirectoryInfo__ctor_m4662,
	DirectoryInfo__ctor_m4663,
	DirectoryInfo__ctor_m4664,
	DirectoryInfo_Initialize_m4665,
	DirectoryInfo_get_Exists_m4666,
	DirectoryInfo_get_Parent_m4667,
	DirectoryInfo_Create_m4668,
	DirectoryInfo_ToString_m4669,
	DirectoryNotFoundException__ctor_m4670,
	DirectoryNotFoundException__ctor_m4671,
	DirectoryNotFoundException__ctor_m4672,
	EndOfStreamException__ctor_m4673,
	EndOfStreamException__ctor_m4674,
	File_Delete_m4675,
	File_Exists_m4676,
	File_Open_m4677,
	File_OpenRead_m2682,
	File_OpenText_m4678,
	FileNotFoundException__ctor_m4679,
	FileNotFoundException__ctor_m4680,
	FileNotFoundException__ctor_m4681,
	FileNotFoundException_get_Message_m4682,
	FileNotFoundException_GetObjectData_m4683,
	FileNotFoundException_ToString_m4684,
	ReadDelegate__ctor_m4685,
	ReadDelegate_Invoke_m4686,
	ReadDelegate_BeginInvoke_m4687,
	ReadDelegate_EndInvoke_m4688,
	WriteDelegate__ctor_m4689,
	WriteDelegate_Invoke_m4690,
	WriteDelegate_BeginInvoke_m4691,
	WriteDelegate_EndInvoke_m4692,
	FileStream__ctor_m4693,
	FileStream__ctor_m4694,
	FileStream__ctor_m4695,
	FileStream__ctor_m4696,
	FileStream__ctor_m4697,
	FileStream_get_CanRead_m4698,
	FileStream_get_CanWrite_m4699,
	FileStream_get_CanSeek_m4700,
	FileStream_get_Length_m4701,
	FileStream_get_Position_m4702,
	FileStream_set_Position_m4703,
	FileStream_ReadByte_m4704,
	FileStream_WriteByte_m4705,
	FileStream_Read_m4706,
	FileStream_ReadInternal_m4707,
	FileStream_BeginRead_m4708,
	FileStream_EndRead_m4709,
	FileStream_Write_m4710,
	FileStream_WriteInternal_m4711,
	FileStream_BeginWrite_m4712,
	FileStream_EndWrite_m4713,
	FileStream_Seek_m4714,
	FileStream_SetLength_m4715,
	FileStream_Flush_m4716,
	FileStream_Finalize_m4717,
	FileStream_Dispose_m4718,
	FileStream_ReadSegment_m4719,
	FileStream_WriteSegment_m4720,
	FileStream_FlushBuffer_m4721,
	FileStream_FlushBuffer_m4722,
	FileStream_FlushBufferIfDirty_m4723,
	FileStream_RefillBuffer_m4724,
	FileStream_ReadData_m4725,
	FileStream_InitBuffer_m4726,
	FileStream_GetSecureFileName_m4727,
	FileStream_GetSecureFileName_m4728,
	FileStreamAsyncResult__ctor_m4729,
	FileStreamAsyncResult_CBWrapper_m4730,
	FileStreamAsyncResult_get_AsyncState_m4731,
	FileStreamAsyncResult_get_AsyncWaitHandle_m4732,
	FileStreamAsyncResult_get_IsCompleted_m4733,
	FileSystemInfo__ctor_m4734,
	FileSystemInfo__ctor_m4735,
	FileSystemInfo_GetObjectData_m4736,
	FileSystemInfo_get_FullName_m4737,
	FileSystemInfo_Refresh_m4738,
	FileSystemInfo_InternalRefresh_m4739,
	FileSystemInfo_CheckPath_m4740,
	IOException__ctor_m4741,
	IOException__ctor_m4742,
	IOException__ctor_m2712,
	IOException__ctor_m4743,
	IOException__ctor_m4744,
	MemoryStream__ctor_m2713,
	MemoryStream__ctor_m2718,
	MemoryStream__ctor_m2719,
	MemoryStream_InternalConstructor_m4745,
	MemoryStream_CheckIfClosedThrowDisposed_m4746,
	MemoryStream_get_CanRead_m4747,
	MemoryStream_get_CanSeek_m4748,
	MemoryStream_get_CanWrite_m4749,
	MemoryStream_set_Capacity_m4750,
	MemoryStream_get_Length_m4751,
	MemoryStream_get_Position_m4752,
	MemoryStream_set_Position_m4753,
	MemoryStream_Dispose_m4754,
	MemoryStream_Flush_m4755,
	MemoryStream_Read_m4756,
	MemoryStream_ReadByte_m4757,
	MemoryStream_Seek_m4758,
	MemoryStream_CalculateNewCapacity_m4759,
	MemoryStream_Expand_m4760,
	MemoryStream_SetLength_m4761,
	MemoryStream_ToArray_m4762,
	MemoryStream_Write_m4763,
	MemoryStream_WriteByte_m4764,
	MonoIO__cctor_m4765,
	MonoIO_GetException_m4766,
	MonoIO_GetException_m4767,
	MonoIO_CreateDirectory_m4768,
	MonoIO_GetFileSystemEntries_m4769,
	MonoIO_GetCurrentDirectory_m4770,
	MonoIO_DeleteFile_m4771,
	MonoIO_GetFileAttributes_m4772,
	MonoIO_GetFileType_m4773,
	MonoIO_ExistsFile_m4774,
	MonoIO_ExistsDirectory_m4775,
	MonoIO_GetFileStat_m4776,
	MonoIO_Open_m4777,
	MonoIO_Close_m4778,
	MonoIO_Read_m4779,
	MonoIO_Write_m4780,
	MonoIO_Seek_m4781,
	MonoIO_GetLength_m4782,
	MonoIO_SetLength_m4783,
	MonoIO_get_ConsoleOutput_m4784,
	MonoIO_get_ConsoleInput_m4785,
	MonoIO_get_ConsoleError_m4786,
	MonoIO_get_VolumeSeparatorChar_m4787,
	MonoIO_get_DirectorySeparatorChar_m4788,
	MonoIO_get_AltDirectorySeparatorChar_m4789,
	MonoIO_get_PathSeparator_m4790,
	Path__cctor_m4791,
	Path_Combine_m2685,
	Path_CleanPath_m4792,
	Path_GetDirectoryName_m4793,
	Path_GetFileName_m4794,
	Path_GetFullPath_m4795,
	Path_WindowsDriveAdjustment_m4796,
	Path_InsecureGetFullPath_m4797,
	Path_IsDsc_m4798,
	Path_GetPathRoot_m4799,
	Path_IsPathRooted_m4800,
	Path_GetInvalidPathChars_m4801,
	Path_GetServerAndShare_m4802,
	Path_SameRoot_m4803,
	Path_CanonicalizePath_m4804,
	PathTooLongException__ctor_m4805,
	PathTooLongException__ctor_m4806,
	PathTooLongException__ctor_m4807,
	SearchPattern__cctor_m4808,
	Stream__ctor_m2714,
	Stream__cctor_m4809,
	Stream_Dispose_m4810,
	Stream_Dispose_m2717,
	Stream_Close_m2716,
	Stream_ReadByte_m4811,
	Stream_WriteByte_m4812,
	Stream_BeginRead_m4813,
	Stream_BeginWrite_m4814,
	Stream_EndRead_m4815,
	Stream_EndWrite_m4816,
	NullStream__ctor_m4817,
	NullStream_get_CanRead_m4818,
	NullStream_get_CanSeek_m4819,
	NullStream_get_CanWrite_m4820,
	NullStream_get_Length_m4821,
	NullStream_get_Position_m4822,
	NullStream_set_Position_m4823,
	NullStream_Flush_m4824,
	NullStream_Read_m4825,
	NullStream_ReadByte_m4826,
	NullStream_Seek_m4827,
	NullStream_SetLength_m4828,
	NullStream_Write_m4829,
	NullStream_WriteByte_m4830,
	StreamAsyncResult__ctor_m4831,
	StreamAsyncResult_SetComplete_m4832,
	StreamAsyncResult_SetComplete_m4833,
	StreamAsyncResult_get_AsyncState_m4834,
	StreamAsyncResult_get_AsyncWaitHandle_m4835,
	StreamAsyncResult_get_IsCompleted_m4836,
	StreamAsyncResult_get_Exception_m4837,
	StreamAsyncResult_get_NBytes_m4838,
	StreamAsyncResult_get_Done_m4839,
	StreamAsyncResult_set_Done_m4840,
	NullStreamReader__ctor_m4841,
	NullStreamReader_Peek_m4842,
	NullStreamReader_Read_m4843,
	NullStreamReader_Read_m4844,
	NullStreamReader_ReadLine_m4845,
	NullStreamReader_ReadToEnd_m4846,
	StreamReader__ctor_m4847,
	StreamReader__ctor_m4848,
	StreamReader__ctor_m4849,
	StreamReader__ctor_m4850,
	StreamReader__ctor_m4851,
	StreamReader__cctor_m4852,
	StreamReader_Initialize_m4853,
	StreamReader_Dispose_m4854,
	StreamReader_DoChecks_m4855,
	StreamReader_ReadBuffer_m4856,
	StreamReader_Peek_m4857,
	StreamReader_Read_m4858,
	StreamReader_Read_m4859,
	StreamReader_FindNextEOL_m4860,
	StreamReader_ReadLine_m4861,
	StreamReader_ReadToEnd_m4862,
	StreamWriter__ctor_m4863,
	StreamWriter__ctor_m4864,
	StreamWriter__cctor_m4865,
	StreamWriter_Initialize_m4866,
	StreamWriter_set_AutoFlush_m4867,
	StreamWriter_Dispose_m4868,
	StreamWriter_Flush_m4869,
	StreamWriter_FlushBytes_m4870,
	StreamWriter_Decode_m4871,
	StreamWriter_Write_m4872,
	StreamWriter_LowLevelWrite_m4873,
	StreamWriter_LowLevelWrite_m4874,
	StreamWriter_Write_m4875,
	StreamWriter_Write_m4876,
	StreamWriter_Write_m4877,
	StreamWriter_Close_m4878,
	StreamWriter_Finalize_m4879,
	StringReader__ctor_m4880,
	StringReader_Dispose_m4881,
	StringReader_Peek_m4882,
	StringReader_Read_m4883,
	StringReader_Read_m4884,
	StringReader_ReadLine_m4885,
	StringReader_ReadToEnd_m4886,
	StringReader_CheckObjectDisposedException_m4887,
	NullTextReader__ctor_m4888,
	NullTextReader_ReadLine_m4889,
	TextReader__ctor_m4890,
	TextReader__cctor_m4891,
	TextReader_Dispose_m4892,
	TextReader_Dispose_m4893,
	TextReader_Peek_m4894,
	TextReader_Read_m4895,
	TextReader_Read_m4896,
	TextReader_ReadLine_m4897,
	TextReader_ReadToEnd_m4898,
	TextReader_Synchronized_m4899,
	SynchronizedReader__ctor_m4900,
	SynchronizedReader_Peek_m4901,
	SynchronizedReader_ReadLine_m4902,
	SynchronizedReader_ReadToEnd_m4903,
	SynchronizedReader_Read_m4904,
	SynchronizedReader_Read_m4905,
	NullTextWriter__ctor_m4906,
	NullTextWriter_Write_m4907,
	NullTextWriter_Write_m4908,
	NullTextWriter_Write_m4909,
	TextWriter__ctor_m4910,
	TextWriter__cctor_m4911,
	TextWriter_Close_m4912,
	TextWriter_Dispose_m4913,
	TextWriter_Dispose_m4914,
	TextWriter_Flush_m4915,
	TextWriter_Synchronized_m4916,
	TextWriter_Write_m4917,
	TextWriter_Write_m4918,
	TextWriter_Write_m4919,
	TextWriter_Write_m4920,
	TextWriter_WriteLine_m4921,
	TextWriter_WriteLine_m4922,
	SynchronizedWriter__ctor_m4923,
	SynchronizedWriter_Close_m4924,
	SynchronizedWriter_Flush_m4925,
	SynchronizedWriter_Write_m4926,
	SynchronizedWriter_Write_m4927,
	SynchronizedWriter_Write_m4928,
	SynchronizedWriter_Write_m4929,
	SynchronizedWriter_WriteLine_m4930,
	SynchronizedWriter_WriteLine_m4931,
	UnexceptionalStreamReader__ctor_m4932,
	UnexceptionalStreamReader__cctor_m4933,
	UnexceptionalStreamReader_Peek_m4934,
	UnexceptionalStreamReader_Read_m4935,
	UnexceptionalStreamReader_Read_m4936,
	UnexceptionalStreamReader_CheckEOL_m4937,
	UnexceptionalStreamReader_ReadLine_m4938,
	UnexceptionalStreamReader_ReadToEnd_m4939,
	UnexceptionalStreamWriter__ctor_m4940,
	UnexceptionalStreamWriter_Flush_m4941,
	UnexceptionalStreamWriter_Write_m4942,
	UnexceptionalStreamWriter_Write_m4943,
	UnexceptionalStreamWriter_Write_m4944,
	UnexceptionalStreamWriter_Write_m4945,
	UnmanagedMemoryStream_get_CanRead_m4946,
	UnmanagedMemoryStream_get_CanSeek_m4947,
	UnmanagedMemoryStream_get_CanWrite_m4948,
	UnmanagedMemoryStream_get_Length_m4949,
	UnmanagedMemoryStream_get_Position_m4950,
	UnmanagedMemoryStream_set_Position_m4951,
	UnmanagedMemoryStream_Read_m4952,
	UnmanagedMemoryStream_ReadByte_m4953,
	UnmanagedMemoryStream_Seek_m4954,
	UnmanagedMemoryStream_SetLength_m4955,
	UnmanagedMemoryStream_Flush_m4956,
	UnmanagedMemoryStream_Dispose_m4957,
	UnmanagedMemoryStream_Write_m4958,
	UnmanagedMemoryStream_WriteByte_m4959,
	AssemblyBuilder_get_Location_m4960,
	AssemblyBuilder_GetModulesInternal_m4961,
	AssemblyBuilder_GetTypes_m4962,
	AssemblyBuilder_get_IsCompilerContext_m4963,
	AssemblyBuilder_not_supported_m4964,
	AssemblyBuilder_UnprotectedGetName_m4965,
	ConstructorBuilder__ctor_m4966,
	ConstructorBuilder_get_CallingConvention_m4967,
	ConstructorBuilder_get_TypeBuilder_m4968,
	ConstructorBuilder_GetParameters_m4969,
	ConstructorBuilder_GetParametersInternal_m4970,
	ConstructorBuilder_GetParameterCount_m4971,
	ConstructorBuilder_Invoke_m4972,
	ConstructorBuilder_Invoke_m4973,
	ConstructorBuilder_get_MethodHandle_m4974,
	ConstructorBuilder_get_Attributes_m4975,
	ConstructorBuilder_get_ReflectedType_m4976,
	ConstructorBuilder_get_DeclaringType_m4977,
	ConstructorBuilder_get_Name_m4978,
	ConstructorBuilder_IsDefined_m4979,
	ConstructorBuilder_GetCustomAttributes_m4980,
	ConstructorBuilder_GetCustomAttributes_m4981,
	ConstructorBuilder_GetILGenerator_m4982,
	ConstructorBuilder_GetILGenerator_m4983,
	ConstructorBuilder_GetToken_m4984,
	ConstructorBuilder_get_Module_m4985,
	ConstructorBuilder_ToString_m4986,
	ConstructorBuilder_fixup_m4987,
	ConstructorBuilder_get_next_table_index_m4988,
	ConstructorBuilder_get_IsCompilerContext_m4989,
	ConstructorBuilder_not_supported_m4990,
	ConstructorBuilder_not_created_m4991,
	EnumBuilder_get_Assembly_m4992,
	EnumBuilder_get_AssemblyQualifiedName_m4993,
	EnumBuilder_get_BaseType_m4994,
	EnumBuilder_get_DeclaringType_m4995,
	EnumBuilder_get_FullName_m4996,
	EnumBuilder_get_Module_m4997,
	EnumBuilder_get_Name_m4998,
	EnumBuilder_get_Namespace_m4999,
	EnumBuilder_get_ReflectedType_m5000,
	EnumBuilder_get_TypeHandle_m5001,
	EnumBuilder_get_UnderlyingSystemType_m5002,
	EnumBuilder_GetAttributeFlagsImpl_m5003,
	EnumBuilder_GetConstructorImpl_m5004,
	EnumBuilder_GetConstructors_m5005,
	EnumBuilder_GetCustomAttributes_m5006,
	EnumBuilder_GetCustomAttributes_m5007,
	EnumBuilder_GetElementType_m5008,
	EnumBuilder_GetEvent_m5009,
	EnumBuilder_GetField_m5010,
	EnumBuilder_GetFields_m5011,
	EnumBuilder_GetInterfaces_m5012,
	EnumBuilder_GetMethodImpl_m5013,
	EnumBuilder_GetMethods_m5014,
	EnumBuilder_GetPropertyImpl_m5015,
	EnumBuilder_HasElementTypeImpl_m5016,
	EnumBuilder_InvokeMember_m5017,
	EnumBuilder_IsArrayImpl_m5018,
	EnumBuilder_IsByRefImpl_m5019,
	EnumBuilder_IsPointerImpl_m5020,
	EnumBuilder_IsPrimitiveImpl_m5021,
	EnumBuilder_IsValueTypeImpl_m5022,
	EnumBuilder_IsDefined_m5023,
	EnumBuilder_CreateNotSupportedException_m5024,
	FieldBuilder_get_Attributes_m5025,
	FieldBuilder_get_DeclaringType_m5026,
	FieldBuilder_get_FieldHandle_m5027,
	FieldBuilder_get_FieldType_m5028,
	FieldBuilder_get_Name_m5029,
	FieldBuilder_get_ReflectedType_m5030,
	FieldBuilder_GetCustomAttributes_m5031,
	FieldBuilder_GetCustomAttributes_m5032,
	FieldBuilder_GetValue_m5033,
	FieldBuilder_IsDefined_m5034,
	FieldBuilder_GetFieldOffset_m5035,
	FieldBuilder_SetValue_m5036,
	FieldBuilder_get_UMarshal_m5037,
	FieldBuilder_CreateNotSupportedException_m5038,
	FieldBuilder_get_Module_m5039,
	GenericTypeParameterBuilder_IsSubclassOf_m5040,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m5041,
	GenericTypeParameterBuilder_GetConstructorImpl_m5042,
	GenericTypeParameterBuilder_GetConstructors_m5043,
	GenericTypeParameterBuilder_GetEvent_m5044,
	GenericTypeParameterBuilder_GetField_m5045,
	GenericTypeParameterBuilder_GetFields_m5046,
	GenericTypeParameterBuilder_GetInterfaces_m5047,
	GenericTypeParameterBuilder_GetMethods_m5048,
	GenericTypeParameterBuilder_GetMethodImpl_m5049,
	GenericTypeParameterBuilder_GetPropertyImpl_m5050,
	GenericTypeParameterBuilder_HasElementTypeImpl_m5051,
	GenericTypeParameterBuilder_IsAssignableFrom_m5052,
	GenericTypeParameterBuilder_IsInstanceOfType_m5053,
	GenericTypeParameterBuilder_IsArrayImpl_m5054,
	GenericTypeParameterBuilder_IsByRefImpl_m5055,
	GenericTypeParameterBuilder_IsPointerImpl_m5056,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m5057,
	GenericTypeParameterBuilder_IsValueTypeImpl_m5058,
	GenericTypeParameterBuilder_InvokeMember_m5059,
	GenericTypeParameterBuilder_GetElementType_m5060,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m5061,
	GenericTypeParameterBuilder_get_Assembly_m5062,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m5063,
	GenericTypeParameterBuilder_get_BaseType_m5064,
	GenericTypeParameterBuilder_get_FullName_m5065,
	GenericTypeParameterBuilder_IsDefined_m5066,
	GenericTypeParameterBuilder_GetCustomAttributes_m5067,
	GenericTypeParameterBuilder_GetCustomAttributes_m5068,
	GenericTypeParameterBuilder_get_Name_m5069,
	GenericTypeParameterBuilder_get_Namespace_m5070,
	GenericTypeParameterBuilder_get_Module_m5071,
	GenericTypeParameterBuilder_get_DeclaringType_m5072,
	GenericTypeParameterBuilder_get_ReflectedType_m5073,
	GenericTypeParameterBuilder_get_TypeHandle_m5074,
	GenericTypeParameterBuilder_GetGenericArguments_m5075,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m5076,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m5077,
	GenericTypeParameterBuilder_get_IsGenericParameter_m5078,
	GenericTypeParameterBuilder_get_IsGenericType_m5079,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m5080,
	GenericTypeParameterBuilder_not_supported_m5081,
	GenericTypeParameterBuilder_ToString_m5082,
	GenericTypeParameterBuilder_Equals_m5083,
	GenericTypeParameterBuilder_GetHashCode_m5084,
	GenericTypeParameterBuilder_MakeGenericType_m5085,
	ILGenerator__ctor_m5086,
	ILGenerator__cctor_m5087,
	ILGenerator_add_token_fixup_m5088,
	ILGenerator_make_room_m5089,
	ILGenerator_emit_int_m5090,
	ILGenerator_ll_emit_m5091,
	ILGenerator_Emit_m5092,
	ILGenerator_Emit_m5093,
	ILGenerator_label_fixup_m5094,
	ILGenerator_Mono_GetCurrentOffset_m5095,
	MethodBuilder_get_ContainsGenericParameters_m5096,
	MethodBuilder_get_MethodHandle_m5097,
	MethodBuilder_get_ReturnType_m5098,
	MethodBuilder_get_ReflectedType_m5099,
	MethodBuilder_get_DeclaringType_m5100,
	MethodBuilder_get_Name_m5101,
	MethodBuilder_get_Attributes_m5102,
	MethodBuilder_get_CallingConvention_m5103,
	MethodBuilder_GetBaseDefinition_m5104,
	MethodBuilder_GetParameters_m5105,
	MethodBuilder_GetParameterCount_m5106,
	MethodBuilder_Invoke_m5107,
	MethodBuilder_IsDefined_m5108,
	MethodBuilder_GetCustomAttributes_m5109,
	MethodBuilder_GetCustomAttributes_m5110,
	MethodBuilder_check_override_m5111,
	MethodBuilder_fixup_m5112,
	MethodBuilder_ToString_m5113,
	MethodBuilder_Equals_m5114,
	MethodBuilder_GetHashCode_m5115,
	MethodBuilder_get_next_table_index_m5116,
	MethodBuilder_NotSupported_m5117,
	MethodBuilder_MakeGenericMethod_m5118,
	MethodBuilder_get_IsGenericMethodDefinition_m5119,
	MethodBuilder_get_IsGenericMethod_m5120,
	MethodBuilder_GetGenericArguments_m5121,
	MethodBuilder_get_Module_m5122,
	MethodToken__ctor_m5123,
	MethodToken__cctor_m5124,
	MethodToken_Equals_m5125,
	MethodToken_GetHashCode_m5126,
	MethodToken_get_Token_m5127,
	ModuleBuilder__cctor_m5128,
	ModuleBuilder_get_next_table_index_m5129,
	ModuleBuilder_GetTypes_m5130,
	ModuleBuilder_getToken_m5131,
	ModuleBuilder_GetToken_m5132,
	ModuleBuilder_RegisterToken_m5133,
	ModuleBuilder_GetTokenGenerator_m5134,
	ModuleBuilderTokenGenerator__ctor_m5135,
	ModuleBuilderTokenGenerator_GetToken_m5136,
	OpCode__ctor_m5137,
	OpCode_GetHashCode_m5138,
	OpCode_Equals_m5139,
	OpCode_ToString_m5140,
	OpCode_get_Name_m5141,
	OpCode_get_Size_m5142,
	OpCode_get_StackBehaviourPop_m5143,
	OpCode_get_StackBehaviourPush_m5144,
	OpCodeNames__cctor_m5145,
	OpCodes__cctor_m5146,
	ParameterBuilder_get_Attributes_m5147,
	ParameterBuilder_get_Name_m5148,
	ParameterBuilder_get_Position_m5149,
	TypeBuilder_GetAttributeFlagsImpl_m5150,
	TypeBuilder_setup_internal_class_m5151,
	TypeBuilder_create_generic_class_m5152,
	TypeBuilder_get_Assembly_m5153,
	TypeBuilder_get_AssemblyQualifiedName_m5154,
	TypeBuilder_get_BaseType_m5155,
	TypeBuilder_get_DeclaringType_m5156,
	TypeBuilder_get_UnderlyingSystemType_m5157,
	TypeBuilder_get_FullName_m5158,
	TypeBuilder_get_Module_m5159,
	TypeBuilder_get_Name_m5160,
	TypeBuilder_get_Namespace_m5161,
	TypeBuilder_get_ReflectedType_m5162,
	TypeBuilder_GetConstructorImpl_m5163,
	TypeBuilder_IsDefined_m5164,
	TypeBuilder_GetCustomAttributes_m5165,
	TypeBuilder_GetCustomAttributes_m5166,
	TypeBuilder_DefineConstructor_m5167,
	TypeBuilder_DefineConstructor_m5168,
	TypeBuilder_DefineDefaultConstructor_m5169,
	TypeBuilder_create_runtime_class_m5170,
	TypeBuilder_is_nested_in_m5171,
	TypeBuilder_has_ctor_method_m5172,
	TypeBuilder_CreateType_m5173,
	TypeBuilder_GetConstructors_m5174,
	TypeBuilder_GetConstructorsInternal_m5175,
	TypeBuilder_GetElementType_m5176,
	TypeBuilder_GetEvent_m5177,
	TypeBuilder_GetField_m5178,
	TypeBuilder_GetFields_m5179,
	TypeBuilder_GetInterfaces_m5180,
	TypeBuilder_GetMethodsByName_m5181,
	TypeBuilder_GetMethods_m5182,
	TypeBuilder_GetMethodImpl_m5183,
	TypeBuilder_GetPropertyImpl_m5184,
	TypeBuilder_HasElementTypeImpl_m5185,
	TypeBuilder_InvokeMember_m5186,
	TypeBuilder_IsArrayImpl_m5187,
	TypeBuilder_IsByRefImpl_m5188,
	TypeBuilder_IsPointerImpl_m5189,
	TypeBuilder_IsPrimitiveImpl_m5190,
	TypeBuilder_IsValueTypeImpl_m5191,
	TypeBuilder_MakeGenericType_m5192,
	TypeBuilder_get_TypeHandle_m5193,
	TypeBuilder_SetParent_m5194,
	TypeBuilder_get_next_table_index_m5195,
	TypeBuilder_get_IsCompilerContext_m5196,
	TypeBuilder_get_is_created_m5197,
	TypeBuilder_not_supported_m5198,
	TypeBuilder_check_not_created_m5199,
	TypeBuilder_check_created_m5200,
	TypeBuilder_ToString_m5201,
	TypeBuilder_IsAssignableFrom_m5202,
	TypeBuilder_IsSubclassOf_m5203,
	TypeBuilder_IsAssignableTo_m5204,
	TypeBuilder_GetGenericArguments_m5205,
	TypeBuilder_GetGenericTypeDefinition_m5206,
	TypeBuilder_get_ContainsGenericParameters_m5207,
	TypeBuilder_get_IsGenericParameter_m5208,
	TypeBuilder_get_IsGenericTypeDefinition_m5209,
	TypeBuilder_get_IsGenericType_m5210,
	UnmanagedMarshal_ToMarshalAsAttribute_m5211,
	AmbiguousMatchException__ctor_m5212,
	AmbiguousMatchException__ctor_m5213,
	AmbiguousMatchException__ctor_m5214,
	ResolveEventHolder__ctor_m5215,
	Assembly__ctor_m5216,
	Assembly_get_code_base_m5217,
	Assembly_get_fullname_m5218,
	Assembly_get_location_m5219,
	Assembly_GetCodeBase_m5220,
	Assembly_get_FullName_m5221,
	Assembly_get_Location_m5222,
	Assembly_IsDefined_m5223,
	Assembly_GetCustomAttributes_m5224,
	Assembly_GetManifestResourceInternal_m5225,
	Assembly_GetTypes_m5226,
	Assembly_GetTypes_m5227,
	Assembly_GetType_m5228,
	Assembly_GetType_m5229,
	Assembly_InternalGetType_m5230,
	Assembly_GetType_m5231,
	Assembly_FillName_m5232,
	Assembly_GetName_m5233,
	Assembly_GetName_m5234,
	Assembly_UnprotectedGetName_m5235,
	Assembly_ToString_m5236,
	Assembly_Load_m5237,
	Assembly_GetModule_m5238,
	Assembly_GetModulesInternal_m5239,
	Assembly_GetModules_m5240,
	Assembly_GetExecutingAssembly_m5241,
	AssemblyCompanyAttribute__ctor_m5242,
	AssemblyCopyrightAttribute__ctor_m5243,
	AssemblyDefaultAliasAttribute__ctor_m5244,
	AssemblyDelaySignAttribute__ctor_m5245,
	AssemblyDescriptionAttribute__ctor_m5246,
	AssemblyFileVersionAttribute__ctor_m5247,
	AssemblyInformationalVersionAttribute__ctor_m5248,
	AssemblyKeyFileAttribute__ctor_m5249,
	AssemblyName__ctor_m5250,
	AssemblyName__ctor_m5251,
	AssemblyName_get_Name_m5252,
	AssemblyName_get_Flags_m5253,
	AssemblyName_get_FullName_m5254,
	AssemblyName_get_Version_m5255,
	AssemblyName_set_Version_m5256,
	AssemblyName_ToString_m5257,
	AssemblyName_get_IsPublicKeyValid_m5258,
	AssemblyName_InternalGetPublicKeyToken_m5259,
	AssemblyName_ComputePublicKeyToken_m5260,
	AssemblyName_SetPublicKey_m5261,
	AssemblyName_SetPublicKeyToken_m5262,
	AssemblyName_GetObjectData_m5263,
	AssemblyName_Clone_m5264,
	AssemblyName_OnDeserialization_m5265,
	AssemblyProductAttribute__ctor_m5266,
	AssemblyTitleAttribute__ctor_m5267,
	Default__ctor_m5268,
	Default_BindToMethod_m5269,
	Default_ReorderParameters_m5270,
	Default_IsArrayAssignable_m5271,
	Default_ChangeType_m5272,
	Default_ReorderArgumentArray_m5273,
	Default_check_type_m5274,
	Default_check_arguments_m5275,
	Default_SelectMethod_m5276,
	Default_SelectMethod_m5277,
	Default_GetBetterMethod_m5278,
	Default_CompareCloserType_m5279,
	Default_SelectProperty_m5280,
	Default_check_arguments_with_score_m5281,
	Default_check_type_with_score_m5282,
	Binder__ctor_m5283,
	Binder__cctor_m5284,
	Binder_get_DefaultBinder_m5285,
	Binder_ConvertArgs_m5286,
	Binder_GetDerivedLevel_m5287,
	Binder_FindMostDerivedMatch_m5288,
	ConstructorInfo__ctor_m5289,
	ConstructorInfo__cctor_m5290,
	ConstructorInfo_get_MemberType_m5291,
	ConstructorInfo_Invoke_m5292,
	CustomAttributeData__ctor_m5293,
	CustomAttributeData_get_Constructor_m5294,
	CustomAttributeData_get_ConstructorArguments_m5295,
	CustomAttributeData_get_NamedArguments_m5296,
	CustomAttributeData_GetCustomAttributes_m5297,
	CustomAttributeData_GetCustomAttributes_m5298,
	CustomAttributeData_GetCustomAttributes_m5299,
	CustomAttributeData_GetCustomAttributes_m5300,
	CustomAttributeData_ToString_m5301,
	CustomAttributeData_Equals_m5302,
	CustomAttributeData_GetHashCode_m5303,
	CustomAttributeNamedArgument_ToString_m5304,
	CustomAttributeNamedArgument_Equals_m5305,
	CustomAttributeNamedArgument_GetHashCode_m5306,
	CustomAttributeTypedArgument_ToString_m5307,
	CustomAttributeTypedArgument_Equals_m5308,
	CustomAttributeTypedArgument_GetHashCode_m5309,
	AddEventAdapter__ctor_m5310,
	AddEventAdapter_Invoke_m5311,
	AddEventAdapter_BeginInvoke_m5312,
	AddEventAdapter_EndInvoke_m5313,
	EventInfo__ctor_m5314,
	EventInfo_get_EventHandlerType_m5315,
	EventInfo_get_MemberType_m5316,
	FieldInfo__ctor_m5317,
	FieldInfo_get_MemberType_m5318,
	FieldInfo_get_IsLiteral_m5319,
	FieldInfo_get_IsStatic_m5320,
	FieldInfo_get_IsNotSerialized_m5321,
	FieldInfo_SetValue_m5322,
	FieldInfo_internal_from_handle_type_m5323,
	FieldInfo_GetFieldFromHandle_m5324,
	FieldInfo_GetFieldOffset_m5325,
	FieldInfo_GetUnmanagedMarshal_m5326,
	FieldInfo_get_UMarshal_m5327,
	FieldInfo_GetPseudoCustomAttributes_m5328,
	MemberInfoSerializationHolder__ctor_m5329,
	MemberInfoSerializationHolder_Serialize_m5330,
	MemberInfoSerializationHolder_Serialize_m5331,
	MemberInfoSerializationHolder_GetObjectData_m5332,
	MemberInfoSerializationHolder_GetRealObject_m5333,
	MethodBase__ctor_m5334,
	MethodBase_GetMethodFromHandleNoGenericCheck_m5335,
	MethodBase_GetMethodFromIntPtr_m5336,
	MethodBase_GetMethodFromHandle_m5337,
	MethodBase_GetMethodFromHandleInternalType_m5338,
	MethodBase_GetParameterCount_m5339,
	MethodBase_Invoke_m5340,
	MethodBase_get_CallingConvention_m5341,
	MethodBase_get_IsPublic_m5342,
	MethodBase_get_IsStatic_m5343,
	MethodBase_get_IsVirtual_m5344,
	MethodBase_get_IsAbstract_m5345,
	MethodBase_get_next_table_index_m5346,
	MethodBase_GetGenericArguments_m5347,
	MethodBase_get_ContainsGenericParameters_m5348,
	MethodBase_get_IsGenericMethodDefinition_m5349,
	MethodBase_get_IsGenericMethod_m5350,
	MethodInfo__ctor_m5351,
	MethodInfo_get_MemberType_m5352,
	MethodInfo_get_ReturnType_m5353,
	MethodInfo_MakeGenericMethod_m5354,
	MethodInfo_GetGenericArguments_m5355,
	MethodInfo_get_IsGenericMethod_m5356,
	MethodInfo_get_IsGenericMethodDefinition_m5357,
	MethodInfo_get_ContainsGenericParameters_m5358,
	Missing__ctor_m5359,
	Missing__cctor_m5360,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m5361,
	Module__ctor_m5362,
	Module__cctor_m5363,
	Module_get_Assembly_m5364,
	Module_get_ScopeName_m5365,
	Module_GetCustomAttributes_m5366,
	Module_GetObjectData_m5367,
	Module_InternalGetTypes_m5368,
	Module_GetTypes_m5369,
	Module_IsDefined_m5370,
	Module_IsResource_m5371,
	Module_ToString_m5372,
	Module_filter_by_type_name_m5373,
	Module_filter_by_type_name_ignore_case_m5374,
	MonoEventInfo_get_event_info_m5375,
	MonoEventInfo_GetEventInfo_m5376,
	MonoEvent__ctor_m5377,
	MonoEvent_get_Attributes_m5378,
	MonoEvent_GetAddMethod_m5379,
	MonoEvent_get_DeclaringType_m5380,
	MonoEvent_get_ReflectedType_m5381,
	MonoEvent_get_Name_m5382,
	MonoEvent_ToString_m5383,
	MonoEvent_IsDefined_m5384,
	MonoEvent_GetCustomAttributes_m5385,
	MonoEvent_GetCustomAttributes_m5386,
	MonoEvent_GetObjectData_m5387,
	MonoField__ctor_m5388,
	MonoField_get_Attributes_m5389,
	MonoField_get_FieldHandle_m5390,
	MonoField_get_FieldType_m5391,
	MonoField_GetParentType_m5392,
	MonoField_get_ReflectedType_m5393,
	MonoField_get_DeclaringType_m5394,
	MonoField_get_Name_m5395,
	MonoField_IsDefined_m5396,
	MonoField_GetCustomAttributes_m5397,
	MonoField_GetCustomAttributes_m5398,
	MonoField_GetFieldOffset_m5399,
	MonoField_GetValueInternal_m5400,
	MonoField_GetValue_m5401,
	MonoField_ToString_m5402,
	MonoField_SetValueInternal_m5403,
	MonoField_SetValue_m5404,
	MonoField_GetObjectData_m5405,
	MonoField_CheckGeneric_m5406,
	MonoGenericMethod__ctor_m5407,
	MonoGenericMethod_get_ReflectedType_m5408,
	MonoGenericCMethod__ctor_m5409,
	MonoGenericCMethod_get_ReflectedType_m5410,
	MonoMethodInfo_get_method_info_m5411,
	MonoMethodInfo_GetMethodInfo_m5412,
	MonoMethodInfo_GetDeclaringType_m5413,
	MonoMethodInfo_GetReturnType_m5414,
	MonoMethodInfo_GetAttributes_m5415,
	MonoMethodInfo_GetCallingConvention_m5416,
	MonoMethodInfo_get_parameter_info_m5417,
	MonoMethodInfo_GetParametersInfo_m5418,
	MonoMethod__ctor_m5419,
	MonoMethod_get_name_m5420,
	MonoMethod_get_base_definition_m5421,
	MonoMethod_GetBaseDefinition_m5422,
	MonoMethod_get_ReturnType_m5423,
	MonoMethod_GetParameters_m5424,
	MonoMethod_InternalInvoke_m5425,
	MonoMethod_Invoke_m5426,
	MonoMethod_get_MethodHandle_m5427,
	MonoMethod_get_Attributes_m5428,
	MonoMethod_get_CallingConvention_m5429,
	MonoMethod_get_ReflectedType_m5430,
	MonoMethod_get_DeclaringType_m5431,
	MonoMethod_get_Name_m5432,
	MonoMethod_IsDefined_m5433,
	MonoMethod_GetCustomAttributes_m5434,
	MonoMethod_GetCustomAttributes_m5435,
	MonoMethod_GetDllImportAttribute_m5436,
	MonoMethod_GetPseudoCustomAttributes_m5437,
	MonoMethod_ShouldPrintFullName_m5438,
	MonoMethod_ToString_m5439,
	MonoMethod_GetObjectData_m5440,
	MonoMethod_MakeGenericMethod_m5441,
	MonoMethod_MakeGenericMethod_impl_m5442,
	MonoMethod_GetGenericArguments_m5443,
	MonoMethod_get_IsGenericMethodDefinition_m5444,
	MonoMethod_get_IsGenericMethod_m5445,
	MonoMethod_get_ContainsGenericParameters_m5446,
	MonoCMethod__ctor_m5447,
	MonoCMethod_GetParameters_m5448,
	MonoCMethod_InternalInvoke_m5449,
	MonoCMethod_Invoke_m5450,
	MonoCMethod_Invoke_m5451,
	MonoCMethod_get_MethodHandle_m5452,
	MonoCMethod_get_Attributes_m5453,
	MonoCMethod_get_CallingConvention_m5454,
	MonoCMethod_get_ReflectedType_m5455,
	MonoCMethod_get_DeclaringType_m5456,
	MonoCMethod_get_Name_m5457,
	MonoCMethod_IsDefined_m5458,
	MonoCMethod_GetCustomAttributes_m5459,
	MonoCMethod_GetCustomAttributes_m5460,
	MonoCMethod_ToString_m5461,
	MonoCMethod_GetObjectData_m5462,
	MonoPropertyInfo_get_property_info_m5463,
	MonoPropertyInfo_GetTypeModifiers_m5464,
	GetterAdapter__ctor_m5465,
	GetterAdapter_Invoke_m5466,
	GetterAdapter_BeginInvoke_m5467,
	GetterAdapter_EndInvoke_m5468,
	MonoProperty__ctor_m5469,
	MonoProperty_CachePropertyInfo_m5470,
	MonoProperty_get_Attributes_m5471,
	MonoProperty_get_CanRead_m5472,
	MonoProperty_get_CanWrite_m5473,
	MonoProperty_get_PropertyType_m5474,
	MonoProperty_get_ReflectedType_m5475,
	MonoProperty_get_DeclaringType_m5476,
	MonoProperty_get_Name_m5477,
	MonoProperty_GetAccessors_m5478,
	MonoProperty_GetGetMethod_m5479,
	MonoProperty_GetIndexParameters_m5480,
	MonoProperty_GetSetMethod_m5481,
	MonoProperty_IsDefined_m5482,
	MonoProperty_GetCustomAttributes_m5483,
	MonoProperty_GetCustomAttributes_m5484,
	MonoProperty_CreateGetterDelegate_m5485,
	MonoProperty_GetValue_m5486,
	MonoProperty_GetValue_m5487,
	MonoProperty_SetValue_m5488,
	MonoProperty_ToString_m5489,
	MonoProperty_GetOptionalCustomModifiers_m5490,
	MonoProperty_GetRequiredCustomModifiers_m5491,
	MonoProperty_GetObjectData_m5492,
	ParameterInfo__ctor_m5493,
	ParameterInfo__ctor_m5494,
	ParameterInfo__ctor_m5495,
	ParameterInfo_ToString_m5496,
	ParameterInfo_get_ParameterType_m5497,
	ParameterInfo_get_Attributes_m5498,
	ParameterInfo_get_IsIn_m5499,
	ParameterInfo_get_IsOptional_m5500,
	ParameterInfo_get_IsOut_m5501,
	ParameterInfo_get_IsRetval_m5502,
	ParameterInfo_get_Member_m5503,
	ParameterInfo_get_Name_m5504,
	ParameterInfo_get_Position_m5505,
	ParameterInfo_GetCustomAttributes_m5506,
	ParameterInfo_IsDefined_m5507,
	ParameterInfo_GetPseudoCustomAttributes_m5508,
	Pointer__ctor_m5509,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m5510,
	PropertyInfo__ctor_m5511,
	PropertyInfo_get_MemberType_m5512,
	PropertyInfo_GetValue_m5513,
	PropertyInfo_SetValue_m5514,
	PropertyInfo_GetOptionalCustomModifiers_m5515,
	PropertyInfo_GetRequiredCustomModifiers_m5516,
	StrongNameKeyPair__ctor_m5517,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m5518,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m5519,
	TargetException__ctor_m5520,
	TargetException__ctor_m5521,
	TargetException__ctor_m5522,
	TargetInvocationException__ctor_m5523,
	TargetInvocationException__ctor_m5524,
	TargetParameterCountException__ctor_m5525,
	TargetParameterCountException__ctor_m5526,
	TargetParameterCountException__ctor_m5527,
	NeutralResourcesLanguageAttribute__ctor_m5528,
	ResourceManager__ctor_m5529,
	ResourceManager__cctor_m5530,
	ResourceInfo__ctor_m5531,
	ResourceCacheItem__ctor_m5532,
	ResourceEnumerator__ctor_m5533,
	ResourceEnumerator_get_Entry_m5534,
	ResourceEnumerator_get_Key_m5535,
	ResourceEnumerator_get_Value_m5536,
	ResourceEnumerator_get_Current_m5537,
	ResourceEnumerator_MoveNext_m5538,
	ResourceEnumerator_Reset_m5539,
	ResourceEnumerator_FillCache_m5540,
	ResourceReader__ctor_m5541,
	ResourceReader__ctor_m5542,
	ResourceReader_System_Collections_IEnumerable_GetEnumerator_m5543,
	ResourceReader_System_IDisposable_Dispose_m5544,
	ResourceReader_ReadHeaders_m5545,
	ResourceReader_CreateResourceInfo_m5546,
	ResourceReader_Read7BitEncodedInt_m5547,
	ResourceReader_ReadValueVer2_m5548,
	ResourceReader_ReadValueVer1_m5549,
	ResourceReader_ReadNonPredefinedValue_m5550,
	ResourceReader_LoadResourceValues_m5551,
	ResourceReader_Close_m5552,
	ResourceReader_GetEnumerator_m5553,
	ResourceReader_Dispose_m5554,
	ResourceSet__ctor_m5555,
	ResourceSet__ctor_m5556,
	ResourceSet__ctor_m5557,
	ResourceSet__ctor_m5558,
	ResourceSet_System_Collections_IEnumerable_GetEnumerator_m5559,
	ResourceSet_Dispose_m5560,
	ResourceSet_Dispose_m5561,
	ResourceSet_GetEnumerator_m5562,
	ResourceSet_GetObjectInternal_m5563,
	ResourceSet_GetObject_m5564,
	ResourceSet_GetObject_m5565,
	ResourceSet_ReadResources_m5566,
	RuntimeResourceSet__ctor_m5567,
	RuntimeResourceSet__ctor_m5568,
	RuntimeResourceSet__ctor_m5569,
	RuntimeResourceSet_GetObject_m5570,
	RuntimeResourceSet_GetObject_m5571,
	RuntimeResourceSet_CloneDisposableObjectIfPossible_m5572,
	SatelliteContractVersionAttribute__ctor_m5573,
	CompilationRelaxationsAttribute__ctor_m5574,
	DefaultDependencyAttribute__ctor_m5575,
	StringFreezingAttribute__ctor_m5576,
	CriticalFinalizerObject__ctor_m5577,
	CriticalFinalizerObject_Finalize_m5578,
	ReliabilityContractAttribute__ctor_m5579,
	ClassInterfaceAttribute__ctor_m5580,
	ComDefaultInterfaceAttribute__ctor_m5581,
	DispIdAttribute__ctor_m5582,
	GCHandle__ctor_m5583,
	GCHandle_get_IsAllocated_m5584,
	GCHandle_get_Target_m5585,
	GCHandle_Alloc_m5586,
	GCHandle_Free_m5587,
	GCHandle_GetTarget_m5588,
	GCHandle_GetTargetHandle_m5589,
	GCHandle_FreeHandle_m5590,
	GCHandle_Equals_m5591,
	GCHandle_GetHashCode_m5592,
	InterfaceTypeAttribute__ctor_m5593,
	Marshal__cctor_m5594,
	Marshal_copy_from_unmanaged_m5595,
	Marshal_Copy_m5596,
	Marshal_Copy_m5597,
	Marshal_ReadByte_m5598,
	Marshal_WriteByte_m5599,
	MarshalDirectiveException__ctor_m5600,
	MarshalDirectiveException__ctor_m5601,
	PreserveSigAttribute__ctor_m5602,
	SafeHandle__ctor_m5603,
	SafeHandle_Close_m5604,
	SafeHandle_DangerousAddRef_m5605,
	SafeHandle_DangerousGetHandle_m5606,
	SafeHandle_DangerousRelease_m5607,
	SafeHandle_Dispose_m5608,
	SafeHandle_Dispose_m5609,
	SafeHandle_SetHandle_m5610,
	SafeHandle_Finalize_m5611,
	TypeLibImportClassAttribute__ctor_m5612,
	TypeLibVersionAttribute__ctor_m5613,
	ActivationServices_get_ConstructionActivator_m5614,
	ActivationServices_CreateProxyFromAttributes_m5615,
	ActivationServices_CreateConstructionCall_m5616,
	ActivationServices_AllocateUninitializedClassInstance_m5617,
	ActivationServices_EnableProxyActivation_m5618,
	AppDomainLevelActivator__ctor_m5619,
	ConstructionLevelActivator__ctor_m5620,
	ContextLevelActivator__ctor_m5621,
	UrlAttribute_get_UrlValue_m5622,
	UrlAttribute_Equals_m5623,
	UrlAttribute_GetHashCode_m5624,
	UrlAttribute_GetPropertiesForNewContext_m5625,
	UrlAttribute_IsContextOK_m5626,
	ChannelInfo__ctor_m5627,
	ChannelInfo_get_ChannelData_m5628,
	ChannelServices__cctor_m5629,
	ChannelServices_CreateClientChannelSinkChain_m5630,
	ChannelServices_CreateClientChannelSinkChain_m5631,
	ChannelServices_RegisterChannel_m5632,
	ChannelServices_RegisterChannel_m5633,
	ChannelServices_RegisterChannelConfig_m5634,
	ChannelServices_CreateProvider_m5635,
	ChannelServices_GetCurrentChannelInfo_m5636,
	CrossAppDomainData__ctor_m5637,
	CrossAppDomainData_get_DomainID_m5638,
	CrossAppDomainData_get_ProcessID_m5639,
	CrossAppDomainChannel__ctor_m5640,
	CrossAppDomainChannel__cctor_m5641,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m5642,
	CrossAppDomainChannel_get_ChannelName_m5643,
	CrossAppDomainChannel_get_ChannelPriority_m5644,
	CrossAppDomainChannel_get_ChannelData_m5645,
	CrossAppDomainChannel_StartListening_m5646,
	CrossAppDomainChannel_CreateMessageSink_m5647,
	CrossAppDomainSink__ctor_m5648,
	CrossAppDomainSink__cctor_m5649,
	CrossAppDomainSink_GetSink_m5650,
	CrossAppDomainSink_get_TargetDomainId_m5651,
	SinkProviderData__ctor_m5652,
	SinkProviderData_get_Children_m5653,
	SinkProviderData_get_Properties_m5654,
	Context__ctor_m5655,
	Context__cctor_m5656,
	Context_Finalize_m5657,
	Context_get_DefaultContext_m5658,
	Context_get_ContextID_m5659,
	Context_get_ContextProperties_m5660,
	Context_get_IsDefaultContext_m5661,
	Context_get_NeedsContextSink_m5662,
	Context_RegisterDynamicProperty_m5663,
	Context_UnregisterDynamicProperty_m5664,
	Context_GetDynamicPropertyCollection_m5665,
	Context_NotifyGlobalDynamicSinks_m5666,
	Context_get_HasGlobalDynamicSinks_m5667,
	Context_NotifyDynamicSinks_m5668,
	Context_get_HasDynamicSinks_m5669,
	Context_get_HasExitSinks_m5670,
	Context_GetProperty_m5671,
	Context_SetProperty_m5672,
	Context_Freeze_m5673,
	Context_ToString_m5674,
	Context_GetServerContextSinkChain_m5675,
	Context_GetClientContextSinkChain_m5676,
	Context_CreateServerObjectSinkChain_m5677,
	Context_CreateEnvoySink_m5678,
	Context_SwitchToContext_m5679,
	Context_CreateNewContext_m5680,
	Context_DoCallBack_m5681,
	Context_AllocateDataSlot_m5682,
	Context_AllocateNamedDataSlot_m5683,
	Context_FreeNamedDataSlot_m5684,
	Context_GetData_m5685,
	Context_GetNamedDataSlot_m5686,
	Context_SetData_m5687,
	DynamicPropertyReg__ctor_m5688,
	DynamicPropertyCollection__ctor_m5689,
	DynamicPropertyCollection_get_HasProperties_m5690,
	DynamicPropertyCollection_RegisterDynamicProperty_m5691,
	DynamicPropertyCollection_UnregisterDynamicProperty_m5692,
	DynamicPropertyCollection_NotifyMessage_m5693,
	DynamicPropertyCollection_FindProperty_m5694,
	ContextCallbackObject__ctor_m5695,
	ContextCallbackObject_DoCallBack_m5696,
	ContextAttribute__ctor_m5697,
	ContextAttribute_get_Name_m5698,
	ContextAttribute_Equals_m5699,
	ContextAttribute_Freeze_m5700,
	ContextAttribute_GetHashCode_m5701,
	ContextAttribute_GetPropertiesForNewContext_m5702,
	ContextAttribute_IsContextOK_m5703,
	ContextAttribute_IsNewContextOK_m5704,
	CrossContextChannel__ctor_m5705,
	SynchronizationAttribute__ctor_m5706,
	SynchronizationAttribute__ctor_m5707,
	SynchronizationAttribute_set_Locked_m5708,
	SynchronizationAttribute_ReleaseLock_m5709,
	SynchronizationAttribute_GetPropertiesForNewContext_m5710,
	SynchronizationAttribute_GetClientContextSink_m5711,
	SynchronizationAttribute_GetServerContextSink_m5712,
	SynchronizationAttribute_IsContextOK_m5713,
	SynchronizationAttribute_ExitContext_m5714,
	SynchronizationAttribute_EnterContext_m5715,
	SynchronizedClientContextSink__ctor_m5716,
	SynchronizedServerContextSink__ctor_m5717,
	LeaseManager__ctor_m5718,
	LeaseManager_SetPollTime_m5719,
	LeaseSink__ctor_m5720,
	LifetimeServices__cctor_m5721,
	LifetimeServices_set_LeaseManagerPollTime_m5722,
	LifetimeServices_set_LeaseTime_m5723,
	LifetimeServices_set_RenewOnCallTime_m5724,
	LifetimeServices_set_SponsorshipTimeout_m5725,
	ArgInfo__ctor_m5726,
	ArgInfo_GetInOutArgs_m5727,
	AsyncResult__ctor_m5728,
	AsyncResult_get_AsyncState_m5729,
	AsyncResult_get_AsyncWaitHandle_m5730,
	AsyncResult_get_CompletedSynchronously_m5731,
	AsyncResult_get_IsCompleted_m5732,
	AsyncResult_get_EndInvokeCalled_m5733,
	AsyncResult_set_EndInvokeCalled_m5734,
	AsyncResult_get_AsyncDelegate_m5735,
	AsyncResult_get_NextSink_m5736,
	AsyncResult_AsyncProcessMessage_m5737,
	AsyncResult_GetReplyMessage_m5738,
	AsyncResult_SetMessageCtrl_m5739,
	AsyncResult_SetCompletedSynchronously_m5740,
	AsyncResult_EndInvoke_m5741,
	AsyncResult_SyncProcessMessage_m5742,
	AsyncResult_get_CallMessage_m5743,
	AsyncResult_set_CallMessage_m5744,
	ClientContextTerminatorSink__ctor_m5745,
	ConstructionCall__ctor_m5746,
	ConstructionCall__ctor_m5747,
	ConstructionCall_InitDictionary_m5748,
	ConstructionCall_set_IsContextOk_m5749,
	ConstructionCall_get_ActivationType_m5750,
	ConstructionCall_get_ActivationTypeName_m5751,
	ConstructionCall_get_Activator_m5752,
	ConstructionCall_set_Activator_m5753,
	ConstructionCall_get_CallSiteActivationAttributes_m5754,
	ConstructionCall_SetActivationAttributes_m5755,
	ConstructionCall_get_ContextProperties_m5756,
	ConstructionCall_InitMethodProperty_m5757,
	ConstructionCall_GetObjectData_m5758,
	ConstructionCall_get_Properties_m5759,
	ConstructionCallDictionary__ctor_m5760,
	ConstructionCallDictionary__cctor_m5761,
	ConstructionCallDictionary_GetMethodProperty_m5762,
	ConstructionCallDictionary_SetMethodProperty_m5763,
	EnvoyTerminatorSink__ctor_m5764,
	EnvoyTerminatorSink__cctor_m5765,
	Header__ctor_m5766,
	Header__ctor_m5767,
	Header__ctor_m5768,
	LogicalCallContext__ctor_m5769,
	LogicalCallContext__ctor_m5770,
	LogicalCallContext_GetObjectData_m5771,
	LogicalCallContext_SetData_m5772,
	LogicalCallContext_Clone_m5773,
	CallContextRemotingData__ctor_m5774,
	CallContextRemotingData_Clone_m5775,
	MethodCall__ctor_m5776,
	MethodCall__ctor_m5777,
	MethodCall__ctor_m5778,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m5779,
	MethodCall_InitMethodProperty_m5780,
	MethodCall_GetObjectData_m5781,
	MethodCall_get_Args_m5782,
	MethodCall_get_LogicalCallContext_m5783,
	MethodCall_get_MethodBase_m5784,
	MethodCall_get_MethodName_m5785,
	MethodCall_get_MethodSignature_m5786,
	MethodCall_get_Properties_m5787,
	MethodCall_InitDictionary_m5788,
	MethodCall_get_TypeName_m5789,
	MethodCall_get_Uri_m5790,
	MethodCall_set_Uri_m5791,
	MethodCall_Init_m5792,
	MethodCall_ResolveMethod_m5793,
	MethodCall_CastTo_m5794,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m5795,
	MethodCall_get_GenericArguments_m5796,
	MethodCallDictionary__ctor_m5797,
	MethodCallDictionary__cctor_m5798,
	DictionaryEnumerator__ctor_m5799,
	DictionaryEnumerator_get_Current_m5800,
	DictionaryEnumerator_MoveNext_m5801,
	DictionaryEnumerator_Reset_m5802,
	DictionaryEnumerator_get_Entry_m5803,
	DictionaryEnumerator_get_Key_m5804,
	DictionaryEnumerator_get_Value_m5805,
	MethodDictionary__ctor_m5806,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m5807,
	MethodDictionary_set_MethodKeys_m5808,
	MethodDictionary_AllocInternalProperties_m5809,
	MethodDictionary_GetInternalProperties_m5810,
	MethodDictionary_IsOverridenKey_m5811,
	MethodDictionary_get_Item_m5812,
	MethodDictionary_set_Item_m5813,
	MethodDictionary_GetMethodProperty_m5814,
	MethodDictionary_SetMethodProperty_m5815,
	MethodDictionary_get_Values_m5816,
	MethodDictionary_Add_m5817,
	MethodDictionary_Contains_m5818,
	MethodDictionary_Remove_m5819,
	MethodDictionary_get_Count_m5820,
	MethodDictionary_get_SyncRoot_m5821,
	MethodDictionary_CopyTo_m5822,
	MethodDictionary_GetEnumerator_m5823,
	MethodReturnDictionary__ctor_m5824,
	MethodReturnDictionary__cctor_m5825,
	MonoMethodMessage_get_Args_m5826,
	MonoMethodMessage_get_LogicalCallContext_m5827,
	MonoMethodMessage_get_MethodBase_m5828,
	MonoMethodMessage_get_MethodName_m5829,
	MonoMethodMessage_get_MethodSignature_m5830,
	MonoMethodMessage_get_TypeName_m5831,
	MonoMethodMessage_get_Uri_m5832,
	MonoMethodMessage_set_Uri_m5833,
	MonoMethodMessage_get_Exception_m5834,
	MonoMethodMessage_get_OutArgCount_m5835,
	MonoMethodMessage_get_OutArgs_m5836,
	MonoMethodMessage_get_ReturnValue_m5837,
	RemotingSurrogate__ctor_m5838,
	RemotingSurrogate_SetObjectData_m5839,
	ObjRefSurrogate__ctor_m5840,
	ObjRefSurrogate_SetObjectData_m5841,
	RemotingSurrogateSelector__ctor_m5842,
	RemotingSurrogateSelector__cctor_m5843,
	RemotingSurrogateSelector_GetSurrogate_m5844,
	ReturnMessage__ctor_m5845,
	ReturnMessage__ctor_m5846,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m5847,
	ReturnMessage_get_Args_m5848,
	ReturnMessage_get_LogicalCallContext_m5849,
	ReturnMessage_get_MethodBase_m5850,
	ReturnMessage_get_MethodName_m5851,
	ReturnMessage_get_MethodSignature_m5852,
	ReturnMessage_get_Properties_m5853,
	ReturnMessage_get_TypeName_m5854,
	ReturnMessage_get_Uri_m5855,
	ReturnMessage_set_Uri_m5856,
	ReturnMessage_get_Exception_m5857,
	ReturnMessage_get_OutArgs_m5858,
	ReturnMessage_get_ReturnValue_m5859,
	ServerContextTerminatorSink__ctor_m5860,
	ServerObjectTerminatorSink__ctor_m5861,
	StackBuilderSink__ctor_m5862,
	SoapAttribute__ctor_m5863,
	SoapAttribute_get_UseAttribute_m5864,
	SoapAttribute_get_XmlNamespace_m5865,
	SoapAttribute_SetReflectionObject_m5866,
	SoapFieldAttribute__ctor_m5867,
	SoapFieldAttribute_get_XmlElementName_m5868,
	SoapFieldAttribute_IsInteropXmlElement_m5869,
	SoapFieldAttribute_SetReflectionObject_m5870,
	SoapMethodAttribute__ctor_m5871,
	SoapMethodAttribute_get_UseAttribute_m5872,
	SoapMethodAttribute_get_XmlNamespace_m5873,
	SoapMethodAttribute_SetReflectionObject_m5874,
	SoapParameterAttribute__ctor_m5875,
	SoapTypeAttribute__ctor_m5876,
	SoapTypeAttribute_get_UseAttribute_m5877,
	SoapTypeAttribute_get_XmlElementName_m5878,
	SoapTypeAttribute_get_XmlNamespace_m5879,
	SoapTypeAttribute_get_XmlTypeName_m5880,
	SoapTypeAttribute_get_XmlTypeNamespace_m5881,
	SoapTypeAttribute_get_IsInteropXmlElement_m5882,
	SoapTypeAttribute_get_IsInteropXmlType_m5883,
	SoapTypeAttribute_SetReflectionObject_m5884,
	ProxyAttribute_CreateInstance_m5885,
	ProxyAttribute_CreateProxy_m5886,
	ProxyAttribute_GetPropertiesForNewContext_m5887,
	ProxyAttribute_IsContextOK_m5888,
	RealProxy__ctor_m5889,
	RealProxy__ctor_m5890,
	RealProxy__ctor_m5891,
	RealProxy_InternalGetProxyType_m5892,
	RealProxy_GetProxiedType_m5893,
	RealProxy_get_ObjectIdentity_m5894,
	RealProxy_InternalGetTransparentProxy_m5895,
	RealProxy_GetTransparentProxy_m5896,
	RealProxy_SetTargetDomain_m5897,
	RemotingProxy__ctor_m5898,
	RemotingProxy__ctor_m5899,
	RemotingProxy__cctor_m5900,
	RemotingProxy_get_TypeName_m5901,
	RemotingProxy_Finalize_m5902,
	TrackingServices__cctor_m5903,
	TrackingServices_NotifyUnmarshaledObject_m5904,
	ActivatedClientTypeEntry__ctor_m5905,
	ActivatedClientTypeEntry_get_ApplicationUrl_m5906,
	ActivatedClientTypeEntry_get_ContextAttributes_m5907,
	ActivatedClientTypeEntry_get_ObjectType_m5908,
	ActivatedClientTypeEntry_ToString_m5909,
	ActivatedServiceTypeEntry__ctor_m5910,
	ActivatedServiceTypeEntry_get_ObjectType_m5911,
	ActivatedServiceTypeEntry_ToString_m5912,
	EnvoyInfo__ctor_m5913,
	EnvoyInfo_get_EnvoySinks_m5914,
	Identity__ctor_m5915,
	Identity_get_ChannelSink_m5916,
	Identity_set_ChannelSink_m5917,
	Identity_get_ObjectUri_m5918,
	Identity_get_Disposed_m5919,
	Identity_set_Disposed_m5920,
	Identity_get_ClientDynamicProperties_m5921,
	Identity_get_ServerDynamicProperties_m5922,
	ClientIdentity__ctor_m5923,
	ClientIdentity_get_ClientProxy_m5924,
	ClientIdentity_set_ClientProxy_m5925,
	ClientIdentity_CreateObjRef_m5926,
	ClientIdentity_get_TargetUri_m5927,
	InternalRemotingServices__cctor_m5928,
	InternalRemotingServices_GetCachedSoapAttribute_m5929,
	ObjRef__ctor_m5930,
	ObjRef__ctor_m5931,
	ObjRef__cctor_m5932,
	ObjRef_get_IsReferenceToWellKnow_m5933,
	ObjRef_get_ChannelInfo_m5934,
	ObjRef_get_EnvoyInfo_m5935,
	ObjRef_set_EnvoyInfo_m5936,
	ObjRef_get_TypeInfo_m5937,
	ObjRef_set_TypeInfo_m5938,
	ObjRef_get_URI_m5939,
	ObjRef_set_URI_m5940,
	ObjRef_GetObjectData_m5941,
	ObjRef_GetRealObject_m5942,
	ObjRef_UpdateChannelInfo_m5943,
	ObjRef_get_ServerType_m5944,
	RemotingConfiguration__cctor_m5945,
	RemotingConfiguration_get_ApplicationName_m5946,
	RemotingConfiguration_set_ApplicationName_m5947,
	RemotingConfiguration_get_ProcessId_m5948,
	RemotingConfiguration_LoadDefaultDelayedChannels_m5949,
	RemotingConfiguration_IsRemotelyActivatedClientType_m5950,
	RemotingConfiguration_RegisterActivatedClientType_m5951,
	RemotingConfiguration_RegisterActivatedServiceType_m5952,
	RemotingConfiguration_RegisterWellKnownClientType_m5953,
	RemotingConfiguration_RegisterWellKnownServiceType_m5954,
	RemotingConfiguration_RegisterChannelTemplate_m5955,
	RemotingConfiguration_RegisterClientProviderTemplate_m5956,
	RemotingConfiguration_RegisterServerProviderTemplate_m5957,
	RemotingConfiguration_RegisterChannels_m5958,
	RemotingConfiguration_RegisterTypes_m5959,
	RemotingConfiguration_SetCustomErrorsMode_m5960,
	ConfigHandler__ctor_m5961,
	ConfigHandler_ValidatePath_m5962,
	ConfigHandler_CheckPath_m5963,
	ConfigHandler_OnStartParsing_m5964,
	ConfigHandler_OnProcessingInstruction_m5965,
	ConfigHandler_OnIgnorableWhitespace_m5966,
	ConfigHandler_OnStartElement_m5967,
	ConfigHandler_ParseElement_m5968,
	ConfigHandler_OnEndElement_m5969,
	ConfigHandler_ReadCustomProviderData_m5970,
	ConfigHandler_ReadLifetine_m5971,
	ConfigHandler_ParseTime_m5972,
	ConfigHandler_ReadChannel_m5973,
	ConfigHandler_ReadProvider_m5974,
	ConfigHandler_ReadClientActivated_m5975,
	ConfigHandler_ReadServiceActivated_m5976,
	ConfigHandler_ReadClientWellKnown_m5977,
	ConfigHandler_ReadServiceWellKnown_m5978,
	ConfigHandler_ReadInteropXml_m5979,
	ConfigHandler_ReadPreload_m5980,
	ConfigHandler_GetNotNull_m5981,
	ConfigHandler_ExtractAssembly_m5982,
	ConfigHandler_OnChars_m5983,
	ConfigHandler_OnEndParsing_m5984,
	ChannelData__ctor_m5985,
	ChannelData_get_ServerProviders_m5986,
	ChannelData_get_ClientProviders_m5987,
	ChannelData_get_CustomProperties_m5988,
	ChannelData_CopyFrom_m5989,
	ProviderData__ctor_m5990,
	ProviderData_CopyFrom_m5991,
	FormatterData__ctor_m5992,
	RemotingException__ctor_m5993,
	RemotingException__ctor_m5994,
	RemotingException__ctor_m5995,
	RemotingException__ctor_m5996,
	RemotingServices__cctor_m5997,
	RemotingServices_GetVirtualMethod_m5998,
	RemotingServices_IsTransparentProxy_m5999,
	RemotingServices_GetServerTypeForUri_m6000,
	RemotingServices_Unmarshal_m6001,
	RemotingServices_Unmarshal_m6002,
	RemotingServices_GetRealProxy_m6003,
	RemotingServices_GetMethodBaseFromMethodMessage_m6004,
	RemotingServices_GetMethodBaseFromName_m6005,
	RemotingServices_FindInterfaceMethod_m6006,
	RemotingServices_CreateClientProxy_m6007,
	RemotingServices_CreateClientProxy_m6008,
	RemotingServices_CreateClientProxyForContextBound_m6009,
	RemotingServices_GetIdentityForUri_m6010,
	RemotingServices_RemoveAppNameFromUri_m6011,
	RemotingServices_GetOrCreateClientIdentity_m6012,
	RemotingServices_GetClientChannelSinkChain_m6013,
	RemotingServices_CreateWellKnownServerIdentity_m6014,
	RemotingServices_RegisterServerIdentity_m6015,
	RemotingServices_GetProxyForRemoteObject_m6016,
	RemotingServices_GetRemoteObject_m6017,
	RemotingServices_RegisterInternalChannels_m6018,
	RemotingServices_DisposeIdentity_m6019,
	RemotingServices_GetNormalizedUri_m6020,
	ServerIdentity__ctor_m6021,
	ServerIdentity_get_ObjectType_m6022,
	ServerIdentity_CreateObjRef_m6023,
	ClientActivatedIdentity_GetServerObject_m6024,
	SingletonIdentity__ctor_m6025,
	SingleCallIdentity__ctor_m6026,
	TypeInfo__ctor_m6027,
	SoapServices__cctor_m6028,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m6029,
	SoapServices_get_XmlNsForClrTypeWithNs_m6030,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m6031,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m6032,
	SoapServices_GetNameKey_m6033,
	SoapServices_GetAssemblyName_m6034,
	SoapServices_GetXmlElementForInteropType_m6035,
	SoapServices_GetXmlNamespaceForMethodCall_m6036,
	SoapServices_GetXmlNamespaceForMethodResponse_m6037,
	SoapServices_GetXmlTypeForInteropType_m6038,
	SoapServices_PreLoad_m6039,
	SoapServices_PreLoad_m6040,
	SoapServices_RegisterInteropXmlElement_m6041,
	SoapServices_RegisterInteropXmlType_m6042,
	SoapServices_EncodeNs_m6043,
	TypeEntry__ctor_m6044,
	TypeEntry_get_AssemblyName_m6045,
	TypeEntry_set_AssemblyName_m6046,
	TypeEntry_get_TypeName_m6047,
	TypeEntry_set_TypeName_m6048,
	TypeInfo__ctor_m6049,
	TypeInfo_get_TypeName_m6050,
	WellKnownClientTypeEntry__ctor_m6051,
	WellKnownClientTypeEntry_get_ApplicationUrl_m6052,
	WellKnownClientTypeEntry_get_ObjectType_m6053,
	WellKnownClientTypeEntry_get_ObjectUrl_m6054,
	WellKnownClientTypeEntry_ToString_m6055,
	WellKnownServiceTypeEntry__ctor_m6056,
	WellKnownServiceTypeEntry_get_Mode_m6057,
	WellKnownServiceTypeEntry_get_ObjectType_m6058,
	WellKnownServiceTypeEntry_get_ObjectUri_m6059,
	WellKnownServiceTypeEntry_ToString_m6060,
	BinaryCommon__cctor_m6061,
	BinaryCommon_IsPrimitive_m6062,
	BinaryCommon_GetTypeFromCode_m6063,
	BinaryCommon_SwapBytes_m6064,
	BinaryFormatter__ctor_m6065,
	BinaryFormatter__ctor_m6066,
	BinaryFormatter_get_DefaultSurrogateSelector_m6067,
	BinaryFormatter_set_AssemblyFormat_m6068,
	BinaryFormatter_get_Binder_m6069,
	BinaryFormatter_get_Context_m6070,
	BinaryFormatter_get_SurrogateSelector_m6071,
	BinaryFormatter_get_FilterLevel_m6072,
	BinaryFormatter_Deserialize_m6073,
	BinaryFormatter_NoCheckDeserialize_m6074,
	BinaryFormatter_ReadBinaryHeader_m6075,
	MessageFormatter_ReadMethodCall_m6076,
	MessageFormatter_ReadMethodResponse_m6077,
	TypeMetadata__ctor_m6078,
	ArrayNullFiller__ctor_m6079,
	ObjectReader__ctor_m6080,
	ObjectReader_ReadObjectGraph_m6081,
	ObjectReader_ReadObjectGraph_m6082,
	ObjectReader_ReadNextObject_m6083,
	ObjectReader_ReadNextObject_m6084,
	ObjectReader_get_CurrentObject_m6085,
	ObjectReader_ReadObject_m6086,
	ObjectReader_ReadAssembly_m6087,
	ObjectReader_ReadObjectInstance_m6088,
	ObjectReader_ReadRefTypeObjectInstance_m6089,
	ObjectReader_ReadObjectContent_m6090,
	ObjectReader_RegisterObject_m6091,
	ObjectReader_ReadStringIntance_m6092,
	ObjectReader_ReadGenericArray_m6093,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m6094,
	ObjectReader_ReadArrayOfPrimitiveType_m6095,
	ObjectReader_BlockRead_m6096,
	ObjectReader_ReadArrayOfObject_m6097,
	ObjectReader_ReadArrayOfString_m6098,
	ObjectReader_ReadSimpleArray_m6099,
	ObjectReader_ReadTypeMetadata_m6100,
	ObjectReader_ReadValue_m6101,
	ObjectReader_SetObjectValue_m6102,
	ObjectReader_RecordFixup_m6103,
	ObjectReader_GetDeserializationType_m6104,
	ObjectReader_ReadType_m6105,
	ObjectReader_ReadPrimitiveTypeValue_m6106,
	FormatterConverter__ctor_m6107,
	FormatterConverter_Convert_m6108,
	FormatterConverter_ToBoolean_m6109,
	FormatterConverter_ToInt16_m6110,
	FormatterConverter_ToInt32_m6111,
	FormatterConverter_ToInt64_m6112,
	FormatterConverter_ToString_m6113,
	FormatterServices_GetUninitializedObject_m6114,
	FormatterServices_GetSafeUninitializedObject_m6115,
	ObjectManager__ctor_m6116,
	ObjectManager_DoFixups_m6117,
	ObjectManager_GetObjectRecord_m6118,
	ObjectManager_GetObject_m6119,
	ObjectManager_RaiseDeserializationEvent_m6120,
	ObjectManager_RaiseOnDeserializingEvent_m6121,
	ObjectManager_RaiseOnDeserializedEvent_m6122,
	ObjectManager_AddFixup_m6123,
	ObjectManager_RecordArrayElementFixup_m6124,
	ObjectManager_RecordArrayElementFixup_m6125,
	ObjectManager_RecordDelayedFixup_m6126,
	ObjectManager_RecordFixup_m6127,
	ObjectManager_RegisterObjectInternal_m6128,
	ObjectManager_RegisterObject_m6129,
	BaseFixupRecord__ctor_m6130,
	BaseFixupRecord_DoFixup_m6131,
	ArrayFixupRecord__ctor_m6132,
	ArrayFixupRecord_FixupImpl_m6133,
	MultiArrayFixupRecord__ctor_m6134,
	MultiArrayFixupRecord_FixupImpl_m6135,
	FixupRecord__ctor_m6136,
	FixupRecord_FixupImpl_m6137,
	DelayedFixupRecord__ctor_m6138,
	DelayedFixupRecord_FixupImpl_m6139,
	ObjectRecord__ctor_m6140,
	ObjectRecord_SetMemberValue_m6141,
	ObjectRecord_SetArrayValue_m6142,
	ObjectRecord_SetMemberValue_m6143,
	ObjectRecord_get_IsInstanceReady_m6144,
	ObjectRecord_get_IsUnsolvedObjectReference_m6145,
	ObjectRecord_get_IsRegistered_m6146,
	ObjectRecord_DoFixups_m6147,
	ObjectRecord_RemoveFixup_m6148,
	ObjectRecord_UnchainFixup_m6149,
	ObjectRecord_ChainFixup_m6150,
	ObjectRecord_LoadData_m6151,
	ObjectRecord_get_HasPendingFixups_m6152,
	SerializationBinder__ctor_m6153,
	CallbackHandler__ctor_m6154,
	CallbackHandler_Invoke_m6155,
	CallbackHandler_BeginInvoke_m6156,
	CallbackHandler_EndInvoke_m6157,
	SerializationCallbacks__ctor_m6158,
	SerializationCallbacks__cctor_m6159,
	SerializationCallbacks_get_HasDeserializedCallbacks_m6160,
	SerializationCallbacks_GetMethodsByAttribute_m6161,
	SerializationCallbacks_Invoke_m6162,
	SerializationCallbacks_RaiseOnDeserializing_m6163,
	SerializationCallbacks_RaiseOnDeserialized_m6164,
	SerializationCallbacks_GetSerializationCallbacks_m6165,
	SerializationEntry__ctor_m6166,
	SerializationEntry_get_Name_m6167,
	SerializationEntry_get_Value_m6168,
	SerializationException__ctor_m6169,
	SerializationException__ctor_m1574,
	SerializationException__ctor_m6170,
	SerializationInfo__ctor_m6171,
	SerializationInfo_AddValue_m1570,
	SerializationInfo_GetValue_m1573,
	SerializationInfo_SetType_m6172,
	SerializationInfo_GetEnumerator_m6173,
	SerializationInfo_AddValue_m6174,
	SerializationInfo_AddValue_m1572,
	SerializationInfo_AddValue_m1571,
	SerializationInfo_AddValue_m6175,
	SerializationInfo_AddValue_m6176,
	SerializationInfo_AddValue_m1586,
	SerializationInfo_AddValue_m6177,
	SerializationInfo_AddValue_m1585,
	SerializationInfo_GetBoolean_m1575,
	SerializationInfo_GetInt16_m6178,
	SerializationInfo_GetInt32_m1584,
	SerializationInfo_GetInt64_m1583,
	SerializationInfo_GetString_m1582,
	SerializationInfoEnumerator__ctor_m6179,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m6180,
	SerializationInfoEnumerator_get_Current_m6181,
	SerializationInfoEnumerator_get_Name_m6182,
	SerializationInfoEnumerator_get_Value_m6183,
	SerializationInfoEnumerator_MoveNext_m6184,
	SerializationInfoEnumerator_Reset_m6185,
	StreamingContext__ctor_m6186,
	StreamingContext__ctor_m6187,
	StreamingContext_get_State_m6188,
	StreamingContext_Equals_m6189,
	StreamingContext_GetHashCode_m6190,
	X509Certificate__ctor_m6191,
	X509Certificate__ctor_m2715,
	X509Certificate__ctor_m1649,
	X509Certificate__ctor_m6192,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6193,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m6194,
	X509Certificate_tostr_m6195,
	X509Certificate_Equals_m6196,
	X509Certificate_GetCertHash_m6197,
	X509Certificate_GetCertHashString_m1654,
	X509Certificate_GetEffectiveDateString_m6198,
	X509Certificate_GetExpirationDateString_m6199,
	X509Certificate_GetHashCode_m6200,
	X509Certificate_GetIssuerName_m6201,
	X509Certificate_GetName_m6202,
	X509Certificate_GetPublicKey_m6203,
	X509Certificate_GetRawCertData_m6204,
	X509Certificate_ToString_m6205,
	X509Certificate_ToString_m1668,
	X509Certificate_get_Issuer_m1671,
	X509Certificate_get_Subject_m1670,
	X509Certificate_Equals_m6206,
	X509Certificate_Import_m1665,
	X509Certificate_Reset_m1667,
	AsymmetricAlgorithm__ctor_m6207,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m6208,
	AsymmetricAlgorithm_get_KeySize_m2663,
	AsymmetricAlgorithm_set_KeySize_m2662,
	AsymmetricAlgorithm_Clear_m2721,
	AsymmetricAlgorithm_GetNamedParam_m6209,
	AsymmetricKeyExchangeFormatter__ctor_m6210,
	AsymmetricSignatureDeformatter__ctor_m2710,
	AsymmetricSignatureFormatter__ctor_m2711,
	Base64Constants__cctor_m6211,
	CryptoConfig__cctor_m6212,
	CryptoConfig_Initialize_m6213,
	CryptoConfig_CreateFromName_m1673,
	CryptoConfig_CreateFromName_m1700,
	CryptoConfig_MapNameToOID_m2656,
	CryptoConfig_EncodeOID_m1675,
	CryptoConfig_EncodeLongNumber_m6214,
	CryptographicException__ctor_m6215,
	CryptographicException__ctor_m1628,
	CryptographicException__ctor_m1632,
	CryptographicException__ctor_m1801,
	CryptographicException__ctor_m6216,
	CryptographicUnexpectedOperationException__ctor_m6217,
	CryptographicUnexpectedOperationException__ctor_m2693,
	CryptographicUnexpectedOperationException__ctor_m6218,
	CspParameters__ctor_m2658,
	CspParameters__ctor_m6219,
	CspParameters__ctor_m6220,
	CspParameters__ctor_m6221,
	CspParameters_get_Flags_m6222,
	CspParameters_set_Flags_m2659,
	DES__ctor_m6223,
	DES__cctor_m6224,
	DES_Create_m2694,
	DES_Create_m6225,
	DES_IsWeakKey_m6226,
	DES_IsSemiWeakKey_m6227,
	DES_get_Key_m6228,
	DES_set_Key_m6229,
	DESTransform__ctor_m6230,
	DESTransform__cctor_m6231,
	DESTransform_CipherFunct_m6232,
	DESTransform_Permutation_m6233,
	DESTransform_BSwap_m6234,
	DESTransform_SetKey_m6235,
	DESTransform_ProcessBlock_m6236,
	DESTransform_ECB_m6237,
	DESTransform_GetStrongKey_m6238,
	DESCryptoServiceProvider__ctor_m6239,
	DESCryptoServiceProvider_CreateDecryptor_m6240,
	DESCryptoServiceProvider_CreateEncryptor_m6241,
	DESCryptoServiceProvider_GenerateIV_m6242,
	DESCryptoServiceProvider_GenerateKey_m6243,
	DSA__ctor_m6244,
	DSA_Create_m1623,
	DSA_Create_m6245,
	DSA_ZeroizePrivateKey_m6246,
	DSA_FromXmlString_m6247,
	DSA_ToXmlString_m6248,
	DSACryptoServiceProvider__ctor_m6249,
	DSACryptoServiceProvider__ctor_m1633,
	DSACryptoServiceProvider__ctor_m6250,
	DSACryptoServiceProvider__cctor_m6251,
	DSACryptoServiceProvider_Finalize_m6252,
	DSACryptoServiceProvider_get_KeySize_m6253,
	DSACryptoServiceProvider_get_PublicOnly_m1622,
	DSACryptoServiceProvider_ExportParameters_m6254,
	DSACryptoServiceProvider_ImportParameters_m6255,
	DSACryptoServiceProvider_CreateSignature_m6256,
	DSACryptoServiceProvider_VerifySignature_m6257,
	DSACryptoServiceProvider_Dispose_m6258,
	DSACryptoServiceProvider_OnKeyGenerated_m6259,
	DSASignatureDeformatter__ctor_m6260,
	DSASignatureDeformatter__ctor_m2678,
	DSASignatureDeformatter_SetHashAlgorithm_m6261,
	DSASignatureDeformatter_SetKey_m6262,
	DSASignatureDeformatter_VerifySignature_m6263,
	DSASignatureFormatter__ctor_m6264,
	DSASignatureFormatter_CreateSignature_m6265,
	DSASignatureFormatter_SetHashAlgorithm_m6266,
	DSASignatureFormatter_SetKey_m6267,
	HMAC__ctor_m6268,
	HMAC_get_BlockSizeValue_m6269,
	HMAC_set_BlockSizeValue_m6270,
	HMAC_set_HashName_m6271,
	HMAC_get_Key_m6272,
	HMAC_set_Key_m6273,
	HMAC_get_Block_m6274,
	HMAC_KeySetup_m6275,
	HMAC_Dispose_m6276,
	HMAC_HashCore_m6277,
	HMAC_HashFinal_m6278,
	HMAC_Initialize_m6279,
	HMAC_Create_m2671,
	HMAC_Create_m6280,
	HMACMD5__ctor_m6281,
	HMACMD5__ctor_m6282,
	HMACRIPEMD160__ctor_m6283,
	HMACRIPEMD160__ctor_m6284,
	HMACSHA1__ctor_m6285,
	HMACSHA1__ctor_m6286,
	HMACSHA256__ctor_m6287,
	HMACSHA256__ctor_m6288,
	HMACSHA384__ctor_m6289,
	HMACSHA384__ctor_m6290,
	HMACSHA384__cctor_m6291,
	HMACSHA384_set_ProduceLegacyHmacValues_m6292,
	HMACSHA512__ctor_m6293,
	HMACSHA512__ctor_m6294,
	HMACSHA512__cctor_m6295,
	HMACSHA512_set_ProduceLegacyHmacValues_m6296,
	HashAlgorithm__ctor_m2655,
	HashAlgorithm_System_IDisposable_Dispose_m6297,
	HashAlgorithm_get_CanReuseTransform_m6298,
	HashAlgorithm_ComputeHash_m1710,
	HashAlgorithm_ComputeHash_m2666,
	HashAlgorithm_Create_m2665,
	HashAlgorithm_get_Hash_m6299,
	HashAlgorithm_get_HashSize_m6300,
	HashAlgorithm_Dispose_m6301,
	HashAlgorithm_TransformBlock_m6302,
	HashAlgorithm_TransformFinalBlock_m6303,
	KeySizes__ctor_m1803,
	KeySizes_get_MaxSize_m6304,
	KeySizes_get_MinSize_m6305,
	KeySizes_get_SkipSize_m6306,
	KeySizes_IsLegal_m6307,
	KeySizes_IsLegalKeySize_m6308,
	KeyedHashAlgorithm__ctor_m2692,
	KeyedHashAlgorithm_Finalize_m6309,
	KeyedHashAlgorithm_get_Key_m6310,
	KeyedHashAlgorithm_set_Key_m6311,
	KeyedHashAlgorithm_Dispose_m6312,
	KeyedHashAlgorithm_ZeroizeKey_m6313,
	MACTripleDES__ctor_m6314,
	MACTripleDES_Setup_m6315,
	MACTripleDES_Finalize_m6316,
	MACTripleDES_Dispose_m6317,
	MACTripleDES_Initialize_m6318,
	MACTripleDES_HashCore_m6319,
	MACTripleDES_HashFinal_m6320,
	MD5__ctor_m6321,
	MD5_Create_m2675,
	MD5_Create_m6322,
	MD5CryptoServiceProvider__ctor_m6323,
	MD5CryptoServiceProvider__cctor_m6324,
	MD5CryptoServiceProvider_Finalize_m6325,
	MD5CryptoServiceProvider_Dispose_m6326,
	MD5CryptoServiceProvider_HashCore_m6327,
	MD5CryptoServiceProvider_HashFinal_m6328,
	MD5CryptoServiceProvider_Initialize_m6329,
	MD5CryptoServiceProvider_ProcessBlock_m6330,
	MD5CryptoServiceProvider_ProcessFinalBlock_m6331,
	MD5CryptoServiceProvider_AddLength_m6332,
	RC2__ctor_m6333,
	RC2_Create_m2695,
	RC2_Create_m6334,
	RC2_get_EffectiveKeySize_m6335,
	RC2_get_KeySize_m6336,
	RC2_set_KeySize_m6337,
	RC2CryptoServiceProvider__ctor_m6338,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m6339,
	RC2CryptoServiceProvider_CreateDecryptor_m6340,
	RC2CryptoServiceProvider_CreateEncryptor_m6341,
	RC2CryptoServiceProvider_GenerateIV_m6342,
	RC2CryptoServiceProvider_GenerateKey_m6343,
	RC2Transform__ctor_m6344,
	RC2Transform__cctor_m6345,
	RC2Transform_ECB_m6346,
	RIPEMD160__ctor_m6347,
	RIPEMD160Managed__ctor_m6348,
	RIPEMD160Managed_Initialize_m6349,
	RIPEMD160Managed_HashCore_m6350,
	RIPEMD160Managed_HashFinal_m6351,
	RIPEMD160Managed_Finalize_m6352,
	RIPEMD160Managed_ProcessBlock_m6353,
	RIPEMD160Managed_Compress_m6354,
	RIPEMD160Managed_CompressFinal_m6355,
	RIPEMD160Managed_ROL_m6356,
	RIPEMD160Managed_F_m6357,
	RIPEMD160Managed_G_m6358,
	RIPEMD160Managed_H_m6359,
	RIPEMD160Managed_I_m6360,
	RIPEMD160Managed_J_m6361,
	RIPEMD160Managed_FF_m6362,
	RIPEMD160Managed_GG_m6363,
	RIPEMD160Managed_HH_m6364,
	RIPEMD160Managed_II_m6365,
	RIPEMD160Managed_JJ_m6366,
	RIPEMD160Managed_FFF_m6367,
	RIPEMD160Managed_GGG_m6368,
	RIPEMD160Managed_HHH_m6369,
	RIPEMD160Managed_III_m6370,
	RIPEMD160Managed_JJJ_m6371,
	RNGCryptoServiceProvider__ctor_m6372,
	RNGCryptoServiceProvider__cctor_m6373,
	RNGCryptoServiceProvider_Check_m6374,
	RNGCryptoServiceProvider_RngOpen_m6375,
	RNGCryptoServiceProvider_RngInitialize_m6376,
	RNGCryptoServiceProvider_RngGetBytes_m6377,
	RNGCryptoServiceProvider_RngClose_m6378,
	RNGCryptoServiceProvider_GetBytes_m6379,
	RNGCryptoServiceProvider_GetNonZeroBytes_m6380,
	RNGCryptoServiceProvider_Finalize_m6381,
	RSA__ctor_m2661,
	RSA_Create_m1620,
	RSA_Create_m6382,
	RSA_ZeroizePrivateKey_m6383,
	RSA_FromXmlString_m6384,
	RSA_ToXmlString_m6385,
	RSACryptoServiceProvider__ctor_m6386,
	RSACryptoServiceProvider__ctor_m2660,
	RSACryptoServiceProvider__ctor_m1634,
	RSACryptoServiceProvider__cctor_m6387,
	RSACryptoServiceProvider_Common_m6388,
	RSACryptoServiceProvider_Finalize_m6389,
	RSACryptoServiceProvider_get_KeySize_m6390,
	RSACryptoServiceProvider_get_PublicOnly_m1618,
	RSACryptoServiceProvider_DecryptValue_m6391,
	RSACryptoServiceProvider_EncryptValue_m6392,
	RSACryptoServiceProvider_ExportParameters_m6393,
	RSACryptoServiceProvider_ImportParameters_m6394,
	RSACryptoServiceProvider_Dispose_m6395,
	RSACryptoServiceProvider_OnKeyGenerated_m6396,
	RSAPKCS1KeyExchangeFormatter__ctor_m2720,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m6397,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m6398,
	RSAPKCS1SignatureDeformatter__ctor_m6399,
	RSAPKCS1SignatureDeformatter__ctor_m2679,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m6400,
	RSAPKCS1SignatureDeformatter_SetKey_m6401,
	RSAPKCS1SignatureDeformatter_VerifySignature_m6402,
	RSAPKCS1SignatureFormatter__ctor_m6403,
	RSAPKCS1SignatureFormatter_CreateSignature_m6404,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m6405,
	RSAPKCS1SignatureFormatter_SetKey_m6406,
	RandomNumberGenerator__ctor_m6407,
	RandomNumberGenerator_Create_m1796,
	RandomNumberGenerator_Create_m6408,
	Rijndael__ctor_m6409,
	Rijndael_Create_m2697,
	Rijndael_Create_m6410,
	RijndaelManaged__ctor_m6411,
	RijndaelManaged_GenerateIV_m6412,
	RijndaelManaged_GenerateKey_m6413,
	RijndaelManaged_CreateDecryptor_m6414,
	RijndaelManaged_CreateEncryptor_m6415,
	RijndaelTransform__ctor_m6416,
	RijndaelTransform__cctor_m6417,
	RijndaelTransform_Clear_m6418,
	RijndaelTransform_ECB_m6419,
	RijndaelTransform_SubByte_m6420,
	RijndaelTransform_Encrypt128_m6421,
	RijndaelTransform_Encrypt192_m6422,
	RijndaelTransform_Encrypt256_m6423,
	RijndaelTransform_Decrypt128_m6424,
	RijndaelTransform_Decrypt192_m6425,
	RijndaelTransform_Decrypt256_m6426,
	RijndaelManagedTransform__ctor_m6427,
	RijndaelManagedTransform_System_IDisposable_Dispose_m6428,
	RijndaelManagedTransform_get_CanReuseTransform_m6429,
	RijndaelManagedTransform_TransformBlock_m6430,
	RijndaelManagedTransform_TransformFinalBlock_m6431,
	SHA1__ctor_m6432,
	SHA1_Create_m1709,
	SHA1_Create_m6433,
	SHA1Internal__ctor_m6434,
	SHA1Internal_HashCore_m6435,
	SHA1Internal_HashFinal_m6436,
	SHA1Internal_Initialize_m6437,
	SHA1Internal_ProcessBlock_m6438,
	SHA1Internal_InitialiseBuff_m6439,
	SHA1Internal_FillBuff_m6440,
	SHA1Internal_ProcessFinalBlock_m6441,
	SHA1Internal_AddLength_m6442,
	SHA1CryptoServiceProvider__ctor_m6443,
	SHA1CryptoServiceProvider_Finalize_m6444,
	SHA1CryptoServiceProvider_Dispose_m6445,
	SHA1CryptoServiceProvider_HashCore_m6446,
	SHA1CryptoServiceProvider_HashFinal_m6447,
	SHA1CryptoServiceProvider_Initialize_m6448,
	SHA1Managed__ctor_m6449,
	SHA1Managed_HashCore_m6450,
	SHA1Managed_HashFinal_m6451,
	SHA1Managed_Initialize_m6452,
	SHA256__ctor_m6453,
	SHA256_Create_m2676,
	SHA256_Create_m6454,
	SHA256Managed__ctor_m6455,
	SHA256Managed_HashCore_m6456,
	SHA256Managed_HashFinal_m6457,
	SHA256Managed_Initialize_m6458,
	SHA256Managed_ProcessBlock_m6459,
	SHA256Managed_ProcessFinalBlock_m6460,
	SHA256Managed_AddLength_m6461,
	SHA384__ctor_m6462,
	SHA384Managed__ctor_m6463,
	SHA384Managed_Initialize_m6464,
	SHA384Managed_Initialize_m6465,
	SHA384Managed_HashCore_m6466,
	SHA384Managed_HashFinal_m6467,
	SHA384Managed_update_m6468,
	SHA384Managed_processWord_m6469,
	SHA384Managed_unpackWord_m6470,
	SHA384Managed_adjustByteCounts_m6471,
	SHA384Managed_processLength_m6472,
	SHA384Managed_processBlock_m6473,
	SHA512__ctor_m6474,
	SHA512Managed__ctor_m6475,
	SHA512Managed_Initialize_m6476,
	SHA512Managed_Initialize_m6477,
	SHA512Managed_HashCore_m6478,
	SHA512Managed_HashFinal_m6479,
	SHA512Managed_update_m6480,
	SHA512Managed_processWord_m6481,
	SHA512Managed_unpackWord_m6482,
	SHA512Managed_adjustByteCounts_m6483,
	SHA512Managed_processLength_m6484,
	SHA512Managed_processBlock_m6485,
	SHA512Managed_rotateRight_m6486,
	SHA512Managed_Ch_m6487,
	SHA512Managed_Maj_m6488,
	SHA512Managed_Sum0_m6489,
	SHA512Managed_Sum1_m6490,
	SHA512Managed_Sigma0_m6491,
	SHA512Managed_Sigma1_m6492,
	SHAConstants__cctor_m6493,
	SignatureDescription__ctor_m6494,
	SignatureDescription_set_DeformatterAlgorithm_m6495,
	SignatureDescription_set_DigestAlgorithm_m6496,
	SignatureDescription_set_FormatterAlgorithm_m6497,
	SignatureDescription_set_KeyAlgorithm_m6498,
	DSASignatureDescription__ctor_m6499,
	RSAPKCS1SHA1SignatureDescription__ctor_m6500,
	SymmetricAlgorithm__ctor_m1802,
	SymmetricAlgorithm_System_IDisposable_Dispose_m6501,
	SymmetricAlgorithm_Finalize_m2653,
	SymmetricAlgorithm_Clear_m2670,
	SymmetricAlgorithm_Dispose_m1810,
	SymmetricAlgorithm_get_BlockSize_m6502,
	SymmetricAlgorithm_set_BlockSize_m6503,
	SymmetricAlgorithm_get_FeedbackSize_m6504,
	SymmetricAlgorithm_get_IV_m1804,
	SymmetricAlgorithm_set_IV_m1805,
	SymmetricAlgorithm_get_Key_m1806,
	SymmetricAlgorithm_set_Key_m1807,
	SymmetricAlgorithm_get_KeySize_m1808,
	SymmetricAlgorithm_set_KeySize_m1809,
	SymmetricAlgorithm_get_LegalKeySizes_m6505,
	SymmetricAlgorithm_get_Mode_m6506,
	SymmetricAlgorithm_set_Mode_m6507,
	SymmetricAlgorithm_get_Padding_m6508,
	SymmetricAlgorithm_set_Padding_m6509,
	SymmetricAlgorithm_CreateDecryptor_m6510,
	SymmetricAlgorithm_CreateEncryptor_m6511,
	SymmetricAlgorithm_Create_m2669,
	ToBase64Transform_System_IDisposable_Dispose_m6512,
	ToBase64Transform_Finalize_m6513,
	ToBase64Transform_get_CanReuseTransform_m6514,
	ToBase64Transform_get_InputBlockSize_m6515,
	ToBase64Transform_get_OutputBlockSize_m6516,
	ToBase64Transform_Dispose_m6517,
	ToBase64Transform_TransformBlock_m6518,
	ToBase64Transform_InternalTransformBlock_m6519,
	ToBase64Transform_TransformFinalBlock_m6520,
	ToBase64Transform_InternalTransformFinalBlock_m6521,
	TripleDES__ctor_m6522,
	TripleDES_get_Key_m6523,
	TripleDES_set_Key_m6524,
	TripleDES_IsWeakKey_m6525,
	TripleDES_Create_m2696,
	TripleDES_Create_m6526,
	TripleDESCryptoServiceProvider__ctor_m6527,
	TripleDESCryptoServiceProvider_GenerateIV_m6528,
	TripleDESCryptoServiceProvider_GenerateKey_m6529,
	TripleDESCryptoServiceProvider_CreateDecryptor_m6530,
	TripleDESCryptoServiceProvider_CreateEncryptor_m6531,
	TripleDESTransform__ctor_m6532,
	TripleDESTransform_ECB_m6533,
	TripleDESTransform_GetStrongKey_m6534,
	SecurityPermission__ctor_m6535,
	SecurityPermission_set_Flags_m6536,
	SecurityPermission_IsUnrestricted_m6537,
	SecurityPermission_IsSubsetOf_m6538,
	SecurityPermission_ToXml_m6539,
	SecurityPermission_IsEmpty_m6540,
	SecurityPermission_Cast_m6541,
	StrongNamePublicKeyBlob_Equals_m6542,
	StrongNamePublicKeyBlob_GetHashCode_m6543,
	StrongNamePublicKeyBlob_ToString_m6544,
	ApplicationTrust__ctor_m6545,
	EvidenceEnumerator__ctor_m6546,
	EvidenceEnumerator_MoveNext_m6547,
	EvidenceEnumerator_Reset_m6548,
	EvidenceEnumerator_get_Current_m6549,
	Evidence__ctor_m6550,
	Evidence_get_Count_m6551,
	Evidence_get_SyncRoot_m6552,
	Evidence_get_HostEvidenceList_m6553,
	Evidence_get_AssemblyEvidenceList_m6554,
	Evidence_CopyTo_m6555,
	Evidence_Equals_m6556,
	Evidence_GetEnumerator_m6557,
	Evidence_GetHashCode_m6558,
	Hash__ctor_m6559,
	Hash__ctor_m6560,
	Hash_GetObjectData_m6561,
	Hash_ToString_m6562,
	Hash_GetData_m6563,
	StrongName_get_Name_m6564,
	StrongName_get_PublicKey_m6565,
	StrongName_get_Version_m6566,
	StrongName_Equals_m6567,
	StrongName_GetHashCode_m6568,
	StrongName_ToString_m6569,
	WindowsIdentity__ctor_m6570,
	WindowsIdentity__cctor_m6571,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m6572,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m6573,
	WindowsIdentity_Dispose_m6574,
	WindowsIdentity_GetCurrentToken_m6575,
	WindowsIdentity_GetTokenName_m6576,
	CodeAccessPermission__ctor_m6577,
	CodeAccessPermission_Equals_m6578,
	CodeAccessPermission_GetHashCode_m6579,
	CodeAccessPermission_ToString_m6580,
	CodeAccessPermission_Element_m6581,
	CodeAccessPermission_ThrowInvalidPermission_m6582,
	PermissionSet__ctor_m6583,
	PermissionSet__ctor_m6584,
	PermissionSet_set_DeclarativeSecurity_m6585,
	PermissionSet_CreateFromBinaryFormat_m6586,
	SecurityContext__ctor_m6587,
	SecurityContext__ctor_m6588,
	SecurityContext_Capture_m6589,
	SecurityContext_get_FlowSuppressed_m6590,
	SecurityContext_get_CompressedStack_m6591,
	SecurityAttribute__ctor_m6592,
	SecurityAttribute_get_Name_m6593,
	SecurityAttribute_get_Value_m6594,
	SecurityElement__ctor_m6595,
	SecurityElement__ctor_m6596,
	SecurityElement__cctor_m6597,
	SecurityElement_get_Children_m6598,
	SecurityElement_get_Tag_m6599,
	SecurityElement_set_Text_m6600,
	SecurityElement_AddAttribute_m6601,
	SecurityElement_AddChild_m6602,
	SecurityElement_Escape_m6603,
	SecurityElement_Unescape_m6604,
	SecurityElement_IsValidAttributeName_m6605,
	SecurityElement_IsValidAttributeValue_m6606,
	SecurityElement_IsValidTag_m6607,
	SecurityElement_IsValidText_m6608,
	SecurityElement_SearchForChildByTag_m6609,
	SecurityElement_ToString_m6610,
	SecurityElement_ToXml_m6611,
	SecurityElement_GetAttribute_m6612,
	SecurityException__ctor_m6613,
	SecurityException__ctor_m6614,
	SecurityException__ctor_m6615,
	SecurityException_get_Demanded_m6616,
	SecurityException_get_FirstPermissionThatFailed_m6617,
	SecurityException_get_PermissionState_m6618,
	SecurityException_get_PermissionType_m6619,
	SecurityException_get_GrantedSet_m6620,
	SecurityException_get_RefusedSet_m6621,
	SecurityException_GetObjectData_m6622,
	SecurityException_ToString_m6623,
	SecurityFrame__ctor_m6624,
	SecurityFrame__GetSecurityStack_m6625,
	SecurityFrame_InitFromRuntimeFrame_m6626,
	SecurityFrame_get_Assembly_m6627,
	SecurityFrame_get_Domain_m6628,
	SecurityFrame_ToString_m6629,
	SecurityFrame_GetStack_m6630,
	SecurityManager__cctor_m6631,
	SecurityManager_get_SecurityEnabled_m6632,
	SecurityManager_Decode_m6633,
	SecurityManager_Decode_m6634,
	SecuritySafeCriticalAttribute__ctor_m6635,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m6636,
	UnverifiableCodeAttribute__ctor_m6637,
	ASCIIEncoding__ctor_m6638,
	ASCIIEncoding_GetByteCount_m6639,
	ASCIIEncoding_GetByteCount_m6640,
	ASCIIEncoding_GetBytes_m6641,
	ASCIIEncoding_GetBytes_m6642,
	ASCIIEncoding_GetBytes_m6643,
	ASCIIEncoding_GetBytes_m6644,
	ASCIIEncoding_GetCharCount_m6645,
	ASCIIEncoding_GetChars_m6646,
	ASCIIEncoding_GetChars_m6647,
	ASCIIEncoding_GetMaxByteCount_m6648,
	ASCIIEncoding_GetMaxCharCount_m6649,
	ASCIIEncoding_GetString_m6650,
	ASCIIEncoding_GetBytes_m6651,
	ASCIIEncoding_GetByteCount_m6652,
	ASCIIEncoding_GetDecoder_m6653,
	Decoder__ctor_m6654,
	Decoder_set_Fallback_m6655,
	Decoder_get_FallbackBuffer_m6656,
	DecoderExceptionFallback__ctor_m6657,
	DecoderExceptionFallback_CreateFallbackBuffer_m6658,
	DecoderExceptionFallback_Equals_m6659,
	DecoderExceptionFallback_GetHashCode_m6660,
	DecoderExceptionFallbackBuffer__ctor_m6661,
	DecoderExceptionFallbackBuffer_get_Remaining_m6662,
	DecoderExceptionFallbackBuffer_Fallback_m6663,
	DecoderExceptionFallbackBuffer_GetNextChar_m6664,
	DecoderFallback__ctor_m6665,
	DecoderFallback__cctor_m6666,
	DecoderFallback_get_ExceptionFallback_m6667,
	DecoderFallback_get_ReplacementFallback_m6668,
	DecoderFallback_get_StandardSafeFallback_m6669,
	DecoderFallbackBuffer__ctor_m6670,
	DecoderFallbackBuffer_Reset_m6671,
	DecoderFallbackException__ctor_m6672,
	DecoderFallbackException__ctor_m6673,
	DecoderFallbackException__ctor_m6674,
	DecoderReplacementFallback__ctor_m6675,
	DecoderReplacementFallback__ctor_m6676,
	DecoderReplacementFallback_get_DefaultString_m6677,
	DecoderReplacementFallback_CreateFallbackBuffer_m6678,
	DecoderReplacementFallback_Equals_m6679,
	DecoderReplacementFallback_GetHashCode_m6680,
	DecoderReplacementFallbackBuffer__ctor_m6681,
	DecoderReplacementFallbackBuffer_get_Remaining_m6682,
	DecoderReplacementFallbackBuffer_Fallback_m6683,
	DecoderReplacementFallbackBuffer_GetNextChar_m6684,
	DecoderReplacementFallbackBuffer_Reset_m6685,
	EncoderExceptionFallback__ctor_m6686,
	EncoderExceptionFallback_CreateFallbackBuffer_m6687,
	EncoderExceptionFallback_Equals_m6688,
	EncoderExceptionFallback_GetHashCode_m6689,
	EncoderExceptionFallbackBuffer__ctor_m6690,
	EncoderExceptionFallbackBuffer_get_Remaining_m6691,
	EncoderExceptionFallbackBuffer_Fallback_m6692,
	EncoderExceptionFallbackBuffer_Fallback_m6693,
	EncoderExceptionFallbackBuffer_GetNextChar_m6694,
	EncoderFallback__ctor_m6695,
	EncoderFallback__cctor_m6696,
	EncoderFallback_get_ExceptionFallback_m6697,
	EncoderFallback_get_ReplacementFallback_m6698,
	EncoderFallback_get_StandardSafeFallback_m6699,
	EncoderFallbackBuffer__ctor_m6700,
	EncoderFallbackException__ctor_m6701,
	EncoderFallbackException__ctor_m6702,
	EncoderFallbackException__ctor_m6703,
	EncoderFallbackException__ctor_m6704,
	EncoderReplacementFallback__ctor_m6705,
	EncoderReplacementFallback__ctor_m6706,
	EncoderReplacementFallback_get_DefaultString_m6707,
	EncoderReplacementFallback_CreateFallbackBuffer_m6708,
	EncoderReplacementFallback_Equals_m6709,
	EncoderReplacementFallback_GetHashCode_m6710,
	EncoderReplacementFallbackBuffer__ctor_m6711,
	EncoderReplacementFallbackBuffer_get_Remaining_m6712,
	EncoderReplacementFallbackBuffer_Fallback_m6713,
	EncoderReplacementFallbackBuffer_Fallback_m6714,
	EncoderReplacementFallbackBuffer_Fallback_m6715,
	EncoderReplacementFallbackBuffer_GetNextChar_m6716,
	ForwardingDecoder__ctor_m6717,
	ForwardingDecoder_GetChars_m6718,
	Encoding__ctor_m6719,
	Encoding__ctor_m6720,
	Encoding__cctor_m6721,
	Encoding___m6722,
	Encoding_get_IsReadOnly_m6723,
	Encoding_get_DecoderFallback_m6724,
	Encoding_set_DecoderFallback_m6725,
	Encoding_get_EncoderFallback_m6726,
	Encoding_SetFallbackInternal_m6727,
	Encoding_Equals_m6728,
	Encoding_GetByteCount_m6729,
	Encoding_GetByteCount_m6730,
	Encoding_GetBytes_m6731,
	Encoding_GetBytes_m6732,
	Encoding_GetBytes_m6733,
	Encoding_GetBytes_m6734,
	Encoding_GetChars_m6735,
	Encoding_GetDecoder_m6736,
	Encoding_InvokeI18N_m6737,
	Encoding_GetEncoding_m6738,
	Encoding_Clone_m6739,
	Encoding_GetEncoding_m6740,
	Encoding_GetHashCode_m6741,
	Encoding_GetPreamble_m6742,
	Encoding_GetString_m6743,
	Encoding_GetString_m6744,
	Encoding_get_ASCII_m1712,
	Encoding_get_BigEndianUnicode_m2667,
	Encoding_InternalCodePage_m6745,
	Encoding_get_Default_m6746,
	Encoding_get_ISOLatin1_m6747,
	Encoding_get_UTF7_m2672,
	Encoding_get_UTF8_m1658,
	Encoding_get_UTF8Unmarked_m6748,
	Encoding_get_UTF8UnmarkedUnsafe_m6749,
	Encoding_get_Unicode_m6750,
	Encoding_get_UTF32_m6751,
	Encoding_get_BigEndianUTF32_m6752,
	Encoding_GetByteCount_m6753,
	Encoding_GetBytes_m6754,
	Latin1Encoding__ctor_m6755,
	Latin1Encoding_GetByteCount_m6756,
	Latin1Encoding_GetByteCount_m6757,
	Latin1Encoding_GetBytes_m6758,
	Latin1Encoding_GetBytes_m6759,
	Latin1Encoding_GetBytes_m6760,
	Latin1Encoding_GetBytes_m6761,
	Latin1Encoding_GetCharCount_m6762,
	Latin1Encoding_GetChars_m6763,
	Latin1Encoding_GetMaxByteCount_m6764,
	Latin1Encoding_GetMaxCharCount_m6765,
	Latin1Encoding_GetString_m6766,
	Latin1Encoding_GetString_m6767,
	StringBuilder__ctor_m6768,
	StringBuilder__ctor_m6769,
	StringBuilder__ctor_m1601,
	StringBuilder__ctor_m636,
	StringBuilder__ctor_m1637,
	StringBuilder__ctor_m1578,
	StringBuilder__ctor_m6770,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m6771,
	StringBuilder_get_Capacity_m6772,
	StringBuilder_set_Capacity_m6773,
	StringBuilder_get_Length_m1702,
	StringBuilder_set_Length_m1746,
	StringBuilder_get_Chars_m6774,
	StringBuilder_set_Chars_m6775,
	StringBuilder_ToString_m640,
	StringBuilder_ToString_m6776,
	StringBuilder_Remove_m6777,
	StringBuilder_Replace_m6778,
	StringBuilder_Replace_m6779,
	StringBuilder_Append_m639,
	StringBuilder_Append_m1648,
	StringBuilder_Append_m1603,
	StringBuilder_Append_m1580,
	StringBuilder_Append_m1579,
	StringBuilder_Append_m6780,
	StringBuilder_Append_m6781,
	StringBuilder_Append_m1715,
	StringBuilder_AppendFormat_m2647,
	StringBuilder_AppendFormat_m6782,
	StringBuilder_AppendFormat_m1602,
	StringBuilder_AppendFormat_m1669,
	StringBuilder_AppendFormat_m1672,
	StringBuilder_Insert_m6783,
	StringBuilder_Insert_m6784,
	StringBuilder_Insert_m6785,
	StringBuilder_InternalEnsureCapacity_m6786,
	UTF32Decoder__ctor_m6787,
	UTF32Decoder_GetChars_m6788,
	UTF32Encoding__ctor_m6789,
	UTF32Encoding__ctor_m6790,
	UTF32Encoding__ctor_m6791,
	UTF32Encoding_GetByteCount_m6792,
	UTF32Encoding_GetBytes_m6793,
	UTF32Encoding_GetCharCount_m6794,
	UTF32Encoding_GetChars_m6795,
	UTF32Encoding_GetMaxByteCount_m6796,
	UTF32Encoding_GetMaxCharCount_m6797,
	UTF32Encoding_GetDecoder_m6798,
	UTF32Encoding_GetPreamble_m6799,
	UTF32Encoding_Equals_m6800,
	UTF32Encoding_GetHashCode_m6801,
	UTF32Encoding_GetByteCount_m6802,
	UTF32Encoding_GetByteCount_m6803,
	UTF32Encoding_GetBytes_m6804,
	UTF32Encoding_GetBytes_m6805,
	UTF32Encoding_GetString_m6806,
	UTF7Decoder__ctor_m6807,
	UTF7Decoder_GetChars_m6808,
	UTF7Encoding__ctor_m6809,
	UTF7Encoding__ctor_m6810,
	UTF7Encoding__cctor_m6811,
	UTF7Encoding_GetHashCode_m6812,
	UTF7Encoding_Equals_m6813,
	UTF7Encoding_InternalGetByteCount_m6814,
	UTF7Encoding_GetByteCount_m6815,
	UTF7Encoding_InternalGetBytes_m6816,
	UTF7Encoding_GetBytes_m6817,
	UTF7Encoding_InternalGetCharCount_m6818,
	UTF7Encoding_GetCharCount_m6819,
	UTF7Encoding_InternalGetChars_m6820,
	UTF7Encoding_GetChars_m6821,
	UTF7Encoding_GetMaxByteCount_m6822,
	UTF7Encoding_GetMaxCharCount_m6823,
	UTF7Encoding_GetDecoder_m6824,
	UTF7Encoding_GetByteCount_m6825,
	UTF7Encoding_GetByteCount_m6826,
	UTF7Encoding_GetBytes_m6827,
	UTF7Encoding_GetBytes_m6828,
	UTF7Encoding_GetString_m6829,
	UTF8Decoder__ctor_m6830,
	UTF8Decoder_GetChars_m6831,
	UTF8Encoding__ctor_m6832,
	UTF8Encoding__ctor_m6833,
	UTF8Encoding__ctor_m6834,
	UTF8Encoding_InternalGetByteCount_m6835,
	UTF8Encoding_InternalGetByteCount_m6836,
	UTF8Encoding_GetByteCount_m6837,
	UTF8Encoding_GetByteCount_m6838,
	UTF8Encoding_InternalGetBytes_m6839,
	UTF8Encoding_InternalGetBytes_m6840,
	UTF8Encoding_GetBytes_m6841,
	UTF8Encoding_GetBytes_m6842,
	UTF8Encoding_GetBytes_m6843,
	UTF8Encoding_InternalGetCharCount_m6844,
	UTF8Encoding_InternalGetCharCount_m6845,
	UTF8Encoding_Fallback_m6846,
	UTF8Encoding_Fallback_m6847,
	UTF8Encoding_GetCharCount_m6848,
	UTF8Encoding_InternalGetChars_m6849,
	UTF8Encoding_InternalGetChars_m6850,
	UTF8Encoding_GetChars_m6851,
	UTF8Encoding_GetMaxByteCount_m6852,
	UTF8Encoding_GetMaxCharCount_m6853,
	UTF8Encoding_GetDecoder_m6854,
	UTF8Encoding_GetPreamble_m6855,
	UTF8Encoding_Equals_m6856,
	UTF8Encoding_GetHashCode_m6857,
	UTF8Encoding_GetByteCount_m6858,
	UTF8Encoding_GetString_m6859,
	UnicodeDecoder__ctor_m6860,
	UnicodeDecoder_GetChars_m6861,
	UnicodeEncoding__ctor_m6862,
	UnicodeEncoding__ctor_m6863,
	UnicodeEncoding__ctor_m6864,
	UnicodeEncoding_GetByteCount_m6865,
	UnicodeEncoding_GetByteCount_m6866,
	UnicodeEncoding_GetByteCount_m6867,
	UnicodeEncoding_GetBytes_m6868,
	UnicodeEncoding_GetBytes_m6869,
	UnicodeEncoding_GetBytes_m6870,
	UnicodeEncoding_GetBytesInternal_m6871,
	UnicodeEncoding_GetCharCount_m6872,
	UnicodeEncoding_GetChars_m6873,
	UnicodeEncoding_GetString_m6874,
	UnicodeEncoding_GetCharsInternal_m6875,
	UnicodeEncoding_GetMaxByteCount_m6876,
	UnicodeEncoding_GetMaxCharCount_m6877,
	UnicodeEncoding_GetDecoder_m6878,
	UnicodeEncoding_GetPreamble_m6879,
	UnicodeEncoding_Equals_m6880,
	UnicodeEncoding_GetHashCode_m6881,
	UnicodeEncoding_CopyChars_m6882,
	CompressedStack__ctor_m6883,
	CompressedStack__ctor_m6884,
	CompressedStack_CreateCopy_m6885,
	CompressedStack_Capture_m6886,
	CompressedStack_GetObjectData_m6887,
	CompressedStack_IsEmpty_m6888,
	EventWaitHandle__ctor_m6889,
	EventWaitHandle_IsManualReset_m6890,
	EventWaitHandle_Reset_m2707,
	EventWaitHandle_Set_m2705,
	ExecutionContext__ctor_m6891,
	ExecutionContext__ctor_m6892,
	ExecutionContext__ctor_m6893,
	ExecutionContext_Capture_m6894,
	ExecutionContext_GetObjectData_m6895,
	ExecutionContext_get_SecurityContext_m6896,
	ExecutionContext_set_SecurityContext_m6897,
	ExecutionContext_get_FlowSuppressed_m6898,
	ExecutionContext_IsFlowSuppressed_m6899,
	Interlocked_CompareExchange_m6900,
	ManualResetEvent__ctor_m2704,
	Monitor_Enter_m1589,
	Monitor_Exit_m1590,
	Monitor_Monitor_pulse_m6901,
	Monitor_Monitor_test_synchronised_m6902,
	Monitor_Pulse_m6903,
	Monitor_Monitor_wait_m6904,
	Monitor_Wait_m6905,
	Mutex__ctor_m6906,
	Mutex_CreateMutex_internal_m6907,
	Mutex_ReleaseMutex_internal_m6908,
	Mutex_ReleaseMutex_m6909,
	NativeEventCalls_CreateEvent_internal_m6910,
	NativeEventCalls_SetEvent_internal_m6911,
	NativeEventCalls_ResetEvent_internal_m6912,
	NativeEventCalls_CloseEvent_internal_m6913,
	SynchronizationLockException__ctor_m6914,
	SynchronizationLockException__ctor_m6915,
	SynchronizationLockException__ctor_m6916,
	Thread__ctor_m6917,
	Thread__cctor_m6918,
	Thread_get_CurrentContext_m6919,
	Thread_CurrentThread_internal_m6920,
	Thread_get_CurrentThread_m6921,
	Thread_FreeLocalSlotValues_m6922,
	Thread_GetDomainID_m6923,
	Thread_Thread_internal_m6924,
	Thread_Thread_init_m6925,
	Thread_GetCachedCurrentCulture_m6926,
	Thread_GetSerializedCurrentCulture_m6927,
	Thread_SetCachedCurrentCulture_m6928,
	Thread_GetCachedCurrentUICulture_m6929,
	Thread_GetSerializedCurrentUICulture_m6930,
	Thread_SetCachedCurrentUICulture_m6931,
	Thread_get_CurrentCulture_m6932,
	Thread_get_CurrentUICulture_m6933,
	Thread_set_IsBackground_m6934,
	Thread_SetName_internal_m6935,
	Thread_set_Name_m6936,
	Thread_Start_m6937,
	Thread_Thread_free_internal_m6938,
	Thread_Finalize_m6939,
	Thread_SetState_m6940,
	Thread_ClrState_m6941,
	Thread_GetNewManagedId_m6942,
	Thread_GetNewManagedId_internal_m6943,
	Thread_get_ExecutionContext_m6944,
	Thread_get_ManagedThreadId_m6945,
	Thread_GetHashCode_m6946,
	Thread_GetCompressedStack_m6947,
	ThreadAbortException__ctor_m6948,
	ThreadAbortException__ctor_m6949,
	ThreadInterruptedException__ctor_m6950,
	ThreadInterruptedException__ctor_m6951,
	ThreadPool_QueueUserWorkItem_m6952,
	ThreadStateException__ctor_m6953,
	ThreadStateException__ctor_m6954,
	TimerComparer__ctor_m6955,
	TimerComparer_Compare_m6956,
	Scheduler__ctor_m6957,
	Scheduler__cctor_m6958,
	Scheduler_get_Instance_m6959,
	Scheduler_Remove_m6960,
	Scheduler_Change_m6961,
	Scheduler_Add_m6962,
	Scheduler_InternalRemove_m6963,
	Scheduler_SchedulerThread_m6964,
	Scheduler_ShrinkIfNeeded_m6965,
	Timer__cctor_m6966,
	Timer_Change_m6967,
	Timer_Dispose_m6968,
	Timer_Change_m6969,
	WaitHandle__ctor_m6970,
	WaitHandle__cctor_m6971,
	WaitHandle_System_IDisposable_Dispose_m6972,
	WaitHandle_get_Handle_m6973,
	WaitHandle_set_Handle_m6974,
	WaitHandle_WaitOne_internal_m6975,
	WaitHandle_Dispose_m6976,
	WaitHandle_WaitOne_m6977,
	WaitHandle_WaitOne_m6978,
	WaitHandle_CheckDisposed_m6979,
	WaitHandle_Finalize_m6980,
	AccessViolationException__ctor_m6981,
	AccessViolationException__ctor_m6982,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m6983,
	ActivationContext_Finalize_m6984,
	ActivationContext_Dispose_m6985,
	ActivationContext_Dispose_m6986,
	Activator_CreateInstance_m6987,
	Activator_CreateInstance_m6988,
	Activator_CreateInstance_m6989,
	Activator_CreateInstance_m6990,
	Activator_CreateInstance_m1617,
	Activator_CheckType_m6991,
	Activator_CheckAbstractType_m6992,
	Activator_CreateInstanceInternal_m6993,
	AppDomain_add_UnhandledException_m592,
	AppDomain_remove_UnhandledException_m6994,
	AppDomain_getFriendlyName_m6995,
	AppDomain_getCurDomain_m6996,
	AppDomain_get_CurrentDomain_m590,
	AppDomain_LoadAssembly_m6997,
	AppDomain_Load_m6998,
	AppDomain_Load_m6999,
	AppDomain_InternalSetContext_m7000,
	AppDomain_InternalGetContext_m7001,
	AppDomain_InternalGetDefaultContext_m7002,
	AppDomain_InternalGetProcessGuid_m7003,
	AppDomain_GetProcessGuid_m7004,
	AppDomain_ToString_m7005,
	AppDomain_DoTypeResolve_m7006,
	AppDomainSetup__ctor_m7007,
	ApplicationException__ctor_m7008,
	ApplicationException__ctor_m7009,
	ApplicationException__ctor_m7010,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m7011,
	ApplicationIdentity_ToString_m7012,
	ArgumentException__ctor_m7013,
	ArgumentException__ctor_m587,
	ArgumentException__ctor_m1681,
	ArgumentException__ctor_m1563,
	ArgumentException__ctor_m7014,
	ArgumentException__ctor_m7015,
	ArgumentException_get_ParamName_m7016,
	ArgumentException_get_Message_m7017,
	ArgumentException_GetObjectData_m7018,
	ArgumentNullException__ctor_m7019,
	ArgumentNullException__ctor_m1565,
	ArgumentNullException__ctor_m1559,
	ArgumentNullException__ctor_m7020,
	ArgumentOutOfRangeException__ctor_m1714,
	ArgumentOutOfRangeException__ctor_m1566,
	ArgumentOutOfRangeException__ctor_m1560,
	ArgumentOutOfRangeException__ctor_m7021,
	ArgumentOutOfRangeException__ctor_m7022,
	ArgumentOutOfRangeException_get_Message_m7023,
	ArgumentOutOfRangeException_GetObjectData_m7024,
	ArithmeticException__ctor_m7025,
	ArithmeticException__ctor_m2646,
	ArithmeticException__ctor_m7026,
	ArrayTypeMismatchException__ctor_m7027,
	ArrayTypeMismatchException__ctor_m7028,
	ArrayTypeMismatchException__ctor_m7029,
	BitConverter__cctor_m7030,
	BitConverter_AmILittleEndian_m7031,
	BitConverter_DoubleWordsAreSwapped_m7032,
	BitConverter_DoubleToInt64Bits_m7033,
	BitConverter_GetBytes_m7034,
	BitConverter_GetBytes_m7035,
	BitConverter_PutBytes_m7036,
	BitConverter_ToInt64_m7037,
	BitConverter_ToString_m2702,
	BitConverter_ToString_m7038,
	Buffer_ByteLength_m7039,
	Buffer_BlockCopy_m1625,
	Buffer_ByteLengthInternal_m7040,
	Buffer_BlockCopyInternal_m7041,
	CharEnumerator__ctor_m7042,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m7043,
	CharEnumerator_System_IDisposable_Dispose_m7044,
	CharEnumerator_get_Current_m7045,
	CharEnumerator_Clone_m7046,
	CharEnumerator_MoveNext_m7047,
	CharEnumerator_Reset_m7048,
	Console__cctor_m7049,
	Console_SetEncodings_m7050,
	Console_get_Error_m1737,
	Console_Open_m7051,
	Console_OpenStandardError_m7052,
	Console_OpenStandardInput_m7053,
	Console_OpenStandardOutput_m7054,
	ContextBoundObject__ctor_m7055,
	Convert__cctor_m7056,
	Convert_InternalFromBase64String_m7057,
	Convert_FromBase64String_m2680,
	Convert_ToBase64String_m2664,
	Convert_ToBase64String_m7058,
	Convert_ToBoolean_m7059,
	Convert_ToBoolean_m7060,
	Convert_ToBoolean_m7061,
	Convert_ToBoolean_m7062,
	Convert_ToBoolean_m7063,
	Convert_ToBoolean_m7064,
	Convert_ToBoolean_m7065,
	Convert_ToBoolean_m7066,
	Convert_ToBoolean_m7067,
	Convert_ToBoolean_m7068,
	Convert_ToBoolean_m7069,
	Convert_ToBoolean_m7070,
	Convert_ToBoolean_m7071,
	Convert_ToBoolean_m7072,
	Convert_ToByte_m7073,
	Convert_ToByte_m7074,
	Convert_ToByte_m7075,
	Convert_ToByte_m7076,
	Convert_ToByte_m7077,
	Convert_ToByte_m7078,
	Convert_ToByte_m7079,
	Convert_ToByte_m7080,
	Convert_ToByte_m7081,
	Convert_ToByte_m7082,
	Convert_ToByte_m7083,
	Convert_ToByte_m7084,
	Convert_ToByte_m7085,
	Convert_ToByte_m7086,
	Convert_ToByte_m7087,
	Convert_ToChar_m2681,
	Convert_ToChar_m7088,
	Convert_ToChar_m7089,
	Convert_ToChar_m7090,
	Convert_ToChar_m7091,
	Convert_ToChar_m7092,
	Convert_ToChar_m7093,
	Convert_ToChar_m7094,
	Convert_ToChar_m7095,
	Convert_ToChar_m7096,
	Convert_ToChar_m7097,
	Convert_ToDateTime_m7098,
	Convert_ToDateTime_m7099,
	Convert_ToDateTime_m7100,
	Convert_ToDateTime_m7101,
	Convert_ToDateTime_m7102,
	Convert_ToDateTime_m7103,
	Convert_ToDateTime_m7104,
	Convert_ToDateTime_m7105,
	Convert_ToDateTime_m7106,
	Convert_ToDateTime_m7107,
	Convert_ToDecimal_m7108,
	Convert_ToDecimal_m7109,
	Convert_ToDecimal_m7110,
	Convert_ToDecimal_m7111,
	Convert_ToDecimal_m7112,
	Convert_ToDecimal_m7113,
	Convert_ToDecimal_m7114,
	Convert_ToDecimal_m7115,
	Convert_ToDecimal_m7116,
	Convert_ToDecimal_m7117,
	Convert_ToDecimal_m7118,
	Convert_ToDecimal_m7119,
	Convert_ToDecimal_m7120,
	Convert_ToDouble_m7121,
	Convert_ToDouble_m7122,
	Convert_ToDouble_m7123,
	Convert_ToDouble_m7124,
	Convert_ToDouble_m7125,
	Convert_ToDouble_m7126,
	Convert_ToDouble_m7127,
	Convert_ToDouble_m7128,
	Convert_ToDouble_m7129,
	Convert_ToDouble_m7130,
	Convert_ToDouble_m7131,
	Convert_ToDouble_m7132,
	Convert_ToDouble_m7133,
	Convert_ToDouble_m7134,
	Convert_ToInt16_m7135,
	Convert_ToInt16_m7136,
	Convert_ToInt16_m7137,
	Convert_ToInt16_m7138,
	Convert_ToInt16_m7139,
	Convert_ToInt16_m7140,
	Convert_ToInt16_m7141,
	Convert_ToInt16_m7142,
	Convert_ToInt16_m7143,
	Convert_ToInt16_m7144,
	Convert_ToInt16_m2651,
	Convert_ToInt16_m7145,
	Convert_ToInt16_m7146,
	Convert_ToInt16_m7147,
	Convert_ToInt16_m7148,
	Convert_ToInt16_m7149,
	Convert_ToInt32_m7150,
	Convert_ToInt32_m7151,
	Convert_ToInt32_m7152,
	Convert_ToInt32_m7153,
	Convert_ToInt32_m7154,
	Convert_ToInt32_m7155,
	Convert_ToInt32_m7156,
	Convert_ToInt32_m7157,
	Convert_ToInt32_m7158,
	Convert_ToInt32_m7159,
	Convert_ToInt32_m7160,
	Convert_ToInt32_m7161,
	Convert_ToInt32_m7162,
	Convert_ToInt32_m7163,
	Convert_ToInt32_m2690,
	Convert_ToInt64_m7164,
	Convert_ToInt64_m7165,
	Convert_ToInt64_m7166,
	Convert_ToInt64_m7167,
	Convert_ToInt64_m7168,
	Convert_ToInt64_m7169,
	Convert_ToInt64_m7170,
	Convert_ToInt64_m7171,
	Convert_ToInt64_m7172,
	Convert_ToInt64_m7173,
	Convert_ToInt64_m7174,
	Convert_ToInt64_m7175,
	Convert_ToInt64_m7176,
	Convert_ToInt64_m7177,
	Convert_ToInt64_m7178,
	Convert_ToInt64_m7179,
	Convert_ToInt64_m7180,
	Convert_ToSByte_m7181,
	Convert_ToSByte_m7182,
	Convert_ToSByte_m7183,
	Convert_ToSByte_m7184,
	Convert_ToSByte_m7185,
	Convert_ToSByte_m7186,
	Convert_ToSByte_m7187,
	Convert_ToSByte_m7188,
	Convert_ToSByte_m7189,
	Convert_ToSByte_m7190,
	Convert_ToSByte_m7191,
	Convert_ToSByte_m7192,
	Convert_ToSByte_m7193,
	Convert_ToSByte_m7194,
	Convert_ToSingle_m7195,
	Convert_ToSingle_m7196,
	Convert_ToSingle_m7197,
	Convert_ToSingle_m7198,
	Convert_ToSingle_m7199,
	Convert_ToSingle_m7200,
	Convert_ToSingle_m7201,
	Convert_ToSingle_m7202,
	Convert_ToSingle_m7203,
	Convert_ToSingle_m7204,
	Convert_ToSingle_m7205,
	Convert_ToSingle_m7206,
	Convert_ToSingle_m7207,
	Convert_ToSingle_m7208,
	Convert_ToString_m7209,
	Convert_ToString_m7210,
	Convert_ToUInt16_m7211,
	Convert_ToUInt16_m7212,
	Convert_ToUInt16_m7213,
	Convert_ToUInt16_m7214,
	Convert_ToUInt16_m7215,
	Convert_ToUInt16_m7216,
	Convert_ToUInt16_m7217,
	Convert_ToUInt16_m7218,
	Convert_ToUInt16_m7219,
	Convert_ToUInt16_m7220,
	Convert_ToUInt16_m7221,
	Convert_ToUInt16_m7222,
	Convert_ToUInt16_m7223,
	Convert_ToUInt16_m7224,
	Convert_ToUInt32_m7225,
	Convert_ToUInt32_m7226,
	Convert_ToUInt32_m7227,
	Convert_ToUInt32_m7228,
	Convert_ToUInt32_m7229,
	Convert_ToUInt32_m7230,
	Convert_ToUInt32_m7231,
	Convert_ToUInt32_m7232,
	Convert_ToUInt32_m7233,
	Convert_ToUInt32_m7234,
	Convert_ToUInt32_m7235,
	Convert_ToUInt32_m7236,
	Convert_ToUInt32_m7237,
	Convert_ToUInt32_m7238,
	Convert_ToUInt64_m7239,
	Convert_ToUInt64_m7240,
	Convert_ToUInt64_m7241,
	Convert_ToUInt64_m7242,
	Convert_ToUInt64_m7243,
	Convert_ToUInt64_m7244,
	Convert_ToUInt64_m7245,
	Convert_ToUInt64_m7246,
	Convert_ToUInt64_m7247,
	Convert_ToUInt64_m7248,
	Convert_ToUInt64_m7249,
	Convert_ToUInt64_m7250,
	Convert_ToUInt64_m7251,
	Convert_ToUInt64_m7252,
	Convert_ToUInt64_m7253,
	Convert_ChangeType_m7254,
	Convert_ToType_m7255,
	DBNull__ctor_m7256,
	DBNull__ctor_m7257,
	DBNull__cctor_m7258,
	DBNull_System_IConvertible_ToBoolean_m7259,
	DBNull_System_IConvertible_ToByte_m7260,
	DBNull_System_IConvertible_ToChar_m7261,
	DBNull_System_IConvertible_ToDateTime_m7262,
	DBNull_System_IConvertible_ToDecimal_m7263,
	DBNull_System_IConvertible_ToDouble_m7264,
	DBNull_System_IConvertible_ToInt16_m7265,
	DBNull_System_IConvertible_ToInt32_m7266,
	DBNull_System_IConvertible_ToInt64_m7267,
	DBNull_System_IConvertible_ToSByte_m7268,
	DBNull_System_IConvertible_ToSingle_m7269,
	DBNull_System_IConvertible_ToType_m7270,
	DBNull_System_IConvertible_ToUInt16_m7271,
	DBNull_System_IConvertible_ToUInt32_m7272,
	DBNull_System_IConvertible_ToUInt64_m7273,
	DBNull_GetObjectData_m7274,
	DBNull_ToString_m7275,
	DBNull_ToString_m7276,
	DateTime__ctor_m7277,
	DateTime__ctor_m7278,
	DateTime__ctor_m626,
	DateTime__ctor_m7279,
	DateTime__ctor_m7280,
	DateTime__cctor_m7281,
	DateTime_System_IConvertible_ToBoolean_m7282,
	DateTime_System_IConvertible_ToByte_m7283,
	DateTime_System_IConvertible_ToChar_m7284,
	DateTime_System_IConvertible_ToDateTime_m7285,
	DateTime_System_IConvertible_ToDecimal_m7286,
	DateTime_System_IConvertible_ToDouble_m7287,
	DateTime_System_IConvertible_ToInt16_m7288,
	DateTime_System_IConvertible_ToInt32_m7289,
	DateTime_System_IConvertible_ToInt64_m7290,
	DateTime_System_IConvertible_ToSByte_m7291,
	DateTime_System_IConvertible_ToSingle_m7292,
	DateTime_System_IConvertible_ToType_m7293,
	DateTime_System_IConvertible_ToUInt16_m7294,
	DateTime_System_IConvertible_ToUInt32_m7295,
	DateTime_System_IConvertible_ToUInt64_m7296,
	DateTime_AbsoluteDays_m7297,
	DateTime_FromTicks_m7298,
	DateTime_get_Month_m7299,
	DateTime_get_Day_m7300,
	DateTime_get_DayOfWeek_m7301,
	DateTime_get_Hour_m7302,
	DateTime_get_Minute_m7303,
	DateTime_get_Second_m7304,
	DateTime_GetTimeMonotonic_m7305,
	DateTime_GetNow_m7306,
	DateTime_get_Now_m629,
	DateTime_get_Ticks_m2703,
	DateTime_get_Today_m7307,
	DateTime_get_UtcNow_m2677,
	DateTime_get_Year_m7308,
	DateTime_get_Kind_m7309,
	DateTime_Add_m7310,
	DateTime_AddTicks_m7311,
	DateTime_AddMilliseconds_m1604,
	DateTime_AddSeconds_m627,
	DateTime_Compare_m7312,
	DateTime_CompareTo_m7313,
	DateTime_CompareTo_m7314,
	DateTime_Equals_m7315,
	DateTime_FromBinary_m7316,
	DateTime_SpecifyKind_m7317,
	DateTime_DaysInMonth_m7318,
	DateTime_Equals_m7319,
	DateTime_CheckDateTimeKind_m7320,
	DateTime_GetHashCode_m7321,
	DateTime_IsLeapYear_m7322,
	DateTime_Parse_m7323,
	DateTime_Parse_m7324,
	DateTime_CoreParse_m7325,
	DateTime_YearMonthDayFormats_m7326,
	DateTime__ParseNumber_m7327,
	DateTime__ParseEnum_m7328,
	DateTime__ParseString_m7329,
	DateTime__ParseAmPm_m7330,
	DateTime__ParseTimeSeparator_m7331,
	DateTime__ParseDateSeparator_m7332,
	DateTime_IsLetter_m7333,
	DateTime__DoParse_m7334,
	DateTime_ParseExact_m2652,
	DateTime_ParseExact_m7335,
	DateTime_CheckStyle_m7336,
	DateTime_ParseExact_m7337,
	DateTime_Subtract_m7338,
	DateTime_ToString_m7339,
	DateTime_ToString_m7340,
	DateTime_ToString_m7341,
	DateTime_ToLocalTime_m1651,
	DateTime_ToUniversalTime_m7342,
	DateTime_op_Addition_m7343,
	DateTime_op_Equality_m7344,
	DateTime_op_GreaterThan_m1679,
	DateTime_op_GreaterThanOrEqual_m1605,
	DateTime_op_Inequality_m7345,
	DateTime_op_LessThan_m1678,
	DateTime_op_LessThanOrEqual_m1677,
	DateTime_op_Subtraction_m7346,
	DateTimeOffset__ctor_m7347,
	DateTimeOffset__ctor_m7348,
	DateTimeOffset__ctor_m7349,
	DateTimeOffset__ctor_m7350,
	DateTimeOffset__cctor_m7351,
	DateTimeOffset_System_IComparable_CompareTo_m7352,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m7353,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7354,
	DateTimeOffset_CompareTo_m7355,
	DateTimeOffset_Equals_m7356,
	DateTimeOffset_Equals_m7357,
	DateTimeOffset_GetHashCode_m7358,
	DateTimeOffset_ToString_m7359,
	DateTimeOffset_ToString_m7360,
	DateTimeOffset_get_DateTime_m7361,
	DateTimeOffset_get_Offset_m7362,
	DateTimeOffset_get_UtcDateTime_m7363,
	DateTimeUtils_CountRepeat_m7364,
	DateTimeUtils_ZeroPad_m7365,
	DateTimeUtils_ParseQuotedString_m7366,
	DateTimeUtils_GetStandardPattern_m7367,
	DateTimeUtils_GetStandardPattern_m7368,
	DateTimeUtils_ToString_m7369,
	DateTimeUtils_ToString_m7370,
	DelegateEntry__ctor_m7371,
	DelegateEntry_DeserializeDelegate_m7372,
	DelegateSerializationHolder__ctor_m7373,
	DelegateSerializationHolder_GetDelegateData_m7374,
	DelegateSerializationHolder_GetObjectData_m7375,
	DelegateSerializationHolder_GetRealObject_m7376,
	DivideByZeroException__ctor_m7377,
	DivideByZeroException__ctor_m7378,
	DllNotFoundException__ctor_m7379,
	DllNotFoundException__ctor_m7380,
	EntryPointNotFoundException__ctor_m7381,
	EntryPointNotFoundException__ctor_m7382,
	SByteComparer__ctor_m7383,
	SByteComparer_Compare_m7384,
	SByteComparer_Compare_m7385,
	ShortComparer__ctor_m7386,
	ShortComparer_Compare_m7387,
	ShortComparer_Compare_m7388,
	IntComparer__ctor_m7389,
	IntComparer_Compare_m7390,
	IntComparer_Compare_m7391,
	LongComparer__ctor_m7392,
	LongComparer_Compare_m7393,
	LongComparer_Compare_m7394,
	MonoEnumInfo__ctor_m7395,
	MonoEnumInfo__cctor_m7396,
	MonoEnumInfo_get_enum_info_m7397,
	MonoEnumInfo_get_Cache_m7398,
	MonoEnumInfo_GetInfo_m7399,
	Environment_get_SocketSecurityEnabled_m7400,
	Environment_get_NewLine_m1636,
	Environment_get_Platform_m7401,
	Environment_GetOSVersionString_m7402,
	Environment_get_OSVersion_m7403,
	Environment_internalGetEnvironmentVariable_m7404,
	Environment_GetEnvironmentVariable_m2701,
	Environment_GetWindowsFolderPath_m7405,
	Environment_GetFolderPath_m2687,
	Environment_ReadXdgUserDir_m7406,
	Environment_InternalGetFolderPath_m7407,
	Environment_get_IsRunningOnWindows_m7408,
	Environment_GetMachineConfigPath_m7409,
	Environment_internalGetHome_m7410,
	EventArgs__ctor_m7411,
	EventArgs__cctor_m7412,
	ExecutionEngineException__ctor_m7413,
	ExecutionEngineException__ctor_m7414,
	FieldAccessException__ctor_m7415,
	FieldAccessException__ctor_m7416,
	FieldAccessException__ctor_m7417,
	FlagsAttribute__ctor_m7418,
	FormatException__ctor_m7419,
	FormatException__ctor_m1591,
	FormatException__ctor_m1748,
	GC_SuppressFinalize_m1798,
	Guid__ctor_m7420,
	Guid__ctor_m7421,
	Guid__cctor_m7422,
	Guid_CheckNull_m7423,
	Guid_CheckLength_m7424,
	Guid_CheckArray_m7425,
	Guid_Compare_m7426,
	Guid_CompareTo_m7427,
	Guid_Equals_m7428,
	Guid_CompareTo_m7429,
	Guid_Equals_m7430,
	Guid_GetHashCode_m7431,
	Guid_ToHex_m7432,
	Guid_NewGuid_m7433,
	Guid_AppendInt_m7434,
	Guid_AppendShort_m7435,
	Guid_AppendByte_m7436,
	Guid_BaseToString_m7437,
	Guid_ToString_m7438,
	Guid_ToString_m7439,
	Guid_ToString_m7440,
	IndexOutOfRangeException__ctor_m7441,
	IndexOutOfRangeException__ctor_m612,
	IndexOutOfRangeException__ctor_m7442,
	InvalidCastException__ctor_m7443,
	InvalidCastException__ctor_m7444,
	InvalidCastException__ctor_m7445,
	InvalidOperationException__ctor_m1564,
	InvalidOperationException__ctor_m1557,
	InvalidOperationException__ctor_m7446,
	InvalidOperationException__ctor_m7447,
	LocalDataStoreSlot__ctor_m7448,
	LocalDataStoreSlot__cctor_m7449,
	LocalDataStoreSlot_Finalize_m7450,
	Math_Abs_m7451,
	Math_Abs_m7452,
	Math_Abs_m7453,
	Math_Floor_m7454,
	Math_Max_m2657,
	Math_Min_m1797,
	Math_Round_m7455,
	Math_Round_m7456,
	Math_Pow_m7457,
	Math_Sqrt_m7458,
	MemberAccessException__ctor_m7459,
	MemberAccessException__ctor_m7460,
	MemberAccessException__ctor_m7461,
	MethodAccessException__ctor_m7462,
	MethodAccessException__ctor_m7463,
	MissingFieldException__ctor_m7464,
	MissingFieldException__ctor_m7465,
	MissingFieldException__ctor_m7466,
	MissingFieldException_get_Message_m7467,
	MissingMemberException__ctor_m7468,
	MissingMemberException__ctor_m7469,
	MissingMemberException__ctor_m7470,
	MissingMemberException__ctor_m7471,
	MissingMemberException_GetObjectData_m7472,
	MissingMemberException_get_Message_m7473,
	MissingMethodException__ctor_m7474,
	MissingMethodException__ctor_m7475,
	MissingMethodException__ctor_m7476,
	MissingMethodException__ctor_m7477,
	MissingMethodException_get_Message_m7478,
	MonoAsyncCall__ctor_m7479,
	AttributeInfo__ctor_m7480,
	AttributeInfo_get_Usage_m7481,
	AttributeInfo_get_InheritanceLevel_m7482,
	MonoCustomAttrs__cctor_m7483,
	MonoCustomAttrs_IsUserCattrProvider_m7484,
	MonoCustomAttrs_GetCustomAttributesInternal_m7485,
	MonoCustomAttrs_GetPseudoCustomAttributes_m7486,
	MonoCustomAttrs_GetCustomAttributesBase_m7487,
	MonoCustomAttrs_GetCustomAttribute_m7488,
	MonoCustomAttrs_GetCustomAttributes_m7489,
	MonoCustomAttrs_GetCustomAttributes_m7490,
	MonoCustomAttrs_GetCustomAttributesDataInternal_m7491,
	MonoCustomAttrs_GetCustomAttributesData_m7492,
	MonoCustomAttrs_IsDefined_m7493,
	MonoCustomAttrs_IsDefinedInternal_m7494,
	MonoCustomAttrs_GetBasePropertyDefinition_m7495,
	MonoCustomAttrs_GetBase_m7496,
	MonoCustomAttrs_RetrieveAttributeUsage_m7497,
	MonoTouchAOTHelper__cctor_m7498,
	MonoTypeInfo__ctor_m7499,
	MonoType_get_attributes_m7500,
	MonoType_GetDefaultConstructor_m7501,
	MonoType_GetAttributeFlagsImpl_m7502,
	MonoType_GetConstructorImpl_m7503,
	MonoType_GetConstructors_internal_m7504,
	MonoType_GetConstructors_m7505,
	MonoType_InternalGetEvent_m7506,
	MonoType_GetEvent_m7507,
	MonoType_GetField_m7508,
	MonoType_GetFields_internal_m7509,
	MonoType_GetFields_m7510,
	MonoType_GetInterfaces_m7511,
	MonoType_GetMethodsByName_m7512,
	MonoType_GetMethods_m7513,
	MonoType_GetMethodImpl_m7514,
	MonoType_GetPropertiesByName_m7515,
	MonoType_GetPropertyImpl_m7516,
	MonoType_HasElementTypeImpl_m7517,
	MonoType_IsArrayImpl_m7518,
	MonoType_IsByRefImpl_m7519,
	MonoType_IsPointerImpl_m7520,
	MonoType_IsPrimitiveImpl_m7521,
	MonoType_IsSubclassOf_m7522,
	MonoType_InvokeMember_m7523,
	MonoType_GetElementType_m7524,
	MonoType_get_UnderlyingSystemType_m7525,
	MonoType_get_Assembly_m7526,
	MonoType_get_AssemblyQualifiedName_m7527,
	MonoType_getFullName_m7528,
	MonoType_get_BaseType_m7529,
	MonoType_get_FullName_m7530,
	MonoType_IsDefined_m7531,
	MonoType_GetCustomAttributes_m7532,
	MonoType_GetCustomAttributes_m7533,
	MonoType_get_MemberType_m7534,
	MonoType_get_Name_m7535,
	MonoType_get_Namespace_m7536,
	MonoType_get_Module_m7537,
	MonoType_get_DeclaringType_m7538,
	MonoType_get_ReflectedType_m7539,
	MonoType_get_TypeHandle_m7540,
	MonoType_GetObjectData_m7541,
	MonoType_ToString_m7542,
	MonoType_GetGenericArguments_m7543,
	MonoType_get_ContainsGenericParameters_m7544,
	MonoType_get_IsGenericParameter_m7545,
	MonoType_GetGenericTypeDefinition_m7546,
	MonoType_CheckMethodSecurity_m7547,
	MonoType_ReorderParamArrayArguments_m7548,
	MulticastNotSupportedException__ctor_m7549,
	MulticastNotSupportedException__ctor_m7550,
	MulticastNotSupportedException__ctor_m7551,
	NonSerializedAttribute__ctor_m7552,
	NotImplementedException__ctor_m7553,
	NotImplementedException__ctor_m1616,
	NotImplementedException__ctor_m7554,
	NotSupportedException__ctor_m1615,
	NotSupportedException__ctor_m1576,
	NotSupportedException__ctor_m7555,
	NullReferenceException__ctor_m7556,
	NullReferenceException__ctor_m585,
	NullReferenceException__ctor_m7557,
	CustomInfo__ctor_m7558,
	CustomInfo_GetActiveSection_m7559,
	CustomInfo_Parse_m7560,
	CustomInfo_Format_m7561,
	NumberFormatter__ctor_m7562,
	NumberFormatter__cctor_m7563,
	NumberFormatter_GetFormatterTables_m7564,
	NumberFormatter_GetTenPowerOf_m7565,
	NumberFormatter_InitDecHexDigits_m7566,
	NumberFormatter_InitDecHexDigits_m7567,
	NumberFormatter_InitDecHexDigits_m7568,
	NumberFormatter_FastToDecHex_m7569,
	NumberFormatter_ToDecHex_m7570,
	NumberFormatter_FastDecHexLen_m7571,
	NumberFormatter_DecHexLen_m7572,
	NumberFormatter_DecHexLen_m7573,
	NumberFormatter_ScaleOrder_m7574,
	NumberFormatter_InitialFloatingPrecision_m7575,
	NumberFormatter_ParsePrecision_m7576,
	NumberFormatter_Init_m7577,
	NumberFormatter_InitHex_m7578,
	NumberFormatter_Init_m7579,
	NumberFormatter_Init_m7580,
	NumberFormatter_Init_m7581,
	NumberFormatter_Init_m7582,
	NumberFormatter_Init_m7583,
	NumberFormatter_Init_m7584,
	NumberFormatter_ResetCharBuf_m7585,
	NumberFormatter_Resize_m7586,
	NumberFormatter_Append_m7587,
	NumberFormatter_Append_m7588,
	NumberFormatter_Append_m7589,
	NumberFormatter_GetNumberFormatInstance_m7590,
	NumberFormatter_set_CurrentCulture_m7591,
	NumberFormatter_get_IntegerDigits_m7592,
	NumberFormatter_get_DecimalDigits_m7593,
	NumberFormatter_get_IsFloatingSource_m7594,
	NumberFormatter_get_IsZero_m7595,
	NumberFormatter_get_IsZeroInteger_m7596,
	NumberFormatter_RoundPos_m7597,
	NumberFormatter_RoundDecimal_m7598,
	NumberFormatter_RoundBits_m7599,
	NumberFormatter_RemoveTrailingZeros_m7600,
	NumberFormatter_AddOneToDecHex_m7601,
	NumberFormatter_AddOneToDecHex_m7602,
	NumberFormatter_CountTrailingZeros_m7603,
	NumberFormatter_CountTrailingZeros_m7604,
	NumberFormatter_GetInstance_m7605,
	NumberFormatter_Release_m7606,
	NumberFormatter_SetThreadCurrentCulture_m7607,
	NumberFormatter_NumberToString_m7608,
	NumberFormatter_NumberToString_m7609,
	NumberFormatter_NumberToString_m7610,
	NumberFormatter_NumberToString_m7611,
	NumberFormatter_NumberToString_m7612,
	NumberFormatter_NumberToString_m7613,
	NumberFormatter_NumberToString_m7614,
	NumberFormatter_NumberToString_m7615,
	NumberFormatter_NumberToString_m7616,
	NumberFormatter_NumberToString_m7617,
	NumberFormatter_NumberToString_m7618,
	NumberFormatter_NumberToString_m7619,
	NumberFormatter_NumberToString_m7620,
	NumberFormatter_NumberToString_m7621,
	NumberFormatter_NumberToString_m7622,
	NumberFormatter_NumberToString_m7623,
	NumberFormatter_NumberToString_m7624,
	NumberFormatter_FastIntegerToString_m7625,
	NumberFormatter_IntegerToString_m7626,
	NumberFormatter_NumberToString_m7627,
	NumberFormatter_FormatCurrency_m7628,
	NumberFormatter_FormatDecimal_m7629,
	NumberFormatter_FormatHexadecimal_m7630,
	NumberFormatter_FormatFixedPoint_m7631,
	NumberFormatter_FormatRoundtrip_m7632,
	NumberFormatter_FormatRoundtrip_m7633,
	NumberFormatter_FormatGeneral_m7634,
	NumberFormatter_FormatNumber_m7635,
	NumberFormatter_FormatPercent_m7636,
	NumberFormatter_FormatExponential_m7637,
	NumberFormatter_FormatExponential_m7638,
	NumberFormatter_FormatCustom_m7639,
	NumberFormatter_ZeroTrimEnd_m7640,
	NumberFormatter_IsZeroOnly_m7641,
	NumberFormatter_AppendNonNegativeNumber_m7642,
	NumberFormatter_AppendIntegerString_m7643,
	NumberFormatter_AppendIntegerString_m7644,
	NumberFormatter_AppendDecimalString_m7645,
	NumberFormatter_AppendDecimalString_m7646,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m7647,
	NumberFormatter_AppendExponent_m7648,
	NumberFormatter_AppendOneDigit_m7649,
	NumberFormatter_FastAppendDigits_m7650,
	NumberFormatter_AppendDigits_m7651,
	NumberFormatter_AppendDigits_m7652,
	NumberFormatter_Multiply10_m7653,
	NumberFormatter_Divide10_m7654,
	NumberFormatter_GetClone_m7655,
	ObjectDisposedException__ctor_m1800,
	ObjectDisposedException__ctor_m7656,
	ObjectDisposedException__ctor_m7657,
	ObjectDisposedException_get_Message_m7658,
	ObjectDisposedException_GetObjectData_m7659,
	OperatingSystem__ctor_m7660,
	OperatingSystem_get_Platform_m7661,
	OperatingSystem_Clone_m7662,
	OperatingSystem_GetObjectData_m7663,
	OperatingSystem_ToString_m7664,
	OutOfMemoryException__ctor_m7665,
	OutOfMemoryException__ctor_m7666,
	OverflowException__ctor_m7667,
	OverflowException__ctor_m7668,
	OverflowException__ctor_m7669,
	RankException__ctor_m7670,
	RankException__ctor_m7671,
	RankException__ctor_m7672,
	ResolveEventArgs__ctor_m7673,
	RuntimeMethodHandle__ctor_m7674,
	RuntimeMethodHandle__ctor_m7675,
	RuntimeMethodHandle_get_Value_m7676,
	RuntimeMethodHandle_GetObjectData_m7677,
	RuntimeMethodHandle_Equals_m7678,
	RuntimeMethodHandle_GetHashCode_m7679,
	StringComparer__ctor_m7680,
	StringComparer__cctor_m7681,
	StringComparer_get_InvariantCultureIgnoreCase_m1610,
	StringComparer_Compare_m7682,
	StringComparer_Equals_m7683,
	StringComparer_GetHashCode_m7684,
	CultureAwareComparer__ctor_m7685,
	CultureAwareComparer_Compare_m7686,
	CultureAwareComparer_Equals_m7687,
	CultureAwareComparer_GetHashCode_m7688,
	OrdinalComparer__ctor_m7689,
	OrdinalComparer_Compare_m7690,
	OrdinalComparer_Equals_m7691,
	OrdinalComparer_GetHashCode_m7692,
	SystemException__ctor_m7693,
	SystemException__ctor_m1716,
	SystemException__ctor_m7694,
	SystemException__ctor_m7695,
	ThreadStaticAttribute__ctor_m7696,
	TimeSpan__ctor_m7697,
	TimeSpan__ctor_m7698,
	TimeSpan__ctor_m7699,
	TimeSpan__cctor_m7700,
	TimeSpan_CalculateTicks_m7701,
	TimeSpan_get_Days_m7702,
	TimeSpan_get_Hours_m7703,
	TimeSpan_get_Milliseconds_m7704,
	TimeSpan_get_Minutes_m7705,
	TimeSpan_get_Seconds_m7706,
	TimeSpan_get_Ticks_m7707,
	TimeSpan_get_TotalDays_m7708,
	TimeSpan_get_TotalHours_m7709,
	TimeSpan_get_TotalMilliseconds_m7710,
	TimeSpan_get_TotalMinutes_m7711,
	TimeSpan_get_TotalSeconds_m7712,
	TimeSpan_Add_m7713,
	TimeSpan_Compare_m7714,
	TimeSpan_CompareTo_m7715,
	TimeSpan_CompareTo_m7716,
	TimeSpan_Equals_m7717,
	TimeSpan_Duration_m7718,
	TimeSpan_Equals_m7719,
	TimeSpan_FromDays_m7720,
	TimeSpan_FromHours_m7721,
	TimeSpan_FromMinutes_m7722,
	TimeSpan_FromSeconds_m7723,
	TimeSpan_FromMilliseconds_m7724,
	TimeSpan_From_m7725,
	TimeSpan_GetHashCode_m7726,
	TimeSpan_Negate_m7727,
	TimeSpan_Subtract_m7728,
	TimeSpan_ToString_m7729,
	TimeSpan_op_Addition_m7730,
	TimeSpan_op_Equality_m7731,
	TimeSpan_op_GreaterThan_m7732,
	TimeSpan_op_GreaterThanOrEqual_m7733,
	TimeSpan_op_Inequality_m7734,
	TimeSpan_op_LessThan_m7735,
	TimeSpan_op_LessThanOrEqual_m7736,
	TimeSpan_op_Subtraction_m7737,
	TimeZone__ctor_m7738,
	TimeZone__cctor_m7739,
	TimeZone_get_CurrentTimeZone_m7740,
	TimeZone_IsDaylightSavingTime_m7741,
	TimeZone_IsDaylightSavingTime_m7742,
	TimeZone_ToLocalTime_m7743,
	TimeZone_ToUniversalTime_m7744,
	TimeZone_GetLocalTimeDiff_m7745,
	TimeZone_GetLocalTimeDiff_m7746,
	CurrentSystemTimeZone__ctor_m7747,
	CurrentSystemTimeZone__ctor_m7748,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7749,
	CurrentSystemTimeZone_GetTimeZoneData_m7750,
	CurrentSystemTimeZone_GetDaylightChanges_m7751,
	CurrentSystemTimeZone_GetUtcOffset_m7752,
	CurrentSystemTimeZone_OnDeserialization_m7753,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m7754,
	TypeInitializationException__ctor_m7755,
	TypeInitializationException_GetObjectData_m7756,
	TypeLoadException__ctor_m7757,
	TypeLoadException__ctor_m7758,
	TypeLoadException__ctor_m7759,
	TypeLoadException_get_Message_m7760,
	TypeLoadException_GetObjectData_m7761,
	UnauthorizedAccessException__ctor_m7762,
	UnauthorizedAccessException__ctor_m7763,
	UnauthorizedAccessException__ctor_m7764,
	UnhandledExceptionEventArgs__ctor_m7765,
	UnhandledExceptionEventArgs_get_ExceptionObject_m593,
	UnhandledExceptionEventArgs_get_IsTerminating_m7766,
	UnitySerializationHolder__ctor_m7767,
	UnitySerializationHolder_GetTypeData_m7768,
	UnitySerializationHolder_GetDBNullData_m7769,
	UnitySerializationHolder_GetModuleData_m7770,
	UnitySerializationHolder_GetObjectData_m7771,
	UnitySerializationHolder_GetRealObject_m7772,
	Version__ctor_m7773,
	Version__ctor_m7774,
	Version__ctor_m1588,
	Version__ctor_m7775,
	Version__ctor_m7776,
	Version_CheckedSet_m7777,
	Version_get_Build_m7778,
	Version_get_Major_m7779,
	Version_get_Minor_m7780,
	Version_get_Revision_m7781,
	Version_Clone_m7782,
	Version_CompareTo_m7783,
	Version_Equals_m7784,
	Version_CompareTo_m7785,
	Version_Equals_m7786,
	Version_GetHashCode_m7787,
	Version_ToString_m7788,
	Version_CreateFromString_m7789,
	Version_op_Equality_m7790,
	Version_op_Inequality_m7791,
	WeakReference__ctor_m7792,
	WeakReference__ctor_m7793,
	WeakReference__ctor_m7794,
	WeakReference__ctor_m7795,
	WeakReference_AllocateHandle_m7796,
	WeakReference_get_Target_m7797,
	WeakReference_get_TrackResurrection_m7798,
	WeakReference_Finalize_m7799,
	WeakReference_GetObjectData_m7800,
	PrimalityTest__ctor_m7801,
	PrimalityTest_Invoke_m7802,
	PrimalityTest_BeginInvoke_m7803,
	PrimalityTest_EndInvoke_m7804,
	MemberFilter__ctor_m7805,
	MemberFilter_Invoke_m7806,
	MemberFilter_BeginInvoke_m7807,
	MemberFilter_EndInvoke_m7808,
	TypeFilter__ctor_m7809,
	TypeFilter_Invoke_m7810,
	TypeFilter_BeginInvoke_m7811,
	TypeFilter_EndInvoke_m7812,
	CrossContextDelegate__ctor_m7813,
	CrossContextDelegate_Invoke_m7814,
	CrossContextDelegate_BeginInvoke_m7815,
	CrossContextDelegate_EndInvoke_m7816,
	HeaderHandler__ctor_m7817,
	HeaderHandler_Invoke_m7818,
	HeaderHandler_BeginInvoke_m7819,
	HeaderHandler_EndInvoke_m7820,
	ThreadStart__ctor_m7821,
	ThreadStart_Invoke_m7822,
	ThreadStart_BeginInvoke_m7823,
	ThreadStart_EndInvoke_m7824,
	TimerCallback__ctor_m7825,
	TimerCallback_Invoke_m7826,
	TimerCallback_BeginInvoke_m7827,
	TimerCallback_EndInvoke_m7828,
	WaitCallback__ctor_m7829,
	WaitCallback_Invoke_m7830,
	WaitCallback_BeginInvoke_m7831,
	WaitCallback_EndInvoke_m7832,
	AppDomainInitializer__ctor_m7833,
	AppDomainInitializer_Invoke_m7834,
	AppDomainInitializer_BeginInvoke_m7835,
	AppDomainInitializer_EndInvoke_m7836,
	AssemblyLoadEventHandler__ctor_m7837,
	AssemblyLoadEventHandler_Invoke_m7838,
	AssemblyLoadEventHandler_BeginInvoke_m7839,
	AssemblyLoadEventHandler_EndInvoke_m7840,
	EventHandler__ctor_m7841,
	EventHandler_Invoke_m7842,
	EventHandler_BeginInvoke_m7843,
	EventHandler_EndInvoke_m7844,
	ResolveEventHandler__ctor_m7845,
	ResolveEventHandler_Invoke_m7846,
	ResolveEventHandler_BeginInvoke_m7847,
	ResolveEventHandler_EndInvoke_m7848,
	UnhandledExceptionEventHandler__ctor_m591,
	UnhandledExceptionEventHandler_Invoke_m7849,
	UnhandledExceptionEventHandler_BeginInvoke_m7850,
	UnhandledExceptionEventHandler_EndInvoke_m7851,
};
