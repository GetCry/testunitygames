﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.CaseInsensitiveComparer
struct CaseInsensitiveComparer_t388;
// System.Globalization.CultureInfo
struct CultureInfo_t186;

#include "mscorlib_System_Object.h"

// System.Collections.CaseInsensitiveComparer
struct  CaseInsensitiveComparer_t388  : public Object_t
{
	// System.Globalization.CultureInfo System.Collections.CaseInsensitiveComparer::culture
	CultureInfo_t186 * ___culture_2;
};
struct CaseInsensitiveComparer_t388_StaticFields{
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultComparer
	CaseInsensitiveComparer_t388 * ___defaultComparer_0;
	// System.Collections.CaseInsensitiveComparer System.Collections.CaseInsensitiveComparer::defaultInvariantComparer
	CaseInsensitiveComparer_t388 * ___defaultInvariantComparer_1;
};
