﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MethodAccessException
struct MethodAccessException_t1190;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t165;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MethodAccessException::.ctor()
extern "C" void MethodAccessException__ctor_m7462 (MethodAccessException_t1190 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.MethodAccessException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MethodAccessException__ctor_m7463 (MethodAccessException_t1190 * __this, SerializationInfo_t165 * ___info, StreamingContext_t166  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
