﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SerializeField
struct SerializeField_t65;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SerializeField::.ctor()
extern "C" void SerializeField__ctor_m277 (SerializeField_t65 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
