﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m8453(__this, ___object, ___method, method) (( void (*) (Transform_1_t1369 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m8407_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m8454(__this, ___key, ___value, method) (( DictionaryEntry_t376  (*) (Transform_1_t1369 *, String_t*, bool, const MethodInfo*))Transform_1_Invoke_m8408_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m8455(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t1369 *, String_t*, bool, AsyncCallback_t42 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m8409_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m8456(__this, ___result, method) (( DictionaryEntry_t376  (*) (Transform_1_t1369 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m8410_gshared)(__this, ___result, method)
