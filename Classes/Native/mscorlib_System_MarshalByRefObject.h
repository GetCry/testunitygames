﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t619;

#include "mscorlib_System_Object.h"

// System.MarshalByRefObject
struct  MarshalByRefObject_t247  : public Object_t
{
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t619 * ____identity_0;
};
