﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m8562_gshared (KeyValuePair_2_t1391 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m8562(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1391 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m8562_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m8563_gshared (KeyValuePair_2_t1391 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m8563(__this, method) (( Object_t * (*) (KeyValuePair_2_t1391 *, const MethodInfo*))KeyValuePair_2_get_Key_m8563_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m8564_gshared (KeyValuePair_2_t1391 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m8564(__this, ___value, method) (( void (*) (KeyValuePair_2_t1391 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m8564_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m8565_gshared (KeyValuePair_2_t1391 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m8565(__this, method) (( int32_t (*) (KeyValuePair_2_t1391 *, const MethodInfo*))KeyValuePair_2_get_Value_m8565_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m8566_gshared (KeyValuePair_2_t1391 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m8566(__this, ___value, method) (( void (*) (KeyValuePair_2_t1391 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m8566_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m8567_gshared (KeyValuePair_2_t1391 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m8567(__this, method) (( String_t* (*) (KeyValuePair_2_t1391 *, const MethodInfo*))KeyValuePair_2_ToString_m8567_gshared)(__this, method)
