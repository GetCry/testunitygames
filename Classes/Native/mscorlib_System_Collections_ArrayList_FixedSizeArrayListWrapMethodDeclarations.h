﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ArrayList/FixedSizeArrayListWrapper
struct FixedSizeArrayListWrapper_t699;
// System.Collections.ArrayList
struct ArrayList_t210;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Collections.ICollection
struct ICollection_t386;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::.ctor(System.Collections.ArrayList)
extern "C" void FixedSizeArrayListWrapper__ctor_m4179 (FixedSizeArrayListWrapper_t699 * __this, ArrayList_t210 * ___innerList, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.ArrayList/FixedSizeArrayListWrapper::get_ErrorMessage()
extern "C" String_t* FixedSizeArrayListWrapper_get_ErrorMessage_m4180 (FixedSizeArrayListWrapper_t699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/FixedSizeArrayListWrapper::get_Capacity()
extern "C" int32_t FixedSizeArrayListWrapper_get_Capacity_m4181 (FixedSizeArrayListWrapper_t699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::set_Capacity(System.Int32)
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m4182 (FixedSizeArrayListWrapper_t699 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.ArrayList/FixedSizeArrayListWrapper::Add(System.Object)
extern "C" int32_t FixedSizeArrayListWrapper_Add_m4183 (FixedSizeArrayListWrapper_t699 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::AddRange(System.Collections.ICollection)
extern "C" void FixedSizeArrayListWrapper_AddRange_m4184 (FixedSizeArrayListWrapper_t699 * __this, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Clear()
extern "C" void FixedSizeArrayListWrapper_Clear_m4185 (FixedSizeArrayListWrapper_t699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Insert(System.Int32,System.Object)
extern "C" void FixedSizeArrayListWrapper_Insert_m4186 (FixedSizeArrayListWrapper_t699 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::InsertRange(System.Int32,System.Collections.ICollection)
extern "C" void FixedSizeArrayListWrapper_InsertRange_m4187 (FixedSizeArrayListWrapper_t699 * __this, int32_t ___index, Object_t * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::Remove(System.Object)
extern "C" void FixedSizeArrayListWrapper_Remove_m4188 (FixedSizeArrayListWrapper_t699 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/FixedSizeArrayListWrapper::RemoveAt(System.Int32)
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m4189 (FixedSizeArrayListWrapper_t699 * __this, int32_t ___index, const MethodInfo* method) IL2CPP_METHOD_ATTR;
