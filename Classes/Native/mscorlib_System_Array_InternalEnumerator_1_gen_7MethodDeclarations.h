﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Type>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m8018(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1333 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m7876_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Type>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8019(__this, method) (( void (*) (InternalEnumerator_1_t1333 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Type>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8020(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1333 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Type>::Dispose()
#define InternalEnumerator_1_Dispose_m8021(__this, method) (( void (*) (InternalEnumerator_1_t1333 *, const MethodInfo*))InternalEnumerator_1_Dispose_m7882_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Type>::MoveNext()
#define InternalEnumerator_1_MoveNext_m8022(__this, method) (( bool (*) (InternalEnumerator_1_t1333 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m7884_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Type>::get_Current()
#define InternalEnumerator_1_get_Current_m8023(__this, method) (( Type_t * (*) (InternalEnumerator_1_t1333 *, const MethodInfo*))InternalEnumerator_1_get_Current_m7886_gshared)(__this, method)
