﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MarshalByRefObject
struct MarshalByRefObject_t247;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t619;

#include "codegen/il2cpp-codegen.h"

// System.Void System.MarshalByRefObject::.ctor()
extern "C" void MarshalByRefObject__ctor_m1614 (MarshalByRefObject_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::get_ObjectIdentity()
extern "C" ServerIdentity_t619 * MarshalByRefObject_get_ObjectIdentity_m3597 (MarshalByRefObject_t247 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
