﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40.h"

// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8726_gshared (InternalEnumerator_1_t1413 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8726(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1413 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8726_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8727_gshared (InternalEnumerator_1_t1413 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8727(__this, method) (( void (*) (InternalEnumerator_1_t1413 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8727_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8728_gshared (InternalEnumerator_1_t1413 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8728(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1413 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8728_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8729_gshared (InternalEnumerator_1_t1413 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8729(__this, method) (( void (*) (InternalEnumerator_1_t1413 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8729_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8730_gshared (InternalEnumerator_1_t1413 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8730(__this, method) (( bool (*) (InternalEnumerator_1_t1413 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8730_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C" uint32_t InternalEnumerator_1_get_Current_m8731_gshared (InternalEnumerator_1_t1413 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8731(__this, method) (( uint32_t (*) (InternalEnumerator_1_t1413 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8731_gshared)(__this, method)
