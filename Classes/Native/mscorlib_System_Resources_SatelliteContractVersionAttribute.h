﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Version
struct Version_t232;

#include "mscorlib_System_Attribute.h"

// System.Resources.SatelliteContractVersionAttribute
struct  SatelliteContractVersionAttribute_t878  : public Attribute_t64
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t232 * ___ver_0;
};
