﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.RegularExpressions.Match
struct Match_t295;
// System.IAsyncResult
struct IAsyncResult_t41;
// System.AsyncCallback
struct AsyncCallback_t42;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Text.RegularExpressions.MatchEvaluator
struct  MatchEvaluator_t371  : public MulticastDelegate_t40
{
};
