﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.IFormatProvider
struct IFormatProvider_t1249;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Globalization_NumberStyles.h"

// System.Boolean System.UInt16::System.IConvertible.ToBoolean(System.IFormatProvider)
extern "C" bool UInt16_System_IConvertible_ToBoolean_m2949 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.UInt16::System.IConvertible.ToByte(System.IFormatProvider)
extern "C" uint8_t UInt16_System_IConvertible_ToByte_m2950 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.UInt16::System.IConvertible.ToChar(System.IFormatProvider)
extern "C" uint16_t UInt16_System_IConvertible_ToChar_m2951 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.UInt16::System.IConvertible.ToDateTime(System.IFormatProvider)
extern "C" DateTime_t118  UInt16_System_IConvertible_ToDateTime_m2952 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.UInt16::System.IConvertible.ToDecimal(System.IFormatProvider)
extern "C" Decimal_t592  UInt16_System_IConvertible_ToDecimal_m2953 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.UInt16::System.IConvertible.ToDouble(System.IFormatProvider)
extern "C" double UInt16_System_IConvertible_ToDouble_m2954 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.UInt16::System.IConvertible.ToInt16(System.IFormatProvider)
extern "C" int16_t UInt16_System_IConvertible_ToInt16_m2955 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::System.IConvertible.ToInt32(System.IFormatProvider)
extern "C" int32_t UInt16_System_IConvertible_ToInt32_m2956 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.UInt16::System.IConvertible.ToInt64(System.IFormatProvider)
extern "C" int64_t UInt16_System_IConvertible_ToInt64_m2957 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.UInt16::System.IConvertible.ToSByte(System.IFormatProvider)
extern "C" int8_t UInt16_System_IConvertible_ToSByte_m2958 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.UInt16::System.IConvertible.ToSingle(System.IFormatProvider)
extern "C" float UInt16_System_IConvertible_ToSingle_m2959 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.UInt16::System.IConvertible.ToType(System.Type,System.IFormatProvider)
extern "C" Object_t * UInt16_System_IConvertible_ToType_m2960 (uint16_t* __this, Type_t * ___targetType, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::System.IConvertible.ToUInt16(System.IFormatProvider)
extern "C" uint16_t UInt16_System_IConvertible_ToUInt16_m2961 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.UInt16::System.IConvertible.ToUInt32(System.IFormatProvider)
extern "C" uint32_t UInt16_System_IConvertible_ToUInt32_m2962 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.UInt16::System.IConvertible.ToUInt64(System.IFormatProvider)
extern "C" uint64_t UInt16_System_IConvertible_ToUInt64_m2963 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.Object)
extern "C" int32_t UInt16_CompareTo_m2964 (uint16_t* __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.Object)
extern "C" bool UInt16_Equals_m2965 (uint16_t* __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::GetHashCode()
extern "C" int32_t UInt16_GetHashCode_m2966 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.UInt16)
extern "C" int32_t UInt16_CompareTo_m2967 (uint16_t* __this, uint16_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.UInt16)
extern "C" bool UInt16_Equals_m2968 (uint16_t* __this, uint16_t ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.IFormatProvider)
extern "C" uint16_t UInt16_Parse_m2969 (Object_t * __this /* static, unused */, String_t* ___s, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.Globalization.NumberStyles,System.IFormatProvider)
extern "C" uint16_t UInt16_Parse_m2970 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.UInt16&)
extern "C" bool UInt16_TryParse_m2971 (Object_t * __this /* static, unused */, String_t* ___s, uint16_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.UInt16&)
extern "C" bool UInt16_TryParse_m2972 (Object_t * __this /* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, uint16_t* ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString()
extern "C" String_t* UInt16_ToString_m2973 (uint16_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.IFormatProvider)
extern "C" String_t* UInt16_ToString_m2974 (uint16_t* __this, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String)
extern "C" String_t* UInt16_ToString_m2975 (uint16_t* __this, String_t* ___format, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String,System.IFormatProvider)
extern "C" String_t* UInt16_ToString_m2976 (uint16_t* __this, String_t* ___format, Object_t * ___provider, const MethodInfo* method) IL2CPP_METHOD_ATTR;
