﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Texture
struct Texture_t34;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Texture::.ctor()
extern "C" void Texture__ctor_m123 (Texture_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
