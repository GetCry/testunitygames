﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t58;

#include "UnityEngine_UnityEngine_Transform.h"

// UnityEngine.RectTransform
struct  RectTransform_t59  : public Transform_t60
{
};
struct RectTransform_t59_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t58 * ___reapplyDrivenProperties_2;
};
