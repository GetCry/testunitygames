﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m8238(__this, ___l, method) (( void (*) (Enumerator_t1359 *, List_1_t142 *, const MethodInfo*))Enumerator__ctor_m8006_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8239(__this, method) (( void (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8007_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8240(__this, method) (( Object_t * (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8008_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m8241(__this, method) (( void (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_Dispose_m8009_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::VerifyState()
#define Enumerator_VerifyState_m8242(__this, method) (( void (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_VerifyState_m8010_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m8243(__this, method) (( bool (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_MoveNext_m8011_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m8244(__this, method) (( PersistentCall_t140 * (*) (Enumerator_t1359 *, const MethodInfo*))Enumerator_get_Current_m8012_gshared)(__this, method)
