﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1280;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t835;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1525;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m9129_gshared (ReadOnlyCollection_1_t1280 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m9129(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t1280 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m9129_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m9130_gshared (ReadOnlyCollection_1_t1280 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m9130(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t1280 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m9130_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m9131_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m9131(__this, method) (( void (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m9131_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m9132_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m9132(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m9132_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m9133_gshared (ReadOnlyCollection_1_t1280 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m9133(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t1280 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m9133_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m9134_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m9134(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m9134_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t838  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m9135_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m9135(__this, ___index, method) (( CustomAttributeTypedArgument_t838  (*) (ReadOnlyCollection_1_t1280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m9135_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m9136_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m9136(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m9136_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9137_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9137(__this, method) (( bool (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9137_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m9138_gshared (ReadOnlyCollection_1_t1280 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m9138(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1280 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m9138_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m9139_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m9139(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m9139_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m9140_gshared (ReadOnlyCollection_1_t1280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m9140(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m9140_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m9141_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m9141(__this, method) (( void (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m9141_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m9142_gshared (ReadOnlyCollection_1_t1280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m9142(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m9142_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m9143_gshared (ReadOnlyCollection_1_t1280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m9143(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m9143_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m9144_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m9144(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m9144_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m9145_gshared (ReadOnlyCollection_1_t1280 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m9145(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t1280 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m9145_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m9146_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m9146(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m9146_gshared)(__this, ___index, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m9147_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m9147(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m9147_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m9148_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m9148(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t1280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m9148_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m9149_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m9149(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t1280 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m9149_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m9150_gshared (ReadOnlyCollection_1_t1280 * __this, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m9150(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t1280 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_Contains_m9150_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m9151_gshared (ReadOnlyCollection_1_t1280 * __this, CustomAttributeTypedArgumentU5BU5D_t1278* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m9151(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t1280 *, CustomAttributeTypedArgumentU5BU5D_t1278*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m9151_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m9152_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m9152(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m9152_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m9153_gshared (ReadOnlyCollection_1_t1280 * __this, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m9153(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t1280 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m9153_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m9154_gshared (ReadOnlyCollection_1_t1280 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m9154(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t1280 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m9154_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t838  ReadOnlyCollection_1_get_Item_m9155_gshared (ReadOnlyCollection_1_t1280 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m9155(__this, ___index, method) (( CustomAttributeTypedArgument_t838  (*) (ReadOnlyCollection_1_t1280 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m9155_gshared)(__this, ___index, method)
