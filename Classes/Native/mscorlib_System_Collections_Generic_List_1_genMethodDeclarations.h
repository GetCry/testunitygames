﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor()
#define List_1__ctor_m595(__this, method) (( void (*) (List_1_t27 *, const MethodInfo*))List_1__ctor_m7942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.ctor(System.Int32)
#define List_1__ctor_m7943(__this, ___capacity, method) (( void (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1__ctor_m7944_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::.cctor()
#define List_1__cctor_m7945(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m7946_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7947(__this, method) (( Object_t* (*) (List_1_t27 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m7949(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t27 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7950_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m7951(__this, method) (( Object_t * (*) (List_1_t27 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m7953(__this, ___item, method) (( int32_t (*) (List_1_t27 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m7955(__this, ___item, method) (( bool (*) (List_1_t27 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7956_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m7957(__this, ___item, method) (( int32_t (*) (List_1_t27 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7958_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m7959(__this, ___index, ___item, method) (( void (*) (List_1_t27 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m7961(__this, ___item, method) (( void (*) (List_1_t27 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7962_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7963(__this, method) (( bool (*) (List_1_t27 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m7965(__this, method) (( Object_t * (*) (List_1_t27 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m7967(__this, ___index, method) (( Object_t * (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7968_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m7969(__this, ___index, ___value, method) (( void (*) (List_1_t27 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Add(T)
#define List_1_Add_m7971(__this, ___item, method) (( void (*) (List_1_t27 *, GcLeaderboard_t28 *, const MethodInfo*))List_1_Add_m7972_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m7973(__this, ___newCount, method) (( void (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m7974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Clear()
#define List_1_Clear_m7975(__this, method) (( void (*) (List_1_t27 *, const MethodInfo*))List_1_Clear_m7976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Contains(T)
#define List_1_Contains_m7977(__this, ___item, method) (( bool (*) (List_1_t27 *, GcLeaderboard_t28 *, const MethodInfo*))List_1_Contains_m7978_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m7979(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t27 *, GcLeaderboardU5BU5D_t1329*, int32_t, const MethodInfo*))List_1_CopyTo_m7980_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::GetEnumerator()
#define List_1_GetEnumerator_m601(__this, method) (( Enumerator_t175  (*) (List_1_t27 *, const MethodInfo*))List_1_GetEnumerator_m7981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::IndexOf(T)
#define List_1_IndexOf_m7982(__this, ___item, method) (( int32_t (*) (List_1_t27 *, GcLeaderboard_t28 *, const MethodInfo*))List_1_IndexOf_m7983_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m7984(__this, ___start, ___delta, method) (( void (*) (List_1_t27 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m7985_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m7986(__this, ___index, method) (( void (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_CheckIndex_m7987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Insert(System.Int32,T)
#define List_1_Insert_m7988(__this, ___index, ___item, method) (( void (*) (List_1_t27 *, int32_t, GcLeaderboard_t28 *, const MethodInfo*))List_1_Insert_m7989_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::Remove(T)
#define List_1_Remove_m7990(__this, ___item, method) (( bool (*) (List_1_t27 *, GcLeaderboard_t28 *, const MethodInfo*))List_1_Remove_m7991_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m7992(__this, ___index, method) (( void (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7993_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::ToArray()
#define List_1_ToArray_m7994(__this, method) (( GcLeaderboardU5BU5D_t1329* (*) (List_1_t27 *, const MethodInfo*))List_1_ToArray_m7995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Capacity()
#define List_1_get_Capacity_m7996(__this, method) (( int32_t (*) (List_1_t27 *, const MethodInfo*))List_1_get_Capacity_m7997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m7998(__this, ___value, method) (( void (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_set_Capacity_m7999_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Count()
#define List_1_get_Count_m8000(__this, method) (( int32_t (*) (List_1_t27 *, const MethodInfo*))List_1_get_Count_m8001_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::get_Item(System.Int32)
#define List_1_get_Item_m8002(__this, ___index, method) (( GcLeaderboard_t28 * (*) (List_1_t27 *, int32_t, const MethodInfo*))List_1_get_Item_m8003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard>::set_Item(System.Int32,T)
#define List_1_set_Item_m8004(__this, ___index, ___value, method) (( void (*) (List_1_t27 *, int32_t, GcLeaderboard_t28 *, const MethodInfo*))List_1_set_Item_m8005_gshared)(__this, ___index, ___value, method)
