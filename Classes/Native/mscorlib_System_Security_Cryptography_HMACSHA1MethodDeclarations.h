﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.HMACSHA1
struct HMACSHA1_t568;
// System.Byte[]
struct ByteU5BU5D_t264;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.HMACSHA1::.ctor()
extern "C" void HMACSHA1__ctor_m6285 (HMACSHA1_t568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA1::.ctor(System.Byte[])
extern "C" void HMACSHA1__ctor_m6286 (HMACSHA1_t568 * __this, ByteU5BU5D_t264* ___key, const MethodInfo* method) IL2CPP_METHOD_ATTR;
