﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t1504;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m9518_gshared (DefaultComparer_t1504 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m9518(__this, method) (( void (*) (DefaultComparer_t1504 *, const MethodInfo*))DefaultComparer__ctor_m9518_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m9519_gshared (DefaultComparer_t1504 * __this, DateTimeOffset_t1163  ___x, DateTimeOffset_t1163  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m9519(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1504 *, DateTimeOffset_t1163 , DateTimeOffset_t1163 , const MethodInfo*))DefaultComparer_Compare_m9519_gshared)(__this, ___x, ___y, method)
