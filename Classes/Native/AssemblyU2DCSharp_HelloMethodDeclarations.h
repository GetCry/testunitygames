﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Hello
struct Hello_t1;

#include "codegen/il2cpp-codegen.h"

// System.Void Hello::.ctor()
extern "C" void Hello__ctor_m0 (Hello_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hello::Start()
extern "C" void Hello_Start_m1 (Hello_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Hello::Update()
extern "C" void Hello_Update_m2 (Hello_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
