﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t1471;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1525;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m9187_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1__ctor_m9187(__this, method) (( void (*) (List_1_t1471 *, const MethodInfo*))List_1__ctor_m9187_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m9188_gshared (List_1_t1471 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m9188(__this, ___capacity, method) (( void (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1__ctor_m9188_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m9189_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m9189(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m9189_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9190_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9190(__this, method) (( Object_t* (*) (List_1_t1471 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9190_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m9191_gshared (List_1_t1471 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m9191(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1471 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m9191_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m9192_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9192(__this, method) (( Object_t * (*) (List_1_t1471 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m9192_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m9193_gshared (List_1_t1471 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m9193(__this, ___item, method) (( int32_t (*) (List_1_t1471 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m9193_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m9194_gshared (List_1_t1471 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m9194(__this, ___item, method) (( bool (*) (List_1_t1471 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m9194_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m9195_gshared (List_1_t1471 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m9195(__this, ___item, method) (( int32_t (*) (List_1_t1471 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m9195_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m9196_gshared (List_1_t1471 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m9196(__this, ___index, ___item, method) (( void (*) (List_1_t1471 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m9196_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m9197_gshared (List_1_t1471 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m9197(__this, ___item, method) (( void (*) (List_1_t1471 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m9197_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9198_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9198(__this, method) (( bool (*) (List_1_t1471 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9198_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m9199_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m9199(__this, method) (( Object_t * (*) (List_1_t1471 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m9199_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m9200_gshared (List_1_t1471 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m9200(__this, ___index, method) (( Object_t * (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m9200_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m9201_gshared (List_1_t1471 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m9201(__this, ___index, ___value, method) (( void (*) (List_1_t1471 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m9201_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m9202_gshared (List_1_t1471 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define List_1_Add_m9202(__this, ___item, method) (( void (*) (List_1_t1471 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_Add_m9202_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m9203_gshared (List_1_t1471 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m9203(__this, ___newCount, method) (( void (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m9203_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m9204_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_Clear_m9204(__this, method) (( void (*) (List_1_t1471 *, const MethodInfo*))List_1_Clear_m9204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m9205_gshared (List_1_t1471 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define List_1_Contains_m9205(__this, ___item, method) (( bool (*) (List_1_t1471 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_Contains_m9205_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m9206_gshared (List_1_t1471 * __this, CustomAttributeTypedArgumentU5BU5D_t1278* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m9206(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1471 *, CustomAttributeTypedArgumentU5BU5D_t1278*, int32_t, const MethodInfo*))List_1_CopyTo_m9206_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t1472  List_1_GetEnumerator_m9207_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m9207(__this, method) (( Enumerator_t1472  (*) (List_1_t1471 *, const MethodInfo*))List_1_GetEnumerator_m9207_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m9208_gshared (List_1_t1471 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define List_1_IndexOf_m9208(__this, ___item, method) (( int32_t (*) (List_1_t1471 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_IndexOf_m9208_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m9209_gshared (List_1_t1471 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m9209(__this, ___start, ___delta, method) (( void (*) (List_1_t1471 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m9209_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m9210_gshared (List_1_t1471 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m9210(__this, ___index, method) (( void (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_CheckIndex_m9210_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m9211_gshared (List_1_t1471 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define List_1_Insert_m9211(__this, ___index, ___item, method) (( void (*) (List_1_t1471 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_Insert_m9211_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m9212_gshared (List_1_t1471 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define List_1_Remove_m9212(__this, ___item, method) (( bool (*) (List_1_t1471 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_Remove_m9212_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m9213_gshared (List_1_t1471 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m9213(__this, ___index, method) (( void (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_RemoveAt_m9213_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t1278* List_1_ToArray_m9214_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_ToArray_m9214(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t1278* (*) (List_1_t1471 *, const MethodInfo*))List_1_ToArray_m9214_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m9215_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m9215(__this, method) (( int32_t (*) (List_1_t1471 *, const MethodInfo*))List_1_get_Capacity_m9215_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m9216_gshared (List_1_t1471 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m9216(__this, ___value, method) (( void (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_set_Capacity_m9216_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m9217_gshared (List_1_t1471 * __this, const MethodInfo* method);
#define List_1_get_Count_m9217(__this, method) (( int32_t (*) (List_1_t1471 *, const MethodInfo*))List_1_get_Count_m9217_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t838  List_1_get_Item_m9218_gshared (List_1_t1471 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m9218(__this, ___index, method) (( CustomAttributeTypedArgument_t838  (*) (List_1_t1471 *, int32_t, const MethodInfo*))List_1_get_Item_m9218_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m9219_gshared (List_1_t1471 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define List_1_set_Item_m9219(__this, ___index, ___value, method) (( void (*) (List_1_t1471 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))List_1_set_Item_m9219_gshared)(__this, ___index, ___value, method)
