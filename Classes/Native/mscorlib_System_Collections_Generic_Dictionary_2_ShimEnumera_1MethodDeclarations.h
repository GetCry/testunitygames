﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t1449;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1443;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m9007_gshared (ShimEnumerator_t1449 * __this, Dictionary_2_t1443 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m9007(__this, ___host, method) (( void (*) (ShimEnumerator_t1449 *, Dictionary_2_t1443 *, const MethodInfo*))ShimEnumerator__ctor_m9007_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m9008_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m9008(__this, method) (( bool (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_MoveNext_m9008_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t376  ShimEnumerator_get_Entry_m9009_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m9009(__this, method) (( DictionaryEntry_t376  (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_get_Entry_m9009_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m9010_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m9010(__this, method) (( Object_t * (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_get_Key_m9010_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m9011_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m9011(__this, method) (( Object_t * (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_get_Value_m9011_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m9012_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m9012(__this, method) (( Object_t * (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_get_Current_m9012_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m9013_gshared (ShimEnumerator_t1449 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m9013(__this, method) (( void (*) (ShimEnumerator_t1449 *, const MethodInfo*))ShimEnumerator_Reset_m9013_gshared)(__this, method)
