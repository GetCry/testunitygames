﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m1717(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1__ctor_m7942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::.ctor(System.Int32)
#define List_1__ctor_m8650(__this, ___capacity, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1__ctor_m7944_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.String>::.cctor()
#define List_1__cctor_m8651(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m7946_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.String>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m8652(__this, method) (( Object_t* (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m8653(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t421 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7950_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.String>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m8654(__this, method) (( Object_t * (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m8655(__this, ___item, method) (( int32_t (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m8656(__this, ___item, method) (( bool (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7956_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m8657(__this, ___item, method) (( int32_t (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7958_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m8658(__this, ___index, ___item, method) (( void (*) (List_1_t421 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m8659(__this, ___item, method) (( void (*) (List_1_t421 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7962_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m8660(__this, method) (( bool (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m8661(__this, method) (( Object_t * (*) (List_1_t421 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.String>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m8662(__this, ___index, method) (( Object_t * (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7968_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m8663(__this, ___index, ___value, method) (( void (*) (List_1_t421 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
#define List_1_Add_m8664(__this, ___item, method) (( void (*) (List_1_t421 *, String_t*, const MethodInfo*))List_1_Add_m7972_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m8665(__this, ___newCount, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m7974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m8666(__this, method) (( void (*) (List_1_t421 *, const MethodInfo*))List_1_Clear_m7976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Contains(T)
#define List_1_Contains_m8667(__this, ___item, method) (( bool (*) (List_1_t421 *, String_t*, const MethodInfo*))List_1_Contains_m7978_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m8668(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t421 *, StringU5BU5D_t122*, int32_t, const MethodInfo*))List_1_CopyTo_m7980_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
#define List_1_GetEnumerator_m8669(__this, method) (( Enumerator_t1404  (*) (List_1_t421 *, const MethodInfo*))List_1_GetEnumerator_m7981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::IndexOf(T)
#define List_1_IndexOf_m8670(__this, ___item, method) (( int32_t (*) (List_1_t421 *, String_t*, const MethodInfo*))List_1_IndexOf_m7983_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m8671(__this, ___start, ___delta, method) (( void (*) (List_1_t421 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m7985_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.String>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m8672(__this, ___index, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_CheckIndex_m7987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,T)
#define List_1_Insert_m8673(__this, ___index, ___item, method) (( void (*) (List_1_t421 *, int32_t, String_t*, const MethodInfo*))List_1_Insert_m7989_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.String>::Remove(T)
#define List_1_Remove_m8674(__this, ___item, method) (( bool (*) (List_1_t421 *, String_t*, const MethodInfo*))List_1_Remove_m7991_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m8675(__this, ___index, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7993_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.String>::ToArray()
#define List_1_ToArray_m7852(__this, method) (( StringU5BU5D_t122* (*) (List_1_t421 *, const MethodInfo*))List_1_ToArray_m7995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Capacity()
#define List_1_get_Capacity_m8676(__this, method) (( int32_t (*) (List_1_t421 *, const MethodInfo*))List_1_get_Capacity_m7997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m8677(__this, ___value, method) (( void (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_set_Capacity_m7999_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m8678(__this, method) (( int32_t (*) (List_1_t421 *, const MethodInfo*))List_1_get_Count_m8001_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m8679(__this, ___index, method) (( String_t* (*) (List_1_t421 *, int32_t, const MethodInfo*))List_1_get_Item_m8003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,T)
#define List_1_set_Item_m8680(__this, ___index, ___value, method) (( void (*) (List_1_t421 *, int32_t, String_t*, const MethodInfo*))List_1_set_Item_m8005_gshared)(__this, ___index, ___value, method)
