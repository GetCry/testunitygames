﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Calendar
struct Calendar_t725;
// System.Object
struct Object_t;
// System.String[]
struct StringU5BU5D_t122;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.Calendar::.ctor()
extern "C" void Calendar__ctor_m4441 (Calendar_t725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Globalization.Calendar::Clone()
extern "C" Object_t * Calendar_Clone_m4442 (Calendar_t725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C" void Calendar_CheckReadOnly_m4443 (Calendar_t725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C" StringU5BU5D_t122* Calendar_get_EraNames_m4444 (Calendar_t725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
