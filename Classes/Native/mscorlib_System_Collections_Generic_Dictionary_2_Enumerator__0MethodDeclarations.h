﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m8463(__this, ___dictionary, method) (( void (*) (Enumerator_t1387 *, Dictionary_2_t243 *, const MethodInfo*))Enumerator__ctor_m8421_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8464(__this, method) (( Object_t * (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8422_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8465(__this, method) (( void (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8423_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8466(__this, method) (( DictionaryEntry_t376  (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8424_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8467(__this, method) (( Object_t * (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8425_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8468(__this, method) (( Object_t * (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8426_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m8469(__this, method) (( bool (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_MoveNext_m8427_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m8470(__this, method) (( KeyValuePair_2_t1386  (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_get_Current_m8428_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m8471(__this, method) (( String_t* (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_get_CurrentKey_m8429_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m8472(__this, method) (( bool (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_get_CurrentValue_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m8473(__this, method) (( void (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_Reset_m8431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m8474(__this, method) (( void (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_VerifyState_m8432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m8475(__this, method) (( void (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_VerifyCurrent_m8433_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m8476(__this, method) (( void (*) (Enumerator_t1387 *, const MethodInfo*))Enumerator_Dispose_m8434_gshared)(__this, method)
