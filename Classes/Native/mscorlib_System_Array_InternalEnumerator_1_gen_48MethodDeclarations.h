﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48.h"

// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8774_gshared (InternalEnumerator_1_t1421 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8774(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1421 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8774_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8775_gshared (InternalEnumerator_1_t1421 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8775(__this, method) (( void (*) (InternalEnumerator_1_t1421 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8775_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8776_gshared (InternalEnumerator_1_t1421 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8776(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1421 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8776_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8777_gshared (InternalEnumerator_1_t1421 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8777(__this, method) (( void (*) (InternalEnumerator_1_t1421 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8777_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8778_gshared (InternalEnumerator_1_t1421 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8778(__this, method) (( bool (*) (InternalEnumerator_1_t1421 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8778_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern "C" int64_t InternalEnumerator_1_get_Current_m8779_gshared (InternalEnumerator_1_t1421 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8779(__this, method) (( int64_t (*) (InternalEnumerator_1_t1421 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8779_gshared)(__this, method)
