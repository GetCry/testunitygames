﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m8099(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1347 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m7876_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8100(__this, method) (( void (*) (InternalEnumerator_1_t1347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8101(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1347 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::Dispose()
#define InternalEnumerator_1_Dispose_m8102(__this, method) (( void (*) (InternalEnumerator_1_t1347 *, const MethodInfo*))InternalEnumerator_1_Dispose_m7882_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::MoveNext()
#define InternalEnumerator_1_MoveNext_m8103(__this, method) (( bool (*) (InternalEnumerator_1_t1347 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m7884_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.DisallowMultipleComponent>::get_Current()
#define InternalEnumerator_1_get_Current_m8104(__this, method) (( DisallowMultipleComponent_t100 * (*) (InternalEnumerator_1_t1347 *, const MethodInfo*))InternalEnumerator_1_get_Current_m7886_gshared)(__this, method)
