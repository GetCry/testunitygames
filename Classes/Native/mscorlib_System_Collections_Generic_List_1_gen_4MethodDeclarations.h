﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor()
#define List_1__ctor_m9457(__this, method) (( void (*) (List_1_t1309 *, const MethodInfo*))List_1__ctor_m7942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.ctor(System.Int32)
#define List_1__ctor_m7861(__this, ___capacity, method) (( void (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1__ctor_m7944_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::.cctor()
#define List_1__cctor_m9458(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m7946_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m9459(__this, method) (( Object_t* (*) (List_1_t1309 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m9460(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1309 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7950_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m9461(__this, method) (( Object_t * (*) (List_1_t1309 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7952_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m9462(__this, ___item, method) (( int32_t (*) (List_1_t1309 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m9463(__this, ___item, method) (( bool (*) (List_1_t1309 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7956_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m9464(__this, ___item, method) (( int32_t (*) (List_1_t1309 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7958_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m9465(__this, ___index, ___item, method) (( void (*) (List_1_t1309 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7960_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m9466(__this, ___item, method) (( void (*) (List_1_t1309 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7962_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m9467(__this, method) (( bool (*) (List_1_t1309 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7964_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m9468(__this, method) (( Object_t * (*) (List_1_t1309 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7966_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m9469(__this, ___index, method) (( Object_t * (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7968_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m9470(__this, ___index, ___value, method) (( void (*) (List_1_t1309 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7970_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Add(T)
#define List_1_Add_m9471(__this, ___item, method) (( void (*) (List_1_t1309 *, StrongName_t1088 *, const MethodInfo*))List_1_Add_m7972_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m9472(__this, ___newCount, method) (( void (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m7974_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Clear()
#define List_1_Clear_m9473(__this, method) (( void (*) (List_1_t1309 *, const MethodInfo*))List_1_Clear_m7976_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Contains(T)
#define List_1_Contains_m9474(__this, ___item, method) (( bool (*) (List_1_t1309 *, StrongName_t1088 *, const MethodInfo*))List_1_Contains_m7978_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m9475(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1309 *, StrongNameU5BU5D_t1497*, int32_t, const MethodInfo*))List_1_CopyTo_m7980_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Security.Policy.StrongName>::GetEnumerator()
#define List_1_GetEnumerator_m9476(__this, method) (( Enumerator_t1498  (*) (List_1_t1309 *, const MethodInfo*))List_1_GetEnumerator_m7981_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::IndexOf(T)
#define List_1_IndexOf_m9477(__this, ___item, method) (( int32_t (*) (List_1_t1309 *, StrongName_t1088 *, const MethodInfo*))List_1_IndexOf_m7983_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m9478(__this, ___start, ___delta, method) (( void (*) (List_1_t1309 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m7985_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m9479(__this, ___index, method) (( void (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_CheckIndex_m7987_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Insert(System.Int32,T)
#define List_1_Insert_m9480(__this, ___index, ___item, method) (( void (*) (List_1_t1309 *, int32_t, StrongName_t1088 *, const MethodInfo*))List_1_Insert_m7989_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Security.Policy.StrongName>::Remove(T)
#define List_1_Remove_m9481(__this, ___item, method) (( bool (*) (List_1_t1309 *, StrongName_t1088 *, const MethodInfo*))List_1_Remove_m7991_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m9482(__this, ___index, method) (( void (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7993_gshared)(__this, ___index, method)
// T[] System.Collections.Generic.List`1<System.Security.Policy.StrongName>::ToArray()
#define List_1_ToArray_m9483(__this, method) (( StrongNameU5BU5D_t1497* (*) (List_1_t1309 *, const MethodInfo*))List_1_ToArray_m7995_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Capacity()
#define List_1_get_Capacity_m9484(__this, method) (( int32_t (*) (List_1_t1309 *, const MethodInfo*))List_1_get_Capacity_m7997_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m9485(__this, ___value, method) (( void (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_set_Capacity_m7999_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Count()
#define List_1_get_Count_m9486(__this, method) (( int32_t (*) (List_1_t1309 *, const MethodInfo*))List_1_get_Count_m8001_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Security.Policy.StrongName>::get_Item(System.Int32)
#define List_1_get_Item_m9487(__this, ___index, method) (( StrongName_t1088 * (*) (List_1_t1309 *, int32_t, const MethodInfo*))List_1_get_Item_m8003_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Security.Policy.StrongName>::set_Item(System.Int32,T)
#define List_1_set_Item_m9488(__this, ___index, ___value, method) (( void (*) (List_1_t1309 *, int32_t, StrongName_t1088 *, const MethodInfo*))List_1_set_Item_m8005_gshared)(__this, ___index, ___value, method)
