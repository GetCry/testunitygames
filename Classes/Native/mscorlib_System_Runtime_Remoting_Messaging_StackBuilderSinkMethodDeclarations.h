﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.Messaging.StackBuilderSink
struct StackBuilderSink_t965;
// System.MarshalByRefObject
struct MarshalByRefObject_t247;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.Messaging.StackBuilderSink::.ctor(System.MarshalByRefObject,System.Boolean)
extern "C" void StackBuilderSink__ctor_m5862 (StackBuilderSink_t965 * __this, MarshalByRefObject_t247 * ___obj, bool ___forceInternalExecute, const MethodInfo* method) IL2CPP_METHOD_ATTR;
