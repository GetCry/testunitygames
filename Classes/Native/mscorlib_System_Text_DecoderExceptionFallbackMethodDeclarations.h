﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.DecoderExceptionFallback
struct DecoderExceptionFallback_t1107;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1106;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.DecoderExceptionFallback::.ctor()
extern "C" void DecoderExceptionFallback__ctor_m6657 (DecoderExceptionFallback_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderExceptionFallback::CreateFallbackBuffer()
extern "C" DecoderFallbackBuffer_t1106 * DecoderExceptionFallback_CreateFallbackBuffer_m6658 (DecoderExceptionFallback_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderExceptionFallback::Equals(System.Object)
extern "C" bool DecoderExceptionFallback_Equals_m6659 (DecoderExceptionFallback_t1107 * __this, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderExceptionFallback::GetHashCode()
extern "C" int32_t DecoderExceptionFallback_GetHashCode_m6660 (DecoderExceptionFallback_t1107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
