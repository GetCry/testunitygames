﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m8618(__this, ___dictionary, method) (( void (*) (Enumerator_t1400 *, Dictionary_2_t253 *, const MethodInfo*))Enumerator__ctor_m8576_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m8619(__this, method) (( Object_t * (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8577_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m8620(__this, method) (( void (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8578_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8621(__this, method) (( DictionaryEntry_t376  (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8579_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8622(__this, method) (( Object_t * (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8580_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8623(__this, method) (( Object_t * (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8581_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m8624(__this, method) (( bool (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_MoveNext_m8582_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m8625(__this, method) (( KeyValuePair_2_t1399  (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_get_Current_m8583_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m8626(__this, method) (( String_t* (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_get_CurrentKey_m8584_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m8627(__this, method) (( int32_t (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_get_CurrentValue_m8585_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Reset()
#define Enumerator_Reset_m8628(__this, method) (( void (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_Reset_m8586_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m8629(__this, method) (( void (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_VerifyState_m8587_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m8630(__this, method) (( void (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_VerifyCurrent_m8588_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m8631(__this, method) (( void (*) (Enumerator_t1400 *, const MethodInfo*))Enumerator_Dispose_m8589_gshared)(__this, method)
