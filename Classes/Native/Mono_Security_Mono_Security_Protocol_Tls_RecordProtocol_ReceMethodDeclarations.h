﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult
struct ReceiveRecordAsyncResult_t516;
// System.AsyncCallback
struct AsyncCallback_t42;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t264;
// System.IO.Stream
struct Stream_t518;
// System.Exception
struct Exception_t134;
// System.Threading.WaitHandle
struct WaitHandle_t564;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::.ctor(System.AsyncCallback,System.Object,System.Byte[],System.IO.Stream)
extern "C" void ReceiveRecordAsyncResult__ctor_m2316 (ReceiveRecordAsyncResult_t516 * __this, AsyncCallback_t42 * ___userCallback, Object_t * ___userState, ByteU5BU5D_t264* ___initialBuffer, Stream_t518 * ___record, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.Stream Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_Record()
extern "C" Stream_t518 * ReceiveRecordAsyncResult_get_Record_m2317 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_ResultingBuffer()
extern "C" ByteU5BU5D_t264* ReceiveRecordAsyncResult_get_ResultingBuffer_m2318 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_InitialBuffer()
extern "C" ByteU5BU5D_t264* ReceiveRecordAsyncResult_get_InitialBuffer_m2319 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncState()
extern "C" Object_t * ReceiveRecordAsyncResult_get_AsyncState_m2320 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncException()
extern "C" Exception_t134 * ReceiveRecordAsyncResult_get_AsyncException_m2321 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_CompletedWithError()
extern "C" bool ReceiveRecordAsyncResult_get_CompletedWithError_m2322 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Threading.WaitHandle Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_AsyncWaitHandle()
extern "C" WaitHandle_t564 * ReceiveRecordAsyncResult_get_AsyncWaitHandle_m2323 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::get_IsCompleted()
extern "C" bool ReceiveRecordAsyncResult_get_IsCompleted_m2324 (ReceiveRecordAsyncResult_t516 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception,System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2325 (ReceiveRecordAsyncResult_t516 * __this, Exception_t134 * ___ex, ByteU5BU5D_t264* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Exception)
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2326 (ReceiveRecordAsyncResult_t516 * __this, Exception_t134 * ___ex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.RecordProtocol/ReceiveRecordAsyncResult::SetComplete(System.Byte[])
extern "C" void ReceiveRecordAsyncResult_SetComplete_m2327 (ReceiveRecordAsyncResult_t516 * __this, ByteU5BU5D_t264* ___resultingBuffer, const MethodInfo* method) IL2CPP_METHOD_ATTR;
