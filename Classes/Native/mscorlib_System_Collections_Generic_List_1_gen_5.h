﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t147;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t1330  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Object>::_items
	ObjectU5BU5D_t147* ____items_0;
	// System.Int32 System.Collections.Generic.List`1<System.Object>::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.List`1<System.Object>::_version
	int32_t ____version_2;
};
struct List_1_t1330_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Object>::EmptyArray
	ObjectU5BU5D_t147* ___EmptyArray_3;
};
