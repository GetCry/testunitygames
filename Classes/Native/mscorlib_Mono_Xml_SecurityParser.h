﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.SecurityElement
struct SecurityElement_t688;
// System.Collections.Stack
struct Stack_t325;

#include "mscorlib_Mono_Xml_SmallXmlParser.h"

// Mono.Xml.SecurityParser
struct  SecurityParser_t686  : public SmallXmlParser_t687
{
	// System.Security.SecurityElement Mono.Xml.SecurityParser::root
	SecurityElement_t688 * ___root_13;
	// System.Security.SecurityElement Mono.Xml.SecurityParser::current
	SecurityElement_t688 * ___current_14;
	// System.Collections.Stack Mono.Xml.SecurityParser::stack
	Stack_t325 * ___stack_15;
};
