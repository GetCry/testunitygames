﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Comparer
struct Comparer_t703;
// System.Globalization.CompareInfo
struct CompareInfo_t580;

#include "mscorlib_System_Object.h"

// System.Collections.Comparer
struct  Comparer_t703  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t580 * ___m_compareInfo_2;
};
struct Comparer_t703_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t703 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t703 * ___DefaultInvariant_1;
};
