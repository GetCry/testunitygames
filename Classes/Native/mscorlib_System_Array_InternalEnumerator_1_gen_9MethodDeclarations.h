﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8038_gshared (InternalEnumerator_1_t1336 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8038(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1336 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8038_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8039_gshared (InternalEnumerator_1_t1336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8039(__this, method) (( void (*) (InternalEnumerator_1_t1336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8039_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8040_gshared (InternalEnumerator_1_t1336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8040(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8040_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8041_gshared (InternalEnumerator_1_t1336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8041(__this, method) (( void (*) (InternalEnumerator_1_t1336 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8041_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8042_gshared (InternalEnumerator_1_t1336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8042(__this, method) (( bool (*) (InternalEnumerator_1_t1336 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8042_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern "C" GcAchievementData_t108  InternalEnumerator_1_get_Current_m8043_gshared (InternalEnumerator_1_t1336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8043(__this, method) (( GcAchievementData_t108  (*) (InternalEnumerator_1_t1336 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8043_gshared)(__this, method)
