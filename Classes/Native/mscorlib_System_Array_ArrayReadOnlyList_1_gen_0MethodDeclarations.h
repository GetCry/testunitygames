﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1475;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1278;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t1525;
// System.Exception
struct Exception_t134;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m9235_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgumentU5BU5D_t1278* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m9235(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgumentU5BU5D_t1278*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m9235_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9236_gshared (ArrayReadOnlyList_1_t1475 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9236(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t1475 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m9236_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t838  ArrayReadOnlyList_1_get_Item_m9237_gshared (ArrayReadOnlyList_1_t1475 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m9237(__this, ___index, method) (( CustomAttributeTypedArgument_t838  (*) (ArrayReadOnlyList_1_t1475 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m9237_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m9238_gshared (ArrayReadOnlyList_1_t1475 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m9238(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m9238_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m9239_gshared (ArrayReadOnlyList_1_t1475 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m9239(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t1475 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m9239_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m9240_gshared (ArrayReadOnlyList_1_t1475 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m9240(__this, method) (( bool (*) (ArrayReadOnlyList_1_t1475 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m9240_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m9241_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m9241(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_Add_m9241_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m9242_gshared (ArrayReadOnlyList_1_t1475 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m9242(__this, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m9242_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m9243_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m9243(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m9243_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m9244_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgumentU5BU5D_t1278* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m9244(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgumentU5BU5D_t1278*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m9244_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m9245_gshared (ArrayReadOnlyList_1_t1475 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m9245(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t1475 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m9245_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m9246_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m9246(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m9246_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m9247_gshared (ArrayReadOnlyList_1_t1475 * __this, int32_t ___index, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m9247(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, int32_t, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m9247_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m9248_gshared (ArrayReadOnlyList_1_t1475 * __this, CustomAttributeTypedArgument_t838  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m9248(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t1475 *, CustomAttributeTypedArgument_t838 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m9248_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m9249_gshared (ArrayReadOnlyList_1_t1475 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m9249(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t1475 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m9249_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C" Exception_t134 * ArrayReadOnlyList_1_ReadOnlyError_m9250_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m9250(__this /* static, unused */, method) (( Exception_t134 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m9250_gshared)(__this /* static, unused */, method)
