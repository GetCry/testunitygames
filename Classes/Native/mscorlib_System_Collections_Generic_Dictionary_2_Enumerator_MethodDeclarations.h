﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t1371;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator_.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m8421_gshared (Enumerator_t1381 * __this, Dictionary_2_t1371 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m8421(__this, ___dictionary, method) (( void (*) (Enumerator_t1381 *, Dictionary_2_t1371 *, const MethodInfo*))Enumerator__ctor_m8421_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m8422_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m8422(__this, method) (( Object_t * (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m8422_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m8423_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m8423(__this, method) (( void (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m8423_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t376  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8424_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8424(__this, method) (( DictionaryEntry_t376  (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m8424_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8425_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8425(__this, method) (( Object_t * (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m8425_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8426_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8426(__this, method) (( Object_t * (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m8426_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m8427_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m8427(__this, method) (( bool (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_MoveNext_m8427_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t1374  Enumerator_get_Current_m8428_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m8428(__this, method) (( KeyValuePair_2_t1374  (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_get_Current_m8428_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m8429_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m8429(__this, method) (( Object_t * (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_get_CurrentKey_m8429_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m8430_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m8430(__this, method) (( bool (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_get_CurrentValue_m8430_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m8431_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_Reset_m8431(__this, method) (( void (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_Reset_m8431_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m8432_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m8432(__this, method) (( void (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_VerifyState_m8432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m8433_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m8433(__this, method) (( void (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_VerifyCurrent_m8433_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m8434_gshared (Enumerator_t1381 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m8434(__this, method) (( void (*) (Enumerator_t1381 *, const MethodInfo*))Enumerator_Dispose_m8434_gshared)(__this, method)
