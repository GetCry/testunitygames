﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1389;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1373;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t165;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t1521;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t161;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t1522;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t375;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m8478_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m8478(__this, method) (( void (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2__ctor_m8478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m8480_gshared (Dictionary_2_t1389 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m8480(__this, ___comparer, method) (( void (*) (Dictionary_2_t1389 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m8480_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m8481_gshared (Dictionary_2_t1389 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m8481(__this, ___capacity, method) (( void (*) (Dictionary_2_t1389 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m8481_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m8483_gshared (Dictionary_2_t1389 * __this, SerializationInfo_t165 * ___info, StreamingContext_t166  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m8483(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1389 *, SerializationInfo_t165 *, StreamingContext_t166 , const MethodInfo*))Dictionary_2__ctor_m8483_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m8485_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m8485(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m8485_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m8487_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m8487(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m8487_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m8489_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m8489(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m8489_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m8491_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m8491(__this, ___key, method) (( bool (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m8491_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m8493_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m8493(__this, ___key, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m8493_gshared)(__this, ___key, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8495_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8495(__this, method) (( Object_t * (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m8495_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8497_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8497(__this, method) (( bool (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m8497_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8499_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2_t1391  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8499(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1389 *, KeyValuePair_2_t1391 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m8499_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8501_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2_t1391  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8501(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1389 *, KeyValuePair_2_t1391 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m8501_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8503_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2U5BU5D_t1521* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8503(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1389 *, KeyValuePair_2U5BU5D_t1521*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m8503_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8505_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2_t1391  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8505(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1389 *, KeyValuePair_2_t1391 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m8505_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m8507_gshared (Dictionary_2_t1389 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m8507(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m8507_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8509_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8509(__this, method) (( Object_t * (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m8509_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8511_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8511(__this, method) (( Object_t* (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m8511_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8513_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8513(__this, method) (( Object_t * (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m8513_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m8515_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m8515(__this, method) (( int32_t (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_get_Count_m8515_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m8517_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m8517(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m8517_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m8519_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m8519(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m8519_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m8521_gshared (Dictionary_2_t1389 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m8521(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1389 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m8521_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m8523_gshared (Dictionary_2_t1389 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m8523(__this, ___size, method) (( void (*) (Dictionary_2_t1389 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m8523_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m8525_gshared (Dictionary_2_t1389 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m8525(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1389 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m8525_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1391  Dictionary_2_make_pair_m8527_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m8527(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1391  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m8527_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m8529_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2U5BU5D_t1521* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m8529(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1389 *, KeyValuePair_2U5BU5D_t1521*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m8529_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m8531_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m8531(__this, method) (( void (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_Resize_m8531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m8533_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m8533(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m8533_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m8535_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m8535(__this, method) (( void (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_Clear_m8535_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m8537_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m8537(__this, ___key, method) (( bool (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m8537_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m8539_gshared (Dictionary_2_t1389 * __this, SerializationInfo_t165 * ___info, StreamingContext_t166  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m8539(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1389 *, SerializationInfo_t165 *, StreamingContext_t166 , const MethodInfo*))Dictionary_2_GetObjectData_m8539_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m8541_gshared (Dictionary_2_t1389 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m8541(__this, ___sender, method) (( void (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m8541_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m8543_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m8543(__this, ___key, method) (( bool (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m8543_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m8545_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m8545(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1389 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m8545_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m8547_gshared (Dictionary_2_t1389 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m8547(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m8547_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m8549_gshared (Dictionary_2_t1389 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m8549(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1389 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m8549_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m8551_gshared (Dictionary_2_t1389 * __this, KeyValuePair_2_t1391  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m8551(__this, ___pair, method) (( bool (*) (Dictionary_2_t1389 *, KeyValuePair_2_t1391 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m8551_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t1394  Dictionary_2_GetEnumerator_m8553_gshared (Dictionary_2_t1389 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m8553(__this, method) (( Enumerator_t1394  (*) (Dictionary_2_t1389 *, const MethodInfo*))Dictionary_2_GetEnumerator_m8553_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t376  Dictionary_2_U3CCopyToU3Em__0_m8555_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m8555(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t376  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m8555_gshared)(__this /* static, unused */, ___key, ___value, method)
