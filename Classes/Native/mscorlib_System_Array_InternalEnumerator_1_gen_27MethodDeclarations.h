﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27.h"

// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m8389_gshared (InternalEnumerator_1_t1376 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m8389(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1376 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m8389_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8390_gshared (InternalEnumerator_1_t1376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8390(__this, method) (( void (*) (InternalEnumerator_1_t1376 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m8390_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8391_gshared (InternalEnumerator_1_t1376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8391(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1376 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m8391_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m8392_gshared (InternalEnumerator_1_t1376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m8392(__this, method) (( void (*) (InternalEnumerator_1_t1376 *, const MethodInfo*))InternalEnumerator_1_Dispose_m8392_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m8393_gshared (InternalEnumerator_1_t1376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m8393(__this, method) (( bool (*) (InternalEnumerator_1_t1376 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m8393_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern "C" int32_t InternalEnumerator_1_get_Current_m8394_gshared (InternalEnumerator_1_t1376 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m8394(__this, method) (( int32_t (*) (InternalEnumerator_1_t1376 *, const MethodInfo*))InternalEnumerator_1_get_Current_m8394_gshared)(__this, method)
