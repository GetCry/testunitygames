﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t201;

#include "mscorlib_System_Collections_Hashtable.h"

// System.Collections.Hashtable/SyncHashtable
struct  SyncHashtable_t710  : public Hashtable_t201
{
	// System.Collections.Hashtable System.Collections.Hashtable/SyncHashtable::host
	Hashtable_t201 * ___host_13;
};
