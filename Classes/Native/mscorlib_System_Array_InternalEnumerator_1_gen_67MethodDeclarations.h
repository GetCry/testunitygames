﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_67.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m9069_gshared (InternalEnumerator_1_t1460 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m9069(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1460 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m9069_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9070_gshared (InternalEnumerator_1_t1460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9070(__this, method) (( void (*) (InternalEnumerator_1_t1460 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9070_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9071_gshared (InternalEnumerator_1_t1460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9071(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1460 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9071_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m9072_gshared (InternalEnumerator_1_t1460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m9072(__this, method) (( void (*) (InternalEnumerator_1_t1460 *, const MethodInfo*))InternalEnumerator_1_Dispose_m9072_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m9073_gshared (InternalEnumerator_1_t1460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m9073(__this, method) (( bool (*) (InternalEnumerator_1_t1460 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m9073_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern "C" LabelData_t795  InternalEnumerator_1_get_Current_m9074_gshared (InternalEnumerator_1_t1460 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m9074(__this, method) (( LabelData_t795  (*) (InternalEnumerator_1_t1460 *, const MethodInfo*))InternalEnumerator_1_get_Current_m9074_gshared)(__this, method)
