﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m9421(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1491 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m7876_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m9422(__this, method) (( void (*) (InternalEnumerator_1_t1491 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m7878_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m9423(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1491 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m7880_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m9424(__this, method) (( void (*) (InternalEnumerator_1_t1491 *, const MethodInfo*))InternalEnumerator_1_Dispose_m7882_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m9425(__this, method) (( bool (*) (InternalEnumerator_1_t1491 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m7884_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Contexts.IContextAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m9426(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1491 *, const MethodInfo*))InternalEnumerator_1_get_Current_m7886_gshared)(__this, method)
